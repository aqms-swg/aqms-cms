package com.isti.quakewatch.corbaclient.qw_client_rec;

/**
 * Holder class for : DomainChanBlkSeq
 * 
 * @author OpenORB Compiler
 */
final public class DomainChanBlkSeqHolder
        implements org.omg.CORBA.portable.Streamable
{
    /**
     * Internal DomainChanBlkSeq value
     */
    public com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlk[] value;

    /**
     * Default constructor
     */
    public DomainChanBlkSeqHolder()
    { }

    /**
     * Constructor with value initialisation
     * @param initial the initial value
     */
    public DomainChanBlkSeqHolder(com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlk[] initial)
    {
        value = initial;
    }

    /**
     * Read DomainChanBlkSeq from a marshalled stream
     * @param istream the input stream
     */
    public void _read(org.omg.CORBA.portable.InputStream istream)
    {
        value = DomainChanBlkSeqHelper.read(istream);
    }

    /**
     * Write DomainChanBlkSeq into a marshalled stream
     * @param ostream the output stream
     */
    public void _write(org.omg.CORBA.portable.OutputStream ostream)
    {
        DomainChanBlkSeqHelper.write(ostream,value);
    }

    /**
     * Return the DomainChanBlkSeq TypeCode
     * @return a TypeCode
     */
    public org.omg.CORBA.TypeCode _type()
    {
        return DomainChanBlkSeqHelper.type();
    }

}
