package com.isti.quakewatch.corbaclient.qw_client_rec;

/**
 * Defines the "service" methods provided to a "push" subscriber
 * to a CORBA outputter.  This object is implemented by the
 * CORBA outputter and its methods are invoked by the
 * subscriber.
 */
public abstract class QWPushSubServicesPOA extends org.omg.PortableServer.Servant
        implements QWPushSubServicesOperations, org.omg.CORBA.portable.InvokeHandler
{
    public QWPushSubServices _this()
    {
        return QWPushSubServicesHelper.narrow(_this_object());
    }

    public QWPushSubServices _this(org.omg.CORBA.ORB orb)
    {
        return QWPushSubServicesHelper.narrow(_this_object(orb));
    }

    private static String [] _ids_list =
    {
        "IDL:com/isti/quakewatch/corbaclient/qw_client_rec/QWPushSubServices:1.0"
    };

    public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte [] objectId)
    {
        return _ids_list;
    }

    public final org.omg.CORBA.portable.OutputStream _invoke(final String opName,
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler)
    {

        if (opName.equals("checkConnection")) {
                return _invoke_checkConnection(_is, handler);
        } else if (opName.equals("connectRecMsgCallBack")) {
                return _invoke_connectRecMsgCallBack(_is, handler);
        } else if (opName.equals("disconnectSubscriber")) {
                return _invoke_disconnectSubscriber(_is, handler);
        } else if (opName.equals("setCallBackEnabled")) {
                return _invoke_setCallBackEnabled(_is, handler);
        } else if (opName.equals("subscribe")) {
                return _invoke_subscribe(_is, handler);
        } else if (opName.equals("unsubscribe")) {
                return _invoke_unsubscribe(_is, handler);
        } else {
            throw new org.omg.CORBA.BAD_OPERATION(opName);
        }
    }

    // helper methods
    private org.omg.CORBA.portable.OutputStream _invoke_checkConnection(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;

        checkConnection();

        _output = handler.createReply();

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_connectRecMsgCallBack(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        com.isti.quakewatch.corbaclient.qw_client_rec.QWRecMsgCallBack arg0_in = com.isti.quakewatch.corbaclient.qw_client_rec.QWRecMsgCallBackHelper.read(_is);
        boolean arg1_in = _is.read_boolean();

        connectRecMsgCallBack(arg0_in, arg1_in);

        _output = handler.createReply();

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_setCallBackEnabled(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        boolean arg0_in = _is.read_boolean();

        setCallBackEnabled(arg0_in);

        _output = handler.createReply();

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_disconnectSubscriber(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;

        String _arg_result = disconnectSubscriber();

        _output = handler.createReply();
        _output.write_string(_arg_result);

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_subscribe(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        String arg0_in = _is.read_string();
        String arg1_in = _is.read_string();

        subscribe(arg0_in, arg1_in);

        _output = handler.createReply();

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_unsubscribe(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        String arg0_in = _is.read_string();
        String arg1_in = _is.read_string();

        unsubscribe(arg0_in, arg1_in);

        _output = handler.createReply();

        return _output;
    }

}
