package com.isti.quakewatch.corbaclient.qw_client_rec;

/**
 * Holder class for : DomainChanBlk
 * 
 * @author OpenORB Compiler
 */
final public class DomainChanBlkHolder
        implements org.omg.CORBA.portable.Streamable
{
    /**
     * Internal DomainChanBlk value
     */
    public com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlk value;

    /**
     * Default constructor
     */
    public DomainChanBlkHolder()
    { }

    /**
     * Constructor with value initialisation
     * @param initial the initial value
     */
    public DomainChanBlkHolder(com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlk initial)
    {
        value = initial;
    }

    /**
     * Read DomainChanBlk from a marshalled stream
     * @param istream the input stream
     */
    public void _read(org.omg.CORBA.portable.InputStream istream)
    {
        value = DomainChanBlkHelper.read(istream);
    }

    /**
     * Write DomainChanBlk into a marshalled stream
     * @param ostream the output stream
     */
    public void _write(org.omg.CORBA.portable.OutputStream ostream)
    {
        DomainChanBlkHelper.write(ostream,value);
    }

    /**
     * Return the DomainChanBlk TypeCode
     * @return a TypeCode
     */
    public org.omg.CORBA.TypeCode _type()
    {
        return DomainChanBlkHelper.type();
    }

}
