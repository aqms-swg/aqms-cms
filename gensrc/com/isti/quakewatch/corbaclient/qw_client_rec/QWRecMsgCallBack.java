package com.isti.quakewatch.corbaclient.qw_client_rec;

/**
 * Defines the call-back method used to send messages from
 * a CORBA outputter to a "push" subscriber.
 * This object is implemented by the subscriber and its
 * method is invoked by the CORBA outputter.
 */
public interface QWRecMsgCallBack extends QWRecMsgCallBackOperations, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity
{
}
