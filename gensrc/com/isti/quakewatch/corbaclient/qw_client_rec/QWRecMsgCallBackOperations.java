package com.isti.quakewatch.corbaclient.qw_client_rec;

/**
 * Defines the call-back method used to send messages from
 * a CORBA outputter to a "push" subscriber.
 * This object is implemented by the subscriber and its
 * method is invoked by the CORBA outputter.
 */
public interface QWRecMsgCallBackOperations
{
    /**
     * Receives a data message.  If 'false' is returned by this
     * method then future message will not be delivered until
     * 'setCallBackEnabled(true)' is called.
     * @return  true if the call-back should remain enabled; false
     * if the call-back should be disabled (in which case the
     * subscriber will use the 'setCallBackEnabled()' method to
     * enable the call-back later).
     */
    public boolean receiveMessage(String xmlMsgStr);

}
