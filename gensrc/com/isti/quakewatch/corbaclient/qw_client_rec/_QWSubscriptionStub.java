package com.isti.quakewatch.corbaclient.qw_client_rec;

/**
 * Defines methods used by subscribers to interact with a
 * QuakeWatch CORBA outputter.
 */
public class _QWSubscriptionStub extends org.omg.CORBA.portable.ObjectImpl
        implements QWSubscription
{
    static final String[] _ids_list =
    {
        "IDL:com/isti/quakewatch/corbaclient/qw_client_rec/QWSubscription:1.0"
    };

    public String[] _ids()
    {
     return _ids_list;
    }

    private final static Class _opsClass = com.isti.quakewatch.corbaclient.qw_client_rec.QWSubscriptionOperations.class;

    /**
     * Operation connectPushSubscriberTo
     */
    public String connectPushSubscriberTo(String subscriberNameStr, com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlk[] domainChanBlkArr, com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesHolder subServicesHolder)
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("connectPushSubscriberTo",true);
                    _output.write_string(subscriberNameStr);
                    com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlkSeqHelper.write(_output,domainChanBlkArr);
                    _input = this._invoke(_output);
                    String _arg_ret = _input.read_string();
                    subServicesHolder.value = com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesHelper.read(_input);
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("connectPushSubscriberTo",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.corbaclient.qw_client_rec.QWSubscriptionOperations _self = (com.isti.quakewatch.corbaclient.qw_client_rec.QWSubscriptionOperations) _so.servant;
                try
                {
                    return _self.connectPushSubscriberTo( subscriberNameStr,  domainChanBlkArr,  subServicesHolder);
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation connectPushSubscriber
     */
    public String connectPushSubscriber(String subscriberNameStr, com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesHolder subServicesHolder)
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("connectPushSubscriber",true);
                    _output.write_string(subscriberNameStr);
                    _input = this._invoke(_output);
                    String _arg_ret = _input.read_string();
                    subServicesHolder.value = com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesHelper.read(_input);
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("connectPushSubscriber",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.corbaclient.qw_client_rec.QWSubscriptionOperations _self = (com.isti.quakewatch.corbaclient.qw_client_rec.QWSubscriptionOperations) _so.servant;
                try
                {
                    return _self.connectPushSubscriber( subscriberNameStr,  subServicesHolder);
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

}
