package com.isti.quakewatch.corbaclient.qw_client_rec;

/**
 * Holder class for : QWPushSubServices
 * 
 * @author OpenORB Compiler
 */
final public class QWPushSubServicesHolder
        implements org.omg.CORBA.portable.Streamable
{
    /**
     * Internal QWPushSubServices value
     */
    public com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServices value;

    /**
     * Default constructor
     */
    public QWPushSubServicesHolder()
    { }

    /**
     * Constructor with value initialisation
     * @param initial the initial value
     */
    public QWPushSubServicesHolder(com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServices initial)
    {
        value = initial;
    }

    /**
     * Read QWPushSubServices from a marshalled stream
     * @param istream the input stream
     */
    public void _read(org.omg.CORBA.portable.InputStream istream)
    {
        value = QWPushSubServicesHelper.read(istream);
    }

    /**
     * Write QWPushSubServices into a marshalled stream
     * @param ostream the output stream
     */
    public void _write(org.omg.CORBA.portable.OutputStream ostream)
    {
        QWPushSubServicesHelper.write(ostream,value);
    }

    /**
     * Return the QWPushSubServices TypeCode
     * @return a TypeCode
     */
    public org.omg.CORBA.TypeCode _type()
    {
        return QWPushSubServicesHelper.type();
    }

}
