package com.isti.quakewatch.corbaclient.qw_client_rec;

/**
 * Defines the call-back method used to send messages from
 * a CORBA outputter to a "push" subscriber.
 * This object is implemented by the subscriber and its
 * method is invoked by the CORBA outputter.
 */
public class QWRecMsgCallBackPOATie extends QWRecMsgCallBackPOA
{

    //
    // Private reference to implementation object
    //
    private QWRecMsgCallBackOperations _tie;

    //
    // Private reference to POA
    //
    private org.omg.PortableServer.POA _poa;

    /**
     * Constructor
     */
    public QWRecMsgCallBackPOATie(QWRecMsgCallBackOperations tieObject)
    {
        _tie = tieObject;
    }

    /**
     * Constructor
     */
    public QWRecMsgCallBackPOATie(QWRecMsgCallBackOperations tieObject, org.omg.PortableServer.POA poa)
    {
        _tie = tieObject;
        _poa = poa;
    }

    /**
     * Get the delegate
     */
    public QWRecMsgCallBackOperations _delegate()
    {
        return _tie;
    }

    /**
     * Set the delegate
     */
    public void _delegate(QWRecMsgCallBackOperations delegate_)
    {
        _tie = delegate_;
    }

    /**
     * _default_POA method
     */
    public org.omg.PortableServer.POA _default_POA()
    {
        if (_poa != null)
            return _poa;
        else
            return super._default_POA();
    }

    /**
     * Operation receiveMessage
     */
    public boolean receiveMessage(String xmlMsgStr)
    {
        return _tie.receiveMessage( xmlMsgStr);
    }

}
