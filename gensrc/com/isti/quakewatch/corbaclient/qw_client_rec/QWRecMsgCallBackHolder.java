package com.isti.quakewatch.corbaclient.qw_client_rec;

/**
 * Holder class for : QWRecMsgCallBack
 * 
 * @author OpenORB Compiler
 */
final public class QWRecMsgCallBackHolder
        implements org.omg.CORBA.portable.Streamable
{
    /**
     * Internal QWRecMsgCallBack value
     */
    public com.isti.quakewatch.corbaclient.qw_client_rec.QWRecMsgCallBack value;

    /**
     * Default constructor
     */
    public QWRecMsgCallBackHolder()
    { }

    /**
     * Constructor with value initialisation
     * @param initial the initial value
     */
    public QWRecMsgCallBackHolder(com.isti.quakewatch.corbaclient.qw_client_rec.QWRecMsgCallBack initial)
    {
        value = initial;
    }

    /**
     * Read QWRecMsgCallBack from a marshalled stream
     * @param istream the input stream
     */
    public void _read(org.omg.CORBA.portable.InputStream istream)
    {
        value = QWRecMsgCallBackHelper.read(istream);
    }

    /**
     * Write QWRecMsgCallBack into a marshalled stream
     * @param ostream the output stream
     */
    public void _write(org.omg.CORBA.portable.OutputStream ostream)
    {
        QWRecMsgCallBackHelper.write(ostream,value);
    }

    /**
     * Return the QWRecMsgCallBack TypeCode
     * @return a TypeCode
     */
    public org.omg.CORBA.TypeCode _type()
    {
        return QWRecMsgCallBackHelper.type();
    }

}
