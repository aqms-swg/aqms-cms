package com.isti.quakewatch.corbaclient.qw_client_rec;

/**
 * Defines the "service" methods provided to a "push" subscriber
 * to a CORBA outputter.  This object is implemented by the
 * CORBA outputter and its methods are invoked by the
 * subscriber.
 */
public class QWPushSubServicesPOATie extends QWPushSubServicesPOA
{

    //
    // Private reference to implementation object
    //
    private QWPushSubServicesOperations _tie;

    //
    // Private reference to POA
    //
    private org.omg.PortableServer.POA _poa;

    /**
     * Constructor
     */
    public QWPushSubServicesPOATie(QWPushSubServicesOperations tieObject)
    {
        _tie = tieObject;
    }

    /**
     * Constructor
     */
    public QWPushSubServicesPOATie(QWPushSubServicesOperations tieObject, org.omg.PortableServer.POA poa)
    {
        _tie = tieObject;
        _poa = poa;
    }

    /**
     * Get the delegate
     */
    public QWPushSubServicesOperations _delegate()
    {
        return _tie;
    }

    /**
     * Set the delegate
     */
    public void _delegate(QWPushSubServicesOperations delegate_)
    {
        _tie = delegate_;
    }

    /**
     * _default_POA method
     */
    public org.omg.PortableServer.POA _default_POA()
    {
        if (_poa != null)
            return _poa;
        else
            return super._default_POA();
    }

    /**
     * Operation checkConnection
     */
    public void checkConnection()
    {
        _tie.checkConnection();
    }

    /**
     * Operation connectRecMsgCallBack
     */
    public void connectRecMsgCallBack(com.isti.quakewatch.corbaclient.qw_client_rec.QWRecMsgCallBack recMsgCallBackObj, boolean initialEnableFlag)
    {
        _tie.connectRecMsgCallBack( recMsgCallBackObj,  initialEnableFlag);
    }

    /**
     * Operation setCallBackEnabled
     */
    public void setCallBackEnabled(boolean enabledFlag)
    {
        _tie.setCallBackEnabled( enabledFlag);
    }

    /**
     * Operation disconnectSubscriber
     */
    public String disconnectSubscriber()
    {
        return _tie.disconnectSubscriber();
    }

    /**
     * Operation subscribe
     */
    public void subscribe(String domainStr, String chanStr)
    {
        _tie.subscribe( domainStr,  chanStr);
    }

    /**
     * Operation unsubscribe
     */
    public void unsubscribe(String domainStr, String chanStr)
    {
        _tie.unsubscribe( domainStr,  chanStr);
    }

}
