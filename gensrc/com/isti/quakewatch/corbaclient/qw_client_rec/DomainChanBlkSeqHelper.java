package com.isti.quakewatch.corbaclient.qw_client_rec;

/** 
 * Helper class for : DomainChanBlkSeq
 *  
 * @author OpenORB Compiler
 */ 
public class DomainChanBlkSeqHelper
{
    private static final boolean HAS_OPENORB;
    static
    {
        boolean hasOpenORB = false;
        try
        {
            Thread.currentThread().getContextClassLoader().loadClass( "org.openorb.orb.core.Any" );
            hasOpenORB = true;
        }
        catch ( ClassNotFoundException ex )
        {
            // do nothing
        }
        HAS_OPENORB = hasOpenORB;
    }
    /**
     * Insert DomainChanBlkSeq into an any
     * @param a an any
     * @param t DomainChanBlkSeq value
     */
    public static void insert(org.omg.CORBA.Any a, com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlk[] t)
    {
        a.insert_Streamable(new com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlkSeqHolder(t));
    }

    /**
     * Extract DomainChanBlkSeq from an any
     *
     * @param a an any
     * @return the extracted DomainChanBlkSeq value
     */
    public static com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlk[] extract( org.omg.CORBA.Any a )
    {
        if ( !a.type().equivalent( type() ) )
        {
            throw new org.omg.CORBA.MARSHAL();
        }
        if ( HAS_OPENORB && a instanceof org.openorb.orb.core.Any )
        {
            // streamable extraction. The jdk stubs incorrectly define the Any stub
            org.openorb.orb.core.Any any = ( org.openorb.orb.core.Any ) a;
            try
            {
                org.omg.CORBA.portable.Streamable s = any.extract_Streamable();
                if ( s instanceof com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlkSeqHolder )
                {
                    return ( ( com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlkSeqHolder ) s ).value;
                }
            }
            catch ( org.omg.CORBA.BAD_INV_ORDER ex )
            {
            }
            com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlkSeqHolder h = new com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlkSeqHolder( read( a.create_input_stream() ) );
            a.insert_Streamable( h );
            return h.value;
        }
        return read( a.create_input_stream() );
    }

    //
    // Internal TypeCode value
    //
    private static org.omg.CORBA.TypeCode _tc = null;

    /**
     * Return the DomainChanBlkSeq TypeCode
     * @return a TypeCode
     */
    public static org.omg.CORBA.TypeCode type()
    {
        if (_tc == null) {
            org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init();
            _tc = orb.create_alias_tc( id(), "DomainChanBlkSeq", orb.create_sequence_tc( 0, com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlkHelper.type() ) );
        }
        return _tc;
    }

    /**
     * Return the DomainChanBlkSeq IDL ID
     * @return an ID
     */
    public static String id()
    {
        return _id;
    }

    private final static String _id = "IDL:com/isti/quakewatch/corbaclient/qw_client_rec/DomainChanBlkSeq:1.0";

    /**
     * Read DomainChanBlkSeq from a marshalled stream
     * @param istream the input stream
     * @return the readed DomainChanBlkSeq value
     */
    public static com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlk[] read(org.omg.CORBA.portable.InputStream istream)
    {
        com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlk[] new_one;
        {
        int size7 = istream.read_ulong();
        new_one = new com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlk[size7];
        for (int i7=0; i7<new_one.length; i7++)
         {
            new_one[i7] = com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlkHelper.read(istream);

         }
        }

        return new_one;
    }

    /**
     * Write DomainChanBlkSeq into a marshalled stream
     * @param ostream the output stream
     * @param value DomainChanBlkSeq value
     */
    public static void write(org.omg.CORBA.portable.OutputStream ostream, com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlk[] value)
    {
        ostream.write_ulong( value.length );
        for ( int i7 = 0; i7 < value.length; i7++ )
        {
            com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlkHelper.write( ostream, value[ i7 ] );

        }
    }

}
