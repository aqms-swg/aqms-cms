package com.isti.quakewatch.corbaclient.qw_client_rec;

/** 
 * Helper class for : QWPushSubServices
 *  
 * @author OpenORB Compiler
 */ 
public class QWPushSubServicesHelper
{
    /**
     * Insert QWPushSubServices into an any
     * @param a an any
     * @param t QWPushSubServices value
     */
    public static void insert(org.omg.CORBA.Any a, com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServices t)
    {
        a.insert_Object(t , type());
    }

    /**
     * Extract QWPushSubServices from an any
     *
     * @param a an any
     * @return the extracted QWPushSubServices value
     */
    public static com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServices extract( org.omg.CORBA.Any a )
    {
        if ( !a.type().equivalent( type() ) )
        {
            throw new org.omg.CORBA.MARSHAL();
        }
        try
        {
            return com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesHelper.narrow( a.extract_Object() );
        }
        catch ( final org.omg.CORBA.BAD_PARAM e )
        {
            throw new org.omg.CORBA.MARSHAL(e.getMessage());
        }
    }

    //
    // Internal TypeCode value
    //
    private static org.omg.CORBA.TypeCode _tc = null;

    /**
     * Return the QWPushSubServices TypeCode
     * @return a TypeCode
     */
    public static org.omg.CORBA.TypeCode type()
    {
        if (_tc == null) {
            org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init();
            _tc = orb.create_interface_tc( id(), "QWPushSubServices" );
        }
        return _tc;
    }

    /**
     * Return the QWPushSubServices IDL ID
     * @return an ID
     */
    public static String id()
    {
        return _id;
    }

    private final static String _id = "IDL:com/isti/quakewatch/corbaclient/qw_client_rec/QWPushSubServices:1.0";

    /**
     * Read QWPushSubServices from a marshalled stream
     * @param istream the input stream
     * @return the readed QWPushSubServices value
     */
    public static com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServices read(org.omg.CORBA.portable.InputStream istream)
    {
        return(com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServices)istream.read_Object(com.isti.quakewatch.corbaclient.qw_client_rec._QWPushSubServicesStub.class);
    }

    /**
     * Write QWPushSubServices into a marshalled stream
     * @param ostream the output stream
     * @param value QWPushSubServices value
     */
    public static void write(org.omg.CORBA.portable.OutputStream ostream, com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServices value)
    {
        ostream.write_Object((org.omg.CORBA.portable.ObjectImpl)value);
    }

    /**
     * Narrow CORBA::Object to QWPushSubServices
     * @param obj the CORBA Object
     * @return QWPushSubServices Object
     */
    public static QWPushSubServices narrow(org.omg.CORBA.Object obj)
    {
        if (obj == null)
            return null;
        if (obj instanceof QWPushSubServices)
            return (QWPushSubServices)obj;

        if (obj._is_a(id()))
        {
            _QWPushSubServicesStub stub = new _QWPushSubServicesStub();
            stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
            return stub;
        }

        throw new org.omg.CORBA.BAD_PARAM();
    }

    /**
     * Unchecked Narrow CORBA::Object to QWPushSubServices
     * @param obj the CORBA Object
     * @return QWPushSubServices Object
     */
    public static QWPushSubServices unchecked_narrow(org.omg.CORBA.Object obj)
    {
        if (obj == null)
            return null;
        if (obj instanceof QWPushSubServices)
            return (QWPushSubServices)obj;

        _QWPushSubServicesStub stub = new _QWPushSubServicesStub();
        stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
        return stub;

    }

}
