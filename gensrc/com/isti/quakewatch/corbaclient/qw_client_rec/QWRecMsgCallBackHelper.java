package com.isti.quakewatch.corbaclient.qw_client_rec;

/** 
 * Helper class for : QWRecMsgCallBack
 *  
 * @author OpenORB Compiler
 */ 
public class QWRecMsgCallBackHelper
{
    /**
     * Insert QWRecMsgCallBack into an any
     * @param a an any
     * @param t QWRecMsgCallBack value
     */
    public static void insert(org.omg.CORBA.Any a, com.isti.quakewatch.corbaclient.qw_client_rec.QWRecMsgCallBack t)
    {
        a.insert_Object(t , type());
    }

    /**
     * Extract QWRecMsgCallBack from an any
     *
     * @param a an any
     * @return the extracted QWRecMsgCallBack value
     */
    public static com.isti.quakewatch.corbaclient.qw_client_rec.QWRecMsgCallBack extract( org.omg.CORBA.Any a )
    {
        if ( !a.type().equivalent( type() ) )
        {
            throw new org.omg.CORBA.MARSHAL();
        }
        try
        {
            return com.isti.quakewatch.corbaclient.qw_client_rec.QWRecMsgCallBackHelper.narrow( a.extract_Object() );
        }
        catch ( final org.omg.CORBA.BAD_PARAM e )
        {
            throw new org.omg.CORBA.MARSHAL(e.getMessage());
        }
    }

    //
    // Internal TypeCode value
    //
    private static org.omg.CORBA.TypeCode _tc = null;

    /**
     * Return the QWRecMsgCallBack TypeCode
     * @return a TypeCode
     */
    public static org.omg.CORBA.TypeCode type()
    {
        if (_tc == null) {
            org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init();
            _tc = orb.create_interface_tc( id(), "QWRecMsgCallBack" );
        }
        return _tc;
    }

    /**
     * Return the QWRecMsgCallBack IDL ID
     * @return an ID
     */
    public static String id()
    {
        return _id;
    }

    private final static String _id = "IDL:com/isti/quakewatch/corbaclient/qw_client_rec/QWRecMsgCallBack:1.0";

    /**
     * Read QWRecMsgCallBack from a marshalled stream
     * @param istream the input stream
     * @return the readed QWRecMsgCallBack value
     */
    public static com.isti.quakewatch.corbaclient.qw_client_rec.QWRecMsgCallBack read(org.omg.CORBA.portable.InputStream istream)
    {
        return(com.isti.quakewatch.corbaclient.qw_client_rec.QWRecMsgCallBack)istream.read_Object(com.isti.quakewatch.corbaclient.qw_client_rec._QWRecMsgCallBackStub.class);
    }

    /**
     * Write QWRecMsgCallBack into a marshalled stream
     * @param ostream the output stream
     * @param value QWRecMsgCallBack value
     */
    public static void write(org.omg.CORBA.portable.OutputStream ostream, com.isti.quakewatch.corbaclient.qw_client_rec.QWRecMsgCallBack value)
    {
        ostream.write_Object((org.omg.CORBA.portable.ObjectImpl)value);
    }

    /**
     * Narrow CORBA::Object to QWRecMsgCallBack
     * @param obj the CORBA Object
     * @return QWRecMsgCallBack Object
     */
    public static QWRecMsgCallBack narrow(org.omg.CORBA.Object obj)
    {
        if (obj == null)
            return null;
        if (obj instanceof QWRecMsgCallBack)
            return (QWRecMsgCallBack)obj;

        if (obj._is_a(id()))
        {
            _QWRecMsgCallBackStub stub = new _QWRecMsgCallBackStub();
            stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
            return stub;
        }

        throw new org.omg.CORBA.BAD_PARAM();
    }

    /**
     * Unchecked Narrow CORBA::Object to QWRecMsgCallBack
     * @param obj the CORBA Object
     * @return QWRecMsgCallBack Object
     */
    public static QWRecMsgCallBack unchecked_narrow(org.omg.CORBA.Object obj)
    {
        if (obj == null)
            return null;
        if (obj instanceof QWRecMsgCallBack)
            return (QWRecMsgCallBack)obj;

        _QWRecMsgCallBackStub stub = new _QWRecMsgCallBackStub();
        stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
        return stub;

    }

}
