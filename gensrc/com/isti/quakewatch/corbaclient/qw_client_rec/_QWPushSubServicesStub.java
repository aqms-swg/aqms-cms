package com.isti.quakewatch.corbaclient.qw_client_rec;

/**
 * Defines the "service" methods provided to a "push" subscriber
 * to a CORBA outputter.  This object is implemented by the
 * CORBA outputter and its methods are invoked by the
 * subscriber.
 */
public class _QWPushSubServicesStub extends org.omg.CORBA.portable.ObjectImpl
        implements QWPushSubServices
{
    static final String[] _ids_list =
    {
        "IDL:com/isti/quakewatch/corbaclient/qw_client_rec/QWPushSubServices:1.0"
    };

    public String[] _ids()
    {
     return _ids_list;
    }

    private final static Class _opsClass = com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesOperations.class;

    /**
     * Operation checkConnection
     */
    public void checkConnection()
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("checkConnection",true);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("checkConnection",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesOperations _self = (com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesOperations) _so.servant;
                try
                {
                    _self.checkConnection();
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation connectRecMsgCallBack
     */
    public void connectRecMsgCallBack(com.isti.quakewatch.corbaclient.qw_client_rec.QWRecMsgCallBack recMsgCallBackObj, boolean initialEnableFlag)
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("connectRecMsgCallBack",true);
                    com.isti.quakewatch.corbaclient.qw_client_rec.QWRecMsgCallBackHelper.write(_output,recMsgCallBackObj);
                    _output.write_boolean(initialEnableFlag);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("connectRecMsgCallBack",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesOperations _self = (com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesOperations) _so.servant;
                try
                {
                    _self.connectRecMsgCallBack( recMsgCallBackObj,  initialEnableFlag);
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation setCallBackEnabled
     */
    public void setCallBackEnabled(boolean enabledFlag)
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("setCallBackEnabled",true);
                    _output.write_boolean(enabledFlag);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("setCallBackEnabled",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesOperations _self = (com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesOperations) _so.servant;
                try
                {
                    _self.setCallBackEnabled( enabledFlag);
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation disconnectSubscriber
     */
    public String disconnectSubscriber()
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("disconnectSubscriber",true);
                    _input = this._invoke(_output);
                    String _arg_ret = _input.read_string();
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("disconnectSubscriber",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesOperations _self = (com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesOperations) _so.servant;
                try
                {
                    return _self.disconnectSubscriber();
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation subscribe
     */
    public void subscribe(String domainStr, String chanStr)
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("subscribe",true);
                    _output.write_string(domainStr);
                    _output.write_string(chanStr);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("subscribe",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesOperations _self = (com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesOperations) _so.servant;
                try
                {
                    _self.subscribe( domainStr,  chanStr);
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation unsubscribe
     */
    public void unsubscribe(String domainStr, String chanStr)
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("unsubscribe",true);
                    _output.write_string(domainStr);
                    _output.write_string(chanStr);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("unsubscribe",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesOperations _self = (com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesOperations) _so.servant;
                try
                {
                    _self.unsubscribe( domainStr,  chanStr);
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

}
