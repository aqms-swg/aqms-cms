package com.isti.quakewatch.corbaclient.qw_client_rec;

/** 
 * Helper class for : QWSubscription
 *  
 * @author OpenORB Compiler
 */ 
public class QWSubscriptionHelper
{
    /**
     * Insert QWSubscription into an any
     * @param a an any
     * @param t QWSubscription value
     */
    public static void insert(org.omg.CORBA.Any a, com.isti.quakewatch.corbaclient.qw_client_rec.QWSubscription t)
    {
        a.insert_Object(t , type());
    }

    /**
     * Extract QWSubscription from an any
     *
     * @param a an any
     * @return the extracted QWSubscription value
     */
    public static com.isti.quakewatch.corbaclient.qw_client_rec.QWSubscription extract( org.omg.CORBA.Any a )
    {
        if ( !a.type().equivalent( type() ) )
        {
            throw new org.omg.CORBA.MARSHAL();
        }
        try
        {
            return com.isti.quakewatch.corbaclient.qw_client_rec.QWSubscriptionHelper.narrow( a.extract_Object() );
        }
        catch ( final org.omg.CORBA.BAD_PARAM e )
        {
            throw new org.omg.CORBA.MARSHAL(e.getMessage());
        }
    }

    //
    // Internal TypeCode value
    //
    private static org.omg.CORBA.TypeCode _tc = null;

    /**
     * Return the QWSubscription TypeCode
     * @return a TypeCode
     */
    public static org.omg.CORBA.TypeCode type()
    {
        if (_tc == null) {
            org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init();
            _tc = orb.create_interface_tc( id(), "QWSubscription" );
        }
        return _tc;
    }

    /**
     * Return the QWSubscription IDL ID
     * @return an ID
     */
    public static String id()
    {
        return _id;
    }

    private final static String _id = "IDL:com/isti/quakewatch/corbaclient/qw_client_rec/QWSubscription:1.0";

    /**
     * Read QWSubscription from a marshalled stream
     * @param istream the input stream
     * @return the readed QWSubscription value
     */
    public static com.isti.quakewatch.corbaclient.qw_client_rec.QWSubscription read(org.omg.CORBA.portable.InputStream istream)
    {
        return(com.isti.quakewatch.corbaclient.qw_client_rec.QWSubscription)istream.read_Object(com.isti.quakewatch.corbaclient.qw_client_rec._QWSubscriptionStub.class);
    }

    /**
     * Write QWSubscription into a marshalled stream
     * @param ostream the output stream
     * @param value QWSubscription value
     */
    public static void write(org.omg.CORBA.portable.OutputStream ostream, com.isti.quakewatch.corbaclient.qw_client_rec.QWSubscription value)
    {
        ostream.write_Object((org.omg.CORBA.portable.ObjectImpl)value);
    }

    /**
     * Narrow CORBA::Object to QWSubscription
     * @param obj the CORBA Object
     * @return QWSubscription Object
     */
    public static QWSubscription narrow(org.omg.CORBA.Object obj)
    {
        if (obj == null)
            return null;
        if (obj instanceof QWSubscription)
            return (QWSubscription)obj;

        if (obj._is_a(id()))
        {
            _QWSubscriptionStub stub = new _QWSubscriptionStub();
            stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
            return stub;
        }

        throw new org.omg.CORBA.BAD_PARAM();
    }

    /**
     * Unchecked Narrow CORBA::Object to QWSubscription
     * @param obj the CORBA Object
     * @return QWSubscription Object
     */
    public static QWSubscription unchecked_narrow(org.omg.CORBA.Object obj)
    {
        if (obj == null)
            return null;
        if (obj instanceof QWSubscription)
            return (QWSubscription)obj;

        _QWSubscriptionStub stub = new _QWSubscriptionStub();
        stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
        return stub;

    }

}
