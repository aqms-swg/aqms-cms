package com.isti.quakewatch.corbaclient.qw_client_rec;

/**
 * Information block holding event domain and chan strings.
 */
public final class DomainChanBlk implements org.omg.CORBA.portable.IDLEntity
{
    /**
     * Struct member domainStr
     */
    public String domainStr;

    /**
     * Struct member channelStr
     */
    public String channelStr;

    /**
     * Default constructor
     */
    public DomainChanBlk()
    { }

    /**
     * Constructor with fields initialization
     * @param domainStr domainStr struct member
     * @param channelStr channelStr struct member
     */
    public DomainChanBlk(String domainStr, String channelStr)
    {
        this.domainStr = domainStr;
        this.channelStr = channelStr;
    }

}
