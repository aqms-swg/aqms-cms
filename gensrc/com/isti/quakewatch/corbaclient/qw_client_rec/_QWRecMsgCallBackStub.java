package com.isti.quakewatch.corbaclient.qw_client_rec;

/**
 * Defines the call-back method used to send messages from
 * a CORBA outputter to a "push" subscriber.
 * This object is implemented by the subscriber and its
 * method is invoked by the CORBA outputter.
 */
public class _QWRecMsgCallBackStub extends org.omg.CORBA.portable.ObjectImpl
        implements QWRecMsgCallBack
{
    static final String[] _ids_list =
    {
        "IDL:com/isti/quakewatch/corbaclient/qw_client_rec/QWRecMsgCallBack:1.0"
    };

    public String[] _ids()
    {
     return _ids_list;
    }

    private final static Class _opsClass = com.isti.quakewatch.corbaclient.qw_client_rec.QWRecMsgCallBackOperations.class;

    /**
     * Operation receiveMessage
     */
    public boolean receiveMessage(String xmlMsgStr)
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("receiveMessage",true);
                    _output.write_string(xmlMsgStr);
                    _input = this._invoke(_output);
                    boolean _arg_ret = _input.read_boolean();
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("receiveMessage",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.corbaclient.qw_client_rec.QWRecMsgCallBackOperations _self = (com.isti.quakewatch.corbaclient.qw_client_rec.QWRecMsgCallBackOperations) _so.servant;
                try
                {
                    return _self.receiveMessage( xmlMsgStr);
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

}
