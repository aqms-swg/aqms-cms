package com.isti.quakewatch.corbaclient.qw_client_rec;

/**
 * Defines methods used by subscribers to interact with a
 * QuakeWatch CORBA outputter.
 */
public class QWSubscriptionPOATie extends QWSubscriptionPOA
{

    //
    // Private reference to implementation object
    //
    private QWSubscriptionOperations _tie;

    //
    // Private reference to POA
    //
    private org.omg.PortableServer.POA _poa;

    /**
     * Constructor
     */
    public QWSubscriptionPOATie(QWSubscriptionOperations tieObject)
    {
        _tie = tieObject;
    }

    /**
     * Constructor
     */
    public QWSubscriptionPOATie(QWSubscriptionOperations tieObject, org.omg.PortableServer.POA poa)
    {
        _tie = tieObject;
        _poa = poa;
    }

    /**
     * Get the delegate
     */
    public QWSubscriptionOperations _delegate()
    {
        return _tie;
    }

    /**
     * Set the delegate
     */
    public void _delegate(QWSubscriptionOperations delegate_)
    {
        _tie = delegate_;
    }

    /**
     * _default_POA method
     */
    public org.omg.PortableServer.POA _default_POA()
    {
        if (_poa != null)
            return _poa;
        else
            return super._default_POA();
    }

    /**
     * Operation connectPushSubscriberTo
     */
    public String connectPushSubscriberTo(String subscriberNameStr, com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlk[] domainChanBlkArr, com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesHolder subServicesHolder)
    {
        return _tie.connectPushSubscriberTo( subscriberNameStr,  domainChanBlkArr,  subServicesHolder);
    }

    /**
     * Operation connectPushSubscriber
     */
    public String connectPushSubscriber(String subscriberNameStr, com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesHolder subServicesHolder)
    {
        return _tie.connectPushSubscriber( subscriberNameStr,  subServicesHolder);
    }

}
