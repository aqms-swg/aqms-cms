package com.isti.quakewatch.corbaclient.qw_client_rec;

/**
 * Defines the "service" methods provided to a "push" subscriber
 * to a CORBA outputter.  This object is implemented by the
 * CORBA outputter and its methods are invoked by the
 * subscriber.
 */
public interface QWPushSubServicesOperations
{
    /**
     * Confirms the status of the connection to the CORBA outputter.
     * If the connection is OK then this method will return successfully;
     * if not then a CORBA exception will be thrown.
     */
    public void checkConnection();

    /**
     * Connects the given call-back object.  "Push" subscribers
     * will use this method.
     * @param  recMsgCallBackObj the subscriber call-back object to
     * use.
     * @param  initialEnableFlag true if the call-back method
     * should be initially enabled; false if the call-back method
     * should be initially disabled (in which case the subscriber
     * will use the 'setCallBackEnabled()' method to enable the
     * call-back later).
     */
    public void connectRecMsgCallBack(com.isti.quakewatch.corbaclient.qw_client_rec.QWRecMsgCallBack recMsgCallBackObj, boolean initialEnableFlag);

    /**
     * Enables the message-delivery call-back object.  "Push"
     * subscribers can use this method to control when messages
     * may be sent.
     * @param  enableFlag true if the call-back method should be
     * enabled; false if not.
     */
    public void setCallBackEnabled(boolean enabledFlag);

    /**
     * Disconnects this subscriber from the CORBA outputter.
     */
    public String disconnectSubscriber();

    /**
     * Adds the given domain/channel values to the list of those
     * registered for this subscriber.  If any domain/channel values
     * are registered then only messages with matching values will
     * be delivered to the subscriber.  If no domain/channel values
     * are registered then all messages will be delivered.
     * @param  domainStr the "domain" of interest.
     * @param  chanStr the "channel" of interest.
     */
    public void subscribe(String domainStr, String chanStr);

    /**
     * Removes the given domain/channel values from the list of those
     * registered for this subscriber.
     * @param  domainStr the "domain" of interest.
     * @param  chanStr the "channel" of interest.
     */
    public void unsubscribe(String domainStr, String chanStr);

}
