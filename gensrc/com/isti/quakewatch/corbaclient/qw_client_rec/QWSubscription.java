package com.isti.quakewatch.corbaclient.qw_client_rec;

/**
 * Defines methods used by subscribers to interact with a
 * QuakeWatch CORBA outputter.
 */
public interface QWSubscription extends QWSubscriptionOperations, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity
{
}
