package com.isti.quakewatch.corbaclient.qw_client_rec;

/**
 * Holder class for : QWSubscription
 * 
 * @author OpenORB Compiler
 */
final public class QWSubscriptionHolder
        implements org.omg.CORBA.portable.Streamable
{
    /**
     * Internal QWSubscription value
     */
    public com.isti.quakewatch.corbaclient.qw_client_rec.QWSubscription value;

    /**
     * Default constructor
     */
    public QWSubscriptionHolder()
    { }

    /**
     * Constructor with value initialisation
     * @param initial the initial value
     */
    public QWSubscriptionHolder(com.isti.quakewatch.corbaclient.qw_client_rec.QWSubscription initial)
    {
        value = initial;
    }

    /**
     * Read QWSubscription from a marshalled stream
     * @param istream the input stream
     */
    public void _read(org.omg.CORBA.portable.InputStream istream)
    {
        value = QWSubscriptionHelper.read(istream);
    }

    /**
     * Write QWSubscription into a marshalled stream
     * @param ostream the output stream
     */
    public void _write(org.omg.CORBA.portable.OutputStream ostream)
    {
        QWSubscriptionHelper.write(ostream,value);
    }

    /**
     * Return the QWSubscription TypeCode
     * @return a TypeCode
     */
    public org.omg.CORBA.TypeCode _type()
    {
        return QWSubscriptionHelper.type();
    }

}
