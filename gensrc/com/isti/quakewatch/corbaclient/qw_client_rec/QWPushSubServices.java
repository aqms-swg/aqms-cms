package com.isti.quakewatch.corbaclient.qw_client_rec;

/**
 * Defines the "service" methods provided to a "push" subscriber
 * to a CORBA outputter.  This object is implemented by the
 * CORBA outputter and its methods are invoked by the
 * subscriber.
 */
public interface QWPushSubServices extends QWPushSubServicesOperations, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity
{
}
