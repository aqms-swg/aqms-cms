package com.isti.quakewatch.corbaclient.qw_client_rec;

/**
 * Defines methods used by subscribers to interact with a
 * QuakeWatch CORBA outputter.
 */
public interface QWSubscriptionOperations
{
    /**
     * Connects a "push" subscriber to a QuakeWatch CORBA outputter.
     * @param  subscriberNameStr the name of the subscriber.
     * @param  domainChanBlkArr the array of domain/channel pairs to
     * be subscribed to.
     * @param  subServicesHolder the subscriber services object
     * to use for interacting with the CORBA outputter.
     * @return  An empty string if successful; an error message if
     * not.
     */
    public String connectPushSubscriberTo(String subscriberNameStr, com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlk[] domainChanBlkArr, com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesHolder subServicesHolder);

    /**
     * Connects a "push" subscriber to a QuakeWatch CORBA outputter.
     * Initially sets up no subscriptions, resulting in all
     * messages being received.
     * @param  subscriberNameStr the name of the subscriber.
     * @param  subServicesHolder the subscriber services object
     * to use for interacting with the CORBA outputter.
     * @return  An empty string if successful; an error message if
     * not.
     */
    public String connectPushSubscriber(String subscriberNameStr, com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesHolder subServicesHolder);

}
