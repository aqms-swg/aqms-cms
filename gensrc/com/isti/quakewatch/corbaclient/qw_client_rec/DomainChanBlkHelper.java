package com.isti.quakewatch.corbaclient.qw_client_rec;

/** 
 * Helper class for : DomainChanBlk
 *  
 * @author OpenORB Compiler
 */ 
public class DomainChanBlkHelper
{
    private static final boolean HAS_OPENORB;
    static
    {
        boolean hasOpenORB = false;
        try
        {
            Thread.currentThread().getContextClassLoader().loadClass( "org.openorb.orb.core.Any" );
            hasOpenORB = true;
        }
        catch ( ClassNotFoundException ex )
        {
            // do nothing
        }
        HAS_OPENORB = hasOpenORB;
    }
    /**
     * Insert DomainChanBlk into an any
     * @param a an any
     * @param t DomainChanBlk value
     */
    public static void insert(org.omg.CORBA.Any a, com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlk t)
    {
        a.insert_Streamable(new com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlkHolder(t));
    }

    /**
     * Extract DomainChanBlk from an any
     *
     * @param a an any
     * @return the extracted DomainChanBlk value
     */
    public static com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlk extract( org.omg.CORBA.Any a )
    {
        if ( !a.type().equivalent( type() ) )
        {
            throw new org.omg.CORBA.MARSHAL();
        }
        if (HAS_OPENORB && a instanceof org.openorb.orb.core.Any) {
            // streamable extraction. The jdk stubs incorrectly define the Any stub
            org.openorb.orb.core.Any any = (org.openorb.orb.core.Any)a;
            try {
                org.omg.CORBA.portable.Streamable s = any.extract_Streamable();
                if ( s instanceof com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlkHolder )
                    return ( ( com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlkHolder ) s ).value;
            }
            catch ( org.omg.CORBA.BAD_INV_ORDER ex )
            {
            }
            com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlkHolder h = new com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlkHolder( read( a.create_input_stream() ) );
            a.insert_Streamable( h );
            return h.value;
        }
        return read( a.create_input_stream() );
    }

    //
    // Internal TypeCode value
    //
    private static org.omg.CORBA.TypeCode _tc = null;
    private static boolean _working = false;

    /**
     * Return the DomainChanBlk TypeCode
     * @return a TypeCode
     */
    public static org.omg.CORBA.TypeCode type()
    {
        if (_tc == null) {
            synchronized(org.omg.CORBA.TypeCode.class) {
                if (_tc != null)
                    return _tc;
                if (_working)
                    return org.omg.CORBA.ORB.init().create_recursive_tc(id());
                _working = true;
                org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init();
                org.omg.CORBA.StructMember _members[] = new org.omg.CORBA.StructMember[ 2 ];

                _members[ 0 ] = new org.omg.CORBA.StructMember();
                _members[ 0 ].name = "domainStr";
                _members[ 0 ].type = orb.get_primitive_tc( org.omg.CORBA.TCKind.tk_string );
                _members[ 1 ] = new org.omg.CORBA.StructMember();
                _members[ 1 ].name = "channelStr";
                _members[ 1 ].type = orb.get_primitive_tc( org.omg.CORBA.TCKind.tk_string );
                _tc = orb.create_struct_tc( id(), "DomainChanBlk", _members );
                _working = false;
            }
        }
        return _tc;
    }

    /**
     * Return the DomainChanBlk IDL ID
     * @return an ID
     */
    public static String id()
    {
        return _id;
    }

    private final static String _id = "IDL:com/isti/quakewatch/corbaclient/qw_client_rec/DomainChanBlk:1.0";

    /**
     * Read DomainChanBlk from a marshalled stream
     * @param istream the input stream
     * @return the readed DomainChanBlk value
     */
    public static com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlk read(org.omg.CORBA.portable.InputStream istream)
    {
        com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlk new_one = new com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlk();

        new_one.domainStr = istream.read_string();
        new_one.channelStr = istream.read_string();

        return new_one;
    }

    /**
     * Write DomainChanBlk into a marshalled stream
     * @param ostream the output stream
     * @param value DomainChanBlk value
     */
    public static void write(org.omg.CORBA.portable.OutputStream ostream, com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlk value)
    {
        ostream.write_string( value.domainStr );
        ostream.write_string( value.channelStr );
    }

}
