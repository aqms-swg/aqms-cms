package com.isti.quakewatch.corbaclient.qw_client_rec;

/**
 * Defines the call-back method used to send messages from
 * a CORBA outputter to a "push" subscriber.
 * This object is implemented by the subscriber and its
 * method is invoked by the CORBA outputter.
 */
public abstract class QWRecMsgCallBackPOA extends org.omg.PortableServer.Servant
        implements QWRecMsgCallBackOperations, org.omg.CORBA.portable.InvokeHandler
{
    public QWRecMsgCallBack _this()
    {
        return QWRecMsgCallBackHelper.narrow(_this_object());
    }

    public QWRecMsgCallBack _this(org.omg.CORBA.ORB orb)
    {
        return QWRecMsgCallBackHelper.narrow(_this_object(orb));
    }

    private static String [] _ids_list =
    {
        "IDL:com/isti/quakewatch/corbaclient/qw_client_rec/QWRecMsgCallBack:1.0"
    };

    public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte [] objectId)
    {
        return _ids_list;
    }

    public final org.omg.CORBA.portable.OutputStream _invoke(final String opName,
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler)
    {

        if (opName.equals("receiveMessage")) {
                return _invoke_receiveMessage(_is, handler);
        } else {
            throw new org.omg.CORBA.BAD_OPERATION(opName);
        }
    }

    // helper methods
    private org.omg.CORBA.portable.OutputStream _invoke_receiveMessage(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        String arg0_in = _is.read_string();

        boolean _arg_result = receiveMessage(arg0_in);

        _output = handler.createReply();
        _output.write_boolean(_arg_result);

        return _output;
    }

}
