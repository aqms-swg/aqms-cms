package com.isti.quakewatch.corbaclient.qw_client_rec;

/**
 * Defines methods used by subscribers to interact with a
 * QuakeWatch CORBA outputter.
 */
public abstract class QWSubscriptionPOA extends org.omg.PortableServer.Servant
        implements QWSubscriptionOperations, org.omg.CORBA.portable.InvokeHandler
{
    public QWSubscription _this()
    {
        return QWSubscriptionHelper.narrow(_this_object());
    }

    public QWSubscription _this(org.omg.CORBA.ORB orb)
    {
        return QWSubscriptionHelper.narrow(_this_object(orb));
    }

    private static String [] _ids_list =
    {
        "IDL:com/isti/quakewatch/corbaclient/qw_client_rec/QWSubscription:1.0"
    };

    public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte [] objectId)
    {
        return _ids_list;
    }

    public final org.omg.CORBA.portable.OutputStream _invoke(final String opName,
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler)
    {

        if (opName.equals("connectPushSubscriber")) {
                return _invoke_connectPushSubscriber(_is, handler);
        } else if (opName.equals("connectPushSubscriberTo")) {
                return _invoke_connectPushSubscriberTo(_is, handler);
        } else {
            throw new org.omg.CORBA.BAD_OPERATION(opName);
        }
    }

    // helper methods
    private org.omg.CORBA.portable.OutputStream _invoke_connectPushSubscriberTo(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        String arg0_in = _is.read_string();
        com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlk[] arg1_in = com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlkSeqHelper.read(_is);
        com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesHolder arg2_out = new com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesHolder();

        String _arg_result = connectPushSubscriberTo(arg0_in, arg1_in, arg2_out);

        _output = handler.createReply();
        _output.write_string(_arg_result);

        com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesHelper.write(_output,arg2_out.value);
        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_connectPushSubscriber(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        String arg0_in = _is.read_string();
        com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesHolder arg1_out = new com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesHolder();

        String _arg_result = connectPushSubscriber(arg0_in, arg1_out);

        _output = handler.createReply();
        _output.write_string(_arg_result);

        com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesHelper.write(_output,arg1_out.value);
        return _output;
    }

}
