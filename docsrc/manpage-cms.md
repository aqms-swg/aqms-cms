# cms {#man-cms}  
  
CISN messaging system  
  

## PAGE LAST UPDATED ON  

2009-01-09  
  

## NAME
  
cms  
  

## VERSION and STATUS
  
status : ACTIVE  
  
  
## PURPOSE  
  
CMS is a messaging system that handles a publication and subscription of messages and assures delivery. It is written in C++ and Java and uses the [​QuakeWatch software](https://www.isti.com/products-offerings/quakewatch) for the management of the messages. For communications, it uses the CORBA messaging middleware protocol for object transmission over TCP/IP.  
  
CMS is used by many of the RT modules to communicate with one another using the concept of named channels. A publisher (either a C++ or Java program) can publish a message into a channel and any subscribers will receive the message. The detailed description of the CMS system can be found in the documentation provided by Caltech and UCBerkeley for its specification. CMS replaces a commercial package known as Talarian Smart Sockets.  
  
CMS is free and available for non-commercial academic and government use.  
  
  
## HOW TO RUN
  
The CMS middleware module is the QWServer program and is found in the /app/rtem/cms directory. There is a simple shell script to start and stop the module.     

``QWServer_init start``  
  
Configuration of CMS is needed for both the C++ modules and for the QWServer middleware system. These configuration files are described in the next section.
  
  
## CONFIGURATION FILE  
    
The CMS configuration is described in detail in the ​[QWServer documentation](https://assets.isti.com/QWIDS/current_dist/), but here is an example configuration file for the server used for a stock RT system found in /app/rtem/cms/QWServer_CMS/conf/QWServerConfig.xml:   
  
```
<QWServerConfig>
  <Settings>
      # ID name string for this QuakeWatch server:
    serverIdName = "CMS QW Server"
      # Name of console output redirect file:
    consoleRedirectFileName = "log/QWServerConsole.txt"
      # Name of the log file:
    serverLogFileName = "log/QWServer.log"
      # Level of log messages sent to the log file:
    serverLogFileLevel = "Debug4"
      # Level of log messages sent to the console:
    serverConsoleLevel = "Info"
      # Maximum age for log files (days, 0=infinite):
    logFilesMaxAgeInDays = 30
      # Port number spec for this server:
    serverPortNum = 39978
      # Port # spec for notification service (0 = not used):
    notifSvcPortNum = 0
      # false to disable QWServices for client connections:
    qwServicesAvailFlag = false
      # alternate server IDs list:
    altServerIdsList = ""
      # true = use numberic IPs instead of host names:
#     publishNumericIPFlag = false
      # enable sending of structured event messages:
    useStructuredEventsFlag = true
      # name of optional log file for event msgs sent to clients:
    messageLogFileName = "log/QWMessages.log"
      # min number of ms between "QWServerThreads.log" entries (0=none):
    threadLogIntervalMSecs = 0
      # min number of ms between memory-info log entries (0=none):
    memoryLogIntervalMSecs = 1000
      # true to allow duplicate messages to be processed:
    allowDuplicatesFlag = true
      # message cache age limit in days (0.041667 = 1 hr):
    maxCacheAgeInDays = 0.041667
      # maximum message size (0 = no limit):
    maximumMessageSize = 200000
  </Settings>
  <QWFeederMod
     Name = "QWCorba"
     Class = "com.isti.quakewatch.server.QWCorbaFeeder"
     LogFileName = "log/QWCorbaFeeder.log"
     LogFileLevel = "Debug4"
     ConsoleLevel = "Info">
  </QWFeederMod>
  <QWOutputterMod
     Name = "QWCorbaOutputter"
     Class = "com.isti.quakewatch.server.outputter.QWCorbaOutputter"
     LogFileName = "log/QWCorbaOutputter.log"
     LogFileLevel = "Debug"
     ConsoleLevel = "Info">
     <QWOutputterSettings>
         # port number for this outputter to listen on:
       outputPortNumber = 36688
         # maximum age for delivered messages (hours, 0.0 = no limit):
       maxMessageAgeHours = 1.0
         # maximum number of queued messages (0 = no limit):
       maxMsgQueueCount = 300
         # seconds before retry after delivery failure to subscriber:
       retryAfterFailDelaySecs = 20
         # max retry time before disconnect (minutes, 0.0 = no limit):
       maximumRetryMinutes = 10.0
         # unpack message from 'QWmessage' element:
       unpackQWMessageFlag = true
         # unpack message from 'DataMessage' element:
       unpackDataMessageFlag = true
         # unpack message from 'EQMessage' element:
       unpackEQMessageFlag = false
     </QWOutputterSettings>
  </QWOutputterMod>
</QWServerConfig>
```   
  
The CMS config file shared by all C++ modules publishing and subscribing, known as *cms.cfg* is shown below:  
  
```
Project             RT4
Host                localhost:38800
CorbaVersion        1.2
MaxQFileSize        200
MaxConnRetry        100
QueueFileHomeDir    /app/rtem/cms/qfiles
SubscriberHost      localhost
SubscriberPort      36688
```  
  
The entries in the above file are as follows:  
  
*  **Project** - the name of the project  
*  **Host** - the hostname:port of the QWServer module (where messages are published too)    
*  **CorbaVersion** - the version of CORBA that the publishers and subscribers are using...for this edition, use 1.2
*  **MaxQFileSize** - the size in megabytes of a Queue file, used by the C++ publishers and subscribers to temporarily store messages before sending to the CMS system.  
*  **MaxConnRetry** - the number of times to retry sending or polling for a message    
*  **QueueFileHomeDir** - where the queue files are stored. Queue files are named by the syntax Project_Hostname_ProgramName_type.q where type is RECV or SEND depending on the queue task.  
*  **SubscriberHost** - the host where the subscriber host resides (this is now built into the QWServer as a plugin module, previous versions had the module separate). This should always be the same as the Host address  
*  **SubscriberPort** - the port for the subscriber module (QWCorbaSubscriber plug-in client of QWServer).  
*  **SleepBetweenRetries** - the number of seconds between attempting to retry a connection to send or receive a message.   
  

Furthermore, there is a common config file used for common channels used by all RT modules and in the RT configuration layout it is placed in the cms/conf/common_signals.cfg. Here is an example file:      
  
```
$ more common_signals.cfg 
SystemSubject   /system/control
StatusSubject   /process/reports/status 
DebugSubject    /process/reports/debug
InfoSubject     /process/reports/info
ErrorSubject    /process/reports/error
```    
  

## ENVIRONMENT

    

## DEPENDENCIES  
  
The CMS system should be up and running and pointed to by this module. This module only runs within a working Earthworm environment 7.X or greater version. 
  
  
## MAINTENANCE  
  
Refer to the log files in the cms/QWServer_CMS/log directory.   
  

## BUG REPORTING  
  
https://gitlab.com/aqms-swg/aqms-cms/-/issues  
  

## MORE INFORMATION
  
*  Look at the QWServer documentation and the CMS specification. The IDL for the CORBA publishing and subscribing is also available at https://gitlab.com/aqms-swg/aqms-cms/-/tree/master/idl  
*  [CMS user guide](https://gitlab.com/aqms-swg/aqms-cms/-/blob/master/docsrc/cms-userguide.pdf)
