@echo off
if not "%OPTIT_HOME%"=="" goto opitset
set OPTIT_HOME=..\optimizeit
:opitset
if not "%optitOldPathSav%"=="" goto opthset
set optitOldPathSav=%PATH%
:opthset
set PATH=%OPTIT_HOME%\lib;%optitOldPathSav%
echo on
java -Xdebug -Xrunjdwp:transport=dt_socket,address=28077,server=y,suspend=n -Xrunpri:filter=%OPTIT_HOME%\filters\DefaultAllOn.oif -Xbootclasspath/a:%OPTIT_HOME%\lib\oibcp.jar -Xms16m -Xmx256m -cp %OPTIT_HOME%\lib\optit.jar;QWServer.jar intuitive.audit.Audit -port 29077 com.isti.quakewatch.server.QWServer %*