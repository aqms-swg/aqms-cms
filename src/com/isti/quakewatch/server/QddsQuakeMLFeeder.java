//QddsQuakeMLFeeder.java:  QuakeWatch feeder plugin module that receives
//                         QDDS messages and passes them into the
//                         QuakeWatch server.  The generated XML conforms
//                         to the QuakeML format.
//
//  3/15/2010 -- [ET]  Initial version.
//

package com.isti.quakewatch.server;

/**
 * Class QddsQuakeMLFeeder is a QuakeWatch feeder plugin module that receives
 * QDDS messages and passes them into the QuakeWatch server.  The generated
 * XML conforms to the QuakeML format.
 */
public class QddsQuakeMLFeeder extends QddsFeeder
{
  public static final String MODULE_NAME = "QddsQuakeMLFeeder";
  public static final String VERSION = "1.0";    //name and version strings
  public static final String FEEDER_NAME = "QddsQuakeML";  //feeder name

  /**
   * Constructs a QDDS-ANSS-EQ feeder module.
   */
  public QddsQuakeMLFeeder()
  {
    super(MODULE_NAME, FEEDER_NAME, VERSION, new CubeQuakeMLParser());
  }
}
