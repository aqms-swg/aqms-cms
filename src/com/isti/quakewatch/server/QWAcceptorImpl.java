//QWAcceptorImpl.java:  Implementation for the QuakeWatch acceptor class,
//                      which accepts new client connections and returns
//                      "IOR:" strings for 'GUIServices' CORBA objects.
//
//  5/22/2003 -- [ET]  Initial version.
//  6/25/2003 -- [ET]  Modified 'newConnection()' to detect when the ORB
//                     manager is not initialized.
//  7/30/2004 -- [ET]  Modified to reference 'QWAcceptor.ERROR_MSG_PREFIX'
//                     instead of 'QWServicesStrings.ERROR_MSG_PREFIX'.
//   9/8/2004 -- [ET]  Added clients manager and support for different
//                     'QWServices' object for obsolete-version clients.
//  9/30/2004 -- [ET]  Added validation of client-login information.
// 10/13/2004 -- [ET]  Modified to support 'rejectUnknownDistFlag' config
//                     item; modified to return error-message string if
//                     client "comm-version" is not new enough to support
//                     QWServices connection-status methods.
// 10/15/2004 -- [ET]  Modified to detect validate-login errors and return
//                     an error-indicator to the client.
// 10/28/2004 -- [ET]  Added output to clients-info log file.
// 11/12/2004 -- [ET]  Added 'getAcceptorIDStr()' method.
// 11/18/2004 -- [ET]  Added support for server redirect.
//

package com.isti.quakewatch.server;

import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.openorbutil.OrbManager;
import com.isti.quakewatch.common.qw_services.QWAcceptor;
import com.isti.quakewatch.common.qw_services.QWAcceptorPOA;
import com.isti.quakewatch.common.qw_services.QWServices;

/**
 * Class QWAcceptorImpl defines the implementation for the QuakeWatch
 * acceptor class, which accepts new client connections and returns
 * "IOR:" strings for 'GUIServices' CORBA objects.
 */
public class QWAcceptorImpl extends QWAcceptorPOA
{
  private final QWServer qwServerObj;
  private final LogFile logObj;
  private final QWClientsManager clientsManagerObj;
         //array of QWServices handles, one slot for each 'CS_' value:
  private final QWServicesImpl [] qwServicesImplArr =
                               new QWServicesImpl[QWServices.NUM_CS_VALUES];
         //"total" variable used with 'redirectPercentValue':
  private int redirectPercentageTotal = 49;

  /**
   * Constructs an acceptor object.
   * @param qwServerObj the 'QWServer' object to use.
   */
  public QWAcceptorImpl(QWServer qwServerObj)
  {
    this.qwServerObj = qwServerObj;
    logObj = qwServerObj.getLogFileObj();
    clientsManagerObj = qwServerObj.getClientsManagerObj();
  }

  /**
   * Establishes a new client connection for QuakeWatch services.
   * @param usernameStr user name string for client.
   * @param passwordStr password string for client.
   * @param clientInfoStr client-connection-information
   * properties string for client.
   * @return An "IOR:" string for the 'QWServices' CORBA object to be
   * used by the calling client, or an error message (preceded by
   * "Error: ").
   */
  public String newConnection(String usernameStr,
                                    String passwordStr, String clientInfoStr)
  {
    logObj.debug3("QWAcceptor 'newConnection()' called, user=\"" +
                              usernameStr + "\", props:  " + clientInfoStr);
    try
    {                                  //get ORB object from QWServer:
      final OrbManager orbManagerObj;
      if((orbManagerObj=qwServerObj.getOrbManagerObj()) == null)
        throw new RuntimeException("ORB manager not initialized");
      final org.omg.CORBA.ORB orbObj = orbManagerObj.orbObj;
         //flag set true if client login is rejected and client version
         // is too old to support QWServices connection-status methods:
      boolean preConnStatClientFlag = false;
      final String infoBlkStr;
      final int connStatusVal;
      if(clientsManagerObj != null)
      {  //clients-manager object OK; do initial client check-in
        final QWClientsManager.ClientInfoBlk infoBlkObj;
        if((infoBlkObj=clientsManagerObj.performClientLogin(
                                        usernameStr,clientInfoStr)) != null)
        {     //info-block object for client created OK
          infoBlkStr = infoBlkObj.toString();    //save info str for client
          final boolean badDistFlag;
          if(!infoBlkObj.isDistribNameValid())
          {   //no match found for client distribution name
            if(clientsManagerObj.getVerInfoDataValidFlag())
            {      //client-versions-information data is available
                        //if cfgProp set then indicate bad client-dist name:
              badDistFlag = qwServerObj.getRejectUnknownDistFlag();
              logObj.debug2("QWAcceptorImpl.newConnection():  " +
                                "Client distribution name not matched (\"" +
                                         infoBlkObj.distribNameStr + "\")");
            }
            else
            {      //client-versions-information data not available
              badDistFlag = false;     //indicate client-dist name OK
              logObj.debug3("QWAcceptorImpl.newConnection():  " +
                           "No client-versions-information data available");
            }
          }
          else     //match found for client distribution name
            badDistFlag = false;       //indicate client-dist name OK
          if(!infoBlkObj.isVersionTooOld())
          {   //client version is not too old
            if(!badDistFlag)
            { //client distribution name is OK
              if(checkServerRedirect())
              {    //server-redirect indicated
                        //setup connection-status value for QWServices obj:
                connStatusVal = QWServices.CS_SERVER_REDIRECT;
              }
              else      //not redirecting to another server
              {    //if "authorization" versions of IP and host name are
                   // available then use them; else use "regular" values:
                String ipStr,nameStr;
                if((ipStr=infoBlkObj.clientAuthIPStr) == null ||
                                                 ipStr.trim().length() <= 0)
                { //"authorization" version of IP addr does not contain data
                  ipStr = infoBlkObj.clientIPStr;     //use "regular" values
                  nameStr = infoBlkObj.clientHostStr;
                }
                else   //"authorization" version of IP address contains data
                  nameStr = infoBlkObj.clientAuthHostStr;  //use "auth" name
                int cSVal;
                try     //check if client-login information passes
                {       // validation; set status value accordingly:
                  cSVal = qwServerObj.validateLoginInformation(
                                    usernameStr,passwordStr,nameStr,ipStr) ?
                     QWServices.CS_CONNECT_OK : QWServices.CS_INVALID_LOGIN;
                }
                catch(ServerLoginException ex)
                {  //some kind of error; setup error-status value
                  cSVal = QWServices.CS_CONN_ERROR;
                  logObj.debug("QWAcceptorImpl.newConnection():  Error " +
                              "calling validateLoginInformation():  " + ex);
                }
                connStatusVal = cSVal;
              }
            }
            else   //client dist name not known
            {           //indicate login-rejected status:
              connStatusVal = QWServices.CS_BAD_DISTNAME;
              logObj.debug("QWAcceptorImpl.newConnection():  " +
                        "Client login rejected because distribution name " +
                     "not matched (\"" + infoBlkObj.distribNameStr + "\")");
            }
          }
          else     //client version is too old; set status value
            connStatusVal = QWServices.CS_VERSION_OBSOLETE;
        }
        else
        {     //error creating info-block object for client
          infoBlkStr = clientInfoStr;                 //use given info str
          connStatusVal = QWServices.CS_CONN_ERROR;   //indicate error
        }
        if(connStatusVal != QWServices.CS_CONNECT_OK &&
                                        infoBlkObj.commVersionStr != null &&
                            infoBlkObj.commVersionStr.trim().length() > 0 &&
                                              UtilFns.compareVersionStrings(
                                       infoBlkObj.commVersionStr,"1.1") < 0)
        {     //login rejected and comm-version string not >= "1.1"
          preConnStatClientFlag = true;     //ind conn-status not supported
        }
      }
      else
      {       //clients-manager object not available
        infoBlkStr = clientInfoStr;              //use given info str
                   //setup connection-status value for QWServices object:
        connStatusVal = QWServices.CS_CONNECT_OK;
        logObj.debug2("QWAcceptor:  Clients manager not available; " +
                                                     "allowing connection");
      }
      if(connStatusVal >= qwServicesImplArr.length)
      {  //connection-status value is too large
        throw new RuntimeException("Invalid connection-status value (" +
                                                       connStatusVal + ")");
      }
      final QWServicesImpl qwServicesImplObj;
      if(qwServicesImplArr[connStatusVal] == null)
      {  //services object not yet created for connection-status value
              //create services object for connection-status value:
        qwServicesImplObj = qwServicesImplArr[connStatusVal] =
                              new QWServicesImpl(qwServerObj,connStatusVal);
      }
      else    //get existing services object for connection-status value
        qwServicesImplObj = qwServicesImplArr[connStatusVal];
      final String infoPrefixStr;
      if(connStatusVal == QWServices.CS_CONNECT_OK)
      {       //client connection successful
        infoPrefixStr = "New connection accepted, ";
      }
      else    //client connection not successful
      {            //invalidate client entry:
        if(clientsManagerObj != null)
          clientsManagerObj.invalidateClient(clientInfoStr);
                   //setup special prefix string if server redirect:
        infoPrefixStr = (connStatusVal == QWServices.CS_SERVER_REDIRECT) ?
                           "New connection redirected to another server, " :
                                              ("New connection rejected (" +
                         qwServicesImplObj.getShortConnStatusStr() + "), ");
      }
                                  //output client info to main log file:
      logObj.debug(infoPrefixStr + "user=\"" + usernameStr +
                                            "\", props:  " + clientInfoStr);
                                  //output to clients-info log file:
      qwServerObj.writeToClientsInfoLog(infoPrefixStr + infoBlkStr);

         //if flag set (indicating login rejected & client does not support
         // QWServices connection-status methods) then return error message;
         // else convert services object to "IOR:" string and return it:
      return preConnStatClientFlag ? (QWAcceptor.ERROR_MSG_PREFIX +
                               qwServicesImplObj.getConnectionStatusMsg()) :
                   orbObj.object_to_string(qwServicesImplObj._this(orbObj));
    }
    catch(Exception ex)
    {         //some kind of exception error; log warning message
      logObj.warning("Error setting up new connection (user=\"" +
                                               usernameStr + "\"):  " + ex);
      logObj.debug("  props:  " + clientInfoStr);
      logObj.warning(UtilFns.getStackTraceString(ex));
      return QWAcceptor.ERROR_MSG_PREFIX + "Unable to setup connection";
    }
  }

  /**
   * Returns an identifier string that uniquely identifies this acceptor
   * object.  This identifier is used by the QWRelayFeeder to determine
   * if the connected server is its own QWServer.
   * @return An identifier string that uniquely identifies this acceptor
   * object.
   */
  public String getAcceptorIDStr()
  {
    return qwServerObj.getQWAcceptorImplIDString();
  }

  /**
   * Tests if the current client connection should be server-redirected.
   * @return true if the current client connection should be
   * server-redirected; false if not.
   */
  private boolean checkServerRedirect()
  {
    final int connsCount;
    if(!qwServerObj.isRedirServersListEmpty() &&
                ((connsCount=qwServerObj.getRedirectConnectsCount()) <= 0 ||
                    qwServerObj.getConnectedClientsCount()-1 >= connsCount))
    {    //redirect servers specified and enough current connections
      final int percentVal;            //check redirect percent value:
      if((percentVal=qwServerObj.getRedirectPercentValue()) >= 100)
        return true;         //if 100% then redirect
              //for each client connection, add redirect percent value
              // to total; when 100+ reached then do redirect:
      redirectPercentageTotal += percentVal;
      logObj.debug3("QWAcceptorImpl:  Added redirect-percent value (" +
                  percentVal + "), new total = " + redirectPercentageTotal);
      if(redirectPercentageTotal >= 100)
      {  //reached total of 100 or over
        redirectPercentageTotal -= 100;          //do mod 100
        return true;         //do redirect
      }
    }
    return false;            //don't do redirect
  }
}
