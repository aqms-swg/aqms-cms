//QWCachedArchiveMgr.java:  Manages the QWServer's cached archive.
//
//   9/9/2003 -- [ET]  Initial version.
// 10/24/2003 -- [ET]  Changed reference from 'ArchivableMsgObjEntry' to
//                     'BasicArchivableMsgObjEntry'.
//  11/5/2003 -- [ET]  Modified to use 'QWMsgRecord' class from 'QWCommon'
//                     project; added 'getLastEnteredMsgObj()' method;
//                     moved 'QWServerMsgRecord' into separate file.
// 11/21/2003 -- [ET]  Added 'waitForAddMessageQueueEmpty()' method.
//  1/14/2004 -- [ET]  Added loading of 'DomainTypeInfoTable' after
//                     preload of cache.
//  7/14/2004 -- [ET]  Added 'allowDuplicatesFlag' parameter to constructor.
//  7/22/2004 -- [ET]  Added return value to 'removeOldObjects()' method;
//                     added 'isCacheEmpty()' and 'getCacheSize()' methods;
//                     modified 'checkAndProcCacheMsgs()' to expect
//                     string form of "QWmessage" from 'QWServerMsgRecord'
//                     objects; modified exception catch in 'addMessage()'
//                     to trap all "Throwable" exceptions (so as to also
//                     catch 'OutOfMemoryError'); added parameters
//                     'cacheMaxObjectCount' and 'cacheMaxTotalDataSize'
//                     to constructor.
//  12/9/2004 -- [ET]  Added 'getCacheInformation()' method.
//  1/10/2005 -- [ET]  Modified to have multiple return values for
//                     'addMessage()' method.
//  1/21/2005 -- [ET]  Added 'fetchLastCachedFdrSourceMsgNum()' method.
//  1/26/2005 -- [ET]  Modified to make 'addMessage()' method use a
//                     'QWServerMsgRecord' parameter.
// 12/27/2005 -- [ET]  Added 'checkDupFdrSourceFlag' to constructor and
//                     modified to always use object keys with cache.
//  9/15/2006 -- [ET]  Added 'msgKeyElement1Name' and 'msgKeyElement2Name'
//                     parameters to constructor.
// 10/20/2006 -- [ET]  Added 'getAllCacheMessages()' method.
//  7/14/2010 -- [ET]  Modified 'checkDupFdrSourceFlag' parameter
//                     description.
//

package com.isti.quakewatch.server;

import java.util.Vector;
import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.util.FlatDailyArchiveManager;
import com.isti.util.IstiMessageCachedArchive;
import com.isti.util.IstiTimeObjectCache;
import com.isti.quakewatch.util.QWUtils;
import com.isti.quakewatch.util.DomainTypeInfoTable;
import com.isti.quakewatch.common.MsgTag;

/**
 * Class QWCachedArchiveMgr manages the QWServer's cached archive.
 */
public class QWCachedArchiveMgr
{
    /* Return value for 'addMessage()' indicating success. */
  public static int ADDMSG_SUCCESS = 0;
    /* Return value for 'addMessage()' indicating message duplicate. */
  public static int ADDMSG_DUPLICATE = 1;
    /* Return value for 'addMessage()' indicating exception error. */
  public static int ADDMSG_ERROR = 2;

  private final IstiMessageCachedArchive msgCachedArchiveObj;
  private final LogFile logObj;
  private final DomainTypeInfoTable domainTypeInfoTableObj;

  /**
   * Creates a manager for the QWServer's cached archive.
   * @param archiveDirNameStr name of directory in which to place
   * archive files, or null to use the current working directory.
   * @param baseFileNameStr the "base" file name to use to generate
   * archive file names.  A date code will be inserted before the
   * extension part of the file name.
   * @param logObj log file object to use.
   * @param toleranceMS number of milliseconds for time tolerance
   * that is used with time checks.
   * @param cacheRemoveAgeSecs the number of seconds to keep objects
   * in the cache.
   * @param cacheMaxObjectCount the maximum-object count for the cache,
   * or 0 for no limit.
   * @param cacheMaxTotalDataSize the maximum-total-data size for the
   * cache, or 0 for no limit.
   * @param archiveRemoveAgeSecs the number of seconds to keep objects
   * in the archive, or 0 for "infinity".
   * @param maxRequestAgeSecs maximum age (in seconds) for messages
   * returned via the 'getNewerMessages()' method, or 0 for no limit.
   * @param allowDuplicatesFlag true to allow duplicate messages; false
   * to reject duplicate messages.
   * @param checkDupFdrSourceFlag true to check for duplicate feeder-data-
   * source message numbers.
   * @param msgKeyElement1Name the name of the upper child-element under
   * the 'DataMessage' element to be used when generating a check-duplicate
   * key strings, or null or an empty string for none.
   * @param msgKeyElement2Name the name of the lower child-element under
   * the 'DataMessage' element to be used when generating a check-duplicate
   * key strings, or null or an empty string for none.
   * @param domainTypeInfoTableObj the table of message-number and
   * time-generated values for domain/type names to use, or null for
   * none.
   * @throws QWCreateMgrException if an error occurs.
   */
  public QWCachedArchiveMgr(String archiveDirNameStr,
                     String baseFileNameStr,LogFile logObj,long toleranceMS,
                             int cacheRemoveAgeSecs,int cacheMaxObjectCount,
                        long cacheMaxTotalDataSize,int archiveRemoveAgeSecs,
                          int maxRequestAgeSecs,boolean allowDuplicatesFlag,
                                              boolean checkDupFdrSourceFlag,
                        String msgKeyElement1Name,String msgKeyElement2Name,
                                 DomainTypeInfoTable domainTypeInfoTableObj)
                                                 throws QWCreateMgrException
  {
    if(baseFileNameStr == null || logObj == null)
    {    //null parameter(s) given
      throw new QWCreateMgrException(
                                  "Null parameter(s) given to constructor");
    }
    this.logObj = logObj;
    this.domainTypeInfoTableObj = domainTypeInfoTableObj;
              //setup flags for message records:
    QWServerMsgRecord.setAllowDuplicatesFlag(allowDuplicatesFlag);
    QWServerMsgRecord.setCheckDupFdrSourceFlag(checkDupFdrSourceFlag);
              //setup child-element names for key-string generation:
    QWServerMsgRecord.setMsgKeyElement1Name(msgKeyElement1Name);
    QWServerMsgRecord.setMsgKeyElement2Name(msgKeyElement2Name);
    final FlatDailyArchiveManager archiveMgrObj;
    try
    {         //create archive manager object:
      archiveMgrObj = new FlatDailyArchiveManager(QWServerMsgRecord.class,
                                         archiveDirNameStr,baseFileNameStr);
    }
    catch(Exception ex)
    {         //error creating archive manager object; throw exception
      throw new QWCreateMgrException(
                                "Unable to create archive manager:  " + ex);
    }
    try
    {    //create cached-archive obj ('useLookupKeyFlag' always 'true'):
      msgCachedArchiveObj = new IstiMessageCachedArchive(archiveMgrObj,
                  logObj,toleranceMS,cacheRemoveAgeSecs,cacheMaxObjectCount,
               cacheMaxTotalDataSize,archiveRemoveAgeSecs,maxRequestAgeSecs,
                                                                      true);
    }
    catch(Exception ex)
    {         //error creating cached-archive object; throw exception
      throw new QWCreateMgrException(
                          "Unable to create cached-archive object:  " + ex);
    }
  }

  /**
   * Performs the initial loading of the cache from existing items in
   * the archive.
   */
  public void preloadCacheFromArchive()
  {
    msgCachedArchiveObj.preloadCacheFromArchive();
              //check messages, load 'domainTypeInfoTableObj':
    checkAndProcCacheMsgs();
  }

  /**
   * Adds the given message record to the cache and to the archive.
   * If a message with a matching data-event key is already in the cache
   * then the given message is not added.  The message object is queued
   * and then added via a worker thread.
   * @param recObj the message-record object to add.
   * @return The value 'ADDMSG_SUCCESS' if the message record was added
   * successfully; 'ADDMSG_DUPLICATE' if not because a message with a
   * matching data-event key was already in the cache; or 'ADDMSG_ERROR'
   * if an error occurred.
   */
  public int addMessage(QWServerMsgRecord recObj)
  {
    try
    {         //check if cache already contains matching message record:
      if(!msgCachedArchiveObj.cacheContains(recObj))
      {       //no matching record in cache
                             //add message to cache and archive:
        msgCachedArchiveObj.addArchivableMsgObj(recObj);
        return ADDMSG_SUCCESS;    //indicate message record added OK
      }
      return ADDMSG_DUPLICATE;    //indicate rejected because duplicate
    }
    catch(Throwable ex)
    {         //error creating or adding; log warning message
      logObj.warning("Error adding message to cached archive:  " + ex);
      if(recObj != null)
      {
        logObj.debug("  msgNum=" + recObj.msgNumber + ", timeGenerated=" +
                                                      recObj.timeGenerated);
      }
      logObj.warning(UtilFns.getStackTraceString(ex));
      return ADDMSG_ERROR;        //indicate exception error occurred
    }
  }

  /**
   * Waits for the cached-archive's add-message queue to be empty, up to
   * 5 seconds.
   * @return true if the add-message queue is empty, false if messages
   * are waiting to be added and the max-wait-time was reached
   */
  public boolean waitForAddMessageQueueEmpty()
  {
    return msgCachedArchiveObj.waitForAddMessageQueueEmpty(5000);
  }

  /**
   * Returns the newest message object entered into the cache or archive.
   * @return The newest 'QWServerMsgRecord' object entered into the
   * cache or archive, or null if none have been entered.
   */
  public QWServerMsgRecord getLastEnteredMsgObj()
  {
    final IstiMessageCachedArchive.ArchivableMsgObjEntry entryObj;
    return ((entryObj=msgCachedArchiveObj.getLastMessageObj()) instanceof
                    QWServerMsgRecord) ? (QWServerMsgRecord)entryObj : null;
  }

  /**
   * Returns the message number for the newest message entered into
   * the cache or archive.
   * @return The message number for the newest message entered into
   * the cache or archive, or 0 if none have been entered.
   */
  public long getLastEnteredMsgNum()
  {
    return msgCachedArchiveObj.getLastMsgNumber();
  }

  /**
   * Returns a list of messages newer or equal to the specified time value
   * or later than the specified message number.  If a non-zero message
   * number is given and the given time value matches then messages with
   * message numbers greater than the given message number are returned;
   * otherwise messages newer or equal to the specified time value are
   * returned (within a tolerance value).  The messages may come from
   * the cache or the archive.
   * @param time time in milliseconds.
   * @param msgNum message number.
   * @param maxCount the maximum number of messages to be returned,
   * or 0 for no limit.
   * @return A new 'IstiTimeObjectCache.VectorWithCount' containing
   * 'QWServerMsgRecord' objects that also holds the requested
   * number of items in its 'count' field.
   */
  public IstiTimeObjectCache.VectorWithCount getNewerMessages(
                                         long time,long msgNum,int maxCount)
  {
    return msgCachedArchiveObj.getNewerMessages(time,msgNum,maxCount);
  }

  /**
   * Removes old objects from the cache and archive.  This method should
   * be called on a periodic basis.
   * @return true if any objects were removed from the cache; false if no
   * objects were removed from the cache.
   */
  public boolean removeOldObjects()
  {
    return msgCachedArchiveObj.removeOldObjects();
  }

  /**
   * Returns the status of whether or not the cache is empty.
   * @return true if the cache is empty, false if not.
   */
  public boolean isCacheEmpty()
  {
    return msgCachedArchiveObj.isCacheEmpty();
  }

  /**
   * Returns the number of entries in the cache.
   * @return The number of entries in the cache.
   */
  public int getCacheSize()
  {
    return msgCachedArchiveObj.getCacheSize();
  }

  /**
   * Returns a vector containing all of the message objects in the cache.
   * Each message object will be of type 'QWServerMsgRecord'.
   * @return A new vector containing all of the message objects in the
   * cache.
   */
  public Vector getAllCacheMessages()
  {
    return msgCachedArchiveObj.getMessageCacheObj().getAllObjects();
  }

  /**
   * Closes the archive and cache.
   */
  public void close()
  {
    msgCachedArchiveObj.close();
  }

  /**
   * Checks the validity of messages in the cache and processes any
   * message-number values for domain/type names in the messages
   * by entering the values into the 'domainTypeInfoTableObj' table.
   */
  private void checkAndProcCacheMsgs()
  {
    logObj.debug("Running 'checkAndProcCacheMsgs()'");
    final Vector vec =       //get Vector containing all messages in cache
             msgCachedArchiveObj.getMessageCacheObj().getNewerMessages(0,0);
    int vecSize = 0;
    try
    {
      vecSize = vec.size();
      logObj.debug("  Number of msgs in cache = " + vecSize);
      if(vecSize > 0)
      {

        QWServerMsgRecord recObj = (QWServerMsgRecord)(vec.elementAt(0));
        logObj.debug("  First item:  " +
                                   com.isti.util.UtilFns.timeMillisToString(
                                   recObj.getArchiveDate().getTime(),true) +
                                                 ", " + recObj.getMsgNum());
        recObj = (QWServerMsgRecord)(vec.elementAt(vecSize-1));
        logObj.debug("   Last item:  " +
                                   com.isti.util.UtilFns.timeMillisToString(
                                   recObj.getArchiveDate().getTime(),true) +
                                                 ", " + recObj.getMsgNum());
      }
    }
    catch(Exception ex)
    {
      logObj.warning("checkAndProcCacheMsgs() showVecData error:  " + ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
    try
    {
      final boolean domainTypeInfoTableFlag =    //set flag if table given
                                           (domainTypeInfoTableObj != null);
      String str,errStr;
      long val;
      Object obj;
      QWServerMsgRecord recObj;
      for(int i=0; i < vecSize; ++i)
      {  //for each message in cache
        errStr = "";
        if((obj=vec.elementAt(i)) instanceof QWServerMsgRecord)
        {
          recObj = (QWServerMsgRecord)obj;
          if(!((obj=recObj.getDataObj()) instanceof String))
            errStr += ", dataObj not of type String, dataObj = " + obj;
          if((str=recObj.getKeyStr()) == null)
            errStr += ", dataStr is null";
          else if(str.length() <= 0)
            errStr += ", dataStr is empty";
          if((val=recObj.getTimeGenerated()) <= 0)
            errStr += ", timeGenerated = " + val;
          if((val=recObj.getMsgNum()) <= 0 || errStr.length() > 0)
          {
            logObj.warning("  checkAndProcCacheMsgs() bad entry (" + i +
                                    "), msgNum = " + val + ", archDate = " +
                                   com.isti.util.UtilFns.timeMillisToString(
                          recObj.getArchiveDate().getTime(),true) + errStr);
          }
          if(domainTypeInfoTableFlag && recObj.msgEvtMsgNumStr != null &&
                                         ((recObj.msgEvtDomainStr != null &&
                                     recObj.msgEvtDomainStr.length() > 0) ||
                                            (recObj.msgEvtTypeStr != null &&
                                      recObj.msgEvtTypeStr.length() > 0)))
          {   //domain/type info table available and 'MsgEvtMsgNum' attrib
              // given and 'MsgEvtDomain' & 'MsgEvtType' contain data
            final Long msgNumObj;           //convert 'MsgEvtMsgNum' value:
            if((msgNumObj=QWUtils.parseStringLong(
                                           recObj.msgEvtMsgNumStr)) != null)
            {      //'MsgEvtMsgNum' value converted OK
                        //enter message-number and time-generated values
                        // for the event domain and type names:
              domainTypeInfoTableObj.setMessageNumberValue(
                                recObj.msgEvtDomainStr,recObj.msgEvtTypeStr,
                                                     msgNumObj.longValue());
            }
            else
            {      //error converting 'MsgEvtMsgNum' value; log warning
              logObj.warning("Bad '" + MsgTag.MSG_EVT_MSG_NUM +
                          "' attribute value (\"" + recObj.msgEvtMsgNumStr +
                                           "\" in cache entry (" + i + ")");
            }
          }
        }
        else
        {
          logObj.debug("  Cache entry (" + i +
                           ") not of type QWServerMsgRecord, obj = " + obj);
        }
      }
    }
    catch(Exception ex)
    {
      logObj.warning("checkAndProcCacheMsgs() error:  " + ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
    logObj.debug("Finished 'checkAndProcCacheMsgs()'");
  }

  /**
   * Finds the last message in the cache that contains a "FdrSourceHost"
   * value matching the given string and returns the "FdrSourceMsgNum"
   * value for that message.
   * @param fdrSrcHostStr the feeder-data-source host-name string to use.
   * @return The feeder-data-source message number for the matching message,
   * or 0 if no match was found.
   */
  public long fetchLastCachedFdrSourceMsgNum(String fdrSrcHostStr)
  {
    try
    {
      final Vector vec =     //get Vector containing all messages in cache
             msgCachedArchiveObj.getMessageCacheObj().getNewerMessages(0,0);
      int i;
      if((i=vec.size()) > 0)
      {
        QWServerMsgRecord recObj;
        do
        {
          recObj = (QWServerMsgRecord)(vec.elementAt(--i));
          if(recObj.fdrSourceHostStr != null &&
                              recObj.fdrSourceHostStr.equals(fdrSrcHostStr))
          {
            return recObj.fdrSourceMsgNumVal;
          }
        }
        while(i > 0);
      }
    }
    catch(Exception ex)
    {    //some kind of exception error; log it
      logObj.warning("fetchLastCachedFdrSourceMsgNum() error:  " + ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
    return 0;
  }

  /**
   * Returns information about the cache.
   * @return A new 'CacheInformation' object.
   */
  public IstiTimeObjectCache.CacheInformation getCacheInformation()
  {
    return msgCachedArchiveObj.getMessageCacheObj().getCacheInformation();
  }


  /**
   * QWCreateMgrException defines an exception thrown while constructing
   * a 'QWCachedArchiveMgr' object.
   */
  public static class QWCreateMgrException extends Exception
  {
    /**
     * Creates an exception thrown while constructing a 'QWCachedArchiveMgr'
     * object.
     * @param msgStr a descriptive message for the exception.
     */
    public QWCreateMgrException(String msgStr)
    {
      super(msgStr);
    }
  }
}
