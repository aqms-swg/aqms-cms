//ServerConfigManager.java:  Manages QuakeWatch server configuration items.
//
//  8/12/2004 -- [KF]  Initial version.
//  8/16/2004 -- [KF]  Added password server items.
//  8/18/2004 -- [ET]  Added 'clientVersionsInfoFile' item; added
//                     default value to 'mainEvtChLocFile' item; put
//                     in 'UtilFns.EMPTY_STRING' references.
//   9/1/2004 -- [KF]  Added 'privKeyFile' item.
//   9/1/2004 -- [ET]  Added 'clientTimeoutSecs' item.
//   9/2/2004 -- [KF]  Added 'coaFile', 'crlFile', and 'certFile' items.
// 10/13/2004 -- [ET]  Added 'rejectUnknownDistFlag' item.
// 10/29/2004 -- [ET]  Added 'clientsInfoLogIntervalSecs',
//                     'clientsInfoLogSwitchDays' and
//                     'clientsInfoLogMaxAgeDaysProp' items.
//  11/5/2004 -- [ET]  Added 'maximumMessageSize' item.
// 11/10/2004 -- [ET]  Added 'publishNumericIPFlag' item; increased default
//                     'clientsInfoLogIntervalSecs' value to 600 seconds.
// 11/18/2004 -- [ET]  Added 'redirectServersList', 'randomizeRedirListFlag',
//                     'redirectConnectsCount' and 'redirectPercentValue'
//                     items.
// 11/19/2004 -- [ET]  Renamed "passwordServer..." items to "pwdDatabase...".
// 11/24/2004 -- [ET]  Added 'qwServicesAvailFlag' item.
//  12/1/2004 -- [ET]  Added 'allowObsLoginClientsFlag' item.
// 12/10/2004 -- [ET]  Added 'recPollingItervalMs' item.
//  1/10/2005 -- [ET]  Added 'clientVInfoPollDelaySecs' item.
//  1/21/2005 -- [ET]  Added 'defaultFdrSourceHost' item.
//  1/28/2005 -- [ET]  Added 'statsLogIntervalSecs' item.
//   2/4/2005 -- [ET]  Added 'systemExitOnCloseFlag' item.
//  7/13/2005 -- [ET]  Changed 'rejectUnknownDistFlag' default to 'false'.
//  9/16/2005 -- [ET]  Changed default log-output levels from "Info" to
//                     "Debug" on 'serverLogFileLevel' and "Warning" to
//                     "Info" on 'serverConsoleLevel'.
// 12/27/2005 -- [ET]  Added 'checkDupFdrSourceFlag' item.
//  2/15/2006 -- [ET]  Added "stRpt..." items.
//  9/15/2006 -- [ET]  Added 'msgKeyElement1Name' and 'msgKeyElement2Name'
//                     items.
//  11/6/2006 -- [ET]  Added 'feederModulesCfgFile' item.
// 11/20/2006 -- [ET]  Added 'checkEQMsgIdentFlag' item.
// 12/11/2006 -- [ET]  Added 'memorySizeWarnMBytes', 'threadCountWarnMargin'
//                     'notifSvrStatusLogName' and 'notifSvrStatusWarnFlag'
//                     items.
//   3/3/2008 -- [ET]  Added 'stRptRemoteDir' item; renamed item from
//                     'stRptRemoteFile' to 'stRptRemoteFName'.
//  3/25/2008 -- [ET]  Changed default 'threadCountWarnMargin' value from
//                     20 to 25.
//   9/1/2010 -- [ET]  Added items 'consoleFilesSwitchIntvlDays' and
//                     'consoleFilesMaxAgeInDays'.
//  11/4/2010 -- [ET]  Added 'checkFdrStatusErrMins' item.
//

package com.isti.quakewatch.server;

import com.isti.util.UtilFns;
import com.isti.util.CfgProperties;
import com.isti.util.CfgPropItem;
import com.isti.util.LogFile;
import com.isti.quakewatch.common.MsgTag;
import com.isti.quakewatch.common.QWConstants;
import com.isti.quakewatch.util.QWConnProperties;

/**
 * Class ServerConfigManager manages QuakeWatch server configuration items.
 */
public class ServerConfigManager extends ServerConfigProcessor implements QWConstants
{
         /** Properties item for Settings. */
  public final CfgProperties settingsProps = new CfgProperties();

         /** ID name string for this QuakeWatch server. */
  public final CfgPropItem serverIdNameProp =
                           settingsProps.add("serverIdName","???",true,null,
                               "ID name string for this QuakeWatch server");

         /* Name of 'main' CORBA-event-channel. */
//  public final CfgPropItem mainMsgServiceNameProp =
//               settingsProps.add("mainMsgServiceName",UtilFns.EMPTY_STRING);

         /** Delay (in seconds) between "Alive" messages (0=none). */
  public final CfgPropItem aliveMsgDelaySecProp =
                  settingsProps.add("aliveMsgDelaySec",Integer.valueOf(10),null,
                                   "Delay between 'Alive' messages (secs)");

         /** Name of the console output redirect file. */
  public final CfgPropItem consoleRedirectFileNameProp =
      settingsProps.add("consoleRedirectFileName",UtilFns.EMPTY_STRING,null,
                                "Name of the console output redirect file");

         /** Name of log file. */
  public final CfgPropItem serverLogFileNameProp =
              settingsProps.add("serverLogFileName","log/QWServer.log",null,
                                                    "Name of the log file");

         /** Message level for log file output. */
  public final CfgPropItem serverLogFileLevelProp =
              settingsProps.add("serverLogFileLevel",LogFile.DEBUG_STR,null,
                                       "Message level for log file output");

         /** Message level for console output. */
  public final CfgPropItem serverConsoleLevelProp =
               settingsProps.add("serverConsoleLevel",LogFile.INFO_STR,null,
                                        "Message level for console output");

         /** Maximum age for log files (days, 0=infinite). */
  public final CfgPropItem logFilesMaxAgeInDaysProp =
              settingsProps.add("logFilesMaxAgeInDays",Integer.valueOf(30),null,
                                "Max age for log files (days, 0=infinite)");

         /** Switch interval for console files, in days. */
  public final CfgPropItem consoleFilesSwitchIntvlDays = settingsProps.add(
                         "consoleFilesSwitchIntvlDays",Integer.valueOf(90),null,
                                "Switch interval for console files (days)");

         /** Console files maximum age in days (0=infinite). */
  public final CfgPropItem consoleFilesMaxAgeInDays = settingsProps.add(
                          "consoleFilesMaxAgeInDays",Integer.valueOf(1096),null,
                                    "Maximum age for console files (days)");

         /** Optional locator file for main event channel. */
  public final CfgPropItem mainEvtChLocFileProp =
         settingsProps.add("mainEvtChLocFile","MainMessageService.loc",null,
                                     "Locator file for main event channel");

         /** Optional host address specification for this server. */
  public final CfgPropItem serverHostAddressProp =
            settingsProps.add("serverHostAddress",UtilFns.EMPTY_STRING,null,
                              "Optional host address spec for this server");

         /** Optional ORB/POA port number specification for this server. */
  public final CfgPropItem serverPortNumProp =
                  settingsProps.add("serverPortNum",Integer.valueOf(39977),null,
                               "Optional port number spec for this server");

         /** Flag to use numberic IPs instead of host names. */
  public final CfgPropItem publishNumericIPFlagProp =
                settingsProps.add("publishNumericIPFlag",Boolean.FALSE,null,
                                  "Use numberic IPs instead of host names");

         /** Flag set true to enable QWServices for client connections. */
  public final CfgPropItem qwServicesAvailFlagProp =
                  settingsProps.add("qwServicesAvailFlag",Boolean.TRUE,null,
                                           "Enable QWServices for clients");

         /** Flag set true to perform 'System.exit()' when program closes. */
  public final CfgPropItem systemExitOnCloseFlagProp =
                settingsProps.add("systemExitOnCloseFlag",Boolean.TRUE,null,
                                        "Perform 'System.exit()' on close");

         /** Optional host address specification for notification service. */
  public final CfgPropItem notifSvcHostAddressProp =
          settingsProps.add("notifSvcHostAddress",UtilFns.EMPTY_STRING,null,
                               "Optional address for notification service");

         /** Optional port number specification for notification service. */
  public final CfgPropItem notifSvcPortNumProp =
                settingsProps.add("notifSvcPortNum",Integer.valueOf(39988),null,
                                "Optional port # for notification service");

         /** Flag that enables sending of structured event messages. */
  public final CfgPropItem useStructuredEventsFlagProp =
             settingsProps.add("useStructuredEventsFlag",Boolean.FALSE,null,
                                "Enables sending of structured event msgs");

         /** Maximum # of seconds to wait for event channel (0=infinite). */
  public final CfgPropItem connWaitRetrySecsProp =
                 settingsProps.add("connWaitRetrySecs",Integer.valueOf(15),null,
                             "Maximum # of secs to wait for event channel");

         /** Name of optional log file of event msgs sent to clients. */
  public final CfgPropItem messageLogFileNameProp =
           settingsProps.add("messageLogFileName",UtilFns.EMPTY_STRING,null,
                                      "Optional logfile of event messages");

         /** Switch interval for msg log files (days, 0 = no swapping). */
  public final CfgPropItem messageLogSwitchDaysProp =
               settingsProps.add("messageLogSwitchDays",Integer.valueOf(7),null,
                                 "Switch interval for message logs (days)");

         /** Feeder status error checking interval, in minutes (0=none). */
  public final CfgPropItem checkFdrStatusErrMinsProp =
            settingsProps.add("checkFdrStatusErrMins",Double.valueOf(15.0),null,
                                  "Feeder status error interval (minutes)");

         /** Min # of ms between "QWServerThreads.log" entries (0=none). */
  public final CfgPropItem threadLogIntervalMSecsProp =
             settingsProps.add("threadLogIntervalMSecs",Integer.valueOf(0),null,
                              "Interval for thread-info logs (ms, 0=none)");

         /** Min # of ms between memory-info log entries (0=none). */
  public final CfgPropItem memoryLogIntervalMSecsProp =
             settingsProps.add("memoryLogIntervalMSecs",Integer.valueOf(0),null,
                              "Interval for memory-info logs (ms, 0=none)");

         /** Maximum age for memory-info log files (days, 0=infinite). */
  public final CfgPropItem memoryLogMaxAgeInDaysProp =
            settingsProps.add("memoryLogMaxAgeInDays",Integer.valueOf(180),null,
                                 "Maximum age for memory-info logs (days)");

         /** Switch interval for mem log files (days, 0 = no swapping). */
  public final CfgPropItem memoryLogSwitchDaysProp =
               settingsProps.add("memoryLogSwitchDays",Integer.valueOf(30),null,
                             "Switch interval for memory-info logs (days)");

         /** # of seconds between clients-info log entries (0 = no log). */
  public final CfgPropItem clientsInfoLogIntervalSecsProp =
            settingsProps.add("clientsInfoLogIntervalSecs",Integer.valueOf(600),
                               null,"Clients-info logging interval (secs)");

         /** Switch interval for clients-info logs (days, 0 = no swapping). */
  public final CfgPropItem clientsInfoLogSwitchDaysProp =
                settingsProps.add("clientsInfoLogSwitchDays",Integer.valueOf(7),
                          null,"Switch intvl for clients-info logs (days)");

         /** Maximum age for clients-info log files (days, 0=infinite). */
  public final CfgPropItem clientsInfoLogMaxAgeDaysProp =
         settingsProps.add("clientsInfoLogMaxAgeDays",Integer.valueOf(180),null,
                                    "Max age for clients-info logs (days)");

         /** Server statistics logging interval (seconds, 0=none). */
  public final CfgPropItem statsLogIntervalSecsProp =
         settingsProps.add("statsLogIntervalSecs",Integer.valueOf(3600),null,
                                    "Server stats logging interval (secs)");

         /** Message cache age limit in days. */
  public final CfgPropItem maxCacheAgeInDaysProp =
                settingsProps.add("maxCacheAgeInDays",Double.valueOf(15.0),null,
                                         "Message cache age limit in days");

         /** Maximum message size (0 = no limit). */
  public final CfgPropItem maximumMessageSizeProp =
             settingsProps.add("maximumMessageSize",Integer.valueOf(65536),null,
                                                    "Maximum message size");

         /** Maximum-object count for cache (0 = no limit). */
  public final CfgPropItem cacheMaxObjectCountProp =
                settingsProps.add("cacheMaxObjectCount",Integer.valueOf(0),null,
                                          "Maximum-object count for cache");

         /** Maximum-total-data size for cache (bytes, 0 = no limit). */
  public final CfgPropItem cacheMaxTotalDataSizeProp =
                 settingsProps.add("cacheMaxTotalDataSize",Long.valueOf(0),null,
                                       "Maximum-total-data size for cache");

         /** Message cache tolerance value for time matches (ms). */
  public final CfgPropItem cacheTimeToleranceProp =
            settingsProps.add("cacheTimeToleranceMSecs",Long.valueOf(5000),null,
                               "Msg cache tolerance for time matches (ms)");

         /** Name of directory for archive files. */
  public final CfgPropItem archiveDirNameStrProp =
                       settingsProps.add("archiveDirNameStr","storage",null,
                                     "Name of directory for archive files");

         /** Base file name to use to generate archive file names. */
  public final CfgPropItem archiveFileNameStrProp =
                 settingsProps.add("archiveFileNameStr","QWEvents.txt",null,
                             "Base name for generating archive file names");

         /** Message archive age limit in days. */
  public final CfgPropItem maxArchiveAgeInDaysProp =
              settingsProps.add("maxArchiveAgeInDays",Double.valueOf(30.0),null,
                                       "Message archive age limit in days");

         /** Maximum number of messages returned in a resend request. */
  public final CfgPropItem maxResendNumMsgsProp =
                  settingsProps.add("maxResendNumMsgs",Integer.valueOf(50),null,
                               "Max # of msgs returned per resend request");

         /** Max age for resend-request messages in days. */
  public final CfgPropItem maxResendAgeInDaysProp =
               settingsProps.add("maxResendAgeInDays",Double.valueOf(15.0),null,
                             "Max age for resend-request messages in days");

         /** true to allow duplicate messages to be processed. */
  public final CfgPropItem allowDuplicatesFlagProp =
                 settingsProps.add("allowDuplicatesFlag",Boolean.FALSE,null,
                                "Allow duplicate messages to be processed");

         /** true to check for duplicate feeder-data-source in messages. */
  public final CfgPropItem checkDupFdrSourceFlagProp =
                settingsProps.add("checkDupFdrSourceFlag",Boolean.TRUE,null,
                                 "Check for duplicate fdr-data-src in mgs");

         /** Upper child-element name for key-string generation for msgs. */
  public final CfgPropItem msgKeyElement1NameProp =
               settingsProps.add("msgKeyElement1Name",MsgTag.EQMESSAGE,null,
                                "Upper child-element name for key-strings");

         /** Lower child-element name for key-string generation for msgs. */
  public final CfgPropItem msgKeyElement2NameProp =
                   settingsProps.add("msgKeyElement2Name",MsgTag.EVENT,null,
                                "Lower child-element name for key-strings");

         /** Alternate server IDs list ("addr1:port1,addr2:port2,..."). */
  public final CfgPropItem altServerIdsListProp =
             settingsProps.add("altServerIdsList",UtilFns.EMPTY_STRING,null,
                                               "Alternate server IDs list");

         /** Default event-domain name to use (if none from feeder). */
  public final CfgPropItem defaultEvtDomainNameProp =
         settingsProps.add("defaultEvtDomainName",UtilFns.EMPTY_STRING,null,
                                        "Default event-domain name to use");

         /** Default event-type name to use (if none from feeder). */
  public final CfgPropItem defaultEvtTypeNameProp =
           settingsProps.add("defaultEvtTypeName",UtilFns.EMPTY_STRING,null,
                                          "Default event-type name to use");

         /** Redirect server IDs list ("addr1:port1,addr2:port2,..."). */
  public final CfgPropItem redirectServersListProp =
          settingsProps.add("redirectServersList",UtilFns.EMPTY_STRING,null,
                                                "Redirect server IDs list");

         /** Flag to randomize redirect server IDs list when used. */
  public final CfgPropItem randomizeRedirListFlagProp =
               settingsProps.add("randomizeRedirListFlag",Boolean.TRUE,null,
                                      "Randomize redirect server IDs list");

         /** Number of client connections to trigger server redirect. */
  public final CfgPropItem redirectConnectsCountProp =
              settingsProps.add("redirectConnectsCount",Integer.valueOf(0),null,
                                      "Number of connections for redirect");

         /** Percent (0-100) of client connections to be server-redirected. */
  public final CfgPropItem redirectPercentValueProp =
             settingsProps.add("redirectPercentValue",Integer.valueOf(100),null,
                                      "Percent of connections to redirect");

         /** Recommended polling interval (ms) for web-services clients. */
  public final CfgPropItem recPollingItervalMsProp =
            settingsProps.add("recPollingItervalMs",Integer.valueOf(10000),null,
                             "Recommended polling interval (web services)");

         /** Name of ORB configuration file. */
  public final CfgPropItem orbConfigFileProp =
                settingsProps.add("orbConfigFile",UtilFns.EMPTY_STRING,null,
                                  "ORB config filename (not usually used)");

         /** Default feeder-data-source host name for messages. */
  public final CfgPropItem defaultFdrSourceHostProp =
         settingsProps.add("defaultFdrSourceHost",UtilFns.EMPTY_STRING,null,
                                    "Default feeder-data-source host name");

         /** Password database host address. */
  public final CfgPropItem pwdDatabaseHostAddressProp =
       settingsProps.add("pwdDatabaseHostAddress","localhost",null,
                                          "Password database host address");

         /** Password database port number. */
  public final CfgPropItem pwdDatabasePortNumberProp =
           settingsProps.add("pwdDatabasePortNumber","3306",null,
                                           "Password database port number");

         /** Password database identifier. */
  public final CfgPropItem pwdDatabaseIdentifierProp =
       settingsProps.add("pwdDatabaseIdentifier",UtilFns.EMPTY_STRING,null,
                                            "Password database identifier");

         /** Password database-access username. */
  public final CfgPropItem pwdDatabaseUsernameProp =
       settingsProps.add("pwdDatabaseUsername",UtilFns.EMPTY_STRING,null,
                                       "Password database-access username");

         /** Password database-access password. */
  public final CfgPropItem pwdDatabasePasswordProp =
       settingsProps.add("pwdDatabasePassword",UtilFns.EMPTY_STRING,null,
                                       "Password database-access password");

         /** Name of the local password file; format is "username=encryptedPwd". */
  public final CfgPropItem localPasswordFileProp =
          settingsProps.add("localPasswordFile", UtilFns.EMPTY_STRING, null,
                                                     "Local password file");

         /** Name of the client-versions-information file. */
  public final CfgPropItem clientVersionsInfoFileProp =
       settingsProps.add("clientVersionsInfoFile",UtilFns.EMPTY_STRING,null,
                                        "Client-versions-information file");

         /** Delay between polls of client-versions-information file (sec). */
  public final CfgPropItem clientVInfoPollDelaySecsProp =
        settingsProps.add("clientVInfoPollDelaySecs",Integer.valueOf(3600),null,
                                    "Client-versions-info-file poll delay");

         /** true to reject client login if distribution name not known. */
  public final CfgPropItem rejectUnknownDistFlagProp =
               settingsProps.add("rejectUnknownDistFlag",Boolean.FALSE,null,
                               "Reject login if unknown distribution name");

         /** true to allow "obsolete-login" clients to connect. */
  public final CfgPropItem allowObsLoginClientsFlagProp =
            settingsProps.add("allowObsLoginClientsFlag",Boolean.FALSE,null,
                                 "Allow obsolete-login clients to connect");

         /** Client-connection-tracking timeout value, in seconds. */
  public final CfgPropItem clientTimeoutSecsProp = settingsProps.add(
      "clientTimeoutSecs", CONNECTION_TIMEOUT_SECS_DEFAULT, null,
      "Client-connection-tracking timeout (secs)");

         /** Private key file (used for signing). */
  public final CfgPropItem privKeyFileProp =
                  settingsProps.add("privKeyFile",UtilFns.EMPTY_STRING,null,
                        "Private key file").setEmptyStringDefaultFlag(true);

         /** Certificate of Authority (CoA) file. */
  public final CfgPropItem coaFileProp =
             settingsProps.add("coaFile",QWConnProperties.DEF_COA_FILE,null,
           "Certificate of authority file").setEmptyStringDefaultFlag(true);

         /** Certificate Revocation List (CRL) file. */
  public final CfgPropItem crlFileProp =
             settingsProps.add("crlFile",QWConnProperties.DEF_CRL_FILE,null,
        "Certificate revocation list file").setEmptyStringDefaultFlag(true);

         /** Certificate file. */
  public final CfgPropItem certFileProp =
           settingsProps.add("certFile",QWConnProperties.DEF_CERT_FILE,null,
                        "Certificate file").setEmptyStringDefaultFlag(true);

         /** Memory-size warning value, in megabytes (0=disable). */
  public final CfgPropItem memorySizeWarnMBytesProp =
             settingsProps.add("memorySizeWarnMBytes",Integer.valueOf(100),null,
                                          "Memory-size warning value (MB)");

         /** Thread-count warning margin (0=disable). */
  public final CfgPropItem threadCountWarnMarginProp =
             settingsProps.add("threadCountWarnMargin",Integer.valueOf(25),null,
                                             "Thread-count warning margin");

         /** Name of Notification Server status log file (""=none). */
  public final CfgPropItem notifSvrStatusLogNameProp =
      settingsProps.add("notifSvrStatusLogName","log/NotifServerStatus.log",
                        null,"Name of Notification Server status log file");

         /** true to enable warning for no notif-server status logs. */
  public final CfgPropItem notifSvrStatusWarnFlagProp =
              settingsProps.add("notifSvrStatusWarnFlag",Boolean.FALSE,null,
                                     "Warn if no Notif-Server status logs");

         /** Status report interval, in minutes (0=none). */
  public final CfgPropItem stRptItvlMinsProp =
                     settingsProps.add("stRptItvlMins",Double.valueOf(0.0),null,
                                        "Status report interval (minutes)");

         /** true to allow status-report queries from clients. */
  public final CfgPropItem stRptClientQueriesFlagProp =
               settingsProps.add("stRptClientQueriesFlag",Boolean.TRUE,null,
                                "Allow status-report queries from clients");

         /** File name for local status-report files. */
  public final CfgPropItem stRptLocalFileProp =
               settingsProps.add("stRptLocalFile",UtilFns.EMPTY_STRING,null,
                                      "File name for local status reports");

         /** Maximum age for status report files (days, 0=infinite). */
  public final CfgPropItem stRptFilesMaxAgeProp =
                   settingsProps.add("stRptFilesMaxAge",Integer.valueOf(7),null,
                             "Max age for report files (days, 0=infinite)");

         /** Remote directory for status reports. */
  public final CfgPropItem stRptRemoteDirProp =
              settingsProps.add("stRptRemoteDir",UtilFns.EMPTY_STRING,null,
                                     "Remote directory for status reports");

         /** Remote file name for status reports. */
  public final CfgPropItem stRptRemoteFNameProp =
             settingsProps.add("stRptRemoteFName",UtilFns.EMPTY_STRING,null,
                                     "Remote file name for status reports");

         /** Remote host name for status reports. */
  public final CfgPropItem stRptRemoteHostProp =
              settingsProps.add("stRptRemoteHost",UtilFns.EMPTY_STRING,null,
                                     "Remote host name for status reports");

         /** Remote username for status reports. */
  public final CfgPropItem stRptRemoteUserProp =
              settingsProps.add("stRptRemoteUser",UtilFns.EMPTY_STRING,null,
                                      "Remote username for status reports");

         /** Remote authorization key file for status reports. */
  public final CfgPropItem stRptRemAuthKeyFileProp =
          settingsProps.add("stRptRemAuthKeyFile",UtilFns.EMPTY_STRING,null,
                                 "Remote auth-key file for status reports");

         /** Check if status-reports remote host is 'known'. */
  public final CfgPropItem stRptRemChkHostFlagProp =
                 settingsProps.add("stRptRemChkHostFlag",Boolean.FALSE,null,
                             "Check if status-reports remote host 'known'");

         /** Minimum tracking log level for status report output. */
  public final CfgPropItem stRptTrkLogLevelProp =
              settingsProps.add("stRptTrkLogLevel",LogFile.WARNING_STR,null,
                             "Min tracking level for status report output");

         /** Maximum number of log messages tracked for status report. */
  public final CfgPropItem stRptTrkLogCountProp =
                  settingsProps.add("stRptTrkLogCount",Integer.valueOf(10),null,
                             "Max # of log msgs tracked for status report");

         /** Max age of log msgs tracked for status report, 0 = no limit. */
  public final CfgPropItem stRptTrkLogHoursProp =
                 settingsProps.add("stRptTrkLogHours",Double.valueOf(24.0),null,
                              "Max age of log msgs tracked for status rep");

         /** true to send status report immediately on tracked log msgs. */
  public final CfgPropItem stRptOnLogFlagProp =
                       settingsProps.add("stRptOnLogFlag",Boolean.TRUE,null,
                              "Send status report immediately on log msgs");

         /** true to include clients list in status report. */
  public final CfgPropItem stRptClientsFlagProp =
                     settingsProps.add("stRptClientsFlag",Boolean.TRUE,null,
                                   "Include clients list in status report");

         /** Dynamic feeder-modules configuration file. */
  public final CfgPropItem feederModulesCfgFileProp =
         settingsProps.add("feederModulesCfgFile",UtilFns.EMPTY_STRING,null,
                                       "Feeder-modules configuration file");

         /** true to check for MsgSrc/MsgIdent values in messages. */
  public final CfgPropItem checkEQMsgIdentFlagProp =
          settingsProps.add("checkEQMsgIdentFlag",Boolean.TRUE,null,
                                "Check for MsgSrc/MsgIdent values in msgs");


  /**
   * Creates an object for managing QuakeWatch server configuration items.
   * @param cfgFileNameStr the name of the XML-format input
   * config file to read and process.
   */
  public ServerConfigManager(String cfgFileNameStr)
  {
    //process XML-format configuration file:
    super(cfgFileNameStr);
  }

  /**
   * Loads the XML-format configuration file and loads the set of
   * "name = value" items specified by the "Settings" element in
   * the configuration file.
   * @param programArgs string array of command-line parameters to be
   * processed.
   * @return 'true' if successful, false if an error occurred (in which
   * case the 'getErrorMessage()' method may be used to fetch a
   * description of the error).
   */
  public boolean loadSettings(String [] programArgs)
  {
    //load "Settings" from configuration file:
    return loadSettings(settingsProps,true,programArgs);
  }
}
