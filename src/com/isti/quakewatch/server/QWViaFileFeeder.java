//QWViaFileFeeder.java:  QuakeWatch feeder plugin module that receives
//                       messages via files placed into an "input"
//                       directory.
//
// 11/21/2003 -- [ET]  Initial version.
//   1/8/2004 -- [ET]  Modified to extend 'QWAbstractFeeder' class and
//                     support feeder domain/type names for events.
//  1/14/2005 -- [ET]  Modified to send null 'feederSourceHostStr'
//                     and 'feederSrcHostMsgIdNum' values to 'dataEvent()'
//                     call.
//   2/2/2005 -- [ET]  Modified 'processFeederSettings()' method to convert
//                     paths via 'QWServer.fromBaseDir()'.
//   8/8/2006 -- [ET]  Added support for "feederParserClass" configuration
//                     property.
//  10/5/2006 -- [ET]  Modified ViaFileFeederCallBack 'processMsg()'
//                     method to return "error" if parsing fails (rather
//                     than null).
// 10/18/2006 -- [ET]  Version 1.4:  Modified 'stopFeeder()' method to
//                     do work via a separate thread.
//  11/1/2006 -- [ET]  Version 1.5:  Moved logging of feeder-parser-class
//                     setup from 'processFeederSettings()' to 'run()'
//                     method.
//  5/21/2007 -- [ET]  Version 1.6:  Renamed 'startFeeder()' method to
//                     'run()' to allow abstract feeder to properly
//                     setup feeder log file.
//  7/12/2010 -- [ET]  Version 1.7:  Modified to always use UTF-8 charset
//                     when decoding input data.
//  11/4/2010 -- [ET]  Version 1.8:  Added call to 'updateLastDataMsgTime()'.
//

package com.isti.quakewatch.server;

import org.jdom.Element;
import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.util.CfgProperties;
import com.isti.util.CfgPropItem;
import com.isti.util.IstiXmlUtils;
import com.isti.util.MsgViaFileProcessor;

/**
 * Class QWViaFileFeeder is a QuakeWatch feeder plugin module that receives
 * messages via files placed into an "input" directory.
 */
public class QWViaFileFeeder extends QWAbstractFeeder
{
  public static final String MODULE_NAME = "QWViaFileFeeder";
  public static final String VERSION = "1.8";    //name and version strings
  public static final String REVISION_STRING = MODULE_NAME + ", " +
                                                       "Version " + VERSION;
  public static final String FEEDER_NAME = "QWViaFile";    //feeder name
                   //feeder-parser handler for this feeder:
  protected final FeederParserHandler feederParserHandlerObj =
                                                  new FeederParserHandler();
                   //configuration-properties object for this feeder:
  protected final CfgProperties cfgPropsObj = new CfgProperties();
                                       //file processor object:
  protected MsgViaFileProcessor msgViaFileProcObj = null;
                                       //properties for file processor:
  protected final MsgViaFileProcessor.ViaFileProperties viaFilePropObj =
                new MsgViaFileProcessor.ViaFileProperties(cfgPropsObj,null);
                   //configuration-property item for "feederParserClass":
  protected final CfgPropItem feederParserClassProp = cfgPropsObj.add(
                         feederParserHandlerObj.getFeederParserClassProp());
         //flag set true if value for "feederParserClass" cfgProp specified:
  protected boolean parserClassSpecifiedflag = false;


  /**
   * Constructs a QWViaFile feeder module.
   */
  public QWViaFileFeeder()
  {
    this(MODULE_NAME,FEEDER_NAME);     //set thread and feeder name
  }

  /**
   * Constructs a QWViaFile feeder module.
   * @param modulename the name of the module.
   * @param feederName the name of the feeder.
   */
  public QWViaFileFeeder(String modulename, String feederName)
  {
    this.feederName = feederName;  //set the feeder name
  }

  /**
   * Returns the revision String for the feeder.
   * @return the revision String for the feeder.
   */
  public String getRevisionString()
  {
    return REVISION_STRING;
  }

  /**
   * Processes feeder-specific settings.  The settings are usually
   * fetched from the "QWFeederSettings" element in the QuakeWatch-server
   * configuration file.  (For 'QWFeederPlugin' interface.)
   * @param settingsStr The settings string to be processed.
   * @throws QWFeederInitException if an error occurs while processing
   * the settings.
   */
  public void processFeederSettings(String settingsStr)
                                                throws QWFeederInitException
  {
    String msgStr;
    try
    {         //load configuration properties from the given string:
      if(cfgPropsObj.load(settingsStr))
      {  //successfully loaded settings
        if(QWServer.isServerBaseDirectorySetup())
        {     //server-base directory is setup for server
                   //modify paths to make them relative to server-base dir:
          viaFilePropObj.inputDirNameProp.setValue(QWServer.fromBaseDir(
                       viaFilePropObj.inputDirNameProp.stringValue(),true));
          viaFilePropObj.storageDirNameProp.setValue(QWServer.fromBaseDir(
                     viaFilePropObj.storageDirNameProp.stringValue(),true));
          viaFilePropObj.processDirNameProp.setValue(QWServer.fromBaseDir(
                     viaFilePropObj.processDirNameProp.stringValue(),true));
        }
        if(feederParserHandlerObj.isParserClassSpecified())
        {     //value for cfgProp item 'feederParserClass' was specified
                   //instantiate instance of underlying parser class:
          if(!feederParserHandlerObj.instantiateParserObj())
          {   //instantiate failed; throw feeder exception with error msg
            throw new QWFeederInitException(
                            feederParserHandlerObj.getErrorMessageString());
          }
          parserClassSpecifiedflag = true;       //indicate specified
        }
        return;
      }
         //error loading settings; get error message
      msgStr = cfgPropsObj.getErrorMessage();
    }
    catch(Exception ex)
    {    //some kind of exception error; build error message
      msgStr = "Error processing settings:  " + ex;
    }
         //throw feeder exception with error message:
    throw new QWFeederInitException(msgStr);
  }

  /**
   * Executing method for the feeder thread.  Starts up the message
   * collection thread of the feeder.
   */
  public void run()
  {
    if(logObj == null)  //if no log file setup then create dummy object
      logObj = new LogFile(null,LogFile.NO_MSGS,LogFile.ERROR);
    try
    {
      if(parserClassSpecifiedflag)
      {   //indicate feeder-parser-class in use
        logObj.info("Using feeder-parser object ('"
                        + feederParserHandlerObj.getFeederParserClassProp().
                                                             stringValue() +
                         "') for QWViaFileFeeder (\"" + feederName + "\")");
      }
              //create file-processor object using call-back object:
      msgViaFileProcObj = new MsgViaFileProcessor(viaFilePropObj,
                                               new ViaFileFeederCallBack());
      msgViaFileProcObj.setProcCharsetNameStr(   //set charset for input
                                        IstiXmlUtils.UTF_8_ENCODING_FORMAT);
      logObj.info("Starting QWViaFileFeeder processing");
      msgViaFileProcObj.startProcessing();       //start processing thread
    }
    catch(Exception ex)
    {         //some kind of exception error; log message
      logObj.error("QWViaFileFeeder:  Error starting feeder:  " + ex);
    }
  }

  /**
   * Stops the message collection threads of the feeder.  The work
   * is done via a separate thread.  (For 'QWFeederPlugin' interface.)
   */
  public void stopFeeder()
  {
    (new Thread("stop-" + getFeederName())
        {          //create separate thread to do "stop" work
          public void run()
          {
            if(logObj != null)
              logObj.debug2("Feeder stop requested");
            if(msgViaFileProcObj != null)
            {    //file processor is running
              if(logObj != null)
                logObj.info("Stopping QWViaFileFeeder processing");
              msgViaFileProcObj.stopProcessing();   //terminate processing
            }
            QWViaFileFeeder.super.stopFeeder();  //call parent method
          }                                      // (closes log file)
        }).start();          //start thread
  }


  /**
   * Class ViaFileFeederCallBack defines the method for processing the
   * message and returning a completion/success result.
   */
  protected class ViaFileFeederCallBack
                              implements MsgViaFileProcessor.ProcMsgCallBack
  {
    /**
     * Processes a message string and returns a completion/success result.
     * The message string is interpreted as an XML element.
     * @param fileNameStr the name of the source-file for the message, or
     * null if an error occurred.
     * @param msgStr the message string to process, or an error message
     * string if the 'fileNameStr' parameter is null.
     * @return A string version of the messages number used if successful,
     * "error" if the message was rejected due to an XML or send error,
     * "dup" if the message was rejected due to it being a duplicate
     * of a previous message, or null if 'fileNameStr'==null.
     */
    public String processMsg(String fileNameStr,String msgStr)
    {
      try
      {
        if(fileNameStr != null)
        {     //filename string was given
          updateLastDataMsgTime();     //mark time of data message received
          if(parserClassSpecifiedflag)
          {   //using feeder-parser specified via "feederParserClass"
            final Element elementObj;  //parse message into JDOM/XML Elems:
            if((elementObj=feederParserHandlerObj.parseFeederMessageStr(
                                                 msgStr,-1,null,0)) != null)
            {      //message parsed OK
                        //send in element object via callback object
                        // (pass in domain/type names for feeder,
                        //  wait for queue to clear; return result string):
              return callbackObj.dataEvent(feederDomainNameStr,
                        feederTypeNameStr,null,elementObj,null,true,null,0);
            }
            else
            {      //error parsing message; build and log error message
              logObj.warning("QWViaFileFeeder (\"" + feederName +
                                          "\"):  Error parsing message:  " +
                            feederParserHandlerObj.getErrorMessageString());
              logObj.debug("  msg: " + msgStr);
              return QWServerCallback.ERR_RET_STR;    //return error result
            }
          }
          else
          {   //not using feeder-parser
                   //pass event message string on to server, via call-back
                   // (pass in domain/type names for feeder,
                   //  wait for queue to clear; return result string):
            return callbackObj.dataEvent(feederDomainNameStr,
                                 feederTypeNameStr,null,msgStr,true,null,0);
          }
        }
              //filename string not given; log error message
        logObj.warning("QWViaFileFeeder error:  " + msgStr);
        return null;
      }
      catch(Exception ex)
      {  //some kind of exception error; log it
        logObj.warning("QWViaFileFeeder error:  " +
                               "Error processing received message:  " + ex);
        logObj.debug(UtilFns.getStackTraceString(ex));
        return QWServerCallback.ERR_RET_STR;     //return "error" string
      }
    }
  }
}
