//QddsAnssEQFeeder.java:  QuakeWatch feeder plugin module that receives
//                        QDDS messages and passes them into the
//                        QuakeWatch server.  The generated XML conforms
//                        to the ANSS EQ XML format.
//
//   8/9/2006 -- [ET]  Initial version.
//  11/1/2006 -- [ET]  Version 1.2:  Modified verison number to match
//                     QddsFeeder.
//

package com.isti.quakewatch.server;

/**
 * Class QddsAnssEQFeeder is a QuakeWatch feeder plugin module that receives
 * QDDS messages and passes them into the QuakeWatch server.  The generated
 * XML conforms to the ANSS EQ XML format.
 */
public class QddsAnssEQFeeder extends QddsFeeder
{
  public static final String MODULE_NAME = "QddsAnssEQFeeder";
  public static final String VERSION = "1.2";    //name and version strings
  public static final String FEEDER_NAME = "QddsAnssEQ";   //feeder name

  /**
   * Constructs a QDDS-ANSS-EQ feeder module.
   */
  public QddsAnssEQFeeder()
  {
    super(MODULE_NAME, FEEDER_NAME, VERSION, new CubeAnssEQParser());
  }
}
