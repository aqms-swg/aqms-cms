//QWServerMsgRecord.java:  Manages a server-side event-message record.
//
//  11/5/2003 -- [ET]  Initial version.
//   1/2/2004 -- [ET]  Added "MsgEvtDomain", "MsgEvtType" and
//                     "MsgEvtMsgNum" variables.
//  7/22/2004 -- [ET]  Modified to store string form of 'QWmessage'
//                     element (instead of 'Element' objects);
//                     added support for data-size value;
//                     added 'setCreateKeyStringsFlag()' and
//                     'getCreateKeyStringsFlag()' methods.
//  1/20/2005 -- [ET]  Added 'fdrSourceHostStr', 'fdrSourceMsgNumStr'
//                     and 'fdrSourceMsgNumVal' variables.
//  1/31/2005 -- [ET]  Removed code that would use "SourceHost"/"SrcHostMsgID"
//                     attributes if feeder-source attributes were not found.
//  4/28/2005 -- [ET]  Fixed key-string generation so that if data-msg
//                     element contains no child elements then text
//                     content of element is used (instead of entire
//                     element including attributes); modified to
//                     include feeder-data-source host and message-number
//                     attributes (if given) in key string.
// 12/27/2005 -- [ET]  Removed "get/setCreateKeyStringsFlag()" methods
//                     and added methods "get/setAllowDuplicatesFlag()"
//                     and "get/setCheckDupFdrSourceFlag()".
//  9/15/2006 -- [ET]  Added methods "get/setMsgKeyElement1Name()" and
//                     "get/setMsgKeyElement2Name()" and support.
// 11/10/2006 -- [ET]  Added 'getRelayServerID()' method.
//  8/29/2007 -- [ET]  Added 'qwMsgString' parameter to constructor;
//                     modified to use IstiXmlUtils 'elementToFixedString()'
//                     method (which processes incoming message-elements so
//                     that whitespace between elements is removed and
//                     control characters within elements are encoded to
//                     "&##;" strings).
//  6/13/2008 -- [ET]  Added null check to 'getTimeGenerated()' method.
//  7/14/2010 -- [ET]  Modified to ignore value of 'checkDupFdrSourceFlag'
//                     (checking now done in 'QWServerCallback' class).
//

package com.isti.quakewatch.server;

import java.util.Date;
import java.util.List;
import java.util.Iterator;
import java.text.DateFormat;
import java.text.ParseException;
import org.jdom.Element;
import com.isti.util.IstiXmlUtils;
import com.isti.util.Archivable;
import com.isti.util.IstiMessageCachedArchive;
import com.isti.quakewatch.util.QWUtils;
import com.isti.quakewatch.message.QWRecordException;
import com.isti.quakewatch.message.QWMsgNumTimeRec;
import com.isti.quakewatch.message.QWStaticMsgNumTimeRec;
import com.isti.quakewatch.common.MsgTag;

/**
 * Class QWServerMsgRecord manages a server-side event-message record.
 */
public class QWServerMsgRecord
                  implements IstiMessageCachedArchive.ArchivableMsgObjEntry,
                                                             QWMsgNumTimeRec
{
      /** Message number from the 'QWmessage' element for this message. */
  public final long msgNumber;
    /** Time that message was created by server (or null if not given). */
  public final Date timeGenerated;
    /** String form of "QWmessage" element. */
  protected final String qwMessageStr;
    /** Value of "MsgEvtDomain" attribute from QWmessage Element. */
  public final String msgEvtDomainStr;
    /** Value of "MsgEvtType" attribute from QWmessage Element. */
  public final String msgEvtTypeStr;
    /** Value of "MsgEvtMsgNum" attribute from QWmessage Element. */
  public final String msgEvtMsgNumStr;
    /** Value of "FdrSourceHost" attribute from QWmessage Element. */
  public final String fdrSourceHostStr;
    /** String value of "FdrSourceMsgNum" attrib from QWmessage Element. */
  public final String fdrSourceMsgNumStr;
    /** Numeric value of "FdrSourceMsgNum" attrib from QWmessage Element. */
  public final long fdrSourceMsgNumVal;
  protected final String keyStr;
  protected final int dataSizeValue;
  protected final short relaySvrIDStrBegIdx,relaySvrIDStrEndIdx;
  protected static boolean allowDuplicatesFlag = false;
  protected static boolean checkDupFdrSourceFlag = true;
  protected static String msgKeyElement1Name = null;
  protected static String msgKeyElement2Name = null;
              //date-formatter object for 'parseAttribTime()' method:
  private static final DateFormat xmlDateFormatterObj =
                                           QWUtils.getXmlDateFormatterObj();


  /**
   * Creates a message-record object.
   * @param qwMsgElement the 'QWmessage' element to use.
   * @param dataMsgElement the 'DataMessage' element to use to generate
   * the key string into the cache, or null for no key.
   * @param msgNumber message number.
   * @param timeGenDateObj the time the message was generated.
   * @param qwMsgString string form of the 'QWmessage' element, or null
   * if not given.
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   */
  public QWServerMsgRecord(Element qwMsgElement, Element dataMsgElement,
                    long msgNumber, Date timeGenDateObj, String qwMsgString)
                                                    throws QWRecordException
  {
    this(null,qwMsgElement,dataMsgElement,msgNumber,
                                                timeGenDateObj,qwMsgString);
  }

  /**
   * Creates a record of data from the archived-string data for an XML
   * message.  This constructor is needed to have this class implement
   * the 'Archivable' interface.
   * @param archivedDataStr the archived-string data to use.
   * @param mkrObj Archivable.Marker parameter to indicate that this
   * constructor is used to dearchive a record.
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   */
  public QWServerMsgRecord(String archivedDataStr,
                          Archivable.Marker mkrObj) throws QWRecordException
  {
    this(archivedDataStr,null,null,0,null,null);
  }

  /**
   * Creates a message-record object.
   * @param archivedDataStr string form of the 'QWmessage' element to
   * use (ignoring all the other parameters), or null to use the other
   * parameters instead.
   * @param qwMsgElement the 'QWmessage' element to use.
   * @param dataMsgElement 'DataMessage' element to use to generate
   * the key string into the cache, or null for no key.
   * @param msgNumber message number.
   * @param timeGenDateObj time the message was generated.
   * @param qwMsgString string form of the 'QWmessage' element, or null
   * if not given.
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   */
  protected QWServerMsgRecord(String archivedDataStr, Element qwMsgElement,
                                     Element dataMsgElement, long msgNumber,
                                    Date timeGenDateObj, String qwMsgString)
  {
    if(archivedDataStr != null)
    {    //string form of "QWmessage" element was given
      qwMessageStr = archivedDataStr;
      try
      {            //convert string to XML element:
        qwMsgElement = IstiXmlUtils.stringToElement(archivedDataStr);
      }
      catch(Exception ex)
      {            //if error then convert to 'QWRecordException':
        throw new QWRecordException(ex.toString());
      }
      checkQWMsgRecordName(qwMsgElement);   //check element name
              //fetch and save message number:
      this.msgNumber = getAttribLong(qwMsgElement,MsgTag.MSG_NUMBER);
              //fetch and save time of message creation at server:
      this.timeGenerated = getOptAttribTime(qwMsgElement,MsgTag.TIME_GENERATED);
              //get "DataMessage" element:
      dataMsgElement = qwMsgElement.getChild(MsgTag.DATA_MESSAGE);
    }
    else
    {    //string form of "QWmessage" element was not given
      this.msgNumber = msgNumber;
      this.timeGenerated = timeGenDateObj;
              //use string form of "QWmessage" if given; otherwise create:
      qwMessageStr = (qwMsgString != null) ? qwMsgString :
                                                    elemToStr(qwMsgElement);
    }
         //create cache-key string:
    final int keyStrDataSize;
    if(!allowDuplicatesFlag && dataMsgElement != null)
    {    //not allowing duplicates and 'DataMessage' element is OK
              //create key string using 'DataMessage' element:
      keyStr = msgElemsToKeyStr(null,dataMsgElement);
      keyStrDataSize = keyStr.length();     //get length for data size
    }
    else      //feeder-data-source info not available and
    {         // allowing duplicates or 'DataMessage' element not OK
      keyStr = qwMessageStr;           //use 'QWmessage' element string
      keyStrDataSize = 0;              //indicate no extra data size
    }
              //get event domain, type and msgNum:
    msgEvtDomainStr = qwMsgElement.getAttributeValue(MsgTag.MSG_EVT_DOMAIN);
    msgEvtTypeStr = qwMsgElement.getAttributeValue(MsgTag.MSG_EVT_TYPE);
    msgEvtMsgNumStr = qwMsgElement.getAttributeValue(MsgTag.MSG_EVT_MSG_NUM);
              //get feeder-source host name and message number:
    fdrSourceHostStr =
                     qwMsgElement.getAttributeValue(MsgTag.FDR_SOURCE_HOST);
    fdrSourceMsgNumStr =
                   qwMsgElement.getAttributeValue(MsgTag.FDR_SOURCE_MSGNUM);
    if(fdrSourceMsgNumStr != null)
    {    //data-source message-number attribute was found
      long numVal;
      try     //convert message-number string to number
      {       //skip any leading '+' and trim whitespace:
        final int p = (fdrSourceMsgNumStr.startsWith("+")) ? 1 : 0;
        numVal = Long.parseLong(fdrSourceMsgNumStr.substring(p).trim());
      }
      catch(NumberFormatException ex)
      {       //unable to parse as a number; use zero
        numVal = 0;
      }
      fdrSourceMsgNumVal = numVal;
    }
    else
      fdrSourceMsgNumVal = 0;
    final String rlySvrIdStr;
    int sLen,sPos;
    if(dataMsgElement != null &&
                              (rlySvrIdStr=dataMsgElement.getAttributeValue(
                                         MsgTag.RELAY_SERVER_ID)) != null &&
                                          (sLen=rlySvrIdStr.length()) > 0 &&
                              (sPos=qwMessageStr.indexOf(rlySvrIdStr)) >= 0)
    {    //'RelayServerID' attribute found and matched in message string
      relaySvrIDStrBegIdx = (short)sPos;    //save beg/end index of text
      relaySvrIDStrEndIdx = (short)(relaySvrIDStrBegIdx + sLen);
    }
    else      //'RelayServerID' value not found and matched
      relaySvrIDStrBegIdx = relaySvrIDStrEndIdx = (short)0;
              //calculate data-size value for message record:
    dataSizeValue = qwMessageStr.length() + keyStrDataSize +
                ((msgEvtDomainStr != null) ? msgEvtDomainStr.length() : 0) +
                    ((msgEvtTypeStr != null) ? msgEvtTypeStr.length() : 0) +
            ((msgEvtMsgNumStr != null) ? msgEvtMsgNumStr.length() : 0) + 44;
  }

  /**
   * Checks if the root element of the given message element object is
   * named "QWmessage".
   * @param qwMsgElement message element object to check.
   * @throws QWRecordException if the root element of the given message
   * element is not named "QWmessage".
   */
  protected final void checkQWMsgRecordName(Element qwMsgElement)
                                                   throws QWRecordException
  {
    if(!MsgTag.QW_MESSAGE.equalsIgnoreCase(qwMsgElement.getName()))
    {    //root element name not "QWmessage"
      throw new QWRecordException("Element tag name \"" +
                                        MsgTag.QW_MESSAGE + "\" not found");
    }
  }

  /**
   * Compares the given 'QWServerMsgRecord' for "equality" using the
   * 'keyStr' field (if available).
   * @param obj the 'QWServerMsgRecord' object to compare.
   * @return true if the given 'QWServerMsgRecord' object is "equal"
   * to this object.
   */
  public boolean equals(Object obj)
  {
    if(obj instanceof QWServerMsgRecord)
    {    //object type matches
      if(keyStr == null)
      {  //key data not available
              //use "QWmessage" string data (if available):
        return (qwMessageStr != null) ?
              (qwMessageStr.equals(((QWServerMsgRecord)obj).qwMessageStr)) :
                                                          super.equals(obj);
      }
      return keyStr.equals(((QWServerMsgRecord)obj).keyStr);
    }
    return false;
  }

  /**
   * Returns the hash-code for the 'keyStr' field (if available).
   * @return The hash-code for the 'keyStr' field (or if not available
   * then the hash-code for the entire object).
   */
  public int hashCode()
  {
    return (keyStr != null) ? keyStr.hashCode() : super.hashCode();
  }

  /**
   * Returns the date that this object should be archived under.  This
   * method is needed to have this class implement the 'Archivable'
   * interface.
   * @return the date we should be archived under
   */
  public Date getArchiveDate()
  {
    return timeGenerated;
  }

  /**
   * Returns the archive-data representation of this object.  This
   * method is needed to have this class implement the 'Archivable'
   * interface.
   * @return A string containing the archive-data representation of
   * this object.
   */
  public String toArchivedForm()
  {
    return qwMessageStr;
  }

  /**
   * Returns the message number from the 'QWmessage' element for this
   * message.  This method is needed to have this class implement the
   * 'QWMsgNumTimeRec' interface.
   * @return The message number from the 'QWmessage' element for this
   * message.
   */
  public long getMsgNum()
  {
    return msgNumber;
  }

  /**
   * Returns the time-generated value for this record.  This method is
   * needed to have this class implement the 'QWMsgNumTimeRec' interface.
   * @return The long-integer epoch time-generated values, in milliseconds
   * since 1/1/1970, or 0 if not available.
   */
  public long getTimeGenerated()
  {
    return (timeGenerated != null) ? timeGenerated.getTime() : 0;
  }

  /**
   * Sets the allow-duplicates flag, which affects how the key strings are
   * created.  If 'allowDuplicatesFlag'==true then the entire message is
   * used for the key string (resulting in duplicate "DataMessage" payloads
   * never being rejected).  If 'allowDuplicatesFlag'==false then the
   * "DataMessage" is used for the key string.
   * @param flgVal true to allow duplicate message; false to reject
   * duplicate messages.
   */
  public static void setAllowDuplicatesFlag(boolean flgVal)
  {
    allowDuplicatesFlag = flgVal;
  }

  /**
   * Returns the allow-duplicates flag.
   * @return true if duplicate messages are allow; false if not.
   */
  public static boolean getAllowDuplicatesFlag()
  {
    return allowDuplicatesFlag;
  }

  /**
   * Sets the check-duplicate-feeder-source flag.  The state of this
   * flag does not affect functionality in this class.
   * @param flgVal check-duplicate-feeder-source flag value to use.
   */
  public static void setCheckDupFdrSourceFlag(boolean flgVal)
  {
    checkDupFdrSourceFlag = flgVal;
  }

  /**
   * Returns the check-duplicate-feeder-source flag.
   * @return The check-duplicate-feeder-source flag.
   */
  public static boolean getCheckDupFdrSourceFlag()
  {
    return checkDupFdrSourceFlag;
  }

  /**
   * Sets the name of the upper child-element under the 'DataMessage'
   * element to be used when generating a check-duplicate key string
   * for the message object.  When a check-duplicate key string is
   * generated, if the 'DataMessage' element contains a child-element
   * matching the 'msgKeyElement1Name' value, and that element contains
   * a child-element matching the 'msgKeyElement2Name' value, then
   * that child-element is used for the key string.  If only
   * the 'msgKeyElement1Name' value is given then its matching
   * child-element will be used.
   * @param nameStr the child-element name to be matched, or null or
   * an empty string for none.
   */
  public static void setMsgKeyElement1Name(String nameStr)
  {
    msgKeyElement1Name = (nameStr != null && nameStr.trim().length() > 0) ?
                                                             nameStr : null;
  }

  /**
   * Returns the name of the upper child-element under the 'DataMessage'
   * element to be used when generating a check-duplicate key string
   * for the message object.
   * @return the child-element name to be matched, or null if none.
   */
  public static String getMsgKeyElement1Name()
  {
    return msgKeyElement1Name;
  }

  /**
   * Sets the name of the lower child-element under the 'DataMessage'
   * element to be used when generating a check-duplicate key string
   * for the message object.  When a check-duplicate key string is
   * generated, if the 'DataMessage' element contains a child-element
   * matching the 'msgKeyElement1Name' value, and that element contains
   * a child-element matching the 'msgKeyElement2Name' value, then
   * that child-element is used for the key string.  If only
   * the 'msgKeyElement1Name' value is given then its matching
   * child-element will be used.
   * @param nameStr the child-element name to be matched, or null or
   * an empty string for none.
   */
  public static void setMsgKeyElement2Name(String nameStr)
  {
    msgKeyElement2Name = (nameStr != null && nameStr.trim().length() > 0) ?
                                                             nameStr : null;
  }

  /**
   * Returns the name of the lower child-element under the 'DataMessage'
   * element to be used when generating a check-duplicate key string
   * for the message object.
   * @return the child-element name to be matched, or null if none.
   */
  public static String getMsgKeyElement2Name()
  {
    return msgKeyElement2Name;
  }

  /**
   * Converts an Element object to a string.
   * @param elementObj the Element object to use.
   * @return A new converted string.
   * @throws QWRecordException if an error occurred.
   */
  protected static String elemToStr(Element elementObj)
  {
    try
    {              //convert Element object to a string and return it:
      return IstiXmlUtils.elementToFixedString(elementObj);
    }
    catch(Exception ex)
    {              //if error then convert to 'QWRecordException':
      throw new QWRecordException(
                              "Error converting element to string:  " + ex);
    }
  }

  /**
   * Returns the long integer value of an attribute in the given element.
   * @param elemObj the element object to use.
   * @param nameStr the name of the attribute.
   * @return The numeric value from the attribute.
   * @throws QWRecordException if the named attribute was not found or if
   * its value could not be converted.
   */
  protected static long getAttribLong(Element elemObj, String nameStr)
                                                    throws QWRecordException
  {
    return parseAttribLong(
                    nameStr,elemObj.getAttributeValue(nameStr)).longValue();
  }

  /**
   * Parses a long integer numeric string.
   * @param nameStr the name of the attribute in use (for error message
   * generation).
   * @param str the floating-point numeric string to parse (or null for
   * none).
   * @return A new Long object holding the value, or null if the 'str'
   * parameter is null.
   * @throws QWRecordException if the string could not be converted.
   */
  protected static Long parseAttribLong(String nameStr, String str)
                                                    throws QWRecordException
  {
    if(str == null)
      return null;
    final Long longObj;
    if((longObj=QWUtils.parseStringLong(str)) == null)
    {    //error parsing numeric value
      throw new QWRecordException("Error parsing long integer \"" +
                              nameStr + "\" attribute value (" + str + ")");
    }
    return longObj;
  }

  /**
   * Returns the date/time value of an optional attribute in the given
   * element.
   * @param elemObj the element object to use.
   * @param nameStr the name of the attribute.
   * @return A new Date object holding the date/time value, or null if
   * the attribute was not found.
   * @throws QWRecordException if the attribute was found but its value
   * could not be converted.
   */
  protected static Date getOptAttribTime(Element elemObj, String nameStr)
                                                    throws QWRecordException
  {
    return parseAttribTime(nameStr,elemObj.getAttributeValue(nameStr));
  }

  /**
   * Parses a date/time specification string.
   * @param nameStr the name of the attribute in use (for error message
   * generation).
   * @param str the date/time string to parse (or null for none).
   * @return A new Date object holding the date/time, or null if the 'str'
   * parameter is null.
   * @throws QWRecordException if the string could not be converted.
   */
  protected static Date parseAttribTime(String nameStr, String str)
                                                    throws QWRecordException
  {
    final Date dateObj;           //parse string as XML date:
    try
    {
      synchronized(xmlDateFormatterObj)
      {  //only allow one thread at a time to use date-format object
        if((dateObj=xmlDateFormatterObj.parse(str)) != null)
          return dateObj;
      }
    }
    catch(ParseException ex)
    {
    }
              //if unable to parse then throw exception:
    throw new QWRecordException("Error parsing \"" + nameStr +
                          "\" attribute date/time value (\"" + str + "\")");
  }

  /**
   * Returns a string containing the feeder-data-source host name and
   * message number extracted from the given 'QWmessage' element.
   * @param qwMsgElement the 'QWmessage' element to use.
   * @return A new string containing the feeder-data-source host name
   * and message number, or null if they could not be extracted.
   */
  public static String getFdrSrcHostMsgNumStr(Element qwMsgElement)
  {
    if(qwMsgElement != null)
    {    //'QWmessage' element was given
      final String hostStr,numStr;
      if((hostStr=qwMsgElement.getAttributeValue(
                                         MsgTag.FDR_SOURCE_HOST)) != null &&
                                     (numStr=qwMsgElement.getAttributeValue(
                                         MsgTag.FDR_SOURCE_MSGNUM)) != null)
      {  //feeder-data-source attributes found; return them
        return hostStr + ',' + numStr;
      }
    }
    return null;
  }

  /**
   * Creates a key string that will be used as a key into the cache.
   * The string consists of the value of the "Action" attribute (if
   * found) and all child elements.  If feeder-data-source host and
   * message-number values are given then they are also included.
   * If the 'msgKeyElement1Name' and 'msgKeyElement2Name' values are
   * setup then they will be used to select a child-element under the
   * given element.
   * @param fdrSrcHostMsgNumStr feeder-data-source host name and message
   * number string to use, or null for none.
   * @param dataMsgElement the 'DataMessage' element object to use, or
   * null for none.
   * @return The generated key string.
   * @throws QWRecordException if an error occurred.
   */
  public static String msgElemsToKeyStr(String fdrSrcHostMsgNumStr,
                            Element dataMsgElement) throws QWRecordException
  {
    String str;
    if(dataMsgElement != null)
    {    //"DataMessage" element not null
                   //if "Action" attribute found then use
                   // its value as a prefix string:
      if((str=dataMsgElement.getAttributeValue(MsgTag.ACTION)) != null)
        str += ",";                 //append comma separator
      else                     //if "Action" attribute not found then
        str = "";              //no prefix string
      if(fdrSrcHostMsgNumStr != null)            //if given then
        str += fdrSrcHostMsgNumStr + ',';        //include feeder-src string
      final List dataMsglistObj = dataMsgElement.getChildren();
      if(dataMsglistObj.size() > 0)
      {  //"DataMessage" element contains child elements
              //check for child-element matching 'msgKeyElement1Name':
        final Iterator iterObj;
        final Element elem1Obj;
        if(msgKeyElement1Name != null &&
                                       (elem1Obj=IstiXmlUtils.getAnyNSChild(
                                dataMsgElement,msgKeyElement1Name)) != null)
        {     //child-element matching 'msgKeyElement1Name' was found
                        //check for sub-child-element matching
                        // 'msgKeyElement2Name'; if found then use
                        // it; otherwise use 'msgKeyElement1Name' element:
          final Element elem2Obj;
          iterObj = (msgKeyElement2Name != null &&
                                       (elem2Obj=IstiXmlUtils.getAnyNSChild(
                                    elem1Obj,msgKeyElement2Name)) != null) ?
                                         elem2Obj.getChildren().iterator() :
                                          elem1Obj.getChildren().iterator();
        }
        else  //child-element matching 'msgKeyElement1Name' not found
          iterObj = dataMsglistObj.iterator();   //use "DataMessage" element
              //add child elements to key string:
        Object obj;
        while(iterObj.hasNext())
        {     //for each child element
          if((obj=iterObj.next()) instanceof Element)      //if type OK then
            str += elemToStr((Element)obj);      //add string form of element
          else
          {        //if unexpected type then throw exception error
            throw new QWRecordException("Non-element object type " +
                                      "returned by 'getChildren()' method");
          }
        }
        return str;
      }
         //"DataMessage" element does not contain child elements
      str += dataMsgElement.getText();      //just add content of element
    }
    else      //"DataMessage" element is null; put in fdrSrc str (if any)
      str = (fdrSrcHostMsgNumStr != null) ? (fdrSrcHostMsgNumStr + ',') : "";
    return str;
  }

  /**
   * Returns the data object for this record.  This
   * method is needed to have this class implement the
   * 'ArchivableMsgObjEntry/MessageObjectEntry' interface.
   * @return The data object for this record; the string version
   * of the "QWmessage" element.
   */
  public Object getDataObj()
  {
    return qwMessageStr;
  }

  /**
   * Returns the "key" string.  This method is needed to have this class
   * implement the 'ArchivableMsgObjEntry/MessageObjectEntry' interface.
   * @return The "key" string; the string form of the DataMessage
   * 'Element' for this object.
   */
  public String getKeyStr()
  {
    return keyStr;
  }

  /**
   * Returns the data-size value for this message record.
   * @return The data-size value for this message record.
   */
  public int getDataSize()
  {
    return dataSizeValue;
  }

  /**
   * Returns the "QWmessage" element object for this record.
   * @return A new 'Element' object for the "QWmessage" data held by this
   * record, or null if the object could not be created.
   */
  public Element getQWMsgElementObj()
  {
    try
    {              //convert string to XML element and return:
      return IstiXmlUtils.stringToElement(qwMessageStr);
    }
    catch(Exception ex)
    {              //if error then return null
      return null;
    }
  }

  /**
   * Returns the message-number and time-generated values from the
   * "Relay..." attributes held by this message record.
   * @return A new 'QWMsgNumTimeRec' object containing the values in
   * the "RelayMsgNum" and "RelayTimeGen" attributes held by this
   * record, or null if the attributes could not be retrieved.
   */
  public QWMsgNumTimeRec getRelayMsgNumTimeRec()
  {
    final Element qwMsgElement;   //convert "QWmessage" str to XML element:
    if((qwMsgElement=getQWMsgElementObj()) == null)
      return null;           //if error then return null
              //get "DataMessage" element:
    final Element dataMsgElement =
                                 qwMsgElement.getChild(MsgTag.DATA_MESSAGE);
    String str;
    final Long longObj;
              //fetch and convert "RelayMsgNum" attribute; fetch
              // "RelayTimeGen" attribute; if all OK then move on:
    if(dataMsgElement != null && (str=dataMsgElement.getAttributeValue(
                                           MsgTag.RELAY_MSG_NUM)) != null &&
                           (longObj=QWUtils.parseStringLong(str)) != null &&
                                      (str=dataMsgElement.getAttributeValue(
                                            MsgTag.RELAY_TIME_GEN)) != null)
    {    //msgNum fetch & converted and timeGen string fetched OK
      final Date dateObj;    //convert date string to 'Date' object:
      if((dateObj=QWUtils.parseStringXmlDate(str)) != null)
      {  //timeGen date converted OK
              //return fetched "Relay" msgNum and timeGen values:
        return new QWStaticMsgNumTimeRec(longObj.longValue(),
                                                         dateObj.getTime());
      }
    }
    return null;        //unable to fetch and convert values
  }

  /**
   * Returns a descriptive string for this message record.
   * @return A descriptive string for this message record.
   */
  public String toString()
  {
    return "QWServerMsgRecord, msgNum=" + msgNumber + ", timeGen=" +
                              timeGenerated + ", dataSize=" + dataSizeValue;
  }

  /**
   * Returns the 'RelayServerID' value from this server-message record.
   * @return The 'RelayServerID' value from this server-message record,
   * or null if none is available.
   */
  public String getRelayServerID()
  {
    return (relaySvrIDStrEndIdx > relaySvrIDStrBegIdx) ?
           qwMessageStr.substring(relaySvrIDStrBegIdx,relaySvrIDStrEndIdx) :
                                                                       null;
  }
}
