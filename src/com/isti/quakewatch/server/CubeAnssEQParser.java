//CubeAnssEQParser.java:  Parses CUBE-format messages into a JDOM/XML
//                        tree of elements.  The generated XML conforms
//                        to the ANSS EQ XML format.
//
//  9/18/2006 -- [ET]  Initial version.
//  10/5/2006 -- [ET]  Added "CUBE_Code" comments to generated messages.
//  8/15/2007 -- [ET]  Modified to detect and pass through XML-format
//                     messages; moved  'parseMessageString()' method
//                     to 'CubeParserSupport' base class; added
//                     'parseCubeMsgStringData()' method; modified
//                     parsing of date to allow spaces.
//  9/21/2007 -- [ET]  Fixed bugs in 'parseCubeMsgStringData()' method
//                     where incomplete CUBE-field items could result in
//                     'StringIndexOutOfBoundsException' being thrown.
//   1/5/2009 -- [ET]  Fixed bug where version code on delete message was
//                     ignored if message had no text comment.
// 11/16/2009 -- [ET]  Modified to support location-method code 'Z' for
//                     ANSS-EQ-XML 'Scope'=="Internal" and to always set
//                     'Event'|'Usage' = "Actual".
//   3/8/2010 -- [ET]  Modified to not drop magnitude value if given and
//                     "number of stations used for location" value not
//                     given.
//  4/21/2010 -- [ET]  Modified to set Tsunami Product 'Type' value to
//                     "Tsunami" instead of "LinkURL"; modified to use
//                     'decValFormatObj' from base class.
//

package com.isti.quakewatch.server;

import java.util.Date;
import java.text.ParseException;
import org.jdom.Element;
import org.jdom.Attribute;
import org.jdom.Text;
import org.jdom.Namespace;
import com.isti.util.UtilFns;
import com.isti.util.TwoStringBlock;
import com.isti.util.gis.GisUtils;
import com.isti.quakewatch.common.MsgTag;
import com.isti.quakewatch.util.CubeCodeTranslator;

/**
 * Class CubeAnssEQParser parses CUBE-format messages into a JDOM/XML
 * tree of elements.  The generated XML conforms to the ANSS EQ XML
 * format.
 */
public class CubeAnssEQParser extends CubeParserSupport
                                                    implements MsgParserIntf
{
    /** Namespace object for elements. */
  public static final Namespace namespaceObj =
                        Namespace.getNamespace(MsgTag.ANSSEQ_NAMESPACE_STR);

                        //table for translating magnitude-type codes:
//  protected static final Hashtable magTypeCodeXlatTable = new Hashtable();
//  static      //static initialization block; executes only once
//  {                     //fill entries for magnitude-type translate table:
//    magTypeCodeXlatTable.put("B","Mb");
//    magTypeCodeXlatTable.put("C","Md");
//    magTypeCodeXlatTable.put("D","Md");
//    magTypeCodeXlatTable.put("E","Me");
//    magTypeCodeXlatTable.put("G","Ml");
//    magTypeCodeXlatTable.put("I","Mi");
//    magTypeCodeXlatTable.put("L","Ml");
//    magTypeCodeXlatTable.put("N","MbLg");
//    magTypeCodeXlatTable.put("O","Mw");
//    magTypeCodeXlatTable.put("P","Mb");
//    magTypeCodeXlatTable.put("S","Ms");
//    magTypeCodeXlatTable.put("T","Mt");
//    magTypeCodeXlatTable.put("W","Mw");
//  }


  /**
   * Parses CUBE-format message data into a JDOM/XML tree of elements.
   * @param messageString the message string to be parsed.
   * @param timeReceivedStr current time (as an XML-format time string).
   * @param actionAttribObj attrib whose value will be set by this method.
   * @return The resulting "EQMessage" Element object, or null if an
   * error occured (in which case the associated error message may be
   * fetched via the 'getErrorMessage()' method).
   */
  protected Element parseCubeMsgStringData(String messageString,
                          String timeReceivedStr, Attribute actionAttribObj)
  {
    final int msgStrLen;
    if((msgStrLen=messageString.length()) < 12)
    {    //too few character; set error message
      setErrorMessage("Not enough characters in message");
      return null;
    }

         //identify message type:
    final int msgTypeIndex;
    if((msgTypeIndex=msgStringsVec.indexOf(
                                         messageString.substring(0,2))) < 0)
    {    //message ID chars not matched; set error message
      setErrorMessage("Unexpected message type (\"" +
                                      messageString.substring(0,2) + "\")");
      return null;
    }

         //save identification "number" for event:
    final String eventIdNumStr = messageString.substring(2,10).trim();

         //save the 2-character source code string:
    final String sourceCodeStr = messageString.substring(10,12).trim();

         //create Event element:
    final Element eventElem = new Element(MsgTag.EVENT,namespaceObj);
                        //add data-source element:
    addElemContent(eventElem,MsgTag.DATA_SOURCE,sourceCodeStr);
                        //add data-source element:
    addElemContent(eventElem,MsgTag.EVENT_ID,eventIdNumStr);

         //create EQMessage element:
    final Element eqMessageElem =
                                 new Element(MsgTag.EQMESSAGE,namespaceObj);
                        //add message-source element:
    addElemContent(eqMessageElem,MsgTag.SOURCE,sourceCodeStr);
                        //add software-module element:
    addElemContent(eqMessageElem,MsgTag.MODULE,QWServer.REVISION_STRING);
                        //add message-sent-time element:
    addElemContent(eqMessageElem,MsgTag.SENT,timeReceivedStr);
                        //add Event element as child to to EQMessage element:
    eqMessageElem.addContent(eventElem);

    String str,msgCommentStr,magTypeStr;
    Element commentElem,originMethodElem,magnitudeElem;
         //process rest of data based on message type:
    switch(msgTypeIndex)
    {
      case EQUAKE_MSGIDX:         //Earthquake
        if(msgStrLen < 43)
        {     //not enough characters for lat/lon values; set error message
          setErrorMessage("Not enough characters in Earthquake message");
          return null;
        }
              //set action attribute to "Update":
        actionAttribObj.setValue(MsgTag.UPDATE);
              //add Version element to Event element:
        addElemContent(eventElem,MsgTag.VERSION,
                                             messageString.substring(12,13));
              //add "Update" Action element to Event element:
        addElemContent(eventElem,MsgTag.ACTION,MsgTag.UPDATE);
              //process date string:
        String dateStr = messageString.substring(13,28);
        final Date dateObj;            //date/time for event
        try        //parse date/time (add "00" to end to make milliseconds
        {          // and replace any spaces with zeroes):
          synchronized(cubeDateFormatterObj)
          {    //only allow one thread at a time to use date-format object
            dateObj = cubeDateFormatterObj.parse(
                                           dateStr.replace(' ','0') + "00");
          }
        }
        catch(ParseException ex)
        {                    //error parsing date/time; set message
          setErrorMessage("Unable to parse date/time (\"" + dateStr + "\")");
          return null;
        }
              //process latitude value:
        final Double latObj;
        if((latObj=parseCubicDouble(messageString.substring(28,35),
                                               10000.0,"latitude")) == null)
        {     //error parsing numeric string
          return null;
        }
              //process longitude value:
        final Double lonObj;
        if((lonObj=parseCubicDouble(messageString.substring(35,43),
                                               10000.0,"longitude")) == null)
        {     //error parsing numeric string
          return null;
        }
                   //set Type to "Earthquake":
        final Element eventTypeElemObj =
                    addElemContent(eventElem,MsgTag.TYPE,MsgTag.EARTHQUAKE);

              //set Event|Usage to "Actual" (CUBE msgs always "Actual"):
        addElemContent(eventElem,MsgTag.USAGE,MsgTag.ACTUAL);
              //set Event|Scope to "Public" (will change if locMeth=='Z'):
        final Element eventScopeElem = addElemContent(
                                      eventElem,MsgTag.SCOPE,MsgTag.PUBLIC);

              //create Origin element:
        final Element originElem = new Element(MsgTag.ORIGIN,namespaceObj);
              //add Origin element to Event element:
        eventElem.addContent(originElem);

                   //set event-origin time:
        synchronized(xmlDateFormatterObj)
        {     //only allow one thread at a time to use date-format object
          addElemContent(originElem,MsgTag.TIME,
                                       xmlDateFormatterObj.format(dateObj));
        }
                   //set latitude value:
        addElemContent(originElem,MsgTag.LATITUDE,
                                            decValFormatObj.format(latObj));
                   //set longitude value:
        addElemContent(originElem,MsgTag.LONGITUDE,
                                            decValFormatObj.format(lonObj));
                   //process depth value:
        if((str=getMsgSubstring(messageString,msgStrLen,43,47)) == null)
          break;        //if no more data then exit case
        Double dObj;
        if(str.trim().length() > 0 && !str.startsWith(ASTERISK_STR))
        {     //item contains data
          if((dObj=parseCubicDouble(str,10.0,"depth")) == null)
            return null;
              //enter value as Depth element of Origin element:
          addElemContent(originElem,MsgTag.DEPTH,
                                              decValFormatObj.format(dObj));
        }
                   //process magnitude value:
        if((str=getMsgSubstring(messageString,msgStrLen,47,49)) == null)
          break;        //if no more data then exit case
        double magnitudeVal;           //value for magnitude
        if(str.trim().length() > 0 && !str.startsWith(ASTERISK_STR))
        {     //item contains data
          if((dObj=parseCubicDouble(str,10.0,"magnitude")) == null)
            return null;
          magnitudeVal = dObj.doubleValue();     //save value
              //create Magnitude element:
          magnitudeElem = new Element(MsgTag.MAGNITUDE,namespaceObj);
        }
        else
        {     //magnitude item contains no data
          magnitudeElem = null;
          magnitudeVal = 0.0;
        }
        Integer iObj, numStaIntObj = null, numPhaIntObj = null;
        Double minDistDblObj = null, stdErrDblObj = null,
               horizErrDblObj = null, vertErrDblObj = null,
               azimGapDblObj = null;
                   //process "# of stations" value:
        if((str=getMsgSubstring(messageString,msgStrLen,49,52)) != null)
        {     //more data left to process
          if(str.trim().length() > 0 && !str.startsWith(ASTERISK_STR))
          {   //item contains data
            if((numStaIntObj=parseCubicInteger(str,"# of stations")) == null)
              return null;
          }
                   //process "# of phases" value:
          if((str=getMsgSubstring(messageString,msgStrLen,52,55)) != null)
          {   //more data left to process
            if(str.trim().length() > 0 && !str.startsWith(ASTERISK_STR))
            {   //item contains data
              if((numPhaIntObj=parseCubicInteger(str,"# of phases")) == null)
                return null;
            }
                   //process "minimum distance" value:
            if((str=getMsgSubstring(messageString,msgStrLen,55,59)) != null)
            {      //more data left to process
              if(str.trim().length() > 0 && !str.startsWith(ASTERISK_STR))
              {    //item contains data
                if((minDistDblObj=parseCubicDouble(
                                      str,10.0,"minimum distance")) == null)
                {
                  return null;
                }
              }
                   //process "RMS time error" value:
              if((str=getMsgSubstring(
                                    messageString,msgStrLen,59,63)) != null)
              {    //more data left to process
                if(str.trim().length() > 0 && !str.startsWith(ASTERISK_STR))
                {  //item contains data
                  if((stdErrDblObj=parseCubicDouble(
                                       str,100.0,"RMS time error")) == null)
                  {
                    return null;
                  }
                }
                   //process "horizontal error" value:
                if((str=getMsgSubstring(
                                    messageString,msgStrLen,63,67)) != null)
                {  //more data left to process
                  if(str.trim().length() > 0 &&
                                              !str.startsWith(ASTERISK_STR))
                  {   //item contains data
                    if((horizErrDblObj=parseCubicDouble(
                                      str,10.0,"horizontal error")) == null)
                    {
                      return null;
                    }
                  }
                        //process "vertical error" value:
                  if((str=getMsgSubstring(
                                    messageString,msgStrLen,67,71)) != null)
                  {  //more data left to process
                    if(str.trim().length() > 0
                                           && !str.startsWith(ASTERISK_STR))
                    {   //item contains data
                      if((vertErrDblObj=parseCubicDouble(
                                        str,10.0,"vertical error")) == null)
                      {
                        return null;
                      }
                    }
                        //process "azimuthal gap" value:
                    if((str=getMsgSubstring(
                                    messageString,msgStrLen,71,73)) != null)
                    {   //more data left to process
                      if(str.trim().length() > 0 &&
                                              !str.startsWith(ASTERISK_STR))
                      {   //item contains data; (convert % of circle to deg)
                        if((azimGapDblObj=parseCubicDouble(
                                      str,(1/3.6),"azimuthal gap")) == null)
                        {
                          return null;
                        }
                      }
                      if(magnitudeElem != null)
                      {   //magnitude value was parsed previously
                               //process "magnitude type" code:
                        if((magTypeStr=getMsgSubstring(
                                  messageString,msgStrLen,73,74)) != null &&
                                             magTypeStr.trim().length() > 0)
                        {    //item contains data
                               //enter as Type element of Magnitude element:
                          addElemContent(magnitudeElem,MsgTag.TYPE_KEY,
                                      translateCubeMagTypeCode(magTypeStr));
                        }
                        else
                        {         //make sure "TypeKey" contains a value:
                          addElemContent(magnitudeElem,MsgTag.TYPE_KEY,
                                                            MsgTag.UNKNOWN);
                          magTypeStr = UtilFns.EMPTY_STRING;
                        }
                          //enter Value element of Magnitude element:
                        addElemContent(magnitudeElem,MsgTag.VALUE,
                                      decValFormatObj.format(magnitudeVal));
                          //process "# of mag stations" value:
                        if(msgStrLen >= 75)
                        {    //more data left to process
                              //process "magnitude error" value:
                          if((str=getMsgSubstring(
                                    messageString,msgStrLen,76,78)) != null)
                          {  //more data left to process
                            if(str.trim().length() > 0 &&
                                              !str.startsWith(ASTERISK_STR))
                            { //item contains data
                              if((dObj=parseCubicDouble(
                                       str,10.0,"magnitude error")) == null)
                              {
                                return null;
                              }
                                  //enter as MagError elem of Magnitude:
                              addElemContent(magnitudeElem,MsgTag.ERROR,
                                              decValFormatObj.format(dObj));
                            }
                          }
                          if((str=getMsgSubstring(
                                  messageString,msgStrLen,74,76)) != null &&
                                                  str.trim().length() > 0 &&
                                              !str.startsWith(ASTERISK_STR))
                          {      //item contains data
                            if((iObj=parseCubicInteger(
                                          str,"# of mag stations")) == null)
                            {
                              return null;
                            }
                                  //enter as NumStations elem of Magnitude:
                            addElemContent(magnitudeElem,
                                       MsgTag.NUM_STATIONS,iObj.toString());
                          }
                        }
                             //single magnitude value; make it "preferred":
                        addElemContent(magnitudeElem,MsgTag.PREFERRED_FLAG,
                                                               MsgTag.TRUE);
                        if(magTypeStr.length() > 0)
                        {   //mag-type available; put into 'Comment' element
                          commentElem =
                                   new Element(MsgTag.COMMENT,namespaceObj);
                          addElemContent(commentElem,MsgTag.TYPE_KEY,
                                                   MsgTag.CUBE_CODE.trim());
                          addElemContent(commentElem,MsgTag.TEXT,
                                           (MsgTag.CUBE_CODE + magTypeStr));
                                       //add to 'Magnitude' element:
                          magnitudeElem.addContent(commentElem);
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }

        if(stdErrDblObj != null)
        {     //value contains data
                   //enter value as StdError element of Origin element:
          addElemContent(originElem,MsgTag.STD_ERROR,
                                      decValFormatObj.format(stdErrDblObj));
        }
        if(azimGapDblObj != null)
        {     //value contains data
                   //enter value as AzimGap element of Origin element:
          addElemContent(originElem,MsgTag.AZIM_GAP,
                                     decValFormatObj.format(azimGapDblObj));
        }
        if(minDistDblObj != null)
        {     //value contains data
                   //enter value as MinDist element of Origin element:
          addElemContent(originElem,MsgTag.MIN_DIST,decValFormatObj.format(
                        GisUtils.kmToDegrees(minDistDblObj.doubleValue())));
        }
        if(horizErrDblObj != null)
        {     //value contains data
                   //enter value as Errh element of Origin element:
          addElemContent(originElem,MsgTag.ERRH,
                                    decValFormatObj.format(horizErrDblObj));
        }
        if(vertErrDblObj != null)
        {     //value contains data
                   //enter value as Errz element of Origin element:
          addElemContent(originElem,MsgTag.ERRZ,
                                     decValFormatObj.format(vertErrDblObj));
        }
        if(numPhaIntObj != null)
        {     //value contains data
                   //enter value as NumPhaUsed element of Origin element:
          addElemContent(originElem,MsgTag.NUM_PHA_USED,
                                                   numPhaIntObj.toString());
        }
        if(numStaIntObj != null)
        {     //value contains data
                   //enter value as NumStaUsed element of Origin element:
          addElemContent(originElem,MsgTag.NUM_STA_USED,
                                                   numStaIntObj.toString());
        }

              //process "location method" code:
        originMethodElem = null;       //holder for Origin|Method elem
        if((str=getMsgSubstring(messageString,msgStrLen,78,79)) != null)
        {     //more data left to process
          if(str.trim().length() > 0)
          {   //item contains data
                   //translate CUBE location-method code to
                   // 'Algorithm' and 'Class' strings:
            final TwoStringBlock blockObj = translateCubeLocMethodCode(str);
                        //create Method element (to be added to Origin):
            originMethodElem = new Element(MsgTag.METHOD,namespaceObj);
                             //add Class child-element to Method element:
            addElemContent(originMethodElem,MsgTag.CLASS,blockObj.string2);
                             //add Algorithm child-element to Method elem:
            addElemContent(originMethodElem,MsgTag.ALGORITHM,
                                                          blockObj.string1);
                   //put location-method code into 'Comment' element:
            commentElem = new Element(MsgTag.COMMENT,namespaceObj);
            addElemContent(commentElem,MsgTag.TYPE_KEY,
                                                   MsgTag.CUBE_CODE.trim());
            addElemContent(commentElem,MsgTag.TEXT,(MsgTag.CUBE_CODE+str));
            originMethodElem.addContent(commentElem);
              //set Origin Automatic/Reviewed status based on case of code:
            addElemContent(originElem,MsgTag.STATUS,
                                     (Character.isLowerCase(str.charAt(0)) ?
                                       MsgTag.REVIEWED : MsgTag.AUTOMATIC));
                   //if location-method code 'Z' then
                   // change Event|Scope to "Internal":
            if(str.equalsIgnoreCase(LOCMETH_INTERNAL_STR))
              eventScopeElem.setText(MsgTag.INTERNAL);
                   //if location method is 'Q' (for "probable quarry
                   // explosion") then change Event|Type to "Quarry":
            else if(str.equalsIgnoreCase(LOCMETH_QUARRY_STR))
              eventTypeElemObj.setText(MsgTag.QUARRY);
          }
                //process check character:
          if((str=getMsgSubstring(messageString,msgStrLen,79,80)) != null)
          {   //more data left to process
            if(str.charAt(0) !=
                          calcMenloCheckChar(messageString.substring(0,79)))
            {     //check character mismatch
              final char rCh = str.charAt(0);
              final char cCh = calcMenloCheckChar(
                                             messageString.substring(0,79));
              setErrorMessage("Mismatch in check character for message, " +
                            "recv'd=" + (int)rCh + "(" + rCh + ") calc'd=" +
                                                (int)cCh + "(" + cCh + ")");
              return null;
            }
          }
        }
              //single Origin element; make it "preferred":
        addElemContent(originElem,MsgTag.PREFERRED_FLAG,MsgTag.TRUE);
              //if avail then add Magnitude elem as child to Origin elem:
        if(magnitudeElem != null)
          originElem.addContent(magnitudeElem);
              //if avail then add Method elem as child to Origin elem:
        if(originMethodElem != null)
          originElem.addContent(originMethodElem);
        break;

      case DELQK_MSGIDX:     //Delete Earthquake
              //set action attribute to "Delete":
        actionAttribObj.setValue(MsgTag.DELETE);
        if(msgStrLen > 12)
        {     //msg has Version code; add as element to Event element:
          addElemContent(eventElem,MsgTag.VERSION,
                                            messageString.substring(12,13));
              //fetch comment data (if any)
          msgCommentStr = (msgStrLen > 14) ?
                  messageString.substring(14).trim() : UtilFns.EMPTY_STRING;
        }
        else       //no more data; indicate no comment data
          msgCommentStr = UtilFns.EMPTY_STRING;
              //add "Delete" Action element to Event element:
        addElemContent(eventElem,MsgTag.ACTION,MsgTag.DELETE);
        if(msgCommentStr.length() > 0)
        {     //comment data available; put into 'Comment' element
          commentElem = new Element(MsgTag.COMMENT,namespaceObj);
          addElemContent(commentElem,MsgTag.TYPE_KEY,MsgTag.INFO);
          addElemContent(commentElem,MsgTag.TEXT,msgCommentStr);
          eventElem.addContent(commentElem);     //add to 'Event' element
        }
        break;

      case TEXT_MSGIDX:      //Text Comment
        if(msgStrLen < 14)
        {     //not enough characters for version chars; set error message
          setErrorMessage("Not enough characters in Text Comment message");
          return null;
        }
              //create Comment element:
        commentElem = new Element(MsgTag.COMMENT,namespaceObj);
                   //add TypeKey element to Comment element:
        addElemContent(commentElem,MsgTag.TYPE_KEY,MsgTag.TEXT_COMMENT);
                   //add Version element to Comment element:
        addElemContent(commentElem,MsgTag.VERSION,
                                     messageString.substring(12,14).trim());
        if((str=messageString.substring(14).trim()).length() > 0)
        {     //text data exists; put it in as Text element
          addElemContent(commentElem,MsgTag.TEXT,Text.normalizeString(str));
              //set action attribute to "Update":
          actionAttribObj.setValue(MsgTag.UPDATE);
              //add "Update" Action element to Comment element:
          addElemContent(commentElem,MsgTag.ACTION,MsgTag.UPDATE);
        }
        else
        {     //no text data; set action attribute to "Delete"
          actionAttribObj.setValue(MsgTag.DELETE);
              //add "Delete" Action element to Comment element:
          addElemContent(commentElem,MsgTag.ACTION,MsgTag.DELETE);
        }
              //add Comment element as child to Event element:
        eventElem.addContent(commentElem);
        break;

      case LINK_MSGIDX:      //Link to Add-on
        if(msgStrLen < 15)
        {     //not enough characters for addon-type value; set error msg
          setErrorMessage("Not enough characters in Link to Addon message");
          return null;
        }
              //create Product element:
        final Element prodLinkElem =
                              new Element(MsgTag.PRODUCT_LINK,namespaceObj);
              //process "type url comment" string in addon message:
        int ePos, sPos = 14;
              //scan through any leading whitespace:
        while(Character.isWhitespace(messageString.charAt(sPos)) &&
                                                        ++sPos < msgStrLen);
              //find next whitespace separator after data:
        if((ePos=UtilFns.indexOfWhitespace(messageString,sPos)) < 0)
          ePos = msgStrLen;       //if not found then use end of string pos
                   //get addon-type code string:
        final String prodCodeStr = messageString.substring(sPos,ePos).trim();
                             //translate CUBE addon-type code to QW product
        String prodTypeStr;  // code, or "LinkURL" if not matched:
        if((prodTypeStr=getProdTypeXlatEntry(prodCodeStr)) == null)
          prodTypeStr = MsgTag.LINK_URL;
        boolean prodUpdateFlag = true;     //true = update; false = delete
        String prodLinkStr = null, prodNoteStr = null;
        if((sPos=ePos+1) < msgStrLen)
        {     //more data in message string
                   //scan through any trailing whitespace:
          while(Character.isWhitespace(messageString.charAt(sPos)) &&
                                                        ++sPos < msgStrLen);
                   //find next whitespace separator after data:
          if((ePos=UtilFns.indexOfWhitespace(messageString,sPos)) < 0)
            ePos = msgStrLen;     //if not found then use end of string pos
                   //save addon-URL value:
          prodLinkStr = messageString.substring(sPos,ePos).trim();
          if((sPos=ePos+1) < msgStrLen)
          {   //more data in message string
            if((str=messageString.substring(sPos).trim()).length() > 0)
            {     //description text exists
              if(str.equalsIgnoreCase("delete")) //if "delete" then
                prodUpdateFlag = false;          //indicate delete Action
              else //action is not "delete"
              {         //save as "Note" value (take out any CR/LF chars):
                prodNoteStr = Text.normalizeString(str);
                if(prodCodeStr.equalsIgnoreCase(TYPE_FR_STR))
                {  //CUBE product type code is "fr"
                  if(str.indexOf(RTMECH_KEYSTR) >= 0)
                  {     //comment makes it a real-time focal mechanism URL
                    prodTypeStr = MsgTag.REALTIME_MECH_URL;
                  }
                  else if(str.indexOf(FEELIT_KEYSTR) >= 0)
                  {     //comment makes it a "Did you feel it?" URL
                    prodTypeStr = MsgTag.DIDYOU_FEELIT;
                  }
                }
              }
            }
          }
        }
              //add TypeKey element to ProductLink element:
        addElemContent(prodLinkElem,MsgTag.TYPE_KEY,prodTypeStr);
              //add Version element to ProductLink element:
        addElemContent(prodLinkElem,MsgTag.VERSION,
                                     messageString.substring(12,14).trim());
        if(prodUpdateFlag)
        {     //action is "update"
                   //set action attribute to "Update":
          actionAttribObj.setValue(MsgTag.UPDATE);
                   //add "Update" Action element to ProductLink element:
          addElemContent(prodLinkElem,MsgTag.ACTION,MsgTag.UPDATE);
        }
        else
        {     //action is "delete"
                   //set action attribute to "Delete":
          actionAttribObj.setValue(MsgTag.DELETE);
                   //add "Delete" Action element to ProductLink element:
          addElemContent(prodLinkElem,MsgTag.ACTION,MsgTag.DELETE);
        }
              //add Code element (set to addon-type value):
        addElemContent(prodLinkElem,MsgTag.CODE,prodCodeStr);
        if(prodNoteStr != null)   //if comment text then add Note element
          addElemContent(prodLinkElem,MsgTag.NOTE,prodNoteStr);
        if(prodLinkStr != null)   //if URL text then add Link element
          addElemContent(prodLinkElem,MsgTag.LINK,prodLinkStr);
              //add ProductLink element as child to Event element:
        eventElem.addContent(prodLinkElem);
        break;

      case TRUMP_MSGIDX:  //Trump event
              //set action attribute to "Trump":
        actionAttribObj.setValue(MsgTag.TRUMP);
        msgCommentStr = UtilFns.EMPTY_STRING;
        if(msgStrLen > 12)
        {     //version data exists; add Version element to Event element
          addElemContent(eventElem,MsgTag.VERSION,
                                             messageString.substring(12,13));
          if(msgStrLen > 14)      //if comment data then save it
            msgCommentStr = messageString.substring(14).trim();
        }
              //add "Trump" Action element to Event element:
        addElemContent(eventElem,MsgTag.ACTION,MsgTag.TRUMP);
        if(msgCommentStr.length() > 0)
        {     //comment data available; put into 'Comment' element
          commentElem = new Element(MsgTag.COMMENT,namespaceObj);
          addElemContent(commentElem,MsgTag.TYPE_KEY,MsgTag.INFO);
          addElemContent(commentElem,MsgTag.TEXT,msgCommentStr);
          eventElem.addContent(commentElem);     //add to 'Event' element
        }
        break;

      default:
        setErrorMessage("Unexpected message type (\"" +
                                      messageString.substring(0,2) + "\")");
        return null;
    }

    return eqMessageElem;         //return generated elements
  }

  /**
   * Creates a new element object with the given name and content string.
   * @param nameStr name string to use.
   * @param contentStr content string to use.
   * @return A new 'Element' object.
   */
  protected Element createElemObj(String nameStr, String contentStr)
  {
    final Element elemObj = new Element(nameStr,namespaceObj);
    elemObj.setText(contentStr);
    return elemObj;
  }

  /**
   * Creates a new element object with the given name and content string
   * and adds it to the given parent element.
   * @param parentElem parent element object to use.
   * @param nameStr name string to use.
   * @param contentStr content string to use.
   * @return The newly-created element.
   */
  protected Element addElemContent(Element parentElem, String nameStr,
                                                          String contentStr)
  {
    final Element elemObj;
    parentElem.addContent(elemObj=createElemObj(nameStr,contentStr));
    return elemObj;
  }

  /**
   * Translates the given CUBE-format magnitude-type code (i.e. "W")
   * to its standard type code (i.e. "Mw").
   * @param codeStr CUBE-format magnitude-type code string.
   * @return The converted magnitude-type code string, or the given code
   * string if it could not be converted.
   */
  public String translateCubeMagTypeCode(String codeStr)
  {
    String typeStr;
    return (codeStr != null &&
            (typeStr=CubeCodeTranslator.fromCubeMagType(codeStr)) != null) ?
                                                          typeStr : codeStr;
  }

  /**
   * Translates the given CUBE-format location-method code (i.e. "H")
   * to its standard algorithm (i.e. "Hypoinverse") and class (i.e.
   * "Geiger's Method & Least Squares") descriptions.
   * @param codeStr CUBE-format location-method code string.
   * @return A 'TwoStringBlock' object holding the algorithm and
   * class description strings.
   */
  public TwoStringBlock translateCubeLocMethodCode(String codeStr)
  {
    TwoStringBlock blockObj;
    return (codeStr != null &&
         (blockObj=CubeCodeTranslator.fromCubeLocMethod(codeStr)) != null) ?
      blockObj : new TwoStringBlock(codeStr,CubeCodeTranslator.UNKNOWN_STR);
  }
}
