//QWRelayFeeder.java:  QuakeWatch feeder plugin module that receives
//                     messages from a QuakeWatch server.
//
//  11/6/2003 -- [ET]  Initial version.
// 11/19/2003 -- [ET]  Added 'closeLogFile()' to 'stopFeeder()' method.
// 12/19/2003 -- [ET]  Added support for relaying event domain, type
//                     and name attributes in messages; added support
//                     for rejecting messages from parent QWServer and
//                     for 'ServerAddress'/'RelayServerAddr' attributes.
//   1/8/2004 -- [ET]  Modified to extend 'QWAbstractFeeder' class and
//                     support feeder domain/type names for events;
//                     took out 'QWRelayMsgNumTimeRec' class and call
//                     to 'QWMessageHandler.setLastMsgRecordObj()'.
//  2/16/2004 -- [ET]  Modified to use local date-format object in a
//                     thread-safe manner.
//  11/3/2004 -- [ET]  Version 1.2:  Modified to 'processFeederSettings()'
//                     to leave alternate servers enabled as the default;
//                     modified to call 'setupConnInfoProps()'; modified
//                     'getLastEventInStorage()' to return the "standard"
//                     message-number and time-generated values for the
//                     last event if the "relay' versions are not available.
//  11/4/2004 -- [ET]  Modified 'processDataMessage()' to always pass
//                     'timeGenDateObj' parameter into 'dataEvent()'
//                     method.
// 11/12/2004 -- [ET]  Modified to enter server-Acceptor object's
//                     identifier into the 'setAcceptorRejectIDStr()'
//                     method of 'QWConnectionManager' to prevent
//                     relay feeder from connecting to its own QWServer.
//  1/14/2005 -- [ET]  Modified to send parameters 'feederSourceHostStr'
//                     and 'feederSrcHostMsgIdNum' to 'dataEvent()' call.
//   2/2/2005 -- [ET]  Modified 'processFeederSettings()' method to convert
//                     paths via 'QWServer.fromBaseDir()'.
//   4/7/2005 -- [ET]  Fixed 'QWRelayMsgProcessor.processDataMessage()'
//                     method to make it actually discard messages whose
//                     source is the feeder's own server, and fixed to
//                     prevent extraneous "Error parsing 'SrcHostMsgID'"
//                     messages.
//  4/28/2005 -- [ET]  Fixed relay of feeder-data-source host name and
//                     message number.
// 10/18/2006 -- [ET]  Version 1.3:  Modified to preprocess cached
//                     messages to track feeder-data-source values;
//                     modified 'stopFeeder()' method to do work via
//                     a separate thread.
//  11/1/2006 -- [ET]  Version 1.31:  Added 'logObj'==null check to
//                     'stopFeeder()' method.
// 11/14/2006 -- [ET]  Version 1.4:  Modified to track last-stored
//                     message for given feeder during preprocess of
//                     cached messages and to return message via
//                     'QWRelayMsgProcessor.getLastEventInStorage()'
//                     method.
//  12/1/2006 -- [ET]  Version 1.5:  Modified to call connection-manager
//                     method 'performExitCleanup()' on feeder-stop.
//  1/22/2010 -- [ET]  Version 1.6:  Modified to set and check
//                     'RelayFirstSvrID' / 'RelayFirstSvrAddr'
//                     attributes in messages (to help prevent
//                     message feedback between cross-connected
//                     servers).
//  8/23/2010 -- [ET]  Version 1.7:  Modified processing of cache messages
//                     in 'run()' method to avoid queuing.
//  11/4/2010 -- [ET]  Version 1.8:  Added 'getStatusErrorMsg()' method
//                     with check for last-received status/alive message;
//                     added call to 'updateLastDataMsgTime()'.
//

package com.isti.quakewatch.server;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.text.DateFormat;
import org.jdom.Element;
import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.util.CfgPropItem;
import com.isti.quakewatch.util.QWUtils;
import com.isti.quakewatch.util.QWConnProperties;
import com.isti.quakewatch.util.QWConnectionMgr;
import com.isti.quakewatch.util.ConnectRejectCallBack;
import com.isti.quakewatch.util.QWAliveMsgListener;
import com.isti.quakewatch.common.MsgTag;
import com.isti.quakewatch.message.QWMessageHandler;
import com.isti.quakewatch.message.QWMsgNumTimeRec;
import com.isti.quakewatch.message.QWDataMsgProcessor;
import com.isti.quakewatch.message.QWStatusMsgRecord;

/**
 * Class QWRelayFeeder is a QuakeWatch feeder plugin module that receives
 * messages from a QuakeWatch server.
 */
public class QWRelayFeeder extends QWAbstractFeeder
                         implements ConnectRejectCallBack,QWAliveMsgListener
{
  public static final String MODULE_NAME = "QWRelayFeeder";
  public static final String VERSION_NUM = "1.8";    //name and version strs
  public static final String REVISION_STRING = MODULE_NAME + ", " +
                                                   "Version " + VERSION_NUM;
  public static final String FEEDER_NAME = "QWRelay";      //feeder name
         //default distribution name for 'DISTRIB_NAME' conn-info-property:
  public static final String DEF_DISTRIB_NAME = "QWRelay_default";
         //default name for optional server-login-information file:
  public static final String DEF_LOGININFO_FNAME =
                                                 "conf/ServerLoginInfo.dat";
         //default value for maximum-server-event-age property (in days):
  protected static final int DEF_MAXSERVEREVTAGE_DAYS = 15;
  protected QWConnectionMgr connectMgrObj = null;
  protected boolean callBackErrMsgFlag = false;
  protected HashMap lastStoredMessagesTable = null;
  protected int serverAliveLimitSecs = 120;
  protected long lastServerAliveMsgTime = 0;
         //connection-properties object for feeder:
  protected final QWConnProperties connPropsObj = new QWConnProperties();
         //date-formatter object for 'processDataMessage()' method:
  private final DateFormat xmlDateFormatterObj =
                                           QWUtils.getXmlDateFormatterObj();


  /**
   * Constructs a QWRelay feeder module.
   */
  public QWRelayFeeder()
  {
    this(MODULE_NAME,FEEDER_NAME);     //set thread and feeder name
  }

  /**
   * Constructs a QWRelay feeder module.
   * @param modulename the name of the module.
   * @param feederName the name of the feeder.
   */
  public QWRelayFeeder(String modulename, String feederName)
  {
    super(modulename);             //set thread name to the module name
    this.feederName = feederName;  //set the feeder name
  }

  /**
   * Returns the revision String for the feeder.
   * @return the revision String for the feeder.
   */
  public String getRevisionString()
  {
    return REVISION_STRING;
  }

  /**
   * Processes feeder-specific settings.  The settings are usually
   * fetched from the "QWFeederSettings" element in the QuakeWatch-server
   * configuration file.  (For 'QWFeederPlugin' interface.)
   * @param settingsStr The settings string to be processed.
   * @throws QWFeederInitException if an error occurs while processing
   * the settings.
   */
  public void processFeederSettings(String settingsStr)
                                                throws QWFeederInitException
  {
         //enter default max-server-event-age value:
    final CfgPropItem itemObj;
    if((itemObj=connPropsObj.getMaxServerEventAgeDaysProp()) != null)
    {
      itemObj.setDefaultAndValue(
                              Double.valueOf((double)DEF_MAXSERVEREVTAGE_DAYS));
    }
         //enter default login-info-file-name value:
    connPropsObj.loginInfoFileNameProp.setDefaultAndValue(
                                                       DEF_LOGININFO_FNAME);
    String msgStr;
    try
    {         //load configuration properties from the given string:
      if(connPropsObj.getConfigProps().load(settingsStr))
      {  //successfully loaded settings
        if(QWServer.isServerBaseDirectorySetup())
        {     //server-base directory is setup for server
                   //modify paths to make them relative to server-base dir:
          connPropsObj.loginInfoFileNameProp.setValue(QWServer.fromBaseDir(
                         connPropsObj.loginInfoFileNameProp.stringValue()));
          connPropsObj.coaFileProp.setValue(QWServer.fromBaseDir(
                                   connPropsObj.coaFileProp.stringValue()));
          connPropsObj.crlFileProp.setValue(QWServer.fromBaseDir(
                                   connPropsObj.crlFileProp.stringValue()));
        }
        return;
      }
         //error loading settings; get error message
      msgStr = connPropsObj.getConfigProps().getErrorMessage();
    }
    catch(Exception ex)
    {    //some kind of exception error; build error message
      msgStr = "Error processing settings:  " + ex;
    }
         //throw feeder exception with error message:
    throw new QWFeederInitException(msgStr);
  }

  /**
   * Stops the message collection threads of the feeder.  The work
   * is done via a separate thread.  (For 'QWFeederPlugin' interface.)
   */
  public void stopFeeder()
  {
    (new Thread("stop-" + getFeederName())
        {          //create separate thread to do "stop" work
          public void run()
          {
            if(logObj != null)
              logObj.debug2("Feeder stop requested");
            if(connectMgrObj != null)
            {  //connection manager object is OK
                   //remove this status/alive-message listener:
              connectMgrObj.removeHdlrAliveMsgListener(QWRelayFeeder.this);
              connectMgrObj.closeConnection();        //close connection
              connectMgrObj.performExitCleanup();     //do mgr exit cleanup
            }
            QWRelayFeeder.super.stopFeeder();    //call parent method
          }                                      // (closes log file)
        }).start();          //start thread
  }

  /**
   * Returns the current status-error message (if any) for the feeder.
   * (For 'QWFeederPlugin' interface.)
   * @return The current status-error message for the feeder, or null
   * if none.
   */
  public String getStatusErrorMsg()
  {
    final long diff;
    if(serverAliveLimitSecs > 0 && lastServerAliveMsgTime > 0 &&
                  (diff=System.currentTimeMillis()-lastServerAliveMsgTime) >
                                          (long)serverAliveLimitSecs * 1000)
    {  //non-zero limit setup, at least one server-alive message received
       // and time since last server-alive message is past limit
      return "Not receiving 'alive' messages from source server (last msg " +
                                            + (diff/1000) + " seconds ago)";
    }
    return null;
  }

  /**
   * Called when a new server-alive message has been received.
   * @param recObj the 'QWStatusMsgRecord' object for the received message,
   * or null if none available.
   */
  public void aliveMsgReceived(QWStatusMsgRecord recObj)
  {
    lastServerAliveMsgTime = System.currentTimeMillis();
  }

  /**
   * Executing method for the feeder thread.  Starts execution of QWRelay
   * threads.
   */
  public void run()
  {
    if(logObj == null)  //if no log file setup then create dummy object
      logObj = new LogFile(null,LogFile.NO_MSGS,LogFile.ERROR);
              //create message-processor object:
    final QWRelayMsgProcessor msgProcessorObj = new QWRelayMsgProcessor();
              //setup parameter arguments array for connection-manager:
    final String [] paramArgsArr = (startupParamsStr != null) ?
                     (new String [] { startupParamsStr }) : (new String[0]);
              //create connection-manager object:
    connectMgrObj = new QWConnectionMgr(
                          paramArgsArr,connPropsObj,msgProcessorObj,logObj);
              //setup client-connection-information properties string
              // to be sent when connecting to server:
    connectMgrObj.setupConnInfoProps(MODULE_NAME,VERSION_NUM,
                     UtilFns.timeMillisToString(System.currentTimeMillis()),
                                                          DEF_DISTRIB_NAME);
              //setup limit for time allowed between server-alive messages
              // detected before status error flagged (default 120 seconds):
    serverAliveLimitSecs =
                         connPropsObj.maxServerAliveSecProp.intValue() * 15;
              //add this status/alive-message listener:
    connectMgrObj.addHdlrAliveMsgListener(this);
    if(callbackObj != null)
    {    //server-callback OK
      final HashMap msgsTable = new HashMap();   //create last-stored table
      try     //run all messages in cache through feeder's message handler
      {       // to enter feeder-data-src tracking values for handler:
        final QWMessageHandler handlerObj;  //get msg handler for feeder:
        if((handlerObj=connectMgrObj.getMsgHandlerObj()) != null)
        {     //message-handler object OK
                        //prevent msgs being fed back into server-callback:
          msgProcessorObj.setProcessDataEnableFlag(false);
          final Iterator iterObj =     //get cache vector and iterator:
                             callbackObj.getAllServerCacheMsgs().iterator();
          logObj.debug(
                      "Starting relay-feeder processing of cache messages");
          QWServerMsgRecord recObj;
          while(iterObj.hasNext())
          {   //for each message-record object in cache
            recObj = (QWServerMsgRecord)(iterObj.next());
                   //process message string (don't allow queuing):
            handlerObj.processMessage((String)(recObj.getDataObj()),false);
                   //save record in last-stored table (key = relay svr ID):
            msgsTable.put(recObj.getRelayServerID(),recObj);
          }
          lastStoredMessagesTable = msgsTable;   //save table
          logObj.debug(
                      "Finished relay-feeder processing of cache messages");
        }
      }
      catch(Exception ex)
      {  //some kind of exception error; log it
        logObj.warning(MODULE_NAME +
                       ":  Error processing server-cache messages:  " + ex);
        logObj.warning(UtilFns.getStackTraceString(ex));
      }
      msgProcessorObj.setProcessDataEnableFlag(true); //re-enable processing
              //set Acceptor-reject ID to correspond to this server's
              // Acceptor so that a circular connection will be rejected:
      connectMgrObj.setAcceptorRejectIDStr(
                                    callbackObj.getQWAcceptorImplIDString(),
                                "QWRelayFeeder source is its own QWServer");
    }
              //setup 'connectAttemptRejected()' to be invoked if
              // the connect-attempt is rejected by the server:
    connectMgrObj.setConnectAttemptRejectCallBackObj(this);
    logObj.info("Initializing connection to server");
    connectMgrObj.initializeConnection();        //connect to server
  }

  /**
   * Call-back method invoked when a connect attempt reaches the server
   * but is rejected because of obsolete version, invalid login, or
   * some other error.  The method implements the 'ConnectRejectCallBack'
   * interface.
   * @param connStatusVal a value indicating the status of the current
   * connection to the server, one of the 'QWServices.CS_' values.
   * @param connStatusStr a message-string describing the status of
   * the current connection to the server.
   */
  public void connectAttemptRejected(int connStatusVal,String connStatusStr)
  {
    if(callbackObj != null)
    {    //server-callback OK; give message and terminate server
      callbackObj.terminateServer(9,
                     "Unable to connect QWRelayFeeder to source server:  " +
                                                             connStatusStr);
    }
    else
    {    //server-callback not setup; log warning message
      logObj.warning(MODULE_NAME +
                ":  Connect-to-server attempt rejected:  " + connStatusStr);
    }
  }


  /**
   * Class QWRelayMsgProcessor handles the processing of received
   * event-data messages.
   */
  protected class QWRelayMsgProcessor implements QWDataMsgProcessor
  {
              //flag set true to enable 'processDataMessage()' method:
    private boolean processDataEnableFlag = true;
              //tracker for received data message data:
    private String dataMessageStringTracker = "";

    /**
     * Processes any number of "Event" and "Product" elements in the given
     * "DataMessage" element.  This method is needed to implement the
     * 'QWDataMsgProcessor' interface.
     * @param qwMsgElement The "QWmessage" element.
     * @param dataMsgElement The "DataMessage" element.
     * @param xmlMsgStr the XML text message string.
     * @param requestedFlag true to set the "requested" flag on the generated
     * data-message objects (to indicate that they should not be processed as
     * a "real-time" message).
     */
    public void processDataMessage(Element qwMsgElement,
              Element dataMsgElement,String xmlMsgStr,boolean requestedFlag)
    {
      if(!processDataEnableFlag)       //if enable flag not set then
        return;                        //abort method
      try
      {
        updateLastDataMsgTime();       //mark time of data message received
                   //clear last-stored-msg table for feeder:
        lastStoredMessagesTable = null;
        if(callbackObj == null)
        {  //callback object handle null
          if(!callBackErrMsgFlag)
          {     //show error message (once)
            logObj.warning(MODULE_NAME + " (\"" + feederName +
                                           "\"):  Callback object not set");
            callBackErrMsgFlag = true;
          }
          return;
        }

              //check if message is a duplicate of the previous message:
        if(xmlMsgStr != null && xmlMsgStr.length() > 0)
        {     //given message string contains data
          if(xmlMsgStr.equals(dataMessageStringTracker))
          {   //message string data same as previous; log warning
            logObj.warning(MODULE_NAME + ":  Data message is duplicate " +
                              "of previous--discarded; msg:  " + xmlMsgStr);
            return;               //exit method
          }
                   //save message data string for next iteration:
          dataMessageStringTracker = xmlMsgStr;
        }
        else  //no data in given message string
          dataMessageStringTracker = "";    //clear message string tracker
//        logObj.info("Received data message:  " + xmlMsgStr);

              //check if relay versions of server ID and address values
              // are the same as those for the parent QWServer:
        String rlyServerIDStr,rlyServerAddrStr;
        if((rlyServerIDStr=qwMsgElement.getAttributeValue(
                                         MsgTag.RELAY_SERVER_ID)) != null &&
                           (rlyServerAddrStr=qwMsgElement.getAttributeValue(
                                       MsgTag.RELAY_SERVER_ADDR)) != null &&
                     rlyServerIDStr.equals(callbackObj.getServerIdName()) &&
                rlyServerAddrStr.equals(callbackObj.getServerHostAddress()))
        {     //relay server ID and address same as parent QWServer
          logObj.debug3(
                  "Message relay-source is this server; message discarded");
          logObj.debug5("  msg:  " + xmlMsgStr);
          return;                 //exit method
        }

              //check if QWmessage versions of server ID and address values
              // are the same as those for the parent QWServer:
        final String serverIDStr =          //get server ID string
                      qwMsgElement.getAttributeValue(MsgTag.SERVER_ID_NAME);
        final String serverAddrStr =        //get server host address
                      qwMsgElement.getAttributeValue(MsgTag.SERVER_ADDRESS);
        if(serverIDStr != null && serverAddrStr != null &&
                        serverIDStr.equals(callbackObj.getServerIdName()) &&
                   serverAddrStr.equals(callbackObj.getServerHostAddress()))
        {     //server ID and address same as parent QWServer
          logObj.debug3("Message source is this server; message discarded");
          logObj.debug5("  msg:  " + xmlMsgStr);
          return;                 //exit method
        }

              //check if relay "first" versions of server ID and address
              // values are the same as those for the parent QWServer:
        if((rlyServerIDStr=dataMsgElement.getAttributeValue(
                                       MsgTag.RELAY_FIRSTSVR_ID)) != null &&
                         (rlyServerAddrStr=dataMsgElement.getAttributeValue(
                                     MsgTag.RELAY_FIRSTSVR_ADDR)) != null &&
                     rlyServerIDStr.equals(callbackObj.getServerIdName()) &&
                rlyServerAddrStr.equals(callbackObj.getServerHostAddress()))
        {     //relay first-server ID and address same as parent QWServer
          logObj.debug3(
            "Message relay-first-source is this server; message discarded");
          logObj.debug5("  msg:  " + xmlMsgStr);
          return;                 //exit method
        }

              //copy several attributes from the "QWmessage" element into
              // "Relay..." attributes in the "DataMessage" element:
        final String msgNumStr;     //copy "MsgNumber" to "RelayMsgNum":
        if((msgNumStr=qwMsgElement.getAttributeValue(MsgTag.MSG_NUMBER)) !=
                                                                       null)
        {
          dataMsgElement.setAttribute(MsgTag.RELAY_MSG_NUM,msgNumStr);
        }
        final String timeGenStr;  //copy "TimeGenerated" to "RelayTimeGen":
        if((timeGenStr=qwMsgElement.getAttributeValue(MsgTag.TIME_GENERATED))
                                                                    != null)
        {
          dataMsgElement.setAttribute(MsgTag.RELAY_TIME_GEN,timeGenStr);
        }
                             //copy "ServerIDName" to "RelayServerID":
        if(serverIDStr != null)
          dataMsgElement.setAttribute(MsgTag.RELAY_SERVER_ID,serverIDStr);
                             //copy "ServerAddress" to "RelayServerAddr":
        if(serverAddrStr != null)
          dataMsgElement.setAttribute(MsgTag.RELAY_SERVER_ADDR,serverAddrStr);
        String str;          //copy "MsgSource" to "RelayMsgSrc":
        if((str=qwMsgElement.getAttributeValue(MsgTag.MSG_SOURCE)) != null)
          dataMsgElement.setAttribute(MsgTag.RELAY_MSG_SRC,str);
              //check/set 'RelayFirstSvrID'/'RelayFirstSvrAddr' attribs:
        if(serverIDStr != null)
        {  //"ServerIDName" found OK
          if(dataMsgElement.getAttribute(MsgTag.RELAY_FIRSTSVR_ID) == null)
          {  //no previous "RelayFirstSvrID"; put in "ServerIDName" value
            dataMsgElement.setAttribute(
                                      MsgTag.RELAY_FIRSTSVR_ID,serverIDStr);
          }
        }
        if(serverAddrStr != null)
        {  //"ServerAddress" found OK
          if(dataMsgElement.getAttribute(MsgTag.RELAY_FIRSTSVR_ADDR) == null)
          {  //no previous "RelayFirstSvrAddr"; put in "ServerAddress" value
            dataMsgElement.setAttribute(
                                  MsgTag.RELAY_FIRSTSVR_ADDR,serverAddrStr);
          }
        }

              //parse time-generated string into a date object:
        final Date timeGenDateObj = parseXmlDateStr(timeGenStr);

              //fetch event domain, type and name values (if available):
        String domainNameStr =
                      qwMsgElement.getAttributeValue(MsgTag.MSG_EVT_DOMAIN);
        String typeNameStr =
                        qwMsgElement.getAttributeValue(MsgTag.MSG_EVT_TYPE);
        String eventNameStr =
                        qwMsgElement.getAttributeValue(MsgTag.MSG_EVT_NAME);
        if(domainNameStr == null && typeNameStr == null &&
                                                       eventNameStr == null)
        {     //no event domain/type/name attributes in message
          domainNameStr = feederDomainNameStr;   //use feeder domain name
          typeNameStr = feederTypeNameStr;       //use feeder type name
        }

              //fetch feeder-data-source host name and message number:
        String sourceHostStr;
        long srcHostMsgIDNum;
        if((sourceHostStr=qwMsgElement.getAttributeValue(
                                           MsgTag.FDR_SOURCE_HOST)) != null)
        {     //feeder-data-source host name was found
          final String srcHostMsgIDNumStr =
                   qwMsgElement.getAttributeValue(MsgTag.FDR_SOURCE_MSGNUM);
          if(srcHostMsgIDNumStr != null &&
                                     srcHostMsgIDNumStr.trim().length() > 0)
          {   //feeder-data-source message number was found
            try
            {           //parse numeric message-number string to integer:
              srcHostMsgIDNum = Long.parseLong(srcHostMsgIDNumStr);
            }
            catch(NumberFormatException ex)
            {      //error parsing numeric message-number string to integer
              logObj.warning(MODULE_NAME + ":  Error parsing \"" +
                                                  MsgTag.FDR_SOURCE_MSGNUM +
                                          "\" attribute as an integer (\"" +
                                                srcHostMsgIDNumStr + "\")");
              logObj.debug("  msg:  " + xmlMsgStr);
              sourceHostStr = null;    //clear data-source host name string
              srcHostMsgIDNum = 0;     //clear data-source message number
            }
          }
          else
          {   //feeder-data-source message number not found
            logObj.debug2("Attribute \"" + MsgTag.FDR_SOURCE_HOST +
                          "\" found without \"" + MsgTag.FDR_SOURCE_MSGNUM +
                                                            "\" attribute");
            logObj.debug3("  msg:  " + xmlMsgStr);
            sourceHostStr = null;      //clear data-source host name string
            srcHostMsgIDNum = 0;       //clear data-source message number
          }
        }
        else       //data-source host name not found
          srcHostMsgIDNum = 0;         //indicate no data-source msgNum

              //detach "DataMessage" element from "QWmessage" element so that
              // it can later be attached to a different "QWmessage" element:
        dataMsgElement.detach();

              //pass event message on to server, via call-back:
        callbackObj.dataEvent(domainNameStr,typeNameStr,eventNameStr,
               dataMsgElement,timeGenDateObj,sourceHostStr,srcHostMsgIDNum);
      }
      catch(Exception ex)
      {  //some kind of exception error; log it
        logObj.warning(MODULE_NAME +
                            ":  Error processing received message:  " + ex);
        logObj.warning(UtilFns.getStackTraceString(ex));
      }
    }

    /**
     * Enters the connection manager to be used by this message processor.
     * This method is needed to implement the 'QWDataMsgProcessor'
     * interface.
     * @param connMgrObj The connection-manager object to use.
     */
    public void setConnectionMgr(QWConnectionMgr connMgrObj)
    {
    }

    /**
     * Returns the "Relay" message-number and time-generated values for
     * the last event held in storage.  This method is needed
     * to implement the 'QWDataMsgProcessor' interface.
     * @return The 'QWMsgNumTimeRec' object for the last event held in
     * storage, or null if no events are available.
     */
    public QWMsgNumTimeRec getLastEventInStorage()
    {
      QWServerMsgRecord recObj;
      QWMsgNumTimeRec msgNumTimeRecObj;
      final String idStr;
              //use local handle to table to be more thread safe:
      final HashMap localMsgsTable = lastStoredMessagesTable;
      if(localMsgsTable != null && connectMgrObj != null &&
                       (idStr=connectMgrObj.getServerIDNameStr()) != null &&
                                                         idStr.length() > 0)
      {  //last-stored messages table OK and server-ID name available
        final Object obj;    //if matching server-ID-name key in table then
                             // return associated message object info:
        if((obj=localMsgsTable.get(idStr)) instanceof QWServerMsgRecord)
        {
          recObj = (QWServerMsgRecord)obj;
          msgNumTimeRecObj = recObj.getRelayMsgNumTimeRec();
          logObj.debug("Message in 'lastStoredMessagesTable' matched " +
                                                "for server-ID \"" + idStr +
                                         "\" in 'getLastEventInStorage()'");
          logObj.debug("  msgNum=" + recObj.getMsgNum() + ", timeGen=" +
                   recObj.getTimeGenerated() + ((msgNumTimeRecObj != null) ?
                          (", relayMsgNum=" + msgNumTimeRecObj.getMsgNum() +
                  ", relayTimeGen=" + msgNumTimeRecObj.getTimeGenerated()) :
                                                     UtilFns.EMPTY_STRING));
          return msgNumTimeRecObj;
        }
      }
              //get last-stored message from server's cached-archive
              // and return its info (if available):
      recObj = null;
      msgNumTimeRecObj = (callbackObj != null &&
                       (recObj=callbackObj.getLastStoredMsgObj()) != null) ?
                                    recObj.getRelayMsgNumTimeRec() : recObj;
      if(recObj != null)
      {
        logObj.debug("Message fetched from cached-archive in " +
                                               "'getLastEventInStorage()'");
        logObj.debug("  msgNum=" + recObj.getMsgNum() + ", timeGen=" +
                   recObj.getTimeGenerated() + ((msgNumTimeRecObj != null) ?
                          (", relayMsgNum=" + msgNumTimeRecObj.getMsgNum() +
                  ", relayTimeGen=" + msgNumTimeRecObj.getTimeGenerated()) :
                                                     UtilFns.EMPTY_STRING));
      }
      return msgNumTimeRecObj;
    }

    /**
     * Parses the given string as an XML date string.  This method is
     * thread safe.
     * @param str the date string to parse.
     * @return A new 'Date' object, or null if the string could not be
     * parsed as an XML date string.
     */
    private Date parseXmlDateStr(String str)
    {
      synchronized(xmlDateFormatterObj)
      {
        try
        {     //parse date/time string into 'Date' object and return it:
          return xmlDateFormatterObj.parse(str);
        }
        catch(Exception ex)
        {     //error parsing
          return null;
        }
      }
    }

    /**
     * Sets the enabled status of the 'processDataMessage()' method.
     * @param flgVal true to enable the 'processDataMessage()' method;
     * false to disable it.
     */
    public void setProcessDataEnableFlag(boolean flgVal)
    {
      processDataEnableFlag = flgVal;
    }
  }
}
