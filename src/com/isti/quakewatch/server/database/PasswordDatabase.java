//PasswordDatabase.java:  Implements a password database connection.
//
//  8/13/2004 -- [KF]  Initial version.
//  9/22/2004 -- [KF]  Updated for new database.
//

package com.isti.quakewatch.server.database;

import java.util.*;
import com.isti.util.database.BasicDatabase;
import com.isti.util.database.ConnectionJDBC;
import com.isti.util.database.DatabaseUtil;

/**
 *  Class PasswordDatabase implements a password database connection.
 */
public class PasswordDatabase extends BasicDatabase
{
  /** The username column. */
  protected final static String COLUMN_USERNAME = "login";
  /** The password column. */
  protected final static String COLUMN_PASSWORD = "password";
  /** The status column. */
  protected final static String COLUMN_STATUS = "status";
  /** Active status. */
  public final static String STATUS_ACTIVE = "ACTIVE";

  /**
   * The column names to use in query or empty for all
   */
  protected final static String[] COLUMN_NAMES =
  {
    COLUMN_PASSWORD,
    COLUMN_STATUS
  };

  /**
  * Creates a password database connection.
  * @param dbConn database connection
  */
  public PasswordDatabase(ConnectionJDBC dbConn)
  {
    super(dbConn);
  }

  /**
   * Gets the password entry from the result map.
   * @param map data map
   * @returns the password.
   * @exception  NullPointerException  if the value was not found.
   * @see getMap
   */
  public String getPassword(Map map)
  {
    return DatabaseUtil.getString(map, COLUMN_PASSWORD);
  }

  /**
   * Gets the status entry from the result map.
   * @param map data map
   * @return the status entry.
   * @exception  NullPointerException  if the value was not found.
   * @see getMap
   */
  public String getStatus(Map map)
  {
    return DatabaseUtil.getString(map, COLUMN_STATUS);
  }

  /**
   * Gets the table name.
   * @return the table name.
   */
  public String getTableName()
  {
    return "user";
  }

  /**
   * Gets the column names to use in query
   * @return the column names to use in query or empty for all
   */
  public String[] getColumnNames()
  {
    return COLUMN_NAMES;
  }

  /**
   * @return the where clause
   * @param text the text for the entire where clause.
   * @param usernameText the user name text.
   */
  public String getWhereClause(String text, String usernameText)
  {
    return DatabaseUtil.getWhereClause(
        text, COLUMN_USERNAME, usernameText, false);
  }

  /**
   * Gets the sql statement for a query.
   * @param usernameText the user name text.
   * @return the sql statement
   *
   * @see executeQuery
   */
  public String getSqlStatement(String usernameText)
  {
    String selectText = getSelectClause(EMPTY_TEXT);
    String fromText = getFromClause(EMPTY_TEXT);
    String whereText = getWhereClause(EMPTY_TEXT, usernameText);
    String orderByText = getOrderByClause(EMPTY_TEXT);

    String sql = DatabaseUtil.combineClauses(
        selectText, fromText, whereText, orderByText);
    return sql;
  }
}
