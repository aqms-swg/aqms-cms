//FeederParserHandler.java:  Handles the instantiation and use of
//                           a message parser to be used with a
//                           QuakeWatch-feeder module.

package com.isti.quakewatch.server;

import com.isti.util.ErrorMessageMgr;
import com.isti.util.CfgPropItem;
import org.jdom.Element;

/**
 * Class FeederParserHandler handles the instantiation and use of a
 * message parser to be used with a QuakeWatch-feeder module.
 */
public class FeederParserHandler extends ErrorMessageMgr
{
    /** Feeder-parser object. */
  protected MsgParserIntf feederParserObj = null;
    /** Configuration-property item for "feederParserClass". */
  protected final CfgPropItem feederParserClassProp =
                                    new CfgPropItem("feederParserClass","");

  /**
   * Returns the configuration-property item for "feederParserClass".
   * This item specifies the 'MsgParserIntf' class to be used.
   * @return The CfgPropItem object for "feederParserClass".
   */
  public CfgPropItem getFeederParserClassProp()
  {
    return feederParserClassProp;
  }

  /**
   * Determines whether or not a value for the configuration-property
   * item for "feederParserClass" was specified.
   * @return true if a value for the configuration-property item for
   * "feederParserClass" was specified; false if not.
   */
  public boolean isParserClassSpecified()
  {
    final String classStr;
    return ((classStr=feederParserClassProp.stringValue()) != null &&
                                              classStr.trim().length() > 0);
  }

  /**
   * Instantiates an instance of the feeder-parser object.
   * @return true if successful; false if not (in which case an error
   * message may be fetched via the 'getErrorMessageString()' method).
   */
  public boolean instantiateParserObj()
  {
    if(feederParserObj == null)
    {    //feeder-parser object not yet instantiated
      final String classStr;
      if((classStr=feederParserClassProp.stringValue()) != null &&
                                               classStr.trim().length() > 0)
      {  //feeder-parser class name cfgProp item not empty
        Object obj;
        try
        {     //load class object for string name:
          obj = Class.forName(classStr).getDeclaredConstructor().newInstance();
        }
        catch(Throwable ex)
        {     //error loading class object for String name:
          setErrorMessageString("Unable to load feeder-parser class \"" +
                                                   classStr + "\":  " + ex);
          return false;
        }
        if(!(obj instanceof MsgParserIntf))
        {     //class is not correct type
          setErrorMessageString("Specified feeder-parser class \"" +
              classStr + "\" does not implement 'MsgParserIntf' interface");
          return false;
        }
        feederParserObj = (MsgParserIntf)obj;    //set handle to object
      }
      else
      {  //feeder-parser class name cfgProp item is empty
        setErrorMessageString("Feeder-parser class not specified");
        return false;
      }
    }
    return true;
  }

  /**
   * Parses a message into a JDOM/XML tree of elements.  Before using
   * this method, the 'instantiateParserObj()' method must be successfully
   * executed.
   * @param messageString the message string to be parsed.
   * @param msgNum message number to be entered as "FeederMsgNum" in
   * the returned Element.
   * @param hostNameStr name of originating host to be entered as
   * "SourceHost" in the returned Element (or null for none).
   * @param hostMessageID message ID number to be entered as
   * "SrcHostMsgID" in the returned (if 'hostNameStr' not null).
   * @return The resulting "DataMessage" Element object, or null if an
   * error occured (in which case the associated error message may be
   * fetched via the 'getErrorMessageString()' method).
   */
  public Element parseFeederMessageStr(String messageString, long msgNum,
                                     String hostNameStr, long hostMessageID)

  {
    if(feederParserObj != null)
    {    //feeder-parser object instantiated
      final Element elemObj;           //parse msg via feeder-parser obj:
      if((elemObj=feederParserObj.parseMessageString(messageString,msgNum,
                                        hostNameStr,hostMessageID)) != null)
      {  //parse successful; return element object
        return elemObj;
      }
              //parse failed; return error message
      setErrorMessageString(feederParserObj.getErrorMessage());
    }
    else      //feeder-parser object not yet instantiated
      setErrorMessageString("Feeder-parser class not instantiated");
    return null;
  }
}
