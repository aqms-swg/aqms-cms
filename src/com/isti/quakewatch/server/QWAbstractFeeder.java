//QWAbstractFeeder.java:  Defines a basic set of framework methods used
//                        by QuakeWatch "feeder" modules to communicate
//                        with the server.
//
//  1/22/2004 -- [ET]  Initial version.
//  1/28/2005 -- [ET]  Added 'getCallbackObj()' method.
//   2/3/2006 -- [ET]  Added 'getLogFileObj()' method.
// 10/18/2006 -- [ET]  Added 'isFeederStopped()' method.
//  11/1/2006 -- [ET]  Modified to delay setup of log file until feeder
//                     is started.
// 12/15/2006 -- [ET]  Modified to setup log file via 'getLogFileObj()'
//                     method (if not already setup).
//  11/4/2010 -- [ET]  Added 'getStatusErrorMsg()', 'getLastDataMsgTime()'
//                     and 'updateLastDataMsgTime()' methods.
// 10/16/2020 -- [KF]  Added 'clearLastDataMsgTime()' method.
//

package com.isti.quakewatch.server;

import com.isti.util.LogFile;

/**
 * Class QWAbstractFeeder defines a basic set of framework methods used
 * by QuakeWatch "feeder" modules to communicate with the server.
 * Feeder-module classes should extend this class.
 */
public abstract class QWAbstractFeeder extends Thread
                                                   implements QWFeederPlugin
{
  protected String feederName = "feeder";        //feeder name
  protected String startupParamsStr = null;      //startup parameters
  protected QWServerCallback callbackObj = null; //server call-back obj
  protected volatile LogFile logObj = null;      //log file object
  protected String feederDomainNameStr = null;   //domain name for feeder
  protected String feederTypeNameStr = null;     //type name for feeder
  protected boolean feederNamesAvailFlag = false;     //true if names set
  protected boolean feederStoppedFlag = false;   //true after 'stopFeeder()'
  protected long lastDataMsgReceivedTime = 0;    //time last data msg recvd
  protected final Object lastDataMsgRecvdTimeSyncObj = new Object();
         //variables for log file setup:
  protected String logFileNameStr = null;
  protected int logFileLogLevel = LogFile.NO_MSGS;
  protected int logFileConsoleLevel = LogFile.NO_MSGS;
  protected boolean logFileUseDateInFnameFlag = true;
  protected int logFileSwitchIntervalDays = 1;
  protected int logFileMaxAgeDays = 30;

  /**
   * Constructs an abstract-feeder instance.
   */
  public QWAbstractFeeder()
  {
  }

  /**
   * Constructs an abstract-feeder instance.
   * @param threadNameStr the name use for the feeder thread.
   */
  public QWAbstractFeeder(String threadNameStr)
  {
    super(threadNameStr);
  }

  /**
   * Sets the name for this feeder.  The name is usually loaded from the
   * QuakeWatch configuration file.
   * @param str the name to use.
   */
  public void setFeederName(String str)
  {
    feederName = str;
  }

  /**
   * Returns the name for this feeder.  The name is usually loaded from the
   * QuakeWatch configuration file.
   * @return The feeder name.
   */
  public String getFeederName()
  {
    return feederName;
  }

  /**
   * Sets the startup parameters string for this feeder.  This string is
   * usually loaded from the QuakeWatch configuration file.
   * @param str the parameters string to use.
   */
  public void setStartupParamsStr(String str)
  {
    startupParamsStr = str;
  }

  /**
   * Returns the startup parameters string for this feeder.  This string
   * is usually loaded from the QuakeWatch configuration file, and then
   * passed along to the connection manager.
   * @return the parameters string.
   */
  public String getStartupParamsStr()
  {
    return startupParamsStr;
  }

  /**
   * Sets up the log file to be used with this feeder.  The log file is
   * not actually created until the 'startFeeder()' method is called.
   * @param logNameStr name of log file, or null for none.
   * @param logFileLevel message level for log file output.
   * @param consoleLevel message level for console output.
   * @param useDateInFnameFlag true to use the date in the file name (and
   * create a new log file everytime the date changes).
   * @param switchIntervalDays the minimum number of days between
   * log file switches via date changes.
   * @param maxLogFileAgeDays the maximum log file age, in days (if
   * 'useDateInFnameFlag' is true).
   */
  public void setupLogFile(String logNameStr,int logFileLevel,
                                int consoleLevel,boolean useDateInFnameFlag,
                        int switchIntervalDays,int maxLogFileAgeDays)
  {
    logFileNameStr = logNameStr;       //save setup values for log file
    logFileLogLevel = logFileLevel;
    logFileConsoleLevel = consoleLevel;
    logFileUseDateInFnameFlag = useDateInFnameFlag;
    logFileSwitchIntervalDays = switchIntervalDays;
    logFileMaxAgeDays = maxLogFileAgeDays;
  }

  /**
   * Returns the log file object for the feeder.
   * @return The 'LogFile' object for the feeder.
   */
  public LogFile getLogFileObj()
  {
    if(logObj == null)                 //if feeder log file not setup then
      setupFeederLogFileObj();         //setup log file for feeder
    return logObj;
  }

  /**
   * Closes the log file for this feeder.
   */
  public void closeLogFile()
  {
    if(logObj != null)
    {    //log file was opened
      logObj.close();        //close log file
      logObj = null;         //clear handle for log file
    }
  }

  /**
   * Enters the callback object to be used for communicating with
   * the QuakeWatch server.  (For 'QWFeederPlugin' interface.)
   * @param callbackObj the callback object to use.
   */
  public void setCallbackObj(QWServerCallback callbackObj)
  {
    this.callbackObj = callbackObj;
  }

  /**
   * Returns the callback object used for communicating with the
   * QuakeWatch server.
   * @return The callback object to in use.
   */
  public QWServerCallback getCallbackObj()
  {
    return callbackObj;
  }

  /**
   * Enters the event domain and type names to be used by this
   * feeder.
   * @param domainStr the event domain name to use, or null for none.
   * @param typeStr the event type name to use, or null for none.
   */
  public void setFeederDomainTypeNames(String domainStr,String typeStr)
  {
    feederDomainNameStr = domainStr;
    feederTypeNameStr = typeStr;
                   //set flag if domain or type name given:
    feederNamesAvailFlag = ((domainStr != null && domainStr.length() > 0) ||
                                 (typeStr != null && typeStr.length() > 0));
  }

  /**
   * Processes feeder-specific settings.  The settings are usually
   * fetched from the "QWFeederSettings" element in the QuakeWatch-server
   * configuration file.  (For 'QWFeederPlugin' interface.)
   * @param settingsStr The settings string to be processed.
   * @throws QWFeederInitException if an error occurs while processing
   * the settings.
   */
  public void processFeederSettings(String settingsStr)
                                                throws QWFeederInitException
  {
  }

  /**
   * Starts up the message collection thread of the feeder.  Subclasses
   * should implement the 'run()' method for the feeder.  The feeder's
   * log file is also setup by this method.
   * (For 'QWFeederPlugin' interface.)
   */
  public void startFeeder()
  {
    feederStoppedFlag = false;         //clear feeder-stopped flag
    if(logObj == null)                 //if feeder log file not setup then
      setupFeederLogFileObj();         //setup log file for feeder
    start();       //startup the thread (executes the 'run' method)
  }

  /**
   * Stops the message collection threads of the feeder.
   * (For 'QWFeederPlugin' interface.)
   */
  public void stopFeeder()
  {
    closeLogFile();                    //close log file (if open)
    feederStoppedFlag = true;          //set feeder-stopped flag
  }

  /**
   * Returns true if the 'stopFeeder()' method has been called and
   * has completed its work.  (For 'QWFeederPlugin' interface.)
   * @return true if the 'stopFeeder()' method has been called and
   * has completed its work; false if not.
   */
  public boolean isFeederStopped()
  {
    return feederStoppedFlag;
  }

  /**
   * Returns the current status-error message (if any) for the feeder.
   * (For 'QWFeederPlugin' interface.)
   * @return The current status-error message for the feeder, or null
   * if none.
   */
  public String getStatusErrorMsg()
  {
    return null;
  }

  /**
   * Returns the time that the last data message was received by the
   * feeder.
   * @return The time that the last data message was received by the
   * feeder (seconds since 1/1/1970), or 0 if none have been received.
   */
  public long getLastDataMsgTime()
  {
    synchronized(lastDataMsgRecvdTimeSyncObj)
    {  //grab thread-synchronization lock for time variable
      return lastDataMsgReceivedTime;
    }
  }

  /**
   * Clears the time that the last data message was received by the
   * feeder.
   */
  protected void clearLastDataMsgTime()
  {
    synchronized(lastDataMsgRecvdTimeSyncObj)
    {  //grab thread-synchronization lock for time variable
      lastDataMsgReceivedTime = 0;
    }
  }

  /**
   * Updates the time that the last data message was received by the
   * feeder to the current timestamp.
   */
  protected void updateLastDataMsgTime()
  {
    synchronized(lastDataMsgRecvdTimeSyncObj)
    {  //grab thread-synchronization lock for time variable
      lastDataMsgReceivedTime = System.currentTimeMillis();
    }
  }

  /**
   * Sets up the log file (if 'setupLogFile()' method was called).
   */
  protected void setupFeederLogFileObj()
  {
    if((logFileNameStr != null && logFileLogLevel != LogFile.NO_MSGS) ||
                                     logFileConsoleLevel != LogFile.NO_MSGS)
    {    //logfile was setup for messages or for console output
      if(logObj != null && logObj.isOpen())
       logObj.close();       //if existing log file open then close it
              //create and setup log file object:
      logObj = new LogFile(logFileNameStr,logFileLogLevel,
                       logFileConsoleLevel,false,logFileUseDateInFnameFlag);
      logObj.setLogFileSwitchIntervalDays(logFileSwitchIntervalDays);
      logObj.setMaxLogFileAge(logFileMaxAgeDays);
    }
    else if(logObj == null)       //if none then setup dummy log file object
      logObj = new LogFile(null,LogFile.NO_MSGS,LogFile.ERROR);
  }
}
