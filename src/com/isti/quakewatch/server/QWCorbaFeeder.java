//QWCorbaFeeder.java:  QuakeWatch feeder plugin module that receives
//                     messages via CORBA method calls.
//
//  1/22/2004 -- [ET]  Initial version.
// 11/24/2004 -- [ET]  Added port number to registered-with-Corbaloc
//                     log message.
//  1/15/2005 -- [ET]  Added "sendSourced..." methods.
//   8/8/2006 -- [ET]  Added support for "feederParserClass" configuration
//                     property.
// 10/18/2006 -- [ET]  Version 1.2:  Modified 'stopFeeder()' method to
//                     do work via a separate thread.
//  11/1/2006 -- [ET]  Version 1.3:  Moved logging of feeder-parser-class
//                     setup from 'processFeederSettings()' to 'run()'
//                     method.
//  7/10/2008 -- [ET]  Version 1.4:  Modified to set thread name to module
//                     name ("QWCorbaFeeder").
//   7/8/2009 -- [ET]  Version 1.5:  Modified to use QWServer's
//                     "serverHostAddress" entry for feeder's CORBA ORB.
//  11/4/2010 -- [ET]  Version 1.6:  Added call to 'updateLastDataMsgTime()'.
//

package com.isti.quakewatch.server;

import org.jdom.Element;
import com.isti.openorbutil.OrbManager;
import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.util.CfgProperties;
import com.isti.util.CfgPropItem;
import com.isti.quakewatch.server.qw_feeder.QWFeederPOA;

/**
 * Class QWCorbaFeeder is a QuakeWatch feeder plugin module that receives
 * messages via CORBA method calls.  The feeder's CORBA servant is
 * accessed through a Corbaloc address like this one:
 * "corbaloc:iiop:1.2@hostAddr:portNum/QWFeeder", where "hostAddr" is
 * the QWServer's host address and "portNum" is the "feederPortNumber"
 * configuration parameter value.
 */
public class QWCorbaFeeder extends QWAbstractFeeder
{
  public static final String MODULE_NAME = "QWCorbaFeeder";
  public static final String VERSION = "1.6";    //name and version strings
  public static final String REVISION_STRING = MODULE_NAME + ", " +
                                                       "Version " + VERSION;
  public static final String FEEDER_NAME = "QWCorba";    //feeder name
  public static final int DEF_PORT_NUM = 38800;  //default feeder port #
                        //reference name for the Corbaloc registration:
  public static final String CORBALOC_REF_NAME = "QWFeeder";
  protected OrbManager orbManagerObj = null;     //ORB manager object
                   //feeder-parser handler for this feeder:
  protected final FeederParserHandler feederParserHandlerObj =
                                                  new FeederParserHandler();

         //configuration properties for this feeder:
  protected final CfgProperties cfgPropsObj = new CfgProperties();
              //port number for this feeder to listen on:
  protected final CfgPropItem feederPortNumberProp =
              cfgPropsObj.add("feederPortNumber",Integer.valueOf(DEF_PORT_NUM));
                   //configuration-property item for "feederParserClass":
  protected final CfgPropItem feederParserClassProp = cfgPropsObj.add(
                         feederParserHandlerObj.getFeederParserClassProp());
         //flag set true if value for "feederParserClass" cfgProp specified:
  protected boolean parserClassSpecifiedflag = false;


  /**
   * Constructs a QWCorba feeder module.
   */
  public QWCorbaFeeder()
  {
    this(MODULE_NAME,FEEDER_NAME);     //set thread and feeder name
  }

  /**
   * Constructs a QWCorba feeder module.
   * @param moduleName the name of the module.
   * @param feederName the name of the feeder.
   */
  public QWCorbaFeeder(String moduleName, String feederName)
  {
    super(moduleName);                 //set thread name to module name
    this.feederName = feederName;      //set the feeder name
  }

  /**
   * Returns the revision String for the feeder.
   * @return the revision String for the feeder.
   */
  public String getRevisionString()
  {
    return REVISION_STRING;
  }

  /**
   * Processes feeder-specific settings.  The settings are usually
   * fetched from the "QWFeederSettings" element in the QuakeWatch-server
   * configuration file.  (For 'QWFeederPlugin' interface.)
   * @param settingsStr The settings string to be processed.
   * @throws QWFeederInitException if an error occurs while processing
   * the settings.
   */
  public void processFeederSettings(String settingsStr)
                                                throws QWFeederInitException
  {
    String msgStr;
    try
    {         //load configuration properties from the given string:
      if(cfgPropsObj.load(settingsStr))
      {  //successfully loaded settings
        if(feederParserHandlerObj.isParserClassSpecified())
        {     //value for cfgProp item 'feederParserClass' was specified
                   //instantiate instance of underlying parser class:
          if(!feederParserHandlerObj.instantiateParserObj())
          {   //instantiate failed; throw feeder exception with error msg
            throw new QWFeederInitException(
                            feederParserHandlerObj.getErrorMessageString());
          }
          parserClassSpecifiedflag = true;       //indicate specified
        }
        return;
      }
         //error loading settings; get error message
      msgStr = cfgPropsObj.getErrorMessage();
    }
    catch(Exception ex)
    {    //some kind of exception error; build error message
      msgStr = "Error processing settings:  " + ex;
    }
         //throw feeder exception with error message:
    throw new QWFeederInitException(msgStr);
  }

  /**
   * Stops the message collection threads of the feeder.  The work
   * is done via a separate thread.  (For 'QWFeederPlugin' interface.)
   */
  public void stopFeeder()
  {
    (new Thread("stop-" + getFeederName())
        {          //create separate thread to do "stop" work
          public void run()
          {
            if(logObj != null)
              logObj.debug2("Feeder stop requested");
            if(orbManagerObj != null)
            {    //ORB manager was initialized
              orbManagerObj.closeImplementation(false);    //stop ORB
              if(orbManagerObj.waitForImplFinished(250))   //wait for done
              {
                if(logObj != null)
                  logObj.debug("ORB implementation closed OK");
              }
              else
              {
                if(logObj != null)
                  logObj.debug("Unable to close ORB implementation");
              }
            }
            QWCorbaFeeder.super.stopFeeder();    //call parent method
          }                                      // (closes log file)
        }).start();          //start thread
  }

  /**
   * Executing method for the feeder thread.  Starts execution of the
   * CORBA-servant threads.
   */
  public void run()
  {
    if(logObj == null)  //if no log file setup then create dummy object
      logObj = new LogFile(null,LogFile.NO_MSGS,LogFile.ERROR);
    try
    {
      if(parserClassSpecifiedflag)
      {   //indicate feeder-parser-class in use
        logObj.info("Using feeder-parser object ('"
                        + feederParserHandlerObj.getFeederParserClassProp().
                                                             stringValue() +
                           "') for QWCorbaFeeder (\"" + feederName + "\")");
      }
      final int feederPortNum;
      if((feederPortNum=feederPortNumberProp.intValue()) <= 0)
      {       //bad port number
        logObj.warning("Invalid feeder port number:  " + feederPortNum);
        return;
      }
              //create ORB manager object (use QWServer's host address):
      orbManagerObj = new OrbManager(
                     null,callbackObj.getServerHostAddress(),feederPortNum);
              //initialize ORB (with 'Corbaloc' service support):
      if(!orbManagerObj.initImplementation(true))
      {       //error initializing; log error message
        logObj.warning("Error initializing CORBA implementation:  " +
                                           orbManagerObj.getErrorMessage());
        orbManagerObj = null;     //clear handle to ORB manager
        return;
      }
      logObj.debug2("Using feeder port number:  " + feederPortNum);
      logObj.debug2("Successfully initialized CORBA ORB for feeder");

         //create QWFeeder object and register it with Corbaloc:
      QWFeederImpl qwFeederObj = null;
      try
      {       //create QWFeeder implementation object:
        qwFeederObj = new QWFeederImpl();
      }
      catch(Exception ex)
      {
        logObj.warning("Error creating QWFeederImpl object:  " + ex);
      }
      if(qwFeederObj != null)
      {  //QWFeeder implementation object created OK
                   //register QWFeeder object with Corbaloc:
        if(orbManagerObj.registerCorbalocObject(CORBALOC_REF_NAME,
                                                               qwFeederObj))
        {
          logObj.info("Registered 'QWFeeder' object with 'Corbaloc' " +
                                        "service at port " + feederPortNum);
        }
        else
        {
          logObj.warning("Error registering QWFeeder object:  " +
                                           orbManagerObj.getErrorMessage());
        }
      }
      logObj.info("QWCorbaFeeder successfully started");

         //run CORBA implementation:
      if(!orbManagerObj.runImplementation())
      {  //error running implementation
        logObj.warning("Error running CORBA implentation:  " +
                                           orbManagerObj.getErrorMessage());
        return;
      }
      logObj.debug2("Returned from CORBA implementation run method");
    }
    catch(Exception ex)
    {         //some kind of exception error; log message
      logObj.warning("Error starting feeder:  " + ex);
    }
  }

  /**
   * Processes the given message, sending it into the server via the
   * call-back object.
   * @param domainStr the domain name to use, or null for
   * none.
   * @param typeStr the type name to use, or null for none.
   * @param nameStr the event name to use, or null for none.
   * @param messageStr the data event message string.
   * @param feederSourceHostStr the data-source host string for the
   * message.
   * @param feederSrcHostMsgIdNum the message-ID number from the data
   * source (positive value incremented after each message).
   * @return true after the message has been successfully stored
   * and processed; false if an error occurred.
   */
  protected boolean processMessage(String domainStr,
                          String typeStr, String nameStr, String messageStr,
                     String feederSourceHostStr, long feederSrcHostMsgIdNum)
  {
    try
    {
      updateLastDataMsgTime();         //mark time of data message received
              //if default feeder names available then
              // check if any event domain/type/name strings passed in:
      if(feederNamesAvailFlag &&
                           (domainStr == null || domainStr.length() <= 0) &&
                               (typeStr == null || typeStr.length() <= 0) &&
                                 (nameStr == null || nameStr.length() <= 0))
      {  //feeder names avail & no event domain/type/name strings passed in
        domainStr = feederDomainNameStr;    //use default domain name
        typeStr = feederTypeNameStr;        //use default type name
      }
      if(parserClassSpecifiedflag)
      {  //using feeder-parser specified via "feederParserClass"

        final Element elementObj;  //parse message into JDOM/XML Elems:
        if((elementObj=feederParserHandlerObj.parseFeederMessageStr(
                                             messageStr,-1,null,0)) != null)
        {     //message parsed OK
                        //send in element object via callback object
                        // (pass in domain/type names for feeder,
                        //  wait for queue to clear; return result flag):
          final String str;              //return true if no error returned
          return ((str=callbackObj.dataEvent(domainStr,typeStr,nameStr,
                                   elementObj,null,true,feederSourceHostStr,
                  feederSrcHostMsgIdNum)) != QWServerCallback.ERR_RET_STR &&
                                       str != QWServerCallback.DUP_RET_STR);
        }
        else
        {     //error parsing message; build and log error message
          logObj.warning("QWCorbaFeeder (\"" + feederName +
                                          "\"):  Error parsing message:  " +
                            feederParserHandlerObj.getErrorMessageString());
          logObj.debug("  msg: " + messageStr);
        }
        return false;
      }
      else
      {  //not using feeder-parser
              //pass event message string on to server, via call-back
              // (pass in domain/type names for feeder,
              //  wait for queue to clear; return result flag):
        final String str;                //return true if no error returned
        return ((str=callbackObj.dataEvent(domainStr,typeStr,nameStr,
              messageStr,true,feederSourceHostStr,feederSrcHostMsgIdNum)) !=
                                             QWServerCallback.ERR_RET_STR &&
                                       str != QWServerCallback.DUP_RET_STR);
      }
    }
    catch(Exception ex)
    {    //some kind of exception error; log it
      logObj.warning("QWCorbaFeeder error:  " +
                               "Error processing received message:  " + ex);
      logObj.debug(UtilFns.getStackTraceString(ex));
      return false;
    }
  }


  /**
   * Class QWFeederImpl implements the 'QWFeeder.idl' methods that are
   * used to send messages into the server.
   */
  protected class QWFeederImpl extends QWFeederPOA
  {
    /**
     * Sends a data event message.  If the event data does not
     * begin with a "DataMessage" XML element then the data will
     * be surrounded with one.  The "sendSourced..." methods are
     * preferred because the feeder-source host name and message
     * number are used for improved message tracking.
     * @param messageStr the data event message string.
     * @param feederSourceHostStr the data-source host string for
     * the message.
     * @param feederSrcHostMsgIdNum the message-ID number from the
     * data source (positive value incremented after each message).
     * @return true after the message has been successfully stored
     * and processed; false if an error occurred.
     */
    public boolean sendSourcedMsg(String messageStr,
                     String feederSourceHostStr, long feederSrcHostMsgIdNum)
    {
      return processMessage(null,null,null,messageStr,
                                 feederSourceHostStr,feederSrcHostMsgIdNum);
    }

    /**
     * Sends a data event message.  If the event data does not
     * begin with a "DataMessage" XML element then the data will
     * be surrounded with one.  The "sendSourced..." methods are
     * preferred because the feeder-source host name and message
     * number are used for improved message tracking.
     * @param domainStr the domain name to use, or null for
     * none.
     * @param typeStr the type name to use, or null for none.
     * @param messageStr the data event message string.
     * @param feederSourceHostStr the data-source host string for
     * the message.
     * @param feederSrcHostMsgIdNum the message-ID number from the
     * data source (positive value incremented after each message).
     * @return true after the message has been successfully stored
     * and processed; false if an error occurred.
     */
    public boolean sendSourcedDomainTypeMsg(
                        String domainStr, String typeStr, String messageStr,
                     String feederSourceHostStr, long feederSrcHostMsgIdNum)
    {
      return processMessage(domainStr,typeStr,null,messageStr,
                                 feederSourceHostStr,feederSrcHostMsgIdNum);
    }

    /**
     * Sends a data event message.  If the event data does not
     * begin with a "DataMessage" XML element then the data will
     * be surrounded with one.  The "sendSourced..." methods are
     * preferred because the feeder-source host name and message
     * number are used for improved message tracking.
     * @param domainStr the domain name to use, or null for
     * none.
     * @param typeStr the type name to use, or null for none.
     * @param nameStr the event name to use, or null for none.
     * @param messageStr the data event message string.
     * @param feederSourceHostStr the data-source host string for
     * the message.
     * @param feederSrcHostMsgIdNum the message-ID number from the
     * data source (positive value incremented after each message).
     * @return true after the message has been successfully stored
     * and processed; false if an error occurred.
     */
    public boolean sendSourcedDomainTypeNameMsg(String domainStr,
                          String typeStr, String nameStr, String messageStr,
                     String feederSourceHostStr, long feederSrcHostMsgIdNum)
    {
      return processMessage(domainStr,typeStr,nameStr,messageStr,
                                 feederSourceHostStr,feederSrcHostMsgIdNum);
    }

    /**
     * Sends a data event message.  If the event data does not
     * begin with a "DataMessage" XML element then the data will
     * be surrounded with one.  The "sendSourced..." methods are
     * preferred because the feeder-source host name and message
     * number are used for improved message tracking.
     * @param messageStr the data event message string.
     * @return true after the message has been successfully stored
     * and processed; false if an error occurred.
     */
    public boolean sendMessage(String messageStr)
    {
      return processMessage(null,null,null,messageStr,null,0);
    }

    /**
     * Sends a data event message.  If the event data does not
     * begin with a "DataMessage" XML element then the data will
     * be surrounded with one.  The "sendSourced..." methods are
     * preferred because the feeder-source host name and message
     * number are used for improved message tracking.
     * @param domainStr the domain name to use, or null for
     * none.
     * @param typeStr the type name to use, or null for none.
     * @param messageStr the data event message string.
     * @return true after the message has been successfully stored
     * and processed; false if an error occurred.
     */
    public boolean sendDomainTypeMessage(String domainStr, String typeStr,
                                                          String messageStr)
    {
      return processMessage(domainStr,typeStr,null,messageStr,null,0);
    }

    /**
     * Sends a data event message.  If the event data does not
     * begin with a "DataMessage" XML element then the data will
     * be surrounded with one.  The "sendSourced..." methods are
     * preferred because the feeder-source host name and message
     * number are used for improved message tracking.
     * @param domainStr the domain name to use, or null for
     * none.
     * @param typeStr the type name to use, or null for none.
     * @param nameStr the event name to use, or null for none.
     * @param messageStr the data event message string.
     * @return true after the message has been successfully stored
     * and processed; false if an error occurred.
     */
    public boolean sendDomainTypeNameMessage(String domainStr,
                          String typeStr, String nameStr, String messageStr)
    {
      return processMessage(domainStr,typeStr,nameStr,messageStr,null,0);
    }
  }
}
