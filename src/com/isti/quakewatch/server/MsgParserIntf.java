//MsgParserIntf.java:  Defines methods for a message parser to be used
//                     with a QuakeWatch-feeder module.
//
//   8/7/2006 -- [ET]
//

package com.isti.quakewatch.server;

import org.jdom.Element;

/**
 * Interface MsgParserIntf defines methods for a message parser to be used
 * with a QuakeWatch-feeder module.
 */
public interface MsgParserIntf
{
  /**
   * Parses a message into a JDOM/XML tree of elements.
   * @param messageString the message string to be parsed.
   * @param msgNum message number to be entered as "FeederMsgNum" in
   * the returned Element.
   * @param hostNameStr name of originating host to be entered as
   * "SourceHost" in the returned Element (or null for none).
   * @param hostMessageID message ID number to be entered as
   * "SrcHostMsgID" in the returned (if 'hostNameStr' not null).
   * @return The resulting "DataMessage" Element object, or null if an
   * error occured (in which case the associated error message may be
   * fetched via the 'getErrorMessage()' method).
   */
  public Element parseMessageString(String messageString, long msgNum,
                                    String hostNameStr, long hostMessageID);

  /**
   * Returns true if an error was detected.  The error message may be
   * fetched via the 'getErrorMessage()' method.
   * @return true if error; false if not.
   */
  public boolean getErrorFlag();

  /**
   * Returns message string for last error (or 'No error' if none) and
   * clears the error.
   * @return The error message text (or 'No error' if none).
   */
  public String getErrorMessage();

  /** Clears the error message string. */
  public void clearErrorMessage();
}
