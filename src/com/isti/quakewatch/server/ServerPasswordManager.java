//ServerPasswordManager.java:  Manages QuakeWatch server passwords.
//
//  8/17/2004 -- [KF]  Initial version.
//  9/22/2004 -- [KF]  Updated for new database.
// 10/13/2004 -- [ET]  Modified 'getStandardPassword()' to check for
//                     no-data parameter; modified to use 'LogFile'
//                     object via constructor parameter; improved
//                     variable names and log outputs; changed
//                     constructor use of  'InstantiationException'
//                     to new 'SvrPwdMgrCreationException' class.
// 10/15/2004 -- [ET]  Modified to make 'validateLoginInformation()'
//                     throw an exception if an error occurs; added
//                     'closeDatabaseConnection()' call to 'shutdown()'.
//  11/2/2004 -- [ET]  Added 'passwordDatabaseObj' null check to
//                     'shutdown()' method.
// 11/19/2004 -- [ET]  Renamed "passwordServer..." items to "pwdDatabase...".
//  1/10/2005 -- [ET]  Changed level of "Executing SQL query" log output
//                     from "Debug2" to "Debug3".
//   2/2/2005 -- [ET]  Modified to convert 'localPasswordFilename' path
//                     'QWServer.fromBaseDir()'.
//

package com.isti.quakewatch.server;

import java.util.Map;
import java.io.File;
import com.isti.util.UtilFns;
import com.isti.util.FileUtils;
import com.isti.util.LogFile;
import com.isti.util.CfgPropItem;
import com.isti.util.CallBackCompletion;
import com.isti.util.SimplePropsParser;
import com.isti.util.database.MySQLConnectionJDBC;
import com.isti.quakewatch.util.QWPasswordUtils;
import com.isti.quakewatch.server.database.PasswordDatabase;

/**
 * Class ServerPasswordManager manages QuakeWatch server passwords.
 */
public class ServerPasswordManager
{
  //parameters
  private final LogFile logObj;
  //true if a database was specified, false otherwise.
  private final boolean databaseFlag;
  //password information
  private final String pwdSvrDatabaseStr;
  private final String pwdSvrHostNameStr;
  private final String [] failOverHostNames;
  private final String pwdSvrPortStr;
  private final String pwdSvrUsernameStr;
  private final String pwdSvrPasswordStr;
  private final String localPasswordFilename;
  //the password database
  private PasswordDatabase passwordDatabaseObj = null;

  /**
   * Creates an object for managing QuakeWatch server passwords.
   * The database connection needs to be created before any password
   * validation may be done but this will happen automatically with the call to
   * the 'validateLoginInformation' method if not done via
   * a call to the 'createDatabaseConnection' method.
   * @param cfgObj the server config manager.
   * @param logObj the log-file object to use.
   * @exception SvrPwdMgrCreationException if the manager could not be
   * instantiated.
   * @see createDatabaseConnection, validateLoginInformation
   */
  public ServerPasswordManager(ServerConfigManager cfgObj, LogFile logObj)
                                           throws SvrPwdMgrCreationException
  {
    //save the parameters
    this.logObj = (logObj != null) ? logObj : LogFile.getNullLogObj();

    if (cfgObj == null)  //if no configuration manager
      throw new SvrPwdMgrCreationException("No configuration manager");

    pwdSvrHostNameStr = getStringValue(cfgObj.pwdDatabaseHostAddressProp);
    failOverHostNames = null;
    pwdSvrPortStr = getStringValue(cfgObj.pwdDatabasePortNumberProp);
    pwdSvrDatabaseStr = getStringValue(cfgObj.pwdDatabaseIdentifierProp);
    pwdSvrUsernameStr = getStringValue(cfgObj.pwdDatabaseUsernameProp);
    pwdSvrPasswordStr = getStringValue(cfgObj.pwdDatabasePasswordProp);
    localPasswordFilename = getStringValue(cfgObj.localPasswordFileProp);

    //if database was provided
    if (pwdSvrDatabaseStr != null)
    {
      logObj.info("Using password database \"" + pwdSvrDatabaseStr +
                                                         "\" on host \"" + (
              (pwdSvrHostNameStr!=null)?pwdSvrHostNameStr:"default") + ":" +
                    ((pwdSvrPortStr!=null)?pwdSvrPortStr:"default") + "\"");
      try
      {
        MySQLConnectionJDBC.registerDriver();
      }
      catch (Exception ex)
      {  //some kind of exception error; log it and re-throw exception
        logObj.debug3(UtilFns.getStackTraceString(ex));
        throw new SvrPwdMgrCreationException(ex.toString());
      }
    }

    //if local password filename was provided
    if (localPasswordFilename != null)
    {
      logObj.info("Using local password file:  \"" +
                                              localPasswordFilename + "\"");
      final File localPasswordFile =
                      new File(QWServer.fromBaseDir(localPasswordFilename));
      if (!localPasswordFile.isFile())
      {
        throw new SvrPwdMgrCreationException("Local password file (\"" +
                              localPasswordFilename + "\") does not exist");
      }
    }

    //if no database or local password file was provided
    if (pwdSvrDatabaseStr != null || localPasswordFilename != null)
    {
      databaseFlag = true;  //database specified
    }
    else
    {
      logObj.info("No password server database specified, " +
                                             "all logins will be accepted");
      databaseFlag = false;  //no database
      return;
    }
  }

  /**
   * Create the database connection.
   * @param callBack callBack method or null to execute on current thread
   * @return true if successfull, false otherwise.
   */
  public boolean createDatabaseConnection(CallBackCompletion callBack)
  {
    if (pwdSvrDatabaseStr == null)  //exit if no database provided
      return true;

    try
    {
      final MySQLConnectionJDBC dbConn = new MySQLConnectionJDBC(
                       pwdSvrHostNameStr, failOverHostNames, pwdSvrPortStr,
                                      pwdSvrDatabaseStr, pwdSvrUsernameStr,
                                              pwdSvrPasswordStr, callBack);
      passwordDatabaseObj = new PasswordDatabase(dbConn);
      return true;
    }
    catch (Exception ex)
    {
      passwordDatabaseObj = null;
      //some kind of exception error; log it
      logObj.warning("Error creating login information:  " + ex);
      logObj.debug2(UtilFns.getStackTraceString(ex));
    }
    return false;
  }

  /**
   * Validates login information.
   * The database connection will be created if not previously done via
   * a call to the 'createDatabaseConnection' method.
   * @param usernameText the username text.
   * @param passwordText the password text.
   * @param clientHostNameString the host name.
   * @param clientHostIPString the IP Address.
   * @return true if the login information should be accepted, false otherwise.
   * @throws ServerLoginException if an error occurred while validating
   * the login.
   * @see createDatabaseConnection
   */
  public boolean validateLoginInformation(String usernameText,
                          String passwordText, String clientHostNameString,
                    String clientHostIPString) throws ServerLoginException
  {
    if (!databaseFlag)  //if no password database
    {
      logObj.debug2("ServerPasswordManager:  " +
                "No password server database specified, accepting user \"" +
                                                       usernameText + "\"");
      return true;
    }

    final String standardPwd = getStandardPassword(usernameText);
    if (standardPwd == null)  //exit if unable to fetch the password
      return false;

    try
    {
      final String memEncKey = QWPasswordUtils.createMemEncKey(
          clientHostNameString, clientHostIPString);
      final String encPwd = QWPasswordUtils.createEncPwd(
          memEncKey, standardPwd);
      final String transEncPwd =
          QWPasswordUtils.createTransEncPwd(memEncKey, encPwd);
      if (!transEncPwd.equals(passwordText))  //if password is not correct
      {
        logObj.debug("ServerPasswordManager:  " +
           "Password for username \"" + usernameText + "\" does not match");
        return false;
      }
      logObj.debug2("ServerPasswordManager:  " +
                  "Password for username \"" + usernameText + "\" matched");
      return true;
    }
    catch(Exception ex)
    {    //some kind of exception error; log it
      final String errStr = "Error validating login password:  " + ex;
      logObj.warning(errStr);
      logObj.warning(UtilFns.getStackTraceString(ex));
      throw new ServerLoginException(errStr);
    }
  }

  /**
   * Gets the string value of the specified property.
   * @param item the property item.
   * @return the string value of the specified property or null if it does
   * not exist or is empty.
   */
  protected static String getStringValue(CfgPropItem item)
  {
    final String stringValue = item.stringValue();
    if (stringValue == null || stringValue.length() <= 0)
      return null;
    return stringValue;
  }

  /**
   * Gets the standard password for the specified username.
   * @param usernameText the username text.
   * @return the standard password for the specified username or null if the
   * password was not found or an error occurred.
   * @throws ServerLoginException if an error occurred while fetching the
   * password.
   */
  protected String getStandardPassword(String usernameText)
                                                 throws ServerLoginException
  {
    if(usernameText == null || usernameText.length() <= 0)
      return null;      //if no username given then return null
    if (localPasswordFilename != null)  //if using local passwords
    {
      try
      {
        //read in the local password file
        final String propsStr = FileUtils.readFileToString(
                               QWServer.fromBaseDir(localPasswordFilename));
        //create s property parser
        final SimplePropsParser propsParser =
            new SimplePropsParser(propsStr, '\n');
        //get the password for the username
        final Object pwdObj = propsParser.getValue(usernameText);
        if (pwdObj instanceof String)  //if the password was found
        {
          return (String)pwdObj;  //return the password
        }
      }
      catch (Exception ex)
      {  //some kind of exception error; log it
        final String errStr = "Error reading password-file information:  " + ex;
        logObj.warning(errStr);
        logObj.warning(UtilFns.getStackTraceString(ex));
        throw new ServerLoginException(errStr);
      }
    }

    if (pwdSvrDatabaseStr == null)
    {    //no database provided
      logObj.debug("ServerPasswordManager:  Entry for user \"" +
                                             usernameText + "\" not found");
      return null;  //no password was found
    }

    final String sqlStr;
    try
    {
      if (passwordDatabaseObj == null)
      {    //not connected to database; create connection now
        createDatabaseConnection(null);  //create the database connection
        if (passwordDatabaseObj == null)
        {  //still error creating connection to database
          logObj.warning("Could not connect to password-server database, " +
                                   "rejecting user \"" + usernameText + "\"");
          return null;  //error occurred
        }
      }
      sqlStr = passwordDatabaseObj.getSqlStatement(usernameText);
    }
    catch(Exception ex)
    {
      final String errStr = "Error setting up password-database query:  " +
                                                                         ex;
      logObj.warning(errStr);
      logObj.warning(UtilFns.getStackTraceString(ex));
      throw new ServerLoginException(errStr);
    }
    logObj.debug3("ServerPasswordManager:  Executing SQL query:  " + sqlStr);

    try
    {
      passwordDatabaseObj.executeQuery(sqlStr);
    }
    catch(Exception ex)
    {    //some kind of exception error; log it
      final String errStr = "Error executing password-database query:  " +
                                                                         ex;
      logObj.warning(errStr);
      logObj.debug2(UtilFns.getStackTraceString(ex));
      throw new ServerLoginException(errStr);
    }

    final int numResults = passwordDatabaseObj.getColumnObjects().size();
    try
    {
      if (numResults != 1)
      {    //number of matches not one
        if (numResults > 1)
        {  //more than one match
          logObj.warning("Too many entries for user \"" + usernameText +
                                           "\" found (" + numResults + ")");
        }
        else
        {  //no matches
          logObj.debug("ServerPasswordManager:  Entry for user \"" +
                                             usernameText + "\" not found");
        }
        return null;  //no password was found
      }
      else
      {
        final Map pwdMapObj = passwordDatabaseObj.getMap(0);
        final String statusStr = passwordDatabaseObj.getStatus(pwdMapObj);
        if (!statusStr.equals(PasswordDatabase.STATUS_ACTIVE))
        {       //status not active
          logObj.debug("ServerPasswordManager:  Status of user \"" +
                              usernameText + "\" is not active (status=\"" +
                                                         statusStr + "\")");
          return null;  //password is not active
        }

        return passwordDatabaseObj.getPassword(pwdMapObj);   //return password
      }
    }
    catch(Exception ex)
    {    //some kind of exception error; log it
      final String errStr = "Error processing password-database query:  " +
                                                                         ex;
      logObj.warning(errStr);
      logObj.warning(UtilFns.getStackTraceString(ex));
      throw new ServerLoginException(errStr);
    }
  }

  /**
   * Shuts down the password manager.
   */
  public void shutdown()
  {
    if(passwordDatabaseObj != null)
      passwordDatabaseObj.closeDatabaseConnection();
    try
    {
      MySQLConnectionJDBC.deregisterDriver();
    }
    catch (Exception ex)
    {    //some kind of exception error; log it
      logObj.warning("Error creating login information:  " + ex);
      logObj.debug2(UtilFns.getStackTraceString(ex));
    }
  }


  /**
   * Class SvrPwdMgrCreationException defines an exception that is thrown
   * when the 'ServerPasswordManager' constructor fails.
   */
  public static class SvrPwdMgrCreationException extends Exception
  {
    /**
     * Creates an exception thrown when the 'ServerPasswordManager'
     * constructor fails.
     * @param msgStr the error message string.
     */
    public SvrPwdMgrCreationException(String msgStr)
    {
      super(msgStr);
    }
  }
}
