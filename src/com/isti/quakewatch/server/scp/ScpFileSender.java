//ScpFileSender.java:  Defines a thread for sending a file to a remote
//                     host via an 'scp' transfer.
//
//  2/15/2006 -- [ET]
//

package com.isti.quakewatch.server.scp;

import java.util.Properties;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSchException;
import com.isti.util.LogFile;
import com.isti.util.IstiNotifyThread;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.ByteArrayInputStream;

/**
 * Class ScpFileSender defines a thread for sending a file to a remote
 * host via an 'scp' transfer.  The 'JSch' package is used for SSH/SCP
 * support (http://www.jcraft.com/jsch).
 */
public class ScpFileSender extends IstiNotifyThread
{
         //'scp' command string sent to remote host:
  protected static final String SCP_SENDCMD_STR = "scp -p -t ";
         //file attributes command string sent to remote host:
  protected static final String C0644_SENDCMD_STR = "C0644 ";
         //length of transmit buffer:
  protected static final int TRAN_BUFF_LEN = 1024;
         //default host port number value:
  protected static final int DEF_HOSTPORT_NUM = 22;
         //default value for 'checkHostFlag' parameter:
  protected static final boolean DEF_CHKHOST_FLAG = true;
         //default name of authorization key file:
  protected static String defAuthKeyFileNameStr =
                       System.getProperty("user.home",".") + "/.ssh/id_rsa";
         //default name of known-hosts file:
  protected static String defKnownHostsFileNameStr =
                  System.getProperty("user.home",".") + "/.ssh/known_hosts";

         //prefix string for log-warning messages:
  protected static String logWarningPrefixStr =
                                          "Error sending file via 'scp':  ";

         //thread counter; incremented for each thread created:
  protected static int threadCounter = 0;
         //handle for current sender object:
  protected static ScpFileSender curScpFileSenderObj = null;
         //thread synchronization object for 'curScpFileSenderObj':
  protected static final Object curScpFileSdrSyncObj = new Object();

         //handles for parameters passed into constructor:
  protected final String sourceFileStr;
  protected final InputStream sourceStmObj;
  protected final long sourceDataLen;
  protected final String remoteFilePathStr;
  protected final ScpHostInfo hostInfoObj;
  protected final LogFile logObj;

         //handles for items used across methods:
  protected InputStream openedInputFileStm = null;
  protected InputStream remoteInputStm;
  protected Session remoteSessionObj = null;

         //flag set true if "fatal" error detected:
  protected boolean fatalErrorFlag = false;


  /**
   * Sends data from the given source input stream to a file on a remote
   * remote host.  A worker thread is used to send the data.  If a
   * previously-executed worker thread is still running then it is
   * terminated.
   * @param sourceFileStr file name associated with source input stream,
   * or null to use 'remoteFilePathStr' value.
   * @param sourceStmObj source input stream to use.
   * @param sourceDataLen length of source data (in bytes).
   * @param remoteFilePathStr path and name for file on remote host.
   * @param hostInfoObj host information object to use.
   * @param logFileObj log file to use, or null for none.
   */
  public static void send(String sourceFileStr, InputStream sourceStmObj,
                               long sourceDataLen, String remoteFilePathStr,
                                ScpHostInfo hostInfoObj, LogFile logFileObj)
  {
    synchronized(curScpFileSdrSyncObj)
    {    //grab thread-synchronization lock
      if(curScpFileSenderObj != null && curScpFileSenderObj.isAlive())
      {  //previous thread exists and is still running
        curScpFileSenderObj.terminate();         //terminate thread
                   //wait for thread to die, up to 5 seconds:
        curScpFileSenderObj.waitForCompletion(5000);
      }
              //create new sender thread:
      curScpFileSenderObj = new ScpFileSender(sourceFileStr,sourceStmObj,
                    sourceDataLen,remoteFilePathStr,hostInfoObj,logFileObj);
              //start sender thread:
      curScpFileSenderObj.start();
    }
  }

  /**
   * Sends data from the given input file to a file on a remote host.
   * A worker thread is used to send the data.  If a previously-executed
   * worker thread is still running then it is terminated.
   * @param sourceFileStr name of input file to use.
   * @param remoteFilePathStr path and name for file on remote host.
   * @param hostInfoObj host information object to use.
   * @param logFileObj log file to use, or null for none.
   */
  public static void send(String sourceFileStr, String remoteFilePathStr,
                                 ScpHostInfo hostInfoObj,LogFile logFileObj)
  {
    send(sourceFileStr,null,0,remoteFilePathStr,hostInfoObj,logFileObj);
  }

  /**
   * Sends data from the given source data array to a file on a
   * remote host.  A worker thread is used to send the data.
   * If a previously-executed worker thread is still running
   * then it is terminated.
   * @param sourceFileStr file name associated with source data array,
   * or null to use 'remoteFilePathStr' value.
   * @param sourceDataArr source data array to use.
   * @param remoteFilePathStr path and name for file on remote host.
   * @param hostInfoObj host information object to use.
   * @param logFileObj log file to use, or null for none.
   */
  public static void send(String sourceFileStr, byte [] sourceDataArr,
       String remoteFilePathStr, ScpHostInfo hostInfoObj,LogFile logFileObj)
  {
              //create input-stream access to source data:
    final ByteArrayInputStream inStmObj =
                                    new ByteArrayInputStream(sourceDataArr);
              //read and transfer data to remote host:
    send(sourceFileStr,inStmObj,sourceDataArr.length,remoteFilePathStr,
                                                    hostInfoObj,logFileObj);
  }

  /**
   * Terminates the sender-worker thread if it is running.
   */
  public static void terminateThread()
  {
    synchronized(curScpFileSdrSyncObj)
    {    //grab thread-synchronization lock
      if(curScpFileSenderObj != null && curScpFileSenderObj.isAlive())
      {  //previous thread exists and is still running
        curScpFileSenderObj.terminate();         //terminate thread
        curScpFileSenderObj = null;              //clear handle
      }
    }
  }

  /**
   * Determines if the sender-worker thread is running.
   * @return true if the sender-worker thread is running; false if not.
   */
  public static boolean isThreadAlive()
  {
    synchronized(curScpFileSdrSyncObj)
    {    //grab thread-synchronization lock
      return (curScpFileSenderObj != null && curScpFileSenderObj.isAlive());
    }
  }

  /**
   * Returns a flag indicating whether or not a "fatal" error was
   * detected by the latest sender-worker thread.  Errors considered
   * "fatal" include "authorization failed", "unknown host" and
   * "Java cryptography support not found".
   * @return true if a "fatal" error was detected; false if not.
   */
  public static boolean getThreadFatalErrorFlag()
  {
    synchronized(curScpFileSdrSyncObj)
    {    //grab thread-synchronization lock
      return (curScpFileSenderObj != null &&
                                   curScpFileSenderObj.getFatalErrorFlag());
    }
  }

  /**
   * Creates a thread object for sending a file to a remote host via an
   * 'scp' transfer.  After being created, the thread object must be
   * executed via the 'start()' method.
   * @param sourceFileStr file name associated with source input stream,
   * or null to use 'remoteFilePathStr' value.
   * @param sourceStmObj source input stream to use, or null if
   * 'sourceFileStr' file should be opened and read.
   * @param sourceDataLen length of source data (in bytes), or 0 if
   * length of 'sourceFileStr' file should be used.
   * @param remoteFilePathStr path and name for file on remote host.
   * @param hostInfoObj host information object to use.
   * @param logFileObj log file to use, or null for none.
   */
  public ScpFileSender(String sourceFileStr, InputStream sourceStmObj,
                               long sourceDataLen, String remoteFilePathStr,
                                ScpHostInfo hostInfoObj, LogFile logFileObj)
  {
    super("ScpFileSender_" + (++threadCounter));
    this.sourceFileStr = sourceFileStr;
    this.sourceStmObj = sourceStmObj;
    this.sourceDataLen = sourceDataLen;
    this.remoteFilePathStr = remoteFilePathStr;
    this.hostInfoObj = hostInfoObj;
    logObj = (logFileObj != null) ? logFileObj : LogFile.getNullLogObj();
  }

  /**
   * Executing method for thread.
   */
  public void run()
  {
    logObj.debug3(getName() + " thread started");
    try
    {
      doSend();         //send file
    }
    catch(Throwable ex)
    {    //some kind of exception error; log it
      String str;
      if(ex instanceof JSchException && (str=ex.getMessage()) != null)
      {  //JSch exception and exception-message text fetched OK
        if(str.indexOf("Auth fail") >= 0)
        {     //error is authorization failure; log warning message
          logObj.warning(logWarningPrefixStr + "Authorization failed");
          fatalErrorFlag = true;       //indicate "fatal" error detected
        }
        else if(str.indexOf("UnknownHostKey") >= 0)
        {     //error is unknown host; log warning message
          logObj.warning(logWarningPrefixStr +
                             "Remote host not found in 'known_hosts' file");
          fatalErrorFlag = true;       //indicate "fatal" error detected
        }
        else       //error not identified; log generic warning message
          logObj.warning(logWarningPrefixStr + ex);
      }
      else if(ex instanceof NoClassDefFoundError &&
                (str=ex.getMessage()) != null && str.indexOf("crypto") >= 0)
      {  //Java cryptography class not found; log warning message
        logObj.warning(logWarningPrefixStr +
           "Java cryptography support not found; Java 1.4 or later needed");
        fatalErrorFlag = true;         //indicate "fatal" error detected
      }
      else         //error not identified; log generic warning message
        logObj.warning(logWarningPrefixStr + ex);
    }
    finally
    {         //if input file was opened then close it now:
      if(openedInputFileStm != null)
      {  //input file was opened by thread
        try
        {     //close input file:
          openedInputFileStm.close();
        }
        catch(IOException ex)
        {     //error closing file; ignore it
        }
        openedInputFileStm = null;     //clear handle to closed file
      }
              //always disconnect session before exiting thread:
      if(remoteSessionObj != null)
      {  //session was connected
        try
        {
          remoteSessionObj.disconnect();
        }
        catch(Exception ex)
        {
          logObj.debug2(
                     "ScpFileSender:  Error disconnecting session:  " + ex);
        }
        remoteSessionObj = null;
      }
    }
    logObj.debug3(getName() + " thread finished");
  }

  /**
   * Waits for the thread to complete.
   * @param waitTimeMs maximum amount of time to wait, in milliseconds.
   * @return true if thread completed (not alive); false if not.
   */
  public boolean waitForCompletion(int waitTimeMs)
  {
    if(!isAlive())           //if thread not alive then
      return true;           //exit method
         //calculate number of loops for timeout:
    int numWaits = waitTimeMs / 100;
    while(true)
    {    //loop while thread alive and not timeout
      waitForNotify(100);    //wait between checks
      if(!isAlive())         //if thread not alive then
        return true;         //exit method
      if(--numWaits <= 0)    //if timeout reached then
        return false;        //exit method
    }
  }

  /**
   * Returns a flag indicating whether or not a "fatal" error was detected.
   * Errors considered "fatal" include "authorization failed",
   * "unknown host" and "Java cryptography support not found".
   * @return true if a "fatal" error was detected; false if not.
   */
  public boolean getFatalErrorFlag()
  {
    return fatalErrorFlag;
  }

  /**
   * Sends data from the given source input stream to a file on a remote
   * host.
   * @throws JSchException if an error occurs.
   */
  protected void doSend() throws JSchException
  {
    final InputStream localSrcStmObj;
    final long localSrcDataLen;
    final String localSrcFileStr;
    final String logSrcStr;
    if(sourceFileStr != null && sourceFileStr.length() > 0)
    {    //source file name given
      localSrcFileStr = sourceFileStr;      //set handle to source file name
      logSrcStr = "\"" + sourceFileStr + "\"";    //set name for log msg
      if(sourceStmObj != null)              //if source stream given then
        localSrcStmObj = sourceStmObj;      //set local handle to stream
      else
      {  //source stream not given
        try
        {         //open file for input:
          localSrcStmObj = new BufferedInputStream(
                                        new FileInputStream(sourceFileStr));
        }
        catch(FileNotFoundException ex)
        {     //file not found; log error message
          logObj.warning(logWarningPrefixStr + ex);
          return;                 //abort method
        }
              //set handle to stream to be closed later:
        openedInputFileStm = localSrcStmObj;
      }
      if(sourceDataLen > 0)                 //if source length given then
        localSrcDataLen = sourceDataLen;    //set local variable to length
      else
      {  //source data length not given
        try
        {         //get length of file:
          localSrcDataLen = (new File(sourceFileStr)).length();
        }
        catch(Exception ex)
        {    //exception error; log message
          logObj.warning(
                logWarningPrefixStr + "Error fetching file length:  " + ex);
          return;                 //abort method
        }
      }
    }
    else
    {    //source file name not given
      if(sourceStmObj == null)
      {  //no source stream
        logObj.warning(logWarningPrefixStr + "No source data given");
        return;                   //abort method
      }
      localSrcStmObj = sourceStmObj;        //set local handle to src stream
      localSrcDataLen = sourceDataLen;      //set local variable to length
      localSrcFileStr = remoteFilePathStr;  //use same name as destination
      logSrcStr = "<stream>";                    //set name for log msg
    }
    logObj.debug3(getName() + ":  Sending " + logSrcStr + " (size=" +
               localSrcDataLen + ") to \"" + hostInfoObj.usernameStr + "@" +
                  hostInfoObj.hostNameStr + ":" + remoteFilePathStr + "\"");

         //setup authorization key file:
    final JSch jschObj = new JSch();
    if(hostInfoObj.authKeyFileStr != null &&
                             hostInfoObj.authKeyFileStr.trim().length() > 0)
    {    //authorization key file name is available
      try
      {       //enter authorization key file name:
        jschObj.addIdentity(hostInfoObj.authKeyFileStr);
      }
      catch(Throwable ex)
      {       //error entering key file name; ignore and more on
      }
    }
         //if checking for host "known" and known-hosts file
         // name not null then enter known-hosts file name:
    if(hostInfoObj.checkHostFlag && hostInfoObj.knownHostsFileStr != null)
      jschObj.setKnownHosts(hostInfoObj.knownHostsFileStr);

         //create JSch session object:
    remoteSessionObj = jschObj.getSession(
                            hostInfoObj.usernameStr,hostInfoObj.hostNameStr,
                                                   hostInfoObj.hostPortNum);

    if(checkTerminated())         //if thread terminated then
      return;                     //exit method immediately

    if(!hostInfoObj.checkHostFlag)
    {    //not checking if host is "known"
      final Properties propsObj = new Properties();
      propsObj.put("StrictHostKeyChecking","no");     //setup property
      remoteSessionObj.setConfig(propsObj);           //enter property
    }

         //if password provided then enter it in session object:
    if(hostInfoObj.passwordStr!=null && hostInfoObj.passwordStr.length()>0)
      remoteSessionObj.setPassword(hostInfoObj.passwordStr);
    remoteSessionObj.connect();        //make connection to remote host

    if(checkTerminated())         //if thread terminated then
      return;                     //exit method immediately

         //setup to execute 'scp -t remoteFilePathStr' remotely:
    final Channel channelObj = remoteSessionObj.openChannel("exec");
    if(checkTerminated())         //if thread terminated then
      return;                     //exit method immediately
    ((ChannelExec)channelObj).setCommand(
                                     SCP_SENDCMD_STR + remoteFilePathStr);
    if(checkTerminated())         //if thread terminated then
      return;                     //exit method immediately

       //get I/O streams for remote 'scp' execution:
    final OutputStream remOutStm;
    try
    {  //get output stream for remote host:
      remOutStm = channelObj.getOutputStream();
    }
    catch(IOException ex)
    {  //I/O error; log error message
      logObj.warning(logWarningPrefixStr +
                   "Error getting output stream for remote host:  " + ex);
      return;                   //abort method
    }
    if(checkTerminated())         //if thread terminated then
      return;                     //exit method immediately
    try
    {  //get input stream for remote host:
      remoteInputStm = channelObj.getInputStream();
    }
    catch(IOException ex)
    {  //I/O error; log error message
      logObj.warning(logWarningPrefixStr +
                    "Error getting input stream for remote host:  " + ex);
      return;                   //abort method
    }
    if(checkTerminated())         //if thread terminated then
      return;                     //exit method immediately

    channelObj.connect();       //connect to remote channel
    if(checkTerminated())         //if thread terminated then
      return;                     //exit method immediately
    if(!checkRemoteAck())       //check acknowledgement from remote host
      return;         //if acknowledgement failed then abort method

       //create file object for parsing file name without path:
    final File fileObj = new File(localSrcFileStr);
       //setup "C0644 filesize filename" command, where
       // 'filename' should not contain leading path:
    final String cmdStr = C0644_SENDCMD_STR + localSrcDataLen + " " +
                                                 fileObj.getName() + "\n";
    try
    {  //send command to 'scp' channel on remote host:
      remOutStm.write(cmdStr.getBytes());
      remOutStm.flush();
    }
    catch(IOException ex)
    {  //I/O error; log error message
      logObj.warning(logWarningPrefixStr +
                      "Error setting up transfer to remote host:  " + ex);
      return;                   //abort method
    }
    if(checkTerminated())         //if thread terminated then
      return;                     //exit method immediately
    if(!checkRemoteAck())       //check acknowledgement from remote host
      return;         //if acknowledgement failed then abort method

       //fetch and send data from source input stream:
    final byte [] transBuff = new byte[TRAN_BUFF_LEN];
    int dataLen;
    long byteCount = 0;
    try
    {
      while(true)
      {     //for each buffer of data sent; read data from source stream
        dataLen = localSrcStmObj.read(transBuff,0,TRAN_BUFF_LEN);
        if(dataLen == TRAN_BUFF_LEN)
        {   //full buffer of data received
          remOutStm.write(transBuff);  //send data to remote host
          if(checkTerminated())        //if thread terminated then
            return;                    //exit method immediately
          remOutStm.flush();
          if(checkTerminated())        //if thread terminated then
            return;                    //exit method immediately
          byteCount += TRAN_BUFF_LEN;  //add to byte count
        }
        else
        {   //less than a full buffer of data received
          if(dataLen > 0)
          {      //buffer not empty
            remOutStm.write(transBuff,0,dataLen);   //send to remote host
            if(checkTerminated())      //if thread terminated then
              return;                  //exit method immediately
            remOutStm.flush();
            if(checkTerminated())      //if thread terminated then
              return;                  //exit method immediately
            byteCount += dataLen;      //add to byte count
          }
          break;           //exit loop
        }
      }
      remOutStm.write(0);       //send '\0' to end transaction
      if(checkTerminated())     //if thread terminated then
        return;                 //exit method immediately
      remOutStm.flush();
      if(checkTerminated())     //if thread terminated then
        return;                 //exit method immediately
    }
    catch(IOException ex)
    {  //I/O error; log error message
      logObj.warning(logWarningPrefixStr +
                    "Error transmitting data to remote host (byteCount=" +
                                                 byteCount + "):  " + ex);
      return;                   //abort method
    }
    checkRemoteAck();        //check acknowledgement from remote host
  }

  /**
   * Fetches and checks acknowledgement from remote host.
   * @return true if a success acknowledgement value is received; false if
   * a non-success acknowledgement value is received or if an error occurs.
   */
  protected boolean checkRemoteAck()
  {
    final int retVal;
    try
    {         //fetch return value from remote host:
      retVal = remoteInputStm.read();
    }
    catch(IOException ex)
    {    //error reading return value; log error message
      logObj.warning(logWarningPrefixStr +
            "Error reading acknowledgement value from remote host:  " + ex);
      return false;          //abort method
    }
    if(checkTerminated())    //if thread terminated then
      return false;          //exit method immediately
    // retVal may be 0 for success,
    //               1 for error,
    //               2 for fatal error,
    //              -1

    if(retVal <= 0)          //if returned value == success then
      return true;           //return success flag
         //not success
    if(retVal == 1 || retVal == 2)
    {  //error value returned; fetch message from remote host
      final StringBuffer buff = new StringBuffer();
      try
      {
        int val;
        while(true)
        {   //for each character received
          if((val=remoteInputStm.read()) == '\n')
            break;         //if newline received then exit loop
          if(checkTerminated())   //if thread terminated then
            return false;         //exit method immediately
          buff.append((char)val);
        }
      }
      catch(IOException ex)
      {     //error reading message; log error message
        logObj.warning(logWarningPrefixStr +
                   "Error reading error message from remote host (retVal=" +
                                                      retVal + "):  " + ex);
      }
            //log error message:
      logObj.warning(logWarningPrefixStr + "Error from remote host:  " +
                                                    buff.toString().trim());
    }
    else
    {  //unknown value returned; log error message
      logObj.warning(logWarningPrefixStr + "Unexpected return value (" +
                                             retVal + ") from remote host");
    }
    return false;
  }

  /**
   * Tests if this thread is terminated.  If terminated then a message
   * is logged.
   * @return true if this thread is terminated; false otherwise.
   */
  protected boolean checkTerminated()
  {
    if(isTerminated())
    {    //thread has been terminated
      logObj.debug(getName() + " thread terminated");
      return true;
    }
    return false;            //thread not terminated
  }


  /**
   * Sets the prefix string for log-warning messages.
   * @param str prefix string to use.
   */
  public static void setLogWarningPrefixStr(String str)
  {
    if(str != null)
      logWarningPrefixStr = str;
  }

  /**
   * Returns the prefix string for log-warning messages.
   * @return The prefix string for log-warning messages.
   */
  public static String getLogWarningPrefixStr()
  {
    return logWarningPrefixStr;
  }

  /**
   * Sets the default name of the authorization key file to be used when
   * connecting to the remote host.  If this method is not called then
   * the authorization key file name "$HOME/.ssh/id_rsa" will be used.
   * @param fNameStr default name of authorization key file, or null for
   * no default authorization key file name.
   */
  public static void setDefAuthKeyFile(String fNameStr)
  {
    defAuthKeyFileNameStr = fNameStr;
  }

  /**
   * Returns the default name of the authorization key file to be used when
   * connecting to the remote host.
   * @return Default name of authorization key file, or null if
   * no default authorization key file name.
   */
  public static String getDefAuthKeyFile()
  {
    return defAuthKeyFileNameStr;
  }

  /**
   * Sets the default name of the known-hosts file to be used when
   * connecting to the remote host.  If this method is not called then
   * the known-hosts file name "$HOME/.ssh/known_hosts" will be used.
   * @param fNameStr default name of known-hosts file, or null for
   * no default known-hosts file name.
   */
  public static void setDefKnownHostsFileNameStr(String fNameStr)
  {
    defKnownHostsFileNameStr = fNameStr;
  }

  /**
   * Returns the default name of the known-hosts file to be used when
   * connecting to the remote host.
   * @return Default name of known-hosts file, or null if no default
   * known-hosts file name.
   */
  public static String getDefKnownHostsFileNameStr()
  {
    return defKnownHostsFileNameStr;
  }


/*
  public static void main(String [] args)
  {
    final String DOTEST_CMDARG_STR = "doTestData";
    final String CHKHOST_CMDARG_STR = "-c";
    boolean checkHostFlag = false;

    final java.util.List argsList = new java.util.ArrayList(
                                             java.util.Arrays.asList(args));
    if(argsList.remove(CHKHOST_CMDARG_STR))
    {
      checkHostFlag = true;
      args = (String [])(argsList.toArray(new String[argsList.size()]));
    }

    final int atPos,colonPos;
    if(args.length < 2 || (atPos=args[1].indexOf('@')) <= 0 ||
                                   (colonPos=args[1].indexOf(':')) <= atPos)
    {
      System.out.println("Usage:  ScpFileSender [" + CHKHOST_CMDARG_STR +
                    "] filename " + "user@hostname:/remotefile [password]");
      System.out.println("Usage:  ScpFileSender [" + CHKHOST_CMDARG_STR +
                                                  "] " + DOTEST_CMDARG_STR +
                                   " user@hostname:/remotefile [password]");
      System.out.println("  where '" + CHKHOST_CMDARG_STR +
                                               "' == check for known host");
      return;
    }
    final String hostNameStr = args[1].substring(atPos+1,colonPos);
    final String usernameStr = args[1].substring(0,atPos);
    final String remoteFileNameStr = args[1].substring(colonPos+1);
    final String passwordStr = (args.length >= 3) ? args[2] : null;
    final ScpHostInfo hostInfoObj =
         new ScpHostInfo(hostNameStr,usernameStr,passwordStr,checkHostFlag);
    final LogFile logFileObj = LogFile.getConsoleLogObj();
    if(DOTEST_CMDARG_STR.equals(args[0]))
    {
      final String tDataStr = "This is test data from 'ScpFileSender'\n" +
                                   "and this is a second line of test data";
      send(null,tDataStr.getBytes(),remoteFileNameStr,hostInfoObj,
                                                                 logFileObj);
    }
    else
    {
      send(args[0],remoteFileNameStr,hostInfoObj,logFileObj);
    }
  }
*/


  /**
   * Class ScpHostInfo defines a block of host information for 'scp'
   * transfers.
   */
  public static class ScpHostInfo
  {
    /** Host name. */
    public final String hostNameStr;
    /** Host port number. */
    public final int hostPortNum;
    /** User name. */
    public final String usernameStr;
    /** Password. */
    public final String passwordStr;
    /** true to check if host is "known"; false to accept all hosts. */
    public final boolean checkHostFlag;
    /** Name of authorization key file. */
    public final String authKeyFileStr;
    /** Name of known-hosts file. */
    public final String knownHostsFileStr;

    /**
     * Creates a block of host information for 'scp' transfers.
     * @param hostNameStr host name to use.
     * @param hostPortNum host port number to use.
     * @param usernameStr user name to use.
     * @param passwordStr password to use, or null for none.
     * @param checkHostFlag true to check if host is listed among those
     * in the "$HOME/.ssh/known_hosts" file; false to accept all hosts.
     * @param authKeyFileStr name of authorization key file, or null or
     * an empty string for default value.
     * @param knownHostsFileStr name of known-hosts file, or null or
     * an empty string for default value.
     */
    public ScpHostInfo(String hostNameStr, int hostPortNum,
              String usernameStr, String passwordStr, boolean checkHostFlag,
                            String authKeyFileStr, String knownHostsFileStr)
    {
      this.hostNameStr = hostNameStr;
      this.hostPortNum = hostPortNum;
      this.usernameStr = usernameStr;
      this.passwordStr = passwordStr;
      this.checkHostFlag = checkHostFlag;
      this.authKeyFileStr =       //if not given then use default
            (authKeyFileStr != null && authKeyFileStr.trim().length() > 0) ?
                                       authKeyFileStr : getDefAuthKeyFile();
      this.knownHostsFileStr = (knownHostsFileStr != null &&
                                    knownHostsFileStr.trim().length() > 0) ?
                          knownHostsFileStr : getDefKnownHostsFileNameStr();
    }

    /**
     * Creates a block of host information for 'scp' transfers.
     * @param hostNameStr host name to use.
     * @param usernameStr user name to use.
     * @param passwordStr password to use, or null for none.
     * @param checkHostFlag true to check if host is listed among those
     * in the "$HOME/.ssh/known_hosts" file; false to accept all hosts.
     * @param authKeyFileStr name of authorization key file, or null or
     * an empty string for default value.
     * @param knownHostsFileStr name of known-hosts file, or null or
     * an empty string for default value.
     */
    public ScpHostInfo(String hostNameStr, String usernameStr,
                                  String passwordStr, boolean checkHostFlag,
                            String authKeyFileStr, String knownHostsFileStr)
    {
      this(hostNameStr,DEF_HOSTPORT_NUM,usernameStr,passwordStr,
                            checkHostFlag,authKeyFileStr,knownHostsFileStr);
    }

    /**
     * Creates a block of host information for 'scp' transfers.
     * @param hostNameStr host name to use.
     * @param hostPortNum host port number to use.
     * @param usernameStr user name to use.
     * @param passwordStr password to use, or null for none.
     * @param checkHostFlag true to check if host is listed among those
     * in the "$HOME/.ssh/known_hosts" file; false to accept all hosts.
     * @param authKeyFileStr name of authorization key file, or null or
     * an empty string for default value.
     */
    public ScpHostInfo(String hostNameStr, int hostPortNum,
              String usernameStr, String passwordStr, boolean checkHostFlag,
                                                      String authKeyFileStr)
    {
      this(hostNameStr,hostPortNum,usernameStr,passwordStr,checkHostFlag,
                                                       authKeyFileStr,null);
    }

    /**
     * Creates a block of host information for 'scp' transfers.
     * @param hostNameStr host name to use.
     * @param usernameStr user name to use.
     * @param passwordStr password to use, or null for none.
     * @param checkHostFlag true to check if host is listed among those
     * in the "$HOME/.ssh/known_hosts" file; false to accept all hosts.
     * @param authKeyFileStr name of authorization key file, or null or
     * an empty string for default value.
     */
    public ScpHostInfo(String hostNameStr, String usernameStr,
           String passwordStr, boolean checkHostFlag, String authKeyFileStr)
    {
      this(hostNameStr,DEF_HOSTPORT_NUM,usernameStr,passwordStr,
                                         checkHostFlag,authKeyFileStr,null);
    }

    /**
     * Creates a block of host information for 'scp' transfers.
     * @param hostNameStr host name to use.
     * @param hostPortNum host port number to use.
     * @param usernameStr user name to use.
     * @param passwordStr password to use, or null for none.
     * @param checkHostFlag true to check if host is listed among those
     * in the "$HOME/.ssh/known_hosts" file; false to accept all hosts.
     */
    public ScpHostInfo(String hostNameStr, int hostPortNum,
              String usernameStr, String passwordStr, boolean checkHostFlag)
    {
      this(hostNameStr,hostPortNum,usernameStr,passwordStr,checkHostFlag,
                                                                 null,null);
    }

    /**
     * Creates a block of host information for 'scp' transfers.
     * @param hostNameStr host name to use.
     * @param usernameStr user name to use.
     * @param passwordStr password to use, or null for none.
     * @param checkHostFlag true to check if host is listed among those
     * in the "$HOME/.ssh/known_hosts" file; false to accept all hosts.
     */
    public ScpHostInfo(String hostNameStr, String usernameStr,
                                  String passwordStr, boolean checkHostFlag)
    {
      this(hostNameStr,DEF_HOSTPORT_NUM,usernameStr,passwordStr,
                                                   checkHostFlag,null,null);
    }

    /**
     * Creates a block of host information for 'scp' transfers.
     * @param hostNameStr host name to use.
     * @param hostPortNum host port number to use.
     * @param usernameStr user name to use.
     * @param passwordStr password to use, or null for none.
     * @param authKeyFileStr name of authorization key file, or null or
     * an empty string for default value.
     * @param knownHostsFileStr name of known-hosts file, or null or
     * an empty string for default value.
     */
    public ScpHostInfo(String hostNameStr, int hostPortNum,
                                     String usernameStr, String passwordStr,
                            String authKeyFileStr, String knownHostsFileStr)
    {
      this(hostNameStr,hostPortNum,usernameStr,passwordStr,DEF_CHKHOST_FLAG,
                                          authKeyFileStr,knownHostsFileStr);
    }

    /**
     * Creates a block of host information for 'scp' transfers.
     * @param hostNameStr host name to use.
     * @param usernameStr user name to use.
     * @param passwordStr password to use, or null for none.
     * @param authKeyFileStr name of authorization key file, or null or
     * an empty string for default value.
     * @param knownHostsFileStr name of known-hosts file, or null or
     * an empty string for default value.
     */
    public ScpHostInfo(String hostNameStr, String usernameStr,
        String passwordStr, String authKeyFileStr, String knownHostsFileStr)
    {
      this(hostNameStr,DEF_HOSTPORT_NUM,usernameStr,passwordStr,
                         DEF_CHKHOST_FLAG,authKeyFileStr,knownHostsFileStr);
    }

    /**
     * Creates a block of host information for 'scp' transfers.
     * @param hostNameStr host name to use.
     * @param hostPortNum host port number to use.
     * @param usernameStr user name to use.
     * @param passwordStr password to use, or null for none.
     * @param authKeyFileStr name of authorization key file, or null or
     * an empty string for default value.
     */
    public ScpHostInfo(String hostNameStr, int hostPortNum,
              String usernameStr, String passwordStr, String authKeyFileStr)
    {
      this(hostNameStr,hostPortNum,usernameStr,passwordStr,DEF_CHKHOST_FLAG,
                                                       authKeyFileStr,null);
    }

    /**
     * Creates a block of host information for 'scp' transfers.
     * @param hostNameStr host name to use.
     * @param usernameStr user name to use.
     * @param passwordStr password to use, or null for none.
     * @param authKeyFileStr name of authorization key file, or null or
     * an empty string for default value.
     */
    public ScpHostInfo(String hostNameStr, String usernameStr,
                                  String passwordStr, String authKeyFileStr)
    {
      this(hostNameStr,DEF_HOSTPORT_NUM,usernameStr,passwordStr,
                                      DEF_CHKHOST_FLAG,authKeyFileStr,null);
    }

    /**
     * Creates a block of host information for 'scp' transfers.
     * @param hostNameStr host name to use.
     * @param hostPortNum host port number to use.
     * @param usernameStr user name to use.
     * @param passwordStr password to use, or null for none.
     */
    public ScpHostInfo(String hostNameStr, int hostPortNum,
                                     String usernameStr, String passwordStr)
    {
      this(hostNameStr,hostPortNum,usernameStr,passwordStr,DEF_CHKHOST_FLAG,
                                                                 null,null);
    }

    /**
     * Creates a block of host information for 'scp' transfers.
     * @param hostNameStr host name to use.
     * @param usernameStr user name to use.
     * @param passwordStr password to use, or null for none.
     */
    public ScpHostInfo(String hostNameStr, String usernameStr,
                                                         String passwordStr)
    {
      this(hostNameStr,DEF_HOSTPORT_NUM,usernameStr,passwordStr,
                                                DEF_CHKHOST_FLAG,null,null);
    }

    /**
     * Creates a block of host information for 'scp' transfers.
     * @param hostNameStr host name to use.
     * @param hostPortNum host port number to use.
     * @param usernameStr user name to use.
     */
    public ScpHostInfo(String hostNameStr, int hostPortNum,
                                                         String usernameStr)
    {
      this(hostNameStr,hostPortNum,usernameStr,null,DEF_CHKHOST_FLAG,
                                                                 null,null);
    }

    /**
     * Creates a block of host information for 'scp' transfers.
     * @param hostNameStr host name to use.
     * @param usernameStr user name to use.
     */
    public ScpHostInfo(String hostNameStr, String usernameStr)
    {
      this(hostNameStr,DEF_HOSTPORT_NUM,usernameStr,null,DEF_CHKHOST_FLAG,
                                                                 null,null);
    }
  }
}
