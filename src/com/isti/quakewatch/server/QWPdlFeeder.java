//QWPdlFeeder.java:  QuakeWatch feeder plugin module that receives PDL
//                   messages and passes them into the QuakeWatch server.
//
//  5/31/2013 -- [ET]  Initial 1.0 version.
//  7/12/2013 -- [ET]  Version 1.1:  Added 'pdlListenerIndex'.
//   8/1/2013 -- [ET]  Version 1.2:  Modified to receive 'internal-origin'
//                     products; added 'pdlMaxTries' and 'pdlRetryDelay'
//                     configuration parameters.
//  8/14/2013 -- [ET]  Version 1.3:  Added workaround to 'setupPdlLogging()'
//                     method to squelch three types of PDL warnings (so
//                     they won't end up triggering spurious alerts in
//                     server status monitoring); modified to log PDL
//                     version at startup.
//  9/19/2013 -- [ET]  Version 1.4:  Added 'getPdlVersionStr()' method;
//                     in method 'setupPdlLogging()' added "unexpected
//                     magnitude type" to PDL warnings to be squelched.
//  9/24/2013 -- [ET]  Version 1.5:  Modified 'setupPdlLogging()' method
//                     so squelching of PDL-warning messages uses
//                     'startsWith()' instead of 'equalsIgnoreCase()';
//                     modified so generated 'ProductLink' messages will
//                     have 'Version' and 'Action' elements inside the
//                     'ProductLink' element (instead of parent 'Event'
//                     element).
//  4/26/2017 -- [KF]  Version 1.51:  Replaced QWPdlFeeder 'dyfiUrlPrefixStr'
//                     and 'dyfiUrlSuffixStr' with 'dyfiUrlFormatStr'.
//  1/30/2018 -- [KF]  Version 1.52:
//                     Removed 1024 byte restriction for text products.
//  3/19/2020 -- [KF]  Version 1.53:
//                     Added 'pdlFilterWarn' property.
// 10/14/2020 -- [KF]  Version 1.6:
//                     Modified to detect an issue with the PDL feeder so
//                     that it can restart.
// 10/16/2020 -- [KF]  Version 1.61:
//                     Fixed continuous restart issue with the PDL feeder.
//  2/10/2021 -- [KF]  Version 1.62:
//                     Added losspager and moment-tensor products;
//                     Fixed logging.
//

package com.isti.quakewatch.server;

import java.util.Date;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicBoolean;

import org.jdom.Element;

import com.isti.quakewatch.common.MsgTag;
import com.isti.quakewatch.server.pdl.PdlClient;
import com.isti.quakewatch.server.pdl.PdlClientCallback;
import com.isti.quakewatch.server.pdl.QWConvMsgRecord;
import com.isti.quakewatch.server.pdl.QWPdlFeederConfig;
import com.isti.util.IstiNotifyThread;
import com.isti.util.TimeConstants;
import com.isti.util.UtilFns;

/**
 * Class QWPdlFeeder is a QuakeWatch feeder plugin module that receives PDL
 * messages (via methods in the PDL ProductClient.jar file) and passes them
 * into the QuakeWatch server.
 */
public class QWPdlFeeder extends QWAbstractFeeder implements PdlClientCallback, TimeConstants
{
  public static final String MODULE_NAME = "QWPdlFeeder";
  public static final String VERSION = "1.62";    //name and version strings
  public static final String FEEDER_NAME = "PDL";     //feeder name
  protected final String feederModuleName;
  protected final String feederRevisionStr;
  protected boolean errMsgFlag = false;
  private final QWPdlFeederConfig config = new QWPdlFeederConfig();
  private volatile PdlClient _pdlClient;
  private final AtomicBoolean pdlstop = new AtomicBoolean();
  protected final MsgRecDispatcher msgRecDispatcherObj =
                                                     new MsgRecDispatcher();
  
  /**
   * Constructs a PDL feeder module.
   */
  public QWPdlFeeder()
  {
    this(MODULE_NAME,FEEDER_NAME,VERSION);
  }

  /**
   * Constructs a PDL feeder module.
   * @param moduleName name of the module.
   * @param feederName name of the feeder.
   * @param versionStr version string for feeder.
   */
  protected QWPdlFeeder(String moduleName, String feederName,
                                                          String versionStr)
  {
    super(moduleName);             //set thread name to the module name
    feederModuleName = moduleName; //set the module name
    this.feederName = feederName;  //set the feeder name
                                   //build revision string for feeder:
    feederRevisionStr = moduleName + ", Version " + versionStr;
  }

  /**
   * Constructs a PDL feeder module.
   * @param moduleName name of the module.
   * @param feederName name of the feeder.
   */
  protected QWPdlFeeder(String moduleName, String feederName)
  {
    this(moduleName,feederName,VERSION);
  }

  /**
   * Returns the revision String for the feeder.
   * @return the revision String for the feeder.
   */
  public String getRevisionString()
  {
    return feederRevisionStr;
  }

  private boolean isStopped() 
  {
    if (pdlstop.get())
        return true;
    if (isInterrupted())
      return true;
    return false;
  }

  /**
   * Processes feeder-specific settings.  The settings are usually
   * fetched from the "QWFeederSettings" element in the QuakeWatch-server
   * configuration file.  (For 'QWFeederPlugin' interface.)
   * @param settingsStr The settings string to be processed.
   * @throws QWFeederInitException if an error occurs while processing
   * the settings.
   */
  public void processFeederSettings(String settingsStr)
                                                throws QWFeederInitException
  {
    String msgStr;
    try
    {         //load configuration properties from the given string:
      if(config.load(settingsStr))
      {  //successfully loaded settings
        return;
      }
         //error loading settings; get error message
      msgStr = config.getErrorMessage();
    }
    catch(Exception ex)
    {    //some kind of exception error; build error message
      msgStr = "Error processing settings:  " + ex;
    }
         //throw feeder exception with error message:
    throw new QWFeederInitException(msgStr);
  }

  /**
   * Stops the PDL-receiver and message-dispatch thread of the feeder
   * (if running).  (For 'QWFeederPlugin' interface.)
   */
  public void stopFeeder()
  {
    if (pdlstop.getAndSet(true)) // exit if already stopped
    {
      return;
    }
    try
    {
      if(logObj != null)
        logObj.debug("Stopping feeder");
      final PdlClient pdlClient = _pdlClient;
      if (pdlClient != null)
      {
        pdlClient.shutdown();
      }
      msgRecDispatcherObj.close();     //shut down dispatcher
      super.stopFeeder();              //do parent shutdown (close log file)
    }
    catch(Exception ex)
    {  //some kind of exception error; log it (if possible)
      if(logObj != null && logObj.isOpen())
      {  //log file is still open
        logObj.warning(feederModuleName + " (\"" + feederName + "\"):  " +
                                           "Error stopping feeder:  " + ex);
        logObj.warning(UtilFns.getStackTraceString(ex));
      }
      else
        ex.printStackTrace();
    }
  }

  /**
   * Executing method for the feeder thread.  Starts execution of PDL
   * client receiver.
   */
  public void run()
  {
    runNow();
    final long restartDelay = config.pdlRestartDelayProp.longValue();
    if (restartDelay <= 0 || isStopped())
    {
      return;
    }
    do
    {
      try
      {
        sleep(restartDelay);
      }
      catch (Exception ex)
      {
        return;
      }
      if (isStopped())
      {
        return;
      }
      logObj.info(feederModuleName + " (\"" + feederName +
          "\"):  starting PDL-client receiver again");
      runNow();
    }
    while (!isStopped());
  }
  
  private void runNow()
  {
    if (pdlstop.get() || _pdlClient != null) // exit if stopped or client exists
    {
      return;
    }
    getLogFileObj(); // create log file if needed
    final PdlClient pdlClient = new PdlClient(config, this, logObj);
    _pdlClient = pdlClient;
    try
    {
      final long pdlDataTimeout = config.pdlDataTimeoutProp.longValue();
      pdlClient.startup();
      if (pdlDataTimeout <= 0) // exit if no timeout
      {
        return;
      }
      logObj.info(feederModuleName + " (\"" + feederName
          + "\"):  Started data timeout check for PDL-client receiver ("
          + pdlDataTimeout + ")");
      long time, now;
      long lastTime = System.currentTimeMillis();
      // check for data message timeout
      while (!isStopped() && !pdlClient.isShutdown())
      {
        sleep(MS_PER_SECOND);
        if (isStopped() || pdlClient.isShutdown())
        {
          return;
        }
        now = System.currentTimeMillis();
        time = getLastDataMsgTime();
        if (time != 0)
        {
          lastTime = time;
        }
        if (now > lastTime + pdlDataTimeout)
        {
          logObj.warning(feederModuleName + " (\"" + feederName +
              "\"):  Data timeout for PDL-client receiver");
          pdlClient.shutdown();
          clearLastDataMsgTime();
        }
      }
    }
    catch(Exception ex)
    {  //some kind of exception error; log message
      logObj.warning(feederModuleName + " (\"" + feederName +
                        "\"):  Error starting PDL-client receiver:  " + ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
    finally
    {
      _pdlClient = null;
    }
  }

  /**
   * Class MsgRecDispatcher manages a sorted queue of 'QWConvMsgRecord'
   * objects, sorted by 'update' time.  While 'fetchModeFlag'==true,
   * received message records are delayed by FETCH_DELAY_MS before being
   * dispatched (to allow for out-of-sequence-order messages).  When
   * 'fetchModeFlag' is set to 'false', all queued message records are
   * dispatched and all proceeding messages are dispatch immediately after
   * being received.  The 'fetchModeFlag' value is set to 'false' after
   * a received message record has an update time after the marked startup
   * time for this dispatcher.
   */
  protected class MsgRecDispatcher
  {
    protected final long startupTimeVal;
    protected final TreeSet<QWConvMsgRecord> sortedRecQueueObj =
                                             new TreeSet<QWConvMsgRecord>();
    protected static final long FETCH_DELAY_MS =
                                           10 * TimeConstants.MS_PER_MINUTE;
    protected boolean fetchModeFlag = true;   //start with fetching old msgs
    protected final IstiNotifyThread queueThreadObj;
    
    /**
     * Creates a message-record dispatcher.
     */
    public MsgRecDispatcher()
    {
      startupTimeVal = System.currentTimeMillis();    //mark startup time
                   //create dispatch-queue thread:
      queueThreadObj = new IstiNotifyThread("QWPdlFeederDispatcher")
          {
            public void run()
            {
              dispatchQueueRun();
            }
          };
    }
    
    /**
     * Pushes a message record out for dispatch.
     * @param cnvMsgRecObj message-record object.
     */
    public void push(QWConvMsgRecord cnvMsgRecObj)
    {
      if(fetchModeFlag)
      {  //historical messages are being fetched and dispatched
        synchronized(sortedRecQueueObj)
        {  //grab thread lock for queue
          if(fetchModeFlag)  //make sure mode flag still set
          {  //flag still set; enter enqueued time into msg record
            cnvMsgRecObj.setEnqueuedTimeVal(System.currentTimeMillis());
            sortedRecQueueObj.add(cnvMsgRecObj);   //add to queue
            if(logObj != null)
            {
              logObj.debug3("MsgRecDispatcher:  Added message to queue " +
                         "(size=" + sortedRecQueueObj.size() + "), rec:  " +
                                                              cnvMsgRecObj);
            }
            if(!queueThreadObj.isAlive())   //if not yet started then
              queueThreadObj.start();       //start dispatch-queue thread
            return;
          }
        }
      }
         //'live' messages are being fetched and dispatched
      dispatchMessage(cnvMsgRecObj);      //dispatch immediately
    }
    
    /**
     * Closes this message-record dispatcher.  The dispatch-queue thread
     * is terminated (if still running) and any records in the queue
     * are dispatched.
     */
    public void close()
    {
      queueThreadObj.terminate();
      synchronized(sortedRecQueueObj)
      {  //grab thread lock for queue
        if(!sortedRecQueueObj.isEmpty())
        {  //queue not empty
          if(logObj != null)
          {
            logObj.debug("MsgRecDispatcher:  Dispatching queue " +
                          "before close, size=" + sortedRecQueueObj.size());
          }
                   //dispatch all message objects in queue:
          for(QWConvMsgRecord recObj : sortedRecQueueObj)
            dispatchUnqueuedMsg(recObj);
          sortedRecQueueObj.clear();        //clear queue
          if(logObj != null)
            logObj.debug("MsgRecDispatcher:  Finished dispatching queue");
        }
      }
    }
    
    /**
     * Performs the dispatch-queue thread actions.
     */
    protected void dispatchQueueRun()
    {
      if(logObj != null)
        logObj.debug("MsgRecDispatcher dispatch-queue thread starting");
      try
      {
        int repCount = 0;
        while(!queueThreadObj.isTerminated())
        {  //loop while thread is not terminated
          synchronized(sortedRecQueueObj)
          {  //grab thread lock for queue
            if(!sortedRecQueueObj.isEmpty())
            {  //queue not empty
              final QWConvMsgRecord lastRecObj = sortedRecQueueObj.last();
              final long lastRecUpdVal = lastRecObj.getUpdateTimeVal();
              if(lastRecUpdVal <= startupTimeVal)
              {  //newest message is not after startup time
                final long curTimeVal = System.currentTimeMillis();
                        //iterate over copy of queue (to allow removals):
                for(QWConvMsgRecord recObj :
                            new TreeSet<QWConvMsgRecord>(sortedRecQueueObj))
                {  //for each message record in queue
                  if(lastRecUpdVal - recObj.getUpdateTimeVal() >
                                                             FETCH_DELAY_MS)
                  {  //current-record update time is older than newest
                     // record in queue by more than 'FETCH_DELAY_MS'
                                                 //remove record from queue:
                    sortedRecQueueObj.remove(recObj);
                    dispatchUnqueuedMsg(recObj); //dispatch message record
                  }
                  else if(recObj.enqueuedTimeBefore(
                                                 curTimeVal-FETCH_DELAY_MS))
                  {  //record has been in queue longer than 'FETCH_DELAY_MS'
                    if(logObj != null)
                    {
                      logObj.debug("MsgRecDispatcher:  Dispatching msgRec " +
                                 "after waiting in queue for longer than " +
                                         FETCH_DELAY_MS + "ms:  " + recObj);
                    }
                                                 //remove record from queue:
                    sortedRecQueueObj.remove(recObj);
                    dispatchUnqueuedMsg(recObj); //dispatch message record
                  }
                  else         //record not in queue too long; stop checking
                    break;     //exit loop
                }
              }
              else
              {  //newest message is after startup time
                if(logObj != null)
                {
                  logObj.debug("MsgRecDispatcher:  Newest message after " +
                       "startup time; changing to 'live' mode; queueSize=" +
                                                  sortedRecQueueObj.size());
                }
                        //dispatch all message objects in queue:
                for(QWConvMsgRecord recObj : sortedRecQueueObj)
                  dispatchUnqueuedMsg(recObj);
                if(logObj != null && !sortedRecQueueObj.isEmpty())
                  logObj.debug("MsgRecDispatcher:  Queue msgs dispatched");
                sortedRecQueueObj.clear();  //clear queue
                fetchModeFlag = false;      //change to 'live' mode
                break;                      //exit loop (and thread)  
              }
            }
          }
          if(++repCount >= 60)
          {  //number of iterations reached
            repCount = 0;         //reset count
            if(logObj != null)
            {                     //show queue size:
              logObj.debug("MsgRecDispatcher:  QueueSize=" +
                                                  sortedRecQueueObj.size());
            }
          }
          queueThreadObj.waitForNotify(1000);    //delay between checks
        }
      }
      catch(Exception ex)
      {  //some kind of exception error; log it and exit thread
        if(logObj != null)
        {
          logObj.warning(feederModuleName + " (\"" + feederName +
                                  "\"):  Error processing dispatch queue " +
                                             "(thread terminated):  " + ex);
          logObj.warning(UtilFns.getStackTraceString(ex));
        }
        else
          ex.printStackTrace();
      }
      if(logObj != null)
        logObj.debug("MsgRecDispatcher dispatch-queue thread exiting");
    }
  
    /**
     * Dispatches the given unqueued message to the QWServer call-back
     * object.  If the message is older than the latest server message
     * then the message update time will be set to the update time for
     * the last server message.
     * @param cnvMsgRecObj message-record object.
     */
    protected void dispatchUnqueuedMsg(QWConvMsgRecord cnvMsgRecObj)
    {
              //get time of last message sent to server (0 if none)
      final long svrLastUpdVal = callbackObj.getLastTimeGeneratedVal();
      if(cnvMsgRecObj.getUpdateTimeVal() < svrLastUpdVal)
      {  //message is too old; change its update-time value
        cnvMsgRecObj.setUpdateTimeObj(new Date(svrLastUpdVal));
        if(logObj != null)
        {
          logObj.debug("MsgRecDispatcher:  Changed update time to " +
                           svrLastUpdVal + " for msgRec:  " + cnvMsgRecObj);
        }
      }
      dispatchMessage(cnvMsgRecObj);   //dispatch message to server
    }
  
    /**
     * Dispatches the given message to the QWServer call-back object.
     * @param cnvMsgRecObj message-record object.
     */
    protected void dispatchMessage(QWConvMsgRecord cnvMsgRecObj)
    {
      try
      {            //fetch 'DataMessage' child element:
        final Element dataMsgElement;
        if((dataMsgElement=cnvMsgRecObj.getDataMsgElement()) == null)
        {  //unable to fetch 'QWmessage' element
          if(logObj != null)
          {
            logObj.warning(feederModuleName + " (\"" + feederName +
                              "\"):  Unable to get 'DataMessage' element " +
                            "for generated message, rec:  " + cnvMsgRecObj);
          }
          return;
        }
              //enter PDL-Product msgNum/updateTime as RelayMsgNum attribute:
        dataMsgElement.setAttribute(MsgTag.RELAY_MSG_NUM,
                            Long.toString(cnvMsgRecObj.getMessageNumVal()));
        if(logObj != null)
        {
          logObj.debug3("MsgRecDispatcher:  Dispatching message to server" +
                                                 ", rec:  " + cnvMsgRecObj);
        }
        dataMsgElement.detach();         //make sure element has no parent
                //pass event message on to server, via call-back:
        callbackObj.dataEvent(feederDomainNameStr,feederTypeNameStr,null,
                             dataMsgElement,cnvMsgRecObj.getUpdateTimeObj(),
                                         cnvMsgRecObj.getFdrSourceHostStr(),
                                      cnvMsgRecObj.getFdrSourceMsgNumVal());
      }
      catch(Exception ex)
      {  //some kind of exception error; log it
        if(logObj != null)
        {
          logObj.warning(feederModuleName + " (\"" + feederName +
                                      "\"):  Error dispatching message:  " +
                                        ex + ", msgRec:  " + cnvMsgRecObj);
          logObj.warning(UtilFns.getStackTraceString(ex));
        }
        else
          ex.printStackTrace();
      }
    }
  }

  @Override
  public String getFeederModuleName()
  {
	  return feederModuleName;
  }

  @Override
  public void processRecord(QWConvMsgRecord cnvMsgRecObj)
  {
	  updateLastDataMsgTime(); // mark time of data message received
	  if (callbackObj == null) { // callback object handle null
		  if (!errMsgFlag) { // show error message (once)
			  if (logObj != null)
				  logObj.warning(feederModuleName + " (\"" + feederName + "\"):  Callback object not set");
			  errMsgFlag = true;
		  }
		  return;
	  }
	  if (cnvMsgRecObj != null)
	  {
		  if (logObj != null)
			  logObj.debug3("Sending generated message record to dispatcher:  " + cnvMsgRecObj);
		  msgRecDispatcherObj.push(cnvMsgRecObj);
	  }
  }
}
