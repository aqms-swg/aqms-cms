//ServerCacheInfoStrings.java:  Holds static strings containing names
//                              for property values returned via the
//                              'getServerCacheInfoTable()' and
//                              'getServerCacheInfoStr()' methods.
//
//  12/9/2004 -- [ET]
//

package com.isti.quakewatch.server;

/**
 * Class ServerCacheInfoStrings holds static strings containing names
 * for property values returned via the 'getServerCacheInfoTable()' and
 * 'getServerCacheInfoStr()' methods.
 */
public class ServerCacheInfoStrings
{
    /** Time value for first message in cache, in ms since 1/1/1970. */
  public static final String CACHE_FIRSTMSG_TIME = "cacheFirstMsgTime";

    /** Message-number value for first message in cache. */
  public static final String CACHE_FIRSTMSG_NUM = "cacheFirstMsgNum";

    /** Time value for last message in cache, in ms since 1/1/1970. */
  public static final String CACHE_LASTMSG_TIME = "cacheLastMsgTime";

    /** Message-number value for last message in cache. */
  public static final String CACHE_LASTMSG_NUM = "cacheLastMsgNum";

    /** Number of messages in cache. */
  public static final String CACHE_MESSAGE_COUNT = "cacheMessageCount";

    /** Total data size of messages in cache. */
  public static final String CACHE_DATA_SIZE = "cacheDataSize";

    /** Age limit for messages in cache, in milliseconds. */
  public static final String CACHE_MAXAGE_MS = "cacheMaxAgeMs";

    /** Recommended polling interval, in milliseconds. */
  public static final String REC_POLL_INTERVALMS = "recPollItervalMs";

    /** Configured ID name string for server. */
  public static final String SERVER_ID_NAME = "serverIdName";

    /** Program name string for server. */
  public static final String SERVER_PROG_NAME = "serverProgName";

    /** Version number string for server. */
  public static final String SERVER_VERSION_NUM = "serverVersionNum";
}
