//CorbaSenderTest.java:  Test program to send messages via CORBA
//                       methods into the 'QWCorbaFeeder'.
//
//  2/11/2004 -- [ET]  Initial version.
//  5/14/2004 -- [KF]  Added support for reading a message file.
// 12/22/2004 -- [ET]  Made 'fileStrings' not "final" to avoid
//                     "variable not initialized" compile error.
//  4/12/2005 -- [ET]  Modified to prevent null-pointer exception
//                     after <Ctrl-C> entered.
//  4/28/2005 -- [ET]  Modified to send feeder-data-source host name
//                     and message number with message into server.
//  6/25/2005 -- [ET]  Modified to support files containing "bare"
//                     messages (no surrounding "<QWmessage>" elements);
//                     added auto-send option (enabled by entering delay
//                     value via user console input).
// 12/27/2005 -- [ET]  Added "-i" ("senderID") parameter to allow feeder-
//                     data-source host name to be specified.
//   8/1/2008 -- [ET]  Added "-d" ("deliver on msg dates") and
//                     "-g" ("show -d debug") parameters.
//

package com.isti.quakewatch.server.test;

import java.util.Random;
import com.isti.util.UtilFns;
import com.isti.util.FileUtils;
import com.isti.util.Archivable;
import com.isti.openorbutil.OrbManager;
import com.isti.quakewatch.server.QWCorbaFeeder;
import com.isti.quakewatch.server.qw_feeder.QWFeeder;
import com.isti.quakewatch.server.qw_feeder.QWFeederHelper;
import com.isti.quakewatch.server.QWServerMsgRecord;

/**
 * Class CorbaSenderTest test program to send messages via CORBA
 * methods into the 'QWCorbaFeeder'.
 * Usage:  CorbaSenderTest [-n] [-f filename] [hostname] [portnum]
 * where "-n" enables testing of event domain/type names and
 * "-f filename" specifies a message file name.
 */
public class CorbaSenderTest
{
  private final static String FILE_SEP_STR = "<QWmessage";
  private boolean domainTypeNamesFlag = false;
  private boolean deliverOnDatesFlag = false;
  private boolean dOnDatesDebugFlag = false;
  private String fileNameStr = null;
  private String serverHostStr = "localhost";
  private int serverPortNum = QWCorbaFeeder.DEF_PORT_NUM;
  private String [] fileStringsArr;
  private int fileMsgStrIdx = 0;
  private String corbaSenderIDStr;
  private long corbaSenderMsgNum = 0;
  private Archivable.Marker archivableMarkerObj = new Archivable.Marker();
  private boolean firstFetchedMsgFlag = true;

  /**
   * Creates a test program to send messages via CORBA
   * methods into the 'QWCorbaFeeder'.
   * @param programArgs array of command-line arguments.
   */
  public CorbaSenderTest(String [] programArgs)
  {
         //create time value for ID (current time ms - 1/1/2000):
    final long idTmVal = System.currentTimeMillis() - 946684800280L;
         //create unique ID string using current time and random value
         // (to be used as feeder-data-source host name):
    corbaSenderIDStr = "SenderTest" + idTmVal +
                    Integer.toString((new Random(idTmVal)).nextInt(100000));

    String str;
    if(programArgs != null)
    {
      int slotNum = 0;
      for(int i=0; i<programArgs.length; ++i)
      {
        if((str=programArgs[i]).startsWith("-"))
        {
          if("-n".equalsIgnoreCase(str))
            domainTypeNamesFlag = true;
          else if("-f".equals(str) && ++i < programArgs.length)
            fileNameStr = programArgs[i];
          else if("-i".equals(str) && ++i < programArgs.length)
            corbaSenderIDStr = programArgs[i];
          else if("-d".equalsIgnoreCase(str))
            deliverOnDatesFlag = true;
          else if("-g".equalsIgnoreCase(str))
            dOnDatesDebugFlag = true;
          else
          {
            if("-h".equalsIgnoreCase(str))
            {
              System.out.println("Usage:  CorbaSenderTest [-n] " +
                                 "[-f filename] [-d] [-g] [-i senderID] " +
                                                    "[hostname] [portnum]");
              System.out.println("-n = use domain/chan, -d = deliver on " +
                                           "msg dates, -g = show -d debug");
              System.exit(0);
              return;
            }
          }
        }
        else
        {
          if(slotNum == 0)
            serverHostStr = programArgs[i];
          else if(slotNum == 1)
          {
            try
            {
              final int val;
              if((val=Integer.parseInt(programArgs[i])) > 0)
                serverPortNum = val;
              else
              {
                System.err.println("Invalid port number parameter:  " +
                                                            programArgs[i]);
                System.exit(1);
                return;
              }
            }
            catch(NumberFormatException ex)
            {
              System.err.println("Invalid port number parameter:  " +
                                                            programArgs[i]);
              System.exit(1);
              return;
            }
          }
          ++slotNum;
        }
      }
    }

    System.out.println("Server host name = \"" + serverHostStr +
                       "\", port number = " + serverPortNum +
        ((fileNameStr == null)? UtilFns.EMPTY_STRING :
        ", file name = \"" + fileNameStr) + "\"");
    System.out.println("Feeder-data-source host name = \"" +
                                                   corbaSenderIDStr + "\"");

    final OrbManager orbManagerObj = new OrbManager(programArgs);
    orbManagerObj.initImplementation();

    org.omg.CORBA.Object cObj;
    System.out.println("Calling resolveCorbalocObject()");
    if((cObj=orbManagerObj.resolveCorbalocObject(serverHostStr,
                    serverPortNum,QWCorbaFeeder.CORBALOC_REF_NAME)) == null)
    {
      System.err.println("Error resolving Corbaloc object:  " +
                                           orbManagerObj.getErrorMessage());
      return;
    }

    System.out.println("Narrowing QWFeeder object");
    final QWFeeder qwFeederObj = QWFeederHelper.narrow(cObj);

         //if using domain/type names then setup array of "type" names:
    final String [] typeStringsArr = domainTypeNamesFlag ? (new String []
                                     { "first", "second", "third" }) : null;

    if(domainTypeNamesFlag)
      System.out.println("Using event domain/type names");

    if (fileNameStr != null)
    {
      try
      {       //read file data into an array of strings:
        final String fileDataStr = FileUtils.readFileToString(fileNameStr);
                   //if any "<QWmessage" separators found then use it as
                   // the separator; otherwise use newline:
        fileStringsArr = UtilFns.parseSeparatedSubstrings(fileDataStr,
                                                    ((!deliverOnDatesFlag &&
                                   fileDataStr.indexOf(FILE_SEP_STR) >= 0) ?
                                                      FILE_SEP_STR : "\n"));
        if(fileStringsArr == null || fileStringsArr.length <= 0)
        {
          System.out.println("File is empty: " + fileNameStr);
          return;
        }
      }
      catch(Exception ex)
      {
        System.out.println("Error reading file: " + ex);
        return;
      }
    }
    else
    {
      fileStringsArr = null;
      deliverOnDatesFlag = false;
    }

      //create new thread to process user input and send messages:
    (new Thread()
        {
          public void run()
          {
            if (fileStringsArr != null)
            {
              if(!deliverOnDatesFlag)
              {
                System.out.println("Enter messages to be sent or press " +
                                "enter for next message (or 'Q' to quit):");
                System.out.println("Enter delay in ms for auto send");
              }
              else
              {
                System.out.println("CorbaSender:  Delivering messages " +
                                              "from file on message dates");
              }
            }
            else
            {
              System.out.println("Enter messages to be sent " +
                                                       "(or 'Q' to quit):");
            }
            int eventCount = 0, fileDelayVal = -1;
            boolean retFlag;
            String str;
            String fileMsgStr = null;
            boolean sendMsgFlag;
            while(true)
            {
              //if file strings are available and next string needs to be found
              if(fileStringsArr != null && fileMsgStr == null)
              {
                //exit if no more messages
                if((fileMsgStr=getNextDataMessageStr()) == null)
                  break;
              }

              if(!deliverOnDatesFlag && fileDelayVal < 0)
                str = UtilFns.getUserConsoleString();
              else
                str = UtilFns.EMPTY_STRING;
              if(str != null)
              {   //user string fetched from console OK
                str = str.trim();
                if(str.equalsIgnoreCase("Q"))
                  break;         //if keyword then exit loop
                if(fileMsgStr != null && fileMsgStr.length() > 0)
                {  //file string is available
                  if(str.length() <= 0)
                  {     //no data entered via console
                    str = fileMsgStr;  //use the next file string
                    sendMsgFlag = true;
                        //if deliver-on-msg-dates then don't show
                        // messages (unless debug flag):
                    if(!deliverOnDatesFlag || dOnDatesDebugFlag)
                      System.out.println(str);
                    fileMsgStr = null;  //clear to get the next one
                  }
                  else
                  {     //data entered via console
                    try
                    {        //attempt to parse as integer delay value
                      fileDelayVal = Integer.parseInt(str);
                      sendMsgFlag = false;
                    }
                    catch(NumberFormatException ex)
                    {
                      sendMsgFlag = true;
                    }
                  }
                }
                else
                  sendMsgFlag = (str.length() > 0);
                if(sendMsgFlag)
                {
                  try
                  {     //send message to feeder:
                    if(domainTypeNamesFlag)
                    {  //using structured events
                      if(corbaSenderIDStr != null &&
                                       corbaSenderIDStr.trim().length() > 0)
                      {  //using feeder-data-source sender ID
                        retFlag = qwFeederObj.sendSourcedDomainTypeNameMsg(
                                                          "CorbaSenderTest",
                           typeStringsArr[eventCount%typeStringsArr.length],
                                               ("event"+(eventCount+1)),str,
                                    corbaSenderIDStr,(++corbaSenderMsgNum));
                      }
                      else
                      {  //not using feeder-data-source sender ID
                        retFlag = qwFeederObj.sendDomainTypeNameMessage(
                                                          "CorbaSenderTest",
                           typeStringsArr[eventCount%typeStringsArr.length],
                                              ("event"+(eventCount+1)),str);
                      }
                      ++eventCount;
                    }
                    else
                    {     //not using structured events
                      if(corbaSenderIDStr != null &&
                                       corbaSenderIDStr.trim().length() > 0)
                      {   //using feeder-data-source sender ID
                               //send msg (with fdr-data-src ID & msgNum):
                        retFlag = qwFeederObj.sendSourcedMsg(
                                str,corbaSenderIDStr,(++corbaSenderMsgNum));
                      }
                      else     //not using feeder-data-source sender ID
                        retFlag = qwFeederObj.sendMessage(str);
                    }
                        //if deliver-on-msg-dates then don't show
                        // response (unless debug flag or error):
                    if(!deliverOnDatesFlag || dOnDatesDebugFlag)
                      System.out.println("Feeder response:  " + retFlag);
                    else if(!retFlag)
                    {
                      System.out.println(
                             "Negative response sending message to feeder");
                    }
                  }
                  catch(Exception ex)
                  {          //exception error; show error message
                    System.err.println(
                                 "Error sending message to feeder:  " + ex);
                  }
                }
                if(fileDelayVal >= 0)            //if value setup then
                  UtilFns.sleep(fileDelayVal);   //delay between sends
              }
              else
              {   //error fetching user string from console
                            //delay a bit before trying again:
                try { Thread.sleep(3000); }
                catch(InterruptedException ex) {}
              }
            }
            System.out.println("Closing implementation");
                   //close implementation; exit program
            orbManagerObj.closeImplementation(false);
          }
        }).start();

    orbManagerObj.runImplementation();      //run implementation
    System.out.println("Implementation has shutdown");
  }

  /**
   * Returns the next data message string from the file.
   * @return the next data message string from the file or null if none.
   */
  protected String getNextDataMessageStr()
  {
    try
    {
      String str;
      while(fileMsgStrIdx < fileStringsArr.length)
      {  //for each line of data from file
        if((str=fileStringsArr[fileMsgStrIdx++].trim()).length() > 0)
        {  //line of data not empty
          if(deliverOnDatesFlag)
          {  //delivering on message dates
            long msgTimeVal;
            try
            {
              final QWServerMsgRecord qwRecObj =
                             new QWServerMsgRecord(str,archivableMarkerObj);
              if((msgTimeVal=qwRecObj.getTimeGenerated()) <= 0)
              {
                System.err.println(
                               "Time-generated value not found in message");
                if(firstFetchedMsgFlag)       //if first message then
                  return null;                //abort program
                firstFetchedMsgFlag = false;
                return UtilFns.EMPTY_STRING;
              }
            }
            catch(Exception ex)
            {
              System.err.println("Error processing message-date value:  " +
                                                                        ex);
              if(firstFetchedMsgFlag)       //if first message then
                return null;                //abort program
              firstFetchedMsgFlag = false;
              return UtilFns.EMPTY_STRING;
            }
            final long timeOffsMs;
            if((timeOffsMs=msgTimeVal-System.currentTimeMillis()) > 0)
            {
              try
              {
                Thread.sleep(timeOffsMs);
              }
              catch(InterruptedException ex)
              {
              }
            }
          }
          final String beginString = "<DataMessage";
          final String endString = "</DataMessage>";
          final int beginIndex = str.indexOf(beginString);
          final int endIndex = str.indexOf(endString);
          if(beginIndex >= 0 && endIndex >= 0)  //if data message
          {  //DataMessage element found; fetch contents
            str = str.substring(beginIndex, endIndex + endString.length());
          }
          str = str.replace('\n', ' ') + '\n';
          return str;
        }
      }
    }
    catch(Exception ex)
    {
      System.err.println("Error fetching message from file:  " + ex);
    }
    return null;
  }

  /**
   * Entry point for program.
   * @param args array of command-line arguments.
   */
  public static void main(String [] args)
  {
    new CorbaSenderTest(args);
  }
}
