//ChmodScripts.java:  A command-line utility that scans the current
//                    directory and all subdirectories for scripts that
//                    match the given patterns and marks them as
//                    executable via a system call to 'chmod'.
//
//  11/1/2006 -- [ET]
//

package com.isti.quakewatch.server.test;

import java.util.HashSet;
import java.util.Arrays;
import java.io.File;
import com.isti.util.IstiFileFilter;
import com.isti.util.FileUtils;

/**
 * Class ChmodScripts is a command-line utility that scans the current
 * directory and all subdirectories for scripts that match the given
 * patterns and marks them as executable via a system call to 'chmod'.
 */
public class ChmodScripts
{
         //set of filename extensions for filter to reject:
  private static final HashSet rejectExtensionsSet =
                                     new HashSet(Arrays.asList(new String []
        { "bat", "exe", "jar", "java", "c", "cpp", "h", "mak", "lib", "dll",
          "py", "php", "inc", "css", "txt", "doc", "html", "htm", "xml",
          "xls", "xsd", "gif", "jpg", "zip", "gz", "sql", "wsdd" }));

  /**
   * Executing method for utility.
   * @param args array of command-line parameters.
   */
  public static void main(String [] args)
  {
         //create file-filter obj, modified to reject extensions from list:
    final IstiFileFilter filterObj = new IstiFileFilter(args,null)
        {
          public boolean accept(File fileObj)
          {
            final String nameStr;
            if(fileObj != null && !fileObj.isDirectory() &&
                                        (nameStr=FileUtils.getFileExtension(
                      fileObj.getName())) != null && nameStr.length() > 0 &&
                        rejectExtensionsSet.contains(nameStr.toLowerCase()))
            { //file obj OK, not a directory, has extension and matched
              return false;
            }
            return super.accept(fileObj);
          }
        };
         //have filter accept all directory to allow them to be searched:
    filterObj.setAcceptDirectories(true);
         //scan for files:
    final File [] fileArr = FileUtils.listAllFiles(new File("."),filterObj);

    try
    {
      String cmdStr;
      int retVal;
      Process procObj;
      for(int i=0; i<fileArr.length; ++i)
      {  //for each file found; show and execute command
        cmdStr = "chmod a+x " + fileArr[i];
        System.out.println(cmdStr);
        procObj = Runtime.getRuntime().exec(cmdStr);
        if((retVal=procObj.waitFor()) != 0)
        {
          System.out.println("Error code executing command:  " + retVal);
          break;
        }
        Thread.sleep(25);         //delay to help let process complete
        procObj.destroy();        //make sure process is gone
        Thread.sleep(25);         //delay to let resources get released
      }
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
}
