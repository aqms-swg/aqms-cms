//CorbaSenderTestConstantET.java:  Test program to send messages via CORBA
//                                 methods into the 'QWCorbaFeeder'.
//
//  2/11/2004 -- [ET]
//  5/16/2008 -- [ET]  Modified to make each message different.
//

package com.isti.quakewatch.server.test;

import java.util.Date;
import java.text.DateFormat;
import com.isti.openorbutil.OrbManager;
import com.isti.quakewatch.server.QWCorbaFeeder;
import com.isti.quakewatch.server.qw_feeder.QWFeeder;
import com.isti.quakewatch.server.qw_feeder.QWFeederHelper;
import com.isti.quakewatch.util.QWUtils;

/**
 * Class CorbaSenderTest test program to send messages via CORBA
 * methods into the 'QWCorbaFeeder'.
 * Usage:  CorbaSenderTest [-n] [hostname] [portnum]
 * where "-n" enables testing of event domain/type names.
 */
public class CorbaSenderTestConstantET
{
  private boolean domainTypeNamesFlag = false;
  private String serverHostStr = "localhost";
  private int serverPortNum = QWCorbaFeeder.DEF_PORT_NUM;
  /**
   * Creates a test program to send messages via CORBA
   * methods into the 'QWCorbaFeeder'.
   * @param programArgs array of command-line arguments.
   */
  public CorbaSenderTestConstantET(String [] programArgs)
  {
//    serverPortNum = 38888;

    String str;
    if(programArgs != null)
    {
      int slotNum = 0;
      for(int i=0; i<programArgs.length; ++i)
      {
        if((str=programArgs[i]).startsWith("-"))
        {
          if("-n".equalsIgnoreCase(str))
            domainTypeNamesFlag = true;
          else
          {
            if("-h".equalsIgnoreCase(str))
            {
              System.out.println(
                       "Usage:  CorbaSenderTest [-n] [hostname] [portnum]");
              System.exit(0);
              return;
            }
          }
        }
        else
        {
          if(slotNum == 0)
            serverHostStr = programArgs[i];
          else if(slotNum == 1)
          {
            try
            {
              final int val;
              if((val=Integer.parseInt(programArgs[i])) > 0)
                serverPortNum = val;
              else
              {
                System.err.println("Invalid port number parameter:  " +
                                                            programArgs[i]);
                System.exit(1);
                return;
              }
            }
            catch(NumberFormatException ex)
            {
              System.err.println("Invalid port number parameter:  " +
                                                            programArgs[i]);
              System.exit(1);
              return;
            }
          }
          ++slotNum;
        }
      }
    }

    System.out.println("Server host name = \"" + serverHostStr +
                                      "\", port number = " + serverPortNum);

    final OrbManager orbManagerObj = new OrbManager(programArgs);
    orbManagerObj.initImplementation();

    org.omg.CORBA.Object cObj;
    System.out.println("Calling resolveCorbalocObject()");
    if((cObj=orbManagerObj.resolveCorbalocObject(serverHostStr,
                    serverPortNum,QWCorbaFeeder.CORBALOC_REF_NAME)) == null)
    {
      System.err.println("Error resolving Corbaloc object:  " +
                                           orbManagerObj.getErrorMessage());
      return;
    }

    System.out.println("Narrowing QWFeeder object");
    final QWFeeder qwFeederObj = QWFeederHelper.narrow(cObj);

         //if using domain/type names then setup array of "type" names:
    final String [] typeStringsArr = domainTypeNamesFlag ? (new String []
                                     { "first", "second", "third" }) : null;

    if(domainTypeNamesFlag)
      System.out.println("Using event domain/type names");

      //create new thread to process user input:
    (new Thread()
        {
          public void run()
          {
            DateFormat xmlDateFormatterObj =  QWUtils.getXmlDateFormatterObj();

            System.out.println(
                             "Enter messages to be sent (or 'Q' to quit):");
            int eventCount = 0;
            boolean retFlag;
            int stringCount = 0;
            String str;

            while(true)
            {
              Date currentTime_1 = new Date();
              String dateString = xmlDateFormatterObj.format(currentTime_1);

              str = "<DataMessage Action=\"Update\" TimeReceived=\"" +
                dateString + "\" FeederMsgNum=\"" + (100000 + stringCount) +
                "\" SourceHost=\"qdds1.wr.usgs.gov\" SrcHostMsgID=\"" +
                (200000 + stringCount) + "\" RelayMsgNum=\"" +
                (300000 + stringCount) + "\" RelayTimeGen=\"" +
                dateString + "\" RelayServerID=\"ISTI Test Server\" " +
                "RelayMsgSrc=\"QddsFeeder, Version 1.0 ('QDDS')\">" +
                getBigMessage(currentTime_1.getTime()) +
                "</DataMessage>";

              System.err.println(str);
              stringCount++;

              if(str.equalsIgnoreCase("Q"))
                break;         //if keyword then exit loop
              try
              {       //send message to feeder:
                if(domainTypeNamesFlag)
                    {     //using structured events
                  retFlag = qwFeederObj.sendDomainTypeNameMessage(
                      "CorbaSenderTest",
                      typeStringsArr[eventCount%typeStringsArr.length],
                      ("event"+(eventCount+1)),str);
                  ++eventCount;
                }
                else  //not using structured events
                  retFlag = qwFeederObj.sendMessage(str);
                System.out.println("Feeder response:  " + retFlag);
              }
              catch(Exception ex)
                  {          //exception error; show error message
                System.err.println(
                    "Error sending message to feeder:  " + ex);
              }
              try { Thread.sleep(1000); }
              catch(InterruptedException ex) {}

              //              }
              //              else
              //              {   //error fetching user string from console
              //                            //delay a bit before trying again:
              //                try { Thread.sleep(3000); }
//                catch(InterruptedException ex) {}
//              }
            }
            System.out.println("Closing implementation");
                   //close implementation; exit program
            orbManagerObj.closeImplementation(false);
          }
        }).start();

    orbManagerObj.runImplementation();      //run implementation
    System.out.println("Implementation has shutdown");
  }

  private String getBigMessage(long time) {

    StringBuffer sb = new StringBuffer();
    sb.append("<ValuesMessage Chan=\"/parameters/trigger/carlsubtrig/subnettrig\" Domain=\"RT101\" Name=\"Subnet_Trigger\" Source=\"MOD_TRIG_PUB\">");

  sb.append("<Real8 Name=\"NoName\">" + (double)time + "</Real8>");

  sb.append("<Int4 Name=\"NoName\">45347</Int4>");

  sb.append("<String Name=\"NoName\">CI</String>");

  sb.append("<String Name=\"NoName\">RT3</String>");

  sb.append("<String Name=\"NoName\">SUBTRIG</String>");

  sb.append("<Real8 Name=\"NoName\">1089321812.000000</Real8>");

  sb.append("<Int4 Name=\"NoName\">0</Int4>");

  sb.append("<Real8 Name=\"NoName\">0.000000</Real8>");

  sb.append("<Real8 Name=\"NoName\">0.000000</Real8>");

  sb.append("<Int4 Name=\"NoName\">46</Int4>");

  sb.append("<StringArray Name=\"NoName\">");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>BK</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>NP</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
  sb.append("</StringArray>");

  sb.append("<StringArray Name=\"NoName\">");
    sb.append("<String>BAK</String>");
    sb.append("<String>BCC</String>");
    sb.append("<String>CBK</String>");
    sb.append("<String>CCA</String>");
    sb.append("<String>CCC</String>");
    sb.append("<String>CCC</String>");
    sb.append("<String>CLC</String>");
    sb.append("<String>CLC</String>");
    sb.append("<String>CLC</String>");
    sb.append("<String>CLT</String>");
    sb.append("<String>DTP</String>");
    sb.append("<String>EW2</String>");
    sb.append("<String>GRA</String>");
    sb.append("<String>GSA</String>");
    sb.append("<String>JFS</String>");
    sb.append("<String>JRC2</String>");
    sb.append("<String>LCP</String>");
    sb.append("<String>LRL</String>");
    sb.append("<String>LRL</String>");
    sb.append("<String>LRR</String>");
    sb.append("<String>MPM</String>");
    sb.append("<String>MPM</String>");
    sb.append("<String>OAK</String>");
    sb.append("<String>POTR</String>");
    sb.append("<String>QAL</String>");
    sb.append("<String>SCZ2</String>");
    sb.append("<String>SLA</String>");
    sb.append("<String>SLA</String>");
    sb.append("<String>SRT</String>");
    sb.append("<String>TCF</String>");
    sb.append("<String>TIN</String>");
    sb.append("<String>TOW</String>");
    sb.append("<String>WBM</String>");
    sb.append("<String>WBS</String>");
    sb.append("<String>WBS</String>");
    sb.append("<String>WCH</String>");
    sb.append("<String>WCS</String>");
    sb.append("<String>WMF</String>");
    sb.append("<String>WNM</String>");
    sb.append("<String>WOR</String>");
    sb.append("<String>WRC</String>");
    sb.append("<String>WRV</String>");
    sb.append("<String>WSH</String>");
    sb.append("<String>WVP</String>");
    sb.append("<String>WWP</String>");
    sb.append("<String>YEG</String>");
  sb.append("</StringArray>");

  sb.append("<StringArray Name=\"NoName\">");
    sb.append("<String>HHZ</String>");
    sb.append("<String>HHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>BHZ</String>");
    sb.append("<String>HHZ</String>");
    sb.append("<String>BHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>HHZ</String>");
    sb.append("<String>HHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>HHZ</String>");
    sb.append("<String>HLZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>HHZ</String>");
    sb.append("<String>HHZ</String>");
    sb.append("<String>BHZ</String>");
    sb.append("<String>HHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>BHZ</String>");
    sb.append("<String>HHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>HHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>HHZ</String>");
    sb.append("<String>BHZ</String>");
    sb.append("<String>HHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>HLZ</String>");
    sb.append("<String>HHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>BHZ</String>");
    sb.append("<String>HHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>EHZ</String>");
  sb.append("</StringArray>");

  sb.append("<StringArray Name=\"NoName\">");
    sb.append("<String>t</String>");
    sb.append("<String>t</String>");
    sb.append("<String>t</String>");
    sb.append("<String>t</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>t</String>");
    sb.append("<String>t</String>");
    sb.append("<String>m</String>");
    sb.append("<String>t</String>");
    sb.append("<String>t</String>");
    sb.append("<String>t</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>t</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>t</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>t</String>");
    sb.append("<String>t</String>");
    sb.append("<String>t</String>");
    sb.append("<String>t</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>t</String>");
    sb.append("<String>t</String>");
    sb.append("<String>t</String>");
    sb.append("<String>t</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>t</String>");
    sb.append("<String>m</String>");
    sb.append("<String>t</String>");
    sb.append("<String>t</String>");
  sb.append("</StringArray>");

  sb.append("<Real8Array Name=\"NoName\">");
    sb.append("<Real8>1089321804.000000</Real8>");
    sb.append("<Real8>1089321803.000000</Real8>");
    sb.append("<Real8>1089321840.000000</Real8>");
    sb.append("<Real8>1089321827.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>1089321827.000000</Real8>");
    sb.append("<Real8>1089321812.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>1089321826.000000</Real8>");
    sb.append("<Real8>1089321805.000000</Real8>");
    sb.append("<Real8>1089321843.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>1089321804.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>1089321832.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>1089321845.000000</Real8>");
    sb.append("<Real8>1089321809.000000</Real8>");
    sb.append("<Real8>1089321820.000000</Real8>");
    sb.append("<Real8>1089321838.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>1089321812.000000</Real8>");
    sb.append("<Real8>1089321808.000000</Real8>");
    sb.append("<Real8>1089321814.000000</Real8>");
    sb.append("<Real8>1089321803.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>1089321807.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>1089321804.000000</Real8>");
    sb.append("<Real8>1089321803.000000</Real8>");
  sb.append("</Real8Array>");

  sb.append("<Real8Array Name=\"NoName\">");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
  sb.append("</Real8Array>");

  sb.append("<Real8Array Name=\"NoName\">");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("</Real8Array>");

    sb.append("</ValuesMessage>");

    return sb.toString();

  }

  /**
   * Entry point for program.
   * @param args array of command-line arguments.
   */
  public static void main(String [] args)
  {
    new CorbaSenderTestConstantET(args);
  }
}
