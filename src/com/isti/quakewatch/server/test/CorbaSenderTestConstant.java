
//CorbaSenderTest.java:  Test program to send messages via CORBA
//                       methods into the 'QWCorbaFeeder'.
//
//  2/11/2004 -- [ET]
//

package com.isti.quakewatch.server.test;

import com.isti.openorbutil.OrbManager;
import com.isti.quakewatch.server.QWCorbaFeeder;
import com.isti.quakewatch.server.qw_feeder.QWFeeder;
import com.isti.quakewatch.server.qw_feeder.QWFeederHelper;

/**
 * Class CorbaSenderTest test program to send messages via CORBA
 * methods into the 'QWCorbaFeeder'.
 * Usage:  CorbaSenderTest [-n] [hostname] [portnum]
 * where "-n" enables testing of event domain/type names.
 */
public class CorbaSenderTestConstant
{
  private boolean domainTypeNamesFlag = true;
  private String serverHostStr = "localhost";
  private int serverPortNum = QWCorbaFeeder.DEF_PORT_NUM;
  /**
   * Creates a test program to send messages via CORBA
   * methods into the 'QWCorbaFeeder'.
   * @param programArgs array of command-line arguments.
   */
  public CorbaSenderTestConstant(String [] programArgs)
  {
    String str;
    if(programArgs != null)
    {
      int slotNum = 0;
      for(int i=0; i<programArgs.length; ++i)
      {
        if((str=programArgs[i]).startsWith("-"))
        {
          if("-n".equalsIgnoreCase(str))
            domainTypeNamesFlag = true;
          else
          {
            if("-h".equalsIgnoreCase(str))
            {
              System.out.println(
                       "Usage:  CorbaSenderTest [-n] [hostname] [portnum]");
              System.exit(0);
              return;
            }
          }
        }
        else
        {
          if(slotNum == 0)
            serverHostStr = programArgs[i];
          else if(slotNum == 1)
          {
            try
            {
              final int val;
              if((val=Integer.parseInt(programArgs[i])) > 0)
                serverPortNum = val;
              else
              {
                System.err.println("Invalid port number parameter:  " +
                                                            programArgs[i]);
                System.exit(1);
                return;
              }
            }
            catch(NumberFormatException ex)
            {
              System.err.println("Invalid port number parameter:  " +
                                                            programArgs[i]);
              System.exit(1);
              return;
            }
          }
          ++slotNum;
        }
      }
    }

    System.out.println("Server host name = \"" + serverHostStr +
                                      "\", port number = " + serverPortNum);

    final OrbManager orbManagerObj = new OrbManager(programArgs);
    orbManagerObj.initImplementation();

    org.omg.CORBA.Object cObj;
    System.out.println("Calling resolveCorbalocObject()");
    if((cObj=orbManagerObj.resolveCorbalocObject(serverHostStr,
                    serverPortNum,QWCorbaFeeder.CORBALOC_REF_NAME)) == null)
    {
      System.err.println("Error resolving Corbaloc object:  " +
                                           orbManagerObj.getErrorMessage());
      return;
    }

    System.out.println("Narrowing QWFeeder object");
    final QWFeeder qwFeederObj = QWFeederHelper.narrow(cObj);

         //if using domain/type names then setup array of "type" names:
    final String [] typeStringsArr = domainTypeNamesFlag ? (new String []
          { "/conntest/receiver" }) : null;

    if(domainTypeNamesFlag)
      System.out.println("Using event domain/type names");

      //create new thread to process user input:
    (new Thread()
        {
          public void run()
          {
            System.out.println(
                             "Enter messages to be sent (or 'Q' to quit):");
            int eventCount = 0;
            boolean retFlag;

            StringBuffer sb = new StringBuffer();
            String str;

            while(true)
            {
              sb.delete(0,500);

              Double d = Double.valueOf(Math.random() * 1000000);

              sb.append("<ValuesMessage Chan=\"/sidconntest/receiver\" Domain=\"sidRT101\" Name=\"Event_Signal\" Source=\"CONNTEST\" SidTest=\"" + d + "\">  <Int4 Name=\"NoName\">0</Int4></ValuesMessage>");

              System.err.println(sb.toString());

              str = sb.toString();

              if(str.equalsIgnoreCase("Q"))
                break;         //if keyword then exit loop
              try
              {       //send message to feeder:
                if(domainTypeNamesFlag)
                    {     //using structured events
                  retFlag = qwFeederObj.sendDomainTypeNameMessage(
                      "RT101",
                      typeStringsArr[eventCount%typeStringsArr.length],
                      ("event"+(eventCount+1)),str);
                  ++eventCount;
                }
                else  //not using structured events
                  retFlag = qwFeederObj.sendMessage(str);
                System.out.println("Feeder response:  " + retFlag);
              }
              catch(Exception ex)
                  {          //exception error; show error message
                System.err.println(
                    "Error sending message to feeder:  " + ex);
              }
              try { Thread.sleep(5000); }
              catch(InterruptedException ex) {}
            }
            System.out.println("Closing implementation");
                   //close implementation; exit program
            orbManagerObj.closeImplementation(false);
          }
        }).start();

    orbManagerObj.runImplementation();      //run implementation
    System.out.println("Implementation has shutdown");
  }

  /**
   * Entry point for program.
   * @param args array of command-line arguments.
   */
  public static void main(String [] args)
  {
    new CorbaSenderTestConstant(args);
  }
}
