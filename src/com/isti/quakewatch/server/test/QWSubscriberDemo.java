//QWSubscriberDemo.java:  Subscriber demonstration program that connects to
//                        a QWCorbaOutputter.
//
//   4/1/2004 -- [ET]  Initial version.
//  7/22/2008 -- [ET]  Converted from QWCorbaClient 'QWPushSubSample'.
//

package com.isti.quakewatch.server.test;

import java.util.ArrayList;
import com.isti.openorbutil.OrbManager;
import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.quakewatch.corbaclient.qw_client_rec.*;

/**
 * Subscriber demonstration program that connects to a QWCorbaOutputter.
 */
public class QWSubscriberDemo
{
  public static final String MODULE_NAME = "QWSubscriberDemo";
  public static final String VERSION = "1.0";    //name and version strings
  public static final String REVISION_STRING = MODULE_NAME + ", " +
                                                       "Version " + VERSION;
  protected final LogFile logObj = LogFile.initGlobalLogObj(
                        (MODULE_NAME+".log"),LogFile.DEBUG4,LogFile.DEBUG2);
  protected String subscriberNameStr = null;
  protected QWSubscription subscriptionObj = null;
  protected int testCount = 0;
  protected QWPushSubServices subServicesObj;    //subscriber-services obj
  protected OrbManager orbManagerObj;
  protected DomainChanBlk [] domainChanBlkArr  = null;
  protected boolean disableAfterReceiveFlag = false;
  protected boolean receiveCorbaExceptionFlag = false;


  /**
   * Creates a subscriber-demonstration object.
   * @param nameStr name string for subscriber.
   */
  public QWSubscriberDemo(String nameStr)
  {
    subscriberNameStr = nameStr;
  }


  /**
   * Connects the subscriber to the QWCorbaOutputter.
   * @param serverHostStr host name for QWCorbaOutputter.
   * @param serverPortNum port number for QWCorbaOutputter.
   * @param domChanList list of domain/channel strings to be subscribed to
   * (must be even number of entries).
   */
  public void connectSubscriber(String serverHostStr, int serverPortNum,
                                                      ArrayList domChanList)
  {
         //create data blocks holding domain/type strings to subscribe to:
    domainChanBlkArr  = new DomainChanBlk[domChanList.size()/2];
    int listIdx = 0, arrIdx = 0;
    while(arrIdx < domainChanBlkArr.length)
    {
      domainChanBlkArr[arrIdx++] = new DomainChanBlk(
                                       (String)(domChanList.get(listIdx++)),
                                      (String)(domChanList.get(listIdx++)));
    }
         //create and initialize ORB manager:
    orbManagerObj = new OrbManager(null);
    orbManagerObj.initImplementation();
         //resolve 'QWSubscription' object from "Corbaloc" address:
    logObj.info("Calling resolveCorbalocObject()");
    org.omg.CORBA.Object cObj;
    if((cObj=orbManagerObj.resolveCorbalocObject(serverHostStr,
                                   serverPortNum,"QWSubscription")) == null)
    {
      logObj.warning("Error resolving Corbaloc object:  " +
                                           orbManagerObj.getErrorMessage());
      orbManagerObj.closeImplementation(false);
      return;
    }
    try
    {
      logObj.info("Narrowing 'QWSubscription' object");
         //narrow 'QWSubscription' object:
      subscriptionObj = QWSubscriptionHelper.narrow(cObj);
    }
    catch(Exception ex)
    {
      logObj.warning("Error narrowing 'QWSubscription' object:  " + ex);
      orbManagerObj.closeImplementation(false);
      return;
    }
         //connect to QWCorbaOutputter and get "services" object:
         //create holder to receive 'QWPushSubServices' obj:
    final QWPushSubServicesHolder servicesHolderObj =
                                              new QWPushSubServicesHolder();
    logObj.info("Connecting subscriber");
    final String resultStr;
//            //instead of subscription via 'domainChanBlkArr' parameter,
//            // do them below via 'subscribe()' method:
//    if((resultStr=subscriptionObj.connectPushSubscriberTo(subscriberNameStr,
//                          domainChanBlkArr,servicesHolderObj)).length() > 0)
    if((resultStr=subscriptionObj.connectPushSubscriber(subscriberNameStr,
                                           servicesHolderObj)).length() > 0)
      // empty string returned if all ok.
    {    //error connecting to QWCorbaOutputter
      logObj.warning("Error connecting subscriber:  " + resultStr);
      orbManagerObj.closeImplementation(false);
      return;
    }
         //fetch 'QWPushSubServices' object from holder:
    if((subServicesObj=servicesHolderObj.value) == null)
    {    //null services object returned
      logObj.warning(
             "Error connecting subscriber:  Null services object returned");
      orbManagerObj.closeImplementation(false);
      return;
    }
         //subscribe to domain/channels:
    for(int ii = 0; ii < domainChanBlkArr.length; ii++)
    {
      subServicesObj.subscribe(domainChanBlkArr[ii].domainStr,
                                           domainChanBlkArr[ii].channelStr);
    }
         //create call-back object to receive and process messages:
    QWRecMsgCallBackPOA recMsgCallBackObj = new QWRecMsgCallBackPOA()
        {
          public boolean receiveMessage(String xmlMsgStr)
          {
            if(receiveCorbaExceptionFlag)
            {  //flag set for throwing simulated CORBA exceptions
              try
              {         //do a delay to slow down retries of sending:
                Thread.sleep(500);
              }
              catch(InterruptedException ex)
              {
              }
              logObj.debug2("Throwing simulated CORBA exception in " +
                                            "response to received message");
              throw new org.omg.CORBA.TRANSIENT(
                                          "Simulated communications error");
            }
            return processReceivedMessage(xmlMsgStr);
          }
        };
    logObj.info("Checking connection");
    try
    {
      subServicesObj.checkConnection();
    }
    catch (Exception ex)
    {
      logObj.warning("Error during initial check of connection:  " + ex);
    }
         //connect call-back object (initially enabled):
    subServicesObj.connectRecMsgCallBack(
                        recMsgCallBackObj._this(orbManagerObj.orbObj),true);
         //create and start thread to handle user inputs:
    (new UserInputThread()).start();
         //create and start thread to periodically check connection:
    (new CheckConnectThread()).start();
    logObj.info("Running CORBA implementation");
         //run CORBA implementation:
    orbManagerObj.runImplementation();
    logObj.info("Returned from Orb Implementation");
  }

  /**
   * Processes a message received from the QWCorbaOutputter.
   * @param xmlMsgStr XML-data message string.
   * @return true if message delivery should remain enabled; false if
   * message delivery should be disabled.
   */
  protected boolean processReceivedMessage(String xmlMsgStr)
  {
    try
    {
      int pos;
      if((pos=xmlMsgStr.indexOf("messageCount")) > 0)
      {  //"messageCount" keyword found in message
        String ts = xmlMsgStr.substring(pos);
        pos = ts.indexOf(">");
        final int epos = ts.indexOf("<");
        logObj.debug("Received:  (count: " + testCount++ +")  " +
                                               ts.substring(pos + 1, epos));
        logObj.debug3("  msg:  " + xmlMsgStr);
      }
      else
        logObj.debug("Received msg:  " + xmlMsgStr);
    }
    catch(Exception ex)
    {
      logObj.warning("Error processing received message:  " + ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
    if(!disableAfterReceiveFlag)       //if flag not set then
      return true;                     //return true for delivery enabled
    disableAfterReceiveFlag = false;   //reset flag
    logObj.info("Returning 'false' to disable message delivery");
    return false;                      //return false for delivery disabled
  }

  /**
   * Disconnects the subscriber from the QWCorbaOutputter and closes the
   * CORBA implementation.
   */
  public void disconnectSubscriber()
  {
    try
    {
      subServicesObj.disconnectSubscriber();
    }
    catch(Exception ex)
    {
      logObj.warning("Error disconnecting subscriber:  " + ex);
    }
    try
    {
      orbManagerObj.closeImplementation(false);
    }
    catch(Exception ex)
    {
      logObj.warning("Error closing CORBA implementation:  " + ex);
    }
  }

  /**
   * Executing method for program.
   * @param args array of command-line parameters.
   */
  public static void main(String [] args)
  {
    String hostStr = null;
    int portNum = 36688;
    String subscriberName = null;
    final ArrayList domChanList = new ArrayList();
    for( int i=0; i< args.length; i++)
    {
      if(args[i].equals("-h"))
        hostStr = args[i+1];
      if(args[i].equals("-p"))
        portNum = Integer.parseInt(args[++i]);
      if(args[i].equals("-n"))
        subscriberName = args[++i];
      if(args[i].equals("-d"))
      {
        while (i < (args.length -1))        //take rest of parameters as
          domChanList.add(args[++i]);       // domain/channel names
        final int dcListSize;
        if((dcListSize=domChanList.size()) / 2 * 2 != dcListSize)
        {
          System.err.println(
                  "Uneven number of domain / channel parameters; aborting");
          System.exit(-1);
        }
      }
    }
    if(hostStr == null || subscriberName == null)
    {
      QWSubscriberDemo.usage();
      System.exit(-1);
    }

     final QWSubscriberDemo qwSubDemoObj =
                                       new QWSubscriberDemo(subscriberName);
     qwSubDemoObj.connectSubscriber(hostStr,portNum,domChanList);

     qwSubDemoObj.logObj.info("Exiting program");
     System.exit(0);
   }

  /**
   * Displays program usage.
   */
  public static void usage()
  {
    System.err.println(REVISION_STRING + "; usage:");
    System.err.println("runSubscriberDemo -h hostname" +
                         " -n subscriberName -p port -d channelDomainList");
    System.err.println("Example usage:");
    System.err.println("runSubscriberDemo -h www.isti.com" +
              " -n test_sub -p 36688 -d domain1 channel1 domain2 channel2");
  }


  /**
   * Class UserInputThread handles user inputs.
   */
  protected class UserInputThread extends Thread
  {
    /**
     * Executing method for thread.
     */
    public void run()
    {
      final String helpStr = "Enter 'Q' to quit program," +
                                   " 'U' to unsubscribe from domain/chan," +
                                     " 'S' to resubscribe to domain/chan," +
                                           " 'D' to disable msg delivery," +
                                            " 'E' to enable msg delivery," +
                                         " 'A' to disable after next msg," +
                                              " 'R' to reset the counter," +
                                " 'C' to toggle receive CORBA exceptions," +
                                                   " '?' to show help info";
      logObj.info("Client started:  " + helpStr);
      int unsubCount = 0;
      String str = null;
      DomainChanBlk domChanBlk;
      while(true)
      {
        if((str=UtilFns.getUserConsoleString()) != null)
        {  //user string fetched from console OK
          if(str.equalsIgnoreCase("Q"))
          {  //exit command entered by user
            logObj.info("Terminating client in response to user input");
            disconnectSubscriber();
            return;
          }
          if(str.equalsIgnoreCase("R"))
          {
            logObj.info("Resetting test counter");
            testCount = 0;
          }
          try
          {
            if(str.equalsIgnoreCase("U"))
            {
              if(unsubCount < domainChanBlkArr.length)
              {
                domChanBlk = domainChanBlkArr[unsubCount++];
                logObj.info("Unsubscribing from " +
                        domChanBlk.domainStr + "/" + domChanBlk.channelStr);
                subServicesObj.unsubscribe(
                                domChanBlk.domainStr,domChanBlk.channelStr);
              }
              else
                System.out.println("No domain/channels subscribed");
            }
            else if(str.equalsIgnoreCase("S"))
            {
              if(unsubCount > 0)
              {
                domChanBlk = domainChanBlkArr[--unsubCount];
                logObj.info("Resubscribing to " +
                        domChanBlk.domainStr + "/" + domChanBlk.channelStr);
                subServicesObj.subscribe(
                                domChanBlk.domainStr,domChanBlk.channelStr);
              }
              else
                System.out.println("No domain/channels to subscribe");
            }
            else if(str.equalsIgnoreCase("E"))
            {
              logObj.info("Enabling message delivery");
              subServicesObj.setCallBackEnabled(true);
            }
            else if(str.equalsIgnoreCase("D"))
            {
              logObj.info("Disabling message delivery");
              subServicesObj.setCallBackEnabled(false);
            }
            else if(str.equalsIgnoreCase("A"))
            {
              disableAfterReceiveFlag = true;
              logObj.info("Will disable delivery after next message");
            }
            else if(str.equalsIgnoreCase("C"))
            {
              if(receiveCorbaExceptionFlag)
              {
                receiveCorbaExceptionFlag = false;
                logObj.info("Receive CORBA exceptions disabled");
              }
              else
              {
                receiveCorbaExceptionFlag = true;
                logObj.info("Receive CORBA exceptions enabled");
              }
            }
            else if(str.length() > 0)
              System.out.println(helpStr);
          }
          catch(Exception ex)
          {
            logObj.warning("Error processing user command:  " + ex);
          }
        }
        else
        {   //error fetching user string from console
          try         //delay a bit before trying again:
          { Thread.sleep(3000); }
          catch(InterruptedException ex) {}
        }
      }
    }
  }


  /**
   * Class CheckConnectThread checks the connection on a periodic basis.
   */
  protected class CheckConnectThread extends Thread
  {
    /**
     * Creates a connection-checking thread.
     */
    public CheckConnectThread()
    {
      setDaemon(true);            //mark as daemon thread
    }

    /**
     * Executing method for thread.
     */
    public void run()
    {
      while(true)
      {
        try
        {     //delay 10 seconds between checks:
          sleep(10000);
        }
        catch(InterruptedException ex)
        {
          break;
        }
        try
        {     //perform connection check:
          subServicesObj.checkConnection();
        }
        catch(Exception ex)
        {
          logObj.warning("Error checking connection:  " + ex);
        }
      }
    }
  }
}
