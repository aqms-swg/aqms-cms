//CubeToXMLSupport.java:  Support for test utilities that convert
//                        CUBE-format messages to XML-format messages.
//
//  5/21/2008 -- [ET]
//

package com.isti.quakewatch.server.test;

import java.util.Date;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DateFormat;
import org.jdom.Element;
import org.jdom.Attribute;
import com.isti.util.IstiXmlUtils;
import com.isti.quakewatch.util.QWUtils;
import com.isti.quakewatch.server.MsgParserIntf;

/**
 * Class CubeToXMLSupport implements support for test utilities that convert
 * CUBE-format messages to XML-format messages.
 */
public class CubeToXMLSupport
{
  protected final CubeParserSupportPub msgParserObj;
  protected final DateFormat xmlDateFormatterObj =
                                           QWUtils.getXmlDateFormatterObj();
  protected final String timeReceivedStr;
  protected final Attribute dummyAttribObj = new Attribute("dummy","dmval");

  /**
   * Processes the given CUBE-format messages to XML-format messages.
   * @param msgParserObj 'MsgParserIntf' object to use for parsing.
   * @param args String [] optional "message" parameter(s).
   */
  public CubeToXMLSupport(CubeParserSupportPub msgParserObj, String [] args)
  {
    this.msgParserObj = msgParserObj;
                   //save current time (as an XML-format time string):
    timeReceivedStr = xmlDateFormatterObj.format(new Date());
    if(args.length > 0)
    {  //"message" parameter(s) given; process each one
      for(int i=0; i<args.length; ++i)
        processMessage(args[i]);
    }
    else
    {  //no "message" parameter(s) given; take input from 'stdin'
      try
      {
        final BufferedReader rdrObj =
                       new BufferedReader(new InputStreamReader(System.in));
        String str;
        while((str=rdrObj.readLine()) != null)
        {  //process each entered line (until EOF)
          processMessage(str);
        }
      }
      catch(Exception ex)
      {  //some kind of exception error; show stack trace
        ex.printStackTrace();
      }
    }
  }

  /**
   * Processes the given CUBE-format message to an XML-format message,
   * sending the text to 'stdout'.
   * @param msgStr CUBE-format message string.
   */
  public void processMessage(String msgStr)
  {
    try
    {
      if(msgStr == null || msgStr.trim().length() <= 0)
        return;         //if no data then abort method
              //if whitespace after 80th char then trim it:
      if(msgStr.length() > 80 && msgStr.substring(80).trim().length() == 0)
        msgStr = msgStr.substring(0,80);
      final Element elemObj;
      if((elemObj=msgParserObj.parseCubeMsgStringData(
                            msgStr,timeReceivedStr,dummyAttribObj)) != null)
      {  //message processed OK
        System.out.println(IstiXmlUtils.elementToString(elemObj));
      }
      else
      {  //error processing message
        System.err.println("Error processing message:  " +
                                            msgParserObj.getErrorMessage());
      }
    }
    catch(Exception ex)
    {  //some kind of exception error; show show stack trace
      ex.printStackTrace();
    }
  }


  /**
   * Interface CubeParserSupportPub is used as part of the mechanism to
   * make the 'parseCubeMsgStringData()' method public.
   */
  interface CubeParserSupportPub extends MsgParserIntf
  {
    /**
     * Parses CUBE-format message data into a JDOM/XML tree of elements.
     * @param messageString the message string to be parsed.
     * @param timeReceivedStr current time (as an XML-format time string).
     * @param actionAttribObj attrib whose value will be set by this method.
     * @return The resulting "EQMessage" Element object, or null if an
     * error occured (in which case the associated error message may be
     * fetched via the 'getErrorMessage()' method).
     */
    public Element parseCubeMsgStringData(String messageString,
                         String timeReceivedStr, Attribute actionAttribObj);
  }
}
