//ServicesTest1.java:  Test program to exercise QuakeWatch acceptor
//                     and services methods.

package com.isti.quakewatch.server.test;

import com.isti.util.UtilFns;
import com.isti.openorbutil.OrbManager;
import com.isti.quakewatch.common.qw_services.QWAcceptor;
import com.isti.quakewatch.common.qw_services.QWAcceptorHelper;
import com.isti.quakewatch.common.qw_services.QWServices;
import com.isti.quakewatch.common.qw_services.QWServicesHelper;

/**
 * Class ServicesTest1 is a test program to exercise QuakeWatch acceptor
 * and services methods.
 */
public class ServicesTest1
{

  public ServicesTest1(String [] args)
  {
    final OrbManager orbManagerObj = new OrbManager(args);
    orbManagerObj.initImplementation();

    org.omg.CORBA.Object cObj;
    System.out.println("Calling resolveCorbalocObject()");
    if((cObj=orbManagerObj.resolveCorbalocObject(
                                   "localhost",39977,"QWAcceptor")) == null)
    {
      System.err.println("Error resolving Corbaloc object:  " +
                                           orbManagerObj.getErrorMessage());
      return;
    }
    System.out.println("Narrowing QWAcceptor object");
    final QWAcceptor qwAcceptorObj = QWAcceptorHelper.narrow(cObj);
    System.out.println("Calling QWAcceptor.newConnection()");
    final String qwServicesIorStr = qwAcceptorObj.newConnection(
                             "testname","testpwd",UtilFns.getLocalHostIP());
    if(qwServicesIorStr == null || qwServicesIorStr.length() <= 0)
    {
      System.err.println(
            "Empty string returned from QWAcceptor.newConnection() method");
      return;
    }
    if(qwServicesIorStr.startsWith(QWAcceptor.ERROR_MSG_PREFIX))
    {
      System.err.println("Error message returned from QWAcceptor." +
                   "newConnection() method:  " + qwServicesIorStr.substring(
                                     QWAcceptor.ERROR_MSG_PREFIX.length()));
      return;
    }
    System.out.println("Received QWServices IOR string:  " +
                                                          qwServicesIorStr);
    System.out.println("Converting and narrowing QWServices object");
    final QWServices qwServicesObj = QWServicesHelper.narrow(
                   orbManagerObj.orbObj.string_to_object(qwServicesIorStr));
    System.out.println("Calling QWServices.getEventChLocStr()");
    final String eventChannelStr = qwServicesObj.getEventChLocStr();
    System.out.println("Received event channel IOR string:  " +
                                                           eventChannelStr);
    System.out.println("Complete");
    orbManagerObj.closeImplementation(false);
  }

  public static void main(String [] args)
  {
    new ServicesTest1(args);
  }
}
