//CubeMsgsExtractor.java:  Extracts CUBE messages from one or more
//                         QuakeWatch-server "QDDSFeeder" log files.
//
//   8/8/2006 -- [ET]
//

package com.isti.quakewatch.server.test;

import com.isti.util.FileProcessor;
import com.isti.util.CircularBuffer;

/**
 * Class CubeMsgsExtractor extracts CUBE messages from one or more
 * QuakeWatch-server "QDDSFeeder" log files.
 */
public class CubeMsgsExtractor
{
  /**
   * Program entry point.
   * @param args array of command line arguments containing one or more
   * input filenames.
   */
  public static void main(String [] args)
  {
    String inFileStr = "", outFileStr, lineStr;
    CircularBuffer circularBufferObj;
    FileProcessor fileProcessorObj;
    int pos;
    for(int argsIdx=0; argsIdx<args.length; ++argsIdx)
    {
      try
      {
        inFileStr = args[argsIdx];
        outFileStr = getFileNameWithoutExtension(inFileStr) +
                                                                "_msgs.txt";
        fileProcessorObj = new FileProcessor(inFileStr,outFileStr);
        System.out.println("Processing \"" + inFileStr + "\" to \"" +
                                                         outFileStr + "\"");
        circularBufferObj = new CircularBuffer(10);
        while((lineStr=fileProcessorObj.readNextLine()) != null)
        {
          if((pos=lineStr.indexOf("] QddsFeeder (")) > 0 &&
                           (pos=lineStr.indexOf("):  message #",pos)) > 0 &&
                                 (pos=lineStr.indexOf(" (from ",pos)) > 0 &&
                                    (pos=lineStr.indexOf("):  ",pos)) > 0 &&
                                                (pos+=4) < lineStr.length())
          {
            lineStr = lineStr.substring(pos);
            if(!circularBufferObj.contains(lineStr))
            {      //message text not duplicated in last 10 lines
              fileProcessorObj.writeNextLine(lineStr);
              circularBufferObj.add(lineStr);
            }
          }
        }
      }
      catch(Exception ex)
      {
        System.err.println("Error processing \"" + inFileStr + "\":  " + ex);
      }
      inFileStr = "";
    }
  }

  /**
   * Returns the index of the extension portion of the file name string.
   * @return the index of the extension portion of the file name string
   * or -1 if none (includes the ".".)
   * @param fileNameStr the name of the file.
   */
  public static int getFileExtensionIndex(String fileNameStr)
  {
    return fileNameStr.lastIndexOf('.');
  }

  /**
   * Returns the file name string without the extension portion.
   * @param fileNameStr the name of the file.
   * @return The file name string without the extension portion.
   */
  public static String getFileNameWithoutExtension(String fileNameStr)
  {
    final int extIdx = getFileExtensionIndex(fileNameStr);
    if(extIdx >= 0)
      fileNameStr = fileNameStr.substring(0, extIdx);
    return fileNameStr;
  }
}
