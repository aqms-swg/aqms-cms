//CubeToAnssEQXML.java:  Test utility that converts CUBE-format messages
//                       to ANSS-EQ-XML-format messages.
//
//  5/21/2008 -- [ET]
//

package com.isti.quakewatch.server.test;

import org.jdom.Element;
import org.jdom.Attribute;
import com.isti.quakewatch.server.CubeAnssEQParser;

/**
 * Class CubeToAnssEQXML is a test utility that converts CUBE-format
 * messages to ANSS-EQ-XML-format messages.
 */
public class CubeToAnssEQXML extends CubeToXMLSupport
{
  /**
   * Processes the given CUBE-format messages to XML-format messages.
   * @param args String [] array of optional "message" parameter(s).
   */
  public CubeToAnssEQXML(String [] args)
  {
    super((new CubeAnssEQParserPub()),args);
  }

  /**
   * Executing method for program.
   * @param args String [] array of optional "message" parameter(s).
   */
  public static void main(String [] args)
  {
    new CubeToAnssEQXML(args);
  }


  /**
   * Class CubeAnssEQParserPub extends CubeAnssEQParser to make the
   * 'parseCubeMsgStringData()' method public.
   */
  protected static class CubeAnssEQParserPub extends CubeAnssEQParser
                                             implements CubeParserSupportPub
  {
    /**
     * Parses CUBE-format message data into a JDOM/XML tree of elements.
     * @param messageString the message string to be parsed.
     * @param timeReceivedStr current time (as an XML-format time string).
     * @param actionAttribObj attrib whose value will be set by this method.
     * @return The resulting "EQMessage" Element object, or null if an
     * error occured (in which case the associated error message may be
     * fetched via the 'getErrorMessage()' method).
     */
    public Element parseCubeMsgStringData(String messageString,
                    String timeReceivedStr, Attribute actionAttribObj)
    {
      return super.parseCubeMsgStringData(messageString,
                                          timeReceivedStr, actionAttribObj);
    }
  }
}
