//TestSourcedReqMsgs.java:  Test program for the QWServer
//                         "requestSourcedMsgsStr()" method.
//
//  1/20/2004 -- [ET]  Initial version.
//  8/28/2007 -- [ET]  Added parameter to call to 'QWServerMsgRecord'
//                     constructor.
//

package com.isti.quakewatch.server.test;

import java.util.List;
import java.util.Iterator;
import java.text.DateFormat;
import org.jdom.Element;
import com.isti.util.UtilFns;
import com.isti.util.IstiXmlUtils;
import com.isti.quakewatch.common.MsgTag;
import com.isti.quakewatch.util.QWUtils;
import com.isti.quakewatch.server.QWServerMsgRecord;
import com.isti.quakewatch.server.QWServer;
import com.isti.util.TagValueTable;

/**
 * Class TestSourcedReqMsgs is a test program for the QWServer
 * "requestSourcedMsgsStr()" method.
 */
public class TestSourcedReqMsgs
{
  private static final DateFormat xmlDateFormatterObj =
                                           QWUtils.getXmlDateFormatterObj();
  private static final DateFormat dispDateFormatterObj =
                   UtilFns.createDateFormatObj("yyyy-MM-dd HH:mm:ss.SSS z");

  public static void main(String [] args)
  {
    System.out.println("Starting up QWServer");
  	final String javaVersionString = QWUtils.checkJavaVersion();
  	if (javaVersionString == null)
  	{
  		return;
  	}
    QWServer.staticInvokeStartupServer(javaVersionString, args);
    System.out.println("Waiting for QWServer to be ready");
    QWServer qwServerObj;
    while((qwServerObj=QWServer.getStaticInvokeServerObj()) == null)
    {
      try { Thread.sleep(100); }
      catch(InterruptedException ex) {}
    }
         //wait an extra second to let feeder messages be displayed:
    try { Thread.sleep(1000); }
    catch(InterruptedException ex) {}
    System.out.println(qwServerObj.getServerRevisionString() + " ready");

    try
    {
//      final String recMsgsStr = qwServerObj.requestSourcedMsgsStr(
//        dispDateFormatterObj.parse("2005-01-20 19:29:07.803 EST").getTime(),
//                                                                     "","");
      final TagValueTable tableObj = new TagValueTable();
      tableObj.put("qdds1.wr.usgs.gov",7577);
      tableObj.put("qdds2.er.usgs.gov",333971);
      tableObj.put("dmc.iris.washington.edu",6591);
      final String recMsgsStr = qwServerObj.requestSourcedMsgsStr(
        dispDateFormatterObj.parse("2005-01-20 23:34:00.467 EST").getTime(),
                                           tableObj.getEntriesListStr(),"");

      System.out.println("Received:  " + recMsgsStr);

      final Element recMsgsElemObj =
                                   IstiXmlUtils.stringToElement(recMsgsStr);
      final List recMsgsListObj = recMsgsElemObj.getChildren();
      System.out.println();
      System.out.println("# of messages received:  " +
                                                     recMsgsListObj.size());
      final Iterator iterObj = recMsgsListObj.iterator();
      Element qwMsgElemObj,dataMsgElemObj;
      String timeGenDateStr,str;
      QWServerMsgRecord recObj;
      int msgCount = 0;
      while(iterObj.hasNext())
      {
        qwMsgElemObj = (Element)(iterObj.next());
        dataMsgElemObj = qwMsgElemObj.getChild(MsgTag.DATA_MESSAGE);
        timeGenDateStr =
                      qwMsgElemObj.getAttributeValue(MsgTag.TIME_GENERATED);
        recObj = new QWServerMsgRecord(qwMsgElemObj,dataMsgElemObj,
                     QWUtils.parseStringLong(qwMsgElemObj.getAttributeValue(
                                            MsgTag.MSG_NUMBER)).longValue(),
                            xmlDateFormatterObj.parse(timeGenDateStr),null);
        System.out.println();
        System.out.println("ReceivedMsg " + (++msgCount) + ":  msgNum = " +
                                         recObj.msgNumber + ", timeGen = " +
                      dispDateFormatterObj.format(recObj.getArchiveDate()) +
                      ", " + recObj.getArchiveDate().getTime() + ", " +
                                                            timeGenDateStr);
        if((str=qwMsgElemObj.getAttributeValue(MsgTag.ORIG_TIME_GENERATED))
                                                                     != null)
        {
          System.out.println("OrigTimeGenerated = " + str);
        }
        System.out.println("FdrSourceHost = \"" + recObj.fdrSourceHostStr +
                      "\", FdrSourceMsgNum = " + recObj.fdrSourceMsgNumVal);
      }
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }

    System.out.println();
    System.out.println("Terminating QWServer");
    QWServer.staticInvokeTerminateServer();
  }
}
