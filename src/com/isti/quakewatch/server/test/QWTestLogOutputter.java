//QWTestLogOutputter.java:  QuakeWatch "outputter" module that sends
//                          received messages to a log file.
//
//  7/14/2008 -- [ET]  Modified from 'QWAbstractFeeder' class.
//

package com.isti.quakewatch.server.test;

import com.isti.util.UtilFns;
import com.isti.quakewatch.server.QWServerMsgRecord;
import com.isti.quakewatch.server.outputter.QWAbstractOutputter;

/**
 * Class QWTestLogOutputter is a QuakeWatch "outputter" module that sends
 * received messages to a log file.
 */
public class QWTestLogOutputter extends QWAbstractOutputter
{
  public static final String MODULE_NAME = "QWTestLogOutputter";
  public static final String VERSION_NUM = "1.0";    //name and version strs
  public static final String REVISION_STRING = MODULE_NAME + ", " +
                                                   "Version " + VERSION_NUM;

  /**
   * Constructs a test-log outputter module.
   */
  public QWTestLogOutputter()
  {
    super(MODULE_NAME);      //set thread name to module name
  }

  /**
   * Writes the given message to the log file.
   * @param recObj server-message-record object for message.
   */
  public void outputMessage(QWServerMsgRecord recObj)
  {
    final String domStr = (recObj.msgEvtDomainStr != null) ? ("domain=\"" +
                    recObj.msgEvtDomainStr + "\", ") : UtilFns.EMPTY_STRING;
    final String typStr = (recObj.msgEvtTypeStr != null) ? ("type=\"" +
                      recObj.msgEvtTypeStr + "\", ") : UtilFns.EMPTY_STRING;
    logObj.info("QWTestLogOutputter:  " + domStr + typStr +
                                        "msg:  " + recObj.toArchivedForm());
  }

  /**
   * Returns the current estimated thread count for the outputter.
   * @return The current estimated thread count for the outputter.
   */
  public int getEstimatedThreadCount()
  {
    return 1;
  }

  /**
   * Returns a status message for the outputter.
   * @return Status message string for the outputter; or null if none.
   */
  public String getStatusMessageStr()
  {
    return null;
  }

  /**
   * Returns the revision String for the feeder.
   * @return the revision String for the feeder.
   */
  public String getRevisionString()
  {
    return REVISION_STRING;
  }
}
