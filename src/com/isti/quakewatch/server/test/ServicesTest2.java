//ServicesTest2.java:  Test program to exercise QuakeWatch acceptor
//                     and services methods, and the request-messages
//                     mechanism.

package com.isti.quakewatch.server.test;

import java.util.*;
import java.text.SimpleDateFormat;
import org.jdom.Element;
import org.jdom.JDOMException;
import com.isti.util.UtilFns;
import com.isti.util.IstiXmlUtils;
import com.isti.openorbutil.OrbManager;
import com.isti.quakewatch.common.qw_services.QWAcceptor;
import com.isti.quakewatch.common.qw_services.QWAcceptorHelper;
import com.isti.quakewatch.common.qw_services.QWServices;
import com.isti.quakewatch.common.qw_services.QWServicesHelper;
import com.isti.quakewatch.common.MsgTag;

/**
 * Class ServicesTest2 is a test program to exercise QuakeWatch acceptor
 * and services methods, and the request-messages mechanism.
 */
public class ServicesTest2
{
  public static final SimpleDateFormat xmlDateFormatterObj =
                       new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

  static      //static initialization block; executes only once
  {                     //set time zone for date formatter to GMT:
    xmlDateFormatterObj.setTimeZone(TimeZone.getTimeZone("GMT"));
  }

  public ServicesTest2(String [] args)
  {
    final OrbManager orbManagerObj = new OrbManager(args);
    orbManagerObj.initImplementation();

    org.omg.CORBA.Object cObj;
    System.out.println("Calling resolveCorbalocObject()");
    if((cObj=orbManagerObj.resolveCorbalocObject(
                                   "localhost",39977,"QWAcceptor")) == null)
    {
      System.err.println("Error resolving Corbaloc object:  " +
                                           orbManagerObj.getErrorMessage());
      return;
    }
    System.out.println("Narrowing QWAcceptor object");
    final QWAcceptor qwAcceptorObj = QWAcceptorHelper.narrow(cObj);
    System.out.println("Calling QWAcceptor.newConnection()");
    final String qwServicesIorStr = qwAcceptorObj.newConnection(
                             "testname","testpwd",UtilFns.getLocalHostIP());
    if(qwServicesIorStr == null || qwServicesIorStr.length() <= 0)
    {
      System.err.println(
            "Empty string returned from QWAcceptor.newConnection() method");
      return;
    }
    if(qwServicesIorStr.startsWith(QWAcceptor.ERROR_MSG_PREFIX))
    {
      System.err.println("Error message returned from QWAcceptor." +
                   "newConnection() method:  " + qwServicesIorStr.substring(
                                     QWAcceptor.ERROR_MSG_PREFIX.length()));
      return;
    }
    System.out.println("Received QWServices IOR string:  " +
                                                          qwServicesIorStr);
    System.out.println("Converting and narrowing QWServices object");
    final QWServices qwServicesObj = QWServicesHelper.narrow(
                   orbManagerObj.orbObj.string_to_object(qwServicesIorStr));
    System.out.println("Calling QWServices.getEventChLocStr()");
    final String eventChannelStr = qwServicesObj.getEventChLocStr();
    System.out.println("Received event channel IOR string:  " +
                                                           eventChannelStr);

    System.out.println(
                  "Making initial call to qwServicesObj.requestMessages()");
    String reqMsgsStr = qwServicesObj.requestMessages(
                           System.currentTimeMillis()-(7L*24*60*60*1000),0);
//    String reqMsgsStr = qwServicesObj.requestMessages(0,0);
    Element elementObj;
    String str;
    int numReqVal;
    long msgNum = 0, timeGenVal = 0;
    List msgElementsList;
    Iterator iterObj;
    Object obj;
    Date timeGenDateObj;
    outerLoop:
    while(true)
    {
      System.out.println("  Received:  " + reqMsgsStr);
      try
      {
        elementObj = IstiXmlUtils.stringToElement(reqMsgsStr);
      }
      catch(JDOMException ex)
      {
        System.err.println("Error reported by 'stringToElement()':  " + ex);
        break;
      }
      if(!MsgTag.QW_RESEND.equals(elementObj.getName()))
      {
        System.err.println("Lead element of received resend not named \"" +
                                                   MsgTag.QW_RESEND + "\"");
        break;
      }
      if((str=elementObj.getAttributeValue(MsgTag.NUM_REQUESTED)) ==  null)
      {
        System.err.println("Unable to find \"" + MsgTag.NUM_REQUESTED +
                                           "\" element in received resend");
        break;
      }
      try
      {
        numReqVal = Integer.parseInt(str);
      }
      catch(NumberFormatException ex)
      {
        System.err.println("Error parsing num-request value (\"" +
                                            str + "\") in received resend");
        break;
      }
      System.out.println("Received num-request value:  " + numReqVal);
      msgElementsList = elementObj.getChildren(MsgTag.QW_MESSAGE);
      System.out.println("Number of message objects received:  " +
                                                    msgElementsList.size());
      iterObj = msgElementsList.iterator();
      while(iterObj.hasNext())
      {
        if((obj=iterObj.next()) instanceof Element)
          elementObj = (Element)obj;
        else
        {
          System.err.println("Non-element object found in list:  " + obj);
          break outerLoop;
        }
        if(!MsgTag.QW_MESSAGE.equals(elementObj.getName()))
        {
          System.err.println("Non-\"" + MsgTag.QW_MESSAGE +
                                                "\"-element found in list");
          break outerLoop;
        }
        if((str=elementObj.getAttributeValue(MsgTag.MSG_NUMBER)) == null)
        {
          System.err.println("Unable to find \"" + MsgTag.MSG_NUMBER +
                  "\" attribute in \"" + MsgTag.QW_MESSAGE +  "\" element");
          break outerLoop;
        }
        try
        {
          msgNum = Long.parseLong(str);
        }
        catch(NumberFormatException ex)
        {
          System.err.println("Error parsing msgNum value (\"" +
                                           str + "\") in received message");
          break;
        }
        if((str=elementObj.getAttributeValue(MsgTag.TIME_GENERATED)) == null)
        {
          System.err.println("Unable to find \"" + MsgTag.TIME_GENERATED +
                  "\" attribute in \"" + MsgTag.QW_MESSAGE +  "\" element");
          break outerLoop;
        }
        try
        {
          timeGenDateObj = xmlDateFormatterObj.parse(str);
        }
        catch(Exception ex)
        {
          System.err.println("Error parsing time-generated value (\"" +
                                           str + "\") in received message");
          break;
        }
        timeGenVal = timeGenDateObj.getTime();
        System.out.println("  Received:  msgNum=" + msgNum +
                                                 ", time-generated=" + str);
      }
      if(msgElementsList.size() >= numReqVal)
        break;
      System.out.println("Calling qwServicesObj.requestMessages(" +
                                           timeGenVal + "," + msgNum + ")");
      reqMsgsStr = qwServicesObj.requestMessages(timeGenVal,msgNum);
    }


    System.out.println("Complete");
    orbManagerObj.closeImplementation(false);
  }

  public static void main(String [] args)
  {
    new ServicesTest2(args);
  }
}
