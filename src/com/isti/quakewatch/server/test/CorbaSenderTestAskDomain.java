//CorbaSenderTestAskDomain.java:  CORBA-feeder test program.
//
//   8/6/2004 -- [ET]  Modified sleep-time ("-w") parameter to allow
//                     floating-point values.
//

package com.isti.quakewatch.server.test;

import com.isti.util.UtilFns;
import com.isti.openorbutil.OrbManager;
import com.isti.quakewatch.server.QWCorbaFeeder;
import com.isti.quakewatch.server.qw_feeder.QWFeeder;
import com.isti.quakewatch.server.qw_feeder.QWFeederHelper;
import java.util.Date;
import com.isti.quakewatch.util.QWUtils;
import com.isti.openorbutil.EvtChEventType;

/**
 * Class CorbaSenderTestAskDomain is a CORBA-feeder test program.
 */
public class CorbaSenderTestAskDomain
{
  private boolean useBig = false;
  private int sleepTime = 1000;
  private String domain = "RT101";
  private String channel = "/conntest/receiver";
  private boolean constantLoopFlag = false;
  private boolean createDupsFlag = false;
  private boolean domainTypeNamesFlag = true;
  private String serverHostStr = "localhost";
  private int serverPortNum = QWCorbaFeeder.DEF_PORT_NUM;

  public CorbaSenderTestAskDomain(String [] programArgs)
  {
//    serverPortNum = 38888;

   String str;
   if(programArgs != null)
   {
     int slotNum = 0;
     for(int i=0; i<programArgs.length; ++i)
     {
       if((str=programArgs[i]).startsWith("-"))
       {
         if("-c".equalsIgnoreCase(str))
           createDupsFlag = true;
         if("-l".equalsIgnoreCase(str))
           constantLoopFlag = true;
         if("-n".equalsIgnoreCase(str))
           domainTypeNamesFlag = true;
         if("-b".equalsIgnoreCase(str))
           useBig = true;
         if("-w".equalsIgnoreCase(str)) {
           //           String s = programArgs[++i];
           //           Integer I = Integer.getInteger(s);
           double sTimeVal;
           try
           {
             sTimeVal = Double.parseDouble(programArgs[++i]);
           }
           catch(NumberFormatException ex)
           {
             sTimeVal = 1.0;
             System.err.println("Unable to parse sleep-time (\"-w\") " +
                                           "parameter:  " + programArgs[i]);
           }
           System.err.println("seconds pause is >" + sTimeVal + "<");
           sleepTime = (int)(sTimeVal * 1000 + 0.5);  //convert secs to ms
         }
         if("-d".equalsIgnoreCase(str)) {
           domain = programArgs[++i];
           System.err.println("domain is >" + domain + "<");
         }
         if("-e".equalsIgnoreCase(str)) {
           channel = programArgs[++i];
           System.err.println("channel is >" + channel + "<");
         }

         else
         {
           if("-h".equalsIgnoreCase(str))
           {
             System.out.println(
                      "Usage:  CorbaSenderTest [-n] [hostname] [portnum]");
             System.exit(0);
             return;
           }
         }
       }
       else
       {
         if(slotNum == 0)
           serverHostStr = programArgs[i];
         else if(slotNum == 1)
         {
           try
           {
             final int val;
             if((val=Integer.parseInt(programArgs[i])) > 0)
               serverPortNum = val;
             else
             {
               System.err.println("Invalid port number parameter:  " +
                                                           programArgs[i]);
               System.exit(1);
               return;
             }
           }
           catch(NumberFormatException ex)
           {
             System.err.println("Invalid port number parameter:  " +
                                                           programArgs[i]);
             System.exit(1);
             return;
           }
         }
         ++slotNum;
       }
     }
   }

   System.out.println("Server host name = \"" + serverHostStr +
                                     "\", port number = " + serverPortNum);

   final OrbManager orbManagerObj = new OrbManager(programArgs);
   orbManagerObj.initImplementation();

   org.omg.CORBA.Object cObj;
   System.out.println("Calling resolveCorbalocObject()");
   if((cObj=orbManagerObj.resolveCorbalocObject(serverHostStr,
                   serverPortNum,QWCorbaFeeder.CORBALOC_REF_NAME)) == null)
   {
     System.err.println("Error resolving Corbaloc object:  " +
                                          orbManagerObj.getErrorMessage());
     return;
   }

   System.out.println("Narrowing QWFeeder object");
   final QWFeeder qwFeederObj = QWFeederHelper.narrow(cObj);

   if(domainTypeNamesFlag)
     System.out.println("Using event domain/type names");

     //create new thread to process user input:
   (new Thread()
       {
         public void run()
         {
           System.out.println(
                            "Enter messages to be sent (or 'Q' to quit):");
           int eventCount = 0;
           boolean retFlag;

           StringBuffer sb = new StringBuffer();
           String domainstr;
           String chanstr;

           while(true)
           {
             if (constantLoopFlag) {
               domainstr = new String(domain + ":" + channel);
             } else {
               domainstr=UtilFns.getUserConsoleString();
             }
             if(domainstr != null)
                     {   //user string fetched from console OK

               if(domainstr.equalsIgnoreCase("Q"))
                 break;         //if keyword then exit loop

               sb.delete(0,500);

               Date currentTime_1 = new Date();

               // time in millis
               long time = -1;
               if (!createDupsFlag) {
                 time = currentTime_1.getTime();
                 time = time/1000;
               }

               EvtChEventType[] tar = null;
               try
               {
                 tar = QWUtils.listStringToEventTypeArray(domainstr);
               }
               catch (Exception ex)
               {
                 ex.printStackTrace();
                 continue;
               }
               if (tar == null)
                 continue;
               if (tar.length == 0)
                 continue;

               domainstr = tar[0].domainStr;
               chanstr = tar[0].typeStr;

               System.out.println(time);
               String tstr;
               if (useBig) {
                 tstr = getBigMessage(time);
               } else {
                 tstr = getKeyMessage(time);
               }
               try
               {       //send message to feeder:
                 if(domainTypeNamesFlag)
                     {     //using structured events
                   retFlag = qwFeederObj.sendDomainTypeNameMessage(
                       domainstr,
                       chanstr,
                       ("event"+(eventCount+1)),tstr);
                   ++eventCount;
                 }
                 else {
                   //not using structured events
                   retFlag = qwFeederObj.sendMessage(domainstr);
                 }
                 System.out.println("Feeder response:  " + retFlag);
               }
               catch(Exception ex)
                   {          //exception error; show error message
                 System.err.println(
                     "Error sending message to feeder:  " + ex);
               }
               try { Thread.sleep(sleepTime); }
               catch(InterruptedException ex) {}
             }
           }
           System.out.println("Closing implementation");
           //close implementation; exit program
           orbManagerObj.closeImplementation(false);
         }
         }).start();

       orbManagerObj.runImplementation();      //run implementation
       System.out.println("Implementation has shutdown");
  }

  private String getKeyMessage(long time) {

    StringBuffer sb = new StringBuffer();
    sb.append("<ValuesMessage Chan=\"/parameters/trigger/carlsubtrig/subnettrig\" Domain=\"" + domain +"\" Name=\"Subnet_Trigger\" Source=\"MOD_TRIG_PUB\">");

    sb.append("<Int4 Name=\"NoName\">45347</Int4>");

    sb.append("<String Name=\"NoName\">CI</String>");

    sb.append("<String Name=\"NoName\">RT3</String>");

    sb.append("<String Name=\"NoName\">SUBTRIG</String>");

    sb.append("<Real8 Name=\"NoName\">1089321812.000000</Real8>");

    sb.append("<Int4 Name=\"NoName\">"  + time/1000 + "</Int4>");

    sb.append("<Real8 Name=\"NoName\">0.000000</Real8>");

    sb.append("<Real8 Name=\"NoName\">0.000000</Real8>");

    sb.append("<Int4 Name=\"NoName\">46</Int4>");

    sb.append("<Int4 Name=\"NoName\">"  + time + "</Int4>");

    sb.append("</ValuesMessage>");

    return sb.toString();

  }

  private String getBigMessage(long time) {

    StringBuffer sb = new StringBuffer();
    sb.append("<ValuesMessage Chan=\"/parameters/trigger/carlsubtrig/subnettrig\" Domain=\"" + domain +"\" Name=\"Subnet_Trigger\" Source=\"MOD_TRIG_PUB\">");

  sb.append("<Int4 Name=\"NoName\">45347</Int4>");

  sb.append("<String Name=\"NoName\">CI</String>");

  sb.append("<String Name=\"NoName\">RT3</String>");

  sb.append("<String Name=\"NoName\">SUBTRIG</String>");

  sb.append("<Real8 Name=\"NoName\">1089321812.000000</Real8>");

  sb.append("<Int4 Name=\"NoName\">"  + time/1000 + "</Int4>");

  sb.append("<Real8 Name=\"NoName\">0.000000</Real8>");

  sb.append("<Real8 Name=\"NoName\">0.000000</Real8>");

  sb.append("<Int4 Name=\"NoName\">46</Int4>");

  sb.append("<StringArray Name=\"NoName\">");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>BK</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>NP</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
    sb.append("<String>CI</String>");
  sb.append("</StringArray>");

  sb.append("<StringArray Name=\"NoName\">");
    sb.append("<String>BAK</String>");
    sb.append("<String>BCC</String>");
    sb.append("<String>CBK</String>");
    sb.append("<String>CCA</String>");
    sb.append("<String>CCC</String>");
    sb.append("<String>CCC</String>");
    sb.append("<String>CLC</String>");
    sb.append("<String>CLC</String>");
    sb.append("<String>CLC</String>");
    sb.append("<String>CLT</String>");
    sb.append("<String>DTP</String>");
    sb.append("<String>EW2</String>");
    sb.append("<String>GRA</String>");
    sb.append("<String>GSA</String>");
    sb.append("<String>JFS</String>");
    sb.append("<String>JRC2</String>");
    sb.append("<String>LCP</String>");
    sb.append("<String>LRL</String>");
    sb.append("<String>LRL</String>");
    sb.append("<String>LRR</String>");
    sb.append("<String>MPM</String>");
    sb.append("<String>MPM</String>");
    sb.append("<String>OAK</String>");
    sb.append("<String>POTR</String>");
    sb.append("<String>QAL</String>");
    sb.append("<String>SCZ2</String>");
    sb.append("<String>SLA</String>");
    sb.append("<String>SLA</String>");
    sb.append("<String>SRT</String>");
    sb.append("<String>TCF</String>");
    sb.append("<String>TIN</String>");
    sb.append("<String>TOW</String>");
    sb.append("<String>WBM</String>");
    sb.append("<String>WBS</String>");
    sb.append("<String>WBS</String>");
    sb.append("<String>WCH</String>");
    sb.append("<String>WCS</String>");
    sb.append("<String>WMF</String>");
    sb.append("<String>WNM</String>");
    sb.append("<String>WOR</String>");
    sb.append("<String>WRC</String>");
    sb.append("<String>WRV</String>");
    sb.append("<String>WSH</String>");
    sb.append("<String>WVP</String>");
    sb.append("<String>WWP</String>");
    sb.append("<String>YEG</String>");
  sb.append("</StringArray>");

  sb.append("<StringArray Name=\"NoName\">");
    sb.append("<String>HHZ</String>");
    sb.append("<String>HHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>BHZ</String>");
    sb.append("<String>HHZ</String>");
    sb.append("<String>BHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>HHZ</String>");
    sb.append("<String>HHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>HHZ</String>");
    sb.append("<String>HLZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>HHZ</String>");
    sb.append("<String>HHZ</String>");
    sb.append("<String>BHZ</String>");
    sb.append("<String>HHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>BHZ</String>");
    sb.append("<String>HHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>HHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>HHZ</String>");
    sb.append("<String>BHZ</String>");
    sb.append("<String>HHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>HLZ</String>");
    sb.append("<String>HHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>BHZ</String>");
    sb.append("<String>HHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>EHZ</String>");
    sb.append("<String>EHZ</String>");
  sb.append("</StringArray>");

  sb.append("<StringArray Name=\"NoName\">");
    sb.append("<String>t</String>");
    sb.append("<String>t</String>");
    sb.append("<String>t</String>");
    sb.append("<String>t</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>t</String>");
    sb.append("<String>t</String>");
    sb.append("<String>m</String>");
    sb.append("<String>t</String>");
    sb.append("<String>t</String>");
    sb.append("<String>t</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>t</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>t</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>t</String>");
    sb.append("<String>t</String>");
    sb.append("<String>t</String>");
    sb.append("<String>t</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>t</String>");
    sb.append("<String>t</String>");
    sb.append("<String>t</String>");
    sb.append("<String>t</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>m</String>");
    sb.append("<String>t</String>");
    sb.append("<String>m</String>");
    sb.append("<String>t</String>");
    sb.append("<String>t</String>");
  sb.append("</StringArray>");

  sb.append("<Real8Array Name=\"NoName\">");
    sb.append("<Real8>1089321804.000000</Real8>");
    sb.append("<Real8>1089321803.000000</Real8>");
    sb.append("<Real8>1089321840.000000</Real8>");
    sb.append("<Real8>1089321827.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>1089321827.000000</Real8>");
    sb.append("<Real8>1089321812.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>1089321826.000000</Real8>");
    sb.append("<Real8>1089321805.000000</Real8>");
    sb.append("<Real8>1089321843.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>1089321804.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>1089321832.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>1089321845.000000</Real8>");
    sb.append("<Real8>1089321809.000000</Real8>");
    sb.append("<Real8>1089321820.000000</Real8>");
    sb.append("<Real8>1089321838.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>1089321812.000000</Real8>");
    sb.append("<Real8>1089321808.000000</Real8>");
    sb.append("<Real8>1089321814.000000</Real8>");
    sb.append("<Real8>1089321803.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>1089321807.000000</Real8>");
    sb.append("<Real8>0.000000</Real8>");
    sb.append("<Real8>1089321804.000000</Real8>");
    sb.append("<Real8>1089321803.000000</Real8>");
  sb.append("</Real8Array>");

  sb.append("<Real8Array Name=\"NoName\">");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
    sb.append("<Real8>1089321792.000000</Real8>");
  sb.append("</Real8Array>");

  sb.append("<Real8Array Name=\"NoName\">");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("<Real8>55.000000</Real8>");
    sb.append("</Real8Array>");


    sb.append("<Int4 Name=\"NoName\">"  + time + "</Int4>");

    sb.append("</ValuesMessage>");

    return sb.toString();

  }

   public static void main(String [] args)
 {
   new CorbaSenderTestAskDomain(args);
 }

}