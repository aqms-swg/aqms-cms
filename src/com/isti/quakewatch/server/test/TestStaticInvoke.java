//TestStaticInvoke.java:  Test program for QWServer "staticInvoke" methods.
//
// 12/10/2004 -- [ET]
//

package com.isti.quakewatch.server.test;

import com.isti.util.UtilFns;
import com.isti.quakewatch.server.QWServer;
import com.isti.quakewatch.util.QWUtils;

/**
 * Class TestStaticInvoke is a test program for QWServer "staticInvoke"
 * methods.
 */
public class TestStaticInvoke
{
  private static boolean serverTerminatedFlag = false;
  private static Thread monitorThreadObj = null;

  public static void main(String [] args)
  {
    System.out.println("Starting up QWServer");
    if(args != null && args.length > 0)
    {
      final String javaVersionString = QWUtils.checkJavaVersion();
      if (javaVersionString == null)
      {
    	  return;
      }
      QWServer.staticInvokeStartupServer(javaVersionString, args);
    }
    else      //no command-line parameters given
    {              //demonstrate how to pass in config file path/file name:
      QWServer.staticInvokeStartupServer("--" + QWServer.CFG_PARAM_SPEC +
                                      "=\"" + QWServer.CONFIG_FNAME + "\"");
    }
    System.out.println("Waiting for QWServer to be ready");
    QWServer qwServerObj;
    while((qwServerObj=QWServer.getStaticInvokeServerObj()) == null)
    {
      try { Thread.sleep(100); }
      catch(InterruptedException ex) {}
    }
         //wait an extra second to let feeder messages be displayed:
    try { Thread.sleep(1000); }
    catch(InterruptedException ex) {}
    System.out.println(qwServerObj.getServerRevisionString() +
                                 " ready; enter 'I' for info, 'Q' to quit");

      //create new thread to process user input:
    (new Thread()
        {
          public void run()
          {
            String str;
            while(true)
            {
              if((str=UtilFns.getUserConsoleString().trim()) != null)
              {   //user string fetched from console OK
                if(str.equalsIgnoreCase("Q"))
                  break;         //if user entered 'Q' then exit loop
                if(str.equalsIgnoreCase("I"))
                {  //user entered 'I'
                  final QWServer qwServerObj;
                  if((qwServerObj=QWServer.getStaticInvokeServerObj()) !=
                                                                       null)
                  {
                    System.out.println("Info:  " +
                                       qwServerObj.getServerCacheInfoStr());
                  }
                }
              }
              else
              {   //error fetching user string from console
                            //delay a bit before trying again:
                try { Thread.sleep(3000); }
                catch(InterruptedException ex) {}
              }
            }
            System.out.println("Terminating QWServer");
            QWServer.staticInvokeTerminateServer();
            serverTerminatedFlag = true;
            if(monitorThreadObj != null)
              monitorThreadObj.interrupt();
          }
        }).start();

    (monitorThreadObj = new Thread()
        {
          public void run()
          {
            QWServer qwServerObj;
            String msgStr;
            long newReqTimeVal, reqTimeVal = System.currentTimeMillis();
            while(!serverTerminatedFlag)
            {
              try { Thread.sleep(10000); }
              catch(InterruptedException ex) {}
              if(serverTerminatedFlag)
                break;
              if((qwServerObj=QWServer.getStaticInvokeServerObj()) != null)
              {
                newReqTimeVal = System.currentTimeMillis();
                if((msgStr=qwServerObj.requestMessagesStr(
                                                reqTimeVal,0,null)) != null)
                {
                  if(msgStr.length() > 30)
                    System.out.println("Received:  " + msgStr);
                }
                else
                  System.out.println("'requestMessagesStr()' returned null");
                reqTimeVal = newReqTimeVal;
              }
            }
          }
        }).start();
  }
}
