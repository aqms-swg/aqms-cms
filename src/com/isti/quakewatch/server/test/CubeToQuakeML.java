//CubeToQuakeML.java:  Test utility that converts CUBE-format messages
//                       to QuakeML-format messages.
//
//  3/12/2010 -- [ET]
//

package com.isti.quakewatch.server.test;

import org.jdom.Element;
import org.jdom.Attribute;
import com.isti.quakewatch.server.CubeQuakeMLParser;

/**
 * Class CubeToQuakeML is a test utility that converts CUBE-format
 * messages to QuakeML-format messages.
 */
public class CubeToQuakeML extends CubeToXMLSupport
{
  /**
   * Processes the given CUBE-format messages to XML-format messages.
   * @param args String [] array of optional "message" parameter(s).
   */
  public CubeToQuakeML(String [] args)
  {
    super((new CubeQuakeMLParserPub()),args);
  }

  /**
   * Executing method for program.
   * @param args String [] array of optional "message" parameter(s).
   */
  public static void main(String [] args)
  {
    new CubeToQuakeML(args);
  }


  /**
   * Class CubeQuakeMLParserPub extends CubeQuakeMLParser to make the
   * 'parseCubeMsgStringData()' method public.
   */
  protected static class CubeQuakeMLParserPub extends CubeQuakeMLParser
                                             implements CubeParserSupportPub
  {
    /**
     * Parses CUBE-format message data into a JDOM/XML tree of elements.
     * @param messageString the message string to be parsed.
     * @param timeReceivedStr current time (as an XML-format time string).
     * @param actionAttribObj attrib whose value will be set by this method.
     * @return The resulting "quakeMessage" Element object, or null if an
     * error occured (in which case the associated error message may be
     * fetched via the 'getErrorMessage()' method).
     */
    public Element parseCubeMsgStringData(String messageString,
                    String timeReceivedStr, Attribute actionAttribObj)
    {
      return super.parseCubeMsgStringData(messageString,
                                          timeReceivedStr, actionAttribObj);
    }
  }
}
