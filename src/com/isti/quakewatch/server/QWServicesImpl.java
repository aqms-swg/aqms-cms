//QWServicesImpl.java:  Implementation for the QuakeWatch services.
//
//   6/4/2003 -- [ET]  Initial version.
//  7/28/2003 -- [ET]  Added 'getServerIdNameStr()' method.
//   8/6/2003 -- [ET]  Added 'getAltServersIdsListStr()' method.
// 12/30/2003 -- [ET]  Added 'getServerHostAddrStr()' and
//                     'getStatusMsgTypeNameStr()' methods.
//   1/5/2004 -- [ET]  Added 'requestFilteredMessages()' method.
//  7/14/2004 -- [ET]  Added placeholder versions of 'clientStatusCheck()'
//                     and 'getClientUpgradeInfo()' methods.
//  7/30/2004 -- [ET]  Added placeholder versions of 'getConnectionStatusVal()'
//                     'getConnectionStatusMsg()' and
//                     'getRedirectedServerLoc()'.
//   9/7/2004 -- [ET]  Added 'connStatusVal' parameter to constructor and
//                     implemented connection-status methods; implemented
//                     'clientStatusCheck()' and 'getClientUpgradeInfo()'
//                     methods.
//  9/15/2004 -- [ET]  Added 'getCertificateFileData()' method.
//  9/30/2004 -- [ET]  Added support for connection-status values
//                     'CS_BAD_DISTNAME' and 'CS_CONN_ERROR'.
// 10/13/2004 -- [ET]  Changed status message for 'CS_BAD_DISTNAME'.
// 11/17/2004 -- [ET]  Implemented 'getRedirectedServerLoc()' method.
// 11/23/2004 -- [ET]  Added methods 'getServerRevisionString()' and
//                     'disconnectClient()'.
//  1/10/2005 -- [ET]  Changed level of "Unknown client attempted to
//                     disconnect" log output from "Warning" to "Debug".
//  1/24/2005 -- [ET]  Added methods 'requestSourcedMessages()' and
//                     'requestSourcedFilteredMessages()'.
//   3/4/2008 -- [ET]  Added methods 'getStatusReportTime()' and
//                     'getStatusReportData()'.
//

package com.isti.quakewatch.server;

import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.quakewatch.common.qw_services.QWServices;
import com.isti.quakewatch.common.qw_services.QWServicesPOA;

/**
 * Class QWServicesImpl defines the implementation for the QuakeWatch
 * services.
 */
public class QWServicesImpl extends QWServicesPOA
{
  private final QWServer qwServerObj;
  private final LogFile logObj;
  private final QWClientsManager clientsManagerObj;
  private final int connectionStatusValue;
  private final String connStatusMessageStr;
  private final String shortConnStatusStr;
  private final boolean msgsAllowedFlag;    //true if message access allowed

  /**
   * Constructs a services object.
   * @param qwServerObj the 'QWServer' object to use.
   * @param connStatusVal the connection-status value for this
   * services object, one of the 'CS_' values.
   */
  public QWServicesImpl(QWServer qwServerObj, int connStatusVal)
  {
    this.qwServerObj = qwServerObj;
    logObj = qwServerObj.getLogFileObj();
    clientsManagerObj = qwServerObj.getClientsManagerObj();
    switch(connectionStatusValue = connStatusVal)
    {    //setup status strings based on value
      case QWServices.CS_CONNECT_OK:
        connStatusMessageStr = "Successfully connected to server";
        shortConnStatusStr = "Connection_OK";
        msgsAllowedFlag = true;        //message access allowed
        break;
      case QWServices.CS_SERVER_REDIRECT:
        connStatusMessageStr = "Redirected to another server";
        shortConnStatusStr = "Server_redirect";
        msgsAllowedFlag = false;       //message access not allowed
        break;
      case QWServices.CS_INVALID_LOGIN:
        connStatusMessageStr = "Login username/password not valid";
        shortConnStatusStr = "Invalid_login";
        msgsAllowedFlag = false;       //message access not allowed
        break;
      case QWServices.CS_VERSION_OBSOLETE:
        connStatusMessageStr = "Client program version too old";
        shortConnStatusStr = "Version_obsolete";
        msgsAllowedFlag = false;       //message access not allowed
        break;
      case QWServices.CS_BAD_DISTNAME:
        connStatusMessageStr = "Unrecognized client; not allowed to connect";
        shortConnStatusStr = "Bad_distName";
        msgsAllowedFlag = false;       //message access not allowed
        break;
      case QWServices.CS_CONN_ERROR:
        connStatusMessageStr = "Error occurred while setting up connection";
        shortConnStatusStr = "Conn_error";
        msgsAllowedFlag = false;       //message access not allowed
        break;
      default:
        connStatusMessageStr = "Unknown connection status";
        shortConnStatusStr = "Unknown_status";
        msgsAllowedFlag = false;       //message access not allowed
    }
  }

  /**
   * Fetches a value indicating the status of the current
   * connection to the server.
   * @return One of the 'CS_' values.
   */
  public int getConnectionStatusVal()
  {
    return connectionStatusValue;
  }

  /**
   * Fetches a message string describing the status of the
   * current connection to the server.
   * @return A message string describing the status of the
   * current connection to the server.
   */
  public String getConnectionStatusMsg()
  {
    return connStatusMessageStr;
  }

  /**
   * Fetches the location of the server that the client is
   * being redirected to.  The returned string may also be
   * a comma-separated list of "hostAddr:portNum" entries.
   * @return The redirect-server location(s) string in the form
   * "hostAddr:portNum", or an empty string if the client is
   * not being redirected.
   */
  public String getRedirectedServerLoc()
  {
    final String redirStr = qwServerObj.getRedirServersIdsList(
                                   qwServerObj.getRandomizeRedirListFlag());
    logObj.debug2("QWServicesImpl:  Returning redirected-server string:  " +
                                                                  redirStr);
    return redirStr;
  }

  /**
   * Fetches the server ID name string (defined in the QWServer's
   * configuration file).
   * @return The server ID name string.
   */
  public String getServerIdNameStr()
  {
    final String retStr;     //return ID name string, or empty str if null:
    return ((retStr=qwServerObj.getServerIdName()) != null) ? retStr :
                                                       UtilFns.EMPTY_STRING;
  }

  /**
   * Fetches the server host address string.
   * @return The server host address string.
   */
  public String getServerHostAddrStr()
  {
    final String retStr;     //return address string, or empty str if null:
    return ((retStr=qwServerObj.getServerHostAddress()) != null) ?
                                              retStr : UtilFns.EMPTY_STRING;
  }

  /**
   * Returns the revision string for this QuakeWatch server.
   * @return The revision string for this QuakeWatch server.
   */
  public String getServerRevisionString()
  {
    final String retStr;     //return revision string, or empty str if null:
    return ((retStr=qwServerObj.getServerRevisionString()) != null) ?
                                              retStr : UtilFns.EMPTY_STRING;
  }

  /**
   * Returns the type-name string used on status messages sent out
   * by the server.  When structured messages are enabled, status
   * messages will be sent with the domain name "StatusMessage"
   * and the type name set to this value.  These names can be used
   * on the client side to filter-in the status messages.
   * @return The type-name string used on status messages sent out
   * by the server (when structured messages are enabled).
   */
  public String getStatusMsgTypeNameStr()
  {
    if(msgsAllowedFlag)
    {    //message access allowed for this services object
      final String retStr;   //return name string, or empty str if null:
      return ((retStr=qwServerObj.getStatusMsgTypeName()) != null) ?
                                              retStr : UtilFns.EMPTY_STRING;
    }
    logObj.warning("Client call to 'getStatusMsgTypeNameStr()' rejected, " +
                          "QWServices conn-status = " + shortConnStatusStr);
    return UtilFns.EMPTY_STRING;
  }

  /**
   * Fetches the list of alternate server IDs (defined in the
   * QWServer's configuration file).
   * @return The list of alternate server IDs, as a string in
   * the form "hostAddr:portNum,hostAddr:portNum,...".
   */
  public String getAltServersIdsListStr()
  {
    if(msgsAllowedFlag)
    {    //message access allowed for this services object
      final String retStr;   //return list string, or empty str if null:
      return ((retStr=qwServerObj.getAltServersIdsList()) != null) ?
                                              retStr : UtilFns.EMPTY_STRING;
    }
    logObj.warning("Client call to 'getAltServersIdsListStr()' rejected, " +
                          "QWServices conn-status = " + shortConnStatusStr);
    return UtilFns.EMPTY_STRING;
  }

  /**
   * Fetches the locator string for the CORBA-event-channel.
   * @return The locator string, or an empty string if an error
   * occurred.
   */
  public String getEventChLocStr()
  {
    if(msgsAllowedFlag)
    {    //message access allowed for this services object
      final String retStr;   //return locator string, or empty str if null:
      return ((retStr=qwServerObj.getMainEvtChLocatorStr()) != null) ?
                                              retStr : UtilFns.EMPTY_STRING;
    }
    logObj.warning("Client call to 'getEventChLocStr()' rejected, " +
                          "QWServices conn-status = " + shortConnStatusStr);
    return UtilFns.EMPTY_STRING;
  }

  /**
   * Requests that a server-alive message be sent immediately.  This allows
   * a client to verify its connection to the event channel more quickly.
   */
  public void requestAliveMessage()
  {
    if(msgsAllowedFlag)
    {    //message access allowed for this services object
      qwServerObj.requestImmedAliveMsg();
    }
    else
    {
      logObj.warning("Client call to 'requestAliveMessage()' rejected, " +
                          "QWServices conn-status = " + shortConnStatusStr);
    }
  }

  /**
   * Requests that messages newer or equal to the specified time value or
   * later than the specified message number be returned.  If a non-zero
   * message number is given and the given time value matches then
   * messages with message numbers greater than the given message number
   * are returned; otherwise messages newer or equal to the specified time
   * value are returned (within a tolerance value).  An XML message
   * containing the messages is built and returned, using the following
   * format: <pre>
   * [QWresend NumRequested="10"]
   *   [QWmessage' ... '/QWmessage]
   *   [QWmessage' ... '/QWmessage]
   *   ...
   * [/QWresend]
   * </pre> (Substitute angle brackets for square brackets.)
   * The number of messages that would be required to fill the request
   * is returned in the "NumRequested" attribute.  However, no more than
   * the oldest 'maxResendNumMsgs' number of messages will be returned
   * in any single call to this method (additional calls may be needed to
   * fetch all of the desired messages).
   * @param timeVal the time-generated value for message associated with
   * the given message number, or the requested time value to be used
   * (milliseconds since 1/1/1970).
   * @param msgNum the message number to use, or 0 or none.
   * @return An XML-formatted string containing the messages, or an empty
   * string if an error occurs.
   */
  public String requestMessages(long timeVal, long msgNum)
  {
    if(msgsAllowedFlag)
    {    //message access allowed for this services object
      return qwServerObj.requestMessagesStr(timeVal,msgNum,null);
    }
    logObj.warning("Client call to 'requestMessages()' rejected, " +
                          "QWServices conn-status = " + shortConnStatusStr);
    return UtilFns.EMPTY_STRING;
  }

  /**
   * Requests that messages newer or equal to the specified time value or
   * later than the specified message number be returned.  If a non-zero
   * message number is given and the given time value matches then
   * messages with message numbers greater than the given message number
   * are returned; otherwise messages newer or equal to the specified time
   * value are returned (within a tolerance value).  Only messages whose
   * event domain and type names match the given list of domain and type
   * names will be returned (unless the given list is an empty string).
   * An XML message containing the messages is built and returned, using
   * the following format: <pre>
   * [QWresend NumRequested="10"]
   *   [QWmessage' ... '/QWmessage]
   *   [QWmessage' ... '/QWmessage]
   *   ...
   * [/QWresend]
   * </pre> (Substitute angle brackets for square brackets.)
   * The number of messages that would be required to fill the request
   * is returned in the "NumRequested" attribute.  However, no more than
   * the oldest 'maxResendNumMsgs' number of messages will be returned
   * in any single call to this method (additional calls may be needed to
   * fetch all of the desired messages).
   * @param timeVal the time-generated value for message associated with
   * the given message number, or the requested time value to be used
   * (milliseconds since 1/1/1970).
   * @param msgNum the message number to use, or 0 or none.
   * @param domainTypeListStr a list string of event domain and
   * type names in the format "domain:type,domain:type...", where
   * occurrences of the ':' and ',' characters not meant as
   * separators may be "quoted" by preceding them with the
   * backslash ('\') character and list items missing the ':'
   * character will be considered to specify only a domain name
   * (the type name will be an empty string); or an empty string
   * for none.
   * @return An XML-formatted string containing the messages, or an empty
   * string if an error occurs.
   */
  public String requestFilteredMessages(long timeVal,long msgNum,
                                                   String domainTypeListStr)
  {
    if(msgsAllowedFlag)
    {    //message access allowed for this services object
      return qwServerObj.requestMessagesStr(timeVal,msgNum,domainTypeListStr);
    }
    logObj.warning("Client call to 'requestFilteredMessages()' rejected, " +
                          "QWServices conn-status = " + shortConnStatusStr);
    return UtilFns.EMPTY_STRING;
  }

  /**
   * Requests that messages corresponding to the given time value
   * and list of feeder-data-source host-name/message-number
   * entries be returned.  An XML message containing the messages
   * is built and returned, using the following format: <pre>
   * [QWresend NumRequested="10"]
   *   [QWmessage' ... '/QWmessage]
   *   [QWmessage' ... '/QWmessage]
   *   ...
   * [/QWresend]
   * </pre> (Substitute angle brackets for square brackets.)
   * The number of messages that would be required to fill the request
   * is returned in the "NumRequested" attribute.  However, no more than
   * the oldest 'maxResendNumMsgs' number of messages will be returned
   * in any single call to this method (additional calls may be needed to
   * fetch all of the desired messages).
   * @param timeVal the time-generated value for message
   * associated with the given message number, or the requested
   * time value to be used (milliseconds since 1/1/1970).
   * @param hostMsgNumListStr a list of feeder-data-source
   * host-name/message-number entries in the form:
   * "hostName"=msgNum,...
   * @return An XML-formatted string containing the messages,
   * or an empty string if an error occurs.
   */
  public String requestSourcedMessages(long timeVal,
                                                   String hostMsgNumListStr)
  {
    if(msgsAllowedFlag)
    {    //message access allowed for this services object
      return qwServerObj.requestSourcedMsgsStr(
                                            timeVal,hostMsgNumListStr,null);
    }
    logObj.warning("Client call to 'requestSourcedMessages()' rejected, " +
                          "QWServices conn-status = " + shortConnStatusStr);
    return UtilFns.EMPTY_STRING;
  }

  /**
   * Requests that messages corresponding to the given time value
   * and list of feeder-data-source host-name/message-number
   * entries be returned.  Only messages whose event domain and
   * type names match the given list of domain and type names will
   * be returned (unless the given list is an empty string).
   * An XML message containing the messages is built and returned,
   * using the following format: <pre>
   * [QWresend NumRequested="10"]
   *   [QWmessage' ... '/QWmessage]
   *   [QWmessage' ... '/QWmessage]
   *   ...
   * [/QWresend]
   * </pre> (Substitute angle brackets for square brackets.)
   * The number of messages that would be required to fill the request
   * is returned in the "NumRequested" attribute.  However, no more than
   * the oldest 'maxResendNumMsgs' number of messages will be returned
   * in any single call to this method (additional calls may be needed to
   * fetch all of the desired messages).
   * @param timeVal the time-generated value for message
   * associated with the given message number, or the requested
   * time value to be used (milliseconds since 1/1/1970).
   * @param hostMsgNumListStr a list of feeder-data-source
   * host-name/message-number entries in the form:
   * "hostName"=msgNum,...
   * @param domainTypeListStr a list string of event domain and
   * type names in the format "domain:type,domain:type...", where
   * occurrences of the ':' and ',' characters not meant as
   * separators may be "quoted" by preceding them with the
   * backslash ('\') character and list items missing the ':'
   * character will be considered to specify only a domain name
   * (the type name will be an empty string); or an empty string
   * for none.
   * @return An XML-formatted string containing the messages,
   * or an empty string if an error occurs.
   */
  public String requestSourcedFilteredMessages(long timeVal,
                         String hostMsgNumListStr, String domainTypeListStr)
  {
    if(msgsAllowedFlag)
    {    //message access allowed for this services object
      return qwServerObj.requestSourcedMsgsStr(
                               timeVal,hostMsgNumListStr,domainTypeListStr);
    }
    logObj.warning("Client call to 'requestSourcedFilteredMessages()' " +
                "rejected, QWServices conn-status = " + shortConnStatusStr);
    return UtilFns.EMPTY_STRING;
  }

  /**
   * Determines the client-version status.  All clients will
   * call this method on a periodical basis while they are
   * connected.
   * @param clientInfoStr the client-connection-information
   * properties string to use.
   * @return true if the an updated version of the client
   * is available; false if not.
   */
  public boolean clientStatusCheck(String clientInfoStr)
  {
         //if clients-manager object OK then do client check-in:
    if(clientsManagerObj != null)
      return clientsManagerObj.performClientCheckIn(clientInfoStr);
    return false;       //clients-manager object not available
  }

  /**
   * Returns information about available client-program upgrades.
   * @param clientInfoStr the client-connection-information
   * properties string to use.
   * @return An XML-formatted string containing information about
   * available client-program upgrades, or an empty string if a
   * clients manager is not available.
   */
  public String getClientUpgradeInfo(String clientInfoStr)
  {
         //if clients-manager object OK then return upgrade info:
    if(clientsManagerObj != null)
      return clientsManagerObj.getClientUpgradeInfoStr(clientInfoStr);
              //clients-manager object not available
    logObj.debug("QWServices:  'getClientUpgradeInfo()' method called " +
                                     " with clients manager not available");
    return UtilFns.EMPTY_STRING;
  }

  /**
   * Fetches the timestamp value for the latest status report.
   * @return The timestamp value for the latest status report,
   * or 0 if no report is available.
   */
  public long getStatusReportTime()
  {
    if(msgsAllowedFlag)
    {    //message access allowed for this services object
      return qwServerObj.getStatusReportTime();
    }
    logObj.warning("Client call to 'getStatusReportTime()' rejected, " +
                          "QWServices conn-status = " + shortConnStatusStr);
    return 0;
  }

  /**
   * Fetches the latest status-report data.
   * @return A string containing the latest status-report data, or
   * an empty string if no report is available.
   */
  public String getStatusReportData()
  {
    if(msgsAllowedFlag)
    {    //message access allowed for this services object
      return qwServerObj.getStatusReportData();
    }
    logObj.warning("Client call to 'getStatusReportData()' rejected, " +
                          "QWServices conn-status = " + shortConnStatusStr);
    return UtilFns.EMPTY_STRING;
  }

  /**
   * Fetches a byte-array of certificate-file data.
   * @return A byte-array of certificate-file data, or a
   * zero-length array if no data is available.
   */
  public byte [] getCertificateFileData()
  {
    if(msgsAllowedFlag)
    {    //message access allowed for this services object
      return qwServerObj.getCertificateFileData();
    }
    logObj.warning("Client call to 'getCertificateFileData()' rejected, " +
                          "QWServices conn-status = " + shortConnStatusStr);
    return QWServer.ZEROLEN_BYTE_ARRAY;
  }

  /**
   * Disconnects the client connection.
   * @param clientInfoStr the client-connection-information
   * properties string for the client.
   */
  public void disconnectClient(String clientInfoStr)
  {
    if(clientsManagerObj != null)
    {    //clients-manager object OK; do removal
      final Object remObj;
      if((remObj=clientsManagerObj.removeClient(clientInfoStr)) != null)
      {  //client-info object removed OK; log it
        final String msgStr = "Client connection terminated by client, " +
                                                                     remObj;
        logObj.debug2("QWServicesImpl:  " + msgStr);
                                  //output to clients-info log file:
        qwServerObj.writeToClientsInfoLog(msgStr);
      }
      else
      {  //matching client-info object not found; log warning
        logObj.debug("Unknown client attempted to disconnect, props:  " +
                                                             clientInfoStr);
      }
    }
  }


  /**
   * Returns the "short" connection-status description string for this
   * services object.
   * @return The "short" connection-status description string for this
   * services object.
   */
  public String getShortConnStatusStr()
  {
    return shortConnStatusStr;
  }
}
