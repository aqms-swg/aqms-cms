//NotifSvrStatusChecker.java:  Helper class for checking Notification
//                             Server status via its status-log file.
//
//   2/5/2006 -- [ET]  Initial version.
//   2/5/2008 -- [ET]  Modified to use 'QWReportStrings' fields.
//

package com.isti.quakewatch.server;

import java.util.Date;
import java.util.Calendar;
import java.text.DateFormat;
import java.io.File;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import com.isti.util.UtilFns;
import com.isti.util.FileUtils;
import com.isti.util.LogFile;
import com.isti.openorbutil.NotifServerTags;
import com.isti.quakewatch.common.QWReportStrings;

/**
 * Class NotifSvrStatusChecker is a helper class for checking Notification
 * Server status via its status-log file.
 */
public class NotifSvrStatusChecker
{
  protected final String notifSvrStatusLogName;
  protected final int inactiveTimeLimitMS;
  protected final boolean notifSvrStatusWarnFlag;
  protected final int memorySizeWarnMBytes;
  protected final int threadCountWarnMargin;
  protected final LogFile logObj;
  protected long nextDateCodeTime = 0;
  protected String currentStatusLogNameStr = null;
  protected final DateFormat fileDateFormatterObj =
                  UtilFns.createDateFormatObj(LogFile.FILE_DATE_PATTERNSTR);
  protected final DateFormat shortDateFormatterObj =
                 UtilFns.createDateFormatObj(LogFile.SHORT_DATE_PATTERNSTR);
  protected long notifStatusMarkTime = 0;
  protected final StatusInfoBlock statusInfoBlockObj = new StatusInfoBlock();

  /**
   * Creates a helper object for checking Notification Server status via
   * its status-log file.
   * @param notifSvrStatusLogName base filename for Notification Server
   * status log file.
   * @param inactiveTimeLimitMS limit time (in milliseconds) for status
   * output to be considered inactive.
   * @param notifSvrStatusWarnFlag true to log a warning messages if no
   * status entries are received within the 'inactiveTimeLimitMS' limit;
   * false to log debug messages.
   * @param memorySizeWarnMBytes memory-size warning value, in megabytes,
   * or 0 for none.
   * @param threadCountWarnMargin margin value for thread-count warning,
   * or 0 for none.
   * @param logObj LogFile object to use.
   */
  public NotifSvrStatusChecker(String notifSvrStatusLogName,
                    int inactiveTimeLimitMS, boolean notifSvrStatusWarnFlag,
                        int memorySizeWarnMBytes, int threadCountWarnMargin,
                                                             LogFile logObj)
  {
    this.notifSvrStatusLogName = notifSvrStatusLogName;
    this.inactiveTimeLimitMS = inactiveTimeLimitMS;
    this.notifSvrStatusWarnFlag = notifSvrStatusWarnFlag;
    this.memorySizeWarnMBytes = memorySizeWarnMBytes;
    this.threadCountWarnMargin = threadCountWarnMargin;
    this.logObj = logObj;
  }

  /**
   * Checks the current memory usage and thread count in the
   * Notification Server status log and generates warning-log
   * messages if either is excessive.
   * @return true if a warning message was logged; false if not.
   */
  public boolean checkWarnStatusLog()
  {
    boolean warnFlag = false;          //set true if warning msgs logged
         //load status-info block from notif-status log file:
    if(loadStatusInfoBlock())
    {    //load of status-info block succeeded
              //check memory and thread values; set warning-issued flag
      warnFlag = statusInfoBlockObj.checkWarnMemThreadValues();
    }
    else
    {    //load of status-info block failed
      final long currentTimeVal = System.currentTimeMillis();
      if(notifStatusMarkTime > 0)
      {  //this is not the first time through
        if(currentTimeVal - notifStatusMarkTime >= inactiveTimeLimitMS)
        {     //enough time has past to declare inactive
          if(statusInfoBlockObj.isDataEntered())
          {   //previous status values were received OK
            logObj.warning(
                "New entries from Notification Server status log file (\"" +
                               currentStatusLogNameStr + "\" missing for " +
                           ((currentTimeVal - notifStatusMarkTime) / 1000) +
                                                                " seconds");
            warnFlag = true;                //indicate warning was logged
          }
          else
          {   //no status values have been received
            logObj.println(       // (if flag then log at 'warning' level)
                 (notifSvrStatusWarnFlag ? LogFile.WARNING : LogFile.DEBUG),
                           "NotifSvrStatusChecker:  No entries read from " +
                                 "Notification Server status log file (\"" +
                                     currentStatusLogNameStr + "\" after " +
                           ((currentTimeVal - notifStatusMarkTime) / 1000) +
                                                                " seconds");
            if(notifSvrStatusWarnFlag)      //if flag then
              warnFlag = true;              //indicate warning was logged
          }
        }
      }
      else    //this is the first time through
        notifStatusMarkTime = currentTimeVal;    //mark current time
    }
    return warnFlag;
  }

  /**
   * Outputs the latest-recevied values from the Notification Server
   * status log to the given print stream.
   * @param indentStr indentation string to use, or null for none.
   * @param outStm PrintStream to send output to.
   */
  public void outputStatusLogValues(String indentStr, PrintStream outStm)
  {
         //if status-info block has never been loaded or is too old then
         // do load of status-info block from notif status log now:
    if((statusInfoBlockObj.isDataEntered() && System.currentTimeMillis() -
                                  statusInfoBlockObj.getDateCodeTimeVal() <=
                                                   inactiveTimeLimitMS/2) ||
                                                      loadStatusInfoBlock())
    {    //status-info block values are OK; output them
      statusInfoBlockObj.outValuesInfo(indentStr,outStm);
    }
  }

  /**
   * Returns the current status log name (including the appended date
   * code).
   * @param currentTimeVal current time value.
   * @return The current status log name.
   */
  protected String getCurrentStatusLogName(long currentTimeVal)
  {
    if(currentStatusLogNameStr == null || currentTimeVal >= nextDateCodeTime)
    {    //first time through or enough time elapsed for switch
      final Date currentDateObj = new Date(currentTimeVal);
                             //get calendar object with current time:
      final Calendar calObj = Calendar.getInstance();
      calObj.setTime(currentDateObj);
              //set time to start of current day so next date code
              // will start at 00:00 time:
      calObj.set(Calendar.HOUR_OF_DAY,0);
      calObj.set(Calendar.MINUTE,0);
      calObj.set(Calendar.SECOND,0);
      calObj.set(Calendar.MILLISECOND,0);
      calObj.add(Calendar.DATE,1);          //move to next day
              //setup time for next day at 00:00:
      nextDateCodeTime = calObj.getTime().getTime();
              //insert separator and date code into filename:
      currentStatusLogNameStr = FileUtils.addStrToFileName(
                         notifSvrStatusLogName, (LogFile.FNAME_DATE_SEPSTR +
                              fileDateFormatterObj.format(currentDateObj)));

    }
    return currentStatusLogNameStr;
  }

  /**
   * Parses an integer value that follows the given tag string.
   * @param dataStr source data string.
   * @param tagStr tag string.
   * @param tag2Str secondary tag string, or null for none.
   * @return Long object holding parsed value, or null if unable
   * to find or parse value.
   */
  protected Long parseTaggedLongVal(String dataStr, String tagStr,
                                                             String tag2Str)
  {
    final int dataStrLen = dataStr.length();
    String errMsgStr;
    int sPos,ePos;
    if((sPos=dataStr.indexOf(tagStr)) >= 0)
    {    //tag string found
      if((sPos+=tagStr.length()) < dataStrLen-1)
      {  //position after tag string OK
        if(tag2Str != null)
        {     //secondary tag string was given
          if((sPos=dataStr.indexOf(tag2Str,sPos)) >= 0)
          {   //secondary tag string found; setup position after tag
            if((sPos+=tag2Str.length()) >= dataStrLen-1)
              sPos = -1;     //if no more data then indicate value not found
          }
        }
      }
      else    //no more data
        sPos = -1;      //indicate value not found
    }
    if(sPos > 0)
    {    //tag string(s) found; scan through any spaces before value:
      while(Character.isWhitespace(dataStr.charAt(sPos)) &&
                                                     ++sPos < dataStrLen-1);
      ePos = sPos;           //scan through numeric digits:
      while(Character.isDigit(dataStr.charAt(ePos)) &&
                                                       ++ePos < dataStrLen);
      try
      {       //parse to 'Long' object and return it:
        return Long.valueOf(dataStr.substring(sPos,ePos));
      }
      catch(NumberFormatException ex)
      {       //error parsing value; do error handling below
      }
      errMsgStr = "Error parsing";          //setup error message
    }
    else      //tag string(s) not found
      errMsgStr = "Unable to find";         //setup error message
    ePos = tagStr.length();
    char ch;       //scan back through spaces or separators in tag string:
    while(ePos > 0 && (Character.isWhitespace(ch=tagStr.charAt(ePos-1)) ||
                                                    ch == '=' || ch == ':'))
    {
      --ePos;
    }
    logObj.debug("NotifSvrStatusChecker:  " + errMsgStr + '\'' +
                                                  tagStr.substring(0,ePos) +
                     "\' value in Notification Server status log file (\"" +
                                           currentStatusLogNameStr + "\")");
    return null;
  }

  /**
   * Parses an integer value that follows the given tag string.
   * @param dataStr source data string.
   * @param tagStr tag string.
   * @return Long object holding parsed value, or null if unable
   * to find or parse value.
   */
  protected Long parseTaggedLongVal(String dataStr, String tagStr)
  {
    return parseTaggedLongVal(dataStr,tagStr,null);
  }

  /**
   * Loads the status-info block with current memory usage and thread
   * count values from the Notification Server status log.
   * @return true if successful; false if not.
   */
  protected boolean loadStatusInfoBlock()
  {
    boolean statValuesFlag = false;    //set true if status received OK
    final long currentTimeVal = System.currentTimeMillis();
    final String statLogName = getCurrentStatusLogName(currentTimeVal);
    if((new File(statLogName)).exists())
    {    //NotifServer status log file exists
      final ByteArrayOutputStream outStm = new ByteArrayOutputStream();
      boolean readFileFlag = false;
      try
      {       //read last 20 lines of notif status log file:
        FileUtils.readFileTail(statLogName,20,outStm);
        readFileFlag = true;      //indicate file read OK
      }
      catch(Exception ex)
      {       //some kind of exception error; log it
        logObj.debug("NotifSvrStatusChecker:  Error reading " +
                                 "Notification Server status log file (\"" +
                                               statLogName + "\"):  " + ex);
      }
      if(outStm.size() > 0)
      {  //data was read OK from tail of file
        final String fileTailDataStr = outStm.toString();
        int hdrPos,ftrPos;
        if((hdrPos=fileTailDataStr.lastIndexOf(
                               NotifServerTags.STATUSLOG_HEADER_STR)) > 0 &&
                               (ftrPos=fileTailDataStr.indexOf("----------",
                 hdrPos+NotifServerTags.STATUSLOG_HEADER_STR.length())) > 0)
        {     //header and trailer for status output found OK
                   //find timestamp at beginning of line:
          int ePos = hdrPos;
          while(ePos > 0 &&       //find digit at end of timestamp:
                        !Character.isDigit(fileTailDataStr.charAt(--ePos)));
          int sPos = ePos;        //find line-end before timestamp:
          while(sPos > 0 && fileTailDataStr.charAt(--sPos) >= ' ');
          String dateCodeStr = UtilFns.EMPTY_STRING;
          Date dateObj = null;
          long dateCodeTimeVal = 0;
          try
          {      //parse date code:
            dateObj = shortDateFormatterObj.parse(dateCodeStr=
                                  fileTailDataStr.substring(sPos+1,ePos+1));
            dateCodeTimeVal = dateObj.getTime();      //get time value
          }
          catch(Exception ex)
          {     //some kind of exception error; log it
            logObj.debug("NotifSvrStatusChecker:  " +
                                             "Error parsing timestamp (\"" +
                                  fileTailDataStr.substring(sPos+1,ePos+1) +
                        "\") from Notification Server status log file (\"" +
                                               statLogName + "\"):  " + ex);
          }
          if(dateObj != null && dateCodeTimeVal > notifStatusMarkTime &&
                     currentTimeVal - dateCodeTimeVal < inactiveTimeLimitMS)
          { //timestamp parsed OK, is newer than previous and is not too old
            int threadCount = -1, connCount = -1;
            long diffMemory = -1;
                        //setup substring with status entry data:
            final String entryStr = fileTailDataStr.substring(
                       hdrPos+NotifServerTags.STATUSLOG_HEADER_STR.length(),
                                                             ftrPos).trim();
            Long longObj;
            if((longObj=parseTaggedLongVal(entryStr,
                          NotifServerTags.STATUSLOG_CONNCOUNT_STR)) != null)
            {      //conn-count value parsed OK; save value
              connCount = longObj.intValue();
              statValuesFlag = true;        //indicate status values found
                                            //log debug info:
              logObj.debug3("NotifSvrStatusChecker:  Parsed " +
                                          "conn-count value (" + connCount +
                          ") from Notification Server status log file (\"" +
                                                        statLogName + "\"");
            }
            if((longObj=parseTaggedLongVal(entryStr,
                        NotifServerTags.STATUSLOG_THREADCOUNT_STR)) != null)
            {      //thread-count value parsed OK; save value
              threadCount = longObj.intValue();
              statValuesFlag = true;        //indicate status values found
                                            //log debug info:
              logObj.debug3("NotifSvrStatusChecker:  Parsed " +
                                      "thread-count value (" + threadCount +
                          ") from Notification Server status log file (\"" +
                                                        statLogName + "\"");
            }
            if((longObj=parseTaggedLongVal(entryStr,
                     NotifServerTags.STATUSLOG_MEMORY_STR,"diff=")) != null)
            {      //diff-memory value parsed OK; save value
              diffMemory = longObj.longValue();
              statValuesFlag = true;        //indicate status values found
                                            //log debug info:
              logObj.debug3("NotifSvrStatusChecker:  Parsed " +
                                       "memory-usage value (" + diffMemory +
                          ") from Notification Server status log file (\"" +
                                                        statLogName + "\"");
            }
            if(statValuesFlag)
            { //status values were found
                        //mark time associated with latest status values:
              notifStatusMarkTime = dateCodeTimeVal;
                        //enter status "snapshot" into info block:
              statusInfoBlockObj.setValues(dateCodeStr,connCount,
                                    threadCount,diffMemory,dateCodeTimeVal);
              return true;        //indicate success
            }
          }
          else
          {   //timestamp bad, not newer than previous or too old
            logObj.debug3("NotifSvrStatusChecker:  Entry in Notification " +
                                "Server status log file (\"" + statLogName +
                                       "\" not used because timestamp (\"" +
                                  fileTailDataStr.substring(sPos+1,ePos+1) +
                                           "\") is bad, not newer, or old");
          }
        }
        else
        {     //header and trailer for status output not found
          logObj.debug("NotifSvrStatusChecker:  Unable to find " +
                    "header and trailer for entry in Notification Server " +
                                "status log file (\"" + statLogName + "\"");
        }
      }
      else if(readFileFlag)
      {  //data was read OK but file is empty; log debug message
        logObj.debug("NotifSvrStatusChecker:  No data found in " +
            "Notification Server status log file (\"" + statLogName + "\"");
      }
    }
    else
    {    //NotifServer status log file does not exist; log debug message
      logObj.debug3("NotifSvrStatusChecker:  Notification Server status " +
                             "log file (\"" + statLogName + "\" not found");
    }
    return false;            //indicate failure
  }


  /**
   * Class StatusInfoBlock defines a "snapshot" of Notification Server
   * status information.
   */
  protected class StatusInfoBlock
  {
    protected String dateCodeStr = UtilFns.EMPTY_STRING;
    protected int connCount = -1;
    protected int threadCount = -1;
    protected long diffMemory = -1;
    protected long dateCodeTimeVal = 0;
    protected boolean dataEnteredFlag = false;

    /**
     * Sets the status-information values.
     * @param dateCodeStr date-code string associated with values.
     * @param connCount connections-count value.
     * @param threadCount thread-count value.
     * @param diffMemory memory-in-use value.
     * @param dateCodeTimeVal date-code time associated with values.
     */
    public synchronized void setValues(String dateCodeStr,
                            int connCount, int threadCount, long diffMemory,
                                                       long dateCodeTimeVal)
    {
      this.dateCodeStr = dateCodeStr;
      this.connCount = connCount;
      this.threadCount = threadCount;
      this.diffMemory = diffMemory;
      this.dateCodeTimeVal = dateCodeTimeVal;
      dataEnteredFlag = true;
    }

    /**
     * Determines if data has been entered via the 'setValues()' method.
     * @return true if data has been entered via the 'setValues()' method;
     * false if not.
     */
    public synchronized boolean isDataEntered()
    {
      return dataEnteredFlag;
    }

    /**
     * Checks the current memory usage and thread count values and
     * generates warning-log messages if either is excessive.
     * @return true if a warning message was logged; false if not.
     */
    public synchronized boolean checkWarnMemThreadValues()
    {
      boolean warnFlag = false;        //set true if warning msgs logged
      if(dataEnteredFlag)
      {  //data values have been entered
        if(connCount > 0)
        {    //conn-count value OK
          final int threadLimit;
          if(threadCountWarnMargin > 0 && threadCount >
                                                  (threadLimit=connCount*2 +
                                                21 + threadCountWarnMargin))
          {   //thread-count-warn value set and thread count over value
            logObj.warning("Excessive NotifServer thread count " +
                   "detected (limit=" + threadLimit + "):  " + threadCount);
            warnFlag = true;          //indicate warning was logged
          }
        }
        if(memorySizeWarnMBytes > 0 && diffMemory >=
                                   (long)memorySizeWarnMBytes * (1024*1024))
        {     //mem-size-warn value set and memory in use over value
          logObj.warning("Excessive NotifServer memory usage " +
                                 "detected (limit=" + memorySizeWarnMBytes +
                                                     "MB):  " + diffMemory);
          warnFlag = true;             //indicate warning was logged
        }
        if(warnFlag)
        {    //warning was logged; show # notif conns
          logObj.debug("NotifSvrStatusChecker:  # of Notif conns = " +
                                                                 connCount);
        }
        else
        {    //no warnings; log debug message
          logObj.debug2("NotifSvrStatusChecker:  NotifServer thread " +
                      "count (" + threadCount + ") and memory usage (" +
                                                   diffMemory + ") OK");
        }
      }
      return warnFlag;
    }

    /**
     * Outputs the status-information values to the given print stream.
     * @param indentStr indentation string to use, or null for none.
     * @param outStm PrintStream to send output to.
     */
    public synchronized void outValuesInfo(
                                       String indentStr, PrintStream outStm)
    {
      if(indentStr == null)
        indentStr = UtilFns.EMPTY_STRING;
      outStm.println(indentStr + QWReportStrings.REP_NOTIFSTAT_STR +
                                                         dateCodeStr + ':');
      outStm.println(indentStr + "  ConnCount=" + connCount +
                 ", ThreadCount=" + threadCount + ", Memory=" + diffMemory);
    }

    /**
     * Returns the date-code time associated with the current values.
     * @return The date-code time associated with the current values
     * (ms since 1/1/1970).
     */
    public synchronized long getDateCodeTimeVal()
    {
      return dateCodeTimeVal;
    }
  }
}
