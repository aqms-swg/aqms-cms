//QWCorbaOutputter.java:  QuakeWatch outputter plugin module that sends
//                        messages via CORBA method calls.
//
//  7/14/2008 -- [ET]  Modified from 'QWCorbaFeeder' class.
// 10/20/2008 -- [ET]  Added 'subscribeAllDefaultFlag' configuration
//                     parameter and changed default behavior to
//                     deliver no messages if no domain/channels are
//                     subscribed to.
//   7/2/2009 -- [ET]  Modified to use QWServer's "serverHostAddress"
//                     entry for outputter's CORBA ORB.
//

package com.isti.quakewatch.server.outputter;

import com.isti.openorbutil.OrbManager;
import com.isti.util.LogFile;
import com.isti.util.CfgProperties;
import com.isti.util.CfgPropItem;
import com.isti.quakewatch.server.QWServerMsgRecord;

/**
 * Class QWCorbaOutputter is a QuakeWatch outputter plugin module that sends
 * messages via CORBA method calls.  The outputter's CORBA servant is
 * accessed through a Corbaloc address like this one:
 * "corbaloc:iiop:1.2@hostAddr:portNum/QWSubscription", where "hostAddr" is
 * the QWServer's host address and "portNum" is the "outputPortNumber"
 * configuration parameter value.
 */
public class QWCorbaOutputter extends QWAbstractOutputter
{
  public static final String MODULE_NAME = "QWCorbaOutputter";
  public static final String VERSION = "1.1";    //name and version strings
  public static final String REVISION_STRING = MODULE_NAME + ", " +
                                                       "Version " + VERSION;
  public static final String OUTPUTTER_NAME = "QWCorba";    //outputter name
  public static final int DEF_PORT_NUM = 36688;  //default outputter port #
                        //reference name for the Corbaloc registration:
  public static final String CORBALOC_REF_NAME = "QWSubscription";
  protected OrbManager orbManagerObj = null;     //ORB manager object
  protected SubscriberManager subscriberManagerObj = null;
                        //define config-prop settings for outputter:
  protected final OutputterSettings outputterSettingsObj =
                                                    new OutputterSettings();

  /**
   * Constructs a QWCorba outputter module.
   * @param moduleName name of module.
   * @param outputterName name of outputter (from config file).
   */
  public QWCorbaOutputter(String moduleName, String outputterName)
  {
    super(moduleName);                      //set thread name to module name
    this.outputterName = outputterName;     //set outputter name
  }

  /**
   * Constructs a QWCorba outputter module.
   */
  public QWCorbaOutputter()
  {
    this(MODULE_NAME,OUTPUTTER_NAME);       //set thread and outputter name
  }

  /**
   * Returns the revision String for the outputter.
   * @return the revision String for the outputter.
   */
  public String getRevisionString()
  {
    return REVISION_STRING;
  }

  /**
   * Processes outputter-specific settings.  The settings are usually
   * fetched from the "QWOutputterSettings" element in the QuakeWatch-server
   * configuration file.  (For 'QWOutputterPlugin' interface.)
   * @param settingsStr The settings string to be processed.
   * @throws QWOutputterInitException if an error occurs while processing
   * the settings.
   */
  public void processOutputterSettings(String settingsStr)
                                                throws QWOutputterInitException
  {
    String msgStr;
    try
    {         //load configuration properties from the given string:
      if(outputterSettingsObj.cfgPropsObj.load(settingsStr))
        return;              //if successful then exit method
         //error loading settings; get error message
      msgStr = outputterSettingsObj.cfgPropsObj.getErrorMessage();
    }
    catch(Exception ex)
    {    //some kind of exception error; build error message
      msgStr = "Error processing settings:  " + ex;
    }
         //throw outputter exception with error message:
    throw new QWOutputterInitException(msgStr);
  }

  /**
   * Stops the message collection threads of the outputter.  The work
   * is done via a separate thread.  (For 'QWOutputterPlugin' interface.)
   */
  public void stopOutputter()
  {
    (new Thread("stop-" + getOutputterName())
        {          //create separate thread to do "stop" work
          public void run()
          {
            try
            {
              if(logObj != null)
                logObj.debug2("QWCorbaOutputter stop requested");
                   //if manager setup then terminate subscriber threads:
              if(subscriberManagerObj != null)
                subscriberManagerObj.terminateSubscribers();
              if(orbManagerObj != null)
              {  //ORB manager was initialized
                orbManagerObj.closeImplementation(false);  //stop ORB
                if(orbManagerObj.waitForImplFinished(250)) //wait for done
                {
                  if(logObj != null)
                    logObj.debug("ORB implementation closed OK");
                }
                else
                {
                  if(logObj != null)
                    logObj.debug("Unable to close ORB implementation");
                }
              }
              QWCorbaOutputter.super.stopOutputter(); //call parent method
            }                                         // (closes log file)
            catch(Exception ex)
            {  //some kind of exception error; log it
              if(logObj != null)
                logObj.warning("Error stopping QWCorbaOutputter:  " + ex);
            }
          }
        }).start();          //start thread
  }

  /**
   * Executing method for the outputter thread.  Starts execution of the
   * CORBA-servant threads.
   */
  public void run()
  {
    if(qwServerObj == null)
    {  //handle to QWServer not setup
      logObj.warning("Error starting QWCorbaOutputter:  Handle to parent " +
                                                        "QWServer is null");
      return;
    }
    if(logObj == null)  //if no log file setup then create dummy object
      logObj = new LogFile(null,LogFile.NO_MSGS,LogFile.ERROR);
    try
    {
      final int outputterPortNum;
      if((outputterPortNum=
                 outputterSettingsObj.outputPortNumberProp.intValue()) <= 0)
      {       //bad port number
        logObj.warning("Invalid QWCorbaOutputter port number:  " +
                                                          outputterPortNum);
        return;
      }
              //create ORB manager object (use QWServer's host address):
      orbManagerObj = new OrbManager(
                  null,qwServerObj.getServerHostAddress(),outputterPortNum);
              //initialize ORB (with 'Corbaloc' service support):
      if(!orbManagerObj.initImplementation(true))
      {       //error initializing; log error message
        logObj.warning("QWCorbaOutputter error initializing CORBA " +
                     "implementation:  " + orbManagerObj.getErrorMessage());
        orbManagerObj = null;     //clear handle to ORB manager
        return;
      }
      logObj.debug2("Using outputter port number:  " + outputterPortNum);
      logObj.debug2("Successfully initialized CORBA ORB for outputter");

         //create subscriber manager:
      subscriberManagerObj = new SubscriberManager(
                                   qwServerObj,outputterSettingsObj,logObj);

         //create QWSubscription object and register it with Corbaloc:
      QWSubscriptionImpl qwSubscriptionObj = null;
      try
      {       //create QWSubscription implementation object:
        qwSubscriptionObj = new QWSubscriptionImpl(subscriberManagerObj);
      }
      catch(Exception ex)
      {
        logObj.warning("Error creating QWSubscriptionImpl object:  " + ex);
      }
      if(qwSubscriptionObj != null)
      {  //QWSubscription implementation object created OK
                   //register QWSubscription object with Corbaloc:
        if(orbManagerObj.registerCorbalocObject(CORBALOC_REF_NAME,
                                                         qwSubscriptionObj))
        {
          logObj.info("Registered 'QWSubscription' object with " +
                          "'Corbaloc' service at port " + outputterPortNum);
        }
        else
        {
          logObj.warning("QWCorbaOutputter error registering " +
              "QWSubscription object:  " + orbManagerObj.getErrorMessage());
        }
      }
      logObj.info("QWCorbaOutputter successfully started");

         //run CORBA implementation:
      if(!orbManagerObj.runImplementation())
      {  //error running implementation
        logObj.warning("Error running CORBA implentation:  " +
                                           orbManagerObj.getErrorMessage());
        return;
      }
      logObj.debug2("Returned from CORBA implementation run method");
    }
    catch(Exception ex)
    {         //some kind of exception error; log message
      logObj.warning("Error starting QWCorbaOutputter:  " + ex);
    }
  }

  /**
   * Delivers the given message to the outputter.
   * @param qwSvrMsgRecObj server-message-record object for message.
   */
  public void outputMessage(QWServerMsgRecord qwSvrMsgRecObj)
  {
    if(subscriberManagerObj != null)
    {  //subscriber manager OK; pass message to manager
      subscriberManagerObj.outputMessage(qwSvrMsgRecObj);
    }
  }

  /**
   * Returns the current estimated thread count for the outputter.
   * @return The current estimated thread count for the outputter.
   */
  public int getEstimatedThreadCount()
  {
    if(subscriberManagerObj != null)
    {  //subscriber manager OK
                   //estimate thread count based on number of subscribers
                   // and +1 if 'Timer' thread started:
      return subscriberManagerObj.getSubscribersCount() * 4 +
              (subscriberManagerObj.isTimerThreadStartedFlag() ? 1 : 0) + 2;
    }
    return 1;      //subscriber manager not available
  }

  /**
   * Returns a status message for the outputter.
   * @return Status message string for the outputter, or null if none.
   */
  public String getStatusMessageStr()
  {
    return "Number of subscribers = " + ((subscriberManagerObj != null) ?
              Integer.toString(subscriberManagerObj.getSubscribersCount()) :
                                                                     "???");
  }


  /**
   * Class OutputterSettings defines the configuration-property settings
   * for the 'QWCorbaOutputter' class.
   */
  public static class OutputterSettings
  {
         //configuration properties for outputter:
    public final CfgProperties cfgPropsObj = new CfgProperties();
              //port number for this outputter to listen on:
    public final CfgPropItem outputPortNumberProp =
             cfgPropsObj.add("outputPortNumber", Integer.valueOf(DEF_PORT_NUM));
              //maximum age for delivered messages (hours, 0.0 = no limit):
    public final CfgPropItem maxMessageAgeHoursProp =
                    cfgPropsObj.add("maxMessageAgeHours", Double.valueOf(24.0));
              //maximum number of queued messages (0 = no limit):
    public final CfgPropItem maxMsgQueueCountProp =
                     cfgPropsObj.add("maxMsgQueueCount", Integer.valueOf(1000));
              //seconds before retry after delivery failure to subscriber:
    public final CfgPropItem retryAfterFailDelaySecsProp =
                cfgPropsObj.add("retryAfterFailDelaySecs", Integer.valueOf(20));
              //max retry time before disconnect (minutes, 0.0 = no limit):
    public final CfgPropItem maximumRetryMinutesProp =
                   cfgPropsObj.add("maximumRetryMinutes", Double.valueOf(10.0));
              //unpack message from 'QWmessage' element:
    public final CfgPropItem unpackQWMessageFlagProp = cfgPropsObj.add(
                                    "unpackQWMessageFlag",Boolean.TRUE,null,
                                 "Unpack message from 'QWmessage' element");
              //unpack message from 'DataMessage' element:
    public final CfgPropItem unpackDataMessageFlagProp = cfgPropsObj.add(
                                  "unpackDataMessageFlag",Boolean.TRUE,null,
                               "Unpack message from 'DataMessage' element");
              //unpack message from 'EQMessage' element:
    public final CfgPropItem unpackEQMessageFlagProp = cfgPropsObj.add(
                                   "unpackEQMessageFlag",Boolean.FALSE,null,
                                 "Unpack message from 'EQMessage' element");
              //subscribe to all channels if none subscribed to:
    public final CfgPropItem subscribeAllDefaultFlagProp = cfgPropsObj.add(
                               "subscribeAllDefaultFlag",Boolean.FALSE,null,
                                  "Subscribe to all if none subscribed to");
              //directory for subscriber message-tracking files:
    public final CfgPropItem trackingDataDirProp =
                         cfgPropsObj.add("trackingDataDir","outputterdata");
  }
}
