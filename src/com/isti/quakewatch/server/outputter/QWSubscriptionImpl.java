//QWSubscriptionImpl.java:  Implements the 'QWSubscription.idl' methods
//                          that are used by subscribers to connect to
//                          the outputter.
//
//   6/6/2008 -- [ET]  Initial version.
// 10/20/2008 -- [ET]  Changed comment on 'connectPushSubscriber()'
//                     method about behavior when no domain/channels
//                     are subscribed to.
//

package com.isti.quakewatch.server.outputter;

import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlk;
import com.isti.quakewatch.corbaclient.qw_client_rec.QWSubscriptionPOA;
import com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesHolder;


/**
 * Class QWSubscriptionImpl implements the 'QWSubscription.idl' methods
 * that are used by subscribers to connect to the outputter.
 */
public class QWSubscriptionImpl extends QWSubscriptionPOA
{
  protected final SubscriberManager subscriberManagerObj;
  protected final LogFile logObj;

  /**
   * Creates a 'QWSubscription' implementation.
   * @param subscriberManagerObj subscriber-manager object to use.
   */
  public QWSubscriptionImpl(SubscriberManager subscriberManagerObj)
  {
    this.subscriberManagerObj = subscriberManagerObj;
    logObj = subscriberManagerObj.getLogFileObj();
  }

  /**
   * Connects a "push" subscriber to a QuakeWatch CORBA outputter.
   * @param subscriberNameStr the name of the subscriber.
   * @param domainChanBlkArr the array of domain/channel pairs to
   * be subscribed to.
   * @param subServicesHolder the subscriber services object
   * to use for interacting with the CORBA outputter.
   * @return An empty string if successful; an error message if
   * not.
   */
  public String connectPushSubscriberTo(String subscriberNameStr,
                                          DomainChanBlk [] domainChanBlkArr,
                                  QWPushSubServicesHolder subServicesHolder)
  {
    try
    {         //add subscriber to manager and get generated object:
      final CorbaOutSubscriber outSubObj =
                       subscriberManagerObj.addSubscriber(subscriberNameStr,
                                                          domainChanBlkArr);
              //associate subscriber object with ORB and enter into holder:
      subServicesHolder.value = outSubObj._this(_orb());
    }
    catch(Exception ex)
    {  //some kind of exception error; log and return warning message
      logObj.warning(
                   "QWCorbaOutputter QWSubscription connect error:  " + ex);
      return "Connect error:  " + ex;
    }
    return UtilFns.EMPTY_STRING;            //return OK string
  }

  /**
   * Connects a "push" subscriber to a QuakeWatch CORBA outputter.
   * Initially sets up no subscriptions, resulting no messages
   * being delivered if 'subscribeAllDefaultFlag'==false' or all
   * messages being delivered if 'subscribeAllDefaultFlag'==true.
   * @param subscriberNameStr the name of the subscriber.
   * @param subServicesHolder the subscriber services object
   * to use for interacting with the CORBA outputter.
   * @return An empty string if successful; an error message if
   * not.
   */
  public String connectPushSubscriber(String subscriberNameStr,
                                  QWPushSubServicesHolder subServicesHolder)
  {
    return connectPushSubscriberTo(subscriberNameStr,null,subServicesHolder);
  }
}
