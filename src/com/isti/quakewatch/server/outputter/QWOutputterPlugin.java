//QWOutputterPlugin.java:  Defines the methods used by QuakeWatch
//                         "outputter" modules to configure how they
//                         communicate with the server.
//
//  7/10/2008 -- [ET]  Modified from 'QWFeederPlugin' interface.
//

package com.isti.quakewatch.server.outputter;

import com.isti.util.LogFile;
import com.isti.quakewatch.server.QWServer;
import com.isti.quakewatch.server.QWServerMsgRecord;

/**
 * Interface QWOutputterPlugin defines the method used by QuakeWatch
 * "outputter" modules to configure how they communicate with the server.
 * All "outputter" modules will implement this inteface.
 */
public interface QWOutputterPlugin
{
  /**
   * Returns the revision String for the outputter.
   * @return the revision String for the outputter.
   */
  public String getRevisionString();

  /**
   * Sets the name for the outputter.  The name is usually loaded from the
   * QuakeWatch-server configuration file.
   * @param str the name to use.
   */
  public void setOutputterName(String str);

  /**
   * Returns the name for the outputter.  The name is usually loaded from
   * the QuakeWatch-server configuration file.
   * @return The outputter name.
   */
  public String getOutputterName();

  /**
   * Sets up the log file to be used with the outputter.
   * @param logNameStr name of log file, or null for none.
   * @param logFileLevel message level for log file output.
   * @param consoleLevel message level for console output.
   * @param useDateInFnameFlag true to use the date in the file name (and
   * create a new log file everytime the date changes).
   * @param logFileSwitchIntervalDays the minimum number of days between
   * log file switches via date changes.
   * @param maxLogFileAgeDays the maximum log file age, in days (if
   * 'useDateInFnameFlag' is true).
   */
  public void setupLogFile(String logNameStr,int logFileLevel,
                                int consoleLevel,boolean useDateInFnameFlag,
                       int logFileSwitchIntervalDays,int maxLogFileAgeDays);

  /**
   * Returns the log file object for the outputter.
   * @return The 'LogFile' object for the outputter.
   */
  public LogFile getLogFileObj();

  /**
   * Closes the log file for the outputter.
   */
  public void closeLogFile();

  /**
   * Processes outputter-specific settings.  The settings are usually
   * fetched from the "QWOutputterSettings" element in the QuakeWatch-server
   * configuration file.
   * @param settingsStr The settings string to be processed.
   * @throws QWOutputterInitException if an error occurs while processing
   * the settings.
   */
  public void processOutputterSettings(String settingsStr)
                                            throws QWOutputterInitException;

  /**
   * Enters the QuakeWatch server object to use.
   * @param qwServerObj the QuakeWatch server object to use.
   */
  public void setQWServerObj(QWServer qwServerObj);

  /**
   * Returns the QuakeWatch server object in use.
   * @return The QuakeWatch server object in use.
   */
  public QWServer getQWServerObj();

  /**
   * Starts up the message collection threads of the outputter.
   */
  public void startOutputter();

  /**
   * Stops the message collection threads of the outputter.
   */
  public void stopOutputter();

  /**
   * Returns true if the 'stopOutputter()' method has been called and
   * has completed its work.
   * @return true if the 'stopOutputter()' method has been called and
   * has completed its work; false if not.
   */
  public boolean isOutputterStopped();

  /**
   * Delivers the given message to the outputter.
   * @param qwSvrMsgRecObj server-message-record object for message.
   */
  public void outputMessage(QWServerMsgRecord qwSvrMsgRecObj);

  /**
   * Returns the current estimated thread count for the outputter.
   * @return The current estimated thread count for the outputter.
   */
  public int getEstimatedThreadCount();

  /**
   * Returns a status message for the outputter.
   * @return Status message string for the outputter, or null if none.
   */
  public String getStatusMessageStr();


  /**
   * Class QWOutputterInitException defines an exception thrown while
   * initializing an outputter module.
   */
  public static class QWOutputterInitException extends Exception
  {
    /**
     * Creates an outputter-module-initialization exception.
     * @param msgStr the information message string to use.
     */
    public QWOutputterInitException(String msgStr)
    {
      super(msgStr);
    }
  }
}
