//SubscriberManager.java:  Manages subscribers to the QWCorbaOutputter
//                         module.
//
//  7/10/2008 -- [ET]  Initial version.
// 10/20/2008 -- [ET]  Added 'getSubscribeAllDefaultFlag()' method.
//

package com.isti.quakewatch.server.outputter;

import java.util.Vector;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import java.io.File;
import com.isti.util.UtilFns;
import com.isti.util.FileUtils;
import com.isti.util.LogFile;
import com.isti.util.FifoHashtable;
import com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlk;
import com.isti.quakewatch.server.QWServerMsgRecord;
import com.isti.quakewatch.server.QWServer;

/**
 * Class SubscriberManager manages subscribers to the QWCorbaOutputter
 * module.
 */
public class SubscriberManager
{
  protected final QWServer qwServerObj;
  protected final QWCorbaOutputter.OutputterSettings outputterSettingsObj;
  protected final String trackingDataDirStr;
  protected final LogFile logObj;
  protected final boolean timerThreadStartedFlag;
  protected final FifoHashtable subscribersTableObj = new FifoHashtable();
  protected boolean subscribersPresentFlag = false;
              //prefix for subscriber message-tracking file names:
  protected static final String TRKFILE_PREFIX_STR = "corbaOutputter_";
              //suffix for subscriber message-tracking file names:
  protected static final String TRKFILE_SUFFIX_STR = ".dat";
              //default 'maxTrackingFilesAgeMs' if 'maxMessageAgeHours'==0
  protected static final long DEF_MAXTRKFILES_AGEMS =
                                                    10 * UtilFns.MS_PER_DAY;

  /**
   * Creates a subscriber manager.
   * @param qwServerObj parent QWServer object.
   * @param outputterSettingsObj configuration-property settings to use.
   * @param logObj log-file object to use.
   */
  public SubscriberManager(QWServer qwServerObj,
                    QWCorbaOutputter.OutputterSettings outputterSettingsObj,
                                                             LogFile logObj)
  {
    if(qwServerObj == null || outputterSettingsObj == null || logObj == null)
      throw new NullPointerException("Null parameter(s)");
    this.qwServerObj = qwServerObj;
    this.outputterSettingsObj = outputterSettingsObj;
    this.logObj = logObj;
         //setup directory for subscriber message-tracking files:
    final String trkDirStr =
              outputterSettingsObj.trackingDataDirProp.stringValue().trim();
    final int ePos;             //check if trailing char is separator:
    if((ePos=trkDirStr.length()-1) >= 0 &&
          (trkDirStr.charAt(ePos) == '/' || trkDirStr.charAt(ePos) == '\\'))
    {  //given directory string ends with separator; use as-is
      trackingDataDirStr = trkDirStr;
    }
    else    //given directory string does not end with separator; add one
      trackingDataDirStr = trkDirStr + File.separator;
         //if message-tracking-directory string not empty then create
         // any needed subdirectories (append 'x' for dummy filename):
    if(trackingDataDirStr.length() > 0)
    {  //message-tracking-directory string not empty
              //create any needed subdirs (append 'x' for dummy filename):
      FileUtils.createParentDirs(trackingDataDirStr + 'x');
              //setup timer task to remove old message-tracking files:
                   //get max age for delivered messages (in ms):
      final long maxMessageAgeMs =
          (long)(outputterSettingsObj.maxMessageAgeHoursProp.doubleValue() *
                                                 UtilFns.MS_PER_HOUR + 0.5);
      if(maxMessageAgeMs > 0)
      {  //max age for delivered messages is greater than zero
                        //setup maximum-age value (use max-msg-age
                        // value x 10, or default if zero):
        final long maxTrackingFilesAgeMs = (maxMessageAgeMs > 0) ?
                               maxMessageAgeMs * 10 : DEF_MAXTRKFILES_AGEMS;
                        //setup check-interval value (use max-msg-age
                        // value, or default if zero):
        final long chkTrkFilesIntervalMs = (maxMessageAgeMs > 0) ?
                                 maxMessageAgeMs : DEF_MAXTRKFILES_AGEMS/10;
                        //create timer object (with daemon thread):
        final Timer timerObj =
                     UtilFns.createNamedTimerObj("SubscriberMgrTimer",true);
                        //setup periodic task to run at each interval:
        timerObj.scheduleAtFixedRate(new TimerTask()
            {
              public void run()
              {                        //set local copy of max-age value:
                long maxAgeMs = maxTrackingFilesAgeMs;
                        //check age of last server message vs max-age value
                        // to make sure in-use message-tracking files will
                        // not be removed because of lack of msg traffic:
                final QWServerMsgRecord recObj;  //get last message received:
                if((recObj=getLastServerMsgObj()) != null)
                {  //fetched record object for last server msg received OK
                  final long recTimeGenVal;
                  if((recTimeGenVal=recObj.getTimeGenerated()) > 0)
                  {  //time value for last server msg received is available
                    final long lastMsgAgeVal =   //calcuate message age
                                 System.currentTimeMillis() - recTimeGenVal;
                             //if max-age less than last-msg age then
                             // make max-age greater than last-msg age:
                    if(maxAgeMs < lastMsgAgeVal)
                      maxAgeMs = lastMsgAgeVal + UtilFns.MS_PER_MINUTE;
                  }
                }
                SubscriberManager.this.logObj.debug3(
                    "SubscriberManager:  Checking message-tracking files;" +
                          " removing if older than " + maxAgeMs + "ms old");
                        //delete any msg-tracking files older than max age:
                FileUtils.deleteOldFiles(new File(trackingDataDirStr),
                                                                   maxAgeMs,
                           (TRKFILE_PREFIX_STR + '*' + TRKFILE_SUFFIX_STR));
              }
            }, chkTrkFilesIntervalMs,chkTrkFilesIntervalMs);
        timerThreadStartedFlag = true;      //indicate timer started
      }
      else
        timerThreadStartedFlag = false;     //indicate timer not started
    }
    else
      timerThreadStartedFlag = false;       //indicate timer not started
  }

  /**
   * Adds a subscriber to the list.  If an entry with the same name exists
   * in the list then its 'CorbaOutSubscriber' object will be returned.
   * @param subscriberNameStr name of subscriber.
   * @param domainChanBlkArr array of domain/channel pairs to be subscribed
   * to, or null for none.
   * @return A 'CorbaOutSubscriber' object to be used for interacting
   * with the CORBA outputter.
   */
  public CorbaOutSubscriber addSubscriber(String subscriberNameStr,
                                          DomainChanBlk [] domainChanBlkArr)
  {
    synchronized(subscribersTableObj)
    {  //grab thread-synchronization lock for table
      final Object obj;
      if((obj=subscribersTableObj.get(subscriberNameStr)) instanceof
                                                       CorbaOutSubscriber)
      {  //entry with same name exists
        final CorbaOutSubscriber outSubObj = (CorbaOutSubscriber)obj;
                                //disable callback (until subscriber calls
                                // 'connectRecMsgCallBack()' method):
        outSubObj.setCallBackEnabled(false);
                                //set new domain/channel-pairs array:
        outSubObj.setDomainChanBlkArr(domainChanBlkArr);
        logObj.debug("SubscriberManager:  Reusing subscriber \"" +
                     subscriberNameStr + "\" in response to 'add' request");
        return outSubObj;       //return existing entry
      }
                                //create new subscriber object:
      final CorbaOutSubscriber outSubObj = new CorbaOutSubscriber(
                                   subscriberNameStr,domainChanBlkArr,this);
                                //add entry to table:
      subscribersTableObj.put(subscriberNameStr,outSubObj);
      subscribersPresentFlag = true;        //indicate at least 1 subscriber
      logObj.debug("SubscriberManager:  Added subscriber \"" +
                                          subscriberNameStr + "\" to list");
      return outSubObj;         //return new entry
    }
  }

  /**
   * Removes the given subscriber from the list.
   * @param subscriberNameStr name of subscriber.
   * @return The 'CorbaOutSubscriber' object for the subscriber, or
   * null if the given subscriber name was not matched.
   */
  public CorbaOutSubscriber removeSubscriber(String subscriberNameStr)
  {
    synchronized(subscribersTableObj)
    {  //grab thread-synchronization lock for table
      final Object obj;
      final CorbaOutSubscriber retObj =
           ((obj=subscribersTableObj.remove(subscriberNameStr)) instanceof
                     CorbaOutSubscriber) ? (CorbaOutSubscriber)obj : null;
                        //indicate if at least 1 subscriber in table:
      subscribersPresentFlag = !subscribersTableObj.isEmpty();
      if(retObj != null)
      {  //subscriber object was found and removed from list
        logObj.debug("SubscriberManager:  Removed subscriber \"" +
                                        subscriberNameStr + "\" from list");
      }
      else
      {  //subscriber object was not found in list
        logObj.debug("SubscriberManager:  Already-removed subscriber \"" +
                      subscriberNameStr + "\" requested removal from list");
      }
      return retObj;
    }
  }

  /**
   * Returns the count of the number of connected subscribers.
   * @return The count of the number of connected subscribers.
   */
  public int getSubscribersCount()
  {
    return subscribersTableObj.size();
  }

  /**
   * Builds the path/file name for a subscriber message-tracking file,
   * given the subscriber name.
   * @param subscriberNameStr name of subscriber.
   * @return The path/file name for a subscriber message-tracking file.
   */
  public String buildTrackingFileName(String subscriberNameStr)
  {
    return trackingDataDirStr + TRKFILE_PREFIX_STR +
           UtilFns.stringToFileName(subscriberNameStr) + TRKFILE_SUFFIX_STR;
  }

  /**
   * Delivers the given message to the subscribers.
   * @param qwSvrMsgRecObj server-message-record object for message.
   */
  public void outputMessage(QWServerMsgRecord qwSvrMsgRecObj)
  {
    if(subscribersPresentFlag)
    {  //at least one subscriber is present
      logObj.debug3("SubscriberManager:  Outputting message (" +
                             qwSvrMsgRecObj.msgNumber + ") to subscribers");
      synchronized(subscribersTableObj)
      {  //grab thread-synchronization lock for table
        final Iterator iterObj = subscribersTableObj.values().iterator();
        Object obj;
        while(iterObj.hasNext())
        {  //for each 'CorbaOutSubscriber' object in table
          if((obj=iterObj.next()) instanceof CorbaOutSubscriber)
            ((CorbaOutSubscriber)obj).addToOutputQueue(qwSvrMsgRecObj);
        }
      }
    }
  }

  /**
   * Determines if the timer task to remove old message-tracking files
   * has been started.
   * @return true if the timer task to remove old message-tracking files
   * has been started; false if not.
   */
  public boolean isTimerThreadStartedFlag()
  {
    return timerThreadStartedFlag;
  }

  /**
   * Returns the maximum age (in milliseconds) for messages delivered to
   * subscribers.
   * @return The maximum age (in milliseconds) for messages delivered to
   * subscribers, or 0 if no limit.
   */
  public long getMaxMessageAgeMS()
  {
    return (long)(outputterSettingsObj.maxMessageAgeHoursProp.doubleValue()*
                                                   UtilFns.MS_PER_HOUR+0.5);
  }

  /**
   * Returns the maximum number messages for a subscriber queue.
   * @return The maximum number messages for a subscriber queue,
   * or 0 if no limit,
   */
  public int getMaxMsgQueueCount()
  {
    return outputterSettingsObj.maxMsgQueueCountProp.intValue();
  }

  /**
   * Returns value for delay before retry after failed delivery to
   * subscriber.
   * @return Value for delay before retry after failed delivery to
   * subscriber, in milliseconds.
   */
  public long getRetryAfterFailDelayMS()
  {
    return outputterSettingsObj.retryAfterFailDelaySecsProp.longValue()*1000;
  }

  /**
   * Returns the maximum delivery-failure retry time before the subscriber
   * is disconnected.
   * @return Maximum retry time before subscriber disconnect, in
   * milliseconds (0 = no limit).
   */
  public long getMaximumRetryMillis()
  {
    return (long)(outputterSettingsObj.maximumRetryMinutesProp.doubleValue()*
                                                 UtilFns.MS_PER_MINUTE+0.5);
  }

  /**
   * Determines if the 'QWmessage' message element should be unpacked.
   * @return true if the 'QWmessage' message element should be unpacked;
   * false if not.
   */
  public boolean getUnpackQWMessageFlag()
  {
    return outputterSettingsObj.unpackQWMessageFlagProp.booleanValue();
  }

  /**
   * Determines if the 'DataMessage' message element should be unpacked.
   * @return true if the 'DataMessage' message element should be unpacked;
   * false if not.
   */
  public boolean getUnpackDataMessageFlag()
  {
    return outputterSettingsObj.unpackDataMessageFlagProp.booleanValue();
  }

  /**
   * Determines if the 'EQMessage' message element should be unpacked.
   * @return true if the 'EQMessage' message element should be unpacked;
   * false if not.
   */
  public boolean getUnpackEQMessageFlag()
  {
    return outputterSettingsObj.unpackEQMessageFlagProp.booleanValue();
  }

  /**
   * Determines if a subscription to no domain/channels should result
   * in all messages being delivered.
   * @return true if a subscription to no domain/channels should result
   * in all messages being delivered.
   */
  public boolean getSubscribeAllDefaultFlag()
  {
    return outputterSettingsObj.subscribeAllDefaultFlagProp.booleanValue();
  }

  /**
   * Fetches messages newer or equal to the specified time value or
   * later than the specified message number from the parent server.
   * If a non-zero message number is given and the given time value
   * matches then messages with message numbers greater than the given
   * message number are returned; otherwise messages newer or equal to
   * the specified time value are returned (within a tolerance value).
   * @param requestTimeVal the time-generated value for message associated
   * with the given message number, or the requested time value to be used
   * (milliseconds since 1/1/1970).
   * @param msgNum the message number to use, or 0 or none.
   * @return A new 'Vector' containing the requested 'QWServerMsgRecord'
   * objects.
   */
  public Vector getServerMsgs(long requestTimeVal, long msgNum)
  {
    return qwServerObj.getCachedArchiveMsgs(requestTimeVal,msgNum);
  }

  /**
   * Returns the newest message object entered into the server cache
   * or archive.
   * @return The newest 'QWServerMsgRecord' object entered into the
   * server cache or archive, or null if none have been entered.
   */
  public QWServerMsgRecord getLastServerMsgObj()
  {
    return qwServerObj.getLastStoredMsgObj();
  }

  /**
   * Terminates all the subscribers and removes them from the list.
   */
  public void terminateSubscribers()
  {
    if(subscribersPresentFlag)
    {  //at least one subscriber is present
      logObj.debug("SubscriberManager:  Terminating all subscribers");
      synchronized(subscribersTableObj)
      {  //grab thread-synchronization lock for table
        final Iterator iterObj = subscribersTableObj.values().iterator();
        Object obj;
        while(iterObj.hasNext())
        {  //for each 'CorbaOutSubscriber' object in table
          if((obj=iterObj.next()) instanceof CorbaOutSubscriber)
            ((CorbaOutSubscriber)obj).terminateSubscriber();
        }
        subscribersPresentFlag = false;     //indicate no subscribers
      }
    }
  }

  /**
   * Returns the log-file object in use.
   * @return The 'LogFile' object in use.
   */
  public LogFile getLogFileObj()
  {
    return logObj;
  }
}
