//QWAbstractOutputter.java:  Defines a basic set of framework methods used
//                           by QuakeWatch "outputter" modules.
//
//  6/17/2008 -- [ET]  Modified from 'QWAbstractFeeder' class.
//

package com.isti.quakewatch.server.outputter;

import com.isti.util.LogFile;
import com.isti.quakewatch.server.QWServer;

/**
 * Class QWAbstractOutputter defines a basic set of framework methods used
 * by QuakeWatch "feeder" modules.  Outputter-module classes should extend
 * this class.
 */
public abstract class QWAbstractOutputter extends Thread
                                                implements QWOutputterPlugin
{
  protected String outputterName = "outputter";  //outputter name
  protected QWServer qwServerObj = null;         //handle to parent QWServer
  protected LogFile logObj = null;               //log file object
  protected boolean outputterStoppedFlag = false;
         //variables for log file setup:
  protected String logFileNameStr = null;
  protected int logFileLogLevel = LogFile.NO_MSGS;
  protected int logFileConsoleLevel = LogFile.NO_MSGS;
  protected boolean logFileUseDateInFnameFlag = true;
  protected int logFileSwitchIntervalDays = 1;
  protected int logFileMaxAgeDays = 30;

  /**
   * Constructs an abstract-outputter instance.
   */
  public QWAbstractOutputter()
  {
  }

  /**
   * Constructs an abstract-outputter instance.
   * @param threadNameStr name use for the outputter thread.
   */
  public QWAbstractOutputter(String threadNameStr)
  {
    super(threadNameStr);
  }

  /**
   * Sets the name for this outputter.  The name is usually loaded from the
   * QuakeWatch-server configuration file.
   * @param str the name to use.
   */
  public void setOutputterName(String str)
  {
    outputterName = str;
  }

  /**
   * Returns the name for this outputter.  The name is usually loaded from
   * the QuakeWatch-server configuration file.
   * @return The outputter name.
   */
  public String getOutputterName()
  {
    return outputterName;
  }

  /**
   * Sets up the log file to be used with this outputter.  The log file is
   * not actually created until the 'startOutputter()' method is called.
   * @param logNameStr name of log file, or null for none.
   * @param logFileLevel message level for log file output.
   * @param consoleLevel message level for console output.
   * @param useDateInFnameFlag true to use the date in the file name (and
   * create a new log file everytime the date changes).
   * @param switchIntervalDays the minimum number of days between
   * log file switches via date changes.
   * @param maxLogFileAgeDays the maximum log file age, in days (if
   * 'useDateInFnameFlag' is true).
   */
  public void setupLogFile(String logNameStr,int logFileLevel,
                                int consoleLevel,boolean useDateInFnameFlag,
                               int switchIntervalDays,int maxLogFileAgeDays)
  {
    logFileNameStr = logNameStr;       //save setup values for log file
    logFileLogLevel = logFileLevel;
    logFileConsoleLevel = consoleLevel;
    logFileUseDateInFnameFlag = useDateInFnameFlag;
    logFileSwitchIntervalDays = switchIntervalDays;
    logFileMaxAgeDays = maxLogFileAgeDays;
  }

  /**
   * Returns the log file object for the outputter.
   * @return The 'LogFile' object for the outputter.
   */
  public LogFile getLogFileObj()
  {
    if(logObj == null)                 //if outputter log file not setup then
      setupOutputterLogFileObj();      //setup log file for outputter
    return logObj;
  }

  /**
   * Closes the log file for this outputter.
   */
  public void closeLogFile()
  {
    if(logObj != null)
    {    //log file was opened
      logObj.close();        //close log file
      logObj = null;         //clear handle for log file
    }
  }

  /**
   * Processes outputter-specific settings.  The settings are usually
   * fetched from the "QWOutputterSettings" element in the QuakeWatch-server
   * configuration file.  (For 'QWOutputterPlugin' interface.)
   * @param settingsStr The settings string to be processed.
   * @throws QWOutputterInitException if an error occurs while processing
   * the settings.
   */
  public void processOutputterSettings(String settingsStr)
                                             throws QWOutputterInitException
  {
  }

  /**
   * Enters the parent QuakeWatch server object to use.
   * @param qwServerObj the parent QuakeWatch server object to use.
   */
  public void setQWServerObj(QWServer qwServerObj)
  {
    this.qwServerObj = qwServerObj;
  }

  /**
   * Returns the parent QuakeWatch server object in use.
   * @return The parent QuakeWatch server object in use.
   */
  public QWServer getQWServerObj()
  {
    return qwServerObj;
  }

  /**
   * Starts up the processsing thread of the outputter.  Subclasses should
   * implement the 'run()' method for the outputter.  The outputter's log
   * file is also setup by this method.
   * (For 'QWOutputterPlugin' interface.)
   */
  public void startOutputter()
  {
    outputterStoppedFlag = false;      //clear outputter-stopped flag
    if(logObj == null)                 //if outputter log file not setup then
      setupOutputterLogFileObj();      //setup log file for outputter
    start();       //startup the thread (executes the 'run' method)
  }

  /**
   * Stops the message collection threads of the outputter.
   * (For 'QWOutputterPlugin' interface.)
   */
  public void stopOutputter()
  {
    closeLogFile();                    //close log file (if open)
    outputterStoppedFlag = true;       //set outputter-stopped flag
  }

  /**
   * Returns true if the 'stopOutputter()' method has been called and
   * has completed its work.  (For 'QWOutputterPlugin' interface.)
   * @return true if the 'stopOutputter()' method has been called and
   * has completed its work; false if not.
   */
  public boolean isOutputterStopped()
  {
    return outputterStoppedFlag;
  }

  /**
   * Sets up the log file (if 'setupLogFile()' method was called).
   */
  protected void setupOutputterLogFileObj()
  {
    if((logFileNameStr != null && logFileLogLevel != LogFile.NO_MSGS) ||
                                     logFileConsoleLevel != LogFile.NO_MSGS)
    {    //logfile was setup for messages or for console output
      if(logObj != null && logObj.isOpen())
       logObj.close();       //if existing log file open then close it
              //create and setup log file object:
      logObj = new LogFile(logFileNameStr,logFileLogLevel,
                       logFileConsoleLevel,false,logFileUseDateInFnameFlag);
      logObj.setLogFileSwitchIntervalDays(logFileSwitchIntervalDays);
      logObj.setMaxLogFileAge(logFileMaxAgeDays);
    }
    else if(logObj == null)       //if none then setup dummy log file object
      logObj = new LogFile(null,LogFile.NO_MSGS,LogFile.ERROR);
  }
}
