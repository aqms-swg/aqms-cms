//CorbaOutSubscriber.java:  Manages a subscriber to the QWCorbaOutputter
//                          module and implements the 'QWPushSubServices.idl'
//                          methods.
//
//  7/21/2008 -- [ET]  Initial version.
// 10/20/2008 -- [ET]  Modified to support 'subscribeAllDefaultFlag'
//                     configuration parameter and changed comment on
//                     'subscribe()' method about behavior when no
//                     domain/channels are subscribed to.
//  10/9/2009 -- [ET]  Minor improvements to delivery-failure logic and
//                     log outputs.
//

package com.isti.quakewatch.server.outputter;

import java.util.List;
import java.util.Iterator;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;
import org.jdom.Element;
import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.util.FileUtils;
import com.isti.util.IstiXmlUtils;
import com.isti.util.TwoObjectMatcher;
import com.isti.util.queue.NotifyEventQueue;
import com.isti.quakewatch.common.MsgTag;
import com.isti.quakewatch.corbaclient.qw_client_rec.DomainChanBlk;
import com.isti.quakewatch.corbaclient.qw_client_rec.QWRecMsgCallBack;
import com.isti.quakewatch.corbaclient.qw_client_rec.QWPushSubServicesPOA;
import com.isti.quakewatch.server.QWServerMsgRecord;

/**
 * Class CorbaOutSubscriber manages a subscriber to the QWCorbaOutputter
 * module and implements the 'QWPushSubServices.idl' methods.
 */
public class CorbaOutSubscriber extends QWPushSubServicesPOA
{
  protected final String subscriberNameStr;
  protected final SubscriberManager subscriberManagerObj;
  protected final String trackingFileNameStr;
  protected final LogFile logObj;
  protected final long maxMessageAgeMS;
  protected final int maxMsgQueueCount;
  protected final long maximumRetryMillis;
  protected final boolean unpackQWMessageFlag;
  protected final boolean unpackDataMessageFlag;
  protected final boolean unpackEQMessageFlag;
  protected final boolean subscribeAllDefaultFlag;
  protected final MessageOutputQueue messageOutputQueueObj;
//  protected long lastActivityTime;
//  protected final Object lastActivityTimeSyncObj = new Object();
  protected boolean subscriberActiveFlag;
  protected QWRecMsgCallBack messageCallBackObj = null;
  protected boolean msgCallBackEnabledFlag = false;
  protected final Object msgCallBackSyncObj = new Object();
  protected final TwoObjectMatcher domainChanMatcherObj =
                                                     new TwoObjectMatcher();
  protected static final String ASTERISK_STR = "*";
              //result values returned by 'processMsgFromQueue()' method:
  protected static final int PROCRES_COMM_ERR = 0;    //communications error
  protected static final int PROCRES_SUCCESS = 1;     //delivered OK
  protected static final int PROCRES_CB_DISABLED = 2; //call-back disabled
  protected static final int PROCRES_NOT_DOMCH = 3;   //domain/chan mismatch
  protected static final int PROCRES_MSG_TOOOLD = 4;  //message too old
  protected static final int PROCRES_PROC_ERROR = 5;  //error processing msg
              //subscriber-terminated message string:
  protected static final String SUB_TERM_STR = "Subscriber terminated";

  /**
   * Creates a 'QWCorbaOutputter' subscriber.
   * @param subscriberNameStr name of subscriber.
   * @param domainChanBlkArr array of domain/channel pairs to be subscribed
   * to, or null for none.
   * @param subscriberManagerObj subscriber-manager object to use.
   */
  public CorbaOutSubscriber(String subscriberNameStr,
                                          DomainChanBlk [] domainChanBlkArr,
                                     SubscriberManager subscriberManagerObj)
  {
    this.subscriberNameStr = subscriberNameStr;
    this.subscriberManagerObj = subscriberManagerObj;
         //set subscriber-active flag (cleared when subscriber terminated):
    subscriberActiveFlag = true;
         //get log-file obj and max-message values from subscriber manager:
    logObj = subscriberManagerObj.getLogFileObj();
    maxMessageAgeMS = subscriberManagerObj.getMaxMessageAgeMS();
    maxMsgQueueCount = subscriberManagerObj.getMaxMsgQueueCount();
    maximumRetryMillis = subscriberManagerObj.getMaximumRetryMillis();
         //get unpack-'QWmessage' flag:
    unpackQWMessageFlag = subscriberManagerObj.getUnpackQWMessageFlag();
         //get unpack-'DataMessage' flag:
    unpackDataMessageFlag = subscriberManagerObj.getUnpackDataMessageFlag();
         //get unpack-'EQMessage' flag:
    unpackEQMessageFlag = subscriberManagerObj.getUnpackEQMessageFlag();
         //get subscribe-all-by-default flag:
    subscribeAllDefaultFlag =
                          subscriberManagerObj.getSubscribeAllDefaultFlag();
    trackingFileNameStr =         //build message-tracking file name
              subscriberManagerObj.buildTrackingFileName(subscriberNameStr);
    setDomainChanBlkArr(domainChanBlkArr);       //setup domain/channels
//    updateLastActivityTime();     //set initial last-activity-time value
                        //create message-output queue:
    messageOutputQueueObj = new MessageOutputQueue();
                        //start message-output queue processing thread:
    messageOutputQueueObj.startThread();
  }

  /**
   * Sets the array of domain/channel pairs for this subscriber.  Any
   * previous domain/channel pairs are discarded.
   * @param domainChanBlkArr array of domain/channel pairs to be subscribed
   * to, or null for none.
   */
  public final void setDomainChanBlkArr(DomainChanBlk [] domainChanBlkArr)
  {
    domainChanMatcherObj.clear();           //clear any previous pairs
    if(domainChanBlkArr != null)
    {  //array object was given
      for(int idx=0; idx<domainChanBlkArr.length; ++idx)
      {  //for each array entry; subscribe to domain/channel values
        subscribe(domainChanBlkArr[idx].domainStr,
                                          domainChanBlkArr[idx].channelStr);
      }
    }
  }

  /**
   * Queues the given message for delivery to the subscriber.
   * @param qwSvrMsgRecObj server-message-record object for message.
   */
  public void addToOutputQueue(QWServerMsgRecord qwSvrMsgRecObj)
  {
    messageOutputQueueObj.pushEvent(qwSvrMsgRecObj);
  }

  /**
   * Updates the last-activity-time value for the subscriber.
   */
//  public final void updateLastActivityTime()
//  {
//    synchronized(lastActivityTimeSyncObj)
//    {  //grab thread local for 'lastActivityTime' variable
//      lastActivityTime = System.currentTimeMillis();
//    }
//  }

  /**
   * Terminates the subscriber (stops the queue-processing thread and
   * clears the message queue).
   */
  public void terminateSubscriber()
  {
    logObj.debug3("CorbaOutSubscriber:  Terminating processing thread " +
                             "for subscriber '" + subscriberNameStr + '\'');
    subscriberActiveFlag = false;              //indicate terminated
    messageOutputQueueObj.stopThread();        //stop processing thread
    messageOutputQueueObj.clearEvents();       //clear events queue
    final QWRecMsgCallBack callBackObj;
    synchronized(msgCallBackSyncObj)
    {  //grab thread-synchronization lock for msg-call-back variables
      callBackObj = messageCallBackObj;     //set local handle to object
      messageCallBackObj = null;            //clear instance handle for obj
      msgCallBackEnabledFlag = false;       //clear call-back enabled flag
    }
    if(callBackObj != null)
    {  //message-call-back object exists
      try
      {                           //release CORBA message-call-back object:
        callBackObj._release();
        logObj.debug2("CorbaOutSubscriber:  Released CORBA " +
                               "message-call-back object for subscriber '" +
                                                  subscriberNameStr + '\'');
      }
      catch(Exception ex)
      {  //some kind of exception error; log it
        logObj.debug("CorbaOutSubscriber:  Error releasing CORBA " +
                               "message-call-back object for subscriber '" +
                                           subscriberNameStr + "':  " + ex);
      }
    }
    try                           //disconnect this CORBA object:
    {
      _default_POA().deactivate_object(_object_id());
      logObj.debug2("CorbaOutSubscriber:  Deactivated CORBA object " +
                             "for subscriber '" + subscriberNameStr + '\'');
    }
    catch(Exception ex)
    {  //some kind of exception error; log it
      logObj.debug("CorbaOutSubscriber:  Error deactivating CORBA object " +
                      "for subscriber '" + subscriberNameStr + "':  " + ex);
    }
  }

  /**
   * Preloads the subscriber queue with messages from the server cache
   * (if the subscriber was receiving messages previously and some were
   * missed).
   */
  protected void preloadQueueFromServerCache()
  {
    try
    {
      logObj.debug2("CorbaOutSubscriber:  Performing preload of message " +
                              "queue for subscriber '" + subscriberNameStr +
                             "', trkFName=\"" + trackingFileNameStr + "\"");
      long trkMsgNum,trkTimeGen;
      try
      {            //read line of string data from message-tracking file:
        final BufferedReader rdrObj =
                    new BufferedReader(new FileReader(trackingFileNameStr));
        final String trkDataStr = rdrObj.readLine();
        rdrObj.close();
        final int pos;            //find separator in file data:
        if((pos=trkDataStr.indexOf(' ')) > 0)
        {  //separator found; parse message-number value before separator
          trkMsgNum = Long.parseLong(trkDataStr.substring(0,pos).trim());
                             //parse time-generated value after separator:
          trkTimeGen = Long.parseLong(trkDataStr.substring(pos).trim());
          logObj.debug2("CorbaOutSubscriber:  Loaded values from " +
                          "message-tracking file \"" + trackingFileNameStr +
                                 "\" for subscriber '" + subscriberNameStr +
                     "':  msgNum=" + trkMsgNum + ", timeGen=" + trkTimeGen);
        }
        else
        {  //separator not found; log warning
          logObj.warning("CorbaOutSubscriber:  Invalid data in " +
                          "message-tracking file \"" + trackingFileNameStr +
                          "\" for subscriber '" + subscriberNameStr + '\'');
          trkMsgNum = trkTimeGen = 0;    //enter zeroes for no values
        }
      }
      catch(FileNotFoundException ex)
      {  //message-tracking file not found
        trkMsgNum = trkTimeGen = 0;    //enter zeroes for no values
        logObj.debug2("CorbaOutSubscriber:  No message-tracking file \"" +
                         trackingFileNameStr + "\" found for subscriber '" +
                                                  subscriberNameStr + '\'');
      }
      catch(IOException ex)
      {  //error reading message-tracking file; log warning
        logObj.warning("CorbaOutSubscriber:  Error reading " +
                          "message-tracking file \"" + trackingFileNameStr +
                   "\" for subscriber '" + subscriberNameStr + "':  " + ex);
        trkMsgNum = trkTimeGen = 0;    //enter zeroes for no values
      }
      catch(NumberFormatException ex)
      {  //error parsing message-tracking file; log warning
        logObj.warning("CorbaOutSubscriber:  Error parsing values in " +
                          "message-tracking file \"" + trackingFileNameStr +
                   "\" for subscriber '" + subscriberNameStr + "':  " + ex);
        trkMsgNum = trkTimeGen = 0;    //enter zeroes for no values
      }
      long messageCount = 0;
      if(trkTimeGen > 0)
      {  //valid time-generated value was read from message-tracking file
        final long minTimeVal = System.currentTimeMillis() - maxMessageAgeMS;
        if(maxMessageAgeMS > 0 && trkTimeGen < minTimeVal)
        {  //maximum-message-age limit setup and time-generated value
           // from message-tracking file is older than limit
          logObj.debug2("CorbaOutSubscriber:  Loaded message-time value (" +
              trkTimeGen + ") from message-tracking file for subscriber '" +
                subscriberNameStr + "' too old; moved up to " + minTimeVal);
          trkTimeGen = minTimeVal;     //set updated message-time value
          trkMsgNum = 0;               //clear message-number value
        }
                   //fetch messages from server's cached-archive:
        List msgsListObj =
                   subscriberManagerObj.getServerMsgs(trkTimeGen,trkMsgNum);
        if(maxMsgQueueCount > 0)
        {  //messages-queue-size limit is setup
          final int msgsListSize = msgsListObj.size();
          final int dropCount;
          if((dropCount=msgsListSize-maxMsgQueueCount) > 0)
          {  //messages-list size too large; truncate beginning of list
            msgsListObj = msgsListObj.subList(dropCount,msgsListSize);
            logObj.debug("CorbaOutSubscriber:  Too many messages (" +
                       msgsListSize + ") for queue during preload (limit=" +
                             maxMsgQueueCount + ", dropCount=" + dropCount +
                           ") for subscriber '" + subscriberNameStr + '\'');
          }
        }
        final Iterator iterObj = msgsListObj.iterator();
        Object obj;
        while(iterObj.hasNext())
        {  //for each fetched message
          if((obj=iterObj.next()) instanceof QWServerMsgRecord)
          {  //fetched object is 'QWServerMsgRecord'; add to queue
            addToOutputQueue((QWServerMsgRecord)obj);
            ++messageCount;            //increment count
          }
        }
      }
         //if no msgs preloaded into queue and msgNum/timeGen values from
         // tracking file don't match values for last server msg then put
         // last server message msgNum/timeGen values into tracking file
         // so subscriber message-position is synchronized with server:
      if(messageCount <= 0)
      {  //no messages preloaded into queue
        final QWServerMsgRecord recObj;     //get last msg recv'd by server:
        final long recTimeGenVal;
        if((recObj=subscriberManagerObj.getLastServerMsgObj()) != null &&
                                                     recObj.msgNumber > 0 &&
                            (recTimeGenVal=recObj.getTimeGenerated()) > 0 &&
                                           (recObj.msgNumber != trkMsgNum ||
                                               recTimeGenVal != trkTimeGen))
        {  //last message object exists and values not same tracking values
          logObj.debug2("CorbaOutSubscriber:  No messages preloaded; " +
                              "writing values to message-tracking file \"" +
                               trackingFileNameStr + "\" for subscriber '" +
                      subscriberNameStr + "':  msgNum=" + recObj.msgNumber +
                                              ", timeGen=" + recTimeGenVal);
          try      //write to message-tracking file for subscriber
          {        // (enter message-number and time-generated values):
            FileUtils.writeStringToFile(trackingFileNameStr,
                      (recObj.msgNumber + " " + recTimeGenVal));
          }
          catch(Exception ex)
          {  //exception error during message-tracking file update; log it
            logObj.warning("CorbaOutSubscriber:  Error writing " +
                          "message-tracking file \"" + trackingFileNameStr +
                   "\" for subscriber '" + subscriberNameStr + "':  " + ex);
          }
        }
        else
        {  //no last-msg object or values not same tracking values
          logObj.debug2("CorbaOutSubscriber:  No messages preloaded " +
                  "into queue for subscriber '" + subscriberNameStr + '\'');
        }
      }
      else
      {  //messages were preloaded into queue
        logObj.debug2("CorbaOutSubscriber:  Preloaded messages " +
                              "into queue for subscriber '" + subscriberNameStr +
                             "', count = " + messageCount);
      }
    }
    catch(Exception ex)
    {  //some kind of exception error; log it
      logObj.warning("CorbaOutSubscriber:  Error preloading message " +
                              "queue for subscriber '" + subscriberNameStr +
                                                               "':  " + ex);
    }
  }

  /**
   * Processes the given message (which has been pulled from the queue),
   * delivering it to the subscriber.
   * @param qwSvrMsgRecObj server-message-record object for message.
   * @return PROCRES_COMM_ERR if the message delivery failed and the
   * message should be pushed back onto the queue; PROCRES_SUCCESS if
   * the message was successfully delivered; PROCRES_NOT_DOMCH if the
   * message was discarded because it contains domain/channel values
   * other than those registered for subscription; PROCRES_MSG_TOOOLD
   * if the message was too old; or PROCRES_CB_DISABLED if the subscriber
   * call-back was disabled.
   */
  protected int processMsgFromQueue(QWServerMsgRecord qwSvrMsgRecObj)
  {
    int retVal;
    try
    {         //get thread-safe local copies of call-back variables:
      final QWRecMsgCallBack callBackObj;
      final boolean enabledFlag;
      synchronized(msgCallBackSyncObj)
      {  //grab thread-synchronization lock for msg-call-back variables
        enabledFlag = msgCallBackEnabledFlag;
        callBackObj = messageCallBackObj;
      }
      if(enabledFlag)
      {  //call-back for subscriber is enabled
        if(maxMessageAgeMS <= 0 ||
           System.currentTimeMillis() - qwSvrMsgRecObj.getTimeGenerated() <=
                                                            maxMessageAgeMS)
        {  //no max-message-age limit setup or message age is <= limit
                   //check message domain/channel vs. registered values:
          if(checkMsgDomainChanValues(qwSvrMsgRecObj))
          {  //message domain/channel values check passed
            String msgDataStr;
            if(unpackQWMessageFlag)
            {  //unpacking 'QWmessage' element data
              final Element qwMsgElemObj;
              final Element dataMsgElemObj =
                      ((qwMsgElemObj=qwSvrMsgRecObj.getQWMsgElementObj()) !=
                        null) ? qwMsgElemObj.getChild(MsgTag.DATA_MESSAGE) :
                                                                       null;
                        //convert element data to string; if flag
                        // then get data below element:
              if(unpackDataMessageFlag && dataMsgElemObj != null)
              {  //unpacking 'DataMessage' element data and element object OK
                final Element eqElemObj;
                if(unpackEQMessageFlag &&
                                      (eqElemObj=IstiXmlUtils.getAnyNSChild(
                                  dataMsgElemObj,MsgTag.EQMESSAGE)) != null)
                {  //unpacking 'EQMessage' element data and element found;
                   // if "Event" child-element then use only its data:
                  final Element eventElemObj;
                  msgDataStr = ((eventElemObj=eqElemObj.getChild(
                          MsgTag.EVENT,eqElemObj.getNamespace())) != null) ?
                                IstiXmlUtils.elementToString(eventElemObj) :
                                    IstiXmlUtils.getChildDataStr(eqElemObj);
                }
                else   //not unpacking 'EQMessage' element data
                  msgDataStr = IstiXmlUtils.getChildDataStr(dataMsgElemObj);
              }
              else     //not unpacking 'DataMessage' element data
                msgDataStr = IstiXmlUtils.elementToString(dataMsgElemObj);
            }
            else   //not unpacking 'QWmessage' element data; use message str
              msgDataStr = qwSvrMsgRecObj.toArchivedForm();
                   //decode any escaped strings to control characters:
            msgDataStr = IstiXmlUtils.ctrlCharsFromEscapedCodes(msgDataStr);
            logObj.debug4("CorbaOutSubscriber:  Delivering message (" +
                            qwSvrMsgRecObj.msgNumber + ") to subscriber '" +
                                   subscriberNameStr + "':  " + msgDataStr);
            try
            {           //deliver message to subscriber:
              if(!callBackObj.receiveMessage(msgDataStr))
              {  //subscriber returned 'false'; disable call-back
                synchronized(msgCallBackSyncObj)
                {  //grab thread-synchronization lock for msg-call-back vars
                  msgCallBackEnabledFlag = false;
                }
              }
              retVal = PROCRES_SUCCESS;     //indicate message delivered OK
            }
            catch(Exception ex)
            {  //exception error during delivery; log it
              logObj.warning("CorbaOutSubscriber:  Error delivering " +
                                    "message (" + qwSvrMsgRecObj.msgNumber +
                     ") to subscriber '" + subscriberNameStr + "':  " + ex);
              return PROCRES_COMM_ERR;      //indicate message not delivered
            }
//            updateLastActivityTime();  //update activity time for subscriber
          }
          else     //message domain/channel values mismatched
            retVal = PROCRES_NOT_DOMCH;     //indicate dom/chan mismatch
        }
        else
        {  //message age is greater than maximum-message-age limit
          logObj.debug2("CorbaOutSubscriber:  Not delivering message (" +
                            qwSvrMsgRecObj.msgNumber + ") to subscriber '" +
                             subscriberNameStr + "' because message age (" +
                        qwSvrMsgRecObj.getTimeGenerated() + ") is too old");
          retVal = PROCRES_MSG_TOOOLD;      //indicate message too old
        }

        try        //update message-tracking file for subscriber
        {          // (enter message-number and time-generated values):
          FileUtils.writeStringToFile(trackingFileNameStr,
                                           (qwSvrMsgRecObj.msgNumber + " " +
                                        qwSvrMsgRecObj.getTimeGenerated()));
        }
        catch(Exception ex)
        {  //exception error during message-tracking file update; log it
          logObj.warning("CorbaOutSubscriber:  Error updating " +
                          "message-tracking file \"" + trackingFileNameStr +
                   "\" for subscriber '" + subscriberNameStr + "':  " + ex);
        }
      }
      else
      {  //call-back for subscriber not enabled
        logObj.debug3("CorbaOutSubscriber:  Not delivering message (" +
                            qwSvrMsgRecObj.msgNumber + ") to subscriber '" +
                     subscriberNameStr + "' because call-back is disabled");
        retVal = PROCRES_CB_DISABLED;
      }
    }
    catch(Exception ex)
    {  //some kind of exception error; log it
      logObj.warning("CorbaOutSubscriber:  Error processing message from " +
                              "queue for subscriber '" + subscriberNameStr +
                                                               "':  " + ex);
      retVal = PROCRES_PROC_ERROR;          //indicate processing error
    }
    return retVal;           //return status value
  }

  /**
   * Determines if the domain/channel values for the given message match
   * any of the registered domain/channel values for this subscriber.  If
   * no domain/channel values are registered for this subscriber then this
   * method will return the value of the 'subscribeAllDefaultFlag'
   * configuration parameter.
   * @param qwSvrMsgRecObj server-message-record object for message.
   * @return true if the domain/channel values for the given message match
   * any of the registered domain/channel values for this subscriber.
   */
  protected boolean checkMsgDomainChanValues(QWServerMsgRecord qwSvrMsgRecObj)
  {
    if(domainChanMatcherObj.isEmpty())    //if no domain/chans then
      return subscribeAllDefaultFlag;     //return 'subscribeAllDefaultFlag'
    if(domainChanMatcherObj.contains(
                                 UtilFns.EMPTY_STRING,UtilFns.EMPTY_STRING))
    {  //"match-all","match-all" entry is in matcher
      return true;
    }
    String msgDomainStr;          //get domain value for message:
    if((msgDomainStr=qwSvrMsgRecObj.msgEvtDomainStr) == null)
      msgDomainStr = UtilFns.EMPTY_STRING;  //convert 'null' to empty string
    String msgChanStr;            //get channel (type) value for message:
    if((msgChanStr=qwSvrMsgRecObj.msgEvtTypeStr) == null)
      msgChanStr = UtilFns.EMPTY_STRING;    //convert 'null' to empty string
    if(msgDomainStr.length() > 0)
    {  //domain value exists for message
      if(msgChanStr.length() > 0)
      {  //both domain and channel values exist for message
                                  //check vs. entries in matcher:
        if(domainChanMatcherObj.contains(msgDomainStr,msgChanStr))
          return true;
                             //check for entry with match-all channel value:
        if(domainChanMatcherObj.contains(msgDomainStr,UtilFns.EMPTY_STRING))
          return true;
                             //check for entry with match-all domain value:
        if(domainChanMatcherObj.contains(UtilFns.EMPTY_STRING,msgChanStr))
          return true;
      }
      else
      {  //domain value exists but no channel value for message
                                  //check vs. entries in matcher:
        if(domainChanMatcherObj.contains(msgDomainStr,msgChanStr))
          return true;
      }
    }
    else if(msgChanStr.length() > 0)
    {  //no domain value but channel value exists for message
                                  //check vs. entries in matcher:
      if(domainChanMatcherObj.contains(msgDomainStr,msgChanStr))
        return true;
    }
    logObj.debug2("CorbaOutSubscriber:  Not delivering message (" +
                                                  qwSvrMsgRecObj.msgNumber +
                                   ") to subscriber '" + subscriberNameStr +
                         "' because no match on domain/channel values (\"" +
                             msgDomainStr + "\" / \"" + msgChanStr + "\")");
    return false;
  }

  //start of 'QWPushSubServicesPOA' methods:

  /**
   * Confirms the status of the connection to the CORBA outputter.
   * If the connection is OK then this method will return successfully;
   * if not then a CORBA exception will be thrown.
   */
  public void checkConnection()
  {
    if(!subscriberActiveFlag)     //if subscriber terminated then throw ex
      throw new org.omg.CORBA.OBJECT_NOT_EXIST("Subscriber terminated");
    if(!messageOutputQueueObj.isEmpty())         //if messages waiting then
      messageOutputQueueObj.notifyThread();      // notify processing thread
  }

  /**
   * Connects the given call-back object.
   * @param messageCallBackObj the subscriber call-back object to
   * use.
   * @param initialEnableFlag true if the call-back method
   * should be initially enabled; false if the call-back method
   * should be initially disabled (in which case the subscriber
   * will use the 'setCallBackEnabled()' method to enable the
   * call-back later).
   */
  public void connectRecMsgCallBack(QWRecMsgCallBack messageCallBackObj,
                                                boolean initialEnableFlag)
  {
    if(!subscriberActiveFlag)     //if subscriber terminated then throw ex
      throw new org.omg.CORBA.OBJECT_NOT_EXIST(SUB_TERM_STR);
    synchronized(msgCallBackSyncObj)
    {  //grab thread-synchronization lock for msg-call-back variables
      this.messageCallBackObj = messageCallBackObj;
                             //if call-back obj not null then set to param:
      msgCallBackEnabledFlag = (messageCallBackObj != null) ?
                                                  initialEnableFlag : false;
    }
    if(initialEnableFlag)    //if being enabled then do thread notify
      messageOutputQueueObj.notifyThread();
  }

  /**
   * Enables the message-delivery call-back object.  Subscribers can
   * use this method to control when messages may be sent.
   * @param enableFlag true if the call-back method should be
   * enabled; false if not.
   */
  public void setCallBackEnabled(boolean enableFlag)
  {
    if(!subscriberActiveFlag)     //if subscriber terminated then throw ex
      throw new org.omg.CORBA.OBJECT_NOT_EXIST(SUB_TERM_STR);
    synchronized(msgCallBackSyncObj)
    {  //grab thread-synchronization lock for msg-call-back variables
      msgCallBackEnabledFlag = (messageCallBackObj != null) ?
                                                         enableFlag : false;
    }
    if(enableFlag)          //if being enabled then do thread notify
      messageOutputQueueObj.notifyThread();
  }

  /**
   * Disconnects this subscriber from the CORBA outputter.
   * @return A status message.
   */
  public String disconnectSubscriber()
  {
    try
    {
      logObj.debug3("CorbaOutSubscriber:  'disconnectSubscriber()' " +
              "method invoked for subscriber '" + subscriberNameStr + '\'');
           //remove this subscriber from manager's list:
      subscriberManagerObj.removeSubscriber(subscriberNameStr);
      terminateSubscriber();      //stop processing thread and clear queue
      return "ok";
    }
    catch(Exception ex)
    {  //some kind of exception error; log and return message
      final String msgStr = "Error disconnecting subscriber:  " + ex;
      logObj.warning("CorbaOutSubscriber:  " + msgStr);
      return msgStr;
    }
  }

  /**
   * Adds the given domain/channel values to the list of those
   * registered for this subscriber.  If any domain/channel values
   * are registered then only messages with matching values will
   * be delivered to the subscriber.  If no domain/channel values
   * are registered then no messages will be delivered if
   * 'subscribeAllDefaultFlag'==false' or all messages will be
   * delivered if 'subscribeAllDefaultFlag'==true.
   * @param domainStr the "domain" of interest.
   * @param chanStr the "channel" of interest.
   */
  public void subscribe(String domainStr, String chanStr)
  {
    if(!subscriberActiveFlag)     //if subscriber terminated then throw ex
      throw new org.omg.CORBA.OBJECT_NOT_EXIST(SUB_TERM_STR);
    try
    {              //translate null or "*" to empty string:
      if(domainStr == null || domainStr.equals(ASTERISK_STR))
        domainStr = UtilFns.EMPTY_STRING;
      if(chanStr == null || chanStr.equals(ASTERISK_STR))
        chanStr = UtilFns.EMPTY_STRING;
      domainChanMatcherObj.add(domainStr,chanStr);
      logObj.debug2("CorbaOutSubscriber:  Subscriber '" +
                   subscriberNameStr + "' registered to domain/channel \"" +
                                    domainStr + "\" / \"" + chanStr + "\"");
    }
    catch(Exception ex)
    {  //some kind of exception error; log and return message
      logObj.warning("CorbaOutSubscriber:  Error with subscriber '" +
                  subscriberNameStr + "' registering to domain/channel \"" +
                            domainStr + "\" / \"" + chanStr + "\":  " + ex);
    }
  }

  /**
   * Removes the given domain/channel values from the list of those
   * registered for this subscriber.
   * @param domainStr the "domain" of interest.
   * @param chanStr the "channel" of interest.
   */
  public void unsubscribe(String domainStr, String chanStr)
  {
    if(!subscriberActiveFlag)     //if subscriber terminated then throw ex
      throw new org.omg.CORBA.OBJECT_NOT_EXIST(SUB_TERM_STR);
    try
    {              //translate null or "*" to empty string:
      if(domainStr == null || domainStr.equals(ASTERISK_STR))
        domainStr = UtilFns.EMPTY_STRING;
      if(chanStr == null || chanStr.equals(ASTERISK_STR))
        chanStr = UtilFns.EMPTY_STRING;
      domainChanMatcherObj.remove(domainStr,chanStr);
      logObj.debug2("CorbaOutSubscriber:  Subscriber '" +
               subscriberNameStr + "' unregistered from domain/channel \"" +
                                    domainStr + "\" / \"" + chanStr + "\"");
    }
    catch(Exception ex)
    {  //some kind of exception error; log and return message
      logObj.warning("CorbaOutSubscriber:  Error with subscriber '" +
              subscriberNameStr + "' unregistering from domain/channel \"" +
                            domainStr + "\" / \"" + chanStr + "\":  " + ex);
    }
  }

  //end of 'QWPushSubServicesPOA' methods


  /**
   * Class MessageOutputQueue defines the message-output queue
   * implementation for the subscriber.
   */
  protected class MessageOutputQueue extends NotifyEventQueue
  {
    /**
     * Creates the message-output queue for the subscriber.
     */
    public MessageOutputQueue()
    {
      super("MessageOutputQueue--'" + subscriberNameStr + '\'');
    }

    /**
     * Executing method for queue.
     */
    public void run()
    {
      logObj.debug2("CorbaOutSubscriber:  Starting message-queue " +
           "processing thread for subscriber '" + subscriberNameStr + '\'');
      preloadQueueFromServerCache();   //preload any missed messages
      try
      {            //delay to let subscriber register domain/channels:
        waitForNotify(2000);
                   //wait-for-notify timeout value (initially infinite):
        long notifyWaitTimeMs = 0;
        long firstFailureTimeMs = 0;   //time of first delivery failure
        boolean lastDeliveryFlag = true;
        Object obj;
        QWServerMsgRecord qwSvrMsgRecObj;
        int procResVal = -1;
        while(!finishRunning())
        {  //loop while processing message objects and thread not "finished"
                   //if queue empty or comm error last iteration then
                   // wait for thread notify; otherwise skip wait so that
                   // next message waiting in queue will be processed now:
          if(isEmpty() || procResVal == PROCRES_COMM_ERR)
          {  //queue is empty or communications error in last iteration
                   //wait for queue notify (via 'addToOutputQueue()'
                   // or 'setCallBackEnabled()' method); if previous
                   // delivery failed then a non-zero timeout will be
                   // setup to facilitate retry of delivery:
            waitForNotify(notifyWaitTimeMs);
            if(finishRunning())        //if thread is "finished" then
              break;                   //exit processing loop now
          }
          if(maxMsgQueueCount > 0 && getQueueSize() > maxMsgQueueCount)
          {  //limit setup and queue size too large; discard oldest message
            obj = pullEvent();         //get oldest message
            lastDeliveryFlag = true;   //set flag as if delivered OK
            logObj.warning("CorbaOutSubscriber:  Too many messages in " +
                                        "queue (limit=" + maxMsgQueueCount +
                                  ") for subscriber '" + subscriberNameStr +
                                              "'; dropping oldest message" +
                                       ((obj instanceof QWServerMsgRecord) ?
                         (" (" + ((QWServerMsgRecord)obj).msgNumber + ")") :
                                                     UtilFns.EMPTY_STRING));
          }
          else if(msgCallBackEnabledFlag &&
                            (obj=pullEvent()) instanceof QWServerMsgRecord)
          {  //callback enabled and next message obj pulled from queue OK
            qwSvrMsgRecObj = (QWServerMsgRecord)obj;
            logObj.debug5("CorbaOutSubscriber:  Unqueuing message (" +
                           qwSvrMsgRecObj.msgNumber + ") for subscriber '" +
                                                  subscriberNameStr + '\'');
                        //deliver message object to subscriber:
            if((procResVal=processMsgFromQueue(qwSvrMsgRecObj)) ==
                                                            PROCRES_SUCCESS)
            {  //delivery succeeded
              notifyWaitTimeMs = 0;         //clear any wait-timeout value
              lastDeliveryFlag = true;      //indicate success
            }
            else if(procResVal >= PROCRES_NOT_DOMCH)
            {  //domain/channel mismatch, msg too old, or exception error
                        //move on with unqueued message object discarded
              notifyWaitTimeMs = 0;         //clear any wait-timeout value
            }
            else
            {  //delivery failed
              if(procResVal == PROCRES_COMM_ERR)
              {  //delivery failed due to communications error
                if(lastDeliveryFlag)
                {  //previous delivery was successful
                  lastDeliveryFlag = false;     //indicate failure
                               //mark time of first delivery failure:
                  firstFailureTimeMs = System.currentTimeMillis();
                  logObj.debug("CorbaOutSubscriber:  Set " +
                                    "lastDeliveryFlag=" + lastDeliveryFlag +
                           " and firstFailureTimeMs=" + firstFailureTimeMs +
                            " for subscriber '" + subscriberNameStr + '\'');
                }
                else
                {  //previous delivery also failed
                  if(firstFailureTimeMs > 0)
                  {  //first-fail time was setup OK
                    final long failTimeVal;      //check duration of failures:
                    if((failTimeVal=System.currentTimeMillis()-
                                   firstFailureTimeMs) > maximumRetryMillis)
                    {  //duration of failures greater than limit; log warning
                      logObj.warning("CorbaOutSubscriber:  Disconnecting " +
                                        "subscriber '" + subscriberNameStr +
                                          "' after delivery failures for " +
                                 (failTimeVal/1000) + " seconds (limit = " +
                                   (maximumRetryMillis/1000) + " seconds)");
                      disconnectSubscriber();    //remove & term subscriber
                      break;                     //exit processing loop
                    }
                    logObj.debug("CorbaOutSubscriber:  Keeping " +
                                        "subscriber '" + subscriberNameStr +
                                "' connected after delivery failures for " +
                                 (failTimeVal/1000) + " seconds (limit = " +
                                   (maximumRetryMillis/1000) + " seconds)");
                  }
                  else
                  {  //first-fail time was not setup (shouldn't happen)
                    logObj.warning("CorbaOutSubscriber:  Subscriber " +
                        "first-fail time not setup (" + firstFailureTimeMs +
                           ") for subscriber '" + subscriberNameStr + '\'');
                    firstFailureTimeMs = System.currentTimeMillis();
                    logObj.debug("CorbaOutSubscriber:  Set " +
                                "firstFailureTimeMs=" + firstFailureTimeMs +
                            " for subscriber '" + subscriberNameStr + '\'');
                  }
                }
                   //delivery failure was due to communications error
                pushEventBackNoNotify(obj);  //put message obj back on queue
              }         //if call-back disabled then put msg back on queue:
              else if(procResVal == PROCRES_CB_DISABLED)
                pushEventBackNoNotify(obj);

                   //if process-result was not "call-back disabled" then
                   // setup wait-for-notify timeout value so that retry of
                   // delivery will occur after delay; otherwise use 0 to
                   // wait indefinitely for 'setCallBackEnabled()' method:
              notifyWaitTimeMs = (procResVal != PROCRES_CB_DISABLED) ?
                        subscriberManagerObj.getRetryAfterFailDelayMS() : 0;
            }
          }
          else
          {  //callback disabled or no next message obj available
                   //if 'finishWorkAndStopThread()' has been called then do
                   // a "hard" stop now because no more work can be done:
            if(shouldFinishWork())
              stopThread();
                   //clear any wait-timeout value (because nothing to send):
            notifyWaitTimeMs = 0;
          }
        }
      }
      catch(Exception ex)
      {  //some kind of exception error; log it
        logObj.warning("CorbaOutSubscriber:  Terminating queue-processing" +
                                   " for subscriber '" + subscriberNameStr +
                                                   "' after error:  " + ex);
      }
      logObj.debug2("CorbaOutSubscriber:  Exiting message-queue " +
           "processing thread for subscriber '" + subscriberNameStr + '\'');
    }
  }
}
