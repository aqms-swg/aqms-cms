//QWServerCallback.java:  QuakeWatch server feeder call-back.
//
//  2/10/2002 -- [ET]  Initial version.
//  6/10/2003 -- [ET]  Modified so log-level tags are no longer put into
//                     the messages log file.
//  6/25/2003 -- [ET]  Modified to respond gracefully if message cache is
//                     uninitialized.
//  7/25/2003 -- [ET]  Improved handling of message number.
//  11/5/2003 -- [ET]  Removed 'QWFeederCallback' interface; added
//                     'getLastStoredMsgObj()' method.
// 11/26/2003 -- [ET]  Modified 'dataEvent()' to increment message number
//                     even if message send failed, to return a result
//                     string, to have a wait-queue parameter, and to
//                     have a string-parameter version.
//  1/14/2004 -- [ET]  Added optional support for event domain, type and
//                     name strings for structured events; added support
//                     for 'ServerAddress' in built messages; modified
//                     to use 'Server.setupQWMessageElement()' and
//                     'Server.sendEventChannelMessage()' methods.
//  1/22/2004 -- [ET]  Added 'DUP_RET_STR' constant.
//  11/3/2004 -- [ET]  Added 'terminateServer()' method.
//  11/4/2004 -- [ET]  Modified 'dataEvent()' method to check validity of
//                     'timeGenDateObj' parameter and give time-generated
//                     value parameter to 'incrementMsgNumber()' method.
//  11/5/2004 -- [ET]  Modified 'dataEvent()' method to enforce maximum
//                     message size limit.
// 11/12/2004 -- [ET]  Added 'getQWAcceptorImplIDString()' method.
// 11/24/2004 -- [ET]  Removed 'evtChManagerObj' variable; increased debug
//                     level of "Message discarded because duplicate or
//                     error" log output from 'debug' to 'debug2'.
//  1/10/2005 -- [ET]  Modified add-to-cache in 'dataEvent()' method to
//                     differentiate between message-duplicate and error;
//                     changed level of "aliveMsg" log output from
//                     "Debug2" to "Debug3".
//  1/22/2005 -- [ET]  Added parameters 'feederSourceHostStr' and
//                     'feederSrcHostMsgIdNum' to 'dataEvent()' methods.
//  1/26/2005 -- [ET]  Modified to give 'addCachedArchiveMessage()' method
//                     a 'QWServerMsgRecord' parameter.
//  1/28/2005 -- [ET]  Added message counter and total-data-size tracker.
//   2/8/2005 -- [ET]  Added check for feeder-data-source message numbers
//                     being sequential; added 'getFdrTblEntriesListStr()'
//                     method.
//   4/7/2005 -- [ET]  Fixed to prevent null-pointer exception when
//                     'getFdrTblEntriesListStr()' called before any
//                     calls to 'dataEvent()' method.
// 12/21/2005 -- [ET]  Modified 'dataEvent()' method that processes string
//                     data to handle non-XML compatible characters.
// 10/20/2006 -- [ET]  Added 'getAllServerCacheMsgs()' method.
// 11/20/2006 -- [ET]  Modified to check for 'MsgSrc'/'MsgIdent' values
//                     in ANSS-EQ-XML-format messages (if enabled via new
//                     'checkEQMsgIdentFlag' configuration parameter).
//  8/28/2007 -- [ET]  Modified processing of incoming message-elements
//                     so that whitespace between elements is removed and
//                     control characters within elements are encoded to
//                     "&##;" strings.
//  6/13/2008 -- [ET]  Modified 'dataEvent()' method to call QWServer
//                     'outputEventMessage()' method (with QWServerMsgRecord
//                     parameter) instead of 'sendEventChannelMessage()'.
//  4/20/2010 -- [ET]  Added support for 'msgSrc'/'msgIdent' elements in
//                     QuakeML format.
//   5/5/2010 -- [ET]  Modified 'dataEvent()' to check for time-generated
//                     value begin after current time (to fix issue where
//                     relay-feeder messages with time-generated timestamp
//                     in future would result in other messages not being
//                     stored in file archive).
//  7/15/2010 -- [ET]  Modified 'dataEvent()' method to make it check for
//                     duplicate feeder-data-source message numbers (if
//                     'checkDupFdrSourceFlag'==true); fixed issue in
//                     'dataEvent()' method where feeder-data-source table
//                     would never receive entries.
//   9/1/2010 -- [ET]  Modified 'getFdrTblEntriesListStr()' method to
//                     separate entries with ", " (instead of ",").
//  5/14/2013 -- [ET]  Added 'getLastTimeGeneratedVal()' method.
//  9/23/2013 -- [ET]  Added checks to 'dataEvent()' methods so unusual
//                     feeder-data-source message numbers will result in
//                     warning-level log messages.
// 11/15/2013 -- [ET]  Modified checks in 'dataEvent()' methods so only
//                     large changes in feeder-data-source message numbers
//                     result in warning-level log messages.
//

package com.isti.quakewatch.server;

import java.util.Date;
import java.util.Vector;
import org.jdom.Element;
import org.jdom.Attribute;
import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.util.IstiXmlUtils;
import com.isti.util.TagValueTable;
import com.isti.util.TimeConstants;
import com.isti.quakewatch.util.DomainTypeInfoTable;
import com.isti.quakewatch.util.DomainTypeInfoRec;
import com.isti.quakewatch.util.QWUtils;
import com.isti.quakewatch.common.MsgTag;
import com.isti.quakewatch.message.QWIdentDataMsgRecord;

/**
 * Class QWServerCallback defines the QuakeWatch server feeder call-back.
 * Each feeder-plugin module will hold its own instance of this object.
 */
public class QWServerCallback implements TimeConstants
{
    /** String returned by 'dataEvent()' if an error occurs. */
  public static final String ERR_RET_STR = "error";
    /** String returned by 'dataEvent()' if the message is a duplicate. */
  public static final String DUP_RET_STR = "dup";
  public final String feederNameStr;
  private static final String OB = "<";     //open bracket
//  private static final String CB = ">";     //close bracket
                   //check string for beginning of message ("<DataMessage"):
//  private static final String DATAMSG_CHECK_STR = OB + MsgTag.DATA_MESSAGE;
                   //data-message prefix string ("<DataMessage>"):
//  private static final String DATAMSG_PREFIX_STR =
//                                              OB + MsgTag.DATA_MESSAGE + CB;
                   //data-message suffix string ("</DataMessage>"):
//  private static final String DATAMSG_SUFFIX_STR =
//                                        OB + "/" + MsgTag.DATA_MESSAGE + CB;
  private static final String MSGCP_STR = "  msg:  ";   //prompt for log msgs
  private final QWServer serverObj;
//  private final QWFeederPlugin qwFeederPluginObj;
  private final LogFile logObj;
  private final LogFile messageLogFileObj;
//  private final boolean useStructuredEventsFlag;
  private final String defaultEvtDomainName;
  private final String defaultEvtTypeName;
  private final boolean defEvtNamesAvailFlag;
  private final int maximumMessageSize;
  private final String defaultFdrSourceHostStr;
  private final boolean checkDupFdrSourceFlag;
  private final boolean checkEQMsgIdentFlag;
  private long feederMessageCount = 0;      //# of messages received
  private long feederTotalDataSize = 0;     //data size of messages received
  private final Object fdrMsgInfoSyncObj = new Object();   //thread-sync obj
  private TagValueTable feederHostMsgNumTableObj = null;   //tracking table


  /**
   * Creates a QuakeWatch server feeder call-back object.
   * @param serverObj parent server object.
   * @param qwFeederPluginObj feeder-plugin module object.
   * @param feederNameStr name associated with feeder module.
   * @param logObj log file object to use.
   * @param messageLogFileObj log file object for messages sent to
   * clients, or null for none.
   */
  public QWServerCallback(QWServer serverObj,
                      QWFeederPlugin qwFeederPluginObj,String feederNameStr,
                                   LogFile logObj,LogFile messageLogFileObj)
  {
    this.serverObj = serverObj;
//    this.qwFeederPluginObj = qwFeederPluginObj;
    this.feederNameStr = feederNameStr;
    this.logObj = logObj;
    this.messageLogFileObj = messageLogFileObj;
         //setup local copies of use-structured-events flag
         // and default event domain and type names:
//    useStructuredEventsFlag = serverObj.getUseStructuredEventsFlag();
    defaultEvtDomainName = serverObj.getDefaultEvtDomainName();
    defaultEvtTypeName = serverObj.getDefaultEvtTypeName();
         //set flag true if default event domain and type name
         // strings are not null and at least one contains data:
    defEvtNamesAvailFlag = (defaultEvtDomainName != null &&
         defaultEvtTypeName != null && (defaultEvtDomainName.length() > 0 ||
                                          defaultEvtTypeName.length() > 0));
         //fetch and set maximum message size limit:
    maximumMessageSize = serverObj.getMaximumMessageSize();
         //fetch and set default data-source host name for messages:
    final String str;
    if((str=serverObj.getDefaultFdrSourceHost()) !=
                                            null && str.trim().length() > 0)
    {    //default data-source host name cfgPropItem contains data
      if(!(qwFeederPluginObj instanceof QddsFeeder) &&
                              !(qwFeederPluginObj instanceof QWRelayFeeder))
      {  //feeder plugin is not 'QddsFeeder' or 'QWRelayFeeder'
        defaultFdrSourceHostStr = str;      //use host-name value
        logObj.debug2(QWServer.PROGRAM_NAME + " \"" + feederNameStr +
                     "\":  Using default feeder-data-source host-name (\"" +
                                           defaultFdrSourceHostStr + "\")");
      }
      else
      {  //feeder plugin is 'QddsFeeder' or 'QWRelayFeeder'
        defaultFdrSourceHostStr = null;     //indicate not used
        logObj.debug(QWServer.PROGRAM_NAME + " \"" + feederNameStr +
                     "\":  Default feeder-data-source host-name (\"" + str +
                                           "\") not used with this feeder");
      }
    }
    else      //default data-source host name does not contain data
      defaultFdrSourceHostStr = null;       //indicate not used
         //setup flag to check dup feeder-data-source msgNums in messages:
    checkDupFdrSourceFlag = serverObj.getCheckDupFdrSourceFlag();
         //setup flag to check for MsgSrc/MsgIdent values in messages:
    checkEQMsgIdentFlag = serverObj.getCheckEQMsgIdentFlag();
  }

  /**
   * Processes a data event.  If 'domainNameStr', 'typeNameStr' and
   * 'eventNameStr' are all null and defalt event domain and type
   * names are available then they will be used.
   * @param domainNameStr the domain name to use, or null for none.
   * @param typeNameStr the type name to use, or null for none.
   * @param eventNameStr the event name to use, or null for none.
   * @param dataEventElement an 'Element' object containing the data-event
   * XML msg.
   * @param timeGenDateObj the time-generated date/time value to use
   * for the message, or null to use the current system time.
   * @param waitQueueFlag if true and the processing is successful then
   * this method will not return until the write-to-archive queue is
   * clear; if false then this method will not wait.
   * @param feederSourceHostStr the data-source host string for the
   * message; null or empty string for none.
   * @param feederSrcHostMsgIdNum the message-ID number from the data
   * source (positive value incremented after each message).
   * @return A string version of the message number used if successful,
   * "error" if the message was rejected due to an XML or send error, or
   * "dup" if the message was rejected due to it being a duplicate
   * of a previous message.
   */
  public synchronized String dataEvent(String domainNameStr,
            String typeNameStr,String eventNameStr,Element dataEventElement,
                                  Date timeGenDateObj,boolean waitQueueFlag,
                      String feederSourceHostStr,long feederSrcHostMsgIdNum)
  {
    final String qwMsgStr;
    final long msgNum;
    synchronized(serverObj.addMessageSyncObj)
    {    //grab sync-lock for message adds
      long msgTimeGenVal;
      final long curTimeVal = System.currentTimeMillis();
         //get last message number and increment it:
      msgNum = serverObj.getLastMsgNumber() + 1;
      final long lastTimeGeneratedVal = serverObj.getLastTimeGeneratedVal();
      // if time-generated date/time value was specified
      if (timeGenDateObj != null) {
        msgTimeGenVal = timeGenDateObj.getTime();
        // if time-generated date/time value is greater than the current time
        if (msgTimeGenVal > curTimeVal) { 
          msgTimeGenVal = curTimeVal; // use the current time
        }
      } else {
        msgTimeGenVal = curTimeVal; // use the current time
      }
      if (lastTimeGeneratedVal != 0) {
        // minimum time-generated is last value incremented since client ensures
        // it is > last value
        final long minTimeGeneratedVal = lastTimeGeneratedVal + 1;
        if (msgTimeGenVal < minTimeGeneratedVal) {
          msgTimeGenVal = minTimeGeneratedVal;
        }
        final long diff = msgTimeGenVal - curTimeVal;
        if (diff > 0) { // if message time-generated is greater than the current time
          if (diff > MS_PER_MINUTE) {
            // since time-generated is never greater than the current time, this
            // should never happen unless the system time is screwed up
            terminateServer(-1,
                QWServer.PROGRAM_NAME + " \"" + feederNameStr + " "
                    + MsgTag.TIME_GENERATED + " is too large (" + diff + "): "
                    + new Date(minTimeGeneratedVal) + ", "
                    + new Date(msgTimeGenVal));
          }
          if (diff > 1) {
            logObj.info(QWServer.PROGRAM_NAME + " \"" + feederNameStr + " "
                + MsgTag.TIME_GENERATED + " is before minimum (" + diff + "): "
                + new Date(minTimeGeneratedVal) + ", "
                + new Date(msgTimeGenVal));
          }
          try {
            Thread.sleep(diff); // wait until minimum time is reached
          } catch (Exception ex) {
          }
          msgTimeGenVal = minTimeGeneratedVal;
        }
      }
         //surround DataMessage with QWMessage:
      final Element qwMsgElement =
              (new Element(MsgTag.QW_MESSAGE)).addContent(dataEventElement);
      boolean filteredNamesFlag;
      if(domainNameStr == null && typeNameStr == null &&
                                                       eventNameStr == null)
      {  //all given names are null
        if(filteredNamesFlag=defEvtNamesAvailFlag)
        {  //default names are available
          domainNameStr = defaultEvtDomainName;    //set default names
          typeNameStr = defaultEvtTypeName;
        }
      }
      else    //at least one given name is not null
        filteredNamesFlag = true;      //indicate names are available
              //add attributes to 'QWmessage':
      serverObj.setupQWMessageElement(qwMsgElement,msgNum,
             msgTimeGenVal,(new Attribute(MsgTag.MSG_SOURCE,feederNameStr)),
                                    domainNameStr,typeNameStr,eventNameStr);
      final DomainTypeInfoRec infoRecObj;
      if(filteredNamesFlag)
      {  //filtered names are available
        final DomainTypeInfoTable infoTableObj;
        if((infoTableObj=serverObj.getDomainTypeInfoTableObj()) != null)
        {     //domain/type information table is available
                   //get info-record for domain/type names:
          infoRecObj = infoTableObj.getInfoRecObj(
                                                 domainNameStr,typeNameStr);
              //enter message-number attribute for domain/type names:
          qwMsgElement.setAttribute(MsgTag.MSG_EVT_MSG_NUM,
                       Long.toString(infoRecObj.getMessageNumberValue()+1));
        }
        else  //domain/type information table not available
          infoRecObj = null;      //indicate no info-record
      }
      else    //filtered names not available
        infoRecObj = null;        //indicate no info-record
         //if enabled then check message for MsgSrc/MsgIdent values:
      String attrFdrSrcHostStr = null;
      long attrFdrSrcHostMsgNum = 0;
      boolean msgFdrSrcFlag = false;
      if(checkEQMsgIdentFlag)
      {  //check QuakeML/ANSS-EQ-XML messages for MsgSrc/MsgIdent values
                        //determine message format:
        final QWUtils.ElemAndMsgFmtSpec elemAndMsgFmtSpecObj =
                           QWUtils.determineMessageFormat(dataEventElement);
        String msgSrcStr = null, msgIdentStr = null;
        if(elemAndMsgFmtSpecObj.messageFormatSpec ==
                                          QWIdentDataMsgRecord.MFMT_QUAKEML)
        {  //message format is QuakeML
                        //get lead element for message:
          final Element elemObj = elemAndMsgFmtSpecObj.elementObj;
          if((msgSrcStr=elemObj.getChildTextTrim(
                  MsgTag.QUAKEML_MSG_SRC,elemObj.getNamespace())) != null &&
                                                   msgSrcStr.length() > 0 &&
                                      (msgIdentStr=elemObj.getChildTextTrim(
                MsgTag.QUAKEML_MSG_IDENT,elemObj.getNamespace())) != null &&
                                                   msgIdentStr.length() > 0)
          {  //'msgSrc'/'msgIdent' values successfully fetched
            msgFdrSrcFlag = true;      //indicate fetched
          }
        }
        else if(elemAndMsgFmtSpecObj.messageFormatSpec ==
                                        QWIdentDataMsgRecord.MFMT_ANSSEQXML)
        {  //message format is ANSS-EQ-XML
                        //get lead element for message:
          final Element elemObj = elemAndMsgFmtSpecObj.elementObj;
          if((msgSrcStr=elemObj.getChildTextTrim(
                          MsgTag.MSG_SRC,elemObj.getNamespace())) != null &&
                                                   msgSrcStr.length() > 0 &&
                                      (msgIdentStr=elemObj.getChildTextTrim(
                        MsgTag.MSG_IDENT,elemObj.getNamespace())) != null &&
                                                   msgIdentStr.length() > 0)
          {  //'MsgSrc'/'MsgIdent' values successfully fetched
            msgFdrSrcFlag = true;      //indicate fetched
          }
        }
        if(msgFdrSrcFlag)
        {  //MsgSrc/MsgIdent values were fetched
          try
          {   //convert 'msgIdent' string value to integer:
            attrFdrSrcHostMsgNum = Long.parseLong(msgIdentStr);
            attrFdrSrcHostStr = msgSrcStr;
          }
          catch(Exception ex)
          {   //error converting 'msgIdent' string value to integer
            msgFdrSrcFlag = false;     //indicate no values
            logObj.debug(QWServer.PROGRAM_NAME + " \"" + feederNameStr +
                                 "\":  Error parsing '" + MsgTag.MSG_IDENT +
                           "' value \"" + msgIdentStr + "\" in message ('" +
                             MsgTag.MSG_SRC + "' = \"" + msgSrcStr + "\")");
          }
        }
      }
      final boolean incFdrSourceMsgNumFlag;
      if(!msgFdrSrcFlag)
      {  //feeder-data-source values not fetched from message
        if(defaultFdrSourceHostStr != null && (feederSourceHostStr == null ||
                                         feederSourceHostStr.length() <= 0))
        {     //default feeder-data-source host name is available and given
              // feeder-data-source host name is does not contain data
                   //use default feeder-data-source host name:
          attrFdrSrcHostStr = defaultFdrSourceHostStr;
                   //use server's feeder-data-source message number:
          attrFdrSrcHostMsgNum = serverObj.getFdrSourceMsgNumber() + 1;
                   //ind feeder-data-source msgNum needs to be incremented:
          incFdrSourceMsgNumFlag = true;
        }
        else
        {  //default feeder-data-source host name not use
                   //use given feeder-data-source host name:
          attrFdrSrcHostStr = feederSourceHostStr;
                   //use given feeder-data-source message number:
          attrFdrSrcHostMsgNum = feederSrcHostMsgIdNum;
          incFdrSourceMsgNumFlag = false;   //indicate fdr msgNum not used
        }
      }
      else
        incFdrSourceMsgNumFlag  = false;    //indicate fdr msgNum not used
              //put in feeder host name and message number:
      if(attrFdrSrcHostStr != null && attrFdrSrcHostStr.length() > 0)
      {       //feeder host name contains data; add attributes to message
        qwMsgElement.setAttribute(MsgTag.FDR_SOURCE_HOST,attrFdrSrcHostStr);
        qwMsgElement.setAttribute(MsgTag.FDR_SOURCE_MSGNUM,
                                       Long.toString(attrFdrSrcHostMsgNum));
      }
      try       //convert 'Element' object to string:
      {                    // (trim and encode whitespace)
        qwMsgStr = IstiXmlUtils.elementToFixedString(qwMsgElement);
      }
      catch(Exception ex)
      {         //some kind of exception error occurred; log it and abort
        logObj.warning(QWServer.PROGRAM_NAME +
                                    " error formatting XML output:  " + ex);
        return ERR_RET_STR;
      }
              //check if message size too large:
      if(maximumMessageSize > 0)
      {  //maximum message size limit is setup
        int sPos;       //find beginning of "<DataMessage>" element:
        if((sPos=qwMsgStr.indexOf(OB+dataEventElement.getName())) < 0)
          sPos = 0;     //if not found then use beginning of string position
        final int len;
        if((len=qwMsgStr.length()-sPos-39) > maximumMessageSize)
        {     //message size is larger than limit
          logObj.warning(QWServer.PROGRAM_NAME + " \"" + feederNameStr +
                        "\":  Rejected received message with size (" + len +
                                      ") larger than max-msg-size limit (" +
                                                  maximumMessageSize + ")");
                        //limit number of characters logged:
          final String dispStr = (len <= 1024) ? qwMsgStr :
                                (qwMsgStr.substring(0,1024).trim() + "...");
          logObj.debug(MSGCP_STR + dispStr);
          return ERR_RET_STR;
        }
      }
      if(feederSourceHostStr != null && feederSourceHostStr.length() > 0 &&
                                                  feederSrcHostMsgIdNum > 0)
      {  //valid feeder-data-source host name and message number were given
                   //if config flag then check if feeder-data-source message
                   // number matches previously-received message number in
                   // tracking buffer for feeder-data-source host name:
        if(checkDupFdrSourceFlag && serverObj.checkFeederDataSourceMsgNumDup(
                                 feederSourceHostStr,feederSrcHostMsgIdNum))
        {  //feeder-data-source message number was previously received
          logObj.debug(QWServer.PROGRAM_NAME + " \"" + feederNameStr +
                       "\":  Message discarded; feeder-data-source msg# (" +
                feederSrcHostMsgIdNum + ") is duplicate of previous (cur=" +
               serverObj.getCurFeederDataSourceMsgNum(feederSourceHostStr) +
                               "), host = \"" + feederSourceHostStr + "\"");
          logObj.debug2(MSGCP_STR + qwMsgStr);
          return DUP_RET_STR;   //exit method (don't send msg or inc msg #)
        }
      }
      final QWServerMsgRecord qwSvrMsgRecObj;
      final int msgDataSize;
      try     //add message to cached archive:
      {            //create server-message-record object:
        qwSvrMsgRecObj = new QWServerMsgRecord(
                                       qwMsgElement,dataEventElement,msgNum,
                                          new Date(msgTimeGenVal),qwMsgStr);
        final int retVal;
        if((retVal=serverObj.addCachedArchiveMessage(qwSvrMsgRecObj)) !=
                                          QWCachedArchiveMgr.ADDMSG_SUCCESS)
        {     //message not added (because duplicate or error)
          if(retVal == QWCachedArchiveMgr.ADDMSG_DUPLICATE)
          {   //message is duplicate of previous message
            logObj.debug3(QWServer.PROGRAM_NAME + " \"" + feederNameStr +
                   "\":  Message discarded; duplicate of previous message");
            logObj.debug3(MSGCP_STR + qwMsgStr);
            return DUP_RET_STR;   //exit method (don't send msg or inc msg #)
          }
              //error adding message (warning already logged)
          logObj.debug3(QWServer.PROGRAM_NAME + " \"" + feederNameStr +
                                                 "\":  Error adding message");
          logObj.debug3(MSGCP_STR + qwMsgStr);
          return ERR_RET_STR;
        }
        msgDataSize = qwSvrMsgRecObj.getDataSize();      //save data size for msg
      }
      catch(Exception ex)
      {  //some kind of exception error; log it
        logObj.warning(QWServer.PROGRAM_NAME + " \"" + feederNameStr +
                             "\":  Exception error adding message:  " + ex);
        logObj.warning(UtilFns.getStackTraceString(ex));
        logObj.debug(MSGCP_STR + qwMsgStr);
        return ERR_RET_STR;
      }
              //message successfully built and stored;
              // increment msg number and enter timeGen value:
      serverObj.incrementMsgNumber(msgTimeGenVal);
      if(incFdrSourceMsgNumFlag)                 //if feeder-data-source msg
        serverObj.incFdrSourceMsgNumber();       // number used then inc
      if(infoRecObj != null)                     //if record in use then
        infoRecObj.incMessageNumberValue();      //update message number
      synchronized(fdrMsgInfoSyncObj)
      {  //grab thread-sync lock for "feederMessage" variables
        ++feederMessageCount;                    //increment message counter
        feederTotalDataSize += msgDataSize;      //add to total data size
      }
      if(feederSourceHostStr != null && feederSourceHostStr.length() > 0 &&
                                                  feederSrcHostMsgIdNum > 0)
      {  //valid feeder-data-source host name and message number were given
        if(feederHostMsgNumTableObj != null)
        {  //feeder-data-source tracking table exists
                   //get last table entry for host name:
          final long tableMsgNum = feederHostMsgNumTableObj.get(
                                                       feederSourceHostStr);
          if(tableMsgNum > 0 && feederSrcHostMsgIdNum != tableMsgNum + 1)
          {  //new feeder-data-source msgNum not increment of previous
            if(feederSrcHostMsgIdNum < tableMsgNum - 100)
            {  //new feeder-data-source msgNum less than prev; log warning
              logObj.warning(QWServer.PROGRAM_NAME + " \"" + feederNameStr +
                    "\":  Received message with feeder-data-source msg# (" +
                    feederSrcHostMsgIdNum + ") much lower than previous (" +
                 tableMsgNum + "), host = \"" + feederSourceHostStr + "\"");
              logObj.debug(MSGCP_STR + qwMsgStr);
            }
            else if(feederSrcHostMsgIdNum > tableMsgNum + 100)
            {  //new feeder-data-source msgNum much larger; log warning
              logObj.warning(QWServer.PROGRAM_NAME + " \"" + feederNameStr +
                    "\":  Received message with feeder-data-source msg# (" +
                   feederSrcHostMsgIdNum + ") much larger than previous (" +
                 tableMsgNum + "), host = \"" + feederSourceHostStr + "\"");
              logObj.debug(MSGCP_STR + qwMsgStr);
            }
            else
            {  //looks like a few msgNums skipped; log as debug2
              logObj.debug2(QWServer.PROGRAM_NAME + " \"" + feederNameStr +
                    "\":  Received message with feeder-data-source msg# (" +
                   feederSrcHostMsgIdNum + ") not increment of previous (" +
                 tableMsgNum + "), host = \"" + feederSourceHostStr + "\"");
              logObj.debug2(MSGCP_STR + qwMsgStr);
            }
          }
        }
        else  //feeder-data-source tracking table does not exist; create now
          feederHostMsgNumTableObj = new TagValueTable();
        feederHostMsgNumTableObj.put(       //add/update msgNum for host
                                 feederSourceHostStr,feederSrcHostMsgIdNum);
      }
      logObj.debug3(QWServer.PROGRAM_NAME + " \"" + feederNameStr +
                                                           "\" dataEvent:");
      logObj.debug3(MSGCP_STR + qwMsgStr);
              //send message out to event channel and outputters:
      serverObj.outputEventMessage(qwSvrMsgRecObj,domainNameStr,typeNameStr,
                                                     eventNameStr,qwMsgStr);
    }
    if(messageLogFileObj != null)           //if enabled then
      messageLogFileObj.println(qwMsgStr);  //send message to msg log file
    if(waitQueueFlag)
    {    //flag set; wait for add-queue to clear
//      final long startTime = System.currentTimeMillis();
      serverObj.waitForCachedArchiveAddQueueEmpty();
//      logObj.info("QWServerCallback.dataEvent:  wait-queue time = " +
//                            (System.currentTimeMillis()-startTime) + " ms");
    }
    return Long.toString(msgNum);      //return string version of msg #
  }

  /**
   * Processes a data event.
   * @param dataEventElement an 'Element' object containing the data-event
   * XML msg.
   * @param timeGenDateObj the time-generated date/time value to use
   * for the message, or null to use the current system time.
   * @param waitQueueFlag if true and the processing is successful then
   * this method will not return until the write-to-archive queue is
   * clear; if false then this method will not wait.
   * @param feederSourceHostStr the data-source host string for the
   * message.
   * @param feederSrcHostMsgIdNum the message-ID number from the data
   * source (positive value incremented after each message).
   * @return A string version of the messages number used if successful,
   * "error" if the message was rejected due to an XML or send error, or
   * "dup" if the message was rejected due to it being a duplicate
   * of a previous message.
   */
  public String dataEvent(Element dataEventElement,
                                  Date timeGenDateObj,boolean waitQueueFlag,
                      String feederSourceHostStr,long feederSrcHostMsgIdNum)
  {
    return dataEvent(null,null,null,dataEventElement,timeGenDateObj,
                   waitQueueFlag,feederSourceHostStr,feederSrcHostMsgIdNum);
  }

  /**
   * Processes a data event.
   * @param domainNameStr the domain name to use, or null for none.
   * @param typeNameStr the type name to use, or null for none.
   * @param eventNameStr the event name to use, or null for none.
   * @param dataEventElement an 'Element' object containing the data-event
   * XML msg.
   * @param timeGenDateObj the time-generated date/time value to use
   * for the message, or null to use the current system time.
   * @param feederSourceHostStr the data-source host string for the
   * message.
   * @param feederSrcHostMsgIdNum the message-ID number from the data
   * source (positive value incremented after each message).
   * @return A string version of the messages number used if successful,
   * "error" if the message was rejected due to an XML or send error, or
   * "dup" if the message was rejected due to it being a duplicate
   * of a previous message.
   */
  public String dataEvent(String domainNameStr,String typeNameStr,
           String eventNameStr,Element dataEventElement,Date timeGenDateObj,
                      String feederSourceHostStr,long feederSrcHostMsgIdNum)
  {
    return dataEvent(domainNameStr,typeNameStr,eventNameStr,
                                      dataEventElement,timeGenDateObj,false,
                                 feederSourceHostStr,feederSrcHostMsgIdNum);
  }

  /**
   * Processes a data event.
   * @param dataEventElement an 'Element' object containing the data-event
   * XML msg.
   * @param timeGenDateObj the time-generated date/time value to use
   * for the message, or null to use the current system time.
   * @param feederSourceHostStr the data-source host string for the
   * message.
   * @param feederSrcHostMsgIdNum the message-ID number from the data
   * source (positive value incremented after each message).
   * @return A string version of the messages number used if successful,
   * "error" if the message was rejected due to an XML or send error, or
   * "dup" if the message was rejected due to it being a duplicate
   * of a previous message.
   */
  public String dataEvent(Element dataEventElement,Date timeGenDateObj,
                      String feederSourceHostStr,long feederSrcHostMsgIdNum)
  {
    return dataEvent(null,null,null,dataEventElement,timeGenDateObj,false,
                                 feederSourceHostStr,feederSrcHostMsgIdNum);
  }

  /**
   * Processes a data event.  If the event data does not begin with a
   * "DataMessage" XML element then the data will be surrounded with
   * one.
   * @param domainNameStr the domain name to use, or null for none.
   * @param typeNameStr the type name to use, or null for none.
   * @param eventNameStr the event name to use, or null for none.
   * @param dataEventStr the data event message string.
   * @param waitQueueFlag if true and the processing is successful then
   * this method will not return until the write-to-archive queue is
   * clear; if false then this method will not wait.
   * @param feederSourceHostStr the data-source host string for the
   * message.
   * @param feederSrcHostMsgIdNum the message-ID number from the data
   * source (positive value incremented after each message).
   * @return A string version of the messages number used if successful,
   * "error" if the message was rejected due to an XML or send error, or
   * "dup" if the message was rejected due to it being a duplicate
   * of a previous message.
   */
  public synchronized String dataEvent(String domainNameStr,
                                     String typeNameStr,String eventNameStr,
                                  String dataEventStr,boolean waitQueueFlag,
                      String feederSourceHostStr,long feederSrcHostMsgIdNum)
  {
    try
    {
      dataEventStr = dataEventStr.trim();     //trim whitespace
      Element eObj;
      try     //attempt to parse as XML element data:
      {
        eObj = IstiXmlUtils.stringToElement(dataEventStr);
      }
      catch(Exception ex)
      {       //unable to parse as XML
        eObj = null;
      }
      final Element elementObj;
      if(eObj != null)
      {  //data parsed OK as XML; check if root element is "DataMessage"
        if(MsgTag.DATA_MESSAGE.equals(eObj.getName()))
          elementObj = eObj;      //if "DataMessage" then accept element
        else
        {     //root element not "DataMessage"; create "DataMessage" element
          elementObj = new Element(MsgTag.DATA_MESSAGE);
          elementObj.addContent(eObj);           //add elem data to element
        }
      }
      else
      {  //data not parsed OK as XML; create "DataMessage" element
        elementObj = new Element(MsgTag.DATA_MESSAGE);
        elementObj.addContent(dataEventStr);     //add str data to element
      }
         //pass event message on to server, via call-back
         // (return result string):
      return dataEvent(domainNameStr,typeNameStr,eventNameStr,elementObj,null,
                   waitQueueFlag,feederSourceHostStr,feederSrcHostMsgIdNum);
    }
    catch(Exception ex)
    {    //error processing string; log warning message
      logObj.warning("QWServerCallback:  " +
                             "Error processing XML message string:  " + ex);
      logObj.warning(MSGCP_STR + dataEventStr);
      return ERR_RET_STR;         //return error string
    }
  }

  /**
   * Processes a data event.  If the event data does not begin with a
   * "DataMessage" XML element then the data will be surrounded with
   * one.
   * @param domainNameStr the domain name to use, or null for none.
   * @param typeNameStr the type name to use, or null for none.
   * @param eventNameStr the event name to use, or null for none.
   * @param dataEventStr the data event message string.
   * @param feederSourceHostStr the data-source host string for the
   * message.
   * @param feederSrcHostMsgIdNum the message-ID number from the data
   * source (positive value incremented after each message).
   * @return A string version of the messages number used if successful,
   * "error" if the message was rejected due to an XML or send error, or
   * "dup" if the message was rejected due to it being a duplicate
   * of a previous message.
   */
  public String dataEvent(String domainNameStr,String typeNameStr,
                                    String eventNameStr,String dataEventStr,
                      String feederSourceHostStr,long feederSrcHostMsgIdNum)
  {
    return dataEvent(domainNameStr,typeNameStr,eventNameStr,dataEventStr,
                           false,feederSourceHostStr,feederSrcHostMsgIdNum);
  }

  /**
   * Processes a data event.  If the event data does not begin with a
   * "DataMessage" XML element then the data will be surrounded with
   * one.
   * @param dataEventStr the data event message string.
   * @param waitQueueFlag if true and the processing is successful then
   * this method will not return until the write-to-archive queue is
   * clear; if false then this method will not wait.
   * @param feederSourceHostStr the data-source host string for the
   * message.
   * @param feederSrcHostMsgIdNum the message-ID number from the data
   * source (positive value incremented after each message).
   * @return A string version of the messages number used if successful,
   * "error" if the message was rejected due to an XML or send error, or
   * "dup" if the message was rejected due to it being a duplicate
   * of a previous message.
   */
  public String dataEvent(String dataEventStr,boolean waitQueueFlag,
                      String feederSourceHostStr,long feederSrcHostMsgIdNum)
  {
    return dataEvent(null,null,null,dataEventStr,waitQueueFlag,
                                 feederSourceHostStr,feederSrcHostMsgIdNum);
  }

  /**
   * Processes a data event.  If the event data does not begin with a
   * "DataMessage" XML element then the data will be surrounded with
   * one.
   * @param dataEventStr the data event message string.
   * @param feederSourceHostStr the data-source host string for the
   * message.
   * @param feederSrcHostMsgIdNum the message-ID number from the data
   * source (positive value incremented after each message).
   * @return A string version of the messages number used if successful,
   * "error" if the message was rejected due to an XML or send error, or
   * "dup" if the message was rejected due to it being a duplicate
   * of a previous message.
   */
  public String dataEvent(String dataEventStr,
                      String feederSourceHostStr,long feederSrcHostMsgIdNum)
  {
    return dataEvent(null,null,null,dataEventStr,false,
                                 feederSourceHostStr,feederSrcHostMsgIdNum);
  }

  /**
   * Processs a status event via an informational message string.
   * @param msgStr the message string.
   */
  public synchronized void statusEvent(String msgStr)
  {
    logObj.info(QWServer.PROGRAM_NAME + " \"" + feederNameStr +
                                              "\" statusEvent:  " + msgStr);
  }

  /**
   * Processes an "alive" message from a server via the feeder.
   * @param msgNum message number from the feeder module.
   * @param hostNameStr name of host that message came from.
   * @param versionStr version string reported by host.
   */
  public synchronized void aliveMessage(long msgNum,String hostNameStr,
                                              String versionStr)
  {
    logObj.debug3(QWServer.PROGRAM_NAME + " \"" + feederNameStr +
        "\" aliveMsg:  " + msgNum + ", " + hostNameStr + ", " + versionStr);
  }

  /**
   * Returns the number of messages that have been received by the
   * feeder module associated with this call-back object.
   * @return The number of messages that have been received.
   */
  public long getFeederMessageCount()
  {
    synchronized(fdrMsgInfoSyncObj)
    {    //grab thread-sync lock for "feederMessage" variables
      return feederMessageCount;
    }
  }

  /**
   * Returns the total data size for the messages that have been received
   * by the feeder module associated with this call-back object.
   * @return The total data size for the messages received.
   */
  public long getFeederTotalDataSize()
  {
    synchronized(fdrMsgInfoSyncObj)
    {    //grab thread-sync lock for "feederMessage" variables
      return feederTotalDataSize;
    }
  }

  /**
   * Returns a data list string for the entries in the feeder-data-source
   * table.
   * @return A list string in the form:  "tag"=value,..., or null if the
   * table is empty.
   */
  public String getFdrTblEntriesListStr()
  {
    return (feederHostMsgNumTableObj != null &&
                             feederHostMsgNumTableObj.getNumEntries() > 0) ?
                    feederHostMsgNumTableObj.getEntriesListStr(", ") : null;
  }

  /**
   * Returns the newest message object entered into the cache or archive.
   * @return The newest 'QWServerMsgRecord' object entered into the
   * cache or archive, or null if none have been entered.
   */
  public QWServerMsgRecord getLastStoredMsgObj()
  {
    return serverObj.getLastStoredMsgObj();
  }

  /**
   * Returns the time-generated value for the last-entered message.
   * @return The time-generated value for the last-entered message, or
   * 0 if no messages have been entered.
   */
  public long getLastTimeGeneratedVal()
  {
    return serverObj.getLastTimeGeneratedVal();
  }

  /**
   * Returns a vector containing all of the message objects in the
   * server's cache.
   * Each message object will be of type 'QWServerMsgRecord'.
   * @return A new vector containing all of the message objects in the
   * server's cache.
   */
  public Vector getAllServerCacheMsgs()
  {
    return serverObj.getAllCacheMessages();
  }

  /**
   * Returns the server ID name string for this QuakeWatch server.
   * @return The server ID name string.
   */
  public String getServerIdName()
  {
    return serverObj.getServerIdName();
  }

  /**
   * Returns the server host address string for this QuakeWatch server.
   * @return The server host address string.
   */
  public String getServerHostAddress()
  {
    return serverObj.getServerHostAddress();
  }

  /**
   * Returns the ID string for the 'QWAcceptorImpl' object for this server.
   * @return The ID string for the 'QWAcceptorImpl' object for this server.
   */
  public String getQWAcceptorImplIDString()
  {
    return serverObj.getQWAcceptorImplIDString();
  }

  /**
   * Terminates the server.
   * @param val exit code to be returned to the operating system.
   * @param errMsgStr error message string, or null for none.
   */
  public void terminateServer(int val,String errMsgStr)
  {
    if(errMsgStr != null)
      logObj.error("Terminating server:  " + errMsgStr);
    serverObj.terminateProgram(val);
  }
}
