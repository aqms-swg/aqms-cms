//CubeFormatParser.java:  Parses CUBE-format messages into a JDOM/XML
//                        tree of elements.  The generated XML conforms
//                        to the ISTI 'QWmessage' format.
//
//  10/7/2002 -- [ET]
//  6/19/2003 -- [KF]  Added Trump event message-type.
//  8/19/2003 -- [ET]  Added detection of "LocationMethod=Q" indicator
//                     (for "probable quarry explosion") and translation
//                     into "Quarry=Y" attribute.
//  2/25/2004 -- [ET]  Modified to make date-format-object usage thread
//                     safe; modified parsing of "link" ("LI") messages
//                     to also accept whitespace separators other than
//                     spaces (i.e. tab).
//   9/8/2004 -- [KF]  Added tag names and values for units.
//  5/30/2006 -- [ET]  Added ShakeMap product-translation codes for Global
//                     and for ShakeMaps from other networks.
//   8/7/2006 -- [ET]  Modified to extend 'CubeParserSupport' and
//                     implement 'MsgParserIntf'; made version code
//                     for delete-earthquake message be a maximum of
//                     one character; added support for version code
//                     and comment data in "Trump" message.
//  8/15/2007 -- [ET]  Modified to detect and pass through XML-format
//                     messages; moved  'parseMessageString()' method
//                     to 'CubeParserSupport' base class; added
//                     'parseCubeMsgStringData()' method; modified
//                     parsing of date to allow spaces.
//  9/21/2007 -- [ET]  Fixed bugs in 'parseCubeMsgStringData()' method
//                     where incomplete CUBE-field items could result in
//                     'StringIndexOutOfBoundsException' being thrown.
//   1/5/2009 -- [ET]  Fixed bug where version code on delete message was
//                     ignored if message had no text comment.
//  4/15/2010 -- [ET]  Modified to set Tsunami Product 'Type' value to
//                     "Tsunami" instead of "LinkURL(Tsunami_XX)";
//                     modified to use 'decValFormatObj' from base class.
//
//

package com.isti.quakewatch.server;

import java.util.Date;
import java.text.ParseException;
import org.jdom.Element;
import org.jdom.Attribute;
import org.jdom.Text;
import com.isti.util.UtilFns;
import com.isti.quakewatch.common.MsgTag;

/**
 * Class CubeFormatParser parses CUBE-format messages into a JDOM/XML
 * tree of elements.  The generated XML conforms to the ISTI 'QWmessage'
 * format.
 */
public class CubeFormatParser extends CubeParserSupport
                                                    implements MsgParserIntf
{

  /**
   * Parses CUBE-format message data into a JDOM/XML tree of elements.
   * @param messageString the message string to be parsed.
   * @param timeReceivedStr current time (as an XML-format time string).
   * @param actionAttribObj attrib whose value will be set by this method.
   * @return The resulting Element object, or null if an error occured
   * (in which case the associated error message may be fetched via the
   * 'getErrorMessage()' method).
   */
  protected Element parseCubeMsgStringData(String messageString,
                          String timeReceivedStr, Attribute actionAttribObj)
  {
    final int msgStrLen;
    if((msgStrLen=messageString.length()) < 12)
    {    //too few character; set error message
      setErrorMessage("Not enough characters in message");
      return null;
    }

         //identify message type:
    final int msgTypeIndex;
    if((msgTypeIndex=msgStringsVec.indexOf(
                                         messageString.substring(0,2))) < 0)
    {    //message ID chars not matched; set error message
      setErrorMessage("Unexpected message type (\"" +
                                      messageString.substring(0,2) + "\")");
      return null;
    }

         //save identification "number" for event:
    final String eventIdNumStr = messageString.substring(2,10).trim();

         //save the 2-character source code string:
    final String sourceCodeStr = messageString.substring(10,12).trim();

         //create Identifier element:
    final Element identifierElem = new Element(MsgTag.IDENTIFIER);
                        //add event-ID-key attribute (data source+ID#)
                        // (make data-source-code part lower case):
    identifierElem.setAttribute(MsgTag.EVENT_ID_KEY,
                               (sourceCodeStr.toLowerCase()+eventIdNumStr));
                        //add data-source attribute:
    identifierElem.setAttribute(MsgTag.DATA_SOURCE,sourceCodeStr);

    Element eventElem, productElem, returnElem = null;
    String str;
         //process rest of data based on message type:
    switch(msgTypeIndex)
    {
      case EQUAKE_MSGIDX:         //Earthquake
        if(msgStrLen < 43)
        {     //not enough characters for lat/lon values; set error message
          setErrorMessage("Not enough characters in Earthquake message");
          return null;
        }
              //add Version attribute to Identifier element:
        identifierElem.setAttribute(MsgTag.VERSION,
                                            messageString.substring(12,13));
              //process date string:
        String dateStr = messageString.substring(13,28);
        final Date dateObj;            //date/time for event
        try        //parse date/time (add "00" to end to make milliseconds
        {          // and replace any spaces with zeroes):
          synchronized(cubeDateFormatterObj)
          {    //only allow one thread at a time to use date-format object
            dateObj = cubeDateFormatterObj.parse(
                                           dateStr.replace(' ','0') + "00");
          }
        }
        catch(ParseException ex)
        {                    //error parsing date/time; set message
          setErrorMessage("Unable to parse date/time (\"" + dateStr + "\")");
          return null;
        }
              //process latitude value:
        final Double latObj;
        if((latObj=parseCubicDouble(messageString.substring(28,35),
                                               10000.0,"latitude")) == null)
        {     //error parsing numeric string
          return null;
        }
              //process longitude value:
        final Double lonObj;
        if((lonObj=parseCubicDouble(messageString.substring(35,43),
                                               10000.0,"longitude")) == null)
        {     //error parsing numeric string
          return null;
        }
              //create Event element:
        eventElem = new Element(MsgTag.EVENT);
                   //set Type to "Earthquake":
        eventElem.setAttribute(MsgTag.TYPE,MsgTag.EARTHQUAKE);
                   //set MsgTypeCode to "E ":
        eventElem.setAttribute(MsgTag.MSG_TYPE_CODE,MsgTag.EQUAKE_MSGSTR);
                   //set event time:
        synchronized(xmlDateFormatterObj)
        {     //only allow one thread at a time to use date-format object
          eventElem.setAttribute(MsgTag.TIME,
                                       xmlDateFormatterObj.format(dateObj));
        }
                   //set latitude value:
        eventElem.setAttribute(MsgTag.LATITUDE,
                                            decValFormatObj.format(latObj));
                   //set longitude value:
        eventElem.setAttribute(MsgTag.LONGITUDE,
                                            decValFormatObj.format(lonObj));
        eventElem.setAttribute(MsgTag.LAT_LON_UNITS,MsgTag.DEGREES);
              //add Identifier element as child to Event element:
        eventElem.addContent(identifierElem);
              //setup to return Event element:
        returnElem = eventElem;
              //set action attribute to "Update":
        actionAttribObj.setValue(MsgTag.UPDATE);

              //process depth value:
        if((str=getMsgSubstring(messageString,msgStrLen,43,47)) == null)
          break;        //if no more data then exit case
        Double dObj;
        if(str.trim().length() > 0 && !str.startsWith(ASTERISK_STR))
        {     //item contains data
          if((dObj=parseCubicDouble(str,10.0,"depth")) == null)
            return null;
              //enter value as Depth attribute of Event element:
          eventElem.setAttribute(MsgTag.DEPTH,decValFormatObj.format(dObj));
          eventElem.setAttribute(MsgTag.DEPTH_UNITS,MsgTag.KM);
        }
              //process magnitude value:
        if((str=getMsgSubstring(messageString,msgStrLen,47,49)) == null)
          break;        //if no more data then exit case
        Element magnitudeElem = null;  //holder for Magnitude element
        if(str.trim().length() > 0 && !str.startsWith(ASTERISK_STR))
        {     //item contains data
          if((dObj=parseCubicDouble(str,10.0,"magnitude")) == null)
            return null;
              //create Magnitude element:
          magnitudeElem = new Element(MsgTag.MAGNITUDE);
              //enter as Value attribute of Magnitude element:
          magnitudeElem.setAttribute(MsgTag.VALUE,
                                              decValFormatObj.format(dObj));
              //add Magnitude element as child to Event element:
          eventElem.addContent(magnitudeElem);
        }
              //process "# of stations" value:
        if((str=getMsgSubstring(messageString,msgStrLen,49,52)) == null)
          break;        //if no more data then exit case
        Integer iObj;
        if(str.trim().length() > 0 && !str.startsWith(ASTERISK_STR))
        {     //item contains data
          if((iObj=parseCubicInteger(str,"# of stations")) == null)
            return null;
              //enter value as NumStations attribute of Event element:
          eventElem.setAttribute(MsgTag.NUM_STATIONS,iObj.toString());
        }
              //process "# of phases" value:
        if((str=getMsgSubstring(messageString,msgStrLen,52,55)) == null)
          break;        //if no more data then exit case
        if(str.trim().length() > 0 && !str.startsWith(ASTERISK_STR))
        {     //item contains data
          if((iObj=parseCubicInteger(str,"# of phases")) == null)
            return null;
              //enter value as NumPhases attribute of Event element:
          eventElem.setAttribute(MsgTag.NUM_PHASES,iObj.toString());
        }
              //process "minimum distance" value:
        if((str=getMsgSubstring(messageString,msgStrLen,55,59)) == null)
          break;        //if no more data then exit case
        if(str.trim().length() > 0 && !str.startsWith(ASTERISK_STR))
        {     //item contains data
          if((dObj=parseCubicDouble(str,10.0,"minimum distance")) == null)
            return null;
              //enter value as MinDistance attribute of Event element:
          eventElem.setAttribute(MsgTag.MIN_DISTANCE,
                                              decValFormatObj.format(dObj));
          eventElem.setAttribute(MsgTag.MIN_DISTANCE_UNITS,MsgTag.KM);
        }
              //process "RMS time error" value:
        if((str=getMsgSubstring(messageString,msgStrLen,59,63)) == null)
          break;        //if no more data then exit case
        if(str.trim().length() > 0 && !str.startsWith(ASTERISK_STR))
        {     //item contains data
          if((dObj=parseCubicDouble(str,100.0,"RMS time error")) == null)
            return null;
              //enter value as RMSTimeError attribute of Event element:
          eventElem.setAttribute(MsgTag.RMS_TIME_ERROR,
                                              decValFormatObj.format(dObj));
          eventElem.setAttribute(MsgTag.RMS_TIME_ERROR_UNITS,MsgTag.SECONDS);
        }
              //process "horizontal error" value:
        if((str=getMsgSubstring(messageString,msgStrLen,63,67)) == null)
          break;        //if no more data then exit case
        if(str.trim().length() > 0 && !str.startsWith(ASTERISK_STR))
        {     //item contains data
          if((dObj=parseCubicDouble(str,10.0,"horizontal error")) == null)
            return null;
              //enter value as HorizontalError attribute of Event element:
          eventElem.setAttribute(MsgTag.HORIZONTAL_ERROR,
                                              decValFormatObj.format(dObj));
          eventElem.setAttribute(MsgTag.HORIZONTAL_ERROR_UNITS,MsgTag.KM);
        }
              //process "vertical error" value:
        if((str=getMsgSubstring(messageString,msgStrLen,67,71)) == null)
          break;        //if no more data then exit case
        if(str.trim().length() > 0 && !str.startsWith(ASTERISK_STR))
        {     //item contains data
          if((dObj=parseCubicDouble(str,10.0,"vertical error")) == null)
            return null;
              //enter value as VerticalError attribute of Event element:
          eventElem.setAttribute(MsgTag.VERTICAL_ERROR,
                                              decValFormatObj.format(dObj));
          eventElem.setAttribute(MsgTag.VERTICAL_ERROR_UNITS,MsgTag.KM);
        }
              //process "azimuthal gap" value:
        if((str=getMsgSubstring(messageString,msgStrLen,71,73)) == null)
          break;        //if no more data then exit case
        if(str.trim().length() > 0 && !str.startsWith(ASTERISK_STR))
        {     //item contains data; (convert % of circle to degrees)
          if((dObj=parseCubicDouble(str,(1/3.6),"azimuthal gap")) == null)
            return null;
              //enter value as AzimuthalGap attribute of Event element:
          eventElem.setAttribute(MsgTag.AZIMUTHAL_GAP,
                                              decValFormatObj.format(dObj));
          eventElem.setAttribute(MsgTag.AZIMUTHAL_GAP_UNITS,MsgTag.DEGREES);
        }
        if(magnitudeElem != null)
        {     //magnitude value was parsed previously
              //process "magnitude type" code:
          if((str=getMsgSubstring(messageString,msgStrLen,73,74)) == null)
            break;      //if no more data then exit case
          if(str.trim().length() > 0)
          {   //item contains data
                   //enter as Type attribute of Magnitude element:
            magnitudeElem.setAttribute(MsgTag.TYPE,str);
          }
              //process "# of mag stations" value:
          if((str=getMsgSubstring(messageString,msgStrLen,74,76)) == null)
            break;      //if no more data then exit case
          if(str.trim().length() > 0 && !str.startsWith(ASTERISK_STR))
          {   //item contains data
            if((iObj=parseCubicInteger(str,"# of mag stations")) == null)
              return null;
                   //enter as NumStations attribute of Magnitude element:
            magnitudeElem.setAttribute(MsgTag.NUM_STATIONS,iObj.toString());
          }
              //process "magnitude error" value:
          if((str=getMsgSubstring(messageString,msgStrLen,76,78)) == null)
            break;        //if no more data then exit case
          if(str.trim().length() > 0 && !str.startsWith(ASTERISK_STR))
          {   //item contains data
            if((dObj=parseCubicDouble(str,10.0,"magnitude error")) == null)
              return null;
                   //enter as MagError attribute of Magnitude element:
            magnitudeElem.setAttribute(MsgTag.MAG_ERROR,
                                              decValFormatObj.format(dObj));
          }
        }
              //process "location method" code:
        if((str=getMsgSubstring(messageString,msgStrLen,78,79)) == null)
          break;        //if no more data then exit case
        if(str.trim().length() > 0)
        {     //item contains data
              //enter value as location method attribute of Event element:
          eventElem.setAttribute(MsgTag.LOCATION_METHOD,str);
              //set Verified attribute based on case of code:
          eventElem.setAttribute(MsgTag.VERIFIED,
               (Character.isLowerCase(str.charAt(0))?MsgTag.YES:MsgTag.NO));
              //if location method is "Q" (for "probable quarry explosion")
              // then add Quarry=Y attribute:
          if(str.equalsIgnoreCase(LOCMETH_QUARRY_STR))
            eventElem.setAttribute(MsgTag.QUARRY,MsgTag.YES);
        }
              //process check character:
        if((str=getMsgSubstring(messageString,msgStrLen,79,80)) == null)
          break;        //if no more data then exit case
        if(str.charAt(0) !=
                          calcMenloCheckChar(messageString.substring(0,79)))
        {     //check character mismatch
          final char rCh = str.charAt(0);
          final char cCh = calcMenloCheckChar(messageString.substring(0,79));
          setErrorMessage("Mismatch in check character for message, " +
                            "recv'd=" + (int)rCh + "(" + rCh + ") calc'd=" +
                                                (int)cCh + "(" + cCh + ")");
          return null;
        }
        break;

      case DELQK_MSGIDX:     //Delete Earthquake
              //create Event element:
        eventElem = new Element(MsgTag.EVENT);
                   //set Type to "Earthquake":
        eventElem.setAttribute(MsgTag.TYPE,MsgTag.EARTHQUAKE);
                   //set MsgTypeCode to "DE":
        eventElem.setAttribute(MsgTag.MSG_TYPE_CODE,MsgTag.DELQK_MSGSTR);
        if(msgStrLen > 12)
        {     //msg has Version code; add as attribute to Identifier elem:
          identifierElem.setAttribute(MsgTag.VERSION,
                                            messageString.substring(12,13));
              //if exists then put in comment data
          if(msgStrLen > 14 &&
                      (str=messageString.substring(14).trim()).length() > 0)
          {
            eventElem.setAttribute(MsgTag.COMMENT,str);
          }
        }
              //add Identifier element as child to Event element:
        eventElem.addContent(identifierElem);
              //set action attribute to "Delete":
        actionAttribObj.setValue(MsgTag.DELETE);
              //setup to return Event element:
        returnElem = eventElem;
        break;

      case TEXT_MSGIDX:      //Text Comment
        if(msgStrLen < 14)
        {     //not enough characters for version chars; set error message
          setErrorMessage("Not enough characters in Text Comment message");
          return null;
        }
              //add Version attribute to Identifier element:
        identifierElem.setAttribute(MsgTag.VERSION,
                                     messageString.substring(12,14).trim());
              //create Product element:
        productElem = new Element(MsgTag.PRODUCT);
                   //set Type to "TextComment":
        productElem.setAttribute(MsgTag.TYPE,MsgTag.TEXT_COMMENT);
                   //set MsgTypeCode to "TX":
        productElem.setAttribute(MsgTag.MSG_TYPE_CODE,MsgTag.TEXT_MSGSTR);
        if((str=messageString.substring(14).trim()).length() > 0)
        {     //text data exists; put it in as Value attribute
          productElem.setAttribute(MsgTag.VALUE,Text.normalizeString(str));
              //set action attribute to "Update":
          actionAttribObj.setValue(MsgTag.UPDATE);
        }
        else  //no text data; set action attribute to "Delete"
          actionAttribObj.setValue(MsgTag.DELETE);
              //add Identifier element as child to Product element:
        productElem.addContent(identifierElem);
              //setup to return Product element:
        returnElem = productElem;
        break;

      case LINK_MSGIDX:      //Link to Add-on
        if(msgStrLen < 15)
        {     //not enough characters for addon-type value; set error msg
          setErrorMessage("Not enough characters in Link to Addon message");
          return null;
        }
              //add Version attribute to Identifier element:
        identifierElem.setAttribute(MsgTag.VERSION,
                                     messageString.substring(12,14).trim());
              //create Product element:
        productElem = new Element(MsgTag.PRODUCT);
              //process "type url comment" string in addon message:
        int ePos, sPos = 14;
              //scan through any leading whitespace:
        while(Character.isWhitespace(messageString.charAt(sPos)) &&
                                                        ++sPos < msgStrLen);
              //find next whitespace separator after data:
        if((ePos=UtilFns.indexOfWhitespace(messageString,sPos)) < 0)
          ePos = msgStrLen;       //if not found then use end of string pos
                   //get addon-type code string:
        final String prodTypeStr = messageString.substring(sPos,ePos).trim();
                   //translate CUBE addon-type code to QW product
                   // code, or "LinkURL(x)" if not matched:
        if((str=getProdTypeXlatEntry(prodTypeStr)) == null)
          str = MsgTag.LINK_URL + '(' + prodTypeStr + ')';
        productElem.setAttribute(MsgTag.TYPE,str);    //set product type
                   //set MsgTypeCode to "LI":
        productElem.setAttribute(MsgTag.MSG_TYPE_CODE,MsgTag.LINK_MSGSTR);
                   //set ProdTypeCode to addon-type value:
        productElem.setAttribute(MsgTag.PROD_TYPE_CODE,prodTypeStr);
        String actionStr = MsgTag.UPDATE;   //setup default Action value
        if((sPos=ePos+1) < msgStrLen)
        {     //more data in message string
                   //scan through any trailing whitespace:
          while(Character.isWhitespace(messageString.charAt(sPos)) &&
                                                        ++sPos < msgStrLen);
                   //find next whitespace separator after data:
          if((ePos=UtilFns.indexOfWhitespace(messageString,sPos)) < 0)
            ePos = msgStrLen;     //if not found then use end of string pos
                   //set Value to addon-URL value:
          productElem.setAttribute(MsgTag.VALUE,
                                 messageString.substring(sPos,ePos).trim());
          if((sPos=ePos+1) < msgStrLen)
          {   //more data in message string
            if((str=messageString.substring(sPos).trim()).length() > 0)
            {     //description text exists
              if(str.equalsIgnoreCase("delete")) //if "delete" then
                actionStr = MsgTag.DELETE;       //setup delete Action
              else
              {    //if not "delete" then put in as Comment attribute
                productElem.setAttribute(   // (take out any CR/LF chars)
                                  MsgTag.COMMENT,Text.normalizeString(str));
                if(prodTypeStr.equalsIgnoreCase(TYPE_FR_STR))
                {  //CUBE product type code is "fr"
                  if(str.indexOf(RTMECH_KEYSTR) >= 0)
                  {     //comment makes it a real-time focal mechanism URL
                    productElem.setAttribute(
                                      MsgTag.TYPE,MsgTag.REALTIME_MECH_URL);
                  }
                  else if(str.indexOf(FEELIT_KEYSTR) >= 0)
                  {     //comment makes it a "Did you feel it?" URL
                    productElem.setAttribute(
                                          MsgTag.TYPE,MsgTag.DIDYOU_FEELIT);
                  }
                }
              }
            }
          }
        }
              //set Action attribute ("Update" or "Delete"):
        actionAttribObj.setValue(actionStr);
              //add Identifier element as child to Product element:
        productElem.addContent(identifierElem);
              //setup to return Product element:
        returnElem = productElem;
        break;

      case TRUMP_MSGIDX:  //Trump event
              //create Event element:
        eventElem = new Element(MsgTag.EVENT);
              //set Type to "Earthquake":
        eventElem.setAttribute(MsgTag.TYPE,MsgTag.EARTHQUAKE);
              //set MsgTypeCode to "TR":
        eventElem.setAttribute(MsgTag.MSG_TYPE_CODE,MsgTag.TRUMP_MSGSTR);
        if(msgStrLen > 12)
        {     //version data exists; add Version attrib to Identifier elem
          identifierElem.setAttribute(MsgTag.VERSION,
                                            messageString.substring(12,13));
          if(msgStrLen > 14 &&
                      (str=messageString.substring(14).trim()).length() > 0)
          {   //comment data exists; add Comment attribute to Event element
            eventElem.setAttribute(MsgTag.COMMENT,str);
          }
        }
              //add Identifier element as child to Event element:
        eventElem.addContent(identifierElem);
              //set action attribute to "Trump":
        actionAttribObj.setValue(MsgTag.TRUMP);
              //setup to return Event element:
        returnElem = eventElem;
        break;

      default:
        setErrorMessage("Unexpected message type (\"" +
                                      messageString.substring(0,2) + "\")");
        return null;
    }

    return returnElem;            //return generated elements
  }
}
