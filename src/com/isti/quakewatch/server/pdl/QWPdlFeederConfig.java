package com.isti.quakewatch.server.pdl;

import java.util.logging.Level;

import com.isti.util.CfgPropItem;
import com.isti.util.CfgProperties;
import com.isti.util.TimeConstants;
import com.isti.util.UtilFns;

/** Configuration properties for the PDL feeder. */
public class QWPdlFeederConfig implements PdlConstants, TimeConstants {
	private String errorMessage = UtilFns.EMPTY_STRING;
	private Level pdlReceiverLogLevelObj = Level.INFO;
	public final CfgProperties cfgPropsObj = new CfgProperties();
	/** PDL server host. */
	public final CfgPropItem pdlServerHostProp = cfgPropsObj.add("pdlServerHost", "prod01-pdl01.cr.usgs.gov");
	/** PDL server port number. */
	public final CfgPropItem pdlServerPortProp = cfgPropsObj.add("pdlServerPort", Integer.valueOf(39977));
	/** PDL alternate servers. */
	public final CfgPropItem pdlAlternateServersProp = cfgPropsObj.add("pdlAlternateServers",
			"prod02-pdl01.cr.usgs.gov:39977");
	/** PDL tracking file. */
	public final CfgPropItem pdlTrackingFileProp = cfgPropsObj.add("pdlTrackingFile",
			"pdldata/receiver_pdl_tracking.dat");
	/** PDL maximum server event age in days. */
	public final CfgPropItem pdlMaxServerEventAgeDaysProp = cfgPropsObj.add("pdlMaxServerEventAgeDays",
			Double.valueOf(3.0));
	/** PDL receiver storage directory. */
	public final CfgPropItem pdlReceiverStorageProp = cfgPropsObj.add("pdlReceiverStorage", "pdldata/receiver_storage");
	/** PDL receiver storage index file. */
	public final CfgPropItem pdlReceiverIndexProp = cfgPropsObj.add("pdlReceiverIndex", "pdldata/receiver_index.db");
	/** PDL listener storage index file. */
	public final CfgPropItem pdlListenerIndexProp = cfgPropsObj.add("pdlListenerIndex", "pdldata/listener_index.db");
	/** PDL expired-products cleanup interval; 900000ms = 15min. */
	public final CfgPropItem pdlCleanupIntervalProp = cfgPropsObj.add("pdlCleanupInterval", Long.valueOf(900000));
	/** PDL age that products expire; 900000ms = 15min. */
	public final CfgPropItem pdlStorageAgeProp = cfgPropsObj.add("pdlStorageAge", Long.valueOf(900000));
	/** PDL number of delivery attempts to make. */
	public final CfgPropItem pdlMaxTriesProp = cfgPropsObj.add("pdlMaxTries", Integer.valueOf(10));
	/** PDL number of ms to wait between delivery attempts that fail. */
	public final CfgPropItem pdlRetryDelayProp = cfgPropsObj.add("pdlRetryDelay", Long.valueOf(30000));
	/** Format string used to build DYFI URLs. */
	public final CfgPropItem dyfiUrlFormatStrProp = cfgPropsObj.add("dyfiUrlFormatStr", DEFAULT_DYFI_URL_FORMAT_STR);
  /** Format string used to build losspager URLs. */
  public final CfgPropItem losspagerUrlFormatStrProp = cfgPropsObj.add("losspagerUrlFormatStr", DEFAULT_LOSSPAGER_URL_FORMAT_STR);
  /** Format string used to build moment-tensor URLs. */
  public final CfgPropItem momentTensorUrlFormatStrProp = cfgPropsObj.add("momentTensorUrlFormatStr", DEFAULT_MOMENTTENSOR_URL_FORMAT_STR);
	/** Format string used to build ShakeMap URLs; ""=default. */
	public final CfgPropItem shakemapUrlFormatStrProp = cfgPropsObj.add("shakemapUrlFormatStr", DEFAULT_SHAKEMAP_URL_FORMAT_STR);
	/** PDL max byte content length */
	public final CfgPropItem pdlMaxByteContentLengthProp = cfgPropsObj.add("pdlMaxByteContentLength",
			Integer.valueOf(16384));
  /** PDL indexer storage directory. */
  public final CfgPropItem pdlIndexerStorageProp = cfgPropsObj.add("pdlIndexerStorage", "pdldata/indexer_storage");
	/** PDL indexer database */
	public final CfgPropItem pdlIndexerDatabaseProp = cfgPropsObj.add("pdlIndexerDatabase", "pdldata/productIndex.db");
	public final CfgPropItem pdlFilterWarnProp = cfgPropsObj.add("pdlFilterWarn", UtilFns.EMPTY_STRING);
  /** PDL number of ms to wait between restarts or 0 to disable restarts. */
  public final CfgPropItem pdlRestartDelayProp = cfgPropsObj.add("pdlRestartDelay", Long.valueOf(30 * MS_PER_SECOND));
  public final CfgPropItem pdlShutdownWarnProp = cfgPropsObj.add("pdlShutdownWarn", UtilFns.EMPTY_STRING);
  public final CfgPropItem pdlDataTimeoutProp = cfgPropsObj.add("pdlDataTimeout", 0L);

	/**
	 * PDL-receiver log-file level; one of: OFF, SEVERE, WARNING, INFO, CONFIG,
	 * FINE, FINER, FINEST, ALL.
	 */
	public final CfgPropItem pdlReceiverLogLevelProp = cfgPropsObj.add("pdlReceiverLogLevel",
			pdlReceiverLogLevelObj.toString());

	/**
	 * Reads a set of property items from the given input string. Each item name
	 * read must match an existing property item name in the current table, and
	 * the read item's value must be a valid ASCII representation of the item's
	 * type. The loading continues even after an error has been detected; the
	 * text error message corresponding to the first error detected is saved.
	 * 
	 * @param inStr
	 *            the string containing the data to read.
	 * @return true if successful, false if any errors are detected, in which
	 *         case a text error message is generated that may be fetched via
	 *         the 'getErrorMessage()' method.
	 */
	public boolean load(String settingsStr) {
		boolean successful = cfgPropsObj.load(settingsStr);
		if (successful) {
			try {
				pdlReceiverLogLevelObj = Level.parse(pdlReceiverLogLevelProp.stringValue());
			} catch (Exception ex) {
				errorMessage = "Illegal value for '" + pdlReceiverLogLevelProp.getName() + "' (\""
						+ pdlReceiverLogLevelProp.stringValue() + "\"), must be one of:  OFF, SEVERE, WARNING, "
						+ "INFO, CONFIG, FINE, FINER, FINEST, ALL";
			}
		} else {
			errorMessage = cfgPropsObj.getErrorMessage();
		}
		return successful;
	}

	/**
	 * @return the error message string
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @return the PDL-receiver log-file level.
	 */
	public Level getPdlReceiverLogLevel() {
		return pdlReceiverLogLevelObj;
	}
}
