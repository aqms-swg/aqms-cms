package com.isti.quakewatch.server.pdl;

public interface PdlClientCallback {
	/**
	 * @return the feeder name.
	 */
	public String getFeederName();

	/**
	 * @return the feeder module name.
	 */
	public String getFeederModuleName();

	/**
	 * Process the record.
	 * 
	 * @param record
	 *            the record.
	 */
	public void processRecord(QWConvMsgRecord record);
}
