package com.isti.quakewatch.server.pdl;

import com.isti.quakewatch.common.EventLinkCreator;

import gov.usgs.earthquake.product.Product;
import gov.usgs.earthquake.product.ProductId;

public class PdlLinkCreator extends EventLinkCreator
{
  /**
   * Get the product link.
   * 
   * @param str        the format string.
   * @param productObj the product.
   * @return the product link or null if none.
   */
  public static String getProductLink(String str, Product productObj)
  {
    ProductId productIdObj = productObj.getId();
    return getProductLink(str, productIdObj.getCode(),
        productIdObj.getSource(), productIdObj.getType(),
        productIdObj.getUpdateTime().getTime(), productObj.getEventSourceCode(),
        productObj.getEventSource());
  }
}
