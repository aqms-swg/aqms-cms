package com.isti.quakewatch.server.pdl;

import com.isti.util.LogFile;

import gov.usgs.earthquake.indexer.Indexer;
import gov.usgs.earthquake.indexer.JDBCProductIndex;
import gov.usgs.earthquake.indexer.ProductIndex;
import gov.usgs.util.Config;

public class PdlIndexer extends Indexer
{
  public PdlIndexer(LogFile logObj, PdlConvertProduct pdlConvertProduct,
      String pdlIndexerDatabase) throws Exception
  {
    super();
    if (pdlIndexerDatabase != null && !pdlIndexerDatabase.isEmpty())
    {
      String[] sa = pdlIndexerDatabase.split(" +");
      if (sa.length > 0)
      {
        Config config = new Config();
        config.setProperty("indexfile", sa[0]);
        if (sa.length > 1)
        {
          config.setProperty("driver", sa[1]);
          if (sa.length > 2)
          {
            // not used for SQLite
            config.setProperty("url", sa[2]);
          }
        }
        ProductIndex productIndex = getProductIndex();
        if (!(productIndex instanceof JDBCProductIndex))
        {
          productIndex = new JDBCProductIndex();
          setProductIndex(productIndex);
        }
        productIndex.configure(config);
      }
    }
    addListener(new PdlIndexerListener(logObj, pdlConvertProduct));
  }
}
