package com.isti.quakewatch.server.pdl;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import gov.usgs.earthquake.distribution.FileProductStorage;
import gov.usgs.earthquake.distribution.ProductStorage;
import gov.usgs.earthquake.product.Product;
import gov.usgs.earthquake.product.ProductId;
import gov.usgs.earthquake.product.io.BinaryProductHandler;
import gov.usgs.earthquake.product.io.BinaryProductSource;
import gov.usgs.earthquake.product.io.DirectoryProductHandler;
import gov.usgs.earthquake.product.io.DirectoryProductSource;
import gov.usgs.earthquake.product.io.ProductHandler;
import gov.usgs.earthquake.product.io.ProductSource;
import gov.usgs.earthquake.product.io.XmlProductHandler;
import gov.usgs.earthquake.product.io.XmlProductSource;
import gov.usgs.earthquake.product.io.ZipProductHandler;
import gov.usgs.earthquake.product.io.ZipProductSource;
import gov.usgs.util.Config;
import gov.usgs.util.StreamUtils;

public class ProductStorageUtil {
	/** File extensions */
	private static final Map<String, String> FILE_EXTENSIONS;
	/** Format for Binary. */
	public static final String FORMAT_BINARY = "bin";
	/** Format default */
	public static final String FORMAT_DEFAULT;
	/** Format for Directory. */
	public static final String FORMAT_DIRECTORY = "dir";
	/** Format for XML. */
	public static final String FORMAT_XML = "xml";
	/** Format for ZIP. */
	public static final String FORMAT_ZIP = "zip";
	/** Property name for the format. */
	public static final String PROPERTY_NAME_FORMAT = "format";
	/** Property name for the product path separator. */
	public static final String PROPERTY_NAME_PRODUCT_PATH_SEPARATOR = "productPathSeparator";
	static {
		final HashMap<String, String> map = new HashMap<String, String>(3);
		map.put(FORMAT_BINARY, ".bin");
		map.put(FORMAT_XML, ".xml");
		map.put(FORMAT_ZIP, ".zip");
		FILE_EXTENSIONS = Collections.unmodifiableMap(map);
		FORMAT_DEFAULT = FORMAT_BINARY;
	}

	/**
	 * Create the product handler.
	 * 
	 * @param format
	 *            the format.
	 * @param file
	 *            the file.
	 * @return the product handler.
	 * @throws IOException
	 *             if an error occurs.
	 */
	public static ProductHandler createProductHandler(String format, File file) throws IOException {
		ProductHandler productHandler;
		if (format.startsWith(FORMAT_BINARY)) {
			productHandler = new BinaryProductHandler(StreamUtils.getOutputStream(file));
		} else if (format.startsWith(FORMAT_XML)) {
			productHandler = new XmlProductHandler(StreamUtils.getOutputStream(file));
		} else if (format.startsWith(FORMAT_ZIP)) {
			productHandler = new ZipProductHandler(StreamUtils.getOutputStream(file));
		} else {
			productHandler = new DirectoryProductHandler(file);
		}
		return productHandler;
	}

	/**
	 * Create the product source.
	 * 
	 * @param format
	 *            the format.
	 * @param file
	 *            the file.
	 * @return the product source.
	 * @throws IOException
	 *             if an error occurs.
	 */
	public static ProductSource createProductSource(String format, File file) throws IOException {
		ProductSource productSource;
		if (format.startsWith(FORMAT_XML)) {
			productSource = new XmlProductSource(StreamUtils.getInputStream(file));
		} else if (format.startsWith(FORMAT_ZIP)) {
			productSource = new ZipProductSource(file);
		} else if (format.startsWith(FORMAT_DEFAULT)) {
			productSource = new BinaryProductSource(StreamUtils.getInputStream(file));
		} else {
			productSource = new DirectoryProductSource(file);
		}
		return productSource;
	}

	/**
	 * Create a new product storage.
	 * 
	 * @param baseDirectory
	 *            the base directory for all products being stored.
	 * @return the product storage.
	 */
	public static ProductStorage createProductStorage(final File baseDirectory) {
		return new FileProductStorage(baseDirectory) {
			private String format = FORMAT_BINARY;
			private String productPathSeparator = "_";

			@Override
			public void configure(Config config) throws Exception {
				super.configure(config);
				format = config.getProperty(PROPERTY_NAME_FORMAT, format);
				productPathSeparator = config.getProperty(PROPERTY_NAME_PRODUCT_PATH_SEPARATOR, productPathSeparator);
			}

			@Override
			protected ProductHandler getProductHandlerFormat(final File file) throws Exception {
				return createProductHandler(format, file);
			}

			@Override
			public String getProductPath(final ProductId id) {
				final String productPath;
				final String fileExt = getFileExt(format);
				if (fileExt != null) {
					StringBuilder buf = new StringBuilder();
					buf.append(id.getSource());
					buf.append(productPathSeparator);
					buf.append(id.getType());
					buf.append(productPathSeparator);
					buf.append(id.getCode());
					buf.append(productPathSeparator);
					buf.append(id.getUpdateTime().getTime());
					buf.append(fileExt);
					productPath = buf.toString();
				} else {
					productPath = super.getProductPath(id);
				}
				return productPath;
			}

			@Override
			protected ProductSource getProductSourceFormat(final File file) throws Exception {
				return createProductSource(format, file);
			}

			@Override
			public ProductId storeProduct(Product product) throws Exception {
				return super.storeProduct(product);
			}

			@Override
			public ProductId storeProductSource(ProductSource source) throws Exception {
				return super.storeProductSource(source);
			}
		};
	}

	/**
	 * Get the file extension for the format.
	 * 
	 * @param format
	 *            the format.
	 * @return the file extension or null if none.
	 */
	public static String getFileExt(String format) {
		String fileExt = FILE_EXTENSIONS.get(format);
		if (fileExt == null) {
			for (Entry<String, String> entry : FILE_EXTENSIONS.entrySet()) {
				if (format.startsWith(entry.getKey())) {
					fileExt = entry.getValue();
					break;
				}
			}
		}
		return fileExt;
	}

	/**
	 * Get the format for the file.
	 * 
	 * @param file
	 *            the file.
	 * @param defaultFormat
	 *            the default format.
	 * @return the format.
	 */
	public static String getFormat(File file, String defaultFormat) {
		String format = defaultFormat;
		String path = file.getPath();
		for (Entry<String, String> entry : FILE_EXTENSIONS.entrySet()) {
			if (path.endsWith(entry.getValue())) {
				format = entry.getKey();
				break;
			}
		}
		return format;
	}

	/**
	 * Run the converter.
	 * 
	 * @param args
	 *            the program arguments.
	 * @see gov.usgs.earthquake.product.io.IOUtil#main(String[])
	 */
	public static void main(String[] args) {
		if (args.length != 2) {
			printUsage();
			return;
		}
		File infile = new File(args[0]), outfile = new File(args[1]);
		if (!infile.exists()) {
			System.err.println("input file does not exist (" + infile + ")");
			printUsage();
			System.exit(1);
		}
		String informat;
		if (infile.isDirectory()) {
			informat = FORMAT_DIRECTORY;
		} else {
			informat = getFormat(infile, null);
		}
		if (informat == null) {
			System.err.println("could not determine formt for input (" + infile + ")");
			printUsage();
			System.exit(2);
		}
		String outformat = getFormat(outfile, FORMAT_DIRECTORY);
		try {
			ProductSource in = createProductSource(informat, infile);
			ProductHandler out = createProductHandler(outformat, outfile);
			in.streamTo(out);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/** Print usage */
	public static void printUsage() {
		System.out.println("Usage:\n   " + ProductStorageUtil.class.getName() + " input output");
	}
}
