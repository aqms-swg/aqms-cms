//OriginProductWedge.java:  Utility class for extracting message data
//                          from a PDL Origin product.
//
//   5/8/2013 -- [ET]  Converted from PDL EIDSOutputWedge class.
//

package com.isti.quakewatch.server.pdl;

import java.io.InputStream;
import gov.usgs.util.StreamUtils;
import gov.usgs.ansseqmsg.EQMessage;
import gov.usgs.earthquake.eqxml.EQMessageParser;
import gov.usgs.earthquake.event.Converter;
import gov.usgs.earthquake.product.Product;

public class OriginProductWedge
{
  public static final String INPUT_TYPE_EQXML = "eqxml.xml";
  public static final String INPUT_TYPE_QUAKEML = "quakeml.xml";
  public static final String OUTPUT_TYPE_EQXML = "eqxml.xml";
  public static final String OUTPUT_TYPE_QUAKEML = "quakeml.xml";
  public static final String OUTPUT_TYPE_CUBE = "cube.txt";

  public static final String [] INPUT_TYPES =
                                   { INPUT_TYPE_EQXML, INPUT_TYPE_QUAKEML };

  public static final String DEFAULT_OUTPUT_FORMAT = OUTPUT_TYPE_EQXML;

  // Local Variables
  private String outputFormat = DEFAULT_OUTPUT_FORMAT;

  /**
   * Create a new OriginProductWedge.
   * @param outFmtStr Output format string to use.
   */
  public OriginProductWedge(String outFmtStr)
  {
    outputFormat = outFmtStr;
  }

  /**
   * Create a new OriginProductWedge.
   */
  public OriginProductWedge()
  {
  }

  /**
   * Convert product object to byte array of message data.
   * @param product product object to be converted.
   * @return A byte array of message data, or null if the product could not be
   * converted.
   * @throws Exception if an error occurs.
   */
  public byte [] convertProductToArray(final Product product) throws Exception
  {
    String input = "";
    // Finishes if product does not have a file of interest
    for(int i = 0, length = INPUT_TYPES.length; i < length; i++)
    {
      if(product.getContents().containsKey(INPUT_TYPES[i]))
      {
        input = INPUT_TYPES[i];
        break;
      }
    }
    return (input.length() > 0) ? convert(product, input) : null;
  }

  /**
   * Convert product object to byte array of message data.
   * @param product product object to be converted.
   * @return A byte array of message data, or null if the product could not be
   * converted.
   * @throws Exception if an error occurs.
   */
  public String convertProductToString(final Product product) throws Exception
  {
    final byte [] btArr = convertProductToArray(product);
    return (btArr != null) ? new String(btArr) : null;
  }

  /**
   * Extracts the contents of the file specified by the fileName from the
   * product and returns the binary data as a byte array.
   * @param product Product object.
   * @param input Input type specifier.
   * @return Byte array of converted data.
   * @throws Exception if an error occurs.
   */
  private byte [] convert(final Product product, String input) throws Exception
  {
    byte [] data;
    InputStream is = product.getContents().get(input).getInputStream();
    // If input is eqxml
    if(input.equals(INPUT_TYPE_EQXML))
    {
      EQMessage e = EQMessageParser.parse(is);
      return eqxmlConvert(e);

    }
    else if(input.equals(INPUT_TYPE_QUAKEML))
    { // If input is quakeml
      data = StreamUtils.readStream(is);
      String quakeString = new String(data);
      return quakemlConvert(quakeString);
    }
    is.close();
    return null;
  }

  /**
   * Handles conversion from an eqxml to either a eqxml(no conversion),
   * quakeml, or cube.
   * @param e EQMessage object.
   * @return A new byte array of converted data, or null if could not be
   * converted.
   * @throws Exception if an error occurs.
   */
  private byte [] eqxmlConvert(EQMessage e) throws Exception
  {
    Converter converter = new Converter();
    if(outputFormat.equals(OUTPUT_TYPE_EQXML))
    {
      return converter.getString(e).getBytes(); // If to eqxml
    }
    else if(outputFormat.equals(OUTPUT_TYPE_QUAKEML))
    { // If to quakeml
      return converter.getString(converter.getQuakeml(e)).getBytes();
    }
    else if(outputFormat.equals(OUTPUT_TYPE_CUBE))
    { // If to cube
      return converter.getString(converter.getCubeMessage(e)).getBytes();
    }

    return null;
  }

  /**
   * Handles conversion from an quakeml to either a eqxml, quakeml (no
   * conversion), or cube.
   * @param qString String containing quakeml data.
   * @return A new byte array of converted data, or null if could not be
   * converted.
   * @throws Exception if an error occurs.
   */
  private byte [] quakemlConvert(String qString) throws Exception
  {
    Converter converter = new Converter();
    if(outputFormat.equals(OUTPUT_TYPE_EQXML))
    { // To eqxml
      return converter.getString(
          converter.getEQMessage(converter.getQuakeml(qString))).getBytes();
    }
    else if(outputFormat.equals(OUTPUT_TYPE_QUAKEML))
    { // To quakeml
      return qString.getBytes();
    }
    else if(outputFormat.equals(OUTPUT_TYPE_CUBE))
    { // To cube
      return converter.getString(
          converter.getCubeMessage(converter.getQuakeml(qString))).getBytes();
    }

    return null;
  }

  public String getOutputFormat()
  {
    return outputFormat;
  }

  public void setOutputFormat(String outputFormat)
  {
    this.outputFormat = outputFormat;
  }
}
