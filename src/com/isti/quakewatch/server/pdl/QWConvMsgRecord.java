//QWConvMsgRecord.java:  Holds the 'DataMessage' element for a generated
//                       QuakeWatch-server message.
//
//  5/14/2013 -- [ET]  Initial version.
//  7/11/2013 -- [ET]  Removed "@Override" directive from 'compareTo()'
//                     method.
//

package com.isti.quakewatch.server.pdl;

import java.util.Date;
import org.jdom.Element;
import com.isti.util.IstiXmlUtils;
import com.isti.quakewatch.common.MsgTag;

/**
 * Class QWConvMsgRecord holds the 'DataMessage' element for a generated
 * QuakeWatch-server message.
 */
public class QWConvMsgRecord implements Comparable
{
  protected final Element dataMsgElement;
  protected final long messageNumVal;
  protected final String fdrSourceHostStr;
  protected final long fdrSourceMsgNumVal;
  protected Date updateTimeObj = null;
  protected String recordIdStr = null;
  protected long enqueuedTimeVal = 0;

  /**
   * Creates a holder for a generated QuakeWatch-server message.
   * @param dataMsgElement DataMessage element object.
   * @param messageNumVal message number for message.
   * @param fdrSourceHostStr feeder-source-host string, or null for none.
   * @param fdrSourceMsgNumVal feeder-source message number, or 0 for none.
   */
  public QWConvMsgRecord(Element dataMsgElement, long messageNumVal,
                           String fdrSourceHostStr, long fdrSourceMsgNumVal)
  {
    this.dataMsgElement = dataMsgElement;
    this.messageNumVal = messageNumVal;
    this.fdrSourceHostStr = fdrSourceHostStr;
    this.fdrSourceMsgNumVal = fdrSourceMsgNumVal;
  }

  /**
   * Returns the "QWmessage" element object for this record.
   * @return A new "QWmessage" Element object containing the data held by
   * this record, or null if the object could not be created.
   */
  public Element getQWMsgElementObj()
  {
    try
    {         //create 'QWmessage' element and add 'DataMessage' child:
      final Element qwMsgElement =
                (new Element(MsgTag.QW_MESSAGE)).addContent(dataMsgElement);
              //add message-number attribute to 'QWmessage' element:
      qwMsgElement.setAttribute(
                            MsgTag.MSG_NUMBER,Long.toString(messageNumVal));
      if(fdrSourceHostStr != null && fdrSourceHostStr.length() > 0)
      {  //feeder host name contains data; add attributes to element
        qwMsgElement.setAttribute(MsgTag.FDR_SOURCE_HOST,fdrSourceHostStr);
        qwMsgElement.setAttribute(MsgTag.FDR_SOURCE_MSGNUM,
                                         Long.toString(fdrSourceMsgNumVal));
      }
      return qwMsgElement;
    }
    catch(Exception ex)
    {              //if error then return null
      return null;
    }
  }

  /**
   * Returns the archive-data representation of this object.
   * @return A string containing the archive-data representation of
   * this object.
   */
  public String toArchivedForm()
  {
    try
    {    //get 'QWmessage' element for message, convert to string & return:
      final Element qwMsgElement;
      if((qwMsgElement=getQWMsgElementObj()) != null)
        return IstiXmlUtils.elementToString(qwMsgElement);
      return "<error: can't create QWmessage element>";
    }
    catch(Exception ex)
    {              //if error then return indicator string
      return "<error: " + ex + ">";
    }
  }

  /**
   * Returns a descriptive string for this message record.
   * @return A descriptive string for this message record.
   */
  public String toString()
  {
    return "(updTime=" + getUpdateTimeVal() + ") " + getRecordIdStr();
  }

  /**
   * Compares this object with the specified object for order.  The
   * update times for the message records are used for the comparison.
   * @param obj object to compare to.
   * @return A negative integer, zero, or a positive integer as this
   * object is less than, equal to, or greater than the specified object.
   * @throws ClassCastException if the specified object's type prevents
   * it from being compared to this object.
   */
  public int compareTo(Object obj)
  {
    if(obj instanceof QWConvMsgRecord)
    {  //given object type is QWConvMsgRecord
      final Date dateObj = ((QWConvMsgRecord)(obj)).updateTimeObj;
              //if both update-time objects are non-null then compare them,
              // if only one non-null then it is greater than the other,
              // if both null then they are equal:
      if(updateTimeObj != null)
        return (dateObj != null) ? updateTimeObj.compareTo(dateObj) : 1;
      return (dateObj == null) ? 0 : -1;
    }
    throw new ClassCastException();    //given obj type not QWConvMsgRecord
  }

  /**
   * Compares this object with the specified object.  The update times
   * for the message records are used for the comparison.
   * @param obj object to compare to.
   * @return true if equal; false if not.
   * @throws ClassCastException if the specified object's type prevents
   * it from being compared to this object.
   */
  @Override
  public boolean equals(Object obj)
  {
    if(obj instanceof QWConvMsgRecord)
    {  //given object type is QWConvMsgRecord
      final Date dateObj = ((QWConvMsgRecord)(obj)).updateTimeObj;
              //if both update-time objects are non-null then compare them,
              // if both null then they are equal:
      if(updateTimeObj != null)
        return (dateObj != null) ? updateTimeObj.equals(dateObj) : false;
      return (dateObj == null);
    }
    throw new ClassCastException();    //given obj type not QWConvMsgRecord
  }
  
  /**
   * Returns a hash code value for this message record.  The update time
   * for the message record is used for the comparison.
   */
  @Override
  public int hashCode()
  {
    return (updateTimeObj != null) ? updateTimeObj.hashCode() :
                                                           super.hashCode();
  }
  
  /**
   * Returns the DataMessage element object.
   * @return The DataMessage element object.
   */
  public Element getDataMsgElement()
  {
    return dataMsgElement;
  }

  /**
   * Returns the message number for message.
   * @return The message number for message.
   */
  public long getMessageNumVal()
  {
    return messageNumVal;
  }
  
  /**
   * Returns the feeder-source-host string.
   * @return The feeder-source-host string, or null for none.
   */
  public String getFdrSourceHostStr()
  {
    return fdrSourceHostStr;
  }

  /**
   * Returns the feeder-source message number.
   * @return The feeder-source message number, or 0 for none.
   */
  public long getFdrSourceMsgNumVal()
  {
    return fdrSourceMsgNumVal;
  }

  /**
   * Sets the update time for this message record.
   * @param timeObj update time object for this message record, or
   * null if none.
   */
  public void setUpdateTimeObj(Date timeObj)
  {
    updateTimeObj = timeObj;
  }

  /**
   * Returns the update time for this message record.
   * @return The update time object for this message record, or null if none.
   */
  public Date getUpdateTimeObj()
  {
    return updateTimeObj;
  }

  /**
   * Returns the update time for this message record as a long integer.
   * @return The update time value for this message record (ms since
   * 1/1/1970), or 0 if none.
   */
  public long getUpdateTimeVal()
  {
    return (updateTimeObj != null) ? updateTimeObj.getTime() : 0L;
  }

  /**
   * Sets the ID string for this message record.
   * @param str ID string for this message record.
   */
  public void setRecordIdStr(String str)
  {
    recordIdStr = str;
  }

  /**
   * Returns the ID string for this message record.
   * @return The ID string for this message record.
   */
  public String getRecordIdStr()
  {
    return (recordIdStr != null) ? recordIdStr : super.toString();
  }

  /**
   * Sets the time this message record was entered into the queue.
   * @param enqueuedTimeVal epoch-time value (ms since 1/1/1970).
   */
  public void setEnqueuedTimeVal(long enqueuedTimeVal)
  {
    this.enqueuedTimeVal = enqueuedTimeVal;
  }
  
  /**
   * Returns the time this message record was entered into the queue.
   * @return The epoch time this message record was entered into the
   * queue (ms since 1/1/1970), or 0 if none.
   */
  public long getEnqueuedTimeVal()
  {
    return enqueuedTimeVal;
  }
  
  /**
   * Tests if the time this message record was entered into the queue
   * is before the given time.  If this message record has not been
   * entered into the queue then 'false' is returned.
   * @param timeVal epoch time value (ms since 1/1/1970).
   * @return true if the time this message record was entered into the
   * queue is before the given time.
   */
  public boolean enqueuedTimeBefore(long timeVal)
  {
    return (enqueuedTimeVal > 0 && enqueuedTimeVal < timeVal);
  }
}
