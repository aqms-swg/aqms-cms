package com.isti.quakewatch.server.pdl;

public interface PdlConstants
{
  /** Default format string used to build DYFI URLs */
  String DEFAULT_DYFI_URL_FORMAT_STR = "https://earthquake.usgs.gov/earthquakes/eventpage/{EVSOURCE}{EVCODE}/dyfi";
  /** Default format string used to build losspager URLs */
  String DEFAULT_LOSSPAGER_URL_FORMAT_STR = "https://earthquake.usgs.gov/earthquakes/eventpage/{EVSOURCE}{EVCODE}/pager";
  /** Default format string used to build moment-tensor URLs */
  String DEFAULT_MOMENTTENSOR_URL_FORMAT_STR = "https://earthquake.usgs.gov/earthquakes/eventpage/{EVSOURCE}{EVCODE}/moment-tensor";
  /** Version code "99" for ShakeMap delete msgs that otherwise don't have one. */
  String DEFAULT_SHAKEMAP_DEL_VERSION_STR = "99";
  /** Default format string used to build ShakeMap URLs */
  String DEFAULT_SHAKEMAP_URL_FORMAT_STR = "https://earthquake.usgs.gov/archive/product/{TYPE}/{CODE}/{SOURCE}/{TIME}/download/shape.zip";
}
