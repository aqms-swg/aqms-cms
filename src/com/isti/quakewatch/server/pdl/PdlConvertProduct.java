package com.isti.quakewatch.server.pdl;

import gov.usgs.earthquake.product.Product;

public interface PdlConvertProduct
{
  /**
   * Convert the PDL Product object to QWConvMsgRecord object.
   * 
   * @param productObj
   *            the PDL Product object.
   * @param associateEventID
   *            associate event ID or null if none.
   * @param lowPriority
   *            true if low priority, false otherwise.
   */
  void convertProduct(final Product productObj, String associateEventID, boolean lowPriority);
}
