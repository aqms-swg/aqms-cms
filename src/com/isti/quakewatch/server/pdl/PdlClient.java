package com.isti.quakewatch.server.pdl;

import java.io.File;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import com.isti.util.IstiLogFileHandler;
import com.isti.util.LogFile;
import com.isti.util.UtilFns;

import gov.usgs.earthquake.distribution.DefaultNotificationListener;
import gov.usgs.earthquake.distribution.EIDSNotificationReceiver;
import gov.usgs.earthquake.distribution.FileProductStorage;
import gov.usgs.earthquake.distribution.JDBCNotificationIndex;
import gov.usgs.earthquake.distribution.ProductClient;
import gov.usgs.earthquake.distribution.ProductTracker;
import gov.usgs.earthquake.eidsutil.EIDSClient;
import gov.usgs.earthquake.indexer.Indexer;
import gov.usgs.earthquake.product.Product;
import gov.usgs.util.ExecutorTask;
import gov.usgs.util.logging.SimpleLogFormatter;

public class PdlClient implements PdlConvertProduct {
	/**
	 * Class PdlNotifListener defines the PDL product receiving method.
	 */
	protected class PdlNotifListener extends DefaultNotificationListener {
		/**
		 * Called when a product is received; sends product into QWServer.
		 */
		@Override
		public void onProduct(final Product productObj) throws Exception {
			convertProduct(productObj, null, false);
		}
	}

	/**
	 * Returns the version string for the PDL library. Directly accessing
	 * "ProductClient.RELEASE_VERSION" is problematic because it is replaced
	 * with a local literal at compile time and will not change if the PDL
	 * library-jar file is changed.
	 * 
	 * @return The version string for the PDL library, or an error message if
	 *         the version string could not be fetched.
	 */
	public static String getPdlVersionStr() {
		try {
			final String verStr = (new ProductClient()).getVersion();
			int p, q;
			if ((p = verStr.indexOf(')')) > 0) // search for trailing
				// parentheses
				++p; // mark position (including parentheses)
			else // if no trailing parentheses then
				p = verStr.length(); // use entire string
			if ((q = verStr.indexOf('\n')) > 0 && q < p) // search for newline
				p = q; // if newline found then trim at newline
			return verStr.substring(0, p);
		} catch (Throwable ex) {
			return "(unable to get version)";
		}
	}

	private final PdlClientCallback callback;
	private final QWPdlFeederConfig config;
	private EIDSNotificationReceiver eNotifRecvrObj;
	private final LogFile logObj;
	private DefaultNotificationListener notifListenerObj;
	private final PdlProductConverter productConverterObj;
	private AtomicBoolean shutdown = new AtomicBoolean();
	private boolean useIndexerFlag = true;

	public PdlClient(QWPdlFeederConfig config, PdlClientCallback callback, LogFile logObj) {
		if (config == null || callback == null) {
			throw new NullPointerException(
					"Config and callback must be specified (config=" + config + ", callback=" + callback + ")");
		}
		this.callback = callback;
		this.config = config;
		this.logObj = logObj;
		productConverterObj = new PdlProductConverter(logObj, config.pdlMaxByteContentLengthProp.intValue());
		String str; // enter URL-string config properties into converter
		str = config.dyfiUrlFormatStrProp.stringValue();
		productConverterObj.setDyfiUrlFormatStr(str);
    str = config.losspagerUrlFormatStrProp.stringValue();
    productConverterObj.setLosspagerUrlFormatStr(str);
    str = config.momentTensorUrlFormatStrProp.stringValue();
    productConverterObj.setMomentTensorUrlFormatStr(str);
		str = config.shakemapUrlFormatStrProp.stringValue();
		productConverterObj.setShakemapUrlFormatStr(str);
	}

	@Override
	public void convertProduct(final Product productObj, String associateEventID, boolean lowPriority) {
		logObj.info("convertProduct: " + productObj.getEventId() + ", " + associateEventID + ", " + lowPriority);
		try {
			final QWConvMsgRecord cnvMsgRecObj = productConverterObj.pdlProductToRecObj(productObj, associateEventID,
					lowPriority);
			callback.processRecord(cnvMsgRecObj);
		} catch (Throwable ex) { // some kind of exception error; log it
			logObj.warning(callback.getFeederModuleName() + " (\"" + callback.getFeederName()
					+ "\"):  Exception error processing product:  " + ex + ", product:  "
					+ ((productObj != null) ? productObj.getId() : null));
			logObj.warning(UtilFns.getStackTraceString(ex));
		}
	}

	/**
	 * Checks if the PDL client was shut down.
	 * @return
	 */
	public boolean isShutdown() {
	  return shutdown.get();
	}

	/**
	 * Sets up PDL log output to be redirected to feeder log file.
	 * 
	 * @param logObj
	 *            the log.
	 * @param logLevelObj
	 *            logging level for log output.
	 */
	private void setupPdlLogging(final LogFile logObj, Level logLevelObj) {
		LogManager.getLogManager().reset();
		Logger.getLogger(Indexer.class.getName()).setLevel(Level.WARNING);
		Logger.getLogger(ExecutorTask.class.getName()).setLevel(Level.WARNING);
		// get Java logger for PDL classes:
		final Logger pdlLoggerObj = Logger.getLogger("");
		pdlLoggerObj.setLevel(logLevelObj);
		// create formatter:
		final SimpleLogFormatter formatterObj = new SimpleLogFormatter();
		final String[] pdlFilterWarnStrings = config.pdlFilterWarnProp.stringArrayValue();
    final String[] pdlShutdownWarnStrings = config.pdlShutdownWarnProp.stringArrayValue();
		// setup handler to filter warning messages to LogFile:
		final Handler logHdlrObj = new IstiLogFileHandler(logObj) {
			public void publish(java.util.logging.LogRecord recObj) {
				// if log level is 'warning'
			  if (recObj == null) // ignore if not log record
			    return;
			  final int level = recObj.getLevel().intValue();
				if (level >= Level.WARNING.intValue()) {
					final String str = recObj.getMessage().trim();
					for (String pdlFilterWarnStr : pdlFilterWarnStrings) {
					  if (str.startsWith(pdlFilterWarnStr)) {
	            recObj.setLevel(Level.FINER); // reduce level
					  }
					}
          for (String pdlShutdownWarnStr : pdlShutdownWarnStrings) {
            if (str.contains(pdlShutdownWarnStr)) {
              shutdown();
            }
          }
				}
				super.publish(recObj);
			}
		};
		logHdlrObj.setLevel(logLevelObj);
		logHdlrObj.setFormatter(formatterObj);
		pdlLoggerObj.addHandler(logHdlrObj);
	}

	public void shutdown() {
	  // exit if already shutdown
	  if (shutdown.getAndSet(true))
	    return;
		if (notifListenerObj != null) { // PDL listener object was created
			try { // stop PDL listener object:
				notifListenerObj.shutdown();
			} catch (Exception ex) { // some kind of exception error; log it
				if (logObj != null) {
					logObj.warning(callback.getFeederModuleName() + " (\"" + callback.getFeederName() + "\"):  "
							+ "Error stopping PDL-client receiver notifListener:  " + ex);
					logObj.warning(UtilFns.getStackTraceString(ex));
				} else
					ex.printStackTrace();
			}
			notifListenerObj = null; // release PDL listener object
		}
		if (eNotifRecvrObj != null) { // PDL receiver object was created
			try { // stop PDL receiver object:
				eNotifRecvrObj.shutdown();
			} catch (Exception ex) { // some kind of exception error; log it
				if (logObj != null) {
					logObj.warning(callback.getFeederModuleName() + " (\"" + callback.getFeederName() + "\"):  "
							+ "Error stopping PDL-client receiver eNotifRecvr:  " + ex);
					logObj.warning(UtilFns.getStackTraceString(ex));
				} else
					ex.printStackTrace();
			}
			eNotifRecvrObj = null; // release PDL receiver object
		}
	}

	public void startup() throws Exception {
		// set up PDL log output to be redirected to feeder log file:
		setupPdlLogging(logObj, config.getPdlReceiverLogLevel());
		// disable PDL product tracker:
		ProductTracker.setTrackerEnabled(false);
		// setup EIDS client and host access information:
		final EIDSClient eidsClientObj = new EIDSClient(config.pdlServerHostProp.stringValue(),
				Integer.valueOf(config.pdlServerPortProp.intValue()), config.pdlAlternateServersProp.stringValue());
		// set tracking file:
		eidsClientObj.setTrackingFileName(config.pdlTrackingFileProp.stringValue());
		eidsClientObj.setMaxServerEventAgeDays( // set max server age:
				Double.valueOf(config.pdlMaxServerEventAgeDaysProp.doubleValue()));
		// create and setup notification receiver:
		eNotifRecvrObj = new EIDSNotificationReceiver();
		eNotifRecvrObj.setName("EIDS_Notif_Receiver");
		// set index file for receiver:
		final JDBCNotificationIndex jRecvrIndexObj = new JDBCNotificationIndex(
				config.pdlReceiverIndexProp.stringValue());
		// connect storage and index to receiver:
    FileProductStorage storage = new FileProductStorage(
        new File(config.pdlReceiverStorageProp.stringValue()));
		eNotifRecvrObj.setProductStorage(storage);
		eNotifRecvrObj.setNotificationIndex(jRecvrIndexObj);
		// set receiver cleanup interval:
		eNotifRecvrObj.setReceiverCleanupInterval(config.pdlCleanupIntervalProp.longValue());
		// set max product age:
		eNotifRecvrObj.setProductStorageMaxAge(config.pdlStorageAgeProp.longValue());
		// connect client to receiver:
		eNotifRecvrObj.setClient(eidsClientObj);
		eidsClientObj.addListener(eNotifRecvrObj);
		// create notification listener for PDL products:
    if (useIndexerFlag) {
      storage = new FileProductStorage(
          new File(config.pdlIndexerStorageProp.stringValue()));
      PdlIndexer pdlIndexer = new PdlIndexer(logObj, this,
          this.config.pdlIndexerDatabaseProp.stringValue());
      pdlIndexer.setProductStorage(storage);
      notifListenerObj = pdlIndexer;
    } else {
      notifListenerObj = new PdlNotifListener();
    }
		notifListenerObj.setName("NotifListener");
		// set index file for listener:
		final JDBCNotificationIndex jListnrIndexObj = new JDBCNotificationIndex(
				config.pdlListenerIndexProp.stringValue());
		// connect index to listener:
		notifListenerObj.setNotificationIndex(jListnrIndexObj);
		// set listener cleanup interval:
		notifListenerObj.setCleanupInterval(config.pdlCleanupIntervalProp.longValue());
		// set flag so "internal-origin" products can be received:
		notifListenerObj.setIncludeInternals(true);
		// set number of delivery attempts to make:
		notifListenerObj.setMaxTries(config.pdlMaxTriesProp.intValue());
		// set how many ms to wait between retries:
		notifListenerObj.setRetryDelay(config.pdlRetryDelayProp.longValue());
		// configure product types to be received:
		final List<String> incTypesList = notifListenerObj.getIncludeTypes();
		incTypesList.add("origin");
		incTypesList.add("internal-origin");
		incTypesList.add("associate");
		incTypesList.add("disassociate");
		incTypesList.add("trump");
		incTypesList.add("trump-origin");
		incTypesList.add("scitech-link");
		incTypesList.add("impact-link");
		incTypesList.add("scitech-text");
		incTypesList.add("impact-text");
		incTypesList.add("shakemap");
		incTypesList.add("dyfi");
    incTypesList.add("losspager");
    incTypesList.add("moment-tensor");
		// connect listener to receiver:
		eNotifRecvrObj.addNotificationListener(notifListenerObj);
		logObj.info("Starting PDL-client receiver, PDL " + getPdlVersionStr());
		notifListenerObj.startup(); // start implementation
		eNotifRecvrObj.startup();
	}
}
