package com.isti.quakewatch.server.pdl;

import java.util.Vector;

import com.isti.util.LogFile;

import gov.usgs.earthquake.indexer.DefaultIndexerListener;
import gov.usgs.earthquake.indexer.Event;
import gov.usgs.earthquake.indexer.IndexerChange;
import gov.usgs.earthquake.indexer.IndexerEvent;
import gov.usgs.earthquake.product.Product;

public class PdlIndexerListener extends DefaultIndexerListener
{
  private final int logLevel = LogFile.INFO;
  private final LogFile logObj;
  private final PdlConvertProduct pdlConvertProduct;

  public PdlIndexerListener(LogFile logObj, PdlConvertProduct pdlConvertProduct)
  {
    this.logObj = logObj;
    this.pdlConvertProduct = pdlConvertProduct;
  }

  @Override
  public void onIndexerEvent(IndexerEvent change) throws Exception
  {
    final Product productObj = change.getProduct();
    if (productObj == null)
    {
      logObj.warning("onIndexerEvent: no product with indexer change");
      return;
    }
    String associateEventID = null;
    boolean lowPriority = false;
    final String productEventId = productObj.getEventId();
    if (productEventId == null)
    {
      logObj.warning("onIndexerEvent: no product event ID with indexer change");
      return;
    }
    final StringBuilder sb = new StringBuilder("onIndexerEvent: ");
    final Vector<IndexerChange> indexerChanges = change.getIndexerChanges();
    sb.append(productObj.getEventId());
    sb.append(" changes { ");
    if (indexerChanges != null)
    {
      synchronized (indexerChanges)
      {
        if (indexerChanges.size() > 0)
        {
          // IndexerChangeType:
          // EVENT_ADDED, EVENT_UPDATED, EVENT_DELETED,
          // EVENT_ARCHIVED, EVENT_MERGED, EVENT_SPLIT,
          // PRODUCT_ADDED, PRODUCT_UPDATED, PRODUCT_DELETED,
          // PRODUCT_ARCHIVED
          IndexerChange indexerChange;
          Event originalEvent, newEvent;
          String newEventID = null, originalEventID = null;
          for (int index = 0; index < indexerChanges.size(); index++)
          {
            indexerChange = indexerChanges.get(index);
            if (index != 0)
            {
              sb.append(", ");
            }
            sb.append(indexerChange);
            originalEvent = indexerChange.getOriginalEvent();
            newEvent = indexerChange.getNewEvent();
            if (newEvent != null
                && (newEventID = newEvent.getEventId()) != null)
            {
              if (!productEventId.equals(newEventID))
              {
                associateEventID = newEventID;
                lowPriority = true;
              }
            }
            if (originalEvent != null
                && (originalEventID = originalEvent.getEventId()) != null
                && !originalEventID.equals(newEventID))
            {
              if (!productEventId.equals(originalEventID))
              {
                if (!lowPriority)
                {
                  associateEventID = originalEventID;
                }
                else
                {
                  logObj.debug(
                      "onIndexerEvent: product already has low priority event ID ("
                          + productEventId + ", " + associateEventID + ", "
                          + originalEventID + ")");
                }
              }
            }
            if (newEventID != null && originalEventID != null
                && !newEventID.equals(originalEventID))
            {
              sb.append(" *** CHANGED ***");
            }
          }
        }
      }
    }
    sb.append(" }");
    logObj.println(logLevel, sb.toString());
    pdlConvertProduct.convertProduct(productObj, associateEventID, lowPriority);
  }
}
