//PdlProductConverter.java:  Converts PDL Product objects to QuakeWatch
//                           QWConvMsgRecord objects.
//
//  5/31/2013 -- [ET]  Initial version.
//   8/1/2013 -- [ET]  Modified to handle 'internal-origin' products.
//  9/24/2013 -- [ET]  Modified so generated 'ProductLink' messages will
//                     have 'Version' and 'Action' elements inside the
//                     'ProductLink' element (instead of parent 'Event'
//                     element); improved catching of 'originProductWedgeObj'
//                     exceptions in 'pdlProductToRecObj()' method.
//  4/26/2017 -- [KF]  Replaced QWPdlFeeder 'dyfiUrlPrefixStr'
//                     and 'dyfiUrlSuffixStr' with 'dyfiUrlFormatStr'.
//  1/30/2018 -- [KF]  Removed 1024 byte restriction for text products.
//  3/13/2019 -- [KF]  Modified to use the PDL Indexer.
//  2/10/2021 -- [KF]  Added losspager and moment-tensor products;
//                     Fixed logging.
//   3/5/2021 -- [KF]  Replaced shakemapUrlPrefixStr and shakemapUrlSuffixStr
//                     with shakemapUrlFormatStr.
//  3/10/2021 -- [KF]  Added EVCODE and EVSOURCE Product Link Variables.
//

package com.isti.quakewatch.server.pdl;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.jdom.Text;

import com.isti.quakewatch.common.MsgTag;
import com.isti.quakewatch.server.QWServer;
import com.isti.quakewatch.util.QWUtils;
import com.isti.util.IstiXmlUtils;
import com.isti.util.LogFile;
import com.isti.util.UtilFns;

import gov.usgs.earthquake.product.ByteContent;
import gov.usgs.earthquake.product.Content;
import gov.usgs.earthquake.product.FileContent;
import gov.usgs.earthquake.product.Product;
import gov.usgs.earthquake.product.ProductId;

/**
 * Class PdlProductConverter converts PDL Product objects to QuakeWatch
 * QWConvMsgRecord objects.
 */
public class PdlProductConverter implements PdlConstants
{
    /** Format string used to build DYFI URLs. */
  protected String dyfiUrlFormatStr = DEFAULT_DYFI_URL_FORMAT_STR;
    /** Format string used to build losspager URLs. */
  protected String losspagerUrlFormatStr = DEFAULT_LOSSPAGER_URL_FORMAT_STR;
    /** Format string used to build moment-tensor URLs. */
  protected String momentTensorUrlFormatStr = DEFAULT_MOMENTTENSOR_URL_FORMAT_STR;
  /** Format string used to build ShakeMap URLs. */
  protected String shakemapUrlFormatStr = DEFAULT_SHAKEMAP_URL_FORMAT_STR;
    /** Fallback prefix string used to build ShakeMap URLs. */
  protected final String shakemapFallbackUrlPrefixStr =
            "http://earthquake.usgs.gov/earthquakes/shakemap/global/shake/";
    /** Utility object for converting Origin products. */
  protected final OriginProductWedge originProductWedgeObj =
               new OriginProductWedge(OriginProductWedge.OUTPUT_TYPE_EQXML);
    /** Date-formatter object for 'eventChildToRecObj()' method. */
  protected final DateFormat xmlDateFormatterObj =
                                           QWUtils.getXmlDateFormatterObj();
    /** Log file to use. */
  protected final LogFile logObj;
  private final int maxByteContentLength;
    /** Namespace object for elements. */
  public static final Namespace namespaceObj =
                        Namespace.getNamespace(MsgTag.ANSSEQ_NAMESPACE_STR);
    /** Table of allowed TypeKey values for ProductLink, with LC keys. */
  protected static final HashMap<String,String> linkMsgTypesTable =
                                               new HashMap<String,String>();
    /** List of product types allowed to be unhandled without warnings. */
  protected static final ArrayList<String> allowedUnhandledList =
                                                    new ArrayList<String>();
  static
  {      //fill table and list entries:
    linkMsgTypesTable.put(MsgTag.LINK_URL.toLowerCase(),MsgTag.LINK_URL);
    linkMsgTypesTable.put(MsgTag.SHAKEMAP_URL.toLowerCase(),
                                                       MsgTag.SHAKEMAP_URL);
    linkMsgTypesTable.put(MsgTag.FOCAL_MECH1_URL.toLowerCase(),
                                                    MsgTag.FOCAL_MECH1_URL);
    linkMsgTypesTable.put(MsgTag.FOCAL_MECH2_URL.toLowerCase(),
                                                    MsgTag.FOCAL_MECH2_URL);
    linkMsgTypesTable.put(MsgTag.REALTIME_MECH_URL.toLowerCase(),
                                                  MsgTag.REALTIME_MECH_URL);
    linkMsgTypesTable.put(MsgTag.MOMENT_TENSOR_URL.toLowerCase(),
                                                  MsgTag.MOMENT_TENSOR_URL);
    linkMsgTypesTable.put(MsgTag.DIDYOU_FEELIT.toLowerCase(),
                                                      MsgTag.DIDYOU_FEELIT);
    linkMsgTypesTable.put(MsgTag.TSUNAMI.toLowerCase(),MsgTag.TSUNAMI);
    linkMsgTypesTable.put(MsgTag.LossPAGER.toLowerCase(),MsgTag.LossPAGER);
    linkMsgTypesTable.put(MsgTag.MomentTensor.toLowerCase(),MsgTag.MomentTensor);
    allowedUnhandledList.add("associate");
    allowedUnhandledList.add("disassociate");
    allowedUnhandledList.add("trump");
    allowedUnhandledList.add("trump-origin");
  }

  /**
   * Creates a PDL Product converter.
   * @param logObj log file object to use, or null for none.
   * @param maxByteContentLength the maximum byte content length.
   */
  public PdlProductConverter(LogFile logObj, int maxByteContentLength)
  {
    this.logObj = logObj;
    this.maxByteContentLength = maxByteContentLength;
  }

  /**
   * Converts the given PDL Product object to a QuakeWatch QWConvMsgRecord
   * object.
   * @param productObj PDL Product object to convert.
   * @param associateEventID associate event ID or null if none.
   * @param lowPriority true if low priority, false otherwise.
   * @return A new QWConvMsgRecorda object, or null of the PDL Product
   * object could not be converted (in which case a warning message will
   * be sent to the log file).
   */
  public QWConvMsgRecord pdlProductToRecObj(
		  Product productObj, String associateEventID, boolean lowPriority)
  {
    try
    {
      final ProductId prodIdObj = productObj.getId();
      final Boolean statusDeleteFlag =      //true if 'delete' message
             Product.STATUS_DELETE.equalsIgnoreCase(productObj.getStatus());
      final String prodTypeStr = prodIdObj.getType();
      QWConvMsgRecord cnvMsgRecObj;
      if("origin".equalsIgnoreCase(prodTypeStr) ||
                            "internal-origin".equalsIgnoreCase(prodTypeStr))
      {  //product type is 'origin' or 'internal-origin'
        boolean wedgeExceptionFlag = false;
        try
        {
          final String eqMsgStr;
          if((eqMsgStr=originProductWedgeObj.convertProductToString(
                                                       productObj)) != null)
          {  //product converted OK
            cnvMsgRecObj = eqMsgStrToRecObj(
            		eqMsgStr,productObj,associateEventID,lowPriority);
            logObj.debug3("PdlProductConverter pdlProductToRecObj:  " +
                 "Converted origin product via XML content:  " + prodIdObj);
          }
          else
            cnvMsgRecObj = null;       //indicate not converted
        }
        catch(Exception ex)
        {  //error converting origin; log warning
          cnvMsgRecObj = null;
          wedgeExceptionFlag = true;        //indicate exception happened
          logObj.warning("PdlProductConverter pdlProductToRecObj " +
                    "wedge exception:  " + ex + ", product:  " + prodIdObj);
          logObj.warning(UtilFns.getStackTraceString(ex));
        }
        if(cnvMsgRecObj == null)
        {  //origin was not converted above
          if(statusDeleteFlag)
          {  //status is 'delete'; create message
            cnvMsgRecObj = deleteEventToRecObj(productObj);
            if(wedgeExceptionFlag)
            {  //exception was caught above
              logObj.debug("PdlProductConverter pdlProductToRecObj:  " +
                           "Converted delete-origin product (after wedge " +
                           "exception) without XML content:  " + prodIdObj);
            }
            else
            {  //no exception happened
              logObj.debug3("PdlProductConverter pdlProductToRecObj:  " +
                  "Converted delete-origin product without XML content:  " +
                                                                 prodIdObj);
            }
          }
          else if(!wedgeExceptionFlag)      //exception not logged above
          {  //no XML content found and status is not 'delete'
            logObj.warning("PdlProductConverter pdlProductToRecObj:  " +
                         "Unable to convert origin product:  " + prodIdObj);
          }
        }
      }
      else if("general-link".equalsIgnoreCase(prodTypeStr) ||
                           "scitech-link".equalsIgnoreCase(prodTypeStr) ||
                              "impact-link".equalsIgnoreCase(prodTypeStr))
      {  //product type is add-on-link message
        cnvMsgRecObj = linkProductToRecObj(productObj);
        logObj.debug3("PdlProductConverter pdlProductToRecObj:  " +
                              "Converted link-type product:  " + prodIdObj);
      }
      else if("general-text".equalsIgnoreCase(prodTypeStr) ||
                           "scitech-text".equalsIgnoreCase(prodTypeStr) ||
                              "impact-text".equalsIgnoreCase(prodTypeStr))
      {  //product type is "text" message
        cnvMsgRecObj = textProductToRecObj(productObj);
        logObj.debug3("PdlProductConverter pdlProductToRecObj:  " +
                              "Converted text-type product:  " + prodIdObj);
      }
      else if("shakemap".equalsIgnoreCase(prodTypeStr))
      {  //product type is 'shakemap'
        cnvMsgRecObj = shakeMapProductToRecObj(productObj);
        logObj.debug3("PdlProductConverter pdlProductToRecObj:  " +
                               "Converted shakemap product:  " + prodIdObj);
      }
      else if("dyfi".equalsIgnoreCase(prodTypeStr))
      {  //product type is 'dyfi'
        cnvMsgRecObj = dyfiProductToRecObj(productObj);
        logObj.debug3("PdlProductConverter pdlProductToRecObj:  " +
                              "Converted dyfi product:  " + prodIdObj);
      }
      else if ("losspager".equalsIgnoreCase(prodTypeStr))
      {
        cnvMsgRecObj = losspagerProductToRecObj(productObj);
        logObj.debug3("PdlProductConverter pdlProductToRecObj:  " +
                              "Converted losspager product:  " + prodIdObj);
      }
      else if ("moment-tensor".equalsIgnoreCase(prodTypeStr))
      {
        cnvMsgRecObj = momentTensorProductToRecObj(productObj);
        logObj.debug3("PdlProductConverter pdlProductToRecObj:  " +
                              "Converted moment-tensor product:  " + prodIdObj);
      }
      else
      {  //unexpected product type
        final String urlStr;
        if((urlStr=productObj.getProperties().get("url")) != null &&
                                                        urlStr.length() > 0)
        {  //product has URL property; handle has addon-link product
          cnvMsgRecObj = linkProductToRecObj(productObj);
          logObj.debug("PdlProductConverter pdlProductToRecObj:  " +
                         "Converted unexpected URL product:  " + prodIdObj);
        }
        else
        {  //unhandled product
          cnvMsgRecObj = null;         //indicate not converted
              //if product type on 'allowedUnhandledList' then log message
              // at 'debug' level; if not then log at 'warning' level:
          final int logLevelVal =
                      (allowedUnhandledList.contains(prodIdObj.getType())) ?
                                            LogFile.DEBUG : LogFile.WARNING;
          logObj.println(logLevelVal,
                               "PdlProductConverter pdlProductToRecObj:  " +
                                        "Unhandled product:  " + prodIdObj);
        }
      }
      if(cnvMsgRecObj != null)
      {  //record was created; enter update time and ID string from Product
        cnvMsgRecObj.setUpdateTimeObj(prodIdObj.getUpdateTime());
        cnvMsgRecObj.setRecordIdStr(prodIdObj.toString());
      }
      return cnvMsgRecObj;
    }
    catch(Throwable ex)
    {  //some kind of exception error happened; log it
      logObj.warning("PdlProductConverter pdlProductToRecObj exception:  " +
                                                       ex + ", product:  " +
                        ((productObj != null) ? productObj.getId() : null));
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
    return null;
  }
  
  /**
   * Returns the format string used to build DYFI URLs.
   * @return The format string used to build DYFI URLs.
   */
  public String getDyfiUrlPrefixStr()
  {
    return dyfiUrlFormatStr;
  }

  /**
   * Sets the format string used to build DYFI URLs.  
   * @param dyfiUrlFormatStr The format string used to build
   * DYFI URLs.
   */
  public void setDyfiUrlFormatStr(String dyfiUrlFormatStr)
  {
    this.dyfiUrlFormatStr = dyfiUrlFormatStr;
  }

  /**
   * Sets the format string used to build losspager URLs.  
   * @param losspagerUrlFormatStr The format string used to build
   * losspager URLs.
   */
  public void setLosspagerUrlFormatStr(String losspagerUrlFormatStr)
  {
    this.losspagerUrlFormatStr = losspagerUrlFormatStr;
  }

  /**
   * Sets the format string used to build DYFI URLs.  
   * @param dyfiUrlFormatStr The format string used to build
   * DYFI URLs.
   */
  public void setMomentTensorUrlFormatStr(String momentTensorUrlFormatStr)
  {
    this.momentTensorUrlFormatStr = momentTensorUrlFormatStr;
  }

  /**
   * Returns the format string used to build ShakeMap URLs.
   * @return The format string used to build ShakeMap URLs.
   */
  public String getShakemapUrlFormatStr()
  {
    return shakemapUrlFormatStr;
  }

  /**
   * Sets the format string used to build ShakeMap URLs.
   * @param shakemapUrlFormatStr The format string used to build ShakeMap URLs.
   */
  public void setShakemapUrlFormatStr(String shakemapUrlFormatStr)
  {
    this.shakemapUrlFormatStr = shakemapUrlFormatStr;
  }

  /**
   * Wraps 'DataMessage' and 'QWmessage' elements around the given
   * 'EQMessage' element and generates a message-record object.
   * @param eqMessageElem Element object containing the 'EQMessage' element.
   * @param propsMap map of product properties (checked for "eids-feeder"
   * and "eids-feeder-sequence" entries).
   * @param msgNumVal message number for generated record.
   * @param deleteFlag true to generate a 'delete' record.
   * @return A new 'QWConvMsgRecord' object.
   */
  protected QWConvMsgRecord eqElementToRecObj(Element eqMessageElem,
            Map<String,String> propsMap, Long msgNumVal, boolean deleteFlag)
  {
    final Element dataMsgElement = new Element(MsgTag.DATA_MESSAGE);
              //add Action attribute to DataMessage element:
    dataMsgElement.setAttribute(MsgTag.ACTION,
                              (deleteFlag ? MsgTag.DELETE : MsgTag.UPDATE));
    dataMsgElement.addContent(eqMessageElem);
    
    final String fdrSourceHostStr;
    long fdrSourceMsgNumVal = 0;
    if((fdrSourceHostStr=propsMap.get("eids-feeder")) != null &&
                                              fdrSourceHostStr.length() > 0)
    {  //Product property for feeder-data-source host contains data
      try
      {       //get value for feeder-data-source message number:
        fdrSourceMsgNumVal =
                       Long.parseLong(propsMap.get("eids-feeder-sequence"));
      }
      catch(Exception ex)
      {       //if error then use zero value
      }
    }
    return new QWConvMsgRecord(dataMsgElement,msgNumVal,
                                       fdrSourceHostStr,fdrSourceMsgNumVal);
  }
  
  /**
   * Wraps 'EQMessage' and 'Event' elements around the given child element
   * and generates a message-record object.
   * @param eventChildElem Element object containing the child element.
   * @param propsMap map of product properties.
   * @param msgNumVal message number for generated record.
   * @param evtVerActionFlag true to include 'Version' and 'Action'
   * elements in generated 'Event' element.
   * @param deleteFlag true to generate a 'delete' record.
   * @param trumpFlag true to set 'Action' to 'Trump'.
   * @return A new 'QWConvMsgRecord' object.
   */
  protected QWConvMsgRecord eventChildToRecObj(Element eventChildElem,
                                Map<String,String> propsMap, Long msgNumVal,
            boolean evtVerActionFlag, boolean deleteFlag, boolean trumpFlag)
  {
                   //save current time (as an XML-format time string):
    final String timeReceivedStr;
    synchronized(xmlDateFormatterObj)
    {    //only allow one thread at a time to use date-format object
      timeReceivedStr = xmlDateFormatterObj.format(new Date());
    }

    final String sourceCodeStr = propsMap.get(Product.EVENTSOURCE_PROPERTY);
    
              //create Event element:
    final Element eventElem = new Element(MsgTag.EVENT,namespaceObj);
                        //add data-source element:
    if(sourceCodeStr != null && sourceCodeStr.length() > 0)
      addElemContent(eventElem,MsgTag.DATA_SOURCE,sourceCodeStr);
    String str;         //add source-code element:
    if((str=propsMap.get(Product.EVENTSOURCECODE_PROPERTY)) != null &&
                                                           str.length() > 0)
    {
      addElemContent(eventElem,MsgTag.EVENT_ID,str);
    }
    if(evtVerActionFlag)
    {  //adding Version and Action elements to Event element
                        //add Version element (if available):
      if((str=propsMap.get(Product.VERSION_PROPERTY)) != null &&
                                                           str.length() > 0)
      {
        addElemContent(eventElem,MsgTag.VERSION,str);
      }
      if(!trumpFlag)
      {  //not Trump message; set action element to "Update" or "Delete"
        addElemContent(eventElem,MsgTag.ACTION,
                                (deleteFlag ? MsgTag.DELETE : MsgTag.UPDATE));
      }
      else
        addElemContent(eventElem,MsgTag.ACTION,MsgTag.TRUMP);
    }      
              //add given element as child to Event element:
    if(eventChildElem != null)
      eventElem.addContent(eventChildElem);

              //create EQMessage element:
    final Element eqMessageElem = new Element(MsgTag.EQMESSAGE,namespaceObj);
                        //add message-source element:
    if(sourceCodeStr != null && sourceCodeStr.length() > 0)
      addElemContent(eqMessageElem,MsgTag.SOURCE,sourceCodeStr);
                        //add software-module element:
    addElemContent(eqMessageElem,MsgTag.MODULE,QWServer.REVISION_STRING);
                        //add message-sent-time element:
    addElemContent(eqMessageElem,MsgTag.SENT,timeReceivedStr);
                        //add Event element as child to to EQMessage element:
    eqMessageElem.addContent(eventElem);
    
    return eqElementToRecObj(eqMessageElem,propsMap,msgNumVal,deleteFlag);
  }
  
  /**
   * Wraps 'EQMessage' and 'Event' elements around the given child element
   * and generates a message-record object.
   * @param eventChildElem Element object containing the child element.
   * @param propsMap map of product properties.
   * @param msgNumVal message number for generated record.
   * @param evtVerActionFlag true to include 'Version' and 'Action'
   * elements in generated 'Event' element.
   * @param deleteFlag true to generate a 'delete' record.
   * @return A new 'QWConvMsgRecord' object.
   */
  protected QWConvMsgRecord eventChildToRecObj(Element eventChildElem,
                                Map<String,String> propsMap, Long msgNumVal,
                               boolean evtVerActionFlag, boolean deleteFlag)
  {
    return eventChildToRecObj(eventChildElem,propsMap,msgNumVal,
                                         evtVerActionFlag,deleteFlag,false);
  }

  /**
   * Converts the given XML-message string to a message-record object.  The
   * root element of the XML-message should be 'EQMessage'.
   * @param eqMsgStr XML-message string to be converted.
   * @param productObj Product object associated with the XML message.
   * @param associateEventID associate event ID or null if none.
   * @param lowPriority true if low priority, false otherwise.
   * @return A new 'QWConvMsgRecord' object.
   * @throws JDOMException if an error occurs while parsing the XML message.
   */
  protected QWConvMsgRecord eqMsgStrToRecObj(
		  String eqMsgStr, Product productObj,
		  String associateEventID, boolean lowPriority) throws JDOMException
  {
    final Element eqMessageElem = IstiXmlUtils.stringToElement(eqMsgStr);
    try
    {
    	final Element eventElement = eqMessageElem.getChild(
    			MsgTag.EVENT,eqMessageElem.getNamespace());
    	if (eventElement != null)
    	{
    		final Element originElement = eventElement.getChild(
    				MsgTag.ORIGIN,eventElement.getNamespace());
    		if (originElement != null)
    		{
    			if (associateEventID != null)
    			{
    				originElement.setAttribute(
    						MsgTag.ASSOCIATE, associateEventID);
    				originElement.setAttribute(
    						MsgTag.LOW_PRIORITY, Boolean.toString(lowPriority));
    			}
    		}
    	}
    }
    catch (Exception ex)
    {
    }
    return eqElementToRecObj(eqMessageElem,productObj.getProperties(),
                                       getProductUpdateTimeLong(productObj),
            Product.STATUS_DELETE.equalsIgnoreCase(productObj.getStatus()));
  }

  /**
   * Converts the given delete-event Product object to a message-record
   * object.
   * @param productObj delete-event Product object to be converted.
   * @return A new 'QWConvMsgRecord' object.
   */
  protected QWConvMsgRecord deleteEventToRecObj(Product productObj)
  {
    final String commentStr = getProductByteContentStr(productObj);
    final Element commentElem;
    if(commentStr != null && commentStr.length() > 0)
    {
      commentElem = new Element(MsgTag.COMMENT,namespaceObj);
      addElemContent(commentElem,MsgTag.TYPE_KEY,MsgTag.INFO);
      addElemContent(commentElem,MsgTag.TEXT,
                                          Text.normalizeString(commentStr));
    }
    else
      commentElem = null;
    return eventChildToRecObj(commentElem,productObj.getProperties(),
                            getProductUpdateTimeLong(productObj),true,true);
  }

  /**
   * Converts the given link-type Product object to a message-record object.
   * @param productObj link-type Product object to be converted.
   * @return A new 'QWConvMsgRecord' object.
   */
  protected QWConvMsgRecord linkProductToRecObj(Product productObj)
  {
              //create ProductLink element:
    final Element prodLinkElem =
                              new Element(MsgTag.PRODUCT_LINK,namespaceObj);
    final Map<String,String> propsMap = productObj.getProperties();
    final Boolean deleteFlag =
             Product.STATUS_DELETE.equalsIgnoreCase(productObj.getStatus());
              //add TypeKey element to ProductLink element:
    addElemContent(prodLinkElem,MsgTag.TYPE_KEY,
                        checkGetLinkMsgTypeKey(propsMap.get("addon-type")));
    String str;
              //add Version element to ProductLink element (if available):
    if((str=propsMap.get(Product.VERSION_PROPERTY)) != null &&
                                                           str.length() > 0)
    {
      addElemContent(prodLinkElem,MsgTag.VERSION,str);
    }
              //add Action element to ProductLink element:
    addElemContent(prodLinkElem,MsgTag.ACTION,
                              (deleteFlag ? MsgTag.DELETE : MsgTag.UPDATE));
              //add Code element to ProductLink element:
    if((str=propsMap.get("addon-code")) != null && str.length() > 0)
      addElemContent(prodLinkElem,MsgTag.CODE,str);
    else if((str=propsMap.get("code")) != null && str.length() > 0)
      addElemContent(prodLinkElem,MsgTag.CODE,str);
    else if((str=getProductIdCodeValue(productObj)).length() > 0)
      addElemContent(prodLinkElem,MsgTag.CODE,str);
    try
    {         //if comment text then add as Note element:
      if((str=propsMap.get("text")) != null && str.length() > 0)
        addElemContent(prodLinkElem,MsgTag.NOTE,Text.normalizeString(str));
      else if((str=getProductByteContentStr(productObj)) != null &&
                                                           str.length() > 0)
      {  //product byte content found; add as Note element
        addElemContent(prodLinkElem,MsgTag.NOTE,Text.normalizeString(str));
      }
    }
    catch(Exception ex)
    {  //exception probably from illegal chars; log it and move on
      logObj.warning("PdlProductConverter linkProductToRecObj (" +
                         productObj.getId() + ") 'Note' exception:  " + ex);
    }
              //add URL as Link element:
    if((str=propsMap.get("url")) != null && str.length() > 0)
      addElemContent(prodLinkElem,MsgTag.LINK,str);
    else if(!deleteFlag)
    {  //no URL property and not a delete message; log warning
      logObj.warning("PdlProductConverter linkProductToRecObj:  " +
                     "No URL property for product:  " + productObj.getId());
    }
    return eventChildToRecObj(prodLinkElem,propsMap,
                     getProductUpdateTimeLong(productObj),false,deleteFlag);
  }
  
  /**
   * Converts the given LossPAGER Product object to a message-record object.
   * @param productObj LossPAGER Product object to be converted.
   * @return A new 'QWConvMsgRecord' object.
   */
  protected QWConvMsgRecord losspagerProductToRecObj(Product productObj)
  {
    //create ProductLink element:
    final Element prodLinkElem =
        new Element(MsgTag.PRODUCT_LINK,namespaceObj);
    final Map<String,String> propsMap = productObj.getProperties();
    final Boolean deleteFlag =
        Product.STATUS_DELETE.equalsIgnoreCase(productObj.getStatus());
    //add TypeKey element to ProductLink element:
    addElemContent(prodLinkElem,MsgTag.TYPE_KEY,MsgTag.LossPAGER);
    String str;
    //add Version element to ProductLink element (if available):
    if((str=propsMap.get(Product.VERSION_PROPERTY)) != null &&
        str.length() > 0)
    {
      addElemContent(prodLinkElem,MsgTag.VERSION,str);
    }
    //add Action element to ProductLink element:
    addElemContent(prodLinkElem,MsgTag.ACTION,
        (deleteFlag ? MsgTag.DELETE : MsgTag.UPDATE));
    if((str=getProductIdCodeValue(productObj)).length() > 0)
      addElemContent(prodLinkElem,MsgTag.CODE,str);
    else
      addElemContent(prodLinkElem,MsgTag.CODE,MsgTag.LossPAGER);
    try
    {         //if comment text then add as Note element:
      if((str=getProductByteContentStr(productObj)) != null &&
          str.length() > 0)
      {  //product byte content found; add as Note element
        addElemContent(prodLinkElem,MsgTag.NOTE,Text.normalizeString(str));
      }
    }
    catch(Exception ex)
    {  //exception probably from illegal chars; log it and move on
      logObj.warning("PdlProductConverter losspagerProductToRecObj (" +
          productObj.getId() + ") 'Note' exception:  " + ex);
    }
    //add URL as Link element:
    if((str=propsMap.get("url")) == null || str.length() == 0)
    {
      // create URL
      str = PdlLinkCreator.getProductLink(losspagerUrlFormatStr, productObj);
      if (str != null)
      {
        logObj.debug(
            "PdlProductConverter losspagerProduct URL built (" + str + ")");
        propsMap.put("url", str);
      }
    }
    else
    {
      logObj.debug(
          "PdlProductConverter losspagerProduct URL input (" + str + ")");
    }    
    addElemContent(prodLinkElem,MsgTag.LINK,str);
    return eventChildToRecObj(prodLinkElem,propsMap,
        getProductUpdateTimeLong(productObj),false,deleteFlag);
  }

  /**
   * Converts the given moment-tensor Product object to a message-record object.
   * @param productObj moment-tensor Product object to be converted.
   * @return A new 'QWConvMsgRecord' object.
   */
  protected QWConvMsgRecord momentTensorProductToRecObj(Product productObj)
  {
    //create ProductLink element:
    final Element prodLinkElem =
        new Element(MsgTag.PRODUCT_LINK,namespaceObj);
    final Map<String,String> propsMap = productObj.getProperties();
    final Boolean deleteFlag =
        Product.STATUS_DELETE.equalsIgnoreCase(productObj.getStatus());
    //add TypeKey element to ProductLink element:
    addElemContent(prodLinkElem,MsgTag.TYPE_KEY,MsgTag.MomentTensor);
    String str;
    //add Version element to ProductLink element (if available):
    if((str=propsMap.get(Product.VERSION_PROPERTY)) != null &&
        str.length() > 0)
    {
      addElemContent(prodLinkElem,MsgTag.VERSION,str);
    }
    //add Action element to ProductLink element:
    addElemContent(prodLinkElem,MsgTag.ACTION,
        (deleteFlag ? MsgTag.DELETE : MsgTag.UPDATE));
    if((str=getProductIdCodeValue(productObj)).length() > 0)
      addElemContent(prodLinkElem,MsgTag.CODE,str);
    else
      addElemContent(prodLinkElem,MsgTag.CODE,MsgTag.MomentTensor);
    try
    {         //if comment text then add as Note element:
      if((str=getProductByteContentStr(productObj)) != null &&
          str.length() > 0)
      {  //product byte content found; add as Note element
        addElemContent(prodLinkElem,MsgTag.NOTE,Text.normalizeString(str));
      }
    }
    catch(Exception ex)
    {  //exception probably from illegal chars; log it and move on
      logObj.warning("PdlProductConverter momentTensorProductToRecObj (" +
          productObj.getId() + ") 'Note' exception:  " + ex);
    }
    //add URL as Link element:
    if((str=propsMap.get("url")) == null || str.length() == 0)
    {
      str = PdlLinkCreator.getProductLink(momentTensorUrlFormatStr, productObj);
      if (str != null)
      {
        logObj.debug(
            "PdlProductConverter momentTensorProduct URL built (" + str + ")");
        propsMap.put("url", str);
      }
    }
    else
    {
      logObj.debug(
          "PdlProductConverter momentTensorProduct URL input (" + str + ")");
    }
    addElemContent(prodLinkElem,MsgTag.LINK,str);
    return eventChildToRecObj(prodLinkElem,propsMap,
        getProductUpdateTimeLong(productObj),false,deleteFlag);
  }

  /**
   * Converts the given text-type Product object to a message-record object.
   * @param productObj text-type Product object to be converted.
   * @return A new 'QWConvMsgRecord' object.
   */
  protected QWConvMsgRecord textProductToRecObj(Product productObj)
  {
              //create Comment element:
    final Element commentElem = new Element(MsgTag.COMMENT,namespaceObj);
    final Map<String,String> propsMap = productObj.getProperties();
    final Boolean deleteFlag =
             Product.STATUS_DELETE.equalsIgnoreCase(productObj.getStatus());
    String str;
              //add Version element to Comment element (if available):
    if((str=propsMap.get(Product.VERSION_PROPERTY)) != null &&
                                                           str.length() > 0)
    {
      addElemContent(commentElem,MsgTag.VERSION,str);
    }
              //add Action element to Comment element:
    addElemContent(commentElem,MsgTag.ACTION,
                              (deleteFlag ? MsgTag.DELETE : MsgTag.UPDATE));
              //add 'code' (or 'addon-code') property as TypeKey element:
    if((str=propsMap.get("code")) != null && str.length() > 0)
      addElemContent(commentElem,MsgTag.TYPE_KEY,str);
    else if((str=propsMap.get("addon-code")) != null && str.length() > 0)
      addElemContent(commentElem,MsgTag.TYPE_KEY,str);
                   //add content text as Text element:
    String contentStr;
    if((contentStr=getProductByteContentStr(productObj)) == null)
    {  //no text content found
      contentStr = UtilFns.EMPTY_STRING;
      if(!deleteFlag)
      {  //no text content and not a delete message; log warning
        logObj.info("PdlProductConverter textProductToRecObj:  " +
                     "No text content for product:  " + productObj.getId());
      }
    }
    addElemContent(commentElem,MsgTag.TEXT,Text.normalizeString(contentStr));
                   //if URL property then add as Link element:
    if((str=propsMap.get("url")) != null && str.length() > 0)
      addElemContent(commentElem,MsgTag.LINK,str);
    return eventChildToRecObj(commentElem,propsMap,
                     getProductUpdateTimeLong(productObj),false,deleteFlag);
  }
  
  /**
   * Converts the given DYFI Product object to a message-record object.
   * @param productObj DYFI Product object to be converted.
   * @return A new 'QWConvMsgRecord' object.
   */
  protected QWConvMsgRecord dyfiProductToRecObj(Product productObj)
  {
              //create Product element:
    final Element prodLinkElem =
                              new Element(MsgTag.PRODUCT_LINK,namespaceObj);
    addElemContent(prodLinkElem,MsgTag.TYPE_KEY,MsgTag.DIDYOU_FEELIT);
    final Map<String,String> propsMap = productObj.getProperties();
    final Boolean deleteFlag =
             Product.STATUS_DELETE.equalsIgnoreCase(productObj.getStatus());
    String str;
              //add Version element to ProductLink element (if available):
    if((str=propsMap.get(Product.VERSION_PROPERTY)) != null &&
                                                           str.length() > 0)
    {
      addElemContent(prodLinkElem,MsgTag.VERSION,str);
    }
              //add Action element to ProductLink element:
    addElemContent(prodLinkElem,MsgTag.ACTION,
                              (deleteFlag ? MsgTag.DELETE : MsgTag.UPDATE));
              //if Code property then add to ProductLink element:
    if((str=propsMap.get("addon-code")) != null && str.length() > 0)
      addElemContent(prodLinkElem,MsgTag.CODE,str);
    else if((str=propsMap.get("code")) != null && str.length() > 0)
      addElemContent(prodLinkElem,MsgTag.CODE,str);
    else if((str=propsMap.get("addon-type")) != null && str.length() > 0)
      addElemContent(prodLinkElem,MsgTag.CODE,str);
    else
      addElemContent(prodLinkElem,MsgTag.CODE,"DYFI");
    str = null;
    try
    {         //if comment text then add as Note element:
      if((str=propsMap.get("text")) != null && str.length() > 0)
        addElemContent(prodLinkElem,MsgTag.NOTE,Text.normalizeString(str));
    }
    catch(Exception ex)
    {  //exception probably from illegal chars; log it and move on
      logObj.warning("PdlProductConverter dyfiProductToRecObj (" +
                         productObj.getId() + ") 'Note' exception:  " + ex);
    }
    if(str == null)
      addElemContent(prodLinkElem,MsgTag.NOTE,"Did you feel it?");
              //if URL property then use as Link element:
    if((str=propsMap.get("url")) != null && str.length() > 0)
      addElemContent(prodLinkElem,MsgTag.LINK,str);
    else
    {  //no URL property; build URL string from format
      if((str=productObj.getEventSourceCode()) != null && str.length() > 0)
      {  //event source code fetched OK
        str = PdlLinkCreator.getProductLink(dyfiUrlFormatStr, productObj);
        if (str != null)
        {
          logObj.debug(
              "PdlProductConverter dyfiProduct (" + str + ")");
          addElemContent(prodLinkElem,MsgTag.LINK,str);
        }
      }
      else if(!deleteFlag)        //if status=delete then no LINK is OK
      {  //no event source code and not delete message; use default URL
        logObj.warning("PdlProductConverter dyfiProductToRecObj:  " +
                     "'eventsourcecode' property not found for product:  " +
                                                        productObj.getId());
        str = "https://earthquake.usgs.gov/data/dyfi/";
        addElemContent(prodLinkElem,MsgTag.LINK, str);
      }
    }
    return eventChildToRecObj(prodLinkElem,propsMap,
                     getProductUpdateTimeLong(productObj),false,deleteFlag);
  }
  
  /**
   * Converts the given ShakeMap Product object to a message-record object.
   * @param productObj ShakeMap Product object to be converted.
   * @return A new 'QWConvMsgRecord' object.
   */
  protected QWConvMsgRecord shakeMapProductToRecObj(Product productObj)
  {
              //create Product element:
    final Element prodLinkElem =
                              new Element(MsgTag.PRODUCT_LINK,namespaceObj);
    String str;
              //add TypeKey element to ProductLink element:
    addElemContent(prodLinkElem,MsgTag.TYPE_KEY,MsgTag.SHAKEMAP_URL);
    final Map<String,String> propsMap = productObj.getProperties();
    final Boolean deleteFlag =
             Product.STATUS_DELETE.equalsIgnoreCase(productObj.getStatus());
              //create parser for ShakeMap-product data values:
    final ShakeMapMetadataParser parserObj = new ShakeMapMetadataParser(
                                                                productObj);
              //if version-code value was found then add Version element;
              // if delete msg with no version-code value then use "99"
              // so version code is higher than previous version codes:
    if(parserObj.versionStr != null && parserObj.versionStr.length() > 0)
      addElemContent(prodLinkElem,MsgTag.VERSION,parserObj.versionStr);
    else if(deleteFlag)
      addElemContent(prodLinkElem,MsgTag.VERSION,DEFAULT_SHAKEMAP_DEL_VERSION_STR);
              //add Action element to ProductLink element:
    addElemContent(prodLinkElem,MsgTag.ACTION,
                              (deleteFlag ? MsgTag.DELETE : MsgTag.UPDATE));
              //if "addon-code" property then add as Code element:
    if((str=propsMap.get("addon-code")) != null && str.length() > 0)
      addElemContent(prodLinkElem,MsgTag.CODE,str);
    try
    {         //if comment text then add as Note element:
      if((str=propsMap.get("text")) != null && str.length() > 0)
        addElemContent(prodLinkElem,MsgTag.NOTE,str);
      else if((str=propsMap.get("event-description")) != null &&
                                                           str.length() > 0)
      {
        addElemContent(prodLinkElem,MsgTag.NOTE,str);
      }
      else if(deleteFlag && (str=getProductByteContentStr(
                                   productObj)) != null && str.length() > 0)
      {  //delete msg and product byte content found; add as Note element
        addElemContent(prodLinkElem,MsgTag.NOTE,Text.normalizeString(str));
      }
    }
    catch(Exception ex)
    {  //exception probably from illegal chars; log it and move on
      logObj.warning("PdlProductConverter shakeMapProductToRecObj (" +
                         productObj.getId() + ") 'Note' exception:  " + ex);
    }
              //if URL-link value was found then add Link element:
    if(parserObj.linkUrlStr != null && parserObj.linkUrlStr.length() > 0)
    {
      addElemContent(prodLinkElem,MsgTag.LINK,parserObj.linkUrlStr);
    }
    else
    {  //unable to fetch or parse ShakeMap URL-link value
      if(!deleteFlag)        //if status=delete then no LINK is OK
      {  //message status is not 'delete'
        logObj.warning("PdlProductConverter shakeMapProductToRecObj:  " +
                    "Unable to parse URL from metadata.xml for product:  " +
                                                        productObj.getId());
                             //use fallback ShakeMap-URL prefix:
        String lnkStr = shakemapFallbackUrlPrefixStr;
        if((str=productObj.getEventSourceCode()) != null)
          lnkStr += str;
        addElemContent(prodLinkElem,MsgTag.LINK,lnkStr);
      }
    }
    return eventChildToRecObj(prodLinkElem,propsMap,
                     getProductUpdateTimeLong(productObj),false,deleteFlag);
  }

  /**
   * Returns the product-update epoch time as a long integer value.  This
   * is used as a message-number value in generated messages.
   * @param productObj Product object to use.
   * @return A long-integer value, or 0 if the value could not be fetched.
   */
  protected long getProductUpdateTimeLong(Product productObj)
  {
    try
    {
      return productObj.getId().getUpdateTime().getTime();
    }
    catch(Exception ex)
    {
      logObj.debug("PdlProductConverter getProductUpdateTimeLong (" +
                        ((productObj != null) ? productObj.getId() : null) +
                                                     ") exception:  " + ex);
    }
    return 0;
  }
  
  /**
   * Returns the code value for the product ID.  If the value is in the
   * form "eventid-code", the "eventid-" part is removed and the first
   * letter of the code value is capitalized.
   * @param productObj Product object to query.
   * @return Code value for product.
   */
  protected String getProductIdCodeValue(Product productObj)
  {
    try
    {
      String str;
      if((str=productObj.getId().getCode()) != null && str.length() > 0)
      {  //product ID code value found
        final String evtIdStr;
        if((evtIdStr=productObj.getEventId()) != null &&
                                     str.length() > evtIdStr.length() + 1 &&
                       str.toLowerCase().startsWith(evtIdStr.toLowerCase()))
        {  //code value starts with and is longer than event ID + 1
          str = str.substring(evtIdStr.length());     //remove event ID
          if(str.charAt(0) == '-')                    //remove '-' separator
            str = str.substring(1);         //capitalize first letter
          str = UtilFns.capFirstLetter(str);
        }
        return str;
      }
    }
    catch(Exception ex)
    {
      logObj.debug(
             "PdlProductConverter getProductIdCodeValue exception:  " + ex);
    }
    return UtilFns.EMPTY_STRING;
  }
  
  /**
   * Returns the byte content held by the given Product object.
   * @param productObj Product object to use.
   * @return A string containing the byte content, or null if none could
   * be fetched.
   */
  protected String getProductByteContentStr(Product productObj)
  {
	byte[] bytes;
	int length;
	String s;
    final Map<String,Content> contMap = productObj.getContents();
    if(contMap != null && contMap.size() > 0)
    {
      final StringBuilder sb = new StringBuilder();
      final Iterator<Content> iterObj = contMap.values().iterator();
      Content contObj;
      while(iterObj.hasNext())
      {
        if((contObj=iterObj.next()) instanceof ByteContent)
        {
          bytes = ((ByteContent)contObj).getByteArray();
          if (bytes.length == 0) // skip if empty
          {
        	  continue;
          }
          length = sb.length() + bytes.length;
          if (length > maxByteContentLength)
          {
        	  logObj.info(
        			  "PdlProductConverter getProductByteContentStr ignoring large byte content ("
        					  + bytes.length + ", " + length + "): " + productObj.getId());
        	  continue;
          }
          if (sb.length() > 0)
          {
        	  sb.append(", ");
          }
          s = new String(bytes);
          sb.append(s);
        }
      }
      if(sb.length() > 0)
      {
        return sb.toString();
      }
    }
    return null;
  }

  /**
   * Creates a new element object with the given name and content string.
   * @param nameStr name string to use.
   * @param contentStr content string to use.
   * @return A new 'Element' object.
   */
  protected Element createElemObj(String nameStr, String contentStr)
  {
    final Element elemObj = new Element(nameStr,namespaceObj);
    elemObj.setText(contentStr);
    return elemObj;
  }

  /**
   * Creates a new element object with the given name and content string
   * and adds it to the given parent element.
   * @param parentElem parent element object to use.
   * @param nameStr name string to use.
   * @param contentStr content string to use.
   * @return The newly-created element.
   */
  protected Element addElemContent(Element parentElem, String nameStr,
                                                          String contentStr)
  {
    final Element elemObj;
    parentElem.addContent(elemObj=createElemObj(nameStr,contentStr));
    return elemObj;
  }
    
  /**
   * Checks to see if the given string matches a type string in the
   * 'linkMsgTypesTable' and returns it if so (with correct case) or
   * 'MsgTag.LINK_URL'.
   * @param msgTypeStr message-type string to check.
   * @return A type string for use with a ProductLink message.
   */
  protected static String checkGetLinkMsgTypeKey(String msgTypeStr)
  {
    final String str;
    if(msgTypeStr != null && 
              (str=linkMsgTypesTable.get(msgTypeStr.toLowerCase())) != null)
    {  //match found
      return str;       //return table value (has correct case)
    }
    return MsgTag.LINK_URL;
  }
  
  /**
   * Class ShakeMapMetadataParser parses the given PDL ShakeMap product
   * properties and metadata for URL-link and version-code values.
   */
  public class ShakeMapMetadataParser
  {
         /** Parsed URL-link value, or null if none. */
    public String linkUrlStr = null;
         /** Parsed version-code value, or null if none. */
    public String versionStr = null;
    
    /**
     * Creates a PDL ShakeMap product data parser and parses the ShakeMap
     * data values, filling in the 'linkUrlStr' and 'versionStr' fields.
     * @param productObj ShakeMap product object.
     */
    public ShakeMapMetadataParser(Product productObj)
    {
      final Map<String,String> propsMap = productObj.getProperties();
      final Map<String,Content> contMap = productObj.getContents();
      try
      {
        String str;
              //attempt to fetch values from product properties:
        if((str=propsMap.get("url")) != null && str.length() > 0)
        {
          linkUrlStr = str;
          logObj.debug(
              "PdlProductConverter ShakeMapMetadataParser ShakeMapURL input (" +
              linkUrlStr + ")");
        }
        else 
        {  //format string for ShakeMapURLs contains data; build URL string
          linkUrlStr = PdlLinkCreator.getProductLink(
              shakemapUrlFormatStr, productObj);
          logObj.debug(
              "PdlProductConverter ShakeMapMetadataParser ShakeMapURL built (" +
              linkUrlStr + ")");
        }
        if((str=propsMap.get(Product.VERSION_PROPERTY)) != null &&
                                                           str.length() > 0)
        {
          versionStr = fixVerStr(str);
        }
        if(linkUrlStr == null || versionStr == null)
        {  //at least one value not found in properties
          final Content contObj;
          if((contObj=contMap.get("download/metadata.xml")) instanceof
                                                                FileContent)
          {  //metadata.xml content is available
            final IstiXmlUtils xmlUtilObj = new IstiXmlUtils();
            Element rootElemObj,elemObj;
            if(xmlUtilObj.loadFile(
                         ((FileContent)contObj).getFile().getPath(),null) &&
                          (rootElemObj=xmlUtilObj.getRootElement()) != null)
            {  //root element ("metadata") found OK
              if(linkUrlStr == null)
              {  //ShakeMap URL-link not already found in properties
                        //<metadata> | <idinfo> | <browse> |
                        //  <browsen>URL/intensity.html
                if((elemObj=IstiXmlUtils.getAnyNSChild(
                                           rootElemObj,"idinfo")) != null &&
                         (elemObj=IstiXmlUtils.getAnyNSChild(
                                               elemObj,"browse")) != null &&
                        (elemObj=IstiXmlUtils.getAnyNSChild(
                                                elemObj,"browsen")) != null)
                {  //target element found
                  str = elemObj.getTextTrim();
                  if(str.length() > 0 && str.startsWith("http"))
                  {  //element text is "http..." URL
                    linkUrlStr = str;
//                    testListenerObj.writeMessage("DEBUG link1str:  " + linkUrlStr);
                  }
                  else
                  {
                    logObj.debug("PdlProductConverter ShakeMapMetadataParser:  " +
                                      "ShakeMap link1str didn't start with http");
                  }
                }
                        //<metadata> | <distinfo> | <stdorder> | <digform> |
                        //  <digtopt> | <onlinopt> | <computer> | <networka> |
                        //  <networkr>URL/download/shape.zip</networkr>
                else if((elemObj=IstiXmlUtils.getAnyNSChild(
                                         rootElemObj,"distinfo")) != null &&
                       (elemObj=IstiXmlUtils.getAnyNSChild(
                                             elemObj,"stdorder")) != null &&
                        (elemObj=IstiXmlUtils.getAnyNSChild(
                                              elemObj,"digform")) != null &&
                        (elemObj=IstiXmlUtils.getAnyNSChild(
                                              elemObj,"digtopt")) != null &&
                       (elemObj=IstiXmlUtils.getAnyNSChild(
                                             elemObj,"onlinopt")) != null &&
                       (elemObj=IstiXmlUtils.getAnyNSChild(
                                             elemObj,"computer")) != null &&
                       (elemObj=IstiXmlUtils.getAnyNSChild(
                                             elemObj,"networka")) != null &&
                       (elemObj=IstiXmlUtils.getAnyNSChild(
                                               elemObj,"networkr")) != null)
                {  //target element found
                  str = elemObj.getTextTrim();
                  if(str.length() > 0 && str.startsWith("http"))
                  {  //element text is "http..." URL
                    final int p;  //truncate trailing "download/shape.zip"
                    if(str.endsWith(".zip") &&
                                      (p=str.lastIndexOf("/download/")) > 0)
                    {
                      str = str.substring(0,p+1);
                    }
//                    testListenerObj.writeMessage("DEBUG link2str:  " + str);
                    linkUrlStr = str;
                  }
                  else
                  {
                    logObj.debug(
                           "PdlProductConverter ShakeMapMetadataParser:  " +
                                "ShakeMap link2str didn't start with http");
                  }
                }
                else
                {
                  logObj.debug(
                           "PdlProductConverter ShakeMapMetadataParser:  " +
                                              "ShakeMap linkstr not found");
                }
              }
              if(versionStr == null)
              {  //version-code value not already found in properties
                        //<metadata> | <idinfo> | <citation> | <citeinfo> |
                        //  <edition>Map Version X; Code Version Y.Z
                if((elemObj=IstiXmlUtils.getAnyNSChild(
                                           rootElemObj,"idinfo")) != null &&
                         (elemObj=IstiXmlUtils.getAnyNSChild(
                                             elemObj,"citation")) != null &&
                        (elemObj=IstiXmlUtils.getAnyNSChild(
                                             elemObj,"citeinfo")) != null &&
                        (elemObj=IstiXmlUtils.getAnyNSChild(
                                                elemObj,"edition")) != null)
                {  //target element found
                  str = elemObj.getTextTrim();
                  final String preStr = "Map Version";
                  if(str.length() > 0 && str.startsWith(preStr))
                  {  //element text starts with "Map Version"
                    str = str.substring(preStr.length()).trim();
                    final int strLen = str.length();
                    int p = 0;         //parse version-code value
                    char ch;
                    while(p < strLen)
                    {  //scan until separator, whitespace or end found
                      ch = str.charAt(p);
                      if(ch == ';' || ch == ',' || Character.isWhitespace(ch))
                        break;
                      ++p;
                    }
                    if(p > 0)     //if version code found then accept it
                      versionStr = fixVerStr(str.substring(0,p));
                    else
                    {
                      logObj.debug("PdlProductConverter " +
                               "ShakeMapMetadataParser:  Unable to parse " +
                                            "ShakeMap 'Map Version' value");
                    }
                  }
                  else
                  {
                    logObj.debug("PdlProductConverter " +
                            "ShakeMapMetadataParser:  ShakeMap 'edition' " +
                                        "doesn't start with 'Map Version'");
                  }
                }
                else
                {
                  logObj.debug(
                           "PdlProductConverter ShakeMapMetadataParser:  " +
                                    "ShakeMap 'edition' element not found");
                }
              }
            }
          }
        }
      }
      catch(Exception ex)
      {
        logObj.warning(
            "PdlProductConverter ShakeMapMetadataParser exception:  " + ex);
        logObj.warning(UtilFns.getStackTraceString(ex));
      }
    }
    
    /**
     * Does on "fix" on the given version string by adding a leading zero
     * if the string is a single digit character.  This helps with
     * lexigraphical sorting of version codes (so "09" < "10").
     * @param str version string.
     * @return Fixed version string, or given string if it did not contain
     * a single digit character.
     */
    protected final String fixVerStr(String str)
    {
      if(str == null || str.length() != 1 ||
                                          !Character.isDigit(str.charAt(0)))
      {  //string is something other than a single digit
        return str;          //return given string
      }
      return "0" + str;      //return string with leading zero
    }
  }
}
