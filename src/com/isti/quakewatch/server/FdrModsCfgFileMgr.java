//FdrModsCfgFileMgr.java:  Manages a feeder-modules configuration file.
//
//  11/7/2006 -- [ET]
//

package com.isti.quakewatch.server;

import java.util.Iterator;
import java.io.ByteArrayInputStream;
import com.isti.util.LogFile;
import com.isti.util.IstiXmlUtils;
import com.isti.util.FifoHashtable;
import com.isti.util.FileDataMonitor;

/**
 * Class FdrModsCfgFileMgr manages a feeder-modules configuration file.
 */
public class FdrModsCfgFileMgr extends FileDataMonitor
{
    /** Value for root "QWFeederConfig" tag in config file. */
  public static final String QW_FEEDER_CONFIG = "QWFeederConfig";
  protected final ServerConfigProcessor serverConfigProcObj;
  protected final int logFilesMaxAgeInDays;
  protected final LogFile logObj;
  protected String lastErrorMsgStr = null;
  protected final IstiXmlUtils istiXmlUtilsObj = new IstiXmlUtils();

  /**
   * Creates a feeder-modules configuration file manager.
   * @param fdrCfgFileNameStr name of feeder-modules configuration file.
   * @param serverConfigProcObj server-configuration-processor object to
   * use.
   * @param logFilesMaxAgeInDays the maximum feeder log file age, in days.
   * @param logObj log file to use.
   */
  public FdrModsCfgFileMgr(String fdrCfgFileNameStr,
                                  ServerConfigProcessor serverConfigProcObj,
                                   int logFilesMaxAgeInDays, LogFile logObj)
  {
    super(fdrCfgFileNameStr);
    if(fdrCfgFileNameStr == null || serverConfigProcObj == null ||
                                                             logObj == null)
    {    //null parameter(s)
      throw new NullPointerException("FdrModsCfgFileMgr:  Null parameter");
    }
    if(fdrCfgFileNameStr.trim().length() <= 0)
    {    //filename parameter is empty
      throw new IllegalArgumentException(
              "FdrModsCfgFileMgr:  Parameter 'fdrCfgFileNameStr' is empty");
    }
    this.serverConfigProcObj = serverConfigProcObj;
    this.logFilesMaxAgeInDays = logFilesMaxAgeInDays;
    this.logObj = logObj;
  }

  /**
   * Checks if the feeder-modules configuration file has been modified
   * and returns a table containing instances of the 'QWFeederPlugin'
   * objects specified.  The first time this method is called the
   * specified 'QWFeederPlugin' objects will be returned.  Each object's
   * 'name', 'startupParamsStr' and log file setup are entered from the
   * configuration file.  The returned table contains entries where
   * the key is a string version of the "QWFeederMod" element and the
   * value is a feeder object (that implements the 'QWFeederPlugin'
   * interface).
   * @returns A table of objects that implement the 'QWFeederPlugin'
   * interface, or null if file has not been modified since the last
   * time this method was called (or if an error occurred).
   */
  public FifoHashtable checkReadQWFeederObjs()
  {
    final byte [] fileDataArr;    //fetch file data (if modified):
    if((fileDataArr=checkReadFileData(60000,65536)) ==
                                                FileDataMonitor.ERROR_ARRAY)
    {    //error reading file
      final String errStr = getErrorMessageString();
              //if error message not same as previous then log
              // at "warning" level; otherwise "debug" level:
      logObj.println(((lastErrorMsgStr == null ||
                                          !lastErrorMsgStr.equals(errStr)) ?
                                          LogFile.WARNING : LogFile.DEBUG2),
                    "Error reading feeder-modules config file:  " + errStr);
      lastErrorMsgStr = errStr;        //save error message
      return null;
    }
    lastErrorMsgStr = null;       //clear any previous error message

    if(fileDataArr != null)
    {    //data from config file is available
      try
      {       //read XML data from feeder-modules config file:
        if(!istiXmlUtilsObj.loadStream(
                     new ByteArrayInputStream(fileDataArr),QW_FEEDER_CONFIG,
                                                       getFileNameString()))
        {     //loading of XML data failed; log error message
          logObj.warning(
               "Error reading XML data from feeder-modules config file \"" +
                                             getFileNameString() + "\":  " +
                                         istiXmlUtilsObj.getErrorMessage());
          return null;
        }
              //process XML data from feeder-modules config file:
        final FifoHashtable fdrsTableObj;
        if((fdrsTableObj=serverConfigProcObj.setupFdrObjsFromElement(
            istiXmlUtilsObj.getRootElement(),logFilesMaxAgeInDays)) == null)
        {     //error processing XML data; log error message
          logObj.warning("Error processing XML data from " +
                                           "feeder-modules config file \"" +
                                             getFileNameString() + "\":  " +
                                     serverConfigProcObj.getErrorMessage());
          return null;
        }
        logObj.info("Loaded feeder-modules configuration " +
                                    "from \"" + getFileNameString() + "\"");
        return fdrsTableObj;
      }
      catch(Exception ex)
      {       //error processing data
        logObj.warning("FdrModsCfgFileMgr:  Error processing " +
                                           "feeder-modules config file \"" +
                                        getFileNameString() + "\":  " + ex);
        return null;
      }
    }
    else
    {    //data from config file not available (file not modified)
      logObj.debug4("FdrModsCfgFileMgr:  Feeder-modules config file " +
                                                             "not changed");
      return null;
    }
  }

  /**
   * Checks for feeder modules specified in the 'loadedFdrModsTable' being
   * duplicated in the 'configFeederModsTable'.  Any duplicate feeder
   * modules found are removed from the 'loadedFdrModsTable' and warning
   * messages are logged.
   * @param configFeederModsTable table of "current" feeder-module objects.
   * @param loadedFdrModsTable table of feeder-modules loaded from the
   * dynamic feeder-modules configuration file.
   * @param loadedFdrModsFileStr name of the dynamic feeder-modules
   * configuration file (for error messages).
   */
  public void checkFdrModsTableDups(FifoHashtable configFeederModsTable,
              FifoHashtable loadedFdrModsTable, String loadedFdrModsFileStr)
  {
    final Iterator iterObj = loadedFdrModsTable.getKeysVector().iterator();
    Object obj;
    while(iterObj.hasNext())
    {    //for each feeder-module entry in 'loadedFdrModsTable'
      if(configFeederModsTable.containsKey(obj=iterObj.next()))
      {  //'loadedFdrModsTable' entry duplicated in 'configFeederModsTable'
        obj = loadedFdrModsTable.remove(obj);
        logObj.warning("Duplicate feeder-module entry (\"" +
                                          ((obj instanceof QWFeederPlugin) ?
                            ((QWFeederPlugin)obj).getFeederName() : "???") +
                         "\") ignored in \"" + loadedFdrModsFileStr + "\"");
      }
    }
  }
}
