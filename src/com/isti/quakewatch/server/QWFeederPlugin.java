//QWFeederPlugin.java:  Defines the method used by QuakeWatch "feeder"
//                      modules to configure how they communicate with
//                      the server.
//
//  8/13/2002 -- [ET]  Initial version.
//  6/10/2003 -- [ET]  Added parameters to the 'setupLogFile()' method.
//  11/4/2003 -- [ET]  Added 'processFeederSettings()' and 'stopFeeder()'
//                     methods; added 'QWFeederInitException' class;
//                     changed 'QWFeederCallback' interface reference
//                     to 'QWServerCallback' class.
//   1/8/2004 -- [ET]  Added 'setFeederDomainTypeNames()' method.
//  1/28/2005 -- [ET]  Added 'getCallbackObj()' method.
//   2/3/2006 -- [ET]  Added 'getLogFileObj()' method.
// 10/18/2006 -- [ET]  Added 'isFeederStopped()' method.
//  11/4/2010 -- [ET]  Added 'getStatusErrorMsg()' and 'getLastDataMsgTime()'
//                     methods.
//

package com.isti.quakewatch.server;

import com.isti.util.LogFile;

/**
 * Interface QWFeederPlugin defines the method used by QuakeWatch "feeder"
 * modules to configure how they communicate with the server.  All "feeder"
 * modules will implement this inteface.
 */
interface QWFeederPlugin
{
  /**
   * Returns the revision String for the feeder.
   * @return the revision String for the feeder.
   */
  public String getRevisionString();

  /**
   * Sets the name for the feeder.  The name is usually loaded from the
   * QuakeWatch-server configuration file.
   * @param str the name to use.
   */
  public void setFeederName(String str);

  /**
   * Returns the name for the feeder.  The name is usually loaded from the
   * QuakeWatch-server configuration file.
   * @return The feeder name.
   */
  public String getFeederName();

  /**
   * Sets the startup parameters string for the feeder.  This string is
   * usually loaded from the QuakeWatch-server configuration file.
   * @param str the parameters string to use.
   */
  public void setStartupParamsStr(String str);

  /**
   * Returns the startup parameters string for the feeder.  This string is
   * usually loaded from the QuakeWatch-server configuration file.
   * @return the parameters string.
   */
  public String getStartupParamsStr();

  /**
   * Sets up the log file to be used with the feeder.
   * @param logNameStr name of log file, or null for none.
   * @param logFileLevel message level for log file output.
   * @param consoleLevel message level for console output.
   * @param useDateInFnameFlag true to use the date in the file name (and
   * create a new log file everytime the date changes).
   * @param logFileSwitchIntervalDays the minimum number of days between
   * log file switches via date changes.
   * @param maxLogFileAgeDays the maximum log file age, in days (if
   * 'useDateInFnameFlag' is true).
   */
  public void setupLogFile(String logNameStr,int logFileLevel,
                                int consoleLevel,boolean useDateInFnameFlag,
                       int logFileSwitchIntervalDays,int maxLogFileAgeDays);

  /**
   * Returns the log file object for the feeder.
   * @return The 'LogFile' object for the feeder.
   */
  public LogFile getLogFileObj();

  /**
   * Closes the log file for the feeder.
   */
  public void closeLogFile();

  /**
   * Enters the callback object to be used for communicating with
   * the QuakeWatch server.
   * @param callbackObj the callback object to use.
   */
  public void setCallbackObj(QWServerCallback callbackObj);

  /**
   * Returns the callback object used for communicating with the
   * QuakeWatch server.
   * @return The callback object to in use.
   */
  public QWServerCallback getCallbackObj();

  /**
   * Enters the event domain and type names to be used by this
   * feeder.
   * @param domainStr the event domain name to use, or null for none.
   * @param typeStr the event type name to use, or null for none.
   */
  public void setFeederDomainTypeNames(String domainStr,String typeStr);

  /**
   * Processes feeder-specific settings.  The settings are usually
   * fetched from the "QWFeederSettings" element in the QuakeWatch-server
   * configuration file.
   * @param settingsStr The settings string to be processed.
   * @throws QWFeederInitException if an error occurs while processing
   * the settings.
   */
  public void processFeederSettings(String settingsStr)
                                               throws QWFeederInitException;

  /**
   * Starts up the message collection threads of the feeder.
   */
  public void startFeeder();

  /**
   * Stops the message collection threads of the feeder.
   */
  public void stopFeeder();

  /**
   * Returns true if the 'stopFeeder()' method has been called and
   * has completed its work.
   * @return true if the 'stopFeeder()' method has been called and
   * has completed its work; false if not.
   */
  public boolean isFeederStopped();

  /**
   * Returns the current status-error message (if any) for the feeder.
   * @return The current status-error message for the feeder, or null
   * if none.
   */
  public String getStatusErrorMsg();

  /**
   * Returns the time that the last data message was received by the
   * feeder.
   * @return The time that the last data message was received by the
   * feeder (seconds since 1/1/1970), or 0 if none have been received.
   */
  public long getLastDataMsgTime();


  /**
   * Class QWFeederInitException defines an exception thrown while
   * initializing a feeder module.
   */
  public static class QWFeederInitException extends Exception
  {
    /**
     * Creates a feeder-module-initialization exception.
     * @param msgStr the information message string to use.
     */
    public QWFeederInitException(String msgStr)
    {
      super(msgStr);
    }
  }
}
