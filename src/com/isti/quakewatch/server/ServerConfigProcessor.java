//ServerConfigProcessor.java:  Processes an XML-format QuakeWatch server
//                             configuration file.

package com.isti.quakewatch.server;

import java.util.*;
import java.io.IOException;
import org.jdom.Element;
import com.isti.util.LogFile;
import com.isti.util.IstiXmlUtils;
import com.isti.util.XmlConfigLoader;
import com.isti.util.CfgProperties;
import com.isti.util.FifoHashtable;
import com.isti.quakewatch.server.outputter.QWOutputterPlugin;

/**
 * Class ServerConfigProcessor processes an XML-format QuakeWatch server
 * configuration file.
 */
public class ServerConfigProcessor extends XmlConfigLoader
{
    /** Value for "QWServerConfig" tag. */
  public static final String QW_SERVER_CONFIG = "QWServerConfig";
    /** Value for "Settings" tag. */
  public static final String SETTINGS = "Settings";
    /** Value for "QWFeederMod" tag. */
  public static final String QW_FEEDER_MOD = "QWFeederMod";
    /** Value for "QWOutputterMod" tag. */
  public static final String QW_OUTPUTTER_MOD = "QWOutputterMod";
    /** Value for "Name" tag. */
  public static final String NAME = "Name";
    /** Value for "Class" tag. */
  public static final String CLASS = "Class";
    /** Value for "Params" tag. */
  public static final String PARAMS = "Params";
    /** Value for "LogFileName" tag. */
  public static final String LOG_FILE_NAME = "LogFileName";
    /** Value for "LogFileMsgLevel" tag. */
  public static final String LOG_FILE_LEVEL = "LogFileLevel";
    /** Value for "ConsoleMsgLevel" tag. */
  public static final String CONSOLE_LEVEL = "ConsoleLevel";
    /** Value for "FeederDomainName" tag. */
  public static final String FEEDER_DOMAIN_NAME = "FeederDomainName";
    /** Value for "FeederTypeName" tag. */
  public static final String FEEDER_TYPE_NAME = "FeederTypeName";
    /** Value for "QWFeederSettings" tag. */
  public static final String QW_FEEDER_SETTINGS = "QWFeederSettings";
    /** Value for "QWOutputterSettings" tag. */
  public static final String QW_OUTPUTTER_SETTINGS = "QWOutputterSettings";
  protected final String cfgFileNameStr;    //handle for cfg file name
  protected boolean loadSettingsCalledFlag = false;

  /**
   * Creates an object for processing XML-format QuakeWatch server
   * configuration files.
   * @param cfgFileNameStr the name of the XML-format input file to
   * be read and processed.
   */
  public ServerConfigProcessor(String cfgFileNameStr)
  {
    this.cfgFileNameStr = cfgFileNameStr;
  }

  /**
   * Loads the XML-format configuration file and loads the set of
   * "name = value" items specified by the "Settings" element in
   * the configuration file.
   * @param cfgPropObj the 'CfgProperties' object to be loaded.
   * @param mustFlag if true and the "Settings" element is not found
   * then an error will be generated; if false and the "Settings" element
   * is not found then the method will simply return 'true'.
   * @param programArgs string array of command-line parameters to be
   * processed.
   * @return 'true' if successful, false if an error occurred (in which
   * case the 'getErrorMessage()' method may be used to fetch a
   * description of the error).
   */
  public boolean loadSettings(CfgProperties cfgPropObj,boolean mustFlag,
                                                      String [] programArgs)
  {
    loadSettingsCalledFlag = true;     //indicate method called
    return loadConfiguration(cfgFileNameStr,QW_SERVER_CONFIG,
                                  SETTINGS,mustFlag,cfgPropObj,programArgs);
  }

  /**
   * Fetches and creates instances for any 'QWFeederPlugin' objects
   * specified in the configuration file.  Each object's 'name',
   * 'startupParamsStr' and log file setup are entered from the
   * configuration file.  The returned table contains entries where
   * the key is a string version of the "QWFeederMod" element and the
   * value is a feeder object (that implements the 'QWFeederPlugin'
   * interface).
   * @param logFilesMaxAgeInDays the maximum log file age, in days.
   * @return A table of objects that implement the 'QWFeederPlugin'
   * interface, an empty list if none were found, or null if an error
   * occurred (in which case the 'getErrorMessage()' method may be used
   * to fetch a description of the error).
   */
  public FifoHashtable getQWFeederObjects(int logFilesMaxAgeInDays)
  {
    try
    {
      if(!loadSettingsCalledFlag)
      {    //root element not setup; set error message
        setErrorMessage("'loadSettings()' method not called before " +
                                           "'getQWFeederObjects()' method");
        return null;           //abort method
      }
      final Element rootElemObj;         //fetch and check root element
      if((rootElemObj=getRootElement()) == null)
      {    //root element not found; set error message
        setErrorMessage(
                  "Unable to load root element from configuration file \"" +
                                                     cfgFileNameStr + "\"");
        return null;           //abort method
      }
                //fetch and setup feeder object via element:
      final FifoHashtable tableObj;
      if((tableObj=setupFdrObjsFromElement(rootElemObj,logFilesMaxAgeInDays))
                                                                    == null)
      {    //error setting up feeders
        setErrorMessageString(
                  "Error processing feeder entry in configuration file \"" +
                              cfgFileNameStr + "\":  " + getErrorMessage());
        return null;
      }
      return tableObj;
    }
    catch(Throwable ex)
    {  //some kind of exception error; set error message
      setErrorMessageString(
        "Error creating feeder object from entry in configuration file \"" +
                                             cfgFileNameStr + "\":  " + ex);
        return null;
    }
  }

  /**
   * Fetches and creates instances for any 'QWFeederPlugin' objects
   * specified in the given elements.  Each object's 'name',
   * 'startupParamsStr' and log file setup are entered from the
   * the given elements.  The returned table contains entries where
   * the key is a string version of the "QWFeederMod" element and the
   * value is a feeder object (that implements the 'QWFeederPlugin'
   * interface).
   * @param fdrElemObj element containing one or more "QWFeederMod"
   * elements.
   * @param logFilesMaxAgeInDays the maximum log-file age for feeder
   * log file, in days.
   * @return A table of objects that implement the 'QWFeederPlugin'
   * interface, an empty list if none were found, or null if an error
   * occurred (in which case the 'getErrorMessage()' method may be used
   * to fetch a description of the error).
   */
  public FifoHashtable setupFdrObjsFromElement(Element fdrElemObj,
                                                   int logFilesMaxAgeInDays)
  {
         //get list of "QWFeederMod" elements:
    final List feederElemsList = fdrElemObj.getChildren(QW_FEEDER_MOD);
    final FifoHashtable retTable = new FifoHashtable(); //objs to be returned
    Object obj;
    Element elementObj;
    String elemNameStr,nameStr,classStr,paramsStr,logNameStr,str;
    Integer intObj;
    int logFileVal,consoleVal;
    String feederModStr,domainStr,typeStr;
    QWFeederPlugin feederPluginObj;
    final Iterator iterObj = feederElemsList.iterator();
    while(iterObj.hasNext())
    {    //for each "QWFeederMod" Element in List
      elemNameStr = null;
      if((obj=iterObj.next()) instanceof Element && QW_FEEDER_MOD.equals(
                       (elemNameStr=((elementObj=(Element)obj).getName()))))
      {  //Element fetched OK and name is "QWFeederMod"
        try
        {     //convert "QWFeederMod" element to a string:
          feederModStr = IstiXmlUtils.elementToString(elementObj);
        }
        catch(IOException ex)
        {     //error converting "QWFeederMod" element
          feederModStr = null;
        }
        if((str=elementObj.getText().trim()) != null && str.length() > 0)
        {     //non-attribute text data in element
          setErrorMessage("Unexpected text data found in \"" +
                                     QW_FEEDER_MOD + "\" element:  " + str);
          return null;            //abort method
        }
              //load Name attribute for QWFeederMod Element:
        if((nameStr=elementObj.getAttributeValue(NAME)) == null)
        {     //Name attribute not found
          setErrorMessage("No \"" + NAME + "\" attribute found in \"" +
                                              QW_FEEDER_MOD + "\" element");
          return null;            //abort method
        }
              //load Class attribute for QWFeederMod Element:
        if((classStr=elementObj.getAttributeValue(CLASS)) == null)
        {     //Class attribute not found
          setErrorMessage("No \"" + CLASS +
                                "\" attribute found in \"" + QW_FEEDER_MOD +
                                   "\" (Name=\"" + nameStr + "\") element");
          return null;            //abort method
        }
              //load optional Params attribute for QWFeederMod Element:
        paramsStr = elementObj.getAttributeValue(PARAMS);

              //load LogFileName attribute for QWFeederMod Element:
        logNameStr = elementObj.getAttributeValue(LOG_FILE_NAME);
              //get LogFileLevel attribute for QWFeederMod Element:
        if((str=elementObj.getAttributeValue(LOG_FILE_LEVEL)) != null)
        {     //LogFileLevel attribute was given; interpret it
          if((intObj=LogFile.levelStringToValue(str)) == null)
          {   //unable to match string; show error message
            setErrorMessage("Invalid value for \"" +
                                       LOG_FILE_LEVEL + "\" attribute (\"" +
                                         str + "\") in \"" + QW_FEEDER_MOD +
                                   "\" (Name=\"" + nameStr + "\") element");
            return null;
          }
          logFileVal = intObj.intValue();        //save value
        }
        else       //LogFileLevel attribute not given
          logFileVal = LogFile.ERROR;
              //get ConsoleLevel attribute for QWFeederMod Element:
        if((str=elementObj.getAttributeValue(CONSOLE_LEVEL)) != null)
        {     //ConsoleLevel attribute was given; interpret it
          if((intObj=LogFile.levelStringToValue(str)) == null)
          {   //unable to match string; show error message
            setErrorMessage("Invalid value for \"" +
                                        CONSOLE_LEVEL + "\" attribute (\"" +
                                         str + "\") in \"" + QW_FEEDER_MOD +
                                   "\" (Name=\"" + nameStr + "\") element");
            return null;
          }
          consoleVal = intObj.intValue();        //save value
        }
        else       //ConsoleLevel attribute not given
          consoleVal = LogFile.ERROR;
              //load FeederDomainName attribute for QWFeederMod Element:
        domainStr = elementObj.getAttributeValue(FEEDER_DOMAIN_NAME);
              //load FeederTypeName attribute for QWFeederMod Element:
        typeStr = elementObj.getAttributeValue(FEEDER_TYPE_NAME);
        if((domainStr == null || domainStr.length() <= 0) &&
                                 (typeStr == null || typeStr.length() <= 0))
        {     //no event domain or type name data given
          domainStr = typeStr = null;       //indicate no domain/type names
        }
              //fetch any given "QWFeederSettings" elements:
        final String feederSettingsStr;
              //get list of "QWFeederSettings" elements:
        final List settingsElemsList =
                                 elementObj.getChildren(QW_FEEDER_SETTINGS);
        if(settingsElemsList.size() > 0)
        {     //at least one "QWFeederSettings" element found
          if(settingsElemsList.size() > 1)
          {   //more than one "QWFeederSettings" element found
            setErrorMessage("More than one \"" + QW_FEEDER_SETTINGS +
                                  "\" element found in \"" + QW_FEEDER_MOD +
                                   "\" (Name=\"" + nameStr + "\") element");
            return null;
          }
              //fetch "QWFeederSettings" element object:
          if(!((obj=settingsElemsList.get(0)) instanceof Element) ||
                                                 !QW_FEEDER_SETTINGS.equals(
                       (elemNameStr=((elementObj=(Element)obj).getName()))))
          {   //unable to fetch Element or name is not "QWFeederSettings"
            setErrorMessage("Error fetching \"" + QW_FEEDER_SETTINGS +
                        "\" element in \"" + QW_FEEDER_MOD + "\" (Name=\"" +
                                  nameStr + "\") element (found name = \"" +
                                                       elemNameStr + "\")");
            return null;
          }
                   //fetch text inside "QWFeederSettings" element:
          feederSettingsStr = elementObj.getText();
        }
        else  //no "QWFeederSettings" elements found
          feederSettingsStr = null;
        try
        {          //load class object for String name:
          obj = Class.forName(classStr).getDeclaredConstructor().newInstance();
        }
        catch(Throwable ex)
        {          //error loading class object for String name:
          setErrorMessage("Unable to load class \"" + classStr +
                               "\" for \"" + QW_FEEDER_MOD + "\" (Name=\"" +
                                           nameStr + "\") element:  " + ex);
          return null;            //abort method
        }
        if(!(obj instanceof QWFeederPlugin))
        {     //class is not correct type
          setErrorMessage("Class does not implement 'QWFeederPlugin'" +
                             " interface for \"" + classStr + "\" from \"" +
                                   QW_FEEDER_MOD + "\" (Name=\"" + nameStr +
                                                             "\") element");
          return null;            //abort method
        }
        feederPluginObj = (QWFeederPlugin)obj;        //set handle to object
        feederPluginObj.setFeederName(nameStr);       //set feeder name
        feederPluginObj.setStartupParamsStr(paramsStr);    //set startup str
                                            //setup log file for feeder:
        feederPluginObj.setupLogFile(QWServer.fromBaseDir(logNameStr),
                         logFileVal,consoleVal,true,1,logFilesMaxAgeInDays);
                                            //setup domain/type names:
        feederPluginObj.setFeederDomainTypeNames(domainStr,typeStr);
        if(feederSettingsStr != null)
        {     //'QWFeederSettings' items were found
          try      //process 'QWFeederSettings' items:
          {
            feederPluginObj.processFeederSettings(feederSettingsStr);
          }
          catch(QWFeederPlugin.QWFeederInitException ex)
          {        //error processing settings
            setErrorMessage("Error processing \"" + QW_FEEDER_SETTINGS +
                       "\" for \"" + QW_FEEDER_MOD + "\" (Name=\"" + nameStr +
                                                  "\"):  " + ex.getMessage());
            return null;            //abort method
          }
        }
        if(feederModStr != null && feederModStr.trim().length() > 0)
        {     //string version of "QWFeederMod" element contains data
          if(retTable.containsKey(feederModStr))
          {   //duplicate entry for feeder
            setErrorMessage("Duplicate \"" + QW_FEEDER_MOD +
                                      "\" entry; name=\"" + nameStr + "\"");
            return null;            //abort method
          }
        }
        else  //string version of "QWFeederMod" element empty
        {               //use string version of hashcode:
          feederModStr = Integer.toHexString(elementObj.hashCode());
                        //make sure key is unique in table:
          if(retTable.containsKey(feederModStr))
            feederModStr = (new Object()).toString();
        }
              //add feeder module to table:
        retTable.put(feederModStr,feederPluginObj);
      }
      else
      {  //unable to fetch Element or name is not "QWFeederMod"; set err msg
        setErrorMessage("Error fetching \"" + QW_FEEDER_MOD +
                       "\" element (found name = \"" + elemNameStr + "\")");
        return null;              //abort method
      }
    }
    return retTable;
  }

  /**
   * Fetches and creates instances for any 'QWOutputterPlugin' objects
   * specified in the configuration file.  Each object's 'name' and log
   * file setup are entered from the configuration file.
   * @param logFilesMaxAgeInDays the maximum log file age, in days.
   * @return A list of objects that implement the 'QWOutputterPlugin'
   * interface, an empty list if none were found, or null if an error
   * occurred (in which case the 'getErrorMessage()' method may be used
   * to fetch a description of the error).
   */
  public List getQWOutputterObjects(int logFilesMaxAgeInDays)
  {
    if(!loadSettingsCalledFlag)
    {    //root element not setup; set error message
      setErrorMessage("'loadSettings()' method not called before " +
                                        "'getQWOutputterObjects()' method");
      return null;           //abort method
    }
    final Element rootElemObj;         //fetch and check root element
    if((rootElemObj=getRootElement()) == null)
    {    //root element not found; set error message
      setErrorMessage(
                  "Unable to load root element from configuration file \"" +
                                                     cfgFileNameStr + "\"");
      return null;           //abort method
    }
              //fetch and setup outputter object via element:
    final List listObj;
    if((listObj=setupOutptrObjsFromElement(rootElemObj,logFilesMaxAgeInDays))
                                                                    == null)
    {    //error setting up outputters
      setErrorMessageString(
            "Error processing outputter entry in in configuration file \"" +
                              cfgFileNameStr + "\":  " + getErrorMessage());
      return null;
    }
    return listObj;
  }

  /**
   * Fetches and creates instances for any 'QWOutputterPlugin' objects
   * specified in the given elements.  Each object's 'name' and log
   * file setup are entered from the the given elements.
   * @param outptrElemObj element containing one or more "QWOutputterMod"
   * elements.
   * @param logFilesMaxAgeInDays the maximum log-file age for outputter
   * log file, in days.
   * @return A list of objects that implement the 'QWOutputterPlugin'
   * interface, an empty list if none were found, or null if an error
   * occurred (in which case the 'getErrorMessage()' method may be used
   * to fetch a description of the error).
   */
  public List setupOutptrObjsFromElement(Element outptrElemObj,
                                                   int logFilesMaxAgeInDays)
  {
         //get list of "QWOutputterMod" elements:
    final List outptrElemsList = outptrElemObj.getChildren(QW_OUTPUTTER_MOD);
    final List retListObj = new ArrayList();     //objs to be returned
    Object obj;
    Element elementObj;
    String elemNameStr,nameStr,classStr,logNameStr,str;
    Integer intObj;
    int logFileVal,consoleVal;
    QWOutputterPlugin outptrPluginObj;
    final Iterator iterObj = outptrElemsList.iterator();
    while(iterObj.hasNext())
    {    //for each "QWOutputterMod" Element in List
      elemNameStr = null;
      if((obj=iterObj.next()) instanceof Element && QW_OUTPUTTER_MOD.equals(
                       (elemNameStr=((elementObj=(Element)obj).getName()))))
      {  //Element fetched OK and name is "QWOutputterMod"
        if((str=elementObj.getText().trim()) != null && str.length() > 0)
        {     //non-attribute text data in element
          setErrorMessage("Unexpected text data found in \"" +
                                  QW_OUTPUTTER_MOD + "\" element:  " + str);
          return null;            //abort method
        }
              //load Name attribute for QWOutputterMod Element:
        if((nameStr=elementObj.getAttributeValue(NAME)) == null)
        {     //Name attribute not found
          setErrorMessage("No \"" + NAME + "\" attribute found in \"" +
                                           QW_OUTPUTTER_MOD + "\" element");
          return null;            //abort method
        }
              //load Class attribute for QWOutputterMod Element:
        if((classStr=elementObj.getAttributeValue(CLASS)) == null)
        {     //Class attribute not found
          setErrorMessage("No \"" + CLASS +
                             "\" attribute found in \"" + QW_OUTPUTTER_MOD +
                                   "\" (Name=\"" + nameStr + "\") element");
          return null;            //abort method
        }

              //load LogFileName attribute for QWOutputterMod Element:
        logNameStr = elementObj.getAttributeValue(LOG_FILE_NAME);
              //get LogFileLevel attribute for QWOutputterMod Element:
        if((str=elementObj.getAttributeValue(LOG_FILE_LEVEL)) != null)
        {     //LogFileLevel attribute was given; interpret it
          if((intObj=LogFile.levelStringToValue(str)) == null)
          {   //unable to match string; show error message
            setErrorMessage("Invalid value for \"" +
                                       LOG_FILE_LEVEL + "\" attribute (\"" +
                                      str + "\") in \"" + QW_OUTPUTTER_MOD +
                                   "\" (Name=\"" + nameStr + "\") element");
            return null;
          }
          logFileVal = intObj.intValue();        //save value
        }
        else       //LogFileLevel attribute not given
          logFileVal = LogFile.ERROR;
              //get ConsoleLevel attribute for QWOutputterMod Element:
        if((str=elementObj.getAttributeValue(CONSOLE_LEVEL)) != null)
        {     //ConsoleLevel attribute was given; interpret it
          if((intObj=LogFile.levelStringToValue(str)) == null)
          {   //unable to match string; show error message
            setErrorMessage("Invalid value for \"" +
                                        CONSOLE_LEVEL + "\" attribute (\"" +
                                      str + "\") in \"" + QW_OUTPUTTER_MOD +
                                   "\" (Name=\"" + nameStr + "\") element");
            return null;
          }
          consoleVal = intObj.intValue();        //save value
        }
        else       //ConsoleLevel attribute not given
          consoleVal = LogFile.ERROR;
              //fetch any given "QWOutputterSettings" elements:
        final String outptrSettingsStr;
              //get list of "QWOutputterSettings" elements:
        final List settingsElemsList =
                              elementObj.getChildren(QW_OUTPUTTER_SETTINGS);
        if(settingsElemsList.size() > 0)
        {     //at least one "QWOutputterSettings" element found
          if(settingsElemsList.size() > 1)
          {   //more than one "QWOutputterSettings" element found
            setErrorMessage("More than one \"" + QW_OUTPUTTER_SETTINGS +
                               "\" element found in \"" + QW_OUTPUTTER_MOD +
                                   "\" (Name=\"" + nameStr + "\") element");
            return null;
          }
              //fetch "QWOutputterSettings" element object:
          if(!((obj=settingsElemsList.get(0)) instanceof Element) ||
                                              !QW_OUTPUTTER_SETTINGS.equals(
                       (elemNameStr=((elementObj=(Element)obj).getName()))))
          {   //unable to fetch Element or name is not "QWOutputterSettings"
            setErrorMessage("Error fetching \"" + QW_OUTPUTTER_SETTINGS +
                     "\" element in \"" + QW_OUTPUTTER_MOD + "\" (Name=\"" +
                                  nameStr + "\") element (found name = \"" +
                                                       elemNameStr + "\")");
            return null;
          }
                   //fetch text inside "QWOutputterSettings" element:
          outptrSettingsStr = elementObj.getText();
        }
        else  //no "QWOutputterSettings" elements found
          outptrSettingsStr = null;
        try
        {          //load class object for String name:
          obj = Class.forName(classStr).getDeclaredConstructor().newInstance();
        }
        catch(Throwable ex)
        {          //error loading class object for String name:
          setErrorMessage("Unable to load class \"" + classStr +
                            "\" for \"" + QW_OUTPUTTER_MOD + "\" (Name=\"" +
                                           nameStr + "\") element:  " + ex);
          return null;            //abort method
        }
        if(!(obj instanceof QWOutputterPlugin))
        {     //class is not correct type
          setErrorMessage("Class does not implement 'QWOutputterPlugin'" +
                             " interface for \"" + classStr + "\" from \"" +
                                QW_OUTPUTTER_MOD + "\" (Name=\"" + nameStr +
                                                             "\") element");
          return null;            //abort method
        }
        outptrPluginObj = (QWOutputterPlugin)obj;        //set handle to object
        outptrPluginObj.setOutputterName(nameStr);       //set outputter name
                                            //setup log file for outputter:
        outptrPluginObj.setupLogFile(QWServer.fromBaseDir(logNameStr),
                         logFileVal,consoleVal,true,1,logFilesMaxAgeInDays);
        if(outptrSettingsStr != null)
        {     //'QWOutputterSettings' items were found
          try      //process 'QWOutputterSettings' items:
          {
            outptrPluginObj.processOutputterSettings(outptrSettingsStr);
          }
          catch(QWOutputterPlugin.QWOutputterInitException ex)
          {        //error processing settings
            setErrorMessage("Error processing \"" + QW_OUTPUTTER_SETTINGS +
                    "\" for \"" + QW_OUTPUTTER_MOD + "\" (Name=\"" + nameStr +
                                                  "\"):  " + ex.getMessage());
            return null;            //abort method
          }
        }
              //add outputter module to list:
        retListObj.add(outptrPluginObj);
      }
      else
      {  //unable to fetch Element or name is not "QWOutputterMod"; set err msg
        setErrorMessage("Error fetching \"" + QW_OUTPUTTER_MOD +
                       "\" element (found name = \"" + elemNameStr + "\")");
        return null;              //abort method
      }
    }
    return retListObj;
  }
}
