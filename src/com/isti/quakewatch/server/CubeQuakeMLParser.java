//CubeQuakeMLParser.java:  Parses CUBE-format messages into a JDOM/XML
//                         tree of elements.  The generated XML conforms
//                         to the QuakeML format.
//
//  4/15/2010 -- [ET]  Initial version created from 'CubeAnssEQParser'.
//

package com.isti.quakewatch.server;

import java.util.Date;
import java.text.NumberFormat;
import java.text.ParseException;
import org.jdom.Element;
import org.jdom.Attribute;
import org.jdom.Text;
import org.jdom.Namespace;
import com.isti.util.UtilFns;
import com.isti.util.TwoStringBlock;
import com.isti.util.gis.GisUtils;
import com.isti.quakewatch.common.MsgTag;
import com.isti.quakewatch.util.CubeCodeTranslator;

/**
 * Class CubeQuakeMLParser parses CUBE-format messages into a JDOM/XML
 * tree of elements.  The generated XML conforms to the QuakeML
 * format.
 */
public class CubeQuakeMLParser extends CubeParserSupport
                                                    implements MsgParserIntf
{
    /** Namespace object for elements. */
  public static final Namespace namespaceObj =
                       Namespace.getNamespace(MsgTag.QUAKEML_NAMESPACE_STR);
    /** Namespace object for "anss". */
  public static final Namespace anssNamespaceObj =
          Namespace.getNamespace(MsgTag.ANSS_STR,MsgTag.ANSS_NAMESPACE_STR);
    /** Floating-point formatter object that can show # as integer. */
  protected final NumberFormat decIntValFormatObj =
                                                 NumberFormat.getInstance();

  /**
   * Creates QuakeML-to-CUBE parsing object.
   */
  public CubeQuakeMLParser()
  {
                   //set number formatter for no trailing ".0" if integer:
    decIntValFormatObj.setMinimumFractionDigits(0);
                   //set maximum number of digits after decimal point:
    decIntValFormatObj.setMaximumFractionDigits(MAX_DECFMT_DIGITS);
    decIntValFormatObj.setGroupingUsed(false);      //set no commas in output
  }

  /**
   * Parses CUBE-format message data into a JDOM/XML tree of elements.
   * @param messageString the message string to be parsed.
   * @param timeReceivedStr current time (as an XML-format time string).
   * @param actionAttribObj attrib whose value will be set by this method.
   * @return The resulting "quakeMessage" Element object, or null if an
   * error occured (in which case the associated error message may be
   * fetched via the 'getErrorMessage()' method).
   */
  protected Element parseCubeMsgStringData(String messageString,
                          String timeReceivedStr, Attribute actionAttribObj)
  {
    final int msgStrLen;
    if((msgStrLen=messageString.length()) < 12)
    {    //too few character; set error message
      setErrorMessage("Not enough characters in message");
      return null;
    }
         //identify message type:
    final int msgTypeIndex;
    if((msgTypeIndex=msgStringsVec.indexOf(
                                         messageString.substring(0,2))) < 0)
    {    //message ID chars not matched; set error message
      setErrorMessage("Unexpected message type (\"" +
                                      messageString.substring(0,2) + "\")");
      return null;
    }
         //save identification "number" for event:
    final String eventIdNumStr = messageString.substring(2,10).trim();
         //save the 2-character source code string:
    final String sourceCodeStr = messageString.substring(10,12).trim();
         //save upper-case version of source-code string:
    final String srcCodeUCStr = sourceCodeStr.toUpperCase();
         //create 'quakeMessage' element:
    final Element quakeMessageElem =
                             new Element(MsgTag.QUAKE_MESSAGE,namespaceObj);
                        //add software-module element:
    addElemContent(quakeMessageElem,MsgTag.QUAKEML_MODULE,
                                                  QWServer.REVISION_STRING);
         //add creation-info element (don't put in 'creationTime' because
         // inhibits detecting duplicate messages in QWServer cache):
    quakeMessageElem.addContent(getCreationInfoElem(sourceCodeStr));
    final String evtIdAttribStr =      //build event-ID-attribute string
                              getEvtIdAttribStr(eventIdNumStr,srcCodeUCStr);
    String str,msgVersionStr,msgCommentStr;

         //process rest of data based on message type:
    switch(msgTypeIndex)
    {
      case EQUAKE_MSGIDX:         //Earthquake
        if(msgStrLen < 43)
        {     //not enough characters for lat/lon values; set error message
          setErrorMessage("Not enough characters in Earthquake message");
          return null;
        }
              //set action attribute to "Update":
        actionAttribObj.setValue(MsgTag.UPDATE);
              //add 'quakeMessage' namespace declaration for 'anss':
        quakeMessageElem.addNamespaceDeclaration(anssNamespaceObj);
              //set 'usage' to "Actual" (CUBE msgs always "Actual"):
        addElemContent(quakeMessageElem,MsgTag.QUAKEML_USAGE,MsgTag.ACTUAL);
              //set 'scope' to "Public" (will change if locMeth=='Z'):
        final Element eventScopeElem = addElemContent(
                       quakeMessageElem,MsgTag.QUAKEML_SCOPE,MsgTag.PUBLIC);
              //create 'quakeml' element:
        final Element quakemlElem =
                                  new Element(MsgTag.QUAKE_ML,namespaceObj);
              //add 'quakeml' element to 'eventParameters' element:
        quakeMessageElem.addContent(quakemlElem);
              //create 'eventParameters' element:
        final Element evtParamsElem =
                          new Element(MsgTag.EVENT_PARAMETERS,namespaceObj);
                   //set 'publicID' attribute for 'eventParameters' element:
        evtParamsElem.setAttribute(MsgTag.PUBLIC_ID,MsgTag.EVENT_PARAMS_ID);
              //add 'eventParameters' element to 'quakeml' element:
        quakemlElem.addContent(evtParamsElem);
              //create 'event' element:
        final Element eventElem =
                             new Element(MsgTag.QUAKEML_EVENT,namespaceObj);
                   //set 'publicID' attribute for 'event' element:
        eventElem.setAttribute(MsgTag.PUBLIC_ID,evtIdAttribStr);
              //add 'event' element to 'eventParameters' element:
        evtParamsElem.addContent(eventElem);

        final String originIDStr =     //generate origin-ID value
                              getOrgIdAttribStr(eventIdNumStr,srcCodeUCStr);
              //add 'preferredOriginID' element to 'event' element:
        addElemContent(eventElem,MsgTag.PREFERRED_ORIGIN_ID,originIDStr);
                   //create 'preferredMagnitudeID' element
        final Element prefMagIDElem =       // (to be filled in later)
                    new Element(MsgTag.PREFERRED_MAGNITUDE_ID,namespaceObj);
              //add 'preferredMagnitudeID' element to 'event' element:
        eventElem.addContent(prefMagIDElem);
              //add 'type' element to 'event' element; save handle to elem:
        final Element eventTypeElemObj = addElemContent(
                   eventElem,MsgTag.QUAKEML_TYPE,MsgTag.QUAKEML_EARTHQUAKE);
              //add 'creationInfo' element to 'event', with version string:
        eventElem.addContent(getCreationInfoElem(
                        sourceCodeStr,null,messageString.substring(12,13)));
              //create 'origin' element:
        final Element originElem =
                            new Element(MsgTag.QUAKEML_ORIGIN,namespaceObj);
                   //set 'publicID' attribute for 'origin' element:
        originElem.setAttribute(MsgTag.PUBLIC_ID,
                             getOrgIdAttribStr(eventIdNumStr,srcCodeUCStr));
              //process date string:
        String dateStr = messageString.substring(13,28);
        final Date dateObj;            //date/time for event
        try        //parse date/time (add "00" to end to make milliseconds
        {          // and replace any spaces with zeroes):
          synchronized(cubeDateFormatterObj)
          {    //only allow one thread at a time to use date-format object
            dateObj = cubeDateFormatterObj.parse(
                                           dateStr.replace(' ','0') + "00");
          }
        }
        catch(ParseException ex)
        {                    //error parsing date/time; set message
          setErrorMessage("Unable to parse date/time (\"" + dateStr + "\")");
          return null;
        }
              //process latitude value:
        final Double latObj;
        if((latObj=parseCubicDouble(messageString.substring(28,35),
                                               10000.0,"latitude")) == null)
        {     //error parsing numeric string
          return null;
        }
              //process longitude value:
        final Double lonObj;
        if((lonObj=parseCubicDouble(messageString.substring(35,43),
                                              10000.0,"longitude")) == null)
        {     //error parsing numeric string
          return null;
        }
              //add 'origin' element to 'event' element:
        eventElem.addContent(originElem);
                   //set event-origin time:
        synchronized(xmlDateFormatterObj)
        {     //only allow one thread at a time to use date-format object
          addElemValueContent(originElem,MsgTag.QUAKEML_TIME,
                                       xmlDateFormatterObj.format(dateObj));
        }
                   //set latitude value:
        addElemValueContent(originElem,MsgTag.QUAKEML_LATITUDE,
                                            decValFormatObj.format(latObj));
                   //set longitude value:
        addElemValueContent(originElem,MsgTag.QUAKEML_LONGITUDE,
                                            decValFormatObj.format(lonObj));
                   //process depth value:
        if((str=getMsgSubstring(messageString,msgStrLen,43,47)) == null)
        {  //no more data; remove empty 'preferredMagnitudeID' element
          eventElem.removeContent(prefMagIDElem);
          break;
        }
        Double dObj;
        if(str.trim().length() > 0 && !str.startsWith(ASTERISK_STR))
        {     //item contains data
          if((dObj=parseCubicDouble(str,10.0,"depth")) == null)
            return null;
              //enter value as 'depth' element of 'origin' element
              // (convert kilometers to meters):
          addElemValueContent(originElem,MsgTag.QUAKEML_DEPTH,
                           decIntValFormatObj.format(dObj.longValue()*1000.0));
        }
                   //process magnitude value:
        if((str=getMsgSubstring(messageString,msgStrLen,47,49)) == null)
        {  //no more data; remove empty 'preferredMagnitudeID' element
          eventElem.removeContent(prefMagIDElem);
          break;
        }
        final Element magnitudeElem;   //element for magnitude
        final Element magMagElem;      //magnitude child-element 'mag'
        if(str.trim().length() > 0 && !str.startsWith(ASTERISK_STR))
        {     //item contains data
          if((dObj=parseCubicDouble(str,10.0,"magnitude")) == null)
            return null;
              //create 'magnitude' element:
          magnitudeElem = new Element(MsgTag.QUAKEML_MAGNITUDE,namespaceObj);
              //add 'mag' element with value to 'magnitude' element:
          magMagElem = addElemValueContent(magnitudeElem,MsgTag.MAG,
                                              decValFormatObj.format(dObj));
        }
        else
        {     //magnitude item contains no data
          magnitudeElem = magMagElem = null;
                   //remove empty 'preferredMagnitudeID' element:
          eventElem.removeContent(prefMagIDElem);
        }
        Integer iObj, numStaIntObj = null, numPhaIntObj = null;
        Double minDistDblObj = null, stdErrDblObj = null,
               horizErrDblObj = null, vertErrDblObj = null,
               azimGapDblObj = null;
        Element magEvalStatusElem = null;   //'evaluationStatus' for mag
        boolean evalStatusRevFlag = false;  //true if eval-status "reviewed"
        String methClassStr = null, locMethStr = null;
                   //process "# of stations" value:
        if((str=getMsgSubstring(messageString,msgStrLen,49,52)) != null)
        {     //more data left to process
          if(str.trim().length() > 0 && !str.startsWith(ASTERISK_STR))
          {   //item contains data
            if((numStaIntObj=parseCubicInteger(str,"# of stations")) == null)
              return null;
          }
                   //process "# of phases" value:
          if((str=getMsgSubstring(messageString,msgStrLen,52,55)) != null)
          {   //more data left to process
            if(str.trim().length() > 0 && !str.startsWith(ASTERISK_STR))
            {   //item contains data
              if((numPhaIntObj=parseCubicInteger(str,"# of phases")) == null)
                return null;
            }
                   //process "minimum distance" value:
            if((str=getMsgSubstring(messageString,msgStrLen,55,59)) != null)
            {      //more data left to process
              if(str.trim().length() > 0 && !str.startsWith(ASTERISK_STR))
              {    //item contains data
                if((minDistDblObj=parseCubicDouble(
                                      str,10.0,"minimum distance")) == null)
                {
                  return null;
                }
              }
                   //process "RMS time error" value:
              if((str=getMsgSubstring(
                                    messageString,msgStrLen,59,63)) != null)
              {    //more data left to process
                if(str.trim().length() > 0 && !str.startsWith(ASTERISK_STR))
                {  //item contains data
                  if((stdErrDblObj=parseCubicDouble(
                                       str,100.0,"RMS time error")) == null)
                  {
                    return null;
                  }
                }
                   //process "horizontal error" value:
                if((str=getMsgSubstring(
                                    messageString,msgStrLen,63,67)) != null)
                {  //more data left to process
                  if(str.trim().length() > 0 &&
                                              !str.startsWith(ASTERISK_STR))
                  {   //item contains data
                    if((horizErrDblObj=parseCubicDouble(
                                      str,10.0,"horizontal error")) == null)
                    {
                      return null;
                    }
                  }
                        //process "vertical error" value:
                  if((str=getMsgSubstring(
                                    messageString,msgStrLen,67,71)) != null)
                  {  //more data left to process
                    if(str.trim().length() > 0
                                           && !str.startsWith(ASTERISK_STR))
                    {   //item contains data
                      if((vertErrDblObj=parseCubicDouble(
                                        str,10.0,"vertical error")) == null)
                      {
                        return null;
                      }
                    }
                        //process "azimuthal gap" value:
                    if((str=getMsgSubstring(
                                    messageString,msgStrLen,71,73)) != null)
                    {   //more data left to process
                      if(str.trim().length() > 0 &&
                                              !str.startsWith(ASTERISK_STR))
                      {   //item contains data; (convert % of circle to deg)
                        if((azimGapDblObj=parseCubicDouble(
                                      str,(1/3.6),"azimuthal gap")) == null)
                        {
                          return null;
                        }
                      }
                      if(magnitudeElem != null)
                      {   //magnitude value was parsed previously
                        String magTypeStr,magTransStr;
                               //process "magnitude type" code:
                        if((magTypeStr=getMsgSubstring(
                                  messageString,msgStrLen,73,74)) != null &&
                                             magTypeStr.trim().length() > 0)
                        {    //item contains data
                               //translate magnitude-character code:
                          magTransStr = translateCubeMagTypeCode(magTypeStr);
                               //enter as 'type' element of 'magnitude':
                          addElemContent(magnitudeElem,MsgTag.QUAKEML_TYPE,
                                                               magTransStr);
                        }
                        else
                          magTypeStr = magTransStr = UtilFns.EMPTY_STRING;
                                  //build public-ID string for magnitude:
                        final String magIdStr = getMagIdAttribStr(
                                    eventIdNumStr,srcCodeUCStr,magTransStr);
                                  //set 'publicID' attribute on 'magnitude':
                        magnitudeElem.setAttribute(MsgTag.PUBLIC_ID,
                                                                  magIdStr);
                                  //set text on 'preferredMagnitudeID':
                        prefMagIDElem.setText(magIdStr);
                               //add 'originID' element to 'magnitude' elem:
                        addElemContent(magnitudeElem,MsgTag.ORIGIN_ID,
                                                               originIDStr);
                             //process "# of mag stations" value:
                        if(msgStrLen >= 75)
                        {  //more data left to process
                             //process "# of mag stations" value:
                          if((str=getMsgSubstring(
                                  messageString,msgStrLen,74,76)) != null &&
                                                  str.trim().length() > 0 &&
                                              !str.startsWith(ASTERISK_STR))
                          {      //item contains data
                            if((iObj=parseCubicInteger(
                                          str,"# of mag stations")) == null)
                            {
                              return null;
                            }
                                  //add 'stationCount' elem to 'magnitude':
                            addElemContent(magnitudeElem,
                                      MsgTag.STATION_COUNT,iObj.toString());
                          }
                             //process "magnitude error" value:
                          if((str=getMsgSubstring(
                                    messageString,msgStrLen,76,78)) != null)
                          {  //more data left to process
                            if(str.trim().length() > 0 &&
                                              !str.startsWith(ASTERISK_STR))
                            { //item contains data
                              if((dObj=parseCubicDouble(
                                       str,10.0,"magnitude error")) == null)
                              {
                                return null;
                              }
                              if(magMagElem != null)
                              {  //elem OK; add 'uncertainty' elem to 'mag'
                                addElemContent(magMagElem,MsgTag.UNCERTAINTY,
                                              decValFormatObj.format(dObj));
                              }
                            }
                          }
                        }
                             //add 'evaluationStatus' element to 'magnitude'
                             // (may be updated later):
                        magEvalStatusElem = addElemContent(magnitudeElem,
                               MsgTag.EVALUATION_STATUS,MsgTag.PRELIMINARY);
                             //add 'creationInfo' element to 'magnitude':
                        magnitudeElem.addContent(
                                        getCreationInfoElem(sourceCodeStr));
                             //add 'comment' element to 'magnitude':
                        if(magTypeStr.length() > 0)
                        {   //mag-type available; put into 'comment' element
                          magnitudeElem.addContent(getCommentElem(
                                                 MsgTag.CUBECODE_MAGTYPE_ID,
                                          (MsgTag.CUBE_CODE + magTypeStr)));
                        }
                      }
                             //process "location method" code:
                      if((str=getMsgSubstring(
                                    messageString,msgStrLen,78,79)) != null)
                      {  //more data left to process
                        if(str.trim().length() > 0)
                        {  //item contains data
                                  //translate CUBE location-method code to
                                  // 'Algorithm' and 'Class' strings:
                          final TwoStringBlock blockObj =
                                            translateCubeLocMethodCode(str);
                                  //add 'methodID' element to 'origin' elem:
                          addElemContent(originElem,MsgTag.METHOD_ID,
                              (MsgTag.METHOD_ID_PREFIX + blockObj.string1));
                                  //save method-class string:
                          methClassStr = blockObj.string2;
                                  //save CUBE-code location-method string:
                          locMethStr = MsgTag.CUBE_CODE + str;
                          if(Character.isLowerCase(str.charAt(0)))
                          {  //"location method" code character is lower case
                            evalStatusRevFlag = true; //indicate "reviewed"
                                  //if magnitude 'evaluationStatus' element
                                  // setup then set value to "reviewed":
                            if(magEvalStatusElem != null)
                            {
                              magEvalStatusElem.setText(
                                                   MsgTag.QUAKEML_REVIEWED);
                            }
                          }
                                  //if location-method code 'Z' then
                                  // change 'scope' to "Internal":
                          if(str.equalsIgnoreCase(LOCMETH_INTERNAL_STR))
                            eventScopeElem.setText(MsgTag.INTERNAL);
                                  //if location method is 'Q' (for "probable
                                  // quarry explosion") then change
                                  // 'event'|'type' to "quarry":
                          if(str.equalsIgnoreCase(LOCMETH_QUARRY_STR))
                            eventTypeElemObj.setText(MsgTag.QUAKEML_QUARRY);
                        }
                              //process check character:
                        if((str=getMsgSubstring(
                                    messageString,msgStrLen,79,80)) != null)
                        {  //more data left to process
                          if(str.charAt(0) != calcMenloCheckChar(
                                             messageString.substring(0,79)))
                          {  //check character mismatch
                            final char rCh = str.charAt(0);
                            final char cCh = calcMenloCheckChar(
                                             messageString.substring(0,79));
                            setErrorMessage("Mismatch in check character " +
                                   "for message, recv'd=" + (int)rCh + "(" +
                                        rCh + ") calc'd=" + (int)cCh + "(" +
                                                                 cCh + ")");
                            return null;
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
              //create 'quality' element:
        final Element qualityElem = new Element(MsgTag.QUALITY,namespaceObj);
        if(numPhaIntObj != null)
        {  //value contains data
              //enter value as 'usedPhaseCount' element of 'quality' elem:
          addElemContent(qualityElem,MsgTag.USED_PHASE_COUNT,
                                                   numPhaIntObj.toString());
        }
        if(numStaIntObj != null)
        {  //value contains data
              //enter value as 'usedStationCount' element of 'quality' elem:
          addElemContent(qualityElem,MsgTag.USED_STATION_COUNT,
                                                   numStaIntObj.toString());
        }
        if(stdErrDblObj != null)
        {  //value contains data
              //enter value as 'standardError' element of 'quality' element:
          addElemContent(qualityElem,MsgTag.STANDARD_ERROR,
                                      decValFormatObj.format(stdErrDblObj));
        }
        if(azimGapDblObj != null)
        {  //value contains data
              //enter value as 'azimuthalGap' element of 'quality' element:
          addElemContent(qualityElem,MsgTag.QUAKEML_AZIMUTHAL_GAP,
                                     decValFormatObj.format(azimGapDblObj));
        }
        if(minDistDblObj != null)
        {  //value contains data
              //enter value as 'minimumDistance' element of 'quality' elem:
          addElemContent(qualityElem,MsgTag.MINIMUM_DISTANCE,
                                                     decValFormatObj.format(
                        GisUtils.kmToDegrees(minDistDblObj.doubleValue())));
        }
        double dblVal;
        if(horizErrDblObj != null &&
                               (dblVal=horizErrDblObj.doubleValue()) != 0.0)
        {  //value contains data and is non-zero
              //enter value as 'anss:errh' element of 'quality' element:
          final Element elemObj =                //create child element:
                          new Element(MsgTag.QUAKEML_ERRH,anssNamespaceObj);
                             //set numeric value (convert km to meters):
          elemObj.setText(decIntValFormatObj.format(dblVal*1000.0));
          qualityElem.addContent(elemObj);       //add to 'quality' element
        }
        if(vertErrDblObj != null &&
                                (dblVal=vertErrDblObj.doubleValue()) != 0.0)
        {  //value contains data and is non-zero
              //enter value as 'anss:errz' element of 'quality' element:
          final Element elemObj =                //create child element:
                          new Element(MsgTag.QUAKEML_ERRZ,anssNamespaceObj);
                             //set numeric value (convert km to meters):
          elemObj.setText(decIntValFormatObj.format(dblVal*1000.0));
          qualityElem.addContent(elemObj);       //add to 'quality' element
        }
              //if 'quality' element not empty then add to 'origin' element:
        if(!qualityElem.getChildren().isEmpty())
          originElem.addContent(qualityElem);
              //add 'evaluationStatus' element to 'origin' element:
        addElemContent(originElem,MsgTag.EVALUATION_STATUS,
                              (evalStatusRevFlag ? MsgTag.QUAKEML_REVIEWED :
                                                       MsgTag.PRELIMINARY));
              //add 'creationInfo' element to 'origin' element:
        originElem.addContent(getCreationInfoElem(sourceCodeStr));
              //add method-class 'comment' element to 'origin' element:
        if(methClassStr != null)
        {  //method-class string was setup
          originElem.addContent(getCommentElem(
                                      MsgTag.METHOD_CLASS_ID,methClassStr));
        }
              //add location-method 'comment' element to 'origin' element:
        if(locMethStr != null)
        {  //CUBE-code location-method string was setup
          originElem.addContent(getCommentElem(
                                    MsgTag.CUBECODE_LOCMETH_ID,locMethStr));
        }
              //if created then add 'magnitude' element to 'event' element:
        if(magnitudeElem != null)
          eventElem.addContent(magnitudeElem);
        break;

      case DELQK_MSGIDX:     //Delete Earthquake
              //set action attribute to "Delete":
        actionAttribObj.setValue(MsgTag.DELETE);
        if(msgStrLen > 12)
        {  //message has version code; save it
          msgVersionStr = messageString.substring(12,13);
              //fetch comment data (if any):
          msgCommentStr = (msgStrLen > 14) ?
                  messageString.substring(14).trim() : UtilFns.EMPTY_STRING;
        }
        else  //no more data; indicate no version and comment data
          msgVersionStr = msgCommentStr = null;
              //create and add 'useControl' element to 'quakeMessage':
        quakeMessageElem.addContent(getUseControlElem(evtIdAttribStr,
                        msgVersionStr,MsgTag.QUAKEML_DELETE,msgCommentStr));
        break;

      case TEXT_MSGIDX:      //Text Comment
        if(msgStrLen < 14)
        {     //not enough characters for version chars; set error message
          setErrorMessage("Not enough characters in Text Comment message");
          return null;
        }
                   //get version string for message:
        msgVersionStr = messageString.substring(12,14).trim();
        final String textStr;
        if((str=messageString.substring(14).trim()).length() > 0)
        {  //text data exists; normalize and save it
          textStr = Text.normalizeString(str);
              //set action attribute to "Update":
          actionAttribObj.setValue(MsgTag.UPDATE);
        }
        else
        {  //no text data
          textStr = UtilFns.EMPTY_STRING;        //empty string for delete
              //set action attribute to "Delete"
          actionAttribObj.setValue(MsgTag.DELETE);
        }
              //create 'comment' element:
        final Element commentElem = getCommentElem(evtIdAttribStr,textStr);
        if(msgVersionStr.length() > 0)
        {  //version string not empty; add 'creationInfo' element with str
          commentElem.addContent(getCreationInfoElem(
                                                  null,null,msgVersionStr));
        }
              //add 'comment' element to 'quakeMessage' element:
        quakeMessageElem.addContent(commentElem);
        break;

      case LINK_MSGIDX:      //Link to Add-on
        if(msgStrLen < 15)
        {     //not enough characters for addon-type value; set error msg
          setErrorMessage("Not enough characters in Link to Addon message");
          return null;
        }
              //process "type url comment" string in addon message:
        int ePos, sPos = 14;
              //scan through any leading whitespace:
        while(Character.isWhitespace(messageString.charAt(sPos)) &&
                                                        ++sPos < msgStrLen);
              //find next whitespace separator after data:
        if((ePos=UtilFns.indexOfWhitespace(messageString,sPos)) < 0)
          ePos = msgStrLen;       //if not found then use end of string pos
                   //get addon-type code string:
        final String prodCodeStr = messageString.substring(sPos,ePos).trim();
                   //translate CUBE addon-type code to QW product code:
        String prodTypeStr,pTypeIdStr;
        if((prodTypeStr=getProdTypeXlatEntry(prodCodeStr)) != null)
          pTypeIdStr = prodTypeStr;    //use QW code in 'publicID' attrib
        else
        {  //code not matched; use "LinkURL" for product type
          prodTypeStr = MsgTag.LINK_URL;
                             //use "LinkURL(x)" in 'publicID' attribute:
          pTypeIdStr = MsgTag.LINK_URL + '(' + prodCodeStr + ')';
        }
              //build string for 'publicID' or 'elementID' value:
        final String prodIdStr = getPrdIdAttribStr(
                                     eventIdNumStr,srcCodeUCStr,pTypeIdStr);
        boolean prodUpdateFlag = true;     //true = update; false = delete
        String prodLinkStr = null, prodNoteStr = null;
        if((sPos=ePos+1) < msgStrLen)
        {     //more data in message string
                   //scan through any trailing whitespace:
          while(Character.isWhitespace(messageString.charAt(sPos)) &&
                                                        ++sPos < msgStrLen);
                   //find next whitespace separator after data:
          if((ePos=UtilFns.indexOfWhitespace(messageString,sPos)) < 0)
            ePos = msgStrLen;     //if not found then use end of string pos
                   //save addon-URL value:
          prodLinkStr = messageString.substring(sPos,ePos).trim();
          if((sPos=ePos+1) < msgStrLen)
          {   //more data in message string
            if((str=messageString.substring(sPos).trim()).length() > 0)
            {     //description text exists
              if(str.equalsIgnoreCase("delete")) //if "delete" then
                prodUpdateFlag = false;          //indicate delete Action
              else //action is not "delete"
              {         //save as 'comment' text (take out any CR/LF chars):
                prodNoteStr = Text.normalizeString(str);
                if(prodCodeStr.equalsIgnoreCase(TYPE_FR_STR))
                {  //CUBE product type code is "fr"
                  if(str.indexOf(RTMECH_KEYSTR) >= 0)
                  {     //comment makes it a real-time focal mechanism URL
                    prodTypeStr = MsgTag.REALTIME_MECH_URL;
                  }
                  else if(str.indexOf(FEELIT_KEYSTR) >= 0)
                  {     //comment makes it a "Did you feel it?" URL
                    prodTypeStr = MsgTag.DIDYOU_FEELIT;
                  }
                }
              }
            }
          }
        }
              //get product-version string:
        final String prodVerStr = messageString.substring(12,14).trim();
              //if action is "update" then set action attribute to "Update":
        if(prodUpdateFlag)
        {  //action is "update"; set action attribute to "Update"
          actionAttribObj.setValue(MsgTag.UPDATE);
                   //create 'productLink' element:
          final Element prodLinkElem =
                      new Element(MsgTag.QUAKEML_PRODUCT_LINK,namespaceObj);
                   //add 'publicID' attribute to 'productLink' element:
          prodLinkElem.setAttribute(MsgTag.PUBLIC_ID,prodIdStr);
                   //add 'type' element to 'productLink' element:
          addElemContent(prodLinkElem,MsgTag.QUAKEML_TYPE,prodTypeStr);
                   //add 'code' element (set to addon-type value):
          addElemContent(prodLinkElem,MsgTag.QUAKEML_CODE,prodCodeStr);
          if(prodLinkStr != null)   //if URL text then add 'link' element
            addElemContent(prodLinkElem,MsgTag.QUAKEML_LINK,prodLinkStr);
                   //get version code and add 'creationInfo' element
                   // with version string to 'productLink' element:
          prodLinkElem.addContent(getCreationInfoElem(sourceCodeStr,null,
                                                               prodVerStr));
          if(prodNoteStr != null)   //if comment text then add 'comment' elem
            prodLinkElem.addContent(getCommentElem(null,prodNoteStr));
                   //add 'productLink' element to 'quakeMessage' element:
          quakeMessageElem.addContent(prodLinkElem);
        }
        else  //action is "delete"
        {          //set action attribute to "Delete":
          actionAttribObj.setValue(MsgTag.DELETE);
                   //create 'useControl' element with "delete" 'action':
          final Element useCtrlElem = getUseControlElem(
                           prodIdStr,prodVerStr,MsgTag.QUAKEML_DELETE,null);
                   //add code 'comment' element to 'useControl' element:
          useCtrlElem.addContent(getCommentElem((MsgTag.SMI_LOCAL_STR +
                   MsgTag.QUAKEML_PRODUCT_LINK + '#' + MsgTag.QUAKEML_CODE),
                                                              prodCodeStr));
                   //add link 'comment' element to 'useControl' element:
          useCtrlElem.addContent(getCommentElem((MsgTag.SMI_LOCAL_STR +
                   MsgTag.QUAKEML_PRODUCT_LINK + '#' + MsgTag.QUAKEML_LINK),
                                                              prodLinkStr));
                   //add 'useControl' element to 'quakeMessage' element:
          quakeMessageElem.addContent(useCtrlElem);
        }
        break;

      case TRUMP_MSGIDX:  //Trump event
              //set action attribute to "Trump":
        actionAttribObj.setValue(MsgTag.TRUMP);
        if(msgStrLen > 12)
        {  //message has version code; save it
          msgVersionStr = messageString.substring(12,13);
              //fetch comment data (if any):
          msgCommentStr = (msgStrLen > 14) ?
                  messageString.substring(14).trim() : UtilFns.EMPTY_STRING;
        }
        else  //no more data; indicate no version and comment data
          msgVersionStr = msgCommentStr = null;
              //create and add 'useControl' element to 'quakeMessage':
        quakeMessageElem.addContent(getUseControlElem(evtIdAttribStr,
                         msgVersionStr,MsgTag.QUAKEML_TRUMP,msgCommentStr));
        break;

      default:
        setErrorMessage("Unexpected message type (\"" +
                                      messageString.substring(0,2) + "\")");
        return null;
    }

    return quakeMessageElem;      //return generated elements
  }

  /**
   * Returns an event-ID-attribute string, in the form
   * "smi:local/NNNNNNNN#evt_SS", where NNNNNNNN is the event-ID
   * number and SS is the source code.
   * @param eventIdNumStr event-ID-number string.
   * @param sourceCodeStr source-code string.
   * @return An event-ID-attribute string.
   */
  protected String getEvtIdAttribStr(String eventIdNumStr,
                                                       String sourceCodeStr)
  {
    return MsgTag.SMI_LOCAL_STR + eventIdNumStr + MsgTag.EVT_ID_STR +
                                                              sourceCodeStr;
  }

  /**
   * Returns an origin-ID-attribute string, in the form
   * "smi:local/NNNNNNNN#org_SS", where NNNNNNNN is the event-ID
   * number and SS is the source code.
   * @param eventIdNumStr event-ID-number string.
   * @param sourceCodeStr source-code string.
   * @return An origin-ID-attribute string.
   */
  protected String getOrgIdAttribStr(String eventIdNumStr,
                                                       String sourceCodeStr)
  {
    return MsgTag.SMI_LOCAL_STR + eventIdNumStr + MsgTag.ORG_ID_STR +
                                                              sourceCodeStr;
  }

  /**
   * Returns an magnitude-ID-attribute string, in the form
   * "smi:local/NNNNNNNN#mag_SS_MM", where NNNNNNNN is the event-ID
   * number, SS is the source code and MM is the magnitude type.
   * @param eventIdNumStr event-ID-number string.
   * @param sourceCodeStr source-code string.
   * @param magTypeStr magnitude-type string, or null or empty/blank
   * string for none.
   * @return An origin-ID-attribute string.
   */
  protected String getMagIdAttribStr(String eventIdNumStr,
                                    String sourceCodeStr, String magTypeStr)
  {
    return MsgTag.SMI_LOCAL_STR + eventIdNumStr +
                                         MsgTag.MAG_ID_STR + sourceCodeStr +
                   ((magTypeStr != null && magTypeStr.trim().length() > 0) ?
                                 ('_' + magTypeStr) : UtilFns.EMPTY_STRING);
  }

  /**
   * Returns a product-ID-attribute string, in the form
   * "smi:local/NNNNNNNN#prd_SS_TYPE", where NNNNNNNN is the event-ID
   * number, SS is the source code, and TYPE is the product type.
   * @param eventIdNumStr event-ID-number string.
   * @param sourceCodeStr source-code string.
   * @param pTypeIdStr product-type string, or null or empty/blank
   * string for none.
   * @return A product-ID-attribute string.
   */
  protected String getPrdIdAttribStr(String eventIdNumStr,
                             String sourceCodeStr, String pTypeIdStr)
  {
    return MsgTag.SMI_LOCAL_STR + eventIdNumStr +
                                         MsgTag.PRD_ID_STR + sourceCodeStr +
                   ((pTypeIdStr != null && pTypeIdStr.trim().length() > 0) ?
                                 ('_' + pTypeIdStr) : UtilFns.EMPTY_STRING);
  }

  /**
   * Builds and returns a 'creationInfo' element.
   * @param agencyIDStr agency-ID string, or null for none.
   * @param creationTimeStr creation-time string, or null for none.
   * @param versionStr version string, or null for none.
   * @return A new 'creationInfo' element
   */
  protected Element getCreationInfoElem(String agencyIDStr,
                                  String creationTimeStr, String versionStr)
  {
                                       //make 'creationInfo' element:
    final Element elemObj = new Element(MsgTag.CREATION_INFO,namespaceObj);
    if(agencyIDStr != null)            //if given then add 'agencyID' element
      addElemContent(elemObj,MsgTag.AGENCY_ID,agencyIDStr);
    if(creationTimeStr != null)        //if given then creation-time element
      addElemContent(elemObj,MsgTag.CREATION_TIME,creationTimeStr);
    if(versionStr != null)             //if given then add 'version' element
      addElemContent(elemObj,MsgTag.QUAKEML_VERSION,versionStr);
    return elemObj;
  }

  /**
   * Builds and returns a 'creationInfo' element.
   * @param agencyIDStr agency-ID string.
   * @param creationTimeStr creation-time string, or null for none.
   * @return A new 'creationInfo' element
   */
  protected Element getCreationInfoElem(String agencyIDStr,
                                                     String creationTimeStr)
  {
    return getCreationInfoElem(agencyIDStr,creationTimeStr,null);
  }

  /**
   * Builds and returns a 'creationInfo' element.
   * @param agencyIDStr agency-ID string.
   * @return A new 'creationInfo' element
   */
  protected Element getCreationInfoElem(String agencyIDStr)
  {
    return getCreationInfoElem(agencyIDStr,null,null);
  }

  /**
   * Builds and returns a 'comment' element.
   * @param idStr value for 'id' attribute, or null or empty string
   * for none.
   * @param textStr value for 'text' child element.
   * @return A new 'comment' element
   */
  protected Element getCommentElem(String idStr, String textStr)
  {
                                       //make 'comment' element:
    final Element elemObj = new Element(MsgTag.QUAKEML_COMMENT,namespaceObj);
    if(idStr != null && idStr.length() > 0)      //if 'id' string given then
      elemObj.setAttribute(MsgTag.ID_STR,idStr); //add 'id' attribute
    addElemContent(elemObj,MsgTag.QUAKEML_TEXT,textStr);   //add 'text' elem
    return elemObj;
  }

  /**
   * Builds and returns a 'useControl' element.
   * @param elementIDStr element ID.
   * @param versionStr version string, or null or empty string for none.
   * @param actionStr action.
   * @param commentStr comment string, or null or empty string for none.
   * @return A new 'useControl' element
   */
  protected Element getUseControlElem(String elementIDStr,
                     String versionStr, String actionStr, String commentStr)
  {
         //create 'useControl' element object:
    final Element useControlElem =
                               new Element(MsgTag.USE_CONTROL,namespaceObj);
         //if element ID given then add 'elementID' element:
    if(elementIDStr != null && elementIDStr.length() > 0)
      addElemContent(useControlElem,MsgTag.ELEMENT_ID,elementIDStr);
         //if version string given then add 'version' element:
    if(versionStr != null && versionStr.length() > 0)
      addElemContent(useControlElem,MsgTag.QUAKEML_VERSION,versionStr);
         //add 'action' element:
    addElemContent(useControlElem,MsgTag.QUAKEML_ACTION,actionStr);
         //if comment string given then add 'comment' element:
    if(commentStr != null && commentStr.length() > 0)
      useControlElem.addContent(getCommentElem(null,commentStr));
    return useControlElem;
  }

  /**
   * Creates a new element object with the given name and content string.
   * @param nameStr name string to use.
   * @param contentStr content string to use.
   * @return A new 'Element' object.
   */
  protected Element createElemObj(String nameStr, String contentStr)
  {
    final Element elemObj = new Element(nameStr,namespaceObj);
    elemObj.setText(contentStr);
    return elemObj;
  }

  /**
   * Creates a new element object with the given name and content string
   * and adds it to the given parent element.
   * @param parentElem parent element object to use.
   * @param nameStr name string to use.
   * @param contentStr content string to use.
   * @return The newly-created element.
   */
  protected Element addElemContent(Element parentElem, String nameStr,
                                                          String contentStr)
  {
    final Element elemObj;
    parentElem.addContent(elemObj=createElemObj(nameStr,contentStr));
    return elemObj;
  }

  /**
   * Creates a new element object with the given name and content string
   * and adds it to the given parent element.  The content is placed
   * inside of a 'value' sub-element.
   * @param parentElem parent element object to use.
   * @param nameStr name string to use.
   * @param contentStr content string to use.
   * @return The newly-created element.
   */
  protected Element addElemValueContent(Element parentElem, String nameStr,
                                                          String contentStr)
  {
              //create named element:
    final Element elemObj = new Element(nameStr,namespaceObj);
              //create 'value' element with content; add to named element:
    elemObj.addContent(createElemObj(MsgTag.QUAKEML_VALUE,contentStr));
    parentElem.addContent(elemObj);    //add named element to parent
    return elemObj;
  }

  /**
   * Translates the given CUBE-format magnitude-type code (i.e. "W")
   * to its standard type code (i.e. "Mw").
   * @param codeStr CUBE-format magnitude-type code string.
   * @return The converted magnitude-type code string, or the given code
   * string if it could not be converted.
   */
  public String translateCubeMagTypeCode(String codeStr)
  {
    String typeStr;
    return (codeStr != null &&
            (typeStr=CubeCodeTranslator.fromCubeMagType(codeStr)) != null) ?
                                                          typeStr : codeStr;
  }

  /**
   * Translates the given CUBE-format location-method code (i.e. "H")
   * to its standard algorithm (i.e. "Hypoinverse") and class (i.e.
   * "Geiger's Method & Least Squares") descriptions.
   * @param codeStr CUBE-format location-method code string.
   * @return A 'TwoStringBlock' object holding the algorithm and
   * class description strings.
   */
  public TwoStringBlock translateCubeLocMethodCode(String codeStr)
  {
    TwoStringBlock blockObj;
    return (codeStr != null &&
         (blockObj=CubeCodeTranslator.fromCubeLocMethod(codeStr)) != null) ?
      blockObj : new TwoStringBlock(codeStr,CubeCodeTranslator.UNKNOWN_STR);
  }
}
