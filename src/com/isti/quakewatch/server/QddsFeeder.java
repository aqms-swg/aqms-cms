//QddsFeeder.java:  QuakeWatch feeder plugin module that receives QDDS
//                  messages and passes them into the QuakeWatch server.
//
//  3/14/2003 -- [ET]  Initial version.
//  6/10/2003 -- [ET]  Added parameters to the 'setupLogFile()' method.
//  6/19/2003 -- [KF]  Changes for using this as base class for TestFeeder.
//  11/5/2003 -- [ET]  Added 'processFeederSettings()' and 'stopFeeder()'
//                     methods; changed 'QWFeederCallback' interface
//                     reference to 'QWServerCallback' class; added 'null'
//                     parameter to 'dataEvent()' call.
// 11/19/2003 -- [ET]  Version 1.01:  Added 'closeLogFile()' to
//                     'stopFeeder()' method.
//   1/8/2004 -- [ET]  Modified to extend 'QWAbstractFeeder' class and
//                     support feeder domain/type names for events.
//  1/14/2005 -- [ET]  Modified to send parameters 'feederSourceHostStr'
//                     and 'feederSrcHostMsgIdNum' to 'dataEvent()' call.
//  1/31/2005 -- [ET]  Reverted back to not sending 'feederSourceHostStr'
//                     and 'feederSrcHostMsgIdNum' parameters to
//                     'dataEvent()' call (because of QDDS duplicate
//                     messages).
//   8/9/2006 -- [ET]  Added optional 'parserObj' parameter to constructor.
//  9/21/2007 -- [ET]  Version 1.12:  Added try/catch blocks to several
//                     methods.
//  3/28/2008 -- [ET]  Version 1.13:  Modified to log "Not enough characters
//                     in message" error at "debug" level instead of
//                     "warning" level.
//  11/4/2010 -- [ET]  Version 1.14:  Added call to 'updateLastDataMsgTime()'.
//

package com.isti.quakewatch.server;

import org.jdom.Element;
import qdds.QDDS;
import qdds.Handler;
import qdds.LogIt;
import qdds.ExtWriteFileCallback;
import qdds.ExtAliveMsgCallback;
import qdds.ExtLogWriteCallback;
import com.isti.util.LogFile;

/**
 * Class QddsFeeder is a QuakeWatch feeder plugin module that receives QDDS
 * messages (via methods in the QDDS.jar file) and passes them into the
 * QuakeWatch server.
 */
public class QddsFeeder extends QWAbstractFeeder implements
                ExtWriteFileCallback,ExtAliveMsgCallback,ExtLogWriteCallback
{
  public static final String MODULE_NAME = "QddsFeeder";
  public static final String VERSION = "1.14";   //name and version strings
  public static final String FEEDER_NAME = "QDDS";  //feeder name
                        //string used to detect an XML message:
  protected static final String XML_START_STR = "<";
  protected final String feederModuleName;
  protected final String feederRevisionStr;
  protected boolean errMsgFlag = false;
                        //CUBE-to-XML-objects parser object:
  protected final MsgParserIntf cubeFormatParserObj;

  protected static final String MSGNUM_STR = "message #";
  protected static final String FROMHOST_STR = " (from \"";
  protected static final String MSGID_STR = "\" msgID=";
  protected static final String PCOLON_STR = "):  ";

  /**
   * Constructs a QDDS feeder module.
   */
  public QddsFeeder()
  {
    this(MODULE_NAME,FEEDER_NAME,VERSION,null);
  }

  /**
   * Constructs a QDDS feeder module.
   * @param moduleName name of the module.
   * @param feederName name of the feeder.
   * @param versionStr version string for feeder.
   * @param parserObj feeder-parser object to use with feeder, or null
   * for default ('CubeFormatParser').
   */
  protected QddsFeeder(String moduleName, String feederName,
                                 String versionStr, MsgParserIntf parserObj)
  {
    super(moduleName);             //set thread name to the module name
    feederModuleName = moduleName; //set the module name
    this.feederName = feederName;  //set the feeder name
                                   //builder revision string for feeder:
    feederRevisionStr = moduleName + ", Version " + versionStr;
                                   //set parser (or default if null):
    cubeFormatParserObj = (parserObj != null) ? parserObj :
                                                     new CubeFormatParser();
  }

  /**
   * Constructs a QDDS feeder module.
   * @param moduleName name of the module.
   * @param feederName name of the feeder.
   */
  protected QddsFeeder(String moduleName, String feederName)
  {
    this(moduleName,feederName,VERSION,null);
  }

  /**
   * Returns the revision String for the feeder.
   * @return the revision String for the feeder.
   */
  public String getRevisionString()
  {
    return feederRevisionStr;
  }

  /**
   * Processes feeder-specific settings.  The settings are usually
   * fetched from the "QWFeederSettings" element in the QuakeWatch-server
   * configuration file.  (For 'QWFeederPlugin' interface.)
   * @param settingsStr The settings string to be processed.
   * @throws QWFeederInitException if an error occurs while processing
   * the settings.
   */
  public void processFeederSettings(String settingsStr)
                                                throws QWFeederInitException
  {
         //if any settings given then throw exception
    if(settingsStr != null && settingsStr.trim().length() > 0)
    {
      throw new QWFeederInitException(
                    "No 'QWFeederSettings' options are supported by the '" +
                                             feederModuleName + "' module");
    }
  }

  /**
   * Executing method for the feeder thread.  Starts execution of QDDS
   * threads.
   */
  public void run()
  {
    if(logObj == null)  //if no log file setup then create dummy object
      logObj = new LogFile(null,LogFile.NO_MSGS,LogFile.WARNING);
    try
    {
            //enter data-msg callback object to be used by QDDS Handler:
      Handler.setExtWriteFileCallbackObj(this);
            //enter alive-msg callback object to be used by QDDS Handler:
      Handler.setExtAliveMsgCallbackObj(this);
            //enter log-msg callback object to be used by QDDS LogIt:
      LogIt.setExtLogWriteCallbackObj(this);
            //start up QDDS threads, with startup params str as argument:
      QDDS.main((startupParamsStr != null) ?
                     (new String[] { startupParamsStr }) : (new String[0]));
    }
    catch(Exception ex)
    {  //some kind of exception error;log message
      logObj.warning("Error starting QDDS-feeder threads:  " + ex);
    }
  }

  /**
   * Callback method (called by QDDS 'Handler' module) that captures
   * data that would have been written to files.
   * (For 'ExtWriteFileCallback' interface.)
   * @param msgNum message number.
   * @param msgStr message data string.
   * @param hostNameStr name of QDDS hub that message came from.
   * @param hostMessageID message ID number from QDDS hub.
   */
  public void writeFile(long msgNum, String msgStr, String hostNameStr,
                                                         long hostMessageID)
  {
    try
    {
      updateLastDataMsgTime();         //mark time of data message received
      if(callbackObj == null)
      {    //callback object handle null
        if(!errMsgFlag)
        {  //show error message (once)
          logObj.warning(feederModuleName + " (\"" + feederName +
                                           "\"):  Callback object not set");
          errMsgFlag = true;
        }
        return;
      }
      if(!msgStr.startsWith(XML_START_STR))
      {    //message is not in XML format
        final Element elementObj;   //parse message into JDOM/XML Elements:
        if((elementObj=cubeFormatParserObj.parseMessageString(
                          msgStr,msgNum,hostNameStr,hostMessageID)) != null)
        {  //message parsed OK
          logObj.debug4(feederModuleName + " (\"" + feederName + "\"):  " +
                          MSGNUM_STR + msgNum + FROMHOST_STR + hostNameStr +
                           MSGID_STR + hostMessageID + PCOLON_STR + msgStr);
                          //send in element object via callback object
                          // (pass in domain/type names for feeder):
          callbackObj.dataEvent(feederDomainNameStr,feederTypeNameStr,null,
                                                    elementObj,null,null,0);
        }
        else
        {  //error parsing message; build and send error message
                   //if empty message then log at "debug" level:
          final int lvlVal = (msgStr.trim().length() > 0) ?
                                            LogFile.WARNING : LogFile.DEBUG;
          logObj.println(lvlVal, (feederModuleName + " (\"" + feederName +
               "\"):  Error parsing " + MSGNUM_STR + msgNum + FROMHOST_STR +
                          hostNameStr + MSGID_STR + hostMessageID + "):  " +
                                    cubeFormatParserObj.getErrorMessage()));
          logObj.debug("  msg: " + msgStr);
        }
      }
      else
      {    //message is in XML format; log it
        logObj.debug(feederModuleName + " (\"" + feederName +
             "\"):  XML-format message (#" + msgNum + ") received from \"" +
                                   hostNameStr + MSGID_STR + hostMessageID);
        logObj.debug2("  msg: " + msgStr);
      }
    }
    catch(Exception ex)
    {  //some kind of exception error;log message
      logObj.warning("Error processing QDDS-feeder data message:  " + ex);
    }
  }

  /**
   * Callback method (called by QDDS 'Handler' module) for capturing
   * "alive" messages from QDDS hubs.
   * (For 'ExtAliveMsgCallback' interface.)
   * @param hostMessageID message ID number from QDDS hub.
   * @param hostNameStr name of QDDS hub that message came from.
   * @param versionStr version string reported by QDDS hub.
   */
  public void reportAliveMsg(long hostMessageID, String hostNameStr,
                                                         String versionStr)
  {
    try
    {
      if(callbackObj == null)
      {    //callback object handle null
        if(!errMsgFlag)
        {  //show error message (once)
          logObj.warning(feederModuleName + " (\"" + feederName +
                                           "\"):  Callback object not set");
          errMsgFlag = true;
        }
        return;
      }
      logObj.debug4(feederModuleName + " (\"" + feederName +
                             "\") alive message:  " + hostMessageID + ", " +
                                           hostNameStr + ", " + versionStr);
                     //send in alive message via callback object:
      callbackObj.aliveMessage(hostMessageID,hostNameStr,versionStr);
    }
    catch(Exception ex)
    {  //some kind of exception error;log message
      logObj.warning("Error processing QDDS-feeder alive message:  " + ex);
    }
  }

    /**
     * Callback method (called by QDDS 'LogIt' module) for capturing log
     * messages from QDDS.
     * (For 'ExtLogWriteCallback' interface.)
     * @param msgStr message string to be logged.
     */
  public void writeStr(String msgStr)
  {
    logObj.info("QDDS_log:  " + msgStr);
  }
}
