//QWClientsManager.java:  Manages the clients connected to the QuakeWatch
//                        server.
//
//   9/8/2004 -- [ET]  Initial version.
//  9/30/2004 -- [ET]  Modified 'performClientLogin()' to return a
//                     client-info-block object; added distrib-info
//                     related methods to 'ClientInfoBlk'.
// 10/13/2004 -- [ET]  Fixed 'CLIENT_AUTH_HOST' in display string in
//                     'ClientInfoBlk'; added 'commVersionStr' variable
//                     to 'ClientInfoBlk' class; increased debug log
//                     level on a couple of log outputs.
// 10/29/2004 -- [ET]  Added methods 'clearClientsChangedFlag()' and
//                     'getClientsChangedFlag()'; added 'extraLogObj'
//                     parameter to 'removeTimedOutClients()' method.
//  11/1/2004 -- [ET]  Added 'username' parameter to 'perfomClientLogin()'
//                     method and 'ClientInfoBlk' class.
// 11/10/2004 -- [ET]  Modified 'updateClientInformation()' so that
//                     'clientsChangedFlag' is only set if a client
//                     is added.
// 11/18/2004 -- [ET]  Added 'getConnectedClientsCount()' method.
// 11/23/2004 -- [ET]  Modified so that update of client-versions-info
//                     file will result in existing 'ClientInfoBlk'
//                     objects being updated; added 'removeClient()'
//                     method.
//  1/10/2005 -- [ET]  Added 'verInfoPollDelaySecs' parameter to
//                     constructor; changed level of "Client checked-in
//                     with unknown client-info" log output from "Warning"
//                     to "Debug".
//  9/13/2005 -- [ET]  Increased debug level and modified wording on
//                     "Client checked-in with..." message in
//                     'updateClientInformation()' method.
// 12/16/2005 -- [ET]  Modified to fetch username from connection-info
//                     properties string (if not given directly).
//  8/29/2006 -- [ET]  Modified 'getDistribInfoObj()' to update the
//                     distribution object even if the last-modified time
//                     for the client-versions-information file is older
//                     than the one currently held.
// 11/29/2006 -- [ET]  Fixed issue where if a client checked in before
//                     logging in its username would not be saved.
//   2/5/2008 -- [ET]  Modified to use 'QWReportStrings' fields.
//  3/25/2008 -- [ET]  Added extra clients-info-to-username tracking table
//                     to allow recovery of username when client connection
//                     has timed out but client then reuses connection.
//  1/30/2018 -- [KF]  Added 'CLIENT_FLAGS'.
//

package com.isti.quakewatch.server;

import java.util.List;
import java.util.Iterator;
import java.util.Map;
import org.jdom.Element;
import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.util.FifoHashtable;
import com.isti.util.IstiXmlUtils;
import com.isti.quakewatch.common.QWUpgradeInfoStrs;
import com.isti.quakewatch.common.QWConnStrings;
import com.isti.quakewatch.common.QWReportStrings;

/**
 * Class QWClientsManager manages the clients connected to the QuakeWatch
 * server.  A versions-information manager is also created and used
 * to manage client versions.  The 'removeTimedOutClients()' method
 * should be called on a periodic basis, and the 'close()' method
 * should be called to "terminate" the managers.
 */
public class QWClientsManager
{
  private final QWVersionsInfoMgr versionsInfoMgrObj;
  private final int clientTimeoutMs;
  private final LogFile logObj;
  private boolean clientsChangedFlag = true;
         //name of root element in client-versions-information XML file:
  private static final String VERINFO_ROOTELEM_STR = "ClientVersionsInfo";
         //display prompt string:
  private static final String INFO_STR = "  infoStr:  ";
         //table of connected 'ClientInfoBlk' objs; keys = client-info strs:
  private final FifoHashtable connectedClientsTable = new FifoHashtable();
         //table of invalidated 'ClientInfoBlk's; keys = client-info strs:
  private final FifoHashtable invalidatedClientsTable = new FifoHashtable();
         //tracking table; keys = client-info strs, values = username strs:
  private final FifoHashtable clientsUsrTrackingTable = new FifoHashtable();

  /**
   * Creates a clients-manager object.  A versions-information manager
   * is also created.
   * @param versionsInfoUrlStr the URL string referencing the
   * client-versions-information file to use, or null or an emptry
   * string for none.
   * @param verInfoPollDelaySecs the number of seconds to delay between
   * polls of the client-versions-information file, or 0 for no polling
   * (after the first poll).
   * @param clientTimeoutSecs the client-connection timeout value, in
   * seconds.
   * @param logObj the log-file object to use.
   */
  public QWClientsManager(String versionsInfoUrlStr,
            int verInfoPollDelaySecs, int clientTimeoutSecs, LogFile logObj)
  {
    clientTimeoutMs = clientTimeoutSecs * 1000;
    this.logObj = logObj;
              //create versions-information manager object:
    versionsInfoMgrObj = new QWVersionsInfoMgr(versionsInfoUrlStr,
                          verInfoPollDelaySecs,VERINFO_ROOTELEM_STR,logObj);
              //if version-info file given then start manager thread:
    if(versionsInfoUrlStr != null && versionsInfoUrlStr.trim().length() > 0)
      versionsInfoMgrObj.start();
  }

  /**
   * Performs a client login into this manager using the given
   * client-connection-information properties string.
   * Clients will call this method when they first connect.
   * @param usernameStr user name string for client.
   * @param clientInfoStr the client-connection-information
   * properties string to use.
   * @return true if the client login should be allowed; false
   * if the client login should be rejected because the client
   * version is too old.
   */
  public ClientInfoBlk performClientLogin(String usernameStr,
                                                       String clientInfoStr)
  {
    try
    {
      final ClientInfoBlk infoBlkObj;
      if((infoBlkObj=updateClientInformation(
                                   usernameStr,clientInfoStr,true)) != null)
      {  //client information created OK
        if(!infoBlkObj.isDistribNameValid())
        {     //no match found for client distribution name; log message
          logObj.debug4("QWClientsManager.performClientLogin:  " +
                                "Client distribution name not matched (\"" +
                                         infoBlkObj.distribNameStr + "\")");
        }
        return infoBlkObj;
      }
    }
    catch(Exception ex)
    {    //some kind of exception error; log it
      logObj.warning("Error in QWClientsManager.performClientLogin():  " +
                                                                        ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
    return null;
  }

  /**
   * Performs a client check-in using the given
   * client-connection-information properties string.
   * All clients will call this method on a periodical
   * basis while they are connected.
   * @param clientInfoStr the client-connection-information
   * properties string to use.
   * @return true if the an updated version of the client
   * is available; false if not.
   */
  public boolean performClientCheckIn(String clientInfoStr)
  {
    try
    {
      final ClientInfoBlk infoBlkObj;
      if((infoBlkObj=updateClientInformation(null,clientInfoStr,false)) !=
                                                                       null)
      {  //client information updated OK
        if(infoBlkObj.isDistribNameValid())
        {     //match found for client distribution name
                   //test if upgrade version is newer; return result
          return infoBlkObj.isUpgradeNewer();
        }
        else
        {     //no match found for client distribution name
          logObj.debug4("QWClientsManager.performClientCheckIn:  " +
                                "Client distribution name not matched (\"" +
                                         infoBlkObj.distribNameStr + "\")");
        }
      }
    }
    catch(Exception ex)
    {    //some kind of exception error; log it
      logObj.warning("Error in QWClientsManager.performClientCheckIn():  " +
                                                                        ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
    return false;
  }

  /**
   * Returns information about available client-program upgrades.
   * The given client-connection-information properties string must
   * have previously been sent to the 'performClientLogin()' method.
   * @param clientInfoStr the client-connection-information
   * properties string to use.
   * @return An XML-formatted string containing information about
   * available client-program upgrades.
   */
  public String getClientUpgradeInfoStr(String clientInfoStr)
  {
         //create "ClientUpgradeInfo" element:
    final Element topInfoElemObj =
                           new Element(QWUpgradeInfoStrs.CLIENT_UPGRD_INFO);
    String dispMsgStr = null;
    Element linksElemObj = null;
    try
    {
      if(clientInfoStr != null && clientInfoStr.trim().length() > 0)
      {  //info string contains data
        final ClientInfoBlk infoBlkObj;
        Object obj;
        if((obj=connectedClientsTable.get(clientInfoStr)) instanceof
                                                            ClientInfoBlk ||
                 (obj=invalidatedClientsTable.get(clientInfoStr)) instanceof
                                                              ClientInfoBlk)
        {     //info string matched in main or "invalidated" table
          infoBlkObj = (ClientInfoBlk)obj;
        }
        else  //info string not matched; create new client-info block
        {     // and add it to the "invalidated" table
          invalidatedClientsTable.put(clientInfoStr, (infoBlkObj =
                 new ClientInfoBlk(null,clientInfoStr,versionsInfoMgrObj)));
          logObj.debug2("QWClientManager:  Client 'getUpgradeInfo' " +
                                  "called with unknown client-info string");
          logObj.debug2(INFO_STR + clientInfoStr);
        }
        final QWVersionsInfoMgr.DistribInformation distribInfoObj;
        if((distribInfoObj=infoBlkObj.getDistribInfoObj()) != null)
        {   //match found for client distribution name
                           //set flag if upgrade version is newer:
          final boolean newerAvailFlag = infoBlkObj.isUpgradeNewer();
                           //set flag if client not too old to connect:
          final boolean connAllowedFlag = !infoBlkObj.isVersionTooOld();
                           //set "NewerVerAvail" attribute:
          topInfoElemObj.setAttribute(QWUpgradeInfoStrs.NEWER_VER_AVAIL,
               (newerAvailFlag?QWUpgradeInfoStrs.YES:QWUpgradeInfoStrs.NO));
                           //set "ConnectAllowed" attribute:
          topInfoElemObj.setAttribute(QWUpgradeInfoStrs.CONNECT_ALLOWED,
              (connAllowedFlag?QWUpgradeInfoStrs.YES:QWUpgradeInfoStrs.NO));
          if(distribInfoObj.newVersionNumStr != null)
          { //version # for upgrade OK; set "NewVersionNum" attribute
            topInfoElemObj.setAttribute(QWUpgradeInfoStrs.NEW_VERSION_NUM,
                                           distribInfoObj.newVersionNumStr);
          }
                           //create "Links" element:
          linksElemObj = new Element(QWUpgradeInfoStrs.LINKS);
          if(newerAvailFlag)
          { //newer-version upgrade is available
                 //setup display message for newer-version
                 // or current-version-too-old text:
            dispMsgStr = connAllowedFlag ?
                                    distribInfoObj.getNewerVersionMsgStr() :
                                    distribInfoObj.getNoConnVersionMsgStr();
                           //add "DistZip" attribute to "Links" element:
            linksElemObj.setAttribute(QWUpgradeInfoStrs.DIST_ZIP,
                                             distribInfoObj.distZipLinkStr);
          }
          else   //no newer-version upgrade avail; setup display message
            dispMsgStr = distribInfoObj.getCurrentVersionMsgStr();
                           //add "DistWebsite" attribute to "Links" elem:
          linksElemObj.setAttribute(QWUpgradeInfoStrs.DIST_WEBSITE,
                                         distribInfoObj.distWebsiteLinkStr);
        }
        else
        {   //no match found for client distribution name
          logObj.debug("QWClientsManager.getClientUpgradeInfoStr:  " +
                              "Client distribution name not matched (\"" +
                                       infoBlkObj.distribNameStr + "\")");
          logObj.debug2(INFO_STR + clientInfoStr);
        }
      }
      else
      {       //info string contains no data; log warning message
        logObj.warning("Client 'getUpgradeInfo' called with empty " +
                                                      "client-info string");
      }
    }
    catch(Exception ex)
    {    //some kind of exception error; log it
      logObj.warning(
             "Error in QWClientsManager.getClientUpgradeInfoStr():  " + ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
    try
    {    //if no display message setup then use unknown-client text:
      if(dispMsgStr == null)
        dispMsgStr = versionsInfoMgrObj.getUnknownClientMsgStr();
                             //create "DisplayMessage" element:
      final Element dispMsgElemObj =
                             new Element(QWUpgradeInfoStrs.DISPLAY_MESSAGE);
      dispMsgElemObj.addContent(dispMsgStr);  //add display-msg text to elem
      topInfoElemObj.addContent(dispMsgElemObj);   //add display-msg element
      if(linksElemObj != null)    //if setup then add "Links" element
        topInfoElemObj.addContent(linksElemObj);
                   //convert element to string and return:
      return IstiXmlUtils.elementToString(topInfoElemObj);
    }
    catch(Exception ex)
    {    //some kind of exception error; log it
      logObj.warning(
             "Error in QWClientsManager.getClientUpgradeInfoStr():  " + ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
      return UtilFns.EMPTY_STRING;
    }
  }

  /**
   * Invalidates the specified client.  Clients are invalidated after they
   * have logged in but are not allowed to connect (because of version too
   * old or invalid login).  Invalidated clients are held in a table to
   * make their info available to the 'getClientUpgradeInfoStr()' method.
   * Invalidated-client entries are removed from the table when a matching
   * client entry logs in or when the entry times out (via the
   * 'removeTimedOutClients()' method).
   * @param clientInfoStr the client-connection-information
   * properties string to use.
   */
  public void invalidateClient(String clientInfoStr)
  {
    synchronized(connectedClientsTable)
    {  //grab thread-sync for clients table ('clientsChangedFlag' may change)
      final Object obj;      //remove entry and add to 'invalidated' table:
      if((obj=connectedClientsTable.remove(clientInfoStr)) != null)
      {  //matching entry found in and removed from table
        clientsChangedFlag = true;     //indicate clients-info changed
        invalidatedClientsTable.put(clientInfoStr,obj);
      }
      else
      {  //entry not found in table
        logObj.debug("QWClientManager:  Attempted to invaliate " +
                                      "non-existent client entry from table");
        logObj.debug2(INFO_STR + clientInfoStr);
      }
    }
  }

  /**
   * Removes the specified client.
   * @param clientInfoStr the client-connection-information
   * properties string for the client.
   * @return the 'ClientInfoBlk' object for the removed client,
   * or null if none was removed.
   */
  public ClientInfoBlk removeClient(String clientInfoStr)
  {
    final Object r1Obj,r2Obj;
    synchronized(connectedClientsTable)
    {    //grab thread-sync for clients table
              //remove client entry from "main" and "invalidated" tables:
      r1Obj = connectedClientsTable.remove(clientInfoStr);
      r2Obj = invalidatedClientsTable.remove(clientInfoStr);
      clientsChangedFlag = true;       //indicate clients-info changed
    }
         //if 'ClientInfoBlk' object removed then return it:
    if(r1Obj instanceof ClientInfoBlk)
      return (ClientInfoBlk)r1Obj;
    if(r2Obj instanceof ClientInfoBlk)
      return (ClientInfoBlk)r2Obj;
    return null;
  }

  /**
   * Removes from the table clients that have not checked in within the
   * 'clientTimeoutSecs' timeout value.  Old entries are also removed
   * from the table of "invalidated" clients.  This method should be
   * called on a periodic basis.
   * @param extraLogObj an extra 'LogFile' object into which to
   * send a message for each client removed from the "main" table,
   * or null for none.
   */
  public void removeTimedOutClients(LogFile extraLogObj)
  {
    try
    {    //remove timed-out clients from main table:
      final long currTimeVal = System.currentTimeMillis();
      ClientInfoBlk infoBlkObj;
      int tableSize,idx;
      String logStr;
      synchronized(connectedClientsTable)
      {  //grab thread-sync for clients table
        tableSize = connectedClientsTable.size();
        idx = 0;
        while(idx < tableSize)
        {     //for each client entry in the table
          infoBlkObj = (ClientInfoBlk)(connectedClientsTable.elementAt(idx));
          if(currTimeVal - infoBlkObj.getLastCheckInTimeMs() >
                                                            clientTimeoutMs)
          {   //too much time elapsed since last client check-in
            clientsChangedFlag = true;      //indicate clients-info changed
            connectedClientsTable.removeElementAt(idx);    //remove entry
            --tableSize;          //reduce table size
            logStr = "Removed connection-timed-out client from table:  " +
                                                                 infoBlkObj;
            logObj.debug2("QWClientsManager:  " + logStr);
            if(extraLogObj != null)         //if "extra" log file given then
              extraLogObj.println(logStr);  //output to "extra" log file
          }
          else          //client entry is OK
            ++idx;      //increment to next entry in table
        }
      }
         //remove old client entries from "invalidated" table:
      synchronized(invalidatedClientsTable)
      {  //grab thread-sync for table
        tableSize = invalidatedClientsTable.size();
        idx = 0;
        while(idx < tableSize)
        {     //for each client entry in the table
          infoBlkObj =
                    (ClientInfoBlk)(invalidatedClientsTable.elementAt(idx));
          if(currTimeVal - infoBlkObj.getLastCheckInTimeMs() >
                                                            clientTimeoutMs)
          {   //client check-in time is too old
            invalidatedClientsTable.removeElementAt(idx);  //remove entry
            --tableSize;          //reduce table size
            logObj.debug2("QWClientsManager:  Removed old client entry" +
                               " from 'invalidated' table:  " + infoBlkObj);
          }
          else          //client entry is OK
            ++idx;      //increment to next entry in table
        }
      }
    }
    catch(Exception ex)
    {    //some kind of exception error; log it
      logObj.warning(
               "Error in QWClientsManager.removeTimedOutClients():  " + ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
  }

  /**
   * Returns a list of currently-connected clients.
   * @return A new list object containing 'ClientInfoBlk' objects.
   */
  public List getConnectedClientsList()
  {
    return connectedClientsTable.getValuesVector();
  }

  /**
   * Returns a listing of the currently-connected clients.
   * @return A listing of the currently-connected clients.
   */
  public String getConnectedClientsStr()
  {
    final List clientsListObj = connectedClientsTable.getValuesVector();
    final int clientsListSize = clientsListObj.size();
    if(clientsListSize > 0)
    {    //clients table not empty
      final StringBuffer buff = new StringBuffer(
                                       QWReportStrings.REP_CONNCLIENTS_STR +
                                             " (" + clientsListSize + "):");
      for(int idx=0; idx<clientsListSize; ++idx)
      {  //for each client entry
        buff.append(UtilFns.newline +
                UtilFns.fixStringLen(Integer.toString(idx+1),5,true,false) +
                                            ". " + clientsListObj.get(idx));
      }
      return buff.toString();
    }
    return QWReportStrings.REP_NOCLIENTS_STR;
  }

  /**
   * Returns the status of the versions-information data.
   * @return true if the versions-information data is valid, false is not.
   */
  public boolean getVerInfoDataValidFlag()
  {
    return versionsInfoMgrObj.getDataValidFlag();
  }

  /**
   * Returns the number of currently-connected clients.
   * @return The number of currently-connected clients.
   */
  public int getConnectedClientsCount()
  {
    return connectedClientsTable.size();
  }

  /**
   * Determines if the clients information has changed since the last
   * call to 'clearClientsChangedFlag()'.
   * @return true if the clients information has changed since the last
   * call to 'clearClientsChangedFlag()'; false if not.
   */
  public boolean getClientsChangedFlag()
  {
    return clientsChangedFlag;
  }

  /**
   * Clears the flag used to determine if the clients information has
   * changed.  The previous value of the flag is returned.
   * @return true if the clients information has changed since the last
   * call to this method; false if not.
   */
  public boolean clearClientsChangedFlag()
  {
    synchronized(connectedClientsTable)
    {  //grab thread-sync for clients table
      final boolean retFlag = clientsChangedFlag;
      clientsChangedFlag = false;
      return retFlag;
    }
  }

  /**
   * Terminates any threads that were started by this manager.
   */
  public void close()
  {
    versionsInfoMgrObj.terminate();
  }

  /**
   * Updates a client's entry in the table using the given
   * client-connection-information properties string.  If
   * a matching entry does not exist in the table then one
   * is created.
   * @param usernameStr user name string for client, or null for none.
   * @param clientInfoStr the client-connection-information
   * properties string to use.
   * @param loginFlag true if call is via client-login method.
   * @return the client-information-block object for the client.
   */
  private ClientInfoBlk updateClientInformation(String usernameStr,
                                    String clientInfoStr, boolean loginFlag)
  {
    if(clientInfoStr != null && clientInfoStr.trim().length() > 0)
    {    //info string contains data
      Object obj;
      ClientInfoBlk infoBlkObj;
      synchronized(connectedClientsTable)
      {  //grab thread-sync for clients table
        if((obj=connectedClientsTable.get(clientInfoStr)) instanceof
                                                              ClientInfoBlk)
        {     //matching entry found in table; save handle to entry
          infoBlkObj = (ClientInfoBlk)obj;
          if(usernameStr == null ||
                                 usernameStr.equals(infoBlkObj.usernameStr))
          {   //no username or username matched to fetched client info OK
                   //update check-in time for client-info block:
            infoBlkObj.updateLastCheckInTimeMs();
            if(loginFlag)
            {   //update is via client-login method
              logObj.debug2("QWClientManager:  Client logged-in with " +
                                             "existing client-info string");
              logObj.debug3(INFO_STR + clientInfoStr);
            }
            return infoBlkObj;
          }
          else
          {   //given username not matched to client info fetched from table
                   //remove existing client info from table (replaced below):
            connectedClientsTable.remove(clientInfoStr);
          }
        }
        if(usernameStr != null)
        {  //username was given; add to client-info/username tracking table
          clientsUsrTrackingTable.put(clientInfoStr,usernameStr);
                   //if client-info/username tracking table has more
                   // than 1000 entries then remove oldest entry:
          if(clientsUsrTrackingTable.size() > 1000)
            clientsUsrTrackingTable.removeElementAt(0);
          if(!loginFlag)
          {  //update is not via client-login method
            logObj.debug("Client (username=\"" + usernameStr +
                        "\") checked-in with unmatched client-info string");
            logObj.debug(INFO_STR + clientInfoStr);
          }
        }
        else if(!loginFlag)
        {  //username not given and update is not via client-login method
                   //try to fetch username from tracking table:
          if((obj=clientsUsrTrackingTable.get(clientInfoStr)) instanceof
                                                                     String)
          {  //username found in client-info/username tracking table
            usernameStr = (String)obj;      //enter fetched username
          }
          logObj.debug("Client checked-in with unmatched client-info " +
                       "string (fetched username=\"" + usernameStr + "\")");
          logObj.debug(INFO_STR + clientInfoStr);
        }
              //create and add new entry for client-info string:
        connectedClientsTable.put(clientInfoStr,
                                            (infoBlkObj = new ClientInfoBlk(
                            usernameStr,clientInfoStr,versionsInfoMgrObj)));
        clientsChangedFlag = true;   //indicate clients-info changed
      }
      return infoBlkObj;
    }
              //info string contains no data; log warning message:
    logObj.warning("Client check-in with empty client-info string");
    return null;
  }

/*
  public static void main(String [] args)
  {
    final LogFile logObj =
                         new LogFile(null,LogFile.NO_MSGS,LogFile.ALL_MSGS);
    final String urlStr = "ClientVersInfoTest.xml";
    final QWClientsManager clientsMgrObj = new QWClientsManager(
                                                         urlStr,300,logObj);
    UtilFns.sleep(2500);

    final String propsStr = "\"ClientName\"=\"CISN Display\"," +
     "\"ClientVersion\"=\"0.363Beta\"," +
     "\"DistribName\"=\"QWClient_default\"," +
     "\"StartupTime\"=\"Aug 12 2004 18:54:20 EDT\"," +
     "\"ClientIP\"=\"192.168.0.66\",\"ClientHost\"=\"cp1800\"," +
     "\"CommVersion\"=\"1.0\",\"OpenORBVersion\"=\"1.4.0_23Feb2004_mod\"," +
     "\"ClientOSName\"=\"Windows 2000\",\"JavaVersion\"=\"1.3.1\"";

    logObj.info("Login accepted = " +
                                clientsMgrObj.performClientLogin(propsStr));
    logObj.info("Check-in upgrade-avail = " +
                              clientsMgrObj.performClientCheckIn(propsStr));
    logObj.info(clientsMgrObj.getConnectedClientsStr());
    logObj.info("UpgradeInfo:  " +
                           clientsMgrObj.getClientUpgradeInfoStr(propsStr));
    clientsMgrObj.close();
  }
*/


  /**
   * Class ClientInfoBlk defines a block of information for a connected
   * client.
   */
  public static class ClientInfoBlk
  {
    public final String usernameStr;
    public final String clientInfoStr;
    public final String distribNameStr;
    public final String clientVersionStr;
    public final String clientIPStr;
    public final String clientHostStr;
    public final String clientAuthIPStr;
    public final String clientAuthHostStr;
    public final String commVersionStr;
    public final String displayString;
    private final QWVersionsInfoMgr versionsInfoMgrObj;
    private final long initCheckInTimeMs;
    private long lastCheckInTimeMs;
    private long verInfoLastModTimeMs = 0;
    private QWVersionsInfoMgr.DistribInformation distribInfoObj = null;
    private static final String SEP_STR = ", ";       //separator string
              //setup table of names for display order and aliases:
    private static final FifoHashtable nameOrderAliasTable =
                                                        new FifoHashtable();
    static
    {
      nameOrderAliasTable.put(QWConnStrings.CLIENT_NAME,"name");
      nameOrderAliasTable.put(QWConnStrings.CLIENT_IP,"IP");
      nameOrderAliasTable.put(QWConnStrings.CLIENT_HOST,"host");
      nameOrderAliasTable.put(QWConnStrings.STARTUP_TIME,"startTime");
      nameOrderAliasTable.put(QWConnStrings.CLIENT_VERSION,"ver");
      nameOrderAliasTable.put(QWConnStrings.CLIENT_AUTH_IP,"authIP");
      nameOrderAliasTable.put(QWConnStrings.CLIENT_AUTH_HOST,"authHost");
      nameOrderAliasTable.put(QWConnStrings.COMM_VERSION,"commVer");
      nameOrderAliasTable.put(QWConnStrings.DISTRIB_NAME,"distrib");
      nameOrderAliasTable.put(QWConnStrings.OPENORB_VERSION,"openORBver");
      nameOrderAliasTable.put(QWConnStrings.CLIENT_OSNAME,"OS");
      nameOrderAliasTable.put(QWConnStrings.JAVA_VERSION,"javaVer");
      nameOrderAliasTable.put(QWConnStrings.CLIENT_FLAGS,"flags");
    }

    /**
     * Creates a block of information for a connected client.
     * @param usernameStr user name string for client.
     * @param clientInfoStr the client-information string to use.
     * @param versionsInfoMgrObj the versions-information-manager
     * object to use.
     */
    public ClientInfoBlk(String usernameStr, String clientInfoStr,
                                       QWVersionsInfoMgr versionsInfoMgrObj)
    {
      this.clientInfoStr = clientInfoStr;
      this.versionsInfoMgrObj = versionsInfoMgrObj;
      initCheckInTimeMs = lastCheckInTimeMs = System.currentTimeMillis();
      if(clientInfoStr.indexOf('=') >= 0)
      {  //info string contains name=value entries
        final FifoHashtable tableObj;
                             //create info-string table from string:
        if((tableObj=FifoHashtable.quotedStringsToTable(
                                         clientInfoStr,',',false)) != null)
        {     //info string converted to table OK
          Object obj;
          if((usernameStr == null || usernameStr.trim().length() <= 0) &&
                       ((obj=tableObj.remove(QWConnStrings.CLIENT_USERNAME))
                                                         instanceof String))
          {   //given username parameter is empty and
              // username property found (and removed)
            usernameStr = (String)obj;      //get username from props string
          }
          this.usernameStr = usernameStr;   //save username string

                   //get value of "DistibName" info-string property:
          distribNameStr = ((obj=tableObj.get(
                           QWConnStrings.DISTRIB_NAME)) instanceof String) ?
                                                         (String)obj : null;
                   //get value of "ClientVersion" info-string property:
          clientVersionStr = ((obj=tableObj.get(
                         QWConnStrings.CLIENT_VERSION)) instanceof String) ?
                                                         (String)obj : null;
                   //get value of "ClientIP" info-string property:
          clientIPStr = ((obj=tableObj.get(
                              QWConnStrings.CLIENT_IP)) instanceof String) ?
                                                         (String)obj : null;
                   //get value of "ClientHost" info-string property:
          clientHostStr = ((obj=tableObj.get(
                            QWConnStrings.CLIENT_HOST)) instanceof String) ?
                                                         (String)obj : null;
                   //get value of "ClientAuthIP" info-string property:
          clientAuthIPStr = ((obj=tableObj.get(
                         QWConnStrings.CLIENT_AUTH_IP)) instanceof String) ?
                                                         (String)obj : null;
                   //get value of "ClientAuthHost" info-string property:
          clientAuthHostStr = ((obj=tableObj.get(
                       QWConnStrings.CLIENT_AUTH_HOST)) instanceof String) ?
                                                         (String)obj : null;
                   //get value of "CommVersion" info-string property:
          commVersionStr = ((obj=tableObj.get(
                           QWConnStrings.COMM_VERSION)) instanceof String) ?
                                                         (String)obj : null;
                   //build a display string from the info-string table by
                   // processing first the entries in the order of those
                   // matched in the 'nameOrderAliasTable' and then adding
                   // in the remaining entries from the info-string table:
          final StringBuffer buff = new StringBuffer();
          Iterator iterObj = nameOrderAliasTable.entrySet().iterator();
          Map.Entry mapEntryObj;
          Object nameObj,valueObj;
          while(true)
          {   //for each entry in 'nameOrderAliasTable'
            if(!iterObj.hasNext())
            {      //end of 'nameOrderAliasTable' reached
              if(tableObj.size() > 0)
              {    //items still remain in info-string table
                iterObj = tableObj.entrySet().iterator();
                while(true)
                {  //for each remaining info-string table entry
                  mapEntryObj = (Map.Entry)(iterObj.next());
                                  //build & add display entry:
                  buff.append(mapEntryObj.getKey() + "=\"" +
                                             mapEntryObj.getValue() + "\"");
                  if(!iterObj.hasNext())    //if no more entries then
                    break;                  //exit (both) loop
                  buff.append(SEP_STR);     //append separator
                }
              }
              break;              //exit loop
            }
                   //get map-entry object (name,alias):
            mapEntryObj = (Map.Entry)(iterObj.next());
            if((valueObj=tableObj.get(nameObj=mapEntryObj.getKey())) != null)
            {      //matching name found in info-string table
                        //build & add display entry (using alias for name):
              buff.append(mapEntryObj.getValue() + "=\"" + valueObj + "\"");
              tableObj.remove(nameObj);     //rem entry from info-str table
              if(tableObj.size() <= 0)      //if info-str table empty then
                break;                      //exit loop
              buff.append(SEP_STR);         //append separator
            }
          }
          displayString = buff.toString();       //convert buffer to string
          return;
        }
      }
         //unable to process client-info string as property entries:
      this.usernameStr = usernameStr;
      distribNameStr = null;                //no distribution-name string
      clientVersionStr = null;              //no client-version string
      clientIPStr = null;                   //no client-IP string
      clientHostStr = null;                 //no client-host string
      clientAuthIPStr = null;               //no client-auth-IP string
      clientAuthHostStr = null;             //no client-auth-host string
      commVersionStr = null;                //no comm-version string
      displayString = clientInfoStr;        //use info str as display string
    }

    /**
     * Updates the last-check-in time for the client to the current time.
     */
    public synchronized void updateLastCheckInTimeMs()
    {
      lastCheckInTimeMs = System.currentTimeMillis();
    }

    /**
     * Returns the initial-check-in time for the client.
     * @return The initial-check-in time for the client, in milliseconds
     * since 1/1/1970.
     */
    public synchronized long getInitCheckInTimeMs()
    {
      return initCheckInTimeMs;
    }

    /**
     * Returns the last-check-in time for the client.
     * @return The last-check-in time for the client, in milliseconds
     * since 1/1/1970.
     */
    public synchronized long getLastCheckInTimeMs()
    {
      return lastCheckInTimeMs;
    }

    /**
     * Returns the distribution-information object associated
     * with this client.  This object will change if the
     * client-versions-information file is updated.
     * @return The distribution-information object associated with
     * this client, or null if no matching object was found.
     */
    public synchronized QWVersionsInfoMgr.DistribInformation
                                                         getDistribInfoObj()
    {
      final long modTime = versionsInfoMgrObj.getVerInfoLastModTimeMs();
      if(distribInfoObj == null || modTime != verInfoLastModTimeMs)
      {  //no current object or versions-info has changed since last fetch
        verInfoLastModTimeMs = modTime;
                   //fetch distribution-info object for client:
        distribInfoObj = (distribNameStr != null) ?
                versionsInfoMgrObj.getDistribInfoObj(distribNameStr) : null;
      }
      return distribInfoObj;
    }

    /**
     * Returns the status of whether or not the client's distribution
     * name was matched to an entry in the versions-information manager.
     * @return true if the status of whether or not the client's
     * distribution name was matched to an entry in the versions-
     * information manager; false if not.
     */
    public boolean isDistribNameValid()
    {
      return (getDistribInfoObj() != null);
    }

    /**
     * Compares the minimum-connect version string to the version
     * string of this client.
     * @return true if the minimum-connect version string is newer than
     * the client's version string; false if not.
     */
    public boolean isVersionTooOld()
    {
      final QWVersionsInfoMgr.DistribInformation infoObj;
      return ((infoObj=getDistribInfoObj()) != null) ?
                          infoObj.isVersionTooOld(clientVersionStr) : false;
    }

    /**
     * Compares the upgrade version string to the version string
     * of this client.
     * @return true if the upgrade version string is newer than the
     * client's version string; false if not.
     */
    public boolean isUpgradeNewer()
    {
      final QWVersionsInfoMgr.DistribInformation infoObj;
      return ((infoObj=getDistribInfoObj()) != null) ?
                           infoObj.isUpgradeNewer(clientVersionStr) : false;
    }

    /**
     * Returns a display string representing the information for the client.
     * @return A display string representing the information for the client.
     */
    public synchronized String toString()
    {
      return "user=" + ((usernameStr != null) ?
                               ("\"" + usernameStr + "\", ") : "<none>, ") +
                                             displayString + ", connTime=" +
                                             UtilFns.durationMillisToString(
                 lastCheckInTimeMs-initCheckInTimeMs) + ", lastCheckIn=\"" +
                       UtilFns.timeMillisToString(lastCheckInTimeMs) + "\"";
    }
  }
}
