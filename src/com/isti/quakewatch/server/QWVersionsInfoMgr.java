//QWVersionsInfoMgr.java:  Manages version information and services for
//                         QuakeWatch clients via the server.
//
//   9/8/2004 -- [ET]  Initial version.
// 11/19/2004 -- [ET]  Set 'CFGFILE_POLL_DELAYSECS' value to 30 seconds.
//  1/10/2005 -- [ET]  Added 'pollDelaySecs' parameter to constructor.
//   6/8/2005 -- [ET]  Modified to use 'FileDataMonitor'.
//  9/16/2005 -- [ET]  Fixed bug where value returned by method
//                     'getVerInfoLastModTimeMs()' was always zero.
//  8/30/2006 -- [ET]  Fixed issue where 'dataValueFlag' would remain
//                     'false' after versions-info file was inaccessible
//                     and then accessible.
//

package com.isti.quakewatch.server;

import java.util.List;
import java.util.HashMap;
import java.util.Iterator;
import java.io.ByteArrayInputStream;
import org.jdom.Element;
import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.util.IstiNotifyThread;
import com.isti.util.IstiXmlUtils;
import com.isti.util.FileDataMonitor;

/**
 * Class QWVersionsInfoMgr manages version information and services
 * for QuakeWatch clients via the server.  An XML-formatted
 * client-versions-information file is loaded at startup and is
 * reloaded (via a separate thread) if modified.  After an instance
 * of this class is created its thread should be started via the
 * 'start()' method.
 */
public class QWVersionsInfoMgr extends IstiNotifyThread
{
         //XML-element-tag string for "DistribName" element:
  private static final String DISTRIB_NAME_TAG = "DistribName";
         //XML-element-tag string for "UnknownClientMsg" element:
  private static final String UNKNOWN_CLIENT_MSG_TAG = "UnknownClientMsg";
  private final String versionsInfoUrlStr;
  private final long verInfoPollDelayMS;
  private final String rootElementName;
  private final LogFile logObj;
         //time of last change to client-versions-information file:
  private long verInfoLastModTimeMs = 0;
  private boolean dataValidFlag = false;    //true if vers-info data ready
         //table of 'DistribInformation' objects; keys = distribution names:
  private final HashMap verDistribInfoTable = new HashMap();
         //message used if distribution name not matched:
  private String unknownClientMsgStr = null;

  /**
   * Creates a client-versions-information manager.
   * @param versionsInfoUrlStr the URL string referencing the
   * client-versions-information file to use.
   * @param pollDelaySecs the number of seconds to delay between
   * polls of the client-versions-information file, or 0 for no polling
   * (after the first poll).
   * @param rootElementName the expected name of the root element in the
   * client-versions-information XML file, or null to accept any name.
   * @param logObj the LogFile object to use, or null for none.
   */
  public QWVersionsInfoMgr(String versionsInfoUrlStr, int pollDelaySecs,
                                     String rootElementName, LogFile logObj)
  {
    super("QWVersionsInfoMgr");        //create and name thread object
    this.versionsInfoUrlStr = versionsInfoUrlStr;
    this.verInfoPollDelayMS = (long)pollDelaySecs * 1000L;
    this.rootElementName = rootElementName;
    this.logObj = (logObj != null) ? logObj : LogFile.getNullLogObj();
  }

  /**
   * Returns the distribution-information object associated with the given
   * distribution name.
   * @param distribNameStr the distribution name to use.
   * @return The distribution-information object associated with the given
   * distribution name, or null if no match was found.
   */
  public synchronized DistribInformation getDistribInfoObj(
                                                      String distribNameStr)
  {
    if(dataValidFlag)
    {
      final Object obj;
      if((obj=verDistribInfoTable.get(distribNameStr)) instanceof
                                                         DistribInformation)
      {
        return (DistribInformation)obj;
      }
    }
    return null;
  }

  /**
   * Returns the unknown-client message string.  This is either the text
   * defined by the 'UnknownClientMsg' XML element in the versions-info
   * data or a default text string.
   * @return The unknown-client message string.
   */
  public synchronized String getUnknownClientMsgStr()
  {
    return (unknownClientMsgStr != null &&
                   unknownClientMsgStr.length() > 0) ? unknownClientMsgStr :
                                               "No upgrades are available.";
  }

  /**
   * Processes the client-versions-information data.
   * @param infoDataElemObj the XML-element object holding the data to
   * be processed, or null if an error occurred while fetching the data.
   * @param lastModTimeMs last-modified time for client-versions-information
   * file.
   * @return true if processing was successful; false if not.
   */
  private synchronized boolean processVersionsInfoData(Element infoDataElemObj,
                                                         long lastModTimeMs)
  {
    boolean retFlag = false;
    try
    {
//      logObj.info("Called processVersionsInfoData(), infoDataElemObj:  " +
//                             IstiXmlUtils.elementToString(infoDataElemObj));
      verInfoLastModTimeMs = lastModTimeMs;      //enter last-mod time
      verDistribInfoTable.clear();     //clear current contents of table
      unknownClientMsgStr = null;      //clear unknown-client-msg string
      if(infoDataElemObj != null)
      {  //versions-info data given; process XML elements
        final List listObj;       //fetch list of "DistribName" elements:
        if((listObj=infoDataElemObj.getChildren(DISTRIB_NAME_TAG)) != null
                                                      && listObj.size() > 0)
        {     //at least on "DistribName" element found
          final Iterator iterObj = listObj.iterator();
          Element elemObj;
          DistribInformation infoBlkObj;
          while(iterObj.hasNext())
          {   //for each "DistribName" element; convert to info-block
            infoBlkObj = new DistribInformation(
                                         elemObj=(Element)(iterObj.next()));
            if(infoBlkObj.isDataValid())
            {      //versions-info-block data is OK; add block to table
              verDistribInfoTable.put(infoBlkObj.distribNameStr,infoBlkObj);
              logObj.debug2("QWVersionsInfoMgr:  Added versions-info " +
                                    "from \"" + DISTRIB_NAME_TAG + "\"=\"" +
                         infoBlkObj.distribNameStr + "\" element to table");
            }
            else
            {      //versions-info-block data not valid; log message
              logObj.warning("Data in \"" + DISTRIB_NAME_TAG +
                 "\" element of client-version-information data not valid");
              logObj.debug("  elem:  " +
                                     IstiXmlUtils.elementToString(elemObj));
            }
          }
          if(verDistribInfoTable.size() > 0)     //if data entered then
            retFlag = true;                      //indicate data is valid
          else
          {
            logObj.warning("No valid distribution data found in " +
                                         "client-version-information data");
          }
        }
        else
        {     //no "DistribName" elements found; log warning message
          logObj.warning("No \"" + DISTRIB_NAME_TAG +
                    "\" elements found in client-version-information data");
        }
        unknownClientMsgStr =          //fetch unknown-client-msg text
                   infoDataElemObj.getChildTextTrim(UNKNOWN_CLIENT_MSG_TAG);
      }
      else    //no versions-info data given; log message
        logObj.debug("QWVersionsInfoMgr:  Version-info table cleared");
    }
    catch(Exception ex)
    {    //some kind of exception error
      logObj.warning(
               "Error processing client-versions-information data:  " + ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
    return retFlag;
  }

  /**
   * Executing method for this object's thread.
   */
  public void run()
  {
    try
    {
      logObj.debug2("QWVersionsInfoMgr:  Using versions-info URL string \"" +
                                                 versionsInfoUrlStr + "\"");
         //create file-data-monitor object for client-versions-info file:
      final FileDataMonitor fileDataMonitorObj =
                                    new FileDataMonitor(versionsInfoUrlStr);
      boolean firstTimeFlag = true, lastProcessFlag = false;
      IstiXmlUtils istiXmlUtilsObj = null;
      byte [] fileDataArr;
      while(!isTerminated())
      {  //for each check on the client-versions-information file
              //check for file data change and read if so
              // (timeout = 60 seconds, max bytes = 64K):
        fileDataArr = fileDataMonitorObj.checkReadFileData(60000,65535);
        logObj.debug4("QWVersionsInfoMgr:  Generated versions-info " +
              "URL:  \"" + fileDataMonitorObj.getFileAccessUrlObj() + "\"");
        if(fileDataArr != FileDataMonitor.ERROR_ARRAY)
        {     //non-error status returned
          if(fileDataArr != null)
          {   //new data read in from client-versions-information file
            dataValidFlag = false;     //indicate data not valid
            final long lastModTimeMs =
                             fileDataMonitorObj.getFileLastModifiedTimeMs();
            logObj.debug2("QWVersionsInfoMgr:  New last-modified " +
                             "time value = " + lastModTimeMs + ", prev = " +
                             fileDataMonitorObj.getPrevFileLastModTimeMs());
            if(istiXmlUtilsObj == null)
              istiXmlUtilsObj = new IstiXmlUtils();
            try
            {     //read XML data from versions-info file:
              final boolean retFlag = istiXmlUtilsObj.loadStream(
                      new ByteArrayInputStream(fileDataArr),rootElementName,
                                                        versionsInfoUrlStr);
              if(retFlag)
              {    //XML data loaded OK; process it (save result flag)
                if(lastProcessFlag=processVersionsInfoData(
                            istiXmlUtilsObj.getRootElement(),lastModTimeMs))
                {  //data processed OK
                  dataValidFlag = true;
                  logObj.info("Loaded client-versions information " +
                                     "from \"" + versionsInfoUrlStr + "\"");
                }
              }
              else
              {   //error loading XML data; fetch and log message
                logObj.warning("QWVersionsInfoMgr:  Error reading " +
                                     "XML data from versions-info file:  " +
                                         istiXmlUtilsObj.getErrorMessage());
                processVersionsInfoData(null,0); //indicate error fetching
                lastProcessFlag = false;         //indicate failure
              }
            }
            catch(Exception ex)
            {       //error reading XML data
              logObj.warning("QWVersionsInfoMgr:  Error processing " +
                                "XML data from versions-info file:  " + ex);
              processVersionsInfoData(null,0);   //indicate error fetching
              lastProcessFlag = false;           //indicate failure
            }
          }
          else
          {   //no new data read in from client-versions-information file
            if(!dataValidFlag && lastProcessFlag)
            {      //data-valid flag clear (because file was inaccessible)
                   // but previous processing of file was OK
              dataValidFlag = true;         //indicate data valid again
              logObj.info("QWVersionsInfoMgr:  Regained access to " +
                   "versions-info file and last-modified time not changed");
            }
            else
            {
              logObj.debug4("QWVersionsInfoMgr:  Versions-info file " +
                                          "last-modified time not changed");
            }
          }
        }
        else
        {     //error checking/reading client-versions-information file
          dataValidFlag = false;       //indicate data not valid
          if(firstTimeFlag)
          {   //first time through; log warning message
            logObj.warning("QWVersionsInfoMgr:  " +
                                     "Error loading versions-info file:  " +
                                fileDataMonitorObj.getErrorMessageString());
          }
          else
          {   //not first time through; log debug message
            logObj.debug2("QWVersionsInfoMgr:  " +
                                fileDataMonitorObj.getErrorMessageString());

          }
        }
        if(firstTimeFlag)
        {     //this was the first time through
          firstTimeFlag = false;       //indicate not first time through
        }
        if(verInfoPollDelayMS <= 0)
        {     //poll delay value not positive
          logObj.debug("QWVersionsInfoMgr:  Poll delay = " +
                         verInfoPollDelayMS + "ms, exiting polling thread");
          break;                  //exit loop (and thread)
        }
        logObj.debug4("QWVersionsInfoMgr:  Delaying " +
                                verInfoPollDelayMS + "ms before next poll");
              //delay between checks of the versions-info file:
        waitForNotify(verInfoPollDelayMS);
      }
    }
    catch(Exception ex)
    {    //some kind of exception error; log it
      logObj.warning("Exception in 'QWVersionsInfoMgr' thread:  " + ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
  }

  /**
   * Returns the last-modified time for the client-versions-information
   * file.
   * @return The last-modified time for the client-versions-information
   * file (in ms since 1/1/1970), or 0 if not known.
   */
  public synchronized long getVerInfoLastModTimeMs()
  {
    return verInfoLastModTimeMs;
  }

  /**
   * Returns the status of the versions-information data.
   * @return true if the versions-information data is valid, false is not.
   */
  public boolean getDataValidFlag()
  {
    return dataValidFlag;
  }

/*
  public static void main(String [] args)
  {
    final LogFile logObj =
                         new LogFile(null,LogFile.NO_MSGS,LogFile.ALL_MSGS);
    final String urlStr = "ClientVersInfoTest.xml";
    final QWVersionsInfoMgr mgrObj = new QWVersionsInfoMgr(
                                        urlStr,"ClientVersionsInfo",logObj);
    mgrObj.start();

    UtilFns.sleep(2500);
    logObj.info("UnknownClientMsg:  " + mgrObj.getUnknownClientMsgStr());

    final DistribInformation infoObj =
                               mgrObj.getDistribInfoObj("QWClient_default");
    logObj.info("infoObj:  " + infoObj.getDisplayString());

    logObj.info("NewVerMsg:  " + infoObj.getNewerVersionMsgStr());
    logObj.info("NoConnMsg:  " + infoObj.getNoConnVersionMsgStr());
    logObj.info("CurVerMsg:  " + infoObj.getCurrentVersionMsgStr());

    logObj.info("0.923Beta too old = " +
                                      infoObj.isVersionTooOld("0.923Beta"));
    logObj.info("1.0 too old = " + infoObj.isVersionTooOld("1.0"));
    logObj.info("1.0Beta too old = " + infoObj.isVersionTooOld("1.0Beta"));
    logObj.info("1.01 too old = " + infoObj.isVersionTooOld("1.01"));

    logObj.info("0.923Beta upgrd newer = " +
                                       infoObj.isUpgradeNewer("0.923Beta"));
    logObj.info("1.5 upgrd newer = " + infoObj.isUpgradeNewer("1.5"));
    logObj.info("1.5Beta upgrd newer = " +
                                         infoObj.isUpgradeNewer("1.5Beta"));
    logObj.info("1.6 upgrd newer = " + infoObj.isUpgradeNewer("1.6"));

    mgrObj.terminate();
  }
*/


  /**
   * Class DistribInformation defines a block of upgrade information for
   * a distribution and a set of query methods.
   */
  public static class DistribInformation
  {
         /** The name of the distribution. */
    public final String distribNameStr;
         /** The version-number string for the available upgrade. */
    public final String newVersionNumStr;
         /** The minimum-version-number string allowed to connect. */
    public final String minConnNumStr;
         /** Message string for the available upgrade. */
    public final String newerVersionMsgStr;
         /** Message string for client too old to connect. */
    public final String noConnVersionMsgStr;
         /** Message string for client up to date. */
    public final String currentVersionMsgStr;
         /** URL string for the distribution zip. */
    public final String distZipLinkStr;
         /** URL string for the distribution website. */
    public final String distWebsiteLinkStr;

         /** XML-tag string for "Name" attribute. */
    public static final String NAME_TAG = "Name";
         /** XML-tag string for "Versions" element. */
    public static final String VERSIONS_TAG = "Versions";
         /** XML-tag string for "NewVersionNum" attribute. */
    public static final String NEW_VER_NUM_TAG = "NewVersionNum";
         /** XML-tag string for "MinConnNum" attribute. */
    public static final String MIN_CONN_NUM_TAG = "MinConnNum";
         /** XML-tag string for "NewerVersionMsg" element. */
    public static final String NEW_VER_MSG_TAG = "NewerVersionMsg";
         /** XML-tag string for "NoConnVersionMsg" element. */
    public static final String NOCONN_VER_MSG_TAG = "NoConnVersionMsg";
         /** XML-tag string for "CurrentVersionMsg" element. */
    public static final String CUR_VER_MSG_TAG = "CurrentVersionMsg";
         /** XML-tag string for "Links" element. */
    public static final String LINKS_TAG = "Links";
         /** XML-tag string for "DistZip" attribute. */
    public static final String DIST_ZIP_TAG = "DistZip";
         /** XML-tag string for "DistWebsite" attribute. */
    public static final String DIST_WEBSITE_TAG = "DistWebsite";

    /**
     * Creates a block of upgrade informations for a distribution.
     * @param distribElemObj the XML-element object from which to
     * fetch the upgrade information.
     */
    public DistribInformation(Element distribElemObj)
    {
                             //fetch distribution-name attribute:
      distribNameStr = distribElemObj.getAttributeValue(NAME_TAG);
      Element elemObj;       //fetch "Versions" element:
      if((elemObj=distribElemObj.getChild(VERSIONS_TAG)) != null)
      {  //"Versions" element OK; fetch version-number attributes
        newVersionNumStr = elemObj.getAttributeValue(NEW_VER_NUM_TAG);
        minConnNumStr = elemObj.getAttributeValue(MIN_CONN_NUM_TAG);
      }
      else
      {  //"Versions" element not OK; clear version-number strings
        newVersionNumStr = null;
        minConnNumStr = null;
      }
                                  //fetch "NewerVersionMsg" element text:
      newerVersionMsgStr = distribElemObj.getChildTextTrim(NEW_VER_MSG_TAG);
      noConnVersionMsgStr =       //fetch "NoConnVersionMsg" element text:
                        distribElemObj.getChildTextTrim(NOCONN_VER_MSG_TAG);
      currentVersionMsgStr =      //fetch "CurrentVersionMsg" element text:
                           distribElemObj.getChildTextTrim(CUR_VER_MSG_TAG);
                             //fetch "Links" element:
      if((elemObj=distribElemObj.getChild(LINKS_TAG)) != null)
      {  //"Links" element OK; fetch URL attributes
        distZipLinkStr = elemObj.getAttributeValue(DIST_ZIP_TAG);
        distWebsiteLinkStr = elemObj.getAttributeValue(DIST_WEBSITE_TAG);
      }
      else
      {  //"Links" element not OK; clear URL strings
        distZipLinkStr = null;
        distWebsiteLinkStr = null;
      }
    }

    /**
     * Returns a status indicator for the distribution information.
     * @return true if the distribution information is valid; false if not.
     */
    public boolean isDataValid()
    {
      return (distribNameStr != null && distribNameStr.length() > 0 &&
                newVersionNumStr != null && newVersionNumStr.length() > 0 &&
                     distZipLinkStr != null && distZipLinkStr.length() > 0);
    }

    /**
     * Compares the upgrade version string to the given version string.
     * @param verStr the version string to compare.
     * @return true if the upgrade version string is newer than the given
     * version string; false if not.
     */
    public boolean isUpgradeNewer(String verStr)
    {
      return (UtilFns.compareVersionStrings(newVersionNumStr,verStr) > 0);
    }

    /**
     * Compares the minimum-connect version string to the given version
     * string.
     * @param verStr the version string to compare.
     * @return true if the minimum-connect version string is newer than
     * the given version string; false if not.
     */
    public boolean isVersionTooOld(String verStr)
    {
      return (minConnNumStr != null && minConnNumStr.length() > 0 &&
                   UtilFns.compareVersionStrings(minConnNumStr,verStr) > 0);
    }

    /**
     * Returns the newer-version-message string.
     * @return The newer-version-message string.
     */
    public String getNewerVersionMsgStr()
    {
      return newerVersionMsgStr;
    }

    /**
     * Returns the connection-not-allowed-message string.
     * @return The connection-not-allowed-message string.
     */
    public String getNoConnVersionMsgStr()
    {
      return noConnVersionMsgStr;
    }

    /**
     * Returns the version-is-current-message string.
     * @return The version-is-current-message string.
     */
    public String getCurrentVersionMsgStr()
    {
      return currentVersionMsgStr;
    }

    /**
     * Returns a display string for this object.
     * @return A display string for this object.
     */
    public String getDisplayString()
    {
      return "name=\"" + distribNameStr + "\", newVer=" + newVersionNumStr +
         ", minConnVer=" + minConnNumStr + ", zipLink=\"" + distZipLinkStr +
                               "\", webLink=\"" + distWebsiteLinkStr + "\"";
    }
  }
}
