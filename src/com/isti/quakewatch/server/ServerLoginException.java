//ServerLoginException.java:  Thrown when an error occurs while
//                            setting up a client login to the server.
//
// 10/15/2004 -- [ET]
//

package com.isti.quakewatch.server;

/**
 * ServerLoginException is thrown when an error occurs while
 * setting up a client login to the server.
 */
public class ServerLoginException extends Exception
{
  /**
   * Creates a server-login exception.
   * @param msgStr the message string to use.
   */
  public ServerLoginException(String msgStr)
  {
    super(msgStr);
  }
}
