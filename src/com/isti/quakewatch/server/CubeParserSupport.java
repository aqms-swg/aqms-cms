//CubeParserSupport.java:  Provides support methods for parsing
//                         CUBE-format messages into a JDOM/XML
//                         tree of elements.
//
//  7/19/2006 -- [ET]  Extracted methods from 'CubeFormatParser' class.
//  8/15/2007 -- [ET]  Modified to detect and pass through XML-format
//                     messages; added 'parseMessageString()' method and
//                     abstract 'parseCubeMsgStringData()' method.
//  9/21/2007 -- [ET]  Added 'getMsgSubstring()' method; added try/catch
//                     block to 'parseMessageString()'.
//  4/21/2008 -- [ET]  Modified parsing to handle numeric strings that
//                     have whitespace before a plus sign.
// 11/11/2009 -- [ET]  Added 'LOCMETH_INTERNAL_STR' field.
//  3/19/2010 -- [ET]  Added 'getProdTypeXlatEntry()' method.
//  4/15/2010 -- [ET]  Added 'decValFormatObj' variable.
//

package com.isti.quakewatch.server;

import java.util.Vector;
import java.util.Hashtable;
import java.util.Arrays;
import java.util.Date;
import java.text.DateFormat;
import java.text.NumberFormat;
import org.jdom.Element;
import org.jdom.Attribute;
import com.isti.util.UtilFns;
import com.isti.util.IstiXmlUtils;
import com.isti.quakewatch.common.MsgTag;
import com.isti.quakewatch.util.QWUtils;
import com.isti.quakewatch.util.CubeCodeTranslator;

/**
 * Class CubeParserSupport provides support methods for parsing
 * CUBE-format messages into a JDOM/XML tree of elements.
 */
public abstract class CubeParserSupport
{
                        //index values for CUBIC message types:
  protected static final int EQUAKE_MSGIDX = 0;  //Earthquake
  protected static final int DELQK_MSGIDX = 1;   //Delete Earthquake
  protected static final int TEXT_MSGIDX = 2;    //Text Comment
  protected static final int LINK_MSGIDX = 3;    //Link to Add-on
  protected static final int TRUMP_MSGIDX = 4;   //Trump event
  protected static final String [] MSGSTRS_ARR = //array of message ID strs
                new String [] { MsgTag.EQUAKE_MSGSTR, MsgTag.DELQK_MSGSTR,
                                   MsgTag.TEXT_MSGSTR, MsgTag.LINK_MSGSTR,
                                   MsgTag.TRUMP_MSGSTR };
                        //convert array of message ID strings to a Vector:
  protected static final Vector msgStringsVec =
                                     new Vector(Arrays.asList(MSGSTRS_ARR));
                        //table for translating product-type codes:
  protected static final Hashtable prodTypeXlatTable = new Hashtable();
                        //product-type code string for "fr" product type:
  protected static final String TYPE_FR_STR = "fr";
                        //key strings for distinguishing "fr" products:
  protected static final String RTMECH_KEYSTR = "Real-time Focal Mechanism";
  protected static final String FEELIT_KEYSTR = "Did you feel it";
              //location-method value for 'Scope'=="Internal':
  protected static final String LOCMETH_INTERNAL_STR =
                                       CubeCodeTranslator.CUBE_LOCMETHZ_STR;
              //location-method value for "probable quarry explosion":
  protected static final String LOCMETH_QUARRY_STR =
                                       CubeCodeTranslator.CUBE_LOCMETHQ_STR;
  protected static final String ASTERISK_STR = "*";
              //lower-case version of "Tsunami" string:
  protected static final String LC_TSUNAMI_STR =
                                               MsgTag.TSUNAMI.toLowerCase();
  protected String errorMessage = null;     //error message from parsing
              //date format object for parsing date/time strings:
  protected final DateFormat cubeDateFormatterObj =
       UtilFns.createDateFormatObj("yyyyMMddHHmmssSSS",QWUtils.gmtTimeZone);
              //date-formatter object for 'parseMessageString()' method:
  protected final DateFormat xmlDateFormatterObj =
                                           QWUtils.getXmlDateFormatterObj();
    /** Floating-point number formatter object. */
  protected final NumberFormat decValFormatObj = NumberFormat.getInstance();
    /** Maximum # of digits after decimal point for 'decValFormatObj'. */
  public static final int MAX_DECFMT_DIGITS = 8;

  static      //static initialization block; executes only once
  {                     //fill entries for product-type translate table:
    prodTypeXlatTable.put("smsc",MsgTag.SHAKEMAP_URL);
    prodTypeXlatTable.put("smnc",MsgTag.SHAKEMAP_URL);
    prodTypeXlatTable.put("smgl",MsgTag.SHAKEMAP_URL);
    prodTypeXlatTable.put("smak",MsgTag.SHAKEMAP_URL);
    prodTypeXlatTable.put("smuw",MsgTag.SHAKEMAP_URL);
    prodTypeXlatTable.put("smnn",MsgTag.SHAKEMAP_URL);
    prodTypeXlatTable.put("smuu",MsgTag.SHAKEMAP_URL);
    prodTypeXlatTable.put("ncfm1",MsgTag.FOCAL_MECH1_URL);
    prodTypeXlatTable.put("ncfm2",MsgTag.FOCAL_MECH2_URL);
    prodTypeXlatTable.put("mtensor",MsgTag.MOMENT_TENSOR_URL);
  }


  /**
   * Creates parsing support object.
   */
  public CubeParserSupport()
  {
                   //set number formatter to always show trailing ".0":
    decValFormatObj.setMinimumFractionDigits(1);
                   //set maximum number of digits after decimal point:
    decValFormatObj.setMaximumFractionDigits(MAX_DECFMT_DIGITS);
    decValFormatObj.setGroupingUsed(false);      //set no commas in output

  }

  /**
   * Parses a CUBE-format message into a JDOM/XML tree of elements.
   * @param messageString the message string to be parsed.
   * @param feederMsgNum message number to be entered as "FeederMsgNum" in
   * the returned Element, or -1 for none.
   * @param hostNameStr name of originating host to be entered as
   * "SourceHost" in the returned Element (or null for none).
   * @param hostMessageID message ID number from QDDS hub to be entered
   * as "SrcHostMsgID" in the returned (if 'hostNameStr' not null).
   * @return The resulting "DataMessage" Element object, or null if an
   * error occured (in which case the associated error message may be
   * fetched via the 'getErrorMessage()' method).
   */
  public Element parseMessageString(String messageString, long feederMsgNum,
                                     String hostNameStr, long hostMessageID)
  {
    try
    {
                   //save current time (as an XML-format time string):
      final String timeReceivedStr;
      synchronized(xmlDateFormatterObj)
      {    //only allow one thread at a time to use date-format object
        timeReceivedStr = xmlDateFormatterObj.format(new Date());
      }
      messageString = messageString.trim();   //trim any surrounding spaces
                //create DataMessage.Action attribute object:
      final Attribute actionAttribObj =
                                 new Attribute(MsgTag.ACTION,MsgTag.UPDATE);
      final Element parsedMsgElem;
      if(messageString.length() > 0 && messageString.charAt(0) == '<')
      {  //message starts like XML format
        try
        {            //parse string into element objects:
          parsedMsgElem = IstiXmlUtils.stringToElement(messageString);
        }
        catch(Exception ex)
        {  //error parsing string into element objects; set error message
          setErrorMessage("Error parsing message as XML:  " + ex);
          return null;
        }
      }
      else
      {    //message does not start like XML format; parse CUBE-format data:
        if((parsedMsgElem=parseCubeMsgStringData(
                    messageString,timeReceivedStr,actionAttribObj)) == null)
        {
          return null;
        }
      }
           //create DataMessage element:
      final Element dataMessageElem = new Element(MsgTag.DATA_MESSAGE);
                //add EQMessage element to DataMessage element:
      dataMessageElem.addContent(parsedMsgElem);
                //enter DataMessage.Action attribute:
      dataMessageElem.setAttribute(actionAttribObj);
                //set TimeReceived attribute for DataMessage:
      dataMessageElem.setAttribute(MsgTag.TIME_RECEIVED,timeReceivedStr);
                //set FeederMsgNum attribute for DataMessage:
      if(feederMsgNum >= 0)
      {    //given feeder-message-number value is not negative
        dataMessageElem.setAttribute(MsgTag.FEEDER_MSG_NUM,
                                               Long.toString(feederMsgNum));
      }
           //set SourceHost attributes for DataMessage (if given):
      if(hostNameStr != null)
      {    //host name given; set it and host 'messageID' value
        dataMessageElem.setAttribute(MsgTag.SOURCE_HOST,hostNameStr);
        dataMessageElem.setAttribute(MsgTag.SRC_HOST_MSG_ID,
                                              Long.toString(hostMessageID));
      }
      return dataMessageElem;       //return generated elements
    }
    catch(Exception ex)
    {  //some kind of exception error; set message
      setErrorMessage("Exception error parsing CUBE message string:  " + ex);
      return null;
    }
  }

  /**
   * Parses CUBE-format message data into a JDOM/XML tree of elements.
   * This abstract method will be defined in subclasses.
   * @param messageString the message string to be parsed.
   * @param timeReceivedStr current time (as an XML-format time string).
   * @param actionAttribObj attrib whose value will be set by this method.
   * @return The resulting "EQMessage" Element object, or null if an
   * error occured (in which case the associated error message may be
   * fetched via the 'getErrorMessage()' method).
   */
  protected abstract Element parseCubeMsgStringData(String messageString,
                         String timeReceivedStr, Attribute actionAttribObj);

  /**
   * Parses the given string into an integer.
   * @param str numeric string to parse.
   * @param itemNameStr name of item being parsed (used for error
   * message).
   * @return A 'Integer' object containing the parsed value, or null
   * if the value could not be parsed (in which case an error message
   * is set via the 'setErrorMessage()' method).
   */
  protected Integer parseCubicInteger(String str, String itemNameStr)
  {
    try
    {
      str = str.trim();           //trim any leading/trailing whitespace
      int sPos = 0;
      if(str.startsWith("+"))
      {  //begins with '+'
        ++sPos;                   //start with next character
        str = str.substring(sPos).trim();   //trim any spaces after '+'
      }
      return Integer.valueOf(str);    //parse numeric string
    }
    catch(Exception ex)
    {
      if(str.startsWith("*"))          //if item is "***" then
        return Integer.valueOf(0);         //return 0
    }
    setErrorMessage("Unable to parse " + itemNameStr + " value (" + str +
                                                                       ")");
    return null;
  }

  /**
   * Parses the given string into a number.  The numeric string must
   * be an integer that is to be divided by a value after it is parsed.
   * @param str numeric string to parse.
   * @param divider value to divide the result by before returning it.
   * @param itemNameStr name of item being parsed (used for error
   * message).
   * @return A 'Double' object containing the parsed value, or null
   * if the value could not be parsed (in which case an error message
   * is set via the 'setErrorMessage()' method).
   */
  protected Double parseCubicDouble(String str, double divider,
                                                         String itemNameStr)
  {
    final Double doubleObj;       //parse numeric string:
    if((doubleObj=parseLongDouble(str,divider)) != null)
      return doubleObj;      //if OK then return object
    setErrorMessage("Unable to parse " + itemNameStr + " value (" + str +
                                                                       ")");
    return null;
  }

  /**
   * Parses the given string into a number.  The numeric string must
   * be an integer that is to be divided by a value after it is parsed.
   * @param str numeric string to parse.
   * @param divider value to divide the result by before returning it.
   * @return A 'Double' object containing the parsed value, or null
   * if the value could not be parsed.
   */
  public static Double parseLongDouble(String str, double divider)
  {
    try
    {
      str = str.trim();           //trim any leading/trailing whitespace
      int sPos = 0;
      if(str.startsWith("+"))
      {  //begins with '+'
        ++sPos;                   //start with next character
        str = str.substring(sPos).trim();   //trim any spaces after '+'
      }
              //parse value as integer, then convert, divide,
              // create 'Double' wrapper object and return it:
      return Double.valueOf((double)Long.parseLong(str) / divider);
    }
    catch(Exception ex)
    {
    }
    return null;        //if error then return null
  }

  /**
   * Convenience method for fetching a substring, with length checks.
   * @param msgStr source message string.
   * @param msgStrLen source message-string length.
   * @param startPos start position for substring (inclusive).
   * @param endPos end position for substring (exclusive).
   * @return The fetched substring, or null if no characters were in range
   * (because the source message string was too short).
   */
  public static String getMsgSubstring(String msgStr, int msgStrLen,
                                                   int startPos, int endPos)
  {
    if(msgStrLen <= startPos)     //if no characters in range then
      return null;                //indicate no data
                   //return substring (after checking length):
    return (msgStrLen > endPos) ? msgStr.substring(startPos,endPos) :
                                                 msgStr.substring(startPos);
  }

  /**
   * Translates the given CUBE product-type code to a QuakeWatch product
   * code.
   * @param keyStr CUBE product-type code string.
   * @return The associated QuakeWatch product code, or null if no match.
   */
  public static String getProdTypeXlatEntry(String keyStr)
  {
    final Object obj;
    if((obj=prodTypeXlatTable.get(keyStr)) instanceof String)
      return (String)obj;    //if table entry match then return entry
         //if starts with "Tsunami" then return "Tsunami" string:
    return keyStr.toLowerCase().startsWith(LC_TSUNAMI_STR) ?
                                                      MsgTag.TSUNAMI : null;
  }

  /**
   * Calculates and returns the "Menlo Park(USGS) check char (Nov.94)"
   * code for the given String (excess 36 modulo 91).
   * @param str the string to use.
   * @return the check character.
   */
  public static char calcMenloCheckChar(String str)
  {
    final byte [] byteArr = str.getBytes();
    final int len = byteArr.length;
    int sum = 0;
    for(int p=0; p<len; ++p)
      sum = ((((sum&1)!=0)?0x8000:0) + (sum>>1) + byteArr[p]) & 0xFFFF;
    return (char)(36 + (sum % 91));
  }

  /**
   * Enter error message (if none previously entered).
   * @param str the error message text.
   */
  protected void setErrorMessage(String str)
  {
    if(errorMessage == null)      //if no previous error then
      errorMessage = str;         //set error message
  }

  /**
   * Returns true if an error was detected.  The error message may be
   * fetched via the 'getErrorMessage()' method.
   * @return true if error; false if not.
   */
  public boolean getErrorFlag()
  {
    return (errorMessage != null);
  }

  /**
   * Returns message string for last error (or 'No error' if none) and
   * clears the error.
   * @return The error message text (or 'No error' if none).
   */
  public String getErrorMessage()
  {
                                  //save the error message:
    final String retStr = (errorMessage != null) ? errorMessage : "No error";
    errorMessage = null;          //clear the error
    return retStr;                //return the error message
  }

  /** Clears the error message string. */
  public void clearErrorMessage()
  {
    errorMessage = null;
  }
}
