//TestFeeder.java:  QuakeWatch feeder plugin module that receives QDDS
//                  messages and passes them into the QuakeWatch server.
//
//  6/19/2003 -- [KF]
//

package com.isti.quakewatch.server;

import java.io.*;
import com.isti.util.LogFile;
import com.isti.util.UtilFns;

import java.util.Date;

/**
 * Class TestFeeder is a QuakeWatch feeder plugin module that receives QDDS
 * messages (via a log file) and passes them into the QuakeWatch server.
 */
public class TestFeeder extends QddsFeeder
{
  public static final String MODULE_NAME = "TestFeeder";
  public static final String VERSION = "1.0";    //name and version strings
  public static final String REVISION_STRING = MODULE_NAME + ", " +
                                                       "Version " + VERSION;
  public static final String FEEDER_NAME = "TEST";  //feeder name
  private final File inputFile = new File("TestFeeder.txt");

  /**
   * Constructs a test feeder module.
   */
  public TestFeeder()
  {
    super(MODULE_NAME, FEEDER_NAME);
  }

  /**
   * Returns the revision String for the feeder.
   * @return the revision String for the feeder.
   */
  public String getRevisionString()
  {
    return REVISION_STRING;
  }

  /**
   * Executing method for the feeder thread.  Starts execution of test
   * threads.
   */
  public void run()
  {
    if(logObj == null)  //if no log file setup then create dummy object
      logObj = new LogFile(null,LogFile.NO_MSGS,LogFile.ERROR);

    if (!inputFile.canRead())  //if file can not be read
    {
      logObj.error(
          "TestFeeder (\"" + feederName +
          "\"):  Cannot read input file + (" + inputFile.getPath() + ")");
      return;
    }

    BufferedReader rdrObj = null;
    try
    {
      //create file input reader
      rdrObj =  new BufferedReader(new FileReader(inputFile));
      String str, subStr;

      long timeDiff = 0;

      while (!isInterrupted())  //while not interrupted
      {
        while((str=rdrObj.readLine()) != null)  //for each line of data in file
        {
          final int dateEndIndex = str.indexOf(": ");

          if (dateEndIndex > 0)  //if string could contain the date
          {
            try
            {
              //determine the date
              subStr = str.substring(0, dateEndIndex);
              final Date dateObj = logObj.getDateFormat().parse(subStr);
              final long timeDelta = System.currentTimeMillis() - dateObj.getTime();

              //if time difference not determined
              if (timeDiff <= 0)
              {
                timeDiff = timeDelta;
              }

              if (timeDelta < timeDiff)
              {
                final long millis = timeDiff - timeDelta;
                logObj.debug4(
                    "TestFeeder (\"" + feederName +
                    "\"):  sleeping for " + millis + " milliseconds.");
                sleep(millis);
              }
            }
            catch (java.text.ParseException ex)
            {
              logObj.warning("TestFeeder (\"" + feederName + "\"):  " + ex);
            }
          }

          //determine if the str contains a message
          final int msgNumIndex = str.indexOf(MSGNUM_STR);
          final int msgStrIndex = str.lastIndexOf(PCOLON_STR);
          final int hostNameStrIndex = str.indexOf(FROMHOST_STR);
          final int hostMessageIDIndex = str.indexOf(MSGID_STR);

          if (msgNumIndex >= 0 && msgStrIndex >= 0 && hostNameStrIndex >= 0 &&
              hostMessageIDIndex >= 0)  //if all required strings were found
          {
            try
            {
              //get the message number
              subStr = str.substring(
                  msgNumIndex+MSGNUM_STR.length(),
                  hostNameStrIndex);
              final int msgNum = Integer.valueOf(subStr).intValue();

              //get the message string
              subStr = str.substring(
                  msgStrIndex+PCOLON_STR.length());
              final String msgStr = subStr;

              //get the host name string
              subStr = str.substring(
                  hostNameStrIndex+FROMHOST_STR.length(),
                  hostMessageIDIndex);
              final String hostNameStr = subStr;

              //get the host message ID
              subStr = str.substring(
                  hostMessageIDIndex+MSGID_STR.length(),
                  msgStrIndex);
              final long hostMessageID = Long.valueOf(subStr).longValue();

              //process the message
              writeFile(msgNum, msgStr, hostNameStr, hostMessageID);
            }
            catch (Exception ex)
            {
              logObj.warning("TestFeeder (\"" + feederName + "\"):  " + ex);
            }
          }
        }
      }
    }
    catch (Exception ex)
    {
      logObj.error("TestFeeder (\"" + feederName + "\"):  " + ex);
    }
    finally
    {
    	UtilFns.closeQuietly(rdrObj);
    }
  }
}
