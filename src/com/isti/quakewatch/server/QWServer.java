//QWServer.java:  QuakeWatch server program.
//
//   6/9/2003 -- [ET]  Version 0.24Beta:  Added initial 'QWAcceptor'
//                     and 'QWServices' implementations; added
//                     'logFilesMaxAgeInDays' parameter and other
//                     logfile enhancements.
//  6/10/2003 -- [ET]  Version 0.241Beta:  Added 'messageLogSwitchDays'
//                     parameter and support for using date codes in
//                     the thread-info, messages and "QWFeeder" log files.
//  6/11/2003 -- [HS,ET]  Version 0.242Beta:  Added shutdown hook to perform
//                     exit cleanup; improved message-cache number file.
//  6/19/2003 -- [KF]  Version 0.243Beta:  Added support for QDDS "Trump"
//                     event message-type.
//  6/25/2003 -- [ET]  Version 0.244Beta:  Fixed so that running with the
//                     help parameter ("-h") does not throw a null-pointer
//                     exception (modified so 'exitCleanup()' checks for
//                     uninitialized ORB manager and message cache objects).
//  7/28/2003 -- [ET]  Version 0.25Beta:  Reduced 'maxResendNumMsgs' from
//                     150 to 50; improved handling of message number; put
//                     memory-info output into a separate log file and
//                     added 'memoryLogMaxAgeInDays' & 'memoryLogSwitchDays'
//                     parameters; added get-server-name QWService method.
//   8/6/2003 -- [ET]  Version 0.251Beta:  Fixed problem where using
//                     "-h" and "-v" parameters could still generate
//                     configuration file errors; added support for
//                     alternate-servers list and QWServices method.
//  8/19/2003 -- [ET]  Version 0.252Beta:  Added support for "Quarry"
//                     attribute in event messages.
//  9/10/2003 -- [ET]  Version 0.3Beta:  Added support event persistence
//                     via cached archive; changed 'maxCacheAgeInMinutes'
//                     to floating-point 'maxCacheAgeInDays'.
//  9/11/2003 -- [ET]  Version 0.31Beta:  Made minor internal improvements
//                     to archive manager.
//  9/17/2003 -- [ET]  Version 0.32Beta:  Fixed handling of cached-archive
//                     data with breaks in message-number sequence.
//  11/5/2003 -- [ET]  Version 0.33Beta:  Added code to stop feeders on
//                     terminate; implemented 'QWRelayFeeder' module.
//  11/6/2003 -- [ET]  Version 0.34Beta:  Minor mods to 'QWRelayFeeder'.
//  11/7/2003 -- [ET]  Version 0.4Beta:  Updated version number.
// 11/13/2003 -- [ET]  Version 0.41Beta:  Modified communications code to
//                     always check for event updates when reconnecting to
//                     a server.
// 11/26/2003 -- [ET]  Version 0.42Beta:  Implemented 'QWViaFileFeeder'
//                     module.
//   1/7/2004 -- [ET]  Version 0.43Beta:  Added optional support for event
//                     domain, type and name strings for structured events.
//  1/22/2004 -- [ET]  Version 0.431Beta:  Implemented 'QWCorbaFeeder'
//                     module.
//  2/26/2004 -- [ET]  Version 0.435Beta:  Modified to make date-format-
//                     object usage thread safe; updated OpenORB libraries
//                     (to "cvs_20040223_mod" version).
//   7/1/2004 -- [ET]  Version 0.44Beta:  Fixed issue with dealing with
//                     messages containing only a 'type' name.
//  7/14/2004 -- [ET]  Version 0.441Beta:  Added 'allowDuplicatesFlag'
//                     configuration parameter.
//  7/22/2004 -- [ET]  Version 0.442Beta:  Modified cache to store string
//                     form of event messages (instead of XML element
//                     objects), lowering memory usage; added configuration
//                     parameters 'cacheMaxObjectCount' and
//                     'cacheMaxTotalDataSize'.
//  8/16/2004 -- [KF]  Version 0.443Beta:  Moved configuration items into
//                     new 'ServerConfigManager' class; added
//                     'ServerPasswordManager' class.
//  8/17/2004 -- [ET]  Lowered log level on alive-msg-thread log output msg.
//   9/1/2004 -- [KF]  Version 0.444Beta:  Added authentication support.
//   9/1/2004 -- [ET]  Added clients-manager object; modified
//                     'AliveMessageThread' to extend 'IstiNotifyThread'.
//   9/2/2004 -- [KF]  Added verification of authentication information.
//   9/7/2004 -- [ET]  Added "housekeeping" thread and call to method
//                     'removeOldClients()' in clients manager.
// 10/14/2004 -- [ET]  Version 0.46Beta:  Implemented client login-info
//                     check.
// 10/15/2004 -- [ET]  Version 0.461Beta:  Added retry to password-database
//                     access.
//  11/5/2004 -- [ET]  Version 0.47Beta:  Added clients-information-tracking
//                     log file; added login information file option to
//                     QWRelay feeder module; made QWRelayFeeder always
//                     try to use original time-generated time.
// 11/10/2004 -- [ET]  Version 0.471Beta:  Added 'maximumMessageSize' and
//                     'publishNumericIPFlag' configuration parameters;
//                     modified to output to clients-info log immediately
//                     in response to client add/remove.
// 11/12/2004 -- [ET]  Version 0.472Beta:  Modified to prevent 'QWRelayFeeder'
//                     from being able to connect to its own QWServer.
// 11/19/2004 -- [ET]  Version 0.473Beta:  Added support for server redirect.
// 11/24/2004 -- [ET]  Version 0.474Beta:  Added support for QWServices
//                     'disconnectClient()' method; added "staticInvoke"
//                     methods.
// 11/30/2004 -- [ET]  Version 0.475Beta:  Updated OpenORB to "nightly"
//                     build from 2004-11-17 (for Java 1.5 support);
//                     added command-line parameter "--configFile" for
//                     specifying the configuration file.
//  12/1/2004 -- [ET]  Version 0.476Beta:  Updated QDDS to version
//                     "1.3.3 Level 2004.11.08".
//  12/3/2004 -- [ET]  Version 0.48Beta:  Updated version number for
//                     release.
// 12/10/2004 -- [ET]  Version 0.481Beta:  Added additional methods for
//                     web services support.
// 12/22/2004 -- [ET]  Modified constructor to throw 'StartupFailException'
//                     to avoid "variable not initialized" compile errors.
//  1/10/2005 -- [ET]  Version 0.49Beta:  Added 'clientVInfoPollDelaySecs'
//                     configuration item.
//  2/10/2005 -- [ET]  Version 0.5Beta:  Added support for requesting
//                     messages via feeder-data-source information; added
//                     server-statistics log output; added static method
//                     'setServerBaseDirectoryStr()'; modified to restore
//                     redirected console output streams on exit; modified
//                     so that if configuration file is missing then error
//                     message is written to default log file
//  3/21/2005 -- [ET]  Version 0.51Beta:  Fixed processing of "-configFile"
//                     parameter when followed by an equals sign.
//  4/15/2005 -- [ET]  Version 0.52Beta:  Fixed QWRelayFeeder to make it
//                     actually discard messages whose source is the
//                     feeder's own server, and fixed to prevent extraneous
//                     "Error parsing 'SrcHostMsgID'" messages; fixed to
//                     prevent "Error logging server statistics ...
//                     NullPointerException" messages; modified to add
//                     and verify server-ID string in "mainEvtChLocFile"
//                     event-channel locator file.
//  4/18/2005 -- [ET]  Version 0.53Beta:  Fixed null-pointer exception at
//                     startup (log output before 'initGlobalLogObj()'
//                     log file setup).
//  4/28/2005 -- [ET]  Version 0.54Beta:  Modified to properly recognize
//                     duplicate messages when data-message element of
//                     incoming message contains no child elements (change
//                     to 'QWServerMsgRecord' class); modified to use
//                     feeder-data-source host name and message number to
//                     differentiate between received messages; fixed
//                     'QWRelayFeeder' processing of feeder-data-source
//                     host name and message number values.
//   6/8/2005 -- [ET]  Version 0.55Beta:  Modified processing of client-
//                     versions-info file to use separate thread and
//                     timeout to prevent possibility of client update
//                     not being offered because of indefinite file I/O
//                     access thread block.
//  7/12/2005 -- [ET]  Version 0.56Beta:  Modified cached-archive code
//                     to avoid thread 'interrupt()' calls that can be
//                     problematic under Solaris OS.
//  9/16/2005 -- [ET]  Version 0.57Beta:  Fixed bug (introduced in version
//                     0.55Beta) where updated client-versions-information
//                     was not detected by a client unless the client was
//                     restarted; modified to always setup clients-info
//                     manager and log file (instead of only when
//                     CORBA QWServices in use); modified to cache
//                     certificate file data.
// 12/14/2005 -- [ET]  Version 0.58Beta:  Recompiled with updated QWCommon
//                     (containing QW Web Services enhancements).
// 12/29/2005 -- [ET]  Version 0.583Beta:  Modified clients-manager to
//                     fetch username from connection-info properties
//                     string (if not given directly); modified to handle
//                     non-XML compatible characters received via
//                     'QWCorbaFeeder'; fixed bug in cached-archive manager
//                     where if all archive 'storage' files where too old
//                     for cache then last file would be left open; added
//                     configuration parameter 'checkDupFdrSourceFlag'.
//  3/14/2006 -- [ET]  Version 0.585Beta:  Updated OpenORB libraries (to
//                     "20060309_isti1" version); fixed so that warning
//                     message is logged when message-send to event channel
//                     fails.
//  5/26/2006 -- [ET]  Version 0.586Beta:  Fixed issue where next "alive"
//                     message output by server could be delayed if
//                     "alive" message requested by a client.
//  8/10/2006 -- [ET]  Version 0.587Beta:  Added 'QddsAnssEQ' feeder and
//                     support for "feederParserClass" configuration setting
//                     for 'QWViaFile' and 'QWCorba' feeders; modified to
//                     recognize ShakeMap product-translation codes for
//                     Global and for ShakeMaps from other networks.
//  8/30/2006 -- [ET]  Version 0.588Beta:  Fixed issue where client-versions
//                     update information would remain invalid after
//                     versions-info file was inaccessible and then
//                     accessible; modified to update the client-versions
//                     update information even if the last-modified time
//                     for the client-versions-information file is older
//                     than the one currently held.
//  9/20/2006 -- [ET]  Version 0.59Beta:  Added configuration parameters
//                     'msgKeyElement1Name' and 'msgKeyElement2Name'.
//  10/6/2006 -- [ET]  Version 0.591Beta:  Fixed issue with 'QWViaFileFeeder'
//                     where input files that failed parsing could get
//                     stuck in the 'process' directory and be processed
//                     again and again; minor improvement to
//                     CUBE-to-ANSS-EQ-XML-format message parsing
//                     (added "CUBE_Code" comments).
// 10/18/2006 -- [ET]  Version 0.592Beta:  Modified to wait for feeder
//                     stops to complete before exiting program.
// 11/14/2006 -- [ET]  Version 0.593Beta:  Added support for dynamic
//                     feeder-modules configuration file (via new
//                     'feederModulesCfgFile' configuration parameter);
//                     modified to better handle case where multiple
//                     QWRelay feeders are used (and feeder-data-src
//                     values are not in use), the server is restarted,
//                     and it needs to find the last message received
//                     for each feeder.
// 11/20/2006 -- [ET]  Version 0.594Beta:  Modified to check for
//                     'MsgSrc'/'MsgIdent' values in ANSS-EQ-XML-format
//                     messages (if enabled via new 'checkEQMsgIdentFlag'
//                     configuration parameter).
// 11/29/2006 -- [ET]  Version 0.595Beta:  Updated OpenORB libraries (to
//                     "20061129_isti2" version); fixed clients-manager
//                     issue where if a client checked in before logging
//                     in its username would not be saved.
// 12/18/2006 -- [ET]  Version 0.596Beta:  Added 'memorySizeWarnMBytes',
//                     'threadCountWarnMargin', 'notifSvrStatusLogName' and
//                     'notifSvrStatusWarnFlag' configuration parameters
//                     and implementation; modified 'QWRelay' feeder to
//                     call connection-manager method 'performExitCleanup()'
//                     on feeder-stop; added memory and thread-count info
//                     to statistics and status output.
//   4/4/2007 -- [KF]  Changed to work with OpenSSL generated private keys.
//  5/21/2007 -- [ET]  Version 0.597Beta:  Fixed bug (introduced in version
//                     0.593Beta) where feeder-log-file for via-file feeder
//                     was never created.
//  6/14/2007 -- [ET]  Version 0.598Beta:  Updated OpenORB libraries (to
//                     "20061129_isti3" version).
//  8/15/2007 -- [ET]  Version 0.6Beta:  Modified 'CubeAnssEQParser' and
//                     'CubeFormatParser' classes to detect and pass through
//                     XML-format messages and to allow spaces in date codes.
//  8/30/2007 -- [ET]  Version 0.61Beta:  Modified processing of incoming
//                     message-elements so that whitespace between elements
//                     is removed and control characters within elements are
//                     encoded to "&##;" strings.
//  9/21/2007 -- [ET]  Version 0.62Beta:  Fixed bugs in CUBE-message
//                     parser where incomplete CUBE-field items could
//                     result in 'StringIndexOutOfBoundsException' being
//                     thrown.
//  3/18/2008 -- [ET]  Version 0.631Beta:  Added QWServices methods
//                     'getStatusReportTime()' and 'getStatusReportData()';
//                     added 'stRptRemoteDir' config item and renamed
//                     'stRptRemoteFile' config item to 'stRptRemoteFName';
//                     modified to use QWCommon 'QWReportStrings' fields.
//  3/25/2008 -- [ET]  Version 0.64Beta:  Modified clients manager to
//                     allow recovery of username when client connection
//                     has timed out but client then reuses connection.
//  3/28/2008 -- [ET]  Version 0.641Beta:  Modified QDDS feeder to log
//                     "Not enough characters in message" error at "debug"
//                     level instead of "warning" level; added averaging
//                     to excessive-thread-count detection; added slight
//                     delay between tracked-log-message trigger and status
//                     report output (to allow multiple "simultaneous"
//                     messages to be included in the same report).
//  4/21/2008 -- [ET]  Version 0.642Beta:  Modified CUBE-format parser to
//                     handle numeric strings that have whitespace before
//                     a plus sign.
//  5/21/2008 -- [ET]  Version 0.644Beta:  Added 'CubeToAnssEQXML' and
//                     'CubeToQWXML' test utilities; using updated QWCommon
//                     (client-side communications layer) modified to
//                     release more CORBA resources when closing connection.
//  7/22/2008 -- [ET]  Version 0.65Beta:  Added "outputters" support and
//                     QWCorbaOutputter module.
//   8/1/2008 -- [ET]  Version 0.651Beta:  Modified 'CorbaSenderTest'.
// 10/20/2008 -- [ET]  Version 0.66Beta:  Added QWCorbaOutputter
//                     configuration parameter 'subscribeAllDefaultFlag'
//                     and changed default QWCorbaOutputter behavior to
//                     deliver no messages if no domain/channels are
//                     subscribed to.
//   1/5/2009 -- [ET]  Version 0.661Beta:  Fixed bug in CUBE-format
//                     feeder-parser where version code on delete message
//                     was ignored if message had no text comment.
//   7/2/2009 -- [ET]  Version 0.662Beta:  Modified QWCorbaOutputter to use
//                     QWServer's "serverHostAddress" entry for outputter's
//                     CORBA ORB.
//   7/8/2009 -- [ET]  Version 0.663Beta:  Modified QWCorbaFeeder to use
//                     QWServer's "serverHostAddress" entry for feeder's
//                     CORBA ORB.
//  7/10/2009 -- [ET]  Version 0.664Beta:  Modified to make use of
//                     'serverHostAddress' configuration parameter
//                     when 'qwServicesAvailFlag' parameter is 'false'.
//  10/9/2009 -- [ET]  Version 0.67Beta:  Modified CorbaOutSubscriber
//                     to make minor improvements to delivery-failure
//                     logic and log outputs.
// 11/11/2009 -- [ET]  Version 0.671Beta:  Modified feeder CUBE-format
//                     parser to support location-method code 'Z' for
//                     ANSS-EQ-XML 'Scope'=="Internal".
//  1/25/2010 -- [ET]  Version 0.672Beta:  Improved relay-feeder detection
//                     and rejection of duplicate "recursive" messages
//                     when servers are cross-connected (added attributes
//                     'RelayFirstSvrID' and 'RelayFirstSvrAddr' to
//                     messages received via relay feeder).
//   2/4/2010 -- [ET]  Version 1.0:  Updated version number.
//  4/26/2010 -- [ET]  Version 1.02:  Created 'QddsQuakeMLFeeder' and
//                     'CubeQuakeMLParser' for QuakeML message-format
//                     support.
//   5/5/2010 -- [ET]  Version 1.03:  Fixed issue where relay-feeder
//                     messages with time-generated timestamp in future
//                     would result in other messages not being stored
//                     in file archive; made slight improvement to order
//                     of actions during server shutdown (to squelch
//                     occasional exceptions).
//  5/13/2010 -- [ET]  Version 1.04:  Modified QuakeML-format feeder
//                     parser to interpret messages that are not wrapped
//                     in 'quakeMessage' element envelope.
//  7/16/2010 -- [ET]  Version 1.05:  Improved handling of messages
//                     containing characters greater than 127 (via
//                     'isti.util' update); modified 'QWViaFileFeeder'
//                     to always use UTF-8 charset when decoding input
//                     data; modified to always reject messages with
//                     duplicate feeder-data-source message numbers
//                     when 'checkDupFdrSourceFlag' is true (regardless
//                     of message payload).
//  8/23/2010 -- [ET]  Version 1.06:  Fixed queuing/memory-usage issue
//                     with 'QWRelayFeeder' processing of cache messages
//                     at startup.
//   9/8/2010 -- [ET]  Version 1.07:  Modified use of console-output file
//                     so a new one is created every 90 days (default) and
//                     files older than 3 years (default) are automatically
//                     deleted.
//  9/24/2010 -- [ET]  Version 1.08:  Improved QWRelayFeeder reconnect-
//                     after-disconnect response (via updated QWCommon).
//  10/6/2010 -- [ET]  Version 1.09:  Modified QWRelayFeeder to log
//                     connect-fail messages at debug level, and then
//                     at warning level after two minutes with errors
//                     have elapsed (via updated QWCommon).
//  11/4/2010 -- [ET]  Version 1.11:  Modified QWRelayFeeder to address
//                     issue where reconnect thread could get 'stuck'
//                     (via updated QWCommon); added checking and reporting
//                     of status-error messages and time of last data
//                     message from feeders; added reporting of time of
//                     last data message sent.
//  11/9/2010 -- [ET]  Version 1.12:  Modified to set system property
//                     'java.awt.headless' to true.
//  7/17/2013 -- [ET]  Version 1.127:  Added PDL feeder.
//   8/1/2013 -- [ET]  Version 1.128:  Modified PDL feeder to receive
//                     'internal-origin' products and support reliable
//                     listener settings.
//  8/14/2013 -- [ET]  Version 1.129:  Modified PDL feeder to squelch
//                     three types of PDL warnings (so they won't end
//                     up triggering spurious alerts in server status
//                     monitoring).
//  8/27/2013 -- [ET]  Version 1.13:  Release version.
//  9/23/2013 -- [ET]  Version 1.132:  Minor modifications to PDL-feeder
//                     module; added checks to feeder input so unusual
//                     feeder-data-source message numbers will result in
//                     warning-level log messages.
//  9/24/2013 -- [ET]  Version 1.14:  Modified product converter in PDL
//                     feeder so generated 'ProductLink' messages will
//                     have 'Version' and 'Action' elements inside the
//                     'ProductLink' element (instead of parent 'Event'
//                     element).
// 11/15/2013 -- [ET]  Version 1.141:  Modified checks to feeder input so
//                     only large changes in feeder-data-source message
//                     numbers will result in warning-level log messages.
//  4/26/2017 -- [KF]  Version 1.142:  Replaced QWPdlFeeder 'dyfiUrlPrefixStr'
//                     and 'dyfiUrlSuffixStr' with 'dyfiUrlFormatStr'.
//  1/30/2018 -- [KF]  Version 1.143:  Modified for Java 1.9 compatibility,
//                     Removed 1024 byte restriction for text products,
//                     Added 'CLIENT_FLAGS'.
//  3/13/2019 -- [KF]  Version 1.73:
//					   Moved PROGRAM_VERSION to QWCommon QWConstants.
//  3/13/2019 -- [KF]  Modified to use the PDL Indexer.
//

package com.isti.quakewatch.server;

import java.util.*;
import java.io.*;
import java.text.DateFormat;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.security.cert.X509CRL;
import org.jdom.Element;
import org.jdom.Attribute;
import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.util.LogOutputStream;
import com.isti.util.FileUtils;
import com.isti.util.FifoHashtable;
import com.isti.util.IstiXmlUtils;
import com.isti.util.ThreadLogger;
import com.isti.util.IstiNotifyThread;
import com.isti.util.AddrPortListMgr;
import com.isti.util.TwoObjectMatcher;
import com.isti.util.TagValueTable;
import com.isti.util.IstiEncryptionUtils;
import com.isti.util.IstiTimeObjectCache;
import com.isti.util.IstiTimeObjectCache.VectorWithCount;
import com.isti.util.IstiMessageObjectCache;
import com.isti.util.Averager;
import com.isti.util.DataChangedListener;
import com.isti.util.TagValueTrkTable;
import com.isti.openorbutil.OrbManager;
import com.isti.openorbutil.EvtChManager;
import com.isti.openorbutil.OrbUtilDeps;
import com.isti.quakewatch.common.MsgTag;
import com.isti.quakewatch.common.QWReportStrings;
import com.isti.quakewatch.common.QWConstants;
import com.isti.quakewatch.util.QWUtils;
import com.isti.quakewatch.util.DomainTypeInfoTable;
import com.isti.quakewatch.util.DomainTypeInfoRec;
import com.isti.quakewatch.util.EncryptDecryptUtil;
import com.isti.quakewatch.server.scp.ScpFileSender;
import com.isti.quakewatch.server.outputter.QWOutputterPlugin;

/**
 * Class QWServer implements the QuakeWatch server.
 */
public class QWServer implements QWConstants
{
    /** Name string for program. */
  public static final String PROGRAM_NAME = "QuakeWatch Server";
    /** Revision string for program. */
  public static final String REVISION_STRING = PROGRAM_NAME + ", Version " +
                                                            PROGRAM_VERSION;
    /** Reference string for QWAcceptor object on ("QWAcceptor"). */
  public static final String QWACCEPTOR_REFSTR = "QWAcceptor";
    /** Name of default server configuration file. */
  public static final String CONFIG_FNAME = "conf/QWServerConfig.xml";
    /** Name of parameter for specifying the server configuration file. */
  public static final String CFG_PARAM_SPEC = "configFile";
                        //name of optional thread-logging output file:
  private static final String THREAD_LOGGER_FNAME =
                                                  "log/QWServerThreads.log";
                        //name of optional memory-info-logging output file:
  private static final String MEMINFO_LOGGER_FNAME =
                                                  "log/QWServerMemInfo.log";
                        //default name of clients-info-logging output file:
  private static final String CLIENTSINFO_LOG_FNAME =
                                              "log/QWServerClientsInfo.log";
                        //remove-interval for old msgs from cache & archive:
  private static final int REM_OLDMSGS_INTERVALMS =
                                               (int)(UtilFns.MS_PER_MINUTE);
                        //interval for checking feeder-modules-config file:
  private static final int CHK_FDRSFILE_INTERVALMS = 10000;
                        //interval for checking memory and threads:
  private static final int CHK_MEMTHREADS_INTERVALMS = 601000;
                        //login username used by "obsolete-login" clients:
  private static final String OBSLOGIN_UNAME_STR = "qwuser2";
                        //remove-interval for tracking timed-out clients:
  private static final int REM_OLDCLIENTS_INTERVALMS = 30 *
                                               (int)(UtilFns.MS_PER_SECOND);
                        //checking window for 'requestSourcedMsgsStr()':
  private static final long SOURCED_REQMSGS_WINDOWMS =
                                                   8L * UtilFns.MS_PER_HOUR;
                        //zero-length byte array:
  public static final byte [] ZEROLEN_BYTE_ARRAY = new byte[0];
                        //runtime object for system:
  public static final Runtime systemRuntimeObj = Runtime.getRuntime();
                        //configuration manager for server:
  private final ServerConfigManager cfgObj;
  						//Java version in use:
  private final String javaVersionString;
  						//OS name:
  private final String osNameString;
                        //ID name for server:
  private final String serverIDNameStr;
                        //host address in use for server:
  private final String serverHostAddress;
                        //port number in use for server:
  private final int serverPortNumber;
                        //string with address, port and name of server:
  private final String serverAddrPortNameStr;
                        //ID, host address and port number for server:
  private final String serverIDNameLocStr;
                        //notification service port number:
  private final int notifSvcPortNum;
                        //type-name string for status messages:
  private final String statusMsgTypeNameStr;
                        //true if structured events are enabled:
  private final boolean useStructuredEventsFlag;
                        //table of msgNum/timeGen values for domain/types:
  private final DomainTypeInfoTable domainTypeInfoTableObj;
                        //alt servers host-address/port-number list mgr:
  private final AddrPortListMgr altServersListMgr = new AddrPortListMgr();
                        //redirect servers host-addr/port-number list mgr:
  private final AddrPortListMgr redirServersListMgr = new AddrPortListMgr();
  private OrbManager orbManagerObj = null;       //ORB manager object
                        //event channel manager object:
  private final EvtChManager evtChManagerObj;
                             //stream object for redirected console output:
  private PrintStream consoleRedirectStream = null;
  private PrintStream originalStdOutStream = null;    //original stdout
  private PrintStream originalStdErrStream = null;    //original stderr
  private LogFile logObj = null;       //handle to log file
  private LogFile messageLogFileObj = null;      //log file for event msgs
  private final long serverStartupTimeMS;        //time of server startup
                             //table of QW-feeder objects from main cfg:
  private final FifoHashtable configFeederModsTable;
                             //table of current QW-feeder objects:
  private final FifoHashtable currentFeederModsTable;
                             //feeder-modules-config-file manager:
  private final FdrModsCfgFileMgr fdrModsCfgFileMgrObj;
                             //list of QW-outputter objects:
  private final List outputtersListObj;
                             //handle for clients-info log file:
  private final LogFile clientsInfoLogFileObj;
                             //handle for clients-manager object:
  private final QWClientsManager clientsManagerObj;
                             //handle for alive message thread object:
  private AliveMessageThread aliveMessageThreadObj = null;
                             //housekeeping thread object:
  private final HousekeepingThread housekeepingThreadObj =
                                                   new HousekeepingThread();
                             //tracking table for feeder-data-src msgNums:
  private TagValueTrkTable fdrDataSrcHostMsgNumTableObj = null;
                             //thread-sync object for table:
  private final Object fdrDataSrcHstMsgNmTblSyncObj = new Object();
  private long lastMessageNumber = 0;       //message number for last msg
  private long lastTimeGeneratedVal = 0;    //time-gen value for last msg
  private long fdrSourceMessageNumber = 0;  //feeder-source message number
  private long reqMsgsTransactionCount = 0; //# of request-messages calls
  private long reqMsgsNumMessagesSent = 0;  //request-messages msg count
  private final Object reqMsgsCountSyncObj = new Object(); //thread sync
  private long lastReqMsgsTransactionCount = 0;  //count tracker
  private long lastReqMsgsNumMessagesSent = 0;   //numMsgs tracker
  private long lastServerStatsLogTime = 0;  //time of last statistics log
  private long lastDataMsgSentTimeVal = 0;  //time of last data msg output
                             //thread sync for 'lastDataMsgSentTimeVal:'
  private final Object lastDMsgSentTimeSyncObj = new Object();
                             //averager objs for reqest-messages counters:
  private final Averager reqMsgsTransCountAveragerObj = new Averager(10);
  private final Averager reqMsgsNumMsgsSentAveragerObj = new Averager(10);
                                       //thread sync obj for message adds:
  public final Object addMessageSyncObj = new Object();
  private ThreadLogger threadLoggerObj = null;   //handle for thread logger
                             //"IOR:" locator string for event channel:
  private String mainEvtChLocatorStr = null;
                             //max # of msgs returned in a resend request:
  private final int maximumResendNumMsgs;
                             //manager for cached archive of messages:
  private final QWCachedArchiveMgr cachedArchiveMgrObj;
  private final int maximumCacheAgeInSeconds;
              //date-formatter object for 'setupQWMessageElement()' method:
  private final DateFormat xmlDateFormatterObj =
                                           QWUtils.getXmlDateFormatterObj();
              //password manager object:
  private final ServerPasswordManager passwordManagerObj;
              //private key for signatures:
  private final PrivateKey privateKeyObj;
  private final byte [] certificateFileDataArray;
                             //ID string for 'QWAcceptorImpl' object:
  private String qwAcceptorImplIDString = UtilFns.EMPTY_STRING;
              //averager used with thread-count checking:
  private Averager threadCountAveragerObj = new Averager(10);
              //checker for notif-server status:
  private NotifSvrStatusChecker notifSvrStatusCheckerObj = null;
              //true if status-report queries from clients allowed:
  private final boolean stRptClientQueriesFlag;
              //status-report data for access via 'getStatusReportData()':
  private String stRptClientDataString = null;
              //status-rep timestamp for access via 'getStatusReportTime()':
  private long stRptClientDataTimeVal = 0;
              //thread-synchronization object for 'stRptClientDataString':
  private final Object stRptClientDataSyncObj = new Object();
              //flag set true if local file for status reports specified:
  private boolean stRptLocalFileOutFlag = false;
              //remote file for status reports path, or null if none:
  private String stRptRemotePathString = null;
              //handle for status-report output to local file:
  private LogFile stRptLocalFileOutObj = null;
              //minimum tracking log level for status report output:
  private int stRptTrkLogLevelValue = LogFile.WARNING;
              //title date formatter for status report:
  private DateFormat stRptTitleDateFormatObj = null;
              //last-data-message-time formatter:
  private DateFormat lastDataMsgTimeFormatObj = null;
              //remote-filename date formatter for status report:
  private DateFormat stRptFNameDateFormatObj = null;
              //remote host info object for status report:
  private ScpFileSender.ScpHostInfo stRptRemHostInfoObj = null;
              //flag cleared after check for status-report fatal error:
  private boolean stRptCheckFatalErrorFlag = true;
              //encrypt/decrypt util for status-report queries from clients:
  private final EncryptDecryptUtil encryptDecryptUtilObj;

         //setup dummy reference to CORBA classes needed to create
         // a stand-alone application 'jar' file using JBuilder:
  @SuppressWarnings("unused")
  private static final OrbUtilDeps dummyDepObj = null;

         // flag to make sure that we don't run our cleanup code twice:
  private boolean serverTerminatedFlag = false;
         //thread-synchronization object used by 'exitCleanup()':
  private final Object exitCleanupSyncObject = new Object();

         //handle to base directory string for server I/O files:
  private static String serverBaseDirectoryStr = null;

         //handle to instance created by 'staticInvokeStartupServer()':
  private static QWServer staticInvokeQWServerObj = null;
         //flag set true after first 'staticInvokeStartupServer()':
  private static boolean staticInvokeStartedFlag = false;
         //flag set true after "staticInvoke" server object ready:
  private static boolean staticInvokeReadyFlag = false;
         //thread synchronization object for "staticInvoke" methods:
  private static final Object staticInvokeSyncObj = new Object();


  /**
   * Class QWServer implements the QuakeWatch server program.
   * @param javaVersionString the java version string.
   * @param programArgs the array of command-line arguments for
   * the program, or null for none.
   * @throws StartupFailException if the server fails to start up.
   */
  public QWServer(String javaVersionString, String [] programArgs) throws StartupFailException
  {
	this.javaVersionString = javaVersionString;
	this.osNameString = UtilFns.getOsName();
    System.out.println(REVISION_STRING);

         //setup shutdown hook:
    Runtime.getRuntime().addShutdownHook(new CleanupThread());

         //check for "configFile" parameter:
    String cfgFNameStr = CONFIG_FNAME;
    final String arg0Str;
    if(programArgs != null && programArgs.length > 0 &&
                                           (arg0Str=programArgs[0]) != null)
    {    //at least one command-line parameter available
      final int arg0Len = arg0Str.length();      //length of first parameter
      final int specLen = CFG_PARAM_SPEC.length();    //length of param spec
      int p = 0;        //skip past up to 2 leading dash characters:
      while(p < arg0Len && arg0Str.charAt(p) == '-' && ++p < 2);
      if(arg0Len >= p+specLen)
      {  //parameter is long enough for match
              //if equals char found then trim param; else use full length:
        final int sepPos = (arg0Len > p+specLen+1 &&
                    arg0Str.charAt(p+specLen) == '=') ? p+specLen : arg0Len;
        if(arg0Str.substring(p,sepPos).equals(CFG_PARAM_SPEC))
        {     //parameter spec matched
          final int nameSlotVal;
          if(sepPos >= arg0Len)
          {   //equals char was not found; use next parameter for filename
            if(programArgs.length > 1 && programArgs[1] != null)
            {      //valid second parameter is available
              cfgFNameStr = programArgs[1];      //fetch filename parameter
              nameSlotVal = 1;         //indicate second parameter used
            }
            else
              nameSlotVal = 0;         //indicate only first parameter used
          }
          else
          {   //equals char was found; use rest of string for filename
            cfgFNameStr = arg0Str.substring(sepPos+1);
            nameSlotVal = 0;           //indicate only first parameter used
          }
          final int len;     //check if name surrounded by quotes:
          if((len=cfgFNameStr.length()) > 1 &&
                                          ((cfgFNameStr.charAt(0) == '\"' &&
                                       cfgFNameStr.charAt(len-1) == '\"') ||
                                           (cfgFNameStr.charAt(0) == '\'' &&
                                        cfgFNameStr.charAt(len-1) == '\'')))
          {   //name is surrounded by quotes; remove them
            cfgFNameStr = cfgFNameStr.substring(1,len-1);
          }
          if(programArgs.length > nameSlotVal+1)
          {   //more command-line parameters are available
                   //"shift down" the remaining command-line parameters:
            int newPos = 0;
            final String [] newArgsArr =    //new string array for params
                               new String[programArgs.length-nameSlotVal-1];
                        //for each remaining param, copy to new array:
            for(p=nameSlotVal+1; p<programArgs.length; ++p)
              newArgsArr[newPos++] = programArgs[p];
            programArgs = newArgsArr;       //enter new array of params
          }
          else     //no more command-line parameters are available
            programArgs = new String[0];    //setup empty array
        }
      }
    }
         //process XML-format configuration file:
    cfgObj = new ServerConfigManager(fromBaseDir(cfgFNameStr));
         //load "Settings" from configuration file:
    if(!cfgObj.loadSettings(programArgs))
    {    //error loading Settings; log and show error message
      logObj = LogFile.initGlobalLogObj(    //setup default log file
                    fromBaseDir(cfgObj.serverLogFileNameProp.stringValue()),
                                  LogFile.DEBUG,LogFile.WARNING,false,true);
      logObj.error(cfgObj.getErrorMessage());
      throw new StartupFailException();          //abort program
    }
         //set server ID name string:
    serverIDNameStr = cfgObj.serverIdNameProp.stringValue();
         //set flag true if structured event messages are enabled:
    useStructuredEventsFlag = cfgObj.useStructuredEventsFlagProp.booleanValue();
         //if structured events enabled then create table
         // of msgNum/timeGen values for domain/types:
    domainTypeInfoTableObj = useStructuredEventsFlag ?
                                           new DomainTypeInfoTable() : null;
         //interpret log-file message level string:
    final String logFileLevelStr = cfgObj.serverLogFileLevelProp.stringValue();
    Integer integerObj;     //convert level string to value:
    if((integerObj=LogFile.levelStringToValue(logFileLevelStr)) == null)
    {    //unable to match string; show error message
      System.err.println("Invalid value for \"" +
                  cfgObj.serverLogFileLevelProp.getName() + "\" setting:  \"" +
                                                    logFileLevelStr + "\"");
      System.err.println("  Available values:  " +    //show avail values
                                         LogFile.getLevelNamesDisplayStr());
      throw new StartupFailException();          //abort program
    }
    final int logFileLevelVal = integerObj.intValue();     //save value
         //interpret console message level string:
    final String consoleLevelStr = cfgObj.serverConsoleLevelProp.stringValue();
         //convert level string to value:
    if((integerObj=LogFile.levelStringToValue(consoleLevelStr)) == null)
    {    //unable to match string; show error message
      System.err.println("Invalid value for \"" +
                 cfgObj.serverConsoleLevelProp.getName() + "\" setting:  \"" +
                                                    consoleLevelStr + "\"");
      System.err.println("  Available values:  " +    //show avail values
                                         LogFile.getLevelNamesDisplayStr());
      throw new StartupFailException();          //abort program
    }
    final int consoleLevelVal = integerObj.intValue();     //save value

         //setup console output redirect for file (if filename given):
                                  //get console output file name
    String consoleFileName = cfgObj.consoleRedirectFileNameProp.stringValue();
    if(consoleFileName != null && consoleFileName.length() > 0)
    {    //console output redirect filename was given
              //create any subdirectories needed for filename:
      FileUtils.createParentDirs(fromBaseDir(consoleFileName));
      try
      {       //setup console output file:
//        consoleRedirectStream = new PrintStream(new BufferedOutputStream(
//                             new TimestampOutputStream(new FileOutputStream(
//                                 fromBaseDir(consoleFileName),true))),true);
                   //use LogFile mechanism for console-output file so
                   // filenames have dates and old files are deleted
                   // (make sure all "console" output suppressed):
        final LogFile conLogObj = new LogFile(fromBaseDir(consoleFileName),
                               LogFile.ALL_MSGS,LogFile.NO_MSGS,false,true);
                        //enter switch-interval value from configuration:
        conLogObj.setLogFileSwitchIntervalDays(
                             cfgObj.consoleFilesSwitchIntvlDays.intValue());
                        //enter maximum-age value from configuration:
        conLogObj.setMaxLogFileAge(
                                cfgObj.consoleFilesMaxAgeInDays.intValue());
                        //get and enter output stream for LogFile:
        consoleRedirectStream =
                            new PrintStream(conLogObj.getLogOutputStream());
      }
      catch(Exception ex)
      {       //error opening file; show message
        System.err.println("Server:  Error opening console redirect " +
            "output file (\"" + consoleFileName + "\") for output:  " + ex);
        consoleRedirectStream = null;       //make sure handle is null
      }
      if(consoleRedirectStream != null)
      {       //output file opened OK
        consoleRedirectStream.println();    //start file with blank line
                                            //put in header message:
        consoleRedirectStream.println("Console output redirect file for " +
                                                 PROGRAM_NAME + " opened " +
                                              UtilFns.timeMillisToString());
        if(!consoleRedirectStream.checkError())       //check for errors
        {     //no stream errors detected
          originalStdOutStream = System.out;     //save original stdout
          originalStdErrStream = System.err;     //save original stderr
          try
          {        //attempt to redirect console output streams to file:
            System.setOut(consoleRedirectStream);
            System.setErr(consoleRedirectStream);
          }
          catch(Exception ex)
          {        //error redirecting output streams; show message
            System.err.println("Server:  Error redirecting console " +
                    "output to file (\"" + consoleFileName + "\"):  " + ex);
            consoleRedirectStream.close();       //close output stream
            consoleRedirectStream = null;        //make sure handle is null
          }
        }
        else
        {     //error writing to file; show message
          System.err.println("Server:  Error writing to redirected " +
                     "console output file (\"" + consoleFileName + "\")");
          consoleRedirectStream.close();      //close output stream
          consoleRedirectStream = null;       //clear handle
        }
      }
    }

         //initialize global log file, use local TZ and add date to fnames:
    logObj = LogFile.initGlobalLogObj(
                    fromBaseDir(cfgObj.serverLogFileNameProp.stringValue()),
                                logFileLevelVal,consoleLevelVal,false,true);
         //log program revision string:
    logObj.info("Server revision:  " + REVISION_STRING);
    final int logFilesMaxAgeInDays =
                                 cfgObj.logFilesMaxAgeInDaysProp.intValue();
    if(logFilesMaxAgeInDays > 0)       //if given then enter max-age value
      logObj.setMaxLogFileAge(logFilesMaxAgeInDays);
    logObj.debug("  logFileLevel=" +   //show log and console output levels
          LogFile.getLevelString(logFileLevelVal) + " (" + logFileLevelVal +
              "), consoleLevel=" + LogFile.getLevelString(consoleLevelVal) +
            " (" + consoleLevelVal + "), maxAge = " + logFilesMaxAgeInDays +
                                                                   " days");
         //log server ID name from configuration file:
    logObj.info("Server ID name:  " + serverIDNameStr);
         //log OpenORB version (if CORBA in use):
    notifSvcPortNum = cfgObj.notifSvcPortNumProp.intValue();
    if(cfgObj.qwServicesAvailFlagProp.booleanValue() || notifSvcPortNum > 0)
    {    //QWServices enabled or Notification-Service event channel in use
      logObj.info(QWReportStrings.REP_ORBVERSION_STR +
                                         OrbManager.getOpenOrbVersionStr());
    }
         //log Java version:
    logObj.info(QWReportStrings.REP_JAVAVERSION_STR + javaVersionString);
         //log OS name:
    logObj.info(QWReportStrings.REP_OSNAME_STR + osNameString);

         //create hostAddr/portNum manager using alt-servers parameter:
    if(!altServersListMgr.addEntriesListStr(
                                 cfgObj.altServerIdsListProp.stringValue()))
    {
      logObj.error("Error in \"" + cfgObj.altServerIdsListProp.getName() +
               "\" setting:  " + altServersListMgr.getErrorMessageString());
      throw new StartupFailException();          //abort program
    }
         //create hostAddr/portNum manager using redir-servers parameter:
    if(!redirServersListMgr.addEntriesListStr(
                              cfgObj.redirectServersListProp.stringValue()))
    {
      logObj.error("Error in \"" + cfgObj.redirectServersListProp.getName()
           + "\" setting:  " + redirServersListMgr.getErrorMessageString());
      throw new StartupFailException();          //abort program
    }

    //generate the private key
    final String privKeyFile = cfgObj.privKeyFileProp.stringValue();
    if (privKeyFile != null && privKeyFile.length() > 0)
    {
      privateKeyObj = IstiEncryptionUtils.generatePrivateKey(
                                                  fromBaseDir(privKeyFile));
      if (privateKeyObj != null)
        logObj.debug("QWServer:  Private key file generated");
    }
    else
    {
      privateKeyObj = null;
      logObj.debug("QWServer:  No private key file name specified");
    }

         //fetch certificate file name:
    final String certFNameStr = cfgObj.certFileProp.stringValue();
    byte [] certFileByteArr = ZEROLEN_BYTE_ARRAY;
    if (certFNameStr != null && certFNameStr.length() > 0)
    {    //certificate file name was specified
      try
      {            //load data from certificate file:
        certFileByteArr =
                      FileUtils.readFileToBuffer(fromBaseDir(certFNameStr));
        logObj.debug("QWServer:  Successfully loaded certificate file \"" +
                                          fromBaseDir(certFNameStr) + "\"");
      }
      catch(FileNotFoundException ex)
      {
        logObj.warning("Certificate file (\"" +
                               fromBaseDir(certFNameStr) + "\") not found");
      }
      catch(Exception ex)
      {
        logObj.warning("Error reading certificate file (\"" +
                                 fromBaseDir(certFNameStr) + "\"):  " + ex);
      }
      final X509Certificate certificate =
          IstiEncryptionUtils.generateX509Certificate(
          certFileByteArr);
      final X509Certificate certificateOfAuthority =
          IstiEncryptionUtils.generateX509Certificate(
          fromBaseDir(cfgObj.coaFileProp.stringValue()));
      final X509CRL crl = IstiEncryptionUtils.generateX509CRL(
          fromBaseDir(cfgObj.crlFileProp.stringValue()));
      if (IstiEncryptionUtils.verifyX509Information(
          certificate, certificateOfAuthority, crl))
      {
        logObj.info("Message-authentication information verified");
      }
      else
      {
        logObj.warning("Message-authentication information is not valid");
      }
    }
    else
      logObj.debug("QWServer:  No certificate file name specified");
    certificateFileDataArray = certFileByteArr;
         //log maximum message size value:
    logObj.info(QWReportStrings.REP_MAXMSGSIZE_STR +
                                  cfgObj.maximumMessageSizeProp.intValue());

    ServerPasswordManager pManObj;
    try
    {    //create the password manager:
      pManObj = new ServerPasswordManager(cfgObj, logObj);
    }
    catch(Exception ex)
    {    //error creating password manager; show error message
      passwordManagerObj = null;
      logObj.error("Error creating password manager:  " + ex);
      logObj.error(UtilFns.getStackTraceString(ex));
      terminateProgram(7);        //exit program
      throw new StartupFailException();     //exception to satisfy compiler
    }
    passwordManagerObj = pManObj;

              //setup maximum # of messages returned in a resend request:
    maximumResendNumMsgs = cfgObj.maxResendNumMsgsProp.intValue();

         //setup optional log file for event messages sent to clients:
    String str;
    if((str=cfgObj.messageLogFileNameProp.stringValue()) != null &&
                                                           str.length() > 0)
    {    //message log file parameter given
              //open as a log file (with no output to console):
      messageLogFileObj = new LogFile(
              fromBaseDir(str),LogFile.ALL_MSGS,LogFile.NO_MSGS,false,true);
              //set switch interval for message log files:
      messageLogFileObj.setLogFileSwitchIntervalDays(
                               cfgObj.messageLogSwitchDaysProp.intValue());
      if(logFilesMaxAgeInDays > 0)     //if given then enter max-age value
        messageLogFileObj.setMaxLogFileAge(logFilesMaxAgeInDays);
      logObj.info("Opened log file for event messages (\"" + str + "\")");
    }

         //create manager for cached archive of messages:
    final String archiveDirNameStr =
               fromBaseDir(cfgObj.archiveDirNameStrProp.stringValue(),true);
    final String archiveFileNameStr =
                                cfgObj.archiveFileNameStrProp.stringValue();
    final long toleranceMS = cfgObj.cacheTimeToleranceProp.longValue();
    maximumCacheAgeInSeconds =
                          (int)(cfgObj.maxCacheAgeInDaysProp.doubleValue() *
                                                   UtilFns.SECONDS_PER_DAY);
    final int cacheMaxObjectCount =
                                  cfgObj.cacheMaxObjectCountProp.intValue();
    final long cacheMaxTotalDataSize =
                                cfgObj.cacheMaxTotalDataSizeProp.intValue();
    final int archiveRemoveAgeSecs =
                        (int)(cfgObj.maxArchiveAgeInDaysProp.doubleValue() *
                                                   UtilFns.SECONDS_PER_DAY);
    final int maxRequestAgeSecs =
                         (int)(cfgObj.maxResendAgeInDaysProp.doubleValue() *
                                                   UtilFns.SECONDS_PER_DAY);
    final boolean allowDuplicatesFlag =
                              cfgObj.allowDuplicatesFlagProp.booleanValue();
    final boolean checkDupFdrSourceFlag =
                            cfgObj.checkDupFdrSourceFlagProp.booleanValue();
    final String msgKeyElement1Name =
                                cfgObj.msgKeyElement1NameProp.stringValue();
    final String msgKeyElement2Name =
                                cfgObj.msgKeyElement2NameProp.stringValue();
    QWCachedArchiveMgr caMgrObj;
    try
    {
      caMgrObj = new QWCachedArchiveMgr(archiveDirNameStr,
             archiveFileNameStr,logObj,toleranceMS,maximumCacheAgeInSeconds,
             cacheMaxObjectCount,cacheMaxTotalDataSize,archiveRemoveAgeSecs,
                maxRequestAgeSecs,allowDuplicatesFlag,checkDupFdrSourceFlag,
              msgKeyElement1Name,msgKeyElement2Name,domainTypeInfoTableObj);
    }
    catch(Throwable ex)
    {
      cachedArchiveMgrObj = null;
      logObj.error("Error creating cached archive:  " + ex);
      logObj.error(UtilFns.getStackTraceString(ex));
      terminateProgram(1);        //exit program
      throw new StartupFailException();     //exception to satisfy compiler
    }
    cachedArchiveMgrObj = caMgrObj;
    logObj.debug("Created cached archive object, dirName=\"" +
                archiveDirNameStr + "\", baseName=\"" + archiveFileNameStr +
                         "\", toleranceMS=" + toleranceMS + ", cacheSecs=" +
                             maximumCacheAgeInSeconds + ", cacheMaxCount=" +
           cacheMaxObjectCount + ", cacheMaxData=" + cacheMaxTotalDataSize +
                 ", archiveSecs=" + archiveRemoveAgeSecs + ", maxReqSecs=" +
                  maxRequestAgeSecs + ", allowDups=" + allowDuplicatesFlag +
                                 ", chkDupFdrSrc=" + checkDupFdrSourceFlag);

         //load QW feeder settings from main configuration file:
    if((configFeederModsTable=cfgObj.getQWFeederObjects(
                                             logFilesMaxAgeInDays)) == null)
    {    //error loading feeders; log error message
      final String errStr = cfgObj.getErrorMessage();
      logObj.error(errStr);
      if(errStr.indexOf("NoClassDefFoundError") >= 0 &&
                  errStr.indexOf("gov/usgs/earthquake/distribution") >= 0 &&
                                  !(new File("ProductClient.jar")).exists())
      {  //error is probably because "ProductClient.jar" is missing
        logObj.error("For PDL feeder, file \"ProductClient.jar\" needs " +
                                    "to be available in program directory");
      }
      terminateProgram(2);        //exit program
    }
         //create table of "current" feeders; starting with
         // feeders from main configuration file:
    currentFeederModsTable = (FifoHashtable)(configFeederModsTable.clone());
         //check feeder-modules-configuration file cfgProp entry:
    String fdrCfgFileNameStr;
    if((fdrCfgFileNameStr=
         cfgObj.feederModulesCfgFileProp.stringValue().trim()).length() > 0)
    {    //feeder-modules-configuration file cfgProp entry contains data
                   //make pathname relative to server-base directory:
      fdrCfgFileNameStr = fromBaseDir(fdrCfgFileNameStr);
                   //create feeder-modules-configuration-file manager:
      fdrModsCfgFileMgrObj = new FdrModsCfgFileMgr(
                      fdrCfgFileNameStr,cfgObj,logFilesMaxAgeInDays,logObj);
      final FifoHashtable tableObj;
      if((tableObj=fdrModsCfgFileMgrObj.checkReadQWFeederObjs()) != null)
      {  //table of feeder specifications read from feeder-mods cfg file
        if(tableObj.size() > 0)
        {     //feeder specifications found in feeder-mods cfg file
                   //check for and remove duplicate entries:
          fdrModsCfgFileMgrObj.checkFdrModsTableDups(configFeederModsTable,
                                                tableObj,fdrCfgFileNameStr);
                   //add entries from fdr-mods cfg file to "current" table:
          currentFeederModsTable.putAll(tableObj);
        }
        else
        {     //no feeder specifications found in feeder-mods cfg file
          logObj.info("No entries found in feeder-modules-configuration " +
                                      "file \"" + fdrCfgFileNameStr + "\"");
        }
      }
    }
    else      //feeder-modules-configuration file cfgProp entry is empty
      fdrModsCfgFileMgrObj = null;

    if(currentFeederModsTable.size() <= 0)  //log warning message if no feeders
      logObj.warning("No feeders specified for " + PROGRAM_NAME);

         //load QW outputter settings from configuration file:
    final List listObj;
    if((listObj=cfgObj.getQWOutputterObjects(logFilesMaxAgeInDays)) == null)
    {    //error loading outputters; log error message
      logObj.error(cfgObj.getErrorMessage());
      terminateProgram(3);        //exit program
    }
    outputtersListObj = (listObj.size() > 0) ? listObj : null;

    String hostAddrVal;
    if(cfgObj.qwServicesAvailFlagProp.booleanValue() || notifSvcPortNum > 0)
    {    //QWServices enabled or Notification-Service event channel in use
              //save configured port number:
      serverPortNumber = cfgObj.serverPortNumProp.intValue();
              //if ORB config file given then log it:
      final String orbConfigFileStr = cfgObj.orbConfigFileProp.stringValue();
      if(orbConfigFileStr != null && orbConfigFileStr.trim().length() > 0)
        logObj.debug("Using ORB configuration file:  " + orbConfigFileStr);
                                       //get host address string property:
      hostAddrVal = cfgObj.serverHostAddressProp.stringValue();
              //create ORB manager object:
      orbManagerObj = new OrbManager(programArgs,hostAddrVal,
         cfgObj.serverPortNumProp.intValue(),fromBaseDir(orbConfigFileStr));
              //configure use of numeric IPs instead of host names:
      orbManagerObj.enterPublishNumericIPFlag(
                            cfgObj.publishNumericIPFlagProp.booleanValue());
              //initialize ORB (with 'Corbaloc' service support):
      if(!orbManagerObj.initImplementation(true))
      {       //error initializing; log error message
        logObj.error(orbManagerObj.getErrorMessage());
        terminateProgram(4);
        throw new StartupFailException();   //exception to satisfy compiler
      }
      if((str=orbManagerObj.getEnteredHostAddrStr()) != null)
      {       //host address was entered into ORB
        hostAddrVal = str;        //save entered address and log it
        logObj.debug("Using local host address:  " + str);
      }
      final int val;
              //if port number entered into ORB then log it:
      if((val=orbManagerObj.getOrbPortNum()) > 0)
        logObj.debug("Using local port number:  " + val);
      logObj.debug("Parameter 'publishNumericIPFlag' = " +
                            cfgObj.publishNumericIPFlagProp.booleanValue());
      logObj.debug2("Successfully initialized CORBA ORB");
      logObj.debug("Using " + (useStructuredEventsFlag ?
                          "structured" : "'Any'-type") + " event messages");
    }
    else
    {    //QWServices not enabled and Notif-Service event channel not in use
                                  //get host address string property:
      hostAddrVal = cfgObj.serverHostAddressProp.stringValue();
      serverPortNumber = 0;       //no port number for QWServices
    }
         //make sure server-host-address variable contains an address:
    if(hostAddrVal == null || hostAddrVal.length() <= 0)
      hostAddrVal = UtilFns.getLocalHostIP();
    serverHostAddress = hostAddrVal;
              //setup port-number string (if port number in use):
    final String colonPortNumStr = (serverPortNumber > 0) ?
                            (":" + serverPortNumber) : UtilFns.EMPTY_STRING;
              //setup string with ID, host address and port number:
    serverIDNameLocStr = '\"' + serverIDNameStr + "\" @ " +
                                        serverHostAddress + colonPortNumStr;
              //setup type name used on status messages:
    statusMsgTypeNameStr = (serverIDNameStr + " " +
                                                  serverHostAddress).trim();
              //create server-ID check string for evt-chan locator file:
    serverAddrPortNameStr = (serverHostAddress + ';' +
                           serverPortNumber + ';' + serverIDNameStr).trim();
    logObj.debug("QWServer operating at " +      //log address/port location
                                       serverHostAddress + colonPortNumStr);
    if(notifSvcPortNum > 0)
    {    //notif-service port greater than zero
      if((str=cfgObj.notifSvcHostAddressProp.stringValue()) != null &&
                                                   str.trim().length() > 0)
      {  //'notifSvcHostAddress' config parameter was specified; log value
        logObj.debug("Notification service host address:  " + str);
      }
                             //log notification service port number:
      logObj.debug("Notification service port number:  " + notifSvcPortNum);
              //create event channel manager:
      evtChManagerObj = new EvtChManager(orbManagerObj,logObj,
                                                   useStructuredEventsFlag);
      String mainEvtChLocFileStr;
      boolean openFlag, connectedViaFileFlag = false;
                   //save max # of secs to wait for connection (0=infinite):
      final int connWaitRetrySecs = cfgObj.connWaitRetrySecsProp.intValue();
                   //mark start time before connection attempts:
      final long startTimeVal = System.currentTimeMillis();
      int count = 0;
      while(true)
      {  //loop while trying to connect to event ch or notification service:
        openFlag = false;
        if((mainEvtChLocFileStr=
             cfgObj.mainEvtChLocFileProp.stringValue().trim()).length() > 0)
        {     //loc file name given; attempt to open existing chan via loc
          String locStr;
          try
          {        //read CORBA-event-channel locator string from file:
            locStr = FileUtils.readMultiOpenFileToString(
                                   fromBaseDir(mainEvtChLocFileStr)).trim();
          }
          catch(Exception ex)
          {             //error opening file
            locStr = null;        //indicate no locator string
          }
          if(locStr != null)
          {   //locator string read from file OK
            str = null;      //init handle in case used in error message
            final int p;     //check if server-ID matches:
            if((p=locStr.indexOf('\n')) > 0 && p < locStr.length()-1 &&
                                               serverAddrPortNameStr.equals(
                                          str=locStr.substring(p+1).trim()))
            {      //server-ID matches; open channel using IOR part of str
              if((openFlag=evtChManagerObj.openViaLocatorString(
                                         locStr.substring(0,p-1).trim())) &&
                                          evtChManagerObj.connectSupplier())
              {    //connected supplier to existing channel OK
                connectedViaFileFlag = true;     //ind connect via loc file
                logObj.debug("Connected to existing event channel via " +
                          "locator file (\"" + mainEvtChLocFileStr + "\")");
                break;      //exit retry loop
              }
              logObj.debug2("Unable to connect to existing event channel " +
                      "via locator file (\"" + mainEvtChLocFileStr + "\")");
            }
            else
            {      //server ID does not match; log debug message
              logObj.debug("Server ID (" +
                            ((str != null) ? ("\"" + str + "\"") : "none") +
                                    ") from event channel locator file \"" +
                                    mainEvtChLocFileStr + "\" not matched");
            }
          }
        }
              //unable to join existing event channel
        if(openFlag)                        //if open succeeded then
          evtChManagerObj.closeChannel();   //close connection to bad channel
        evtChManagerObj.clearErrorMessage();     //clear connect-attempt err
                   //create event channel and supplier:
        if((openFlag=evtChManagerObj.createChViaNotifService(
                               cfgObj.notifSvcHostAddressProp.stringValue(),
                               cfgObj.notifSvcPortNumProp.stringValue())) &&
                                          evtChManagerObj.connectSupplier())
        {     //created event channel and connected supplier OK
          logObj.debug("Created and connected to new event channel");
          break;             //exit retry loop
        }
        if(openFlag)                      //if open succeeded then
          evtChManagerObj.closeChannel(); //close connection to bad channel
              //error creating event channel or supplier
        if(connWaitRetrySecs > 0 &&
                 (int)((System.currentTimeMillis() - startTimeVal) / 1000) >
                                                          connWaitRetrySecs)
        {     //too much time elapsed
          logObj.error(evtChManagerObj.getErrorMessage());
          logObj.error("Unable to connect to event channel or " +
                                "Notification Service within time limit (" +
                                              connWaitRetrySecs + " secs)");
          terminateProgram(5);
          throw new StartupFailException();  //exception to satisfy compiler
        }
        logObj.info(evtChManagerObj.getErrorMessage());
        logObj.info("Waiting 1 second and retrying connection (" +
                                                           (++count) + ")");
        try { Thread.sleep(1000); }
        catch(InterruptedException ex) {}
      }

      if(!connectedViaFileFlag)
      {  //did not connect via locator file (new event channel was created)
                //write event channel locator string to file:
        if((mainEvtChLocatorStr=evtChManagerObj.getLocatorString()) != null)
        {       //locator string OK
          try
          {             //create file containing IOR string and server ID:
            FileUtils.writeStringToFile(fromBaseDir(mainEvtChLocFileStr),
                             (mainEvtChLocatorStr.trim() + UtilFns.newline +
                                                    serverAddrPortNameStr));
            logObj.info("Created main event channel locator file (\"" +
                                               mainEvtChLocFileStr + "\")");
          }
          catch(Exception ex)
          {             //error creating file; log error message
            logObj.warning("Error creating main event channel locator " +
                          "file (\"" + mainEvtChLocFileStr + "\"):  " + ex);
          }
        }
        else
        {     //locator string not available
          logObj.warning("Unable to write event channel locator file \"" +
                  mainEvtChLocFileStr + "\"; locator string not available");
        }
      }
      else    //connected via locator file; save event-channel locator string
        mainEvtChLocatorStr = evtChManagerObj.getLocatorString();
    }
    else      //notif-service port not greater than zero
    {
      evtChManagerObj = null;     //setup for no notif-service event channel
      logObj.debug("Notification Service event channel not used ('" +
                            cfgObj.notifSvcPortNumProp.getName() + "'<=0)");
    }

         //preload cache data from archive:
    logObj.debug("Starting preload of cache from archive");
    cachedArchiveMgrObj.preloadCacheFromArchive();
    logObj.debug("Finished preload of cache from archive");
         //get last message number after preload:
    lastMessageNumber = cachedArchiveMgrObj.getLastEnteredMsgNum();
         //get timeGen for last message after preload:
    final QWServerMsgRecord lastRecObj;
    lastTimeGeneratedVal =
         ((lastRecObj=cachedArchiveMgrObj.getLastEnteredMsgObj()) != null) ?
                                          lastRecObj.getTimeGenerated() : 0;
         //if default-feeder-data-source host-name cfgProp contains data
         // then find the last feeder-data-source msg number that was used:
    final String defFdrHostStr;
    if((defFdrHostStr=cfgObj.defaultFdrSourceHostProp.stringValue()) != null
                                       && defFdrHostStr.trim().length() > 0)
    {    //default-feeder-data-source host-name cfgProp contains data
              //check if feeder-data-source host-name for last-received
              // message matches the default-feeder-data-source host-name:
      if(lastRecObj != null && lastRecObj.fdrSourceHostStr != null &&
                          lastRecObj.fdrSourceHostStr.equals(defFdrHostStr))
      {  //feeder-data-source host-name matches; get message number
        fdrSourceMessageNumber = lastRecObj.fdrSourceMsgNumVal;
      }
      else    //feeder-data-source host-name does not match
      {            //scan cache for last msg matching feeder-data-source
                   // host and fetch its feeder-data-source message number:
        fdrSourceMessageNumber =
          cachedArchiveMgrObj.fetchLastCachedFdrSourceMsgNum(defFdrHostStr);
      }
      if(fdrSourceMessageNumber > 0)
      {  //feeder-data-source message number was found; log it
        logObj.debug2("QWServer:  Fetched feeder-data-source message " +
                                      "number for last \"" + defFdrHostStr +
                         "\" message in cache:  " + fdrSourceMessageNumber);
      }
    }

         //if status report enabled then setup resources:
    final boolean cQueriesFlag =  //setup flag for client-queries cfgProp
                           cfgObj.stRptClientQueriesFlagProp.booleanValue();
    if(cfgObj.stRptItvlMinsProp.doubleValue() > 0.0 &&
             (cfgObj.stRptLocalFileProp.stringValue().trim().length() > 0 ||
              cfgObj.stRptRemoteDirProp.stringValue().trim().length() > 0 ||
                                                              cQueriesFlag))
    {  //status report interval > 0 and local or remote file
       // for status reports specified
      stRptClientQueriesFlag = cQueriesFlag;     //set instance flag
                   //customize error-msg prefix for log outputs:
      ScpFileSender.setLogWarningPrefixStr(
                          "Error sending status-report file via 'scp':  ");
      if(cfgObj.stRptLocalFileProp.stringValue().trim().length() > 0)
        stRptLocalFileOutFlag = true;  //if local file setup then set flag
      String pStr;
      if((pStr=cfgObj.stRptRemoteDirProp.stringValue().trim()).length() > 0)
      {  //remote status-report directory was specified
        if(pStr.charAt(pStr.length()-1) != '/')
          pStr += '/';       //if no trailing dir separator then append one
        String fStr;
        if((fStr=cfgObj.stRptRemoteFNameProp.stringValue().trim()).
                                                              length() <= 0)
        {  //remote status-report filename was not specified
                   //build filename from alphanumeric chars in server ID:
          if((fStr=UtilFns.removeNonAlphanumChars(serverIDNameStr)).length()
                                                                       <= 0)
          {  //no alphanumeric chars found in server ID
            fStr = "QWServer_StatRpt.txt";       //use default filename
          }
        }
        stRptRemotePathString = pStr + fStr;          //build pathname
      }
      if(cfgObj.stRptTrkLogCountProp.intValue() > 0)
      {    //max # of log msgs tracked > 0
        if((integerObj=LogFile.levelStringToValue(
                        cfgObj.stRptTrkLogLevelProp.stringValue())) != null)
        {     //min tracking log level string for status report output OK
          stRptTrkLogLevelValue = integerObj.intValue();   //save value
        }
        else
        {     //unable to match string; show error message
          logObj.warning("Invalid value for \"" +
                 cfgObj.stRptTrkLogLevelProp.getName() + "\" setting:  \"" +
                          cfgObj.stRptTrkLogLevelProp.stringValue() + "\"");
          logObj.warning("  Available values:  " +  //show avail values
                                         LogFile.getLevelNamesDisplayStr());
          logObj.warning("  Using default level (\"" +
              LogFile.getLevelString(stRptTrkLogLevelValue) + "\") for \"" +
                                 cfgObj.stRptTrkLogLevelProp.stringValue() +
                                                              "\" setting");
        }
        if(stRptTrkLogLevelValue <= LogFile.ERROR)
        {     //min tracking log level within range; setup log-msg tracking
          logObj.setupMessageTracking(stRptTrkLogLevelValue,
                                    cfgObj.stRptTrkLogCountProp.intValue());
          if(cfgObj.stRptOnLogFlagProp.booleanValue())
          {   //status report to be sent immediately on tracked log messages
            logObj.addTrackedMsgListener(     //add listener to trigger send
                              housekeepingThreadObj.stRptTrkLogMsgListener);
          }
        }
      }
      else    //not tracking log msgs; indicate not tracking
        stRptTrkLogLevelValue = LogFile.NO_MSGS;
                                  //setup encrypt/decrypt object for
      encryptDecryptUtilObj =     // status-report queries from clients
                  new EncryptDecryptUtil(serverIDNameStr,serverHostAddress);
    }
    else
    {  //not generating status reports
      stRptClientQueriesFlag = false;  //disable client stat-rpt queries
      stRptTrkLogLevelValue = LogFile.NO_MSGS;   //don't track log messages
      encryptDecryptUtilObj = null;    //no stat-rpt queries from clients
    }

         //save startup time for server:
    serverStartupTimeMS = System.currentTimeMillis();

              //if outputters list setup then start QW outputters:
    if(outputtersListObj != null)
      startOutputters();
         //start QW feeders specified in configuration file(s):
    startFeeders(currentFeederModsTable);

         //setup clients-info log file:
                   //if QWServices not setup & 'clientsInfoLogIntervalSecs'
                   // not specified in config then no clients-info log file:
    if(!cfgObj.qwServicesAvailFlagProp.booleanValue() &&
                     !cfgObj.clientsInfoLogIntervalSecsProp.getLoadedFlag())
    {                             //set zero for no clients-info log file:
      cfgObj.clientsInfoLogIntervalSecsProp.setValue(0);
    }
    if(cfgObj.clientsInfoLogIntervalSecsProp.intValue() > 0)
    {    //logging-interval value is greater than zero
      final int clientsInfoLogSwitchDays =  //get log file switch interval
                             cfgObj.clientsInfoLogSwitchDaysProp.intValue();
      final int clientsInfoLogMaxAgeDays =  //get log file max age
                             cfgObj.clientsInfoLogMaxAgeDaysProp.intValue();
      if(clientsInfoLogSwitchDays > 0 || clientsInfoLogMaxAgeDays > 0)
      {  //log file switch interval or max-age value given
                                  //setup to use date-code in name:
        clientsInfoLogFileObj = new LogFile(
                                         fromBaseDir(CLIENTSINFO_LOG_FNAME),
                               LogFile.ALL_MSGS,LogFile.NO_MSGS,false,true);
              //setup log file switch interval and max-age values:
        clientsInfoLogFileObj.setLogFileSwitchIntervalDays(
                                                  clientsInfoLogSwitchDays);
        clientsInfoLogFileObj.setMaxLogFileAge(clientsInfoLogMaxAgeDays);
      }
      else    //log file switch interval and max-age value not given
      {            //setup for single log file with no max age:
        clientsInfoLogFileObj = new LogFile(
                                         fromBaseDir(CLIENTSINFO_LOG_FNAME),
                                    LogFile.ALL_MSGS,LogFile.NO_MSGS,false);
      }
    }
    else      //logging-interval value is less than zero
      clientsInfoLogFileObj = null;         //no clients-info log file

          //create clients-manager object:
    QWClientsManager cMgrObj = null;
    try
    {
      cMgrObj = new QWClientsManager(
               fromBaseDir(cfgObj.clientVersionsInfoFileProp.stringValue()),
                             cfgObj.clientVInfoPollDelaySecsProp.intValue(),
                            cfgObj.clientTimeoutSecsProp.intValue(),logObj);
    }
    catch(Exception ex)
    {    //some kind of exception error; log it
      logObj.warning("Error creating clients manager:  " + ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
    clientsManagerObj = cMgrObj;

    if(cfgObj.qwServicesAvailFlagProp.booleanValue())
    {    //QWServices for clients is enabled
              //create QWAcceptor object and register it with Corbaloc:
      QWAcceptorImpl qwAcceptorObj = null;
      try
      {       //create QWAcceptor implementation object:
        qwAcceptorObj = new QWAcceptorImpl(this);
      }
      catch(Exception ex)
      {
        logObj.warning("Error creating QWAcceptor object:  " + ex);
      }
      if(qwAcceptorObj != null)
      {  //QWAcceptor implementation object created OK
              //setup ID string for QWAcceptor implementation object:
        qwAcceptorImplIDString = OrbManager.buildCorbalocString(
                      serverHostAddress,serverPortNumber,QWACCEPTOR_REFSTR);
        logObj.debug2("QWServer:  AcceptorImpl-ID string = \"" +
                                             qwAcceptorImplIDString + "\"");
                   //register QWAcceptor object with Corbaloc:
        if(orbManagerObj.registerCorbalocObject(
                                           QWACCEPTOR_REFSTR,qwAcceptorObj))
        {
          logObj.info(
                  "Registered 'QWAcceptor' object with 'Corbaloc' service");
        }
        else
        {
          logObj.warning("Error registering QWAcceptor object:  " +
                                           orbManagerObj.getErrorMessage());
        }
      }
    }
    else
    {    //QWServices for clients not enabled
      logObj.debug("CORBA QWServices not enabled ('" +
                    cfgObj.qwServicesAvailFlagProp.getName() + "'==false)");
    }

         //create and start thread for "Alive" messages:
    if(notifSvcPortNum > 0)
    {    //Notification-Service event channel is in use
      aliveMessageThreadObj = new AliveMessageThread(
                               cfgObj.aliveMsgDelaySecProp.intValue()*1000);
      aliveMessageThreadObj.start();
    }
         //start thread for "housekeeping":
    housekeepingThreadObj.start();

    if(cfgObj.threadLogIntervalMSecsProp.intValue() > 0)
    {    //non-zero thread logging interval given; create and start logger
      if(logFilesMaxAgeInDays > 0)
      {  //max-age value was given; enter it
        threadLoggerObj = new ThreadLogger(fromBaseDir(THREAD_LOGGER_FNAME),
                               cfgObj.threadLogIntervalMSecsProp.intValue(),
                                                      logFilesMaxAgeInDays);
      }
      else
      {  //no max-age value
        threadLoggerObj = new ThreadLogger(fromBaseDir(THREAD_LOGGER_FNAME),
                              cfgObj.threadLogIntervalMSecsProp.intValue());
      }
    }

    logObj.info("Server successfully started");

         //run CORBA implementation in a new thread:
    if(cfgObj.qwServicesAvailFlagProp.booleanValue())
    {    //QWServices for clients is enabled
      (new Thread("runOrbImplementation")
          {
            public void run()
            {
              if(!orbManagerObj.runImplementation())
              {      //error running implementation
                if(logObj != null)
                  logObj.error(orbManagerObj.getErrorMessage());
                terminateProgram(6);
                return;
              }
              if(logObj != null)
              {
                logObj.debug2(
                           "Returned from CORBA implementation run method");
              }
              if(!serverTerminatedFlag)        //if not already terminating
                terminateProgram(0);           // then terminate program
            }
          }).start();
    }
  }

  /**
   * Starts the feeders specified in the given table.
   * @param feedersTableObj table containing feeder objects.
   */
  private void startFeeders(FifoHashtable feedersTableObj)
  {
    Object obj;
    String nameStr;
    QWFeederPlugin feederPluginObj;
    LogFile fdrLogObj;
    final Iterator iterObj = feedersTableObj.getValuesVector().iterator();
    while(iterObj.hasNext())
    {    //for each "QWFeederPlugin" object in List
      if((obj=iterObj.next()) instanceof QWFeederPlugin)
      {  //'QWFeederPlugin' object fetched OK
        feederPluginObj = (QWFeederPlugin)obj;   //set handle to object
                   //create name string for feeder module:
        nameStr = feederPluginObj.getRevisionString() + " ('" +
                                     feederPluginObj.getFeederName() + "')";
                   //create call-back object and enter it:
        feederPluginObj.setCallbackObj(new QWServerCallback(
                    this,feederPluginObj,nameStr,logObj,messageLogFileObj));
              //if enabled then setup log-file tracking:
        if(stRptTrkLogLevelValue <= LogFile.ERROR &&
                        (fdrLogObj=feederPluginObj.getLogFileObj()) != null)
        {     //min tracking log level within range; setup log-msg tracking
          fdrLogObj.setupMessageTracking(stRptTrkLogLevelValue,
                                    cfgObj.stRptTrkLogCountProp.intValue());
          if(cfgObj.stRptOnLogFlagProp.booleanValue())
          {   //status report to be sent immediately on tracked log messages
            fdrLogObj.addTrackedMsgListener(  //add listener to trigger send
                              housekeepingThreadObj.stRptTrkLogMsgListener);
          }
        }
        logObj.info("Starting feeder:  " + nameStr);
                   //startup feeder:
        feederPluginObj.startFeeder();
      }
    }
  }

  /**
   * Stops the feeders specified in the given table.
   * @param feedersTableObj table containing feeder objects.
   */
  private void stopFeeders(FifoHashtable feedersTableObj)
  {
    Object obj;
    QWFeederPlugin feederPluginObj;
    Iterator iterObj = feedersTableObj.getValuesVector().iterator();
    while(iterObj.hasNext())
    {    //for each "QWFeederPlugin" object in List
      if((obj=iterObj.next()) instanceof QWFeederPlugin)
      {  //'QWFeederPlugin' object fetched OK
        feederPluginObj = (QWFeederPlugin)obj;  //set handle to object
        logObj.info("Requesting feeder stop:  " +
                               feederPluginObj.getRevisionString() + " ('" +
                                    feederPluginObj.getFeederName() + "')");
                   //stop feeder:
        feederPluginObj.stopFeeder();
      }
    }
    iterObj = feedersTableObj.getValuesVector().iterator();
    while(iterObj.hasNext())
    {   //for each "QWFeederPlugin" object in List
      if((obj=iterObj.next()) instanceof QWFeederPlugin)
      {      //'QWFeederPlugin' object fetched OK
        feederPluginObj = (QWFeederPlugin)obj;  //set handle to object
        int cnt = 0;
        while(!feederPluginObj.isFeederStopped())
        {    //loop while waiting for feeder stop to complete
          if(++cnt > 70)
          {  //too many loops; log timeout message and exit loop
            logObj.warning("Timeout waiting for feeder stop:  " +
                               feederPluginObj.getRevisionString() + " ('" +
                                    feederPluginObj.getFeederName() + "')");
            break;
          }
          try { Thread.sleep(100); }
          catch(InterruptedException ex) {}
        }
      }
    }
  }

  /**
   * Checks the feeder-modules-configuration file and starts/stops
   * feeders in response to changes.
   */
  private void checkFeederModsCfgFile()
  {
    final FifoHashtable fdrsTableObj;
    if((fdrsTableObj=fdrModsCfgFileMgrObj.checkReadQWFeederObjs()) != null)
    {    //table of feeder specifications read from feeder-mods cfg file
              //create new table of "current" feeders; starting
              // with feeders from main configuration file:
      final FifoHashtable newCurFeederModsTable =
                             (FifoHashtable)(configFeederModsTable.clone());
      if(fdrsTableObj.size() > 0)
      {  //feeder specifications found in feeder-mods cfg file
              //check for and remove duplicate entries:
        fdrModsCfgFileMgrObj.checkFdrModsTableDups(configFeederModsTable,
                     fdrsTableObj,fdrModsCfgFileMgrObj.getFileNameString());
              //add entries from fdr-mods cfg file to new "current" table:
        newCurFeederModsTable.putAll(fdrsTableObj);
      }
      else
      {  //no feeder specifications found in feeder-mods cfg file
        logObj.info("No entries found in feeder-modules-configuration " +
               "file \"" + fdrModsCfgFileMgrObj.getFileNameString() + "\"");
      }
         //determine which entries were removed from feeder-mods cfg file:
      final FifoHashtable stopFdrModsTable =
                             (FifoHashtable)(currentFeederModsTable.clone());
      stopFdrModsTable.removeAllKeys(newCurFeederModsTable.keySet());
      final int stopTableSize;
      if((stopTableSize=stopFdrModsTable.size()) > 0)
      {       //entries were removed from feeder-mods cfg file
        logObj.debug("Stopping feeders for entries (" + stopTableSize +
                        ") removed from feeder-modules-configuration file");
        stopFeeders(stopFdrModsTable);
      }
         //determine which entries were added from feeder-mods cfg file:
      final FifoHashtable startFdrModsTable =
                             (FifoHashtable)(newCurFeederModsTable.clone());
      startFdrModsTable.removeAllKeys(currentFeederModsTable.keySet());
      final int startTableSize;
      if((startTableSize=startFdrModsTable.size()) > 0)
      {       //entries were added from feeder-mods cfg file
        logObj.debug("Starting feeders for entries (" + startTableSize +
                          ") added from feeder-modules-configuration file");
        startFeeders(startFdrModsTable);
      }
      if(stopTableSize + startTableSize > 0)
      {  //feeders were removed or added; accept new "current" fdrs table
        synchronized(currentFeederModsTable)
        {     //grab thread lock for 'currentFeederModsTable'
          currentFeederModsTable.clear();
          currentFeederModsTable.putAll(newCurFeederModsTable);
        }
      }
    }
  }

  /**
   * Checks the status-error messages for the feeder modules and logs
   * them (if found).
   */
  private void checkFeedersStatusErrorMsgs()
  {
    if(currentFeederModsTable != null)
    {  //feeder table OK
      Object obj;
      String msgStr;
      QWFeederPlugin feederPluginObj;
      final Iterator iterObj =
                        currentFeederModsTable.getValuesVector().iterator();
      while(iterObj.hasNext())
      {  //for each "QWFeederPlugin" object in List
        if((obj=iterObj.next()) instanceof QWFeederPlugin)
        {  //'QWFeederPlugin' object fetched OK
          feederPluginObj = (QWFeederPlugin)obj;  //set handle to object
          if((msgStr=feederPluginObj.getStatusErrorMsg()) != null)
          {  //feeder is reporting status error message; log it
            logObj.warning("Status error reported by feeder \"" +
                         feederPluginObj.getFeederName() +"\":  " + msgStr);
          }
        }
      }
    }
  }

  /**
   * Starts the outputters specified in the configuration file
   */
  private void startOutputters()
  {
    final Iterator iterObj = outputtersListObj.iterator();
    Object obj;
    QWOutputterPlugin outptrPluginObj;
    LogFile fdrLogObj;
    while(iterObj.hasNext())
    {    //for each "QWOutputterPlugin" object in List
      if((obj=iterObj.next()) instanceof QWOutputterPlugin)
      {  //'QWOutputterPlugin' object fetched OK
        outptrPluginObj = (QWOutputterPlugin)obj;   //set handle to object
        outptrPluginObj.setQWServerObj(this);       //enter this QWServer obj
              //if enabled then setup log-file tracking:
        if(stRptTrkLogLevelValue <= LogFile.ERROR &&
                        (fdrLogObj=outptrPluginObj.getLogFileObj()) != null)
        {     //min tracking log level within range; setup log-msg tracking
          fdrLogObj.setupMessageTracking(stRptTrkLogLevelValue,
                                    cfgObj.stRptTrkLogCountProp.intValue());
          if(cfgObj.stRptOnLogFlagProp.booleanValue())
          {   //status report to be sent immediately on tracked log messages
            fdrLogObj.addTrackedMsgListener(  //add listener to trigger send
                              housekeepingThreadObj.stRptTrkLogMsgListener);
          }
        }
        logObj.info("Starting outputter:  " +
                               outptrPluginObj.getRevisionString() + " ('" +
                                 outptrPluginObj.getOutputterName() + "')");
                   //startup outputter:
        outptrPluginObj.startOutputter();
      }
    }
  }

  /**
   * Stops the outputters specified in the configuration file
   */
  private void stopOutputters()
  {
         //send "stop" requests to outputters:
    Iterator iterObj = outputtersListObj.iterator();
    Object obj;
    QWOutputterPlugin outptrPluginObj;
    while(iterObj.hasNext())
    {    //for each "QWOutputterPlugin" object in List
      if((obj=iterObj.next()) instanceof QWOutputterPlugin)
      {  //'QWOutputterPlugin' object fetched OK
        outptrPluginObj = (QWOutputterPlugin)obj;   //set handle to object
        logObj.info("Stopping outputter:  " +
                               outptrPluginObj.getRevisionString() + " ('" +
                                 outptrPluginObj.getOutputterName() + "')");
        outptrPluginObj.stopOutputter();    //stop outputter
      }
    }
         //wait for outputters to terminate (up to timeout):
    iterObj = outputtersListObj.iterator();
    while(iterObj.hasNext())
    {    //for each "QWOutputterPlugin" object in List
      if((obj=iterObj.next()) instanceof QWOutputterPlugin)
      {  //'QWOutputterPlugin' object fetched OK
        outptrPluginObj = (QWOutputterPlugin)obj;   //set handle to object
        int cnt = 0;
        while(!outptrPluginObj.isOutputterStopped())
        {    //loop while waiting for outputter stop to complete
          if(++cnt > 70)
          {  //too many loops; log timeout message and exit loop
            logObj.warning("Timeout waiting for outputter stop:  " +
                               outptrPluginObj.getRevisionString() + " ('" +
                                 outptrPluginObj.getOutputterName() + "')");
            break;
          }
          try { Thread.sleep(100); }
          catch(InterruptedException ex) {}
        }
      }
    }
  }

  /**
   * Delivers the given message to the outputters.
   * @param qwSvrMsgRecObj server-message-record object for message.
   */
  private void sendMsgToOutputters(QWServerMsgRecord qwSvrMsgRecObj)
  {
    final Iterator iterObj = outputtersListObj.iterator();
    Object obj;
    while(iterObj.hasNext())
    {    //for each "QWOutputterPlugin" object in List
      if((obj=iterObj.next()) instanceof QWOutputterPlugin)
      {  //'QWOutputterPlugin' object fetched OK
                   //send message to outputter:
        ((QWOutputterPlugin)obj).outputMessage(qwSvrMsgRecObj);
      }
    }
  }

  /**
   * Returns the total of the estimated thread counts for all of
   * the outputters.
   * @return The total of the estimated thread counts for all of
   * the outputters.
   */
  private int getOutputtersThreadCount()
  {
    final Iterator iterObj = outputtersListObj.iterator();
    int totalCount = 0;
    Object obj;
    while(iterObj.hasNext())
    {    //for each "QWOutputterPlugin" object in List
      if((obj=iterObj.next()) instanceof QWOutputterPlugin)
      {  //'QWOutputterPlugin' object fetched OK
                   //get estimated thread count; add to total:
        totalCount += ((QWOutputterPlugin)obj).getEstimatedThreadCount();
      }
    }
    return totalCount;
  }

  /**
   * Logs information about the outputters (if any).
   * @param indentStr indentation to use, or null for none.
   * @param logOutStm output stream into which information will be sent.
   * @param blankLineFlag true to generate blank line before information
   * (if any is generated).
   */
  private void logOutputtersInfo(String indentStr, OutputStream logOutStm,
                                                      boolean blankLineFlag)
  {
    if(outputtersListObj != null && outputtersListObj.size() > 0)
    {  //outputters list is not empty
      if(indentStr == null)
        indentStr = UtilFns.EMPTY_STRING;
                        //create print-stream access to output stream:
      @SuppressWarnings("resource")
      final PrintStream outStm =
               (logOutStm instanceof PrintStream) ? (PrintStream)logOutStm :
                       new PrintStream(new BufferedOutputStream(logOutStm));
      if(blankLineFlag)           //if flag then
        outStm.println();         //generate blank line
              //show count of number of outputters:
      outStm.println(indentStr + "Number of outputters = " +
                                                  outputtersListObj.size());
              //show status information from each outputter:
      final Iterator iterObj = outputtersListObj.iterator();
      Object obj;
      QWOutputterPlugin outptrPluginObj;
      String msgStr;
      while(iterObj.hasNext())
      {    //for each "QWOutputterPlugin" object in List
        if((obj=iterObj.next()) instanceof QWOutputterPlugin)
        {  //'QWOutputterPlugin' object fetched OK
          outptrPluginObj = (QWOutputterPlugin)obj;
          if((msgStr=outptrPluginObj.getStatusMessageStr()) != null &&
                                                 msgStr.trim().length() > 0)
          {  //outputter status message contains data
            msgStr = ":  " + msgStr;        //prepend separator string
          }
          else     //outputter status message empty
            msgStr = UtilFns.EMPTY_STRING;
                     //show outputter ID and status message:
          outStm.println(indentStr + "  " +
                               outptrPluginObj.getOutputterName() + msgStr);
        }
      }
      outStm.flush();             //flush output to file
    }
  }

  /**
   * Formats and sends a status message into the CORBA-event-channel.
   * @param msgTypeStr type string for message.
   * @param msgDataStr data string for message, or null for none.
   */
  public void sendStatusMessage(String msgTypeStr,String msgDataStr)
  {
         //get current time in milliseconds:
    final long timeVal = System.currentTimeMillis();
         //create StatusMessage element:
    final Element statusMsgObj = new Element(MsgTag.STATUS_MESSAGE);
         //set message type string:
    statusMsgObj.setAttribute(MsgTag.MSG_TYPE,msgTypeStr);
         //if given then set message data string:
    if(msgDataStr != null && msgDataStr.length() > 0)
      statusMsgObj.setAttribute(MsgTag.MSG_DATA,msgDataStr);
         //surround StatusMessage with QWMessage:
    final Element qwMsgObj =
                  (new Element(MsgTag.QW_MESSAGE)).addContent(statusMsgObj);
         //if using structured events then setup to use domain name
         // "StatusMessage" and type name "serverID/addr", otherwise none:
    final String domainNameStr = useStructuredEventsFlag ?
                                               MsgTag.STATUS_MESSAGE : null;
    final String typeNameStr = useStructuredEventsFlag ?
                                                statusMsgTypeNameStr : null;
    Vector fMsgNumVec = null;
    long lastMsgNumVal = 0;
    try
    {
         //if event domain and type names are in use then
         // build Vector of 'FilteredMsgNum' elements:
      Element fMsgNumElemObj;
      synchronized(addMessageSyncObj)
      {    //grab sync-lock for message adds
        lastMsgNumVal = getLastMsgNumber();
        if(domainTypeInfoTableObj != null)
        {  //domain/type msgNum/timeGen info table is available
          fMsgNumVec = new Vector();     //Vector for 'FilteredMsgNum' elems
          final Iterator iterObj = domainTypeInfoTableObj.values().iterator();
          Object obj;
          DomainTypeInfoRec recObj;
          long infoRecMsgNumVal;
          while(iterObj.hasNext())
          {     //for each domain/type element in table
            if((obj=iterObj.next()) instanceof DomainTypeInfoRec)
            {   //info-record object fetched OK
              recObj = (DomainTypeInfoRec)obj;
              if((infoRecMsgNumVal=recObj.getMessageNumberValue()) > 0)
              { //message-number value is OK
                     //setup 'FilteredMsgNum' element with 'DomainName',
                     // 'TypeName' and 'Value' attributes:
                fMsgNumElemObj = new Element(MsgTag.FILTERED_MSG_NUM);
                fMsgNumElemObj.setAttribute(MsgTag.DOMAIN_NAME,
                      ((recObj.domainNameStr != null) ? recObj.domainNameStr :
                                                       UtilFns.EMPTY_STRING));
                fMsgNumElemObj.setAttribute(MsgTag.TYPE_NAME,
                          ((recObj.typeNameStr != null) ? recObj.typeNameStr :
                                                       UtilFns.EMPTY_STRING));
                fMsgNumElemObj.setAttribute(MsgTag.VALUE,
                                             Long.toString(infoRecMsgNumVal));
                fMsgNumVec.add(fMsgNumElemObj);    //add element to Vector
              }
            }
            if(fMsgNumVec.size() <= 0)   //if no items added to Vector then
              fMsgNumVec = null;         //indicate no Vector
          }
        }
      }
    }
    catch(Exception ex)
    {         //some kind of exception error occurred; log it
      logObj.warning(PROGRAM_NAME + " error building list of " +
                                       "'FilteredMsgNum' elements:  " + ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
                             //add attributes to 'QWmessage':
    setupQWMessageElement(qwMsgObj,lastMsgNumVal,
                               timeVal,null,domainNameStr,typeNameStr,null);
              //if 'FilteredMsgNum' elements added to Vector then
              // add elements under 'StatusMessage' element:
    if(fMsgNumVec != null)
      statusMsgObj.setContent(fMsgNumVec);
    final String msgStr;
    try       //convert 'Element' object to string:
    {                    // (normalize string)
      msgStr = IstiXmlUtils.elementToString(qwMsgObj,true);
    }
    catch(Exception ex)
    {         //some kind of exception error occurred; log it and abort
      logObj.warning(PROGRAM_NAME + " error formatting XML output:  " + ex);
      return;
    }
                 //send message out to event channel:
    sendEventChannelMessage(domainNameStr,typeNameStr,null,msgStr);
  }

  /**
   * Formats and sends a status message into the CORBA-event-channel.
   * @param msgTypeStr type string for message.
   */
  public void sendStatusMessage(String msgTypeStr)
  {
    sendStatusMessage(msgTypeStr,null);
  }

  /**
   * Requests that a server-alive message be sent immediately.  This allows
   * a client to verify its connection to the event channel more quickly.
   */
  public void requestImmedAliveMsg()
  {
    logObj.debug2("Received server-alive message request from client");
    aliveMessageThreadObj.requestMessage();
  }

  /**
   * Requests that messages newer or equal to the specified time value or
   * later than the specified message number be returned.  If a non-zero
   * message number is given and the given time value matches then
   * messages with message numbers greater than the given message number
   * are returned; otherwise messages newer or equal to the specified time
   * value are returned (within a tolerance value).  If a list of domain
   * and type names is given then only messages whose event domain and
   * type names match the given list will be returned.  An XML message
   * containing the messages is built and returned, using the following
   * format: <pre>
   * [QWresend NumRequested="10"]
   *   [QWmessage ...] [/QWmessage]
   *   [QWmessage ...] [/QWmessage]
   *   ...
   * [/QWresend]
   * </pre> (Substitute angle brackets for square brackets.)
   * The number of messages that would be required to fill the request
   * is returned in the "NumRequested" attribute.  However, no more than
   * the oldest 'maxResendNumMsgs' number of messages will be returned
   * in any single call to this method (additional calls may be needed to
   * fetch all of the desired messages).
   * @param requestTimeVal the time-generated value for message associated
   * with the given message number, or the requested time value to be used
   * (milliseconds since 1/1/1970).
   * @param msgNum the message number to use, or 0 or none.
   * @param domainTypeListStr a list string of event domain and
   * type names in the format "domain:type,domain:type...", where
   * occurrences of the ':' and ',' characters not meant as
   * separators may be "quoted" by preceding them with the
   * backslash ('\') character and list items missing the ':'
   * character will be considered to specify only a domain name
   * (the type name will be an empty string); null or an empty string
   * for none.
   * @return An XML-formatted string containing the messages, or an empty
   * string if an error occurs.
   */
  public String requestMessagesStr(long requestTimeVal,long msgNum,
                                                   String domainTypeListStr)
  {
    try
    {
      logObj.debug4("QWServer.requestMessagesStr:  requestTimeVal=" +
          requestTimeVal + ", msgNum=" + msgNum + ", domainTypeListStr = " +
                                              ((domainTypeListStr != null) ?
                             ("\"" + domainTypeListStr + "\"") : "<null>"));
      synchronized(reqMsgsCountSyncObj)
      {  //only allow one thread at a time to access counters
        ++reqMsgsTransactionCount;               //inc transaction count
      }
              //convert list of event domain and type names (if given)
              // to a matcher object:
      final TwoObjectMatcher matcherObj =
             (domainTypeListStr != null && domainTypeListStr.length() > 0) ?
                      QWUtils.listStringToMatcher(domainTypeListStr) : null;
              //set flag if filtering by event domain and type names:
      final boolean filterFlag = (matcherObj != null);
      Element resendElem = null;
      int numMsgs, msgCount, numRequested = 0;
      while(true)
      {  //loop if all fetched messages are filtered out
              //get Vector of requested messages (up to 'maxResendNumMsgs'):
        final VectorWithCount vec = cachedArchiveMgrObj.getNewerMessages(
                                requestTimeVal,msgNum,maximumResendNumMsgs);
        if(resendElem == null)
        {     //QWresend element not yet created; create it now
          resendElem = new Element(MsgTag.QW_RESEND);
                        //get number of messages for request:
          numRequested = vec.getCount();
          resendElem.setAttribute(     //enter number of messages requested
                       MsgTag.NUM_REQUESTED,Integer.toString(numRequested));
        }
        if((numMsgs=vec.size()) > maximumResendNumMsgs)
        {     //too many messages in Vector (shouldn't happen)
          logObj.debug("requestMessagesStr() 'numMsgs' value reduced from " +
                                   numMsgs + " to " + maximumResendNumMsgs +
                                                     " (shouldn't happen)");
          numMsgs = maximumResendNumMsgs;   //limit # of messages returned
        }
        Object obj;
        QWServerMsgRecord recObj = null;
        msgCount = 0;
        Element elemObj;
        for(int i=0; i<numMsgs; ++i)
        {  //for each entry; fetch XML element from entry
          if((obj=vec.elementAt(i)) instanceof QWServerMsgRecord)
          {   //element object fetched OK
            recObj = (QWServerMsgRecord)obj;
            if(!filterFlag || matcherObj.contains(recObj.msgEvtDomainStr,
                                                      recObj.msgEvtTypeStr))
            {      //not filtering or domain/type names matched OK
              if((elemObj=recObj.getQWMsgElementObj()) != null)
              {    //element fetched OK; add as child to QWresend element:
                resendElem.addContent(elemObj);
                ++msgCount;      //increment message count
              }
              else
              {      //unable to fetch element; log error message
                logObj.warning("Error in 'requestMessagesStr()':  Bad entry " +
                           "in cache (getQWMsgElementObj failed), entry[" + i +
                                  "] = " + vec.elementAt(i) + ", obj = " + obj);
                return UtilFns.EMPTY_STRING;
              }
            }
          }
          else
          {     //unexpected object fetched; log error message
            logObj.warning("Error in 'requestMessagesStr()':  Bad entry " +
                            "in cache (not QWServerMsgRecord), entry[" + i +
                              "] = " + vec.elementAt(i) + ", obj = " + obj);
            return UtilFns.EMPTY_STRING;
          }
        }
        if(numMsgs <= 0 || msgCount > 0 || recObj == null)
          break;        //if no messages or not all skipped then exit loop
              //all messages from the fetch were filtered out; fetch
              // again using timeGen and msgNum from last fetched msg:
        requestTimeVal = recObj.getTimeGenerated();
        msgNum = recObj.getMsgNum();
        logObj.debug2("requestMessagesStr:  Fetching more messages (time=" +
                               requestTimeVal + ", msgNum=" + msgNum + ")");
      }
      synchronized(reqMsgsCountSyncObj)
      {  //only allow one thread at a time to access counters
        reqMsgsNumMessagesSent += msgCount;      //add to messages total
      }
              //if notification-service event channel in use then
              // use "Debug" log level; if not then use "Debug3"
              // (because clients will be constantly polling):
      logObj.println(((notifSvcPortNum>0)?LogFile.DEBUG:LogFile.DEBUG3),
                                   "Servicing request for messages (time=" +
              requestTimeVal + ", msgNum=" + msgNum + "); " + numRequested +
                                       " requested, " + msgCount + " sent");
              //convert QWresend element to string and return it:
              // ((trim and encode whitespace)
      return IstiXmlUtils.elementToFixedString(resendElem);
    }
    catch(Exception ex)
    {         //some kind of error occurred; log it
      logObj.warning("Error in 'requestMessagesStr()':  " + ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
      return UtilFns.EMPTY_STRING;     //return empty string to indicate err
    }
  }

  /**
   * Requests that messages corresponding to the given time value and
   * list of feeder-data-source host-name/message-number entries be
   * returned.  If a list of domain and type names is given then only
   * messages whose event domain and type names match the given list
   * will be returned.  An XML message containing the messages is built
   * and returned, using the following format: <pre>
   * [QWresend NumRequested="10"]
   *   [QWmessage ...] [/QWmessage]
   *   [QWmessage ...] [/QWmessage]
   *   ...
   * [/QWresend]
   * </pre> (Substitute angle brackets for square brackets.)
   * The number of messages that would be required to fill the request
   * is returned in the "NumRequested" attribute.  However, no more than
   * the oldest 'maxResendNumMsgs' number of messages will be returned
   * in any single call to this method (additional calls may be needed to
   * fetch all of the desired messages).
   * @param requestTimeVal the time-generated value for message associated
   * with the given message number, or the requested time value to be used
   * (milliseconds since 1/1/1970).
   * @param hostMsgNumListStr a list of feeder-data-source
   * host-name/message-number entries in the form:
   * "hostName"=msgNum,...
   * @param domainTypeListStr a list string of event domain and
   * type names in the format "domain:type,domain:type...", where
   * occurrences of the ':' and ',' characters not meant as
   * separators may be "quoted" by preceding them with the
   * backslash ('\') character and list items missing the ':'
   * character will be considered to specify only a domain name
   * (the type name will be an empty string); null or an empty string
   * for none.
   * @return An XML-formatted string containing the messages, or an empty
   * string if an error occurs.
   */
  public String requestSourcedMsgsStr(long requestTimeVal,
                          String hostMsgNumListStr,String domainTypeListStr)
  {
    try
    {
      logObj.debug2("QWServer.requestSourcedMsgsStr:  requestTimeVal=" +
                                 requestTimeVal + ", domainTypeListStr = " +
                                              ((domainTypeListStr != null) ?
                             ("\"" + domainTypeListStr + "\"") : "<null>"));
      logObj.debug2("  hostMsgNumListStr:  " +
              ((hostMsgNumListStr != null) ? hostMsgNumListStr : "<null>"));
      synchronized(reqMsgsCountSyncObj)
      {  //only allow one thread at a time to access counters
        ++reqMsgsTransactionCount;               //inc transaction count
      }
              //convert list of host-name/message-number entries
              // (if given) to a feeder-data-source tag-value table:
      TagValueTable hostMsgNumTableObj,trkHstMsgNumTblObj;
      if(hostMsgNumListStr != null && hostMsgNumListStr.length() > 0)
      {  //list string was given
        if((hostMsgNumTableObj=TagValueTable.entriesListStrToTable(
                                                hostMsgNumListStr)) == null)
        {     //error converting list string
          logObj.warning("Error in 'requestSourcedMsgsStr()':  " +
              "Unable to parse 'hostMsgNumListStr':  " + hostMsgNumListStr);
          return UtilFns.EMPTY_STRING;
        }
              //create tracking table by cloning feeder-data-source table:
        trkHstMsgNumTblObj = (TagValueTable)(hostMsgNumTableObj.clone());
      }
      else    //list string not given
        hostMsgNumTableObj = trkHstMsgNumTblObj = null;
              //convert list of event domain and type names
              // (if given) to a matcher object:
      final TwoObjectMatcher matcherObj =
             (domainTypeListStr != null && domainTypeListStr.length() > 0) ?
                      QWUtils.listStringToMatcher(domainTypeListStr) : null;
              //set flag if filtering by event domain and type names:
      final boolean filterFlag = (matcherObj != null);
      Element resendElem = null;
      int numMsgs, msgCount, numRequested = 0, maxNumMsgs = 0;
      long timeVal, msgNum = 0;
      if(hostMsgNumTableObj != null)
      {  //table of host-name/message-number entries is available
              //setup value for time "window" of messages to be retrieved:
        if((timeVal=requestTimeVal-SOURCED_REQMSGS_WINDOWMS) < 0)
          timeVal = 0;       //don't allow negative value
      }
      else    //table of host-name/message-number entries not available
        timeVal = requestTimeVal;      //use given requested-time value
      outerLoop:
      while(true)
      {  //loop if all fetched messages are filtered out
              //get Vector of requested messages (up to 'maxResendNumMsgs'):
        final VectorWithCount vec = cachedArchiveMgrObj.getNewerMessages(
                                                 timeVal,msgNum,maxNumMsgs);
        if(resendElem == null)
        {     //QWresend element not yet created; create it now
          resendElem = new Element(MsgTag.QW_RESEND);
        }
        numMsgs = vec.size();
        if(maxNumMsgs > 0 && numMsgs > maxNumMsgs)
        {     //too many messages in Vector (shouldn't happen)
          logObj.debug("requestSourcedMsgsStr() 'numMsgs' value reduced " +
                                   "from " + numMsgs + " to " + maxNumMsgs +
                                                     " (shouldn't happen)");
          numMsgs = maxNumMsgs;        //limit # of messages returned
        }
        Object obj;
        QWServerMsgRecord recObj = null;
        msgCount = 0;
        Element elemObj;
        TagValueTable.MutableLong mLongObj;
        long trkMsgNumVal;
        boolean includeFlag,checkTimeFlag;
        for(int i=0; i<numMsgs; ++i)
        {  //for each entry; fetch XML element from entry
          if((obj=vec.elementAt(i)) instanceof QWServerMsgRecord)
          {   //element object fetched OK
            recObj = (QWServerMsgRecord)obj;
                   //check if value of "FdrSourceHost" from message matches
                   // an entry in feeder-source host-names/msg-nums table:
            if(hostMsgNumTableObj != null &&
                                          recObj.fdrSourceHostStr != null &&
                                     recObj.fdrSourceHostStr.length() > 0 &&
                                            recObj.fdrSourceMsgNumVal > 0 &&
                                (mLongObj=hostMsgNumTableObj.getMutableLong(
                                          recObj.fdrSourceHostStr)) != null)
            {      //table available and feeder-source host name matches
                        //if feeder-source msg-num from msg is greater
                        // than msg-num from table then include message:
              if(includeFlag =
                          (recObj.fdrSourceMsgNumVal > mLongObj.getValue()))
              {    //message is to be included
                        //if this is the first message received for the
                        // feeder-data-source host name then verify that
                        // the feeder-data-source message number is equal
                        // to the specified message number + 1:
                if((trkMsgNumVal=trkHstMsgNumTblObj.get(
                                              recObj.fdrSourceHostStr)) > 0)
                {  //match found in feeder-data-source tracking table
                  if(recObj.fdrSourceMsgNumVal != trkMsgNumVal + 1)
                  {     //message number is not increment of table msgNum
                    logObj.debug("QWServer.requestSourcedMsgsStr:  " +
                         "Fetched message with feeder-data-source msg# (" +
                                                 recObj.fdrSourceMsgNumVal +
                           ") not increment of specified (" + trkMsgNumVal +
                            "), msgNum=" + recObj.msgNumber + ", timeGen=" +
                                recObj.getTimeGenerated() + ", fdrHost=\"" +
                                            recObj.fdrSourceHostStr + "\"");
                  }
                        //remove entry from tracking table:
                  trkHstMsgNumTblObj.remove(recObj.fdrSourceHostStr);
                }
              }
              checkTimeFlag = true;    //check timeGen vs. request time
            }
            else   //feeder-source host name from msg not matched to table
            {           //include message if time value >= request time:
              includeFlag = (recObj.getTimeGenerated() >= requestTimeVal);
              checkTimeFlag = false;   //don't need to check timeGen value
            }
            if(includeFlag && (!filterFlag || matcherObj.contains(
                              recObj.msgEvtDomainStr,recObj.msgEvtTypeStr)))
            {      //message is to be included and
                   // not filtering or domain/type names matched OK
              if((elemObj=recObj.getQWMsgElementObj()) != null)
              {    //element fetched OK
                        //if message timeGen is less than the request time
                        // then update the message timeGen attribute:
                if(checkTimeFlag &&
                                 recObj.getTimeGenerated() < requestTimeVal)
                {  //message timeGen is < request time; update attribute
                        //save copy of value in "OldTimeGenerated" attrib:
                  final String timeGenStr;
                  if((timeGenStr=elemObj.getAttributeValue(
                                          MsgTag.TIME_GENERATED)) != null &&
                                                    timeGenStr.length() > 0)
                  {     //timeGen value found; set "OldTimeGenerated" attrib
                    elemObj.setAttribute(MsgTag.ORIG_TIME_GENERATED,
                                                                timeGenStr);
                  }
                        //update message timeGen to request-time value:
                  synchronized(xmlDateFormatterObj)
                  { //only allow one thread at a time to use date-format obj
                    elemObj.setAttribute(MsgTag.TIME_GENERATED,
                                                 xmlDateFormatterObj.format(
                                                 new Date(requestTimeVal)));
                  }
                }
                        //add element as child to "QWresend" element
                resendElem.addContent(elemObj);
                ++msgCount;      //increment message count
                if(maximumResendNumMsgs > 0 &&
                                           msgCount >= maximumResendNumMsgs)
                {  //message limit exists and has been reached
                  numRequested += 2;   //indicate more msgs were requested
                  break outerLoop;     //exit 'while' loop
                }
              }
              else
              {      //unable to fetch element; log error message
                logObj.warning("Error in 'requestSourcedMsgsStr()':  " +
                    "Bad entry in cache (getQWMsgElementObj failed), entry[" +
                            i + "] = " + vec.elementAt(i) + ", obj = " + obj);
                return UtilFns.EMPTY_STRING;
              }
              ++numRequested;    //increment "request" count
            }
          }
          else
          {     //unexpected object fetched; log error message
            logObj.warning("Error in 'requestSourcedMsgsStr()':  " +
                      "Bad entry in cache (not QWServerMsgRecord), entry[" +
                          i + "] = " + vec.elementAt(i) + ", obj = " + obj);
            return UtilFns.EMPTY_STRING;
          }
        }
              //if no messages fetched or any messages were included
              // or no message-count limit then exit loop:
        if(numMsgs <= 0 || msgCount > 0 || maxNumMsgs <= 0 || recObj == null)
          break;
              //all messages from the fetch were filtered out; fetch
              // again using timeGen and msgNum from last fetched msg:
        timeVal = recObj.getTimeGenerated();
        msgNum = recObj.getMsgNum();
        maxNumMsgs = maximumResendNumMsgs;  //use msgs limit on next iter
        hostMsgNumTableObj = null;     //don't check table on next iteration
        logObj.debug2("requestSourcedMsgsStr:  Fetching more messages " +
                    "(time=" + requestTimeVal + ", msgNum=" + msgNum + ")");
      }
      resendElem.setAttribute(         //enter number of messages requested
                       MsgTag.NUM_REQUESTED,Integer.toString(numRequested));
      synchronized(reqMsgsCountSyncObj)
      {  //only allow one thread at a time to access counters
        reqMsgsNumMessagesSent += msgCount;      //add to messages total
      }
              //if notification-service event channel in use then
              // use "Debug" log level; if not then use "Debug3"
              // (because clients will be constantly polling):
      logObj.println(((notifSvcPortNum>0)?LogFile.DEBUG:LogFile.DEBUG3),
                           "Servicing request for sourced messages (time=" +
                                     requestTimeVal + "); " + numRequested +
                                       " requested, " + msgCount + " sent");
              //convert QWresend element to string and return it:
              // ((trim and encode whitespace)
      return IstiXmlUtils.elementToFixedString(resendElem);
    }
    catch(Exception ex)
    {         //some kind of error occurred; log it
      logObj.warning("Error in 'requestSourcedMsgsStr()':  " + ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
      return UtilFns.EMPTY_STRING;     //return empty string to indicate err
    }
  }

  /**
   * Adds the given message record to the cache and to the archive.
   * If a message with a matching data-event key is already in the cache
   * then the given message object is not added.  The message object is
   * queued and then added via a worker thread.
   * @param recObj the message-record object to add.
   * @return The value 'QWCachedArchiveMgr.ADDMSG_SUCCESS' if the message
   * record was added successfully; 'QWCachedArchiveMgr.ADDMSG_DUPLICATE'
   * if not because a message with a matching data-event key was already
   * in the cache; or 'QWCachedArchiveMgr.ADDMSG_ERROR' if an error
   * occurred.
   */
  public int addCachedArchiveMessage(QWServerMsgRecord recObj)
  {
    return cachedArchiveMgrObj.addMessage(recObj);
  }

  /**
   * Waits for the cached-archive's add-message queue to be empty, up to
   * 5 seconds.
   * @return true if the add-message queue is empty, false if messages
   * are waiting to be added and the max-wait-time was reached
   */
  public boolean waitForCachedArchiveAddQueueEmpty()
  {
    return cachedArchiveMgrObj.waitForAddMessageQueueEmpty();
  }

  /**
   * Sets up the standard attributes on the given 'QWmessage' element.
   * @param qwMsgElement the 'QWmessage' element to use.
   * @param msgNumVal the message-number value to use.
   * @param timeGenVal the time-generated value to use.
   * @param extraAttrib an extra attribute to add, or null for none.
   * @param domainNameStr the domain name to use, or null for none.
   * @param typeNameStr the type name to use, or null for none.
   * @param eventNameStr the event name to use, or null for none.
   */
  public void setupQWMessageElement(Element qwMsgElement,long msgNumVal,
                 long timeGenVal,Attribute extraAttrib,String domainNameStr,
                                     String typeNameStr,String eventNameStr)
  {
         //set message number:
    qwMsgElement.setAttribute(MsgTag.MSG_NUMBER,Long.toString(msgNumVal));
         //set the version number for the message format:
    qwMsgElement.setAttribute(MsgTag.MSG_VERSION,MsgTag.VERSION_NUMBER);
          //set the server program revision string:
    qwMsgElement.setAttribute(MsgTag.SERVER_REV_STR,
                                                  QWServer.REVISION_STRING);
         //set the server ID name string:
    qwMsgElement.setAttribute(MsgTag.SERVER_ID_NAME,getServerIdName());
         //set the server host address string:
    qwMsgElement.setAttribute(MsgTag.SERVER_ADDRESS,getServerHostAddress());
         //set the time generated value:
    synchronized(xmlDateFormatterObj)
    {    //only allow one thread at a time to use date-format object
      qwMsgElement.setAttribute(MsgTag.TIME_GENERATED,
                          xmlDateFormatterObj.format(new Date(timeGenVal)));
    }
    if(extraAttrib != null)       //if given then add extra attribute
      qwMsgElement.setAttribute(extraAttrib);
         //if given then add domain-name attribute:
    if(domainNameStr != null && domainNameStr.length() > 0)
      qwMsgElement.setAttribute(MsgTag.MSG_EVT_DOMAIN,domainNameStr);
         //if given then add type-name attribute:
    if(typeNameStr != null && typeNameStr.length() > 0)
      qwMsgElement.setAttribute(MsgTag.MSG_EVT_TYPE,typeNameStr);
         //if given then add event-name attribute:
    if(eventNameStr != null && eventNameStr.length() > 0)
      qwMsgElement.setAttribute(MsgTag.MSG_EVT_NAME,eventNameStr);

    if (privateKeyObj != null)  //if the private key exists
    {
      final String msgText = QWUtils.getTextForSignature(qwMsgElement);
      final String sigText = UtilFns.insertQuoteChars(
          IstiEncryptionUtils.generateSignatureText(msgText, privateKeyObj),
            UtilFns.DEF_SPECIAL_CHARS_STR);
      if (sigText != null && sigText.length() > 0)
      {
        //set the signature attribute
        qwMsgElement.setAttribute(MsgTag.SIGNATURE, sigText);
      }
    }
  }

  /**
   * Sends the given message string into the CORBA event channel.
   * @param domainNameStr the domain name to use, or null for none.
   * @param typeNameStr the type name to use, or null for none.
   * @param eventNameStr the event name to use, or null for none.
   * @param msgStr the message string to send.
   */
  public void sendEventChannelMessage(String domainNameStr,
                       String typeNameStr,String eventNameStr,String msgStr)
  {
    if(evtChManagerObj != null)
    {    //event channel is available
      try
      {       //send message string into CORBA event channel:
        final boolean retFlag;
        if(useStructuredEventsFlag)
        {     //using structured events
          retFlag = evtChManagerObj.sendMessage( //send names with message
                             domainNameStr,typeNameStr,eventNameStr,msgStr);
        }
        else  //not using structured events or names not given
          retFlag = evtChManagerObj.sendMessage(msgStr);   //just send msg
        if(!retFlag)
        {     //error flag returned by 'sendMessage()' method; log message
          logObj.warning(QWServer.PROGRAM_NAME +
                                         " error sending event message:  " +
                                         evtChManagerObj.getErrorMessage());
          evtChManagerObj.clearErrorMessage();   //clear message
        }
      }
      catch(Exception ex)
      {       //some kind of exception error error occurred; log it
        logObj.warning(QWServer.PROGRAM_NAME +
                                    " error sending event message:  " + ex);
        logObj.warning(UtilFns.getStackTraceString(ex));
      }
    }
  }

  /**
   * Sends the given event message to the event channel (if enabled) and
   * any outputters that are configured.
   * @param qwSvrMsgRecObj server-message-record object for message.
   * @param domainNameStr domain name to use, or null for none.
   * @param typeNameStr type name to use, or null for none.
   * @param eventNameStr event name to use, or null for none.
   * @param msgStr message string to send.
   */
  public void outputEventMessage(QWServerMsgRecord qwSvrMsgRecObj,
                                   String domainNameStr, String typeNameStr,
                                         String eventNameStr, String msgStr)
  {
    synchronized(lastDMsgSentTimeSyncObj)
    {  //grad thread synchronization object for time value
      lastDataMsgSentTimeVal = System.currentTimeMillis();   //mark msg time
    }
    sendEventChannelMessage(domainNameStr,typeNameStr,eventNameStr,msgStr);
    if(outputtersListObj != null)
    {  //outputters list is setup; send message to outputters
      sendMsgToOutputters(qwSvrMsgRecObj);
    }
  }

  /**
   * Returns the newest message object entered into the cache or archive.
   * @return The newest 'QWServerMsgRecord' object entered into the
   * cache or archive, or null if none have been entered.
   */
  public QWServerMsgRecord getLastStoredMsgObj()
  {
    return cachedArchiveMgrObj.getLastEnteredMsgObj();
  }

  /**
   * Increments the message number used in generated messages.
   * @param timeGenVal the time-generated value for the last-entered
   * message.
   */
  public void incrementMsgNumber(long timeGenVal)
  {
    synchronized(addMessageSyncObj)
    {    //grab sync-lock for message adds
      ++lastMessageNumber;                  //increment the message number
      lastTimeGeneratedVal = timeGenVal;    //enter timeGen value
    }
  }

  /**
   * Returns the message number used in the last generated message.
   * @return The message number used in the last generated message, or
   * 0 if no messages have been generated.
   */
  public long getLastMsgNumber()
  {
    synchronized(addMessageSyncObj)
    {    //grab sync-lock for message adds
      return lastMessageNumber;        //return the message number
    }
  }

  /**
   * Returns the time-generated value for the last-entered message.
   * @return The time-generated value for the last-entered message, or
   * 0 if no messages have been entered.
   */
  public long getLastTimeGeneratedVal()
  {
    synchronized(addMessageSyncObj)
    {    //grab sync-lock for message adds
      return lastTimeGeneratedVal;
    }
  }

  /**
   * Increments the "feeder source" message number.
   */
  public void incFdrSourceMsgNumber()
  {
    synchronized(addMessageSyncObj)
    {    //grab sync-lock for message adds
      ++fdrSourceMessageNumber;             //increment the message number
    }
  }

  /**
   * Returns the "feeder source" message number.
   * @return The "feeder source" message number, or 0 if no messages
   * have been generated that use the "feeder source" message number.
   */
  public long getFdrSourceMsgNumber()
  {
    synchronized(addMessageSyncObj)
    {    //grab sync-lock for message adds
      return fdrSourceMessageNumber;        //return the message number
    }
  }

  /**
   * Returns the default data-source host name for messages.
   * @return The default data-source host name for messages.
   */
  public String getDefaultFdrSourceHost()
  {
    return cfgObj.defaultFdrSourceHostProp.stringValue();
  }

  /**
   * Checks the given message number for the given feeder-data-source
   * host name to see if the same message number was previously
   * received.  The message number is then stored in the tracking
   * for the feeder-data-source host name.
   * @param feederSourceHostStr feeder-data-source host name.
   * @param feederSrcHostMsgIdNum feeder-data-source message number.
   * @return true if the message number is a duplicate; false if not.
   */
  public boolean checkFeederDataSourceMsgNumDup(String feederSourceHostStr,
                                                 long feederSrcHostMsgIdNum)
  {
    synchronized(fdrDataSrcHstMsgNmTblSyncObj)
    {  //grab thread-sync lock for feeder-data-source tracking table
      if(fdrDataSrcHostMsgNumTableObj != null)
      {  //feeder-data-source tracking table was been created
        if(fdrDataSrcHostMsgNumTableObj.isValueInTrkBuffer(
                                 feederSourceHostStr,feederSrcHostMsgIdNum))
        {  //host name / message number exists in tracking table
          return true;
        }
      }
      else    //feeder-data-source tracking table not yet created; create now
        fdrDataSrcHostMsgNumTableObj = new TagValueTrkTable();
                   //add host name / message number to tracking table:
      fdrDataSrcHostMsgNumTableObj.put(
                                 feederSourceHostStr,feederSrcHostMsgIdNum);
    }
    return false;
  }

  /**
   * Returns the current message number for the given feeder-data-source
   * host name.
   * @param feederSourceHostStr feeder-data-source host name.
   * @return The current message number for the given feeder-data-source
   * host name.
   */
  public long getCurFeederDataSourceMsgNum(String feederSourceHostStr)
  {
    synchronized(fdrDataSrcHstMsgNmTblSyncObj)
    {  //grab thread-sync lock for feeder-data-source tracking table
      return (fdrDataSrcHostMsgNumTableObj != null) ?
                  fdrDataSrcHostMsgNumTableObj.get(feederSourceHostStr) : 0;
    }
  }

  /**
   * Returns the flag to enable checking for MsgSrc/MsgIdent values
   * in messages.
   * @return true if checking for MsgSrc/MsgIdent values in messages
   * is enabled; false if not.
   */
  public boolean getCheckEQMsgIdentFlag()
  {
    return cfgObj.checkEQMsgIdentFlagProp.booleanValue();
  }

  /**
   * Returns the flag to enable checking for duplicate feeder-data-source
   * message numbers in messages.
   * @return true if checking for duplicate feeder-data-source message
   * numbers in messages; false if not.
   */
  public boolean getCheckDupFdrSourceFlag()
  {
    return cfgObj.checkDupFdrSourceFlagProp.booleanValue();
  }

  /**
   * Validates login information.
   * @param usernameText the username text.
   * @param passwordText the password text.
   * @param clientHostNameString the host name.
   * @param clientHostIPString the IP Address.
   * @return true if the login information should be accepted,
   * false otherwise.
   * @throws ServerLoginException if an error occurred while validating
   * the login.
   */
  public boolean validateLoginInformation(String usernameText,
                           String passwordText, String clientHostNameString,
                      String clientHostIPString) throws ServerLoginException
  {
    if(cfgObj.allowObsLoginClientsFlagProp.booleanValue() &&
                                    OBSLOGIN_UNAME_STR.equals(usernameText))
    {    //'allowObsLoginClientsFlag'==true and username=="qwuser2"
      logObj.debug3("QWServer.validateLoginInformation:  Allowing " +
                                     "obsolete-login client to connect ('" +
                 cfgObj.allowObsLoginClientsFlagProp.getName() +"'==true)");
      return true;      //accept login
    }
    return passwordManagerObj.validateLoginInformation(usernameText,
                      passwordText,clientHostNameString,clientHostIPString);
  }

  /**
   * Returns true if status-report queries from clients are allowed.
   * @return true if status-report queries from clients are allowed;
   * false if not.
   */
  public boolean getStRptClientQueriesFlag()
  {
    return stRptClientQueriesFlag;
  }

  /**
   * Returns the timestamp value for the latest status report.
   * @return The timestamp value for the latest status report,
   * or 0 if no report is available.
   */
  public long getStRptClientDataTimeVal()
  {
    synchronized(stRptClientDataSyncObj)
    {  //grab thread-synchronization object for 'stRptClientDataTimeVal'
      return stRptClientDataTimeVal;
    }
  }

  /**
   * Returns the latest status-report data.
   * @return A string containing the latest status-report data, or
   * an empty string if no report is available.
   */
  public String getStRptClientDataString()
  {
    synchronized(stRptClientDataSyncObj)
    {  //grab thread-synchronization object for 'stRptClientDataString'
      return stRptClientDataString;
    }
  }

  /**
   * Fetches the timestamp value for the latest status report.
   * @return The timestamp value for the latest status report,
   * or 0 if no report is available.
   */
  public long getStatusReportTime()
  {
         //if status-report queries from clients allowed then return
         // timestamp value for last report; otherwise return 0:
    return getStRptClientQueriesFlag() ? getStRptClientDataTimeVal() : 0;
  }

  /**
   * Fetches the latest status-report data.
   * @return A string containing the latest status-report data, or
   * an empty string if no report is available.
   */
  public String getStatusReportData()
  {
         //if status-report queries from clients allowed then return
         // data for last report; otherwise return empty string:
    final String dataStr;
    return (getStRptClientQueriesFlag() &&
                             (dataStr=getStRptClientDataString()) != null) ?
                                             dataStr : UtilFns.EMPTY_STRING;
  }

  /**
   * Returns a byte-array of certificate-file data.
   * @return A byte-array of certificate-file data, or a
   * zero-length array if no data is available.
   */
  public byte [] getCertificateFileData()
  {
    return certificateFileDataArray;
  }

  /**
   * Logs memory, thread-count, feeder, message and cache information.
   * @param indentStr indentation to use, or null for none.
   * @param logOutStm output stream into which information will be sent.
   * @param memThrdSpaceFlag true to generate blank line after memory
   * and thread-count information is logged.
   */
  private void logMemThrdFdrMsgsCacheInfo(String indentStr,
                           OutputStream logOutStm, boolean memThrdSpaceFlag)
  {
    if(indentStr == null)
      indentStr = UtilFns.EMPTY_STRING;
    if(lastDataMsgTimeFormatObj == null)
    {    //last-data-message-time formatter not yet created; create it now
      lastDataMsgTimeFormatObj =
                 UtilFns.createDateFormatObj("yyyy-MM-dd HH:mm:ss z");
    }
                        //create print-stream access to output stream:
    final PrintStream outStm =
               (logOutStm instanceof PrintStream) ? (PrintStream)logOutStm :
                       new PrintStream(new BufferedOutputStream(logOutStm));
              //show memory and thread-count information for QWServer:
    outStm.println(indentStr + QWReportStrings.REP_QWSEVERMEM_STR +
                                                UtilFns.getMemoryInfoStr());
    outStm.println(indentStr + QWReportStrings.REP_QWSTHRDCNT_STR +
                                             UtilFns.getTotalThreadCount());
              //if notif status-checker created then output notif values:
    if(notifSvrStatusCheckerObj != null)
      notifSvrStatusCheckerObj.outputStatusLogValues(indentStr,outStm);
    if(memThrdSpaceFlag)          //if flag then
      outStm.println();           //generate blank line
    outStm.println(indentStr + QWReportStrings.REP_NUMFEEDERS_STR +
                                             currentFeederModsTable.size());
         //show information for each feeder:
    Object obj;
    String nameStr,lastMsgTmStr,tblStr;
    QWFeederPlugin feederPluginObj;
    QWServerCallback callbackObj;
    long msgTimeVal, numMsgs, dataSize, totNumMsgs = 0, totDataSize = 0;
    final Iterator iterObj =
                        currentFeederModsTable.getValuesVector().iterator();
    while(iterObj.hasNext())
    {    //for each "QWFeederPlugin" object in List
      if((obj=iterObj.next()) instanceof QWFeederPlugin)
      {  //'QWFeederPlugin' object fetched OK
        feederPluginObj = (QWFeederPlugin)obj;   //set handle to object
                   //create name string for feeder module:
        nameStr = feederPluginObj.getRevisionString() + " ('" +
                                     feederPluginObj.getFeederName() + "')";
        if((msgTimeVal=feederPluginObj.getLastDataMsgTime()) > 0)
        {  //valid last-data-message-time value fetched
          lastMsgTmStr = "lastDataMsg @ " +      //build display string
               lastDataMsgTimeFormatObj.format(new Date(msgTimeVal)) + ", ";
        }
        else  //no valid last-data-message-time value
          lastMsgTmStr = UtilFns.EMPTY_STRING;
        if((callbackObj=feederPluginObj.getCallbackObj()) != null)
        {
          numMsgs = callbackObj.getFeederMessageCount();
          dataSize = callbackObj.getFeederTotalDataSize();
          outStm.println(indentStr + "  " + nameStr + ":  " + lastMsgTmStr +
                           "numMsgs=" + numMsgs + ", dataSize=" + dataSize +
                 (((tblStr=callbackObj.getFdrTblEntriesListStr()) != null) ?
                      (", feederSrcs:  " + tblStr) : UtilFns.EMPTY_STRING));
          totNumMsgs += numMsgs;
          totDataSize += dataSize;
        }
        else
        {
          outStm.println(indentStr + "  " + nameStr +
                         ":  " + lastMsgTmStr + "Call-back object is null");
        }
      }
    }
    if(totNumMsgs > 0)
    {    //messages have been received; show totals and average
      outStm.println(indentStr + "Total:  numMsgs=" + totNumMsgs +
                                    ", dataSize=" + totDataSize + ", avg=" +
                                     ((float)totNumMsgs*UtilFns.MS_PER_HOUR/
                         (System.currentTimeMillis()-serverStartupTimeMS)) +
                                                                   "/hour");
    }
         //show information about cache:
    outStm.println(indentStr + "Cache:  " +
                                         getServerCacheInfoStr(", ",false));
    synchronized(lastDMsgSentTimeSyncObj)
    {  //grad thread synchronization object for time value
      if(lastDataMsgSentTimeVal > 0)
      {  //time of last data message has a value; build display string
        lastMsgTmStr = "Last data message sent @ " +
          lastDataMsgTimeFormatObj.format(new Date(lastDataMsgSentTimeVal));
      }
      else    //no value for time of last data message
        lastMsgTmStr = "No data messages have been sent";
    }
    outStm.println(indentStr + lastMsgTmStr);    //show last-data-msg time
    outStm.flush();          //flush output to file
  }

  /**
   * Logs server-statistics information.
   */
  private void logServerStatisticsInfo()
  {
    logObj.debug("Server statistics information:");
                   //setup output stream for log output:
    final LogOutputStream logOutStm = logObj.getLogOutputStream();
                   //configure to output at "Debug" log level:
    logOutStm.setLoggingLevel(LogFile.DEBUG);
         //show feeder, message and cache information:
    logMemThrdFdrMsgsCacheInfo("  ",logOutStm,false);
         //show connected-clients information:
    if(clientsManagerObj != null)
    {    //clients manager has been initialized
      logObj.debug("  " + QWReportStrings.REP_NUMCLIENTS_STR +
                                                getConnectedClientsCount());
    }
         //show information about client request-messages transactions:
    final long transactionCount,numMessagesSent;
                   //get local copies of counter values:
    synchronized(reqMsgsCountSyncObj)
    {  //only allow one thread at a time to access counters
      transactionCount = reqMsgsTransactionCount;     //get transaction count
      numMessagesSent = reqMsgsNumMessagesSent;       //get messages total
    }
                   //calculate count differences since last iterval:
    final long itvlTransCount =
                             transactionCount - lastReqMsgsTransactionCount;
    final long itvlNumMsgsSent =
                               numMessagesSent - lastReqMsgsNumMessagesSent;
    final long curTimeVal = System.currentTimeMillis();
    logObj.debug("  Client request-messages transactions:");
    final long elapsedTimeMS;
    if(lastServerStatsLogTime > 0 &&
                      (elapsedTimeMS=curTimeVal-lastServerStatsLogTime) > 0)
    {    //this is not the first iteration and elapsed time is positive
      logObj.debug("    Since last log:  numTransactions=" +
                       itvlTransCount + ", numMsgsSent=" + itvlNumMsgsSent);
                   //calculate average count per hour, rounded up:
      final long avgTransCount,avgNumMsgsSent;
      avgTransCount = reqMsgsTransCountAveragerObj.enter(
                  (itvlTransCount * UtilFns.MS_PER_HOUR + elapsedTimeMS/2) /
                                                             elapsedTimeMS);
      avgNumMsgsSent = reqMsgsNumMsgsSentAveragerObj.enter(
                 (itvlNumMsgsSent * UtilFns.MS_PER_HOUR + elapsedTimeMS/2) /
                                                             elapsedTimeMS);
      logObj.debug("    Avg per hour:  numTransactions=" +
                       avgTransCount + ", numMsgsSent=" + avgNumMsgsSent);
    }
    logObj.debug("    Total:  numTransactions=" +
                     transactionCount + ", numMsgsSent=" + numMessagesSent);
                   //save values for next iteration:
    lastReqMsgsTransactionCount = transactionCount;
    lastReqMsgsNumMessagesSent = numMessagesSent;
    lastServerStatsLogTime = curTimeVal;
                   //log information about the outputters (if any):
    logOutputtersInfo("  ",logOutStm,false);
  }

  /**
   * Checks the current memory usage and thread count and logs warning
   * messages if either is excessive.
   * @return true if a warning message was logged; false if not.
   */
  private boolean checkWarnMemoryThreadUsage()
  {
    boolean warnFlag = false;
    int feedersCount = -1, clientsCount = -1, threadCount = -1;
    final int threadCountWarnMargin =
                                cfgObj.threadCountWarnMarginProp.intValue();
    if(threadCountWarnMargin > 0)
    {  //thread-count-warn value is setup
                                  //get feeder and client count values:
      feedersCount = currentFeederModsTable.size();   //get
      clientsCount = getConnectedClientsCount();
                   //get estimated thread count for outputters (if any):
      final int outptrsThreadCount = (outputtersListObj != null) ?
                                             getOutputtersThreadCount() : 0;
                                  //calculate thread-count limit:
      final int threadLimit = feedersCount*11 + clientsCount +
                            outptrsThreadCount + 21 + threadCountWarnMargin;
      threadCount = UtilFns.getTotalThreadCount();    //get thread count
      threadCountAveragerObj.enter(threadCount);      //enter into averager
      if(threadCount > threadLimit &&
                   (int)(threadCountAveragerObj.getAverage()) > threadLimit)
      {    //thread count over limit and avg also over limit; log message
        logObj.warning("Excessive QWServer thread count detected (limit=" +
                                          threadLimit + "):  " + threadCount);
        warnFlag = true;            //indicate warning was logged
      }
    }
    final int memorySizeWarnMBytes =
                                 cfgObj.memorySizeWarnMBytesProp.intValue();
                                  //get memory-in-use value:
    final long diffMemVal = systemRuntimeObj.totalMemory() -
                                              systemRuntimeObj.freeMemory();
    if(memorySizeWarnMBytes > 0 &&
                     diffMemVal >= (long)memorySizeWarnMBytes * (1024*1024))
    {    //mem-size-warn value set and memory in use over value; log msg
      logObj.warning("Excessive QWServer memory usage detected (limit=" +
              memorySizeWarnMBytes + "MB):  " + UtilFns.getMemoryInfoStr());
                                  //get feeder and client count values:
      feedersCount = currentFeederModsTable.size();
      clientsCount = getConnectedClientsCount();
      warnFlag = true;            //indicate warning was logged
    }
    if(warnFlag)
    {    //warning was logged; show # of feeders & clients
      logObj.debug("checkWarnMemoryThreadUsage:  # of feeders = " +
                         feedersCount + ", # of clients = " + clientsCount);
    }
    else
    {    //no warnings; log debug message
      logObj.debug2("checkWarnMemoryThreadUsage:  QWServer thread count (" +
                                      threadCount + ") and memory usage (" +
                                                       diffMemVal + ") OK");
    }
         //check notif-server status and log warnings if necessary:
    if(notifSvrStatusCheckerObj != null &&
                              notifSvrStatusCheckerObj.checkWarnStatusLog())
    {    //notif-server-status checker OK and warnings were logged
      warnFlag = true;       //indicate warnings logged
    }
    return warnFlag;
  }

  /**
   * Generates a server-status report.  Report data is sent to the
   * local and remote files specified by the "stRpt..." configuration
   * items.
   */
  private void generateServerStatusReport()
  {
                   //create byte-array output stream for report:
    final ByteArrayOutputStream btArrOutStm = new ByteArrayOutputStream();
                   //generate report into byte-array output stream:
    final long currentTimeVal = generateServerStatusReport(btArrOutStm);
    String repDataStr;
    if(stRptClientQueriesFlag && encryptDecryptUtilObj != null)
    {  //status-report queries from clients allowed and enc/decrypt setup OK
      repDataStr = btArrOutStm.toString();       //convert stream to string
                                                 //encrypt data:
      final String encRepStr = encryptDecryptUtilObj.encrypt(repDataStr);
      synchronized(stRptClientDataSyncObj)
      {  //grab thread-synchronization object for 'stRptClientDataString'
        stRptClientDataString = encRepStr;            //save data
        stRptClientDataTimeVal = currentTimeVal;      //save timestamp
      }
      if(encRepStr == null)
      {  //encrypt return null; log warning message
        logObj.warning("Error generating status report:  " +
                                             "'encrypt()' returned 'null'");
      }
    }
    else      //status-report queries from clients not allowed
      repDataStr = null;

    if(stRptLocalFileOutFlag)
    {    //status reports being output to local file
      if(stRptLocalFileOutObj == null)
      {  //local file not yet opened; open it now
        final int stRptFilesMaxAge;
        if((stRptFilesMaxAge=cfgObj.stRptFilesMaxAgeProp.intValue()) > 0)
        {     //local file max-age value given; use date-code in name
          stRptLocalFileOutObj = new LogFile(
                       fromBaseDir(cfgObj.stRptLocalFileProp.stringValue()),
                               LogFile.ALL_MSGS,LogFile.NO_MSGS,false,true);
              //setup local file max-age value:
          stRptLocalFileOutObj.setMaxLogFileAge(stRptFilesMaxAge);
        }
        else
        {     //local file max-age value not given
          stRptLocalFileOutObj = new LogFile(
                       fromBaseDir(cfgObj.stRptLocalFileProp.stringValue()),
                                    LogFile.ALL_MSGS,LogFile.NO_MSGS,false);
        }
      }
      logObj.debug2("Sending status report to local file \"" +
                                 stRptLocalFileOutObj.getFileName() + "\"");
      if(repDataStr == null)                     //if data string not setup
        repDataStr = btArrOutStm.toString();     // yet then convert now
              //send status report and separator to local file:
      stRptLocalFileOutObj.println(UtilFns.newline + UtilFns.newline +
                      repDataStr + UtilFns.newline + "-------------------" +
               "---------------------------------------------------------");
    }

    if(stRptRemotePathString != null)
    {    //status reports being output to remote file
      if(stRptFNameDateFormatObj == null)
      {  //remote-filename date formatter not yet created; create it now
        stRptFNameDateFormatObj =
                       UtilFns.createDateFormatObj("_yyyyMMdd'T'HHmmssSSS");
      }
      final boolean firstTimeFlag;
      if(firstTimeFlag = (stRptRemHostInfoObj == null))
      {  //host-info object not yet created; create it now
        stRptRemHostInfoObj = new ScpFileSender.ScpHostInfo(
                                   cfgObj.stRptRemoteHostProp.stringValue(),
                              cfgObj.stRptRemoteUserProp.stringValue(),null,
                              cfgObj.stRptRemChkHostFlagProp.booleanValue(),
                              cfgObj.stRptRemAuthKeyFileProp.stringValue());
      }
         //if this is second time through check to see if first
         // send attempt resulted in "fatal" error (auth-fail,
         // unknown-host, no-crypto) and disable future sends if so:
      if(stRptCheckFatalErrorFlag && !firstTimeFlag)
      {  //still need to check for fatal error and not first time here
        stRptCheckFatalErrorFlag = false;   //reset "check" flag
        if(ScpFileSender.getThreadFatalErrorFlag())
        {     //result of previous send was a "fatal" error
          stRptRemotePathString = null;     //disable future sends
          logObj.debug(
                 "Future sending of status-report file via 'scp' disabled");
          return;                           //abort method
        }
      }
              //add date-time code to remote filename:
      final String remFilePathStr = FileUtils.addStrToFileName(
          stRptRemotePathString,stRptFNameDateFormatObj.format(new Date()));
      logObj.debug2("Sending status report to remote file \"" +
                                     stRptRemHostInfoObj.usernameStr + "@" +
                                     stRptRemHostInfoObj.hostNameStr + ":" +
                                                     remFilePathStr + "\"");
              //transfer file via 'scp' (using worker thread):
      ScpFileSender.send(null,btArrOutStm.toByteArray(),remFilePathStr,
                                                stRptRemHostInfoObj,logObj);
    }
  }

  /**
   * Generates a server-status report to the given output stream.
   * @param repOutStmObj target 'OutputStream' object for report data.
   * @return The current-time value reflected in the report header
   * (milliseconds since 1/1/1970).
   */
  private long generateServerStatusReport(OutputStream repOutStmObj)
  {
    final long currentTimeVal = System.currentTimeMillis();
    if(stRptTitleDateFormatObj == null)
    {    //title-date formatter not yet created; create it now
      stRptTitleDateFormatObj =
                       UtilFns.createDateFormatObj("MMM d yyyy HH:mm:ss z");
    }
         //setup print-stream access to output stream:
    final PrintStream outStm =
                    new PrintStream(new BufferedOutputStream(repOutStmObj));
    outStm.println(QWReportStrings.REP_HEADER_STR + ", " +     //print title
                  stRptTitleDateFormatObj.format(new Date(currentTimeVal)));
    outStm.println();                            //print blank line
    outStm.println(QWServer.REVISION_STRING);    //print revision string
                                                 //print server ID:
    outStm.println(QWReportStrings.REP_SERVERID_STR + serverIDNameLocStr);

    if(stRptTrkLogLevelValue <= LogFile.ERROR)
    {    //min tracking log level within range (msgs are being tracked)
      final int maxTrkAgeMs =     //calc max tracking log msgs age, in ms
                           (int)(cfgObj.stRptTrkLogHoursProp.doubleValue() *
                                                 UtilFns.MS_PER_HOUR + 0.5);
      boolean msgsAvailFlag;
      String trkMsgsStr;          //fetch tracked log messages:
      if((trkMsgsStr=logObj.getTrackedMsgsAsString(
                                      UtilFns.newline,maxTrkAgeMs)) != null)
      {  //tracked log messages fetched OK; indicate if any available
        if(!(msgsAvailFlag = (trkMsgsStr.trim().length() > 0)))
          trkMsgsStr = null;      //if no messages then clear handle
      }
      else
      {  //problem fetching tracked log messages
        msgsAvailFlag = false;         //indicate no messages
        logObj.warning(
                   "Error fetching tracked log messages for status report");
      }
              //fetch tracked log messages from feeders:
      StringBuffer fdrBuffObj = null;       //buffer for fetched messages
      Object obj;
      String fdrNameStr,fdrMsgsStr;
      QWFeederPlugin feederPluginObj;
      LogFile fdrLogObj;
      final Iterator iterObj =
                        currentFeederModsTable.getValuesVector().iterator();
      while(iterObj.hasNext())
      {       //for each "QWFeederPlugin" object in List
        if((obj=iterObj.next()) instanceof QWFeederPlugin)
        {     //'QWFeederPlugin' object fetched OK
          feederPluginObj = (QWFeederPlugin)obj;      //set handle to object
                   //create name string for feeder module:
          fdrNameStr = feederPluginObj.getRevisionString() + " ('" +
                                     feederPluginObj.getFeederName() + "')";
          if((fdrLogObj=feederPluginObj.getLogFileObj()) != null &&
                               (fdrMsgsStr=fdrLogObj.getTrackedMsgsAsString(
                                      UtilFns.newline,maxTrkAgeMs)) != null)
          {   //tracked log messages for feeder fetched OK
            if(fdrMsgsStr.trim().length() > 0)
            {      //at least one tracked log msg for feeder fetched
              if(fdrBuffObj == null)             //if buffer not allocd then
                fdrBuffObj = new StringBuffer(); //allocate buffer
                   //add tracked log messages for feeder to buffer:
              fdrBuffObj.append(UtilFns.newline + "From " + fdrNameStr +
                                        ':' + UtilFns.newline + fdrMsgsStr);
              msgsAvailFlag = true;         //indicate messages available
            }
          }
          else
          {   //problem fetching tracked log messages for feeder
            logObj.warning("Error fetching tracked log messages for " +
                        "status report from feeder \"" + fdrNameStr + "\"");
          }
        }
      }
      if(msgsAvailFlag)
      {  //tracked log messages are available
        outStm.println();              //print blank line
                                       //print tracked-messages header:
        outStm.println(QWReportStrings.REP_TRKLOGMSGS_STR + " (\"" +
                             LogFile.getLevelString(stRptTrkLogLevelValue) +
                   "\" or greater" + ((maxTrkAgeMs > 0) ? (" within last " +
                       cfgObj.stRptTrkLogHoursProp.doubleValue() + " hour" +
                               ((maxTrkAgeMs != UtilFns.MS_PER_HOUR) ? "s" :
                                                    UtilFns.EMPTY_STRING)) :
                                              UtilFns.EMPTY_STRING) + "):");
        if(trkMsgsStr != null)              //if messages available then
          outStm.println(trkMsgsStr);       //print messages
        if(fdrBuffObj != null)                        //if msgs from feeders
          outStm.println(fdrBuffObj.toString());      // avail then print
      }
      else
      {  //no tracked log messages available
        outStm.println();              //print blank line
                                       //print no-tracked-messages message:
        outStm.println(QWReportStrings.REP_NOLOGMSGS_STR + " (\"" +
                             LogFile.getLevelString(stRptTrkLogLevelValue) +
                   "\" or greater" + ((maxTrkAgeMs > 0) ? (" within last " +
                       cfgObj.stRptTrkLogHoursProp.doubleValue() + " hour" +
                               ((maxTrkAgeMs != UtilFns.MS_PER_HOUR) ? "s" :
                                                    UtilFns.EMPTY_STRING)) :
                                               UtilFns.EMPTY_STRING) + ")");
      }
    }

         //print connected-clients information:
    if(clientsManagerObj != null)
    {    //clients manager has been initialized
      outStm.println();                //print blank line
      final int clientsCount;
      if((clientsCount=clientsManagerObj.getConnectedClientsCount()) > 0)
      {  //clients are connected
              //if list-clients flag set then show connected-clients list;
              // otherwise just show count:
        if(cfgObj.stRptClientsFlagProp.booleanValue())
          outStm.println(clientsManagerObj.getConnectedClientsStr());
        else
          outStm.println(QWReportStrings.REP_NUMCLIENTS_STR + clientsCount);
      }
      else    //no clients connected
        outStm.println(QWReportStrings.REP_NOCLIENTS_STR);
    }

         //print memory, threads, feeder, message and cache information:
    outStm.println();                  //print blank line
    logMemThrdFdrMsgsCacheInfo(null,outStm,true);
                   //print information about the outputters (if any):
    logOutputtersInfo(null,outStm,true);

    outStm.println();                  //print blank line
    if(cfgObj.qwServicesAvailFlagProp.booleanValue() || notifSvcPortNum > 0)
    {    //QWServices enabled or Notification-Service event channel in use
                             //print OpenORB version:
      outStm.println(QWReportStrings.REP_ORBVERSION_STR +
                                         OrbManager.getOpenOrbVersionStr());
    }
         //print Java version:
    outStm.println(QWReportStrings.REP_JAVAVERSION_STR + javaVersionString);
         //print OS name:
    outStm.println("OS name:  " + osNameString);
         //print maximum message size value:
    outStm.println(QWReportStrings.REP_MAXMSGSIZE_STR +
                                  cfgObj.maximumMessageSizeProp.intValue());

    outStm.flush();          //flush all data out to stream
    return currentTimeVal;   //return current-time ms value
  }

  /**
   * Returns a table of information property values for the server's cache
   * and the server itself.  The names for the property values will come
   * from the strings found in the 'ServerCacheInfoStrings' class.
   * @param incExtraFlag true to include extra non-cache items; false to
   * only include cache-message-related items.
   * @return A new table of information property values.
   */
  public FifoHashtable getServerCacheInfoTable(boolean incExtraFlag)
  {
    final FifoHashtable tableObj = new FifoHashtable();
    final IstiTimeObjectCache.CacheInformation cacheInfoObj;
    if(cachedArchiveMgrObj != null &&
           (cacheInfoObj=cachedArchiveMgrObj.getCacheInformation()) != null)
    {    //cached-archive manager OK and cache info fetched OK
                   //add entry for number of messages in cache:
      tableObj.put(ServerCacheInfoStrings.CACHE_MESSAGE_COUNT,
                               Integer.toString(cacheInfoObj.messageCount));
                   //add entry for total data size of messages in cache:
      tableObj.put(ServerCacheInfoStrings.CACHE_DATA_SIZE,
                                 Long.toString(cacheInfoObj.totalDataSize));
      if(cacheInfoObj.firstMsgObj != null)
      {  //first-cache-message object is available
                   //add entry for message time:
        tableObj.put(ServerCacheInfoStrings.CACHE_FIRSTMSG_TIME,
                Long.toString(cacheInfoObj.firstMsgObj.getTimeGenerated()));
        if(cacheInfoObj.firstMsgObj instanceof
                                  IstiMessageObjectCache.MessageObjectEntry)
        {     //message object is 'MessageObjectEntry' type
                   //add entry for message number:
          tableObj.put(ServerCacheInfoStrings.CACHE_FIRSTMSG_NUM,
                  Long.toString(((IstiMessageObjectCache.MessageObjectEntry)
                                  (cacheInfoObj.firstMsgObj)).getMsgNum()));
        }
      }
      if(cacheInfoObj.lastMsgObj != null)
      {  //last-cache-message object is available
                   //add entry for message time:
        tableObj.put(ServerCacheInfoStrings.CACHE_LASTMSG_TIME,
                 Long.toString(cacheInfoObj.lastMsgObj.getTimeGenerated()));
        if(cacheInfoObj.lastMsgObj instanceof
                                  IstiMessageObjectCache.MessageObjectEntry)
        {     //message object is 'MessageObjectEntry' type
                   //add entry for message number:
          tableObj.put(ServerCacheInfoStrings.CACHE_LASTMSG_NUM,
                  Long.toString(((IstiMessageObjectCache.MessageObjectEntry)
                                   (cacheInfoObj.lastMsgObj)).getMsgNum()));
        }
      }
      if(incExtraFlag)
      {  //include "extra" items
                   //add entry for age limit for messages in cache, in ms:
        tableObj.put(ServerCacheInfoStrings.CACHE_MAXAGE_MS,
                        Long.toString((long)maximumCacheAgeInSeconds*1000));
                   //add entry for recommended polling interval, in ms:
        tableObj.put(ServerCacheInfoStrings.REC_POLL_INTERVALMS,
               Integer.toString(cfgObj.recPollingItervalMsProp.intValue()));
      }
    }
    if(incExtraFlag)
    {    //include "extra" items
                   //add entry for server ID name:
      tableObj.put(ServerCacheInfoStrings.SERVER_ID_NAME,serverIDNameStr);
                   //add entry for server program name:
      tableObj.put(ServerCacheInfoStrings.SERVER_PROG_NAME,
                                                     QWServer.PROGRAM_NAME);
                   //add entry for server program version number:
      tableObj.put(ServerCacheInfoStrings.SERVER_VERSION_NUM,
                                                  QWServer.PROGRAM_VERSION);
    }
    return tableObj;
  }

  /**
   * Returns a string of information property values for the server's
   * cache and the server itself.  The string will contain a set of
   * comma-separated entries in the form:  name="value".  The names
   * for the entries will come from the strings found in the
   * 'ServerCacheInfoStrings' class.
   * @param sepStr the string that separates the values.
   * @param incExtraFlag true to include extra non-cache items; false to
   * only include cache-message-related items.
   * @return A string containing information property values.
   */
  public String getServerCacheInfoStr(String sepStr,boolean incExtraFlag)
  {           // (if including "extra" items then put quotes around values)
    return getServerCacheInfoTable(incExtraFlag).toQuotedStrings(
                                           sepStr,true,true,!incExtraFlag);
  }

  /**
   * Returns a string of information property values for the server's
   * cache and the server itself.  The string will contain a set of
   * comma-separated entries in the form:  name="value".  The names
   * for the entries will come from the strings found in the
   * 'ServerCacheInfoStrings' class.
   * @return A string containing information property values.
   */
  public String getServerCacheInfoStr()
  {
    return getServerCacheInfoStr(",",true);
  }

  /**
   * Returns a vector containing all of the message objects in the cache.
   * Each message object will be of type 'QWServerMsgRecord'.
   * @return A new vector containing all of the message objects in the
   * cache.
   */
  public Vector getAllCacheMessages()
  {
    return cachedArchiveMgrObj.getAllCacheMessages();
  }

  /**
   * Fetches messages newer or equal to the specified time value or
   * later than the specified message number.  If a non-zero message
   * number is given and the given time value matches then messages
   * with message numbers greater than the given message number are
   * returned; otherwise messages newer or equal to the specified time
   * value are returned (within a tolerance value).
   * @param requestTimeVal the time-generated value for message associated
   * with the given message number, or the requested time value to be used
   * (milliseconds since 1/1/1970).
   * @param msgNum the message number to use, or 0 or none.
   * @return A new 'Vector' containing the requested 'QWServerMsgRecord'
   * objects.
   */
  public Vector getCachedArchiveMsgs(long requestTimeVal, long msgNum)
  {
    return cachedArchiveMgrObj.getNewerMessages(requestTimeVal,msgNum,0);
  }

  /**
   * Returns the recommended polling interval for web-services clients.
   * @return The recommended polling interval for web-services clients,
   * in milliseconds.
   */
  public int getRecPollingItervalMs()
  {
    return cfgObj.recPollingItervalMsProp.intValue();
  }

  /**
   * Writes the given string to the clients-info log file.
   * @param str the string to write.
   */
  public void writeToClientsInfoLog(String str)
  {
    if(clientsInfoLogFileObj != null)
      clientsInfoLogFileObj.println(str);
  }

  /**
   * Returns the server ID name string for this QuakeWatch server.
   * @return The server ID name string.
   */
  public String getServerIdName()
  {
    return serverIDNameStr;
  }

  /**
   * Returns the server host address string for this QuakeWatch server.
   * @return The server host address string.
   */
  public String getServerHostAddress()
  {
    return serverHostAddress;
  }

  /**
   * Returns a string containing the host address, port number and ID name
   * for this QuakeWatch server.
   * @return A string in the format "addr;port;name".
   */
  public String getServerAddrPortNameStr()
  {
    return serverAddrPortNameStr;
  }

  /**
   * Returns the revision string for this QuakeWatch server.
   * @return The revision string.
   */
  public String getServerRevisionString()
  {
    return REVISION_STRING;
  }

  /**
   * Returns the type-name string used on status messages sent out
   * by the server.  When structured messages are enabled, status
   * messages will be sent with the domain name "StatusMessage" and
   * the type name set to this value.  These names can be used on the
   * client side to filter-in the status messages.
   * @return The type-name string used on status messages sent out
   * by the server (when structured messages are enabled).
   */
  public String getStatusMsgTypeName()
  {
    return statusMsgTypeNameStr;
  }

  /**
   * Returns a flag indicating whether or not the sending of structured
   * events is enabled.
   * @return true if the sending of structured events is enabled,
   * false if not.
   */
  public boolean getUseStructuredEventsFlag()
  {
    return useStructuredEventsFlag;
  }

  /**
   * Returns a flag indicating whether or not clients with unknown
   * distribution names should be rejected when logging in.
   * @return true if clients with unknown distribution names should
   * be rejected when logging in; false if not.
   */
  public boolean getRejectUnknownDistFlag()
  {
    return cfgObj.rejectUnknownDistFlagProp.booleanValue();
  }

  /**
   * Returns the number of currently-connected clients.
   * @return The number of currently-connected clients.
   */
  public int getConnectedClientsCount()
  {
    return (clientsManagerObj != null) ?
                           clientsManagerObj.getConnectedClientsCount() : 0;
  }

  /**
   * Returns the clients-manager object.
   * @return The clients-manager object.
   */
  public QWClientsManager getClientsManagerObj()
  {
    return clientsManagerObj;
  }

  /**
   * Returns the table of message-number/time-generated values for event
   * domain and type names.
   * @return The table of message-number/time-generated values for event
   * domain and type names, or null if not available.
   */
  public DomainTypeInfoTable getDomainTypeInfoTableObj()
  {
    return domainTypeInfoTableObj;
  }

  /**
   * Returns the default event-domain name.  This name is added to
   * event messages when no domain, type or event-name is provided
   * by the feeder module.
   * @return The default event-domain name, or an empty string is
   * none was specified.
   */
  public String getDefaultEvtDomainName()
  {
    return cfgObj.defaultEvtDomainNameProp.stringValue();
  }

  /**
   * Returns the default event-type name.  This name is added to
   * event messages when no domain, type or event-name is provided
   * by the feeder module.
   * @return The default event-type name, or an empty string is
   * none was specified.
   */
  public String getDefaultEvtTypeName()
  {
    return cfgObj.defaultEvtTypeNameProp.stringValue();
  }

  /**
   * Returns the ID string for the 'QWAcceptorImpl' object for this server.
   * @return The ID string for the 'QWAcceptorImpl' object for this server.
   */
  public String getQWAcceptorImplIDString()
  {
    return qwAcceptorImplIDString;
  }

  /**
   * Returns the maximum-size-limit value for messages.
   * @return The maximum-size-limit value for messages, or 0 if
   * no limit exists.
   */
  public int getMaximumMessageSize()
  {
    return cfgObj.maximumMessageSizeProp.intValue();
  }

  /**
   * Returns the list of alternate server IDs (defined in the
   * QWServer's configuration file).
   * @return The list of alternate server IDs, as a string in
   * the form "hostAddr:portNum,hostAddr:portNum,...".
   */
  public String getAltServersIdsList()
  {
    return altServersListMgr.getEntriesListStr();
  }

  /**
   * Returns the list of redirect server IDs (defined in the
   * QWServer's configuration file).
   * @param randomFlag true to return the entries in random order;
   * false to return the entries in order as entered.
   * @return The list of redirect server IDs, as a string in
   * the form "hostAddr:portNum,hostAddr:portNum,...".
   */
  public String getRedirServersIdsList(boolean randomFlag)
  {
    return redirServersListMgr.getEntriesListStr(randomFlag);
  }

  /**
   * Returns an indicator of whether or not the list of
   * redirect servers IDs is empty.
   * @return true if the list of redirect servers IDs is
   * empty; false if not.
   */
  public boolean isRedirServersListEmpty()
  {
    return (redirServersListMgr.size() <= 0);
  }

  /**
   * Returns true if this instance of the server has been terminated.
   * @return true if this instance of the server has been terminated;
   * false if not.
   */
  public boolean isServerTerminatedFlag()
  {
    return serverTerminatedFlag;
  }

  /**
   * Returns an indicator of whether or not the list of
   * redirect servers IDs should be randomized.
   * @return true if the list of redirect servers IDs should
   * be randomized; false if not.
   */
  public boolean getRandomizeRedirListFlag()
  {
    return cfgObj.randomizeRedirListFlagProp.booleanValue();
  }

  /**
   * Returns the number of connected clients beyond which
   * all new clients will be server-redirected.
   * @return The number of connected clients beyond which
   * all new clients will be server-redirected.
   */
  public int getRedirectConnectsCount()
  {
    return cfgObj.redirectConnectsCountProp.intValue();
  }

  /**
   * Returns the percentage of connected clients that are to be
   * server-redirected.
   * @return The percentage of connected clients that are to be
   * server-redirected, as an integer from 0 to 100.
   */
  public int getRedirectPercentValue()
  {
    return cfgObj.redirectPercentValueProp.intValue();
  }

  /**
   * Returns the main event-channel locator string.
   * @return The "IOR:" locator string for the main event channel,
   * or null if not available.
   */
  public String getMainEvtChLocatorStr()
  {
    return mainEvtChLocatorStr;
  }

  /**
   * Returns the ORB manager object.
   * @return The ORB manager object, or null if it has not been created.
   */
  public OrbManager getOrbManagerObj()
  {
    return orbManagerObj;
  }

  /**
   * Returns the server's log file object.
   * @return A 'LogFile' object.
   */
  public LogFile getLogFileObj()
  {
    return logObj;
  }

  /**
   * Terminates program via a clean exit.
   * @param val exit code to be returned to the operating system.
   */
  public final void terminateProgram(int val)
  {
    exitCleanup(true);       //do exit clean (wait for completions)
    if(cfgObj != null && cfgObj.systemExitOnCloseFlagProp.booleanValue())
      System.exit(val);      //if config flag then call system exit method
  }

  /**
   * Do all needed pre-exit cleanup
   * @param waitFlag true to wait for CORBA operations to complete, false
   * to return immediately.
   */
  private void exitCleanup(boolean waitFlag)
  {
    synchronized(exitCleanupSyncObject)
    {    //only allow one thread access at a time
      if(!serverTerminatedFlag)
      {  //clean not already run
        serverTerminatedFlag = true;   //ind cleanup performed; terminated
        if(logObj != null)
          logObj.debug("Performing exit cleanup");
              //if feeder table setup then stop QW feeders:
        if(currentFeederModsTable != null)
          stopFeeders(currentFeederModsTable);
              //if outputters list setup then stop QW outputters:
        if(outputtersListObj != null)
          stopOutputters();
        if(cachedArchiveMgrObj != null)       //if initialized then
          cachedArchiveMgrObj.close();        //close cached archive
        if(passwordManagerObj != null)        //if initialized then
          passwordManagerObj.shutdown();      //shutdown pwd DB connection
        if(housekeepingThreadObj != null)     //if created then
          housekeepingThreadObj.terminate();  //terminate housekeeping thread
        if(aliveMessageThreadObj != null)     //if created then
          aliveMessageThreadObj.terminate();  //terminate alive msg thread
        if(clientsManagerObj != null)         //if created then
          clientsManagerObj.close();          //close clients manager
        if(clientsInfoLogFileObj != null)     //if created then
          clientsInfoLogFileObj.close();      //close clients-info log file
        if(stRptLocalFileOutObj != null)      //if created then
          stRptLocalFileOutObj.close();       //close status-reports file
        if(messageLogFileObj != null)         //if message output opened then
          messageLogFileObj.close();          //close output file
        if(orbManagerObj != null)
        {     //ORB manager was initialized
          orbManagerObj.closeImplementation(waitFlag);  //stop ORB running
          if(orbManagerObj.waitForImplFinished(250))    //wait for finished
          {
            if(logObj != null)
              logObj.debug("ORB implementation closed OK");
          }
          else
          {
            if(logObj != null)
              logObj.debug("Unable to close ORB implementation");
          }
        }
        if(threadLoggerObj != null)           //if created then
          threadLoggerObj.terminate();        //terminate thread logger
        if(logObj != null)                    //if log file opened then
          logObj.close();                     //close log file
        if(consoleRedirectStream != null)
        {     //console redirect was setup
          try
          {        //attempt to restore console output streams:
            if(originalStdOutStream != null)
              System.setOut(originalStdOutStream);
            if(originalStdErrStream != null)
              System.setErr(originalStdErrStream);
          }
          catch(Exception ex)
          {
          }
          consoleRedirectStream.close();      //close output stream
        }
      }
    }
  }

  /**
   * Program entry point.
   * @param args an array of command-line arguments for the program,
   * or null for none.
   */
  public static void main(String [] args)
  {
    try
    {
    	System.setProperty("java.awt.headless","true"); //use headless AWT
    	//get Java version in use:
    	final String javaVersionString = QWUtils.checkJavaVersion();
      	if (javaVersionString == null)
      	{
      		return;
      	}
      	new QWServer(javaVersionString, args);
    }
    catch(StartupFailException ex)
    {    //server startup failed (error message already reported)
    }
  }

  /**
   * Sets the base directory to be used by the server for its input
   * and output files.  If a server-base directory is setup then all
   * non-absolute file pathnames will be referenced from this directory.
   * @param dirStr the server-base directory to use, or null or an
   * empty string for none.
   */
  public static void setServerBaseDirectoryStr(String dirStr)
  {
    serverBaseDirectoryStr = dirStr;
  }

  /**
   * Returns the base directory to be used by the server for its input
   * and output files.  If a server-base directory is setup then all
   * non-absolute file pathnames will be referenced from this directory.
   * @return The server-base directory in use, or null or an
   * empty string if none is in use.
   */
  public static String getServerBaseDirectoryStr()
  {
    return serverBaseDirectoryStr;
  }

  /**
   * Returns an indicator of whether or not a server-base directory
   * is setup.
   * @return true if a server-base directory is setup; false if not.
   */
  public static boolean isServerBaseDirectorySetup()
  {
    return (serverBaseDirectoryStr != null &&
                                serverBaseDirectoryStr.trim().length() > 0);
  }

  /**
   * Converts the given pathname to a pathname relative to the current
   * server-base directory.  If no server-base directory is setup or
   * if the given pathname is absolute (or a URL) then the original
   * pathname string is returned.
   * @param pathStr the pathname string to use.
   * @param subOnEmptyFlag if true and the given pathname is null or
   * an empty string and a server-base directory is setup then the
   * server-base directory path is returned; if false and the given
   * pathname is null or an empty string then the original pathname
   * is returned.
   * @return The converted pathname string; or the original pathname
   * string if not converted.
   */
  public static String fromBaseDir(String pathStr,boolean subOnEmptyFlag)
  {
    if(serverBaseDirectoryStr != null &&
                               serverBaseDirectoryStr.trim().length() > 0 &&
                                             !UtilFns.isURLAddress(pathStr))
    {    //server-base directory is setup and given path is not a URL
              //if given pathname is null or an empty string then
              // return server base dir or given pathname:
      if(pathStr == null || pathStr.trim().length() <= 0)
        return subOnEmptyFlag ? serverBaseDirectoryStr : pathStr;
      final File pathFileObj = new File(pathStr);
      if(!pathFileObj.isAbsolute())
      {  //given path is not an absolute path
              //make given path a child of the server-base dir
              // and return it as a pathname string:
        return (new File(serverBaseDirectoryStr,pathStr)).getPath();
      }
    }
    return pathStr;          //return original string
  }

  /**
   * Converts the given pathname to a pathname relative to the current
   * server-base directory.  If no server-base directory is setup or
   * if the given pathname is absolute (or a URL) then the original
   * pathname string is returned.  If the given pathname is null or
   * an empty string then the original pathname is returned.
   * @param pathStr the pathname string to use.
   * @return The converted pathname string; or the original pathname
   * string if not converted.
   */
  public static String fromBaseDir(String pathStr)
  {
    return fromBaseDir(pathStr,false);
  }

  /**
   * Starts up an instance of a QWServer.  A new worker thread is
   * launched to perform the startup.  The 'staticInvokeTerminateServer()'
   * method should be used terminate the server.  If a server instance was
   * already started (and not terminated) then this method does nothing.
   * @param javaVersionString the java version string.
   * @param args an array of command-line arguments for the server,
   * or null for none.
   */
  public static void staticInvokeStartupServer(
		  final String javaVersionString, final String [] args)
  {
    synchronized(staticInvokeSyncObj)
    {    //grab thread-synchronization object
      if(staticInvokeStartedFlag)
      {  //server has been started; check if server has been terminated
        if(getStaticInvokeServerObj() != null)
          return;       //if server not terminated then exit method
      }
      staticInvokeStartedFlag = true;       //indicate started
    }
         //do startup using a new thread:
    (new Thread("staticInvokeStartupServer")
        {
          public void run()
          {             //create instance of server and save handle:
            try
            {
              final QWServer serverObj = new QWServer(
            		  javaVersionString, args);
              synchronized(staticInvokeSyncObj)
              {    //grab thread-synchronization object
                staticInvokeQWServerObj = serverObj;  //save handle obj
                staticInvokeReadyFlag = true;         //indicate ready
              }
            }
            catch(StartupFailException ex)
            {      //server startup failed (error message already reported)
            }
          }
        }).start();
  }

  /**
   * Starts up an instance of a QWServer.  A new worker thread is
   * launched to perform the startup.  The 'staticInvokeTerminateServer()'
   * method should be used terminate the server.  If a server instance was
   * already started (and not terminated) then this method does nothing.
   */
  public static void staticInvokeStartupServer()
  {
    final String javaVersionString = QWUtils.checkJavaVersion();
    if (javaVersionString == null)
    {
      return;
    }
    staticInvokeStartupServer(javaVersionString, (String [])null);
  }

  /**
   * Starts up an instance of a QWServer.  A new worker thread is
   * launched to perform the startup.  The 'staticInvokeTerminateServer()'
   * method should be used terminate the server.  If a server instance was
   * already started (and not terminated) then this method does nothing.
   * @param javaVersionString the java version string.
   * @param paramStr a single command-line argument for the server,
   * or null for none.
   */
  public static void staticInvokeStartupServer(
		  final String javaVersionString, String paramStr)
  {
    staticInvokeStartupServer(javaVersionString, new String[] { paramStr });
  }

  /**
   * Starts up an instance of a QWServer.  A new worker thread is
   * launched to perform the startup.  The 'staticInvokeTerminateServer()'
   * method should be used terminate the server.  If a server instance was
   * already started (and not terminated) then this method does nothing.
   * @param javaVersionString the java version string.
   */
  public static void staticInvokeStartupServer(final String javaVersionString)
  {
    staticInvokeStartupServer(javaVersionString, (String [])null);
  }

  /**
   * Returns the QWServer instance that was started up via the
   * 'staticInvokeStartupServer()' method.
   * @return The started-up QWServer object, or null if the server
   * has not been started, is not yet ready or has been terminated.
   */
  public static QWServer getStaticInvokeServerObj()
  {
    synchronized(staticInvokeSyncObj)
    {    //grab thread-synchronization object
      if(staticInvokeReadyFlag)
      {  //server has been statically invoked and is ready
        if(staticInvokeQWServerObj != null)
        {     //static handle to server is setup
                   //if server not terminated then return handle:
          if(!staticInvokeQWServerObj.isServerTerminatedFlag())
            return staticInvokeQWServerObj;
        }
        staticInvokeReadyFlag = false;      //clear server-ready flag
      }
      return null;
    }
  }

  /**
   * Terminates the QWServer instance that was started up via the
   * 'staticInvokeStartupServer()' method.  If the server has not
   * been started or is not yet ready then this method does nothing.
   */
  public static void staticInvokeTerminateServer()
  {
    synchronized(staticInvokeSyncObj)
    {    //grab thread-synchronization object
      if(!staticInvokeReadyFlag)
        return;         //if server not running then exit method
              //clear flag to disable 'getStaticInvokeServerObj()' method:
      staticInvokeReadyFlag = false;
    }
    if(staticInvokeQWServerObj != null)               //if handle OK then
      staticInvokeQWServerObj.exitCleanup(true);      //terminate server
    synchronized(staticInvokeSyncObj)
    {    //grab thread-synchronization object
      staticInvokeQWServerObj = null;       //clear handle to instance
      staticInvokeStartedFlag = false;      //reset started-up flag
    }
  }


  /**
   * Class StartupFailException defines an exception thrown when the server
   * failes to startup.
   */
  private class StartupFailException extends Exception
  {
    /**
     * Creates an instance of the exception thrown when the server
     * failes to startup.
     */
    public StartupFailException()
    {
    }
  }


  /**
   * Class CleanupThread is inserted as a shutdown hook, and is
   * responsible for cleaning up the server before a forced exit.
   */
  private class CleanupThread extends Thread
  {
    public void run()
    {
      //System.out.println("Cleanup thread starting");
      exitCleanup(false);    //do exit clean (don't wait for completions)
      //System.out.println("Cleanup thread exiting");
    }
  }


  /**
   * Class AliveMessageThread defines a thread that sends periodic "Alive"
   * messages into the CORBA-event-channel.
   */
  private class AliveMessageThread extends IstiNotifyThread
  {
    private final int aliveDelayMsVal;
    private boolean requestMsgFlag = false;

    /**
     * Creates a thread that sends periodic "Alive" messages into the
     * CORBA-event-channel.
     * @param delayMsVal the amount of time (in milliseconds) to delay
     * between messages.
     */
    public AliveMessageThread(int delayMsVal)
    {
      super("AliveMessageThread");
      aliveDelayMsVal = delayMsVal;
    }

    /**
     * Method run when thread starts.
     */
    public void run()
    {
      waitForNotify(1000);        //delay to let channel startup
      if(isTerminated())          //if terminate flag set then
        return;                   //exit thread
      sendStatusMessage(MsgTag.STARTUP);    //send out "Startup" message
      long currentTimeVal;
      long lastAliveMsgTime = System.currentTimeMillis();
      while(true)
      {   //loop while waiting
        waitForNotify(1000);
        if(isTerminated())    //if terminate flag set then
          return;             //exit thread
        currentTimeVal = System.currentTimeMillis();
        if((aliveDelayMsVal > 0 &&
                    currentTimeVal >= lastAliveMsgTime + aliveDelayMsVal) ||
               (requestMsgFlag && currentTimeVal - lastAliveMsgTime > 1500))
        {     //periodic "Alive" messages enabled and time for next one
              // or immediate alive message requested and more than
              // 1.5 seconds since last one
          if(requestMsgFlag)
          {   //immediate alive message requested by client
            requestMsgFlag = false;              //clear request flag
            logObj.debug2(
                      "Sending server-alive message requested by client");
          }
          lastAliveMsgTime = currentTimeVal;     //save time-sent value
                   //if timestamp for latest status report available
                   // then setup to add it as data value in message:
          long dataTimeVal;       // (precede timestamp with "stRptTime")
          final String msgDataStr =
                           ((dataTimeVal=getStRptClientDataTimeVal()) > 0) ?
                                    MsgTag.ST_RPT_TIME + dataTimeVal : null;
          try
          {                  //send out message:
            sendStatusMessage(MsgTag.ALIVE,msgDataStr);
          }
          catch(Exception ex)
          {   //some kind of exception error; log it
            logObj.warning("Error sending 'Alive' message:  " + ex);
            logObj.warning(UtilFns.getStackTraceString(ex));
          }
        }
      }
    }

    /**
     * Requests that an alive message be sent immediately.
     */
    public void requestMessage()
    {
      requestMsgFlag = true;
      notifyThread();        //interrupt any 'waitForNotify()' in progress
    }
  }


  /**
   * Class HousekeepingThread defines a thread that performs various
   * "housekeeping" tasks.  These include checking the feeder-modules
   * configuration file for changes and stopping/starting feeders as
   * needed, removing old messages from the cache and archive, removing
   * timed-out clients from the clients manager, outputting to the
   * clients-info log file, and outputting to the memory-information
   * log file.
   */
  private class HousekeepingThread extends IstiNotifyThread
  {
         //flag set true when time to generate a status report:
    private boolean genStatusReportFlag = false;
         //flag set true when warning messages trigger a status report:
    private boolean warningMsgTrigFlag = false;
         //time value for trigger delay after warning message
         // (to allow multiple messages to get into same report):
    private long delayStatusRepTimeVal = 0;
         //value for delay after report trigger via warning message:
    private static final int MSGTRIG_DELAY_MS = 2000;

    /** Status-report tracked-log-message listener. */
    public final DataChangedListener stRptTrkLogMsgListener =
      new DataChangedListener()
        {     //listener to trigger immediate report on tracked log msgs
          public void dataChanged(Object sourceObj)
          {        //only set 'generate' flag if calling thread
            try    // is not the 'Housekeeping' thread itself and
            {      // is not an 'ScpFileSender' thread:
              final Thread threadObj;
              if(!warningMsgTrigFlag &&
                             !((threadObj=Thread.currentThread()) instanceof
                                                      HousekeepingThread) &&
                                      !(threadObj instanceof ScpFileSender))
              {  //not already triggered by warn msg and calling thread OK
                        //setup time delay value (to allow multiple warning
                        // messages to get into same report):
                delayStatusRepTimeVal = System.currentTimeMillis() +
                                                           MSGTRIG_DELAY_MS;
                        //set generate-report and triggered flags
                genStatusReportFlag = warningMsgTrigFlag = true;
              }
            }
            catch(Exception ex)
            {      //some kind exception error; ignore and move on
            }
          }
        };


    /**
     * Creates a thread that performs various "housekeeping" tasks.
     */
    public HousekeepingThread()
    {
      super("HousekeepingThread");
    }

    /**
     * Method run when thread starts.
     */
    public void run()
    {
              //setup checking interval for feeders status error messages:
      final long checkFdrErrIntervalMS =
                     (long)(cfgObj.checkFdrStatusErrMinsProp.doubleValue() *
                                                     UtilFns.MS_PER_MINUTE);
      final LogFile memoryLogFileObj;  //handle for mem-info logger
      final int memoryLogIntervalMS;   //get memory-info log interval value
      if((memoryLogIntervalMS=cfgObj.memoryLogIntervalMSecsProp.intValue())
                                                                        > 0)
      {  //memory-info log file is enabled; setup log file
        memoryLogFileObj = new LogFile(fromBaseDir(MEMINFO_LOGGER_FNAME),
                               LogFile.ALL_MSGS,LogFile.NO_MSGS,false,true);
              //set switch interval for memory-info log files:
        memoryLogFileObj.setLogFileSwitchIntervalDays(
                                 cfgObj.memoryLogSwitchDaysProp.intValue());
              //if given then enter max-age value for memory-info logs:
        final int memLogMaxAgeInDays;
        if((memLogMaxAgeInDays=cfgObj.memoryLogMaxAgeInDaysProp.intValue())
                                                                        > 0)
        {
          memoryLogFileObj.setMaxLogFileAge(memLogMaxAgeInDays);
        }
      }
      else
        memoryLogFileObj = null;

              //setup logging interval for clients-info log, in ms:
      final long clientsInfoLogIntervalMS = (clientsInfoLogFileObj!=null) ?
          ((long)(cfgObj.clientsInfoLogIntervalSecsProp.intValue())*1000L) :
                                                                         0L;
              //setup check/warn interval memory and threads usage, in ms:
      final long chkMemThreadsIntervalMS =
                          (cfgObj.memorySizeWarnMBytesProp.intValue() > 0 ||
                          cfgObj.threadCountWarnMarginProp.intValue() > 0) ?
                                              CHK_MEMTHREADS_INTERVALMS : 0;
              //setup logging interval for statistics log, in ms:
      final long statsLogIntervalMS =
                 (long)(cfgObj.statsLogIntervalSecsProp.intValue()) * 1000L;
              //setup logging interval for status-report output, in ms:
      final long stRptOutIntervalMS =
                             (long)(cfgObj.stRptItvlMinsProp.doubleValue() *
                                                     UtilFns.MS_PER_MINUTE);
              //setup minimum iterval between status-report outputs
              // (use 1 minute unless output interval is small):
      final long stRptMinOutItvlMS =
                          (stRptOutIntervalMS >= UtilFns.MS_PER_MINUTE*10) ?
                              UtilFns.MS_PER_MINUTE : stRptOutIntervalMS/10;
      if(notifSvcPortNum > 0 && cfgObj.notifSvrStatusLogNameProp.
                                        stringValue().trim().length() > 0 &&
                   (chkMemThreadsIntervalMS > 0 || statsLogIntervalMS > 0 ||
                                                    stRptOutIntervalMS > 0))
      {  //Notification-Service event channel in use and
         // name of Notification-Service status log was specified and
         // at memory/thread checking or status output is enabled
                        //create checker for notif-server status:
        notifSvrStatusCheckerObj = new NotifSvrStatusChecker(
                fromBaseDir(cfgObj.notifSvrStatusLogNameProp.stringValue()),
                                               CHK_MEMTHREADS_INTERVALMS*10,
                           cfgObj.notifSvrStatusWarnFlagProp.booleanValue(),
                                 cfgObj.memorySizeWarnMBytesProp.intValue(),
                                cfgObj.threadCountWarnMarginProp.intValue(),
                                                                    logObj);
      }
      long currentTimeVal,fdrModsCfgChkTime,lastRemMsgsTime,
           lastRemClientsTime,chkMemThreadsLogTime,statisticsLogTime;
      long lastChkFdrMsgTime = 0, lastMemLogTime = 0,
           lastClientsInfoLogTime = 0, stRptOutputTime = 0,
           stRptOutCheckTime = 0;
      String infoStr, lastClientsInfoStr = UtilFns.EMPTY_STRING;
              //initialize last-remove and stats-log times to "now":
      fdrModsCfgChkTime = lastRemMsgsTime = lastRemClientsTime =
                                  chkMemThreadsLogTime = statisticsLogTime =
                                                 System.currentTimeMillis();
      while(!isTerminated())
      {  //loop while thread not terminated
        waitForNotify(1000);
        if(isTerminated())        //if terminate flag set then
          break;                  //exit thread
        currentTimeVal = System.currentTimeMillis();
              //check if feeder-modules-cfg file in use and modified:
        if(fdrModsCfgFileMgrObj != null &&
              currentTimeVal >= fdrModsCfgChkTime + CHK_FDRSFILE_INTERVALMS)
        {     //feeder-modules-cfg file in use and time for next check
          fdrModsCfgChkTime = currentTimeVal;    //mark next check time
          try
          {   //check feeder-modules-cfg file and start/stop feeders:
            checkFeederModsCfgFile();
          }
          catch(Exception ex)
          {     //some kind of exception error; log it
            logObj.warning("Error processing feeder-modules " +
                                              "configuration file:  " + ex);
            logObj.warning(UtilFns.getStackTraceString(ex));
          }
          if(isTerminated())      //if terminate flag set then
            break;                //exit thread
        }
              //check feeders status error messages (if enabled):
        if(checkFdrErrIntervalMS > 0 &&
                currentTimeVal >= lastChkFdrMsgTime + checkFdrErrIntervalMS)
        {  //checking enabled and time for next check
          lastChkFdrMsgTime = currentTimeVal;    //set time of check
          try
          {     //check and log feeders status-error messages (if any):
            checkFeedersStatusErrorMsgs();
          }
          catch(Exception ex)
          {      //some kind of exception error; log it
            logObj.warning(
                    "Error checking feeders status-error messages:  " + ex);
            logObj.warning(UtilFns.getStackTraceString(ex));
          }
          if(isTerminated())      //if terminate flag set then
            break;                //exit thread
        }
              //remove old messages from cache and archive:
        if(cachedArchiveMgrObj != null &&
                 currentTimeVal >= lastRemMsgsTime + REM_OLDMSGS_INTERVALMS)
        {     //cached archive is initialized and time for next iteration
          lastRemMsgsTime = currentTimeVal;      //mark removal time
          try
          {   //remove old messages; track changes:
            final int oldCacheSize = cachedArchiveMgrObj.getCacheSize();
            if(cachedArchiveMgrObj.removeOldObjects())
            {
              final int newCacheSize = cachedArchiveMgrObj.getCacheSize();
              logObj.debug3("HousekeepingThread:  Removed " +
                                             (oldCacheSize-newCacheSize) +
                             " item(s) from message cache, new numMsgs = " +
                                                              newCacheSize);
            }
          }
          catch(Exception ex)
          {     //some kind of exception error; log it
            logObj.warning("Error removing old messages from cache:  " + ex);
            logObj.warning(UtilFns.getStackTraceString(ex));
          }
          if(isTerminated())      //if terminate flag set then
            break;                //exit thread
        }
        if(clientsManagerObj != null)
        {     //client manager is initialized
                   //remove timed-out clients from the clients manager:
          if(currentTimeVal >=
                             lastRemClientsTime + REM_OLDCLIENTS_INTERVALMS)
          {   //time for next iteration for client-conn check/removal
            lastRemClientsTime = currentTimeVal;   //mark removal time
            try
            {
              clientsManagerObj.removeTimedOutClients(clientsInfoLogFileObj);
            }
            catch(Exception ex)
            {     //some kind of exception error; log it
              logObj.warning("Error removing timed-out clients from " +
                                                 "clients manager:  " + ex);
              logObj.warning(UtilFns.getStackTraceString(ex));
            }
          }
                   //manage clients-info log file:
          if(clientsInfoLogIntervalMS > 0 &&
                             (clientsManagerObj.clearClientsChangedFlag() ||
                                                           currentTimeVal >=
                         lastClientsInfoLogTime + clientsInfoLogIntervalMS))
          {   //log enabled and clients changed or time for next check/log
            lastClientsInfoLogTime = currentTimeVal;  //set new 'last' time
            try
            {
              if(!lastClientsInfoStr.equals(
                        infoStr=clientsManagerObj.getConnectedClientsStr()))
              {    //clients-changed flag set and info string changed
                lastClientsInfoStr = infoStr;    //save new info str
                clientsInfoLogFileObj.println(   //log info str
                                          UtilFns.newline + "  " + infoStr);
                                                 //put blank line after:
                clientsInfoLogFileObj.println(UtilFns.EMPTY_STRING);
              }
            }
            catch(Exception ex)
            {      //some kind of exception error; log it
              logObj.warning("Error logging clients info:  " + ex);
              logObj.warning(UtilFns.getStackTraceString(ex));
            }
          }
          if(isTerminated())      //if terminate flag set then
            break;                //exit thread
        }
              //output to memory-information log file (if enabled):
        if(memoryLogIntervalMS > 0 &&
                     currentTimeVal >= lastMemLogTime + memoryLogIntervalMS)
        {     //memory-info logs enabled and time for next memory-info log
          lastMemLogTime = currentTimeVal;  //set time of memory-info output
          try
          {         //log memory info:
            memoryLogFileObj.println(UtilFns.getMemoryInfoStr());
          }
          catch(Exception ex)
          {      //some kind of exception error; log it
            logObj.warning("Error logging memory info:  " + ex);
            logObj.warning(UtilFns.getStackTraceString(ex));
          }
          if(isTerminated())      //if terminate flag set then
            break;                //exit thread
        }
                   //log server statistics:
        if(statsLogIntervalMS > 0 &&
                   currentTimeVal >= statisticsLogTime + statsLogIntervalMS)
        {     //time for next iteration for statistics log
          statisticsLogTime = currentTimeVal;   //mark log time
          try
          {
            logServerStatisticsInfo();
          }
          catch(Exception ex)
          {   //some kind of exception error; log it
            logObj.warning("Error logging server statistics:  " + ex);
            logObj.warning(UtilFns.getStackTraceString(ex));
          }
          if(isTerminated())      //if terminate flag set then
            break;                //exit thread
        }
                   //check/warn memory usage and thread count:
        if(chkMemThreadsIntervalMS > 0 && currentTimeVal >=
                             chkMemThreadsLogTime + chkMemThreadsIntervalMS)
        {     //time for next iteration for check/warn
          chkMemThreadsLogTime = currentTimeVal;      //mark chk time
          try
          {
            if(checkWarnMemoryThreadUsage() && stRptOutIntervalMS > 0 &&
                                   stRptTrkLogLevelValue <= LogFile.WARNING)
            { //memory/thread warning was logged, status reports enabled
              // and tracking "warning"-level log messages
                        //setup time delay value (to allow multiple warning
                        // messages to get into same report):
              delayStatusRepTimeVal = currentTimeVal + MSGTRIG_DELAY_MS;
                        //set generate-report and triggered flags
              genStatusReportFlag = warningMsgTrigFlag = true;
            }
          }
          catch(Exception ex)
          {   //some kind of exception error; log it
            logObj.warning("Error checking memory/thread usage:  " + ex);
            logObj.warning(UtilFns.getStackTraceString(ex));
          }
          if(isTerminated())      //if terminate flag set then
            break;                //exit thread
        }
                   //generate status report:
        if(stRptOutIntervalMS > 0 &&
                     currentTimeVal >= stRptOutputTime + stRptOutIntervalMS)
        {     //interval setup and time for next iteration of status report
          stRptOutputTime = currentTimeVal;      //mark output time
          genStatusReportFlag = true;            //set flag to do report
                   //clear output-check time to prevent possible delay:
          stRptOutCheckTime = 0;
        }
        if(genStatusReportFlag && currentTimeVal >= delayStatusRepTimeVal)
        {  //flag set to generate status report and at or past delay time
                   //check that 'stRptMinOutItvlMS' time has elapsed since
                   // last status-report output (to make sure that tracked
                   // log messages are not generating outputs too quickly):
          if(currentTimeVal - stRptOutCheckTime >= stRptMinOutItvlMS)
          {   //time since last status-report output >= than minimum
                        //clear generate-report and triggered flags
            genStatusReportFlag = warningMsgTrigFlag = false;
            stRptOutCheckTime = currentTimeVal;  //save output-check time
            try
            {
              generateServerStatusReport();
            }
            catch(Exception ex)
            {      //some kind of exception error; log it
              logObj.warning("Error generating status report:  " + ex);
              logObj.warning(UtilFns.getStackTraceString(ex));
            }
          }
        }
      }
      if(memoryLogFileObj != null)          //if created then
        memoryLogFileObj.close();           //close mem-info logger
    }
  }
}
