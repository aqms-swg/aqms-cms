# CMS (QWServer)

#### Introduction

The CISN Messaging Service (CMS) provides an end-to-end implementation for reliable XML-based messaging. It was previously known as the QuakeWatch Information Distribution System (QWIDS.) Its features include multi-platform support, a high level of configurability, persistent archiving of messages, and missed-message tracking/recovery. QWIDS was developed as a next-generation earthquake notification system to alert users, in near real-time, of seismic data and vital earthquake-hazards information following significant events.

The QuakeWatch server operates as a "hub", receiving messages from one or more sources and passing them on to any number of QuakeWatch-client applications. It receives its data from configurable "feeder" modules.

The messages are then passed along to QuakeWatch clients via a Notification Service CORBA-event-channel. The QuakeWatch server provides support for resending requested messages, allowing clients to catch-up on missed messages.
   
The QuakeWatch server is written entirely in Java, allowing it to run on all major operating systems and hardware. The messaging system uses an open-source version of the industry-standard Common Object Request Broker Architecture (CORBA).

#### Requirements

- This project may be compiled using Gradle or Apache Ant and Apache BCEL.

##### Gradle build
- Java 1.8 or later needs to be installed
- [Gradle](https://gradle.org/) needs to installed

This builds the executable ``QWServer.jar`` in the ``aqms-cms/build/libs`` directory.
```
$ cd aqms-cms-libs
$ gradle build
$ cd ../aqms-cms
$ gradle build
```

To run the QuakeWatch server:
```
$ cd aqms-cms/build/libs
$ java -jar QWServer.jar
```

##### Ant build

- Java 1.8 needs to be installed
- [Apache Ant](https://ant.apache.org/) needs to installed
- [Apache BCEL](https://commons.apache.org/proper/commons-bcel/) needs to be installed
The easiest way to do so is to run the following:
```
mkdir -p ~/.ant/lib
cd ~/.ant/lib
wget https://assets.isti.com/libs/zips/bcel-5.2.jar
mv bcel-5.2.jar bcel.jar
```
- PDL (https://usgs.github.io/pdl/index.html) is required. A ``ProductClient`` directory containing all of the PDL files should be at the same level as the ``aqms-cms`` directory.
- The ``aqms-cms-libs`` project.

#### Directory structure
- ``ProductClient `` -> PDL (https://usgs.github.io/pdl/index.html) files, which is the Product Distribution Layer (PDL) library.
- ``aqms-cms-libs`` -> The ``aqms-cms-libs`` project, which is the AQMS CMS library source code.
- ``aqms-cms`` -> This ``aqms-cms`` project, which is the AQMS CMS source code.

#### How to compile, build, and run CMS (QWServer)

This builds the executable ``QWServer.jar`` in the ``aqms-cms`` directory. This must be done with Java 1.8.
```

To run the QuakeWatch server:
```
$ cd aqms-cms
$ java -jar QWServer.jar
```
