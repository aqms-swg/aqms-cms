#include <iostream>
#include <cstdlib>
#include <cstring>
#include "Connection.h"
#include "Message.h"
#include "RetCodes.h"
#include "Compat.h"

const int maxmsg = 100000;

enum COMMAND{BADCMD=0,EXIT,SUBSCRIBE,UNSUBSCRIBE,LISTEN,SEND};

void handleEvent(Message &m,void *arg);
COMMAND getCode(char* cmd,char** args,int &size);

struct object{
    int val;
};


int main(int argc,char* argv[]){
  char cmd[1024];
  char* args[10];
  int charcount = 0;
  char c;

  char* file  = (char*)"./tms.cfg";
  char* name;
  if(argc<4){
    cout <<"Usage: cmsclient  -config <filename> -name <program name>"<<endl;
    return -1;
  }
  
  for(int n=1;n<4;n++){
    if(strcmp(argv[n],"-config")==0){
      file = argv[n+1];
    }
    else if(strcmp(argv[n],"-name")==0){
      name = argv[n+1];
    }
  }

  Connection conn(file,name);
  if(!conn){
    cout <<"No valid Connection object returned"<<endl;
    return -1;
  }
  if(conn.registerHandler(TN_TMT_EVENT_SIG,handleEvent,NULL) != TN_SUCCESS){
      cout<<"Error registering QW message handler\n";
      exit(-1);
  }   
  
  cout<<endl;
  while(1){
    charcount = 0;
    cout<<name<<">";
    do{
      c = cin.get();
      cmd[charcount] = c;
      charcount++;
    }while(c != '\n');
    cmd[charcount-1] = '\0';
    
    //Get command code//
    int size=0;
    char* chan = NULL;
    char* secstr = NULL;
    char* msgstr = NULL;
    Message* msg = NULL;
    int sec = 0;
    int msgid = 0;

    memset(args,0,10*sizeof(char*));
    COMMAND code = (COMMAND)getCode(cmd,args,size);
    // cout <<"Code = "<<code<<endl;
    switch(code){
    case EXIT:
      exit(0);
      
    case SUBSCRIBE:
      chan = args[0];
      if(chan!=NULL)
	conn.subscribe(chan);
      else{
	cout<<"Usage: Sub <channel name>"<<endl;
      }
      break;
    case UNSUBSCRIBE:
      chan = args[0];
      if(chan!=NULL)
	conn.unsubscribe(chan);
      else{
	cout<<"Usage: Unsub <channel name>"<<endl;
      }
      break;    
    case LISTEN:
      secstr = args[0];
      sec = 0;
      if(secstr!=NULL)
	sec = atoi(secstr);
      if(sec>0){
	conn.listen(sec);
      }
      else{
	cout<<"Usage: Listen <seconds>"<<endl;
      }
      break;
    case SEND:
      msgstr = args[0];
      chan = args[1];
      msgid = 0;
      if(msgstr == NULL || chan == NULL){
		cout<<"Usage: Send <msg id> <channel name>"<<endl;
		break;
      }
      msgid = atoi(msgstr);
      if(msgid>0){
	msg = new Message(TN_TMT_EVENT_SIG);
	msg->setDest(chan);
	msg->append(msgid);
	conn.send(*msg);
      }
      break;
    case BADCMD:
    default:
      break;
    } 
  }
}

COMMAND getCode(char* cmd,char* args[],int &size){
  int argc = 0;
  char* arg= NULL;
  char* cmdstr = strtok(cmd," ");
  COMMAND c=BADCMD;

  //cout<<"Received command \""<<cmdstr<<"\""<<endl;
  if(cmdstr==NULL){
    return (COMMAND)BADCMD;
  }
  
  if(strcasecmp(cmdstr,"exit")==0){
    c = EXIT;
  }
  else if(strcasecmp(cmdstr,"sub")==0){
    c = SUBSCRIBE;
  }
  else if(strcasecmp(cmdstr,"unsub")==0){
    c = UNSUBSCRIBE;
  }
  else if(strcasecmp(cmdstr,"listen")==0){
    c = LISTEN;
  }
  else if(strcasecmp(cmdstr,"send")==0){
    c = SEND;
  }


  while((arg = strtok(NULL," "))!=NULL){
    args[argc] = arg;
    argc++;
  }
  size  = argc - 1;

  return c;
}

// int main(int argc,char* argv[]){
    
// //   object *o1 = new object();
// //   o1->val = 10;
  
// //   object& o2 = *o1;
// //   o2.val = 20;

// //   cout <<"o1->val = "<<o1->val<<endl;

//   char* mode;
//   char* file  = "./tms.cfg";
//   char* name;
//   char* chan;
//   if(argc<8){
//     cout <<"Usage: cmsclient -mode (send | receive) -config <filename> -chan <channel name> -name <program name>"<<endl;
//     return -1;
//   }
  
  
//   for(int n=1;n<8;n++){
//     if(strcmp(argv[n],"-mode")==0){
//       mode = argv[n+1];
//     }
//     else if(strcmp(argv[n],"-config")==0){
//       file = argv[n+1];
//     }
//     else if(strcmp(argv[n],"-name")==0){
//       name = argv[n+1];
//     }
//     else if(strcmp(argv[n],"-chan")==0){
//       chan = argv[n+1];
//     }
//   }

//   Connection::initialize(file,name);
//   Connection conn = Connection::getConnection();
//   if(!conn){
//     cout <<"No valid Connection object returned"<<endl;
//     return -1;
//   }
//   //  conn.subscribe("/abc/abc");
  
//   if(strcmp(mode,"send")==0){
//     //  cout <<"Trying to send "<<maxmsg<<" messages"<<endl;
//     //Send 100 Messages//
//     long oldtime=0;
//     short count = 0;
//     long id=0;
//     for(long i=0;i<maxmsg;i++){
//       //    sleep(2);
//       struct timeval time;
//       gettimeofday(&time,NULL);
//       if(oldtime == time.tv_sec){
// 	count++;
//       }
//       else{
// 	oldtime = time.tv_sec;
// 	count = 0;
//       }
//       id++;
//       Message* msg = new Message(TN_TMT_EVENT_SIG);
//       msg->setDest(chan);
//       for(int x = 0;x<1;x++){
	  
// 	  msg->append(id);
//       }
//       conn.send(*msg);
//       cout <<id<<" sent."<<endl;
//       //  cout <<"Message "<<i+1<<" has been sent successfuly"<<endl;
//       delete msg;
//       //    cout <<"Done"<<endl;
//     }
//   }
//   else if(strcmp(mode,"receive")==0){
//     cout <<"Receiving.."<<endl;
//     conn.subscribe(chan);
//     cout <<"Subscribed to "<<chan<<endl;
//     // Register handler for TN_TMT_EVENT_SIG messages
//     if(conn.registerHandler(TN_TMT_EVENT_SIG,handleEvent,NULL) != TN_SUCCESS){
//       cout<<"Error registering QW message handler\n";
//       exit(-1);
//     }   
//     cout <<"Registered callback"<<endl;
//     int n=0;
//     while(n<2){
//       cout<<"*** Invoking listen() ****"<<endl;
//       conn.listen(20);
//       n++;
//       // sleep(5);
//     }    
//   }
//   else if(strcmp(mode,"crazy")==0){
//     cout <<"Subscribing 100 channels"<<endl;
//     char chanstr[128];
//     for(int i=0;i<100;i++){
//       sprintf(chanstr,"%s_%d",chan,i);
//       conn.subscribe(chanstr);
//     }
//     // Register handler for TN_TMT_EVENT_SIG messages
//     if(conn.registerHandler(TN_TMT_EVENT_SIG,handleEvent,NULL) != TN_SUCCESS){
//       cout<<"Error registering QW message handler\n";
//       exit(-1);
//     }   
//     cout <<"Registered callback"<<endl;
//     for(int z=0;z<2;z++){
// 	  conn.listen(20);
//     }  
//   }
//   else{
//     cout <<"Invalid mode"<<endl;
//     return -1;
//   }
//   cout <<"Disconnecting...";
//   Connection::removeConnection();
//   cout <<"Bye"<<endl;
//   return 0;
// }


void handleEvent(Message &m,void *arg){
  long id = 0;
  // Unpack the event identifier
  if (m.next(id) != TN_SUCCESS) {
      cout<<"(handleEventMsg): Unable to unpack message identifier";
      return;
  }
  cout <<Compat::Form("Received Message %d",id)<<endl;
}
