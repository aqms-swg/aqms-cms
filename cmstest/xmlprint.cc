#include <iostream>
#include <list>
#include <cstdlib>
#include <cerrno>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>

XERCES_CPP_NAMESPACE_USE
using namespace std;


void printXML(const char* msgstr);
void printElement(DOMNode* node,int tabcount);


int main(int argc,char* argv[]){
    try{
	XMLPlatformUtils::Initialize();
    }
    catch(const XMLException& e){
	char* msg = XMLString::transcode(e.getMessage());
	cout << "Error during initialization : " << msg << endl;
	XMLString::release(&msg);
	return NULL;
    }
    if(argc < 2){
	cout <<"Usage:$parse <xml file>"<<endl;
	return 0;
    }
    //Read XML file in a buffer
    char* buf = (char*)malloc(1024*10);
    char* tmp = buf;
    int xfd;
    if((xfd = open(argv[1],O_RDONLY))<0){
	cout << "Error opening file "<<argv[1]<<endl;
	return -1;
    }

    int n;
    while((n = read(xfd,buf,1024))>0){
       buf += n;
    }
    if(n<0){
	free(buf);
	cout<< "Error reading file "<<argv[1]<<endl;
    }
    buf = tmp;

    printXML(buf);

    return 0;
}

void printXML(const char* msgstr){
  if(!msgstr)
    return;

    XercesDOMParser* parser = new XercesDOMParser();
    parser->setValidationScheme(XercesDOMParser::Val_Never);
    parser->setDoNamespaces(false);
    
    ErrorHandler* errHandler = (ErrorHandler*) new HandlerBase();
    parser->setErrorHandler(errHandler);

    XMLCh* sysid = XMLString::transcode("QWMessage");
    MemBufInputSource* xmlbuf = new MemBufInputSource((const   XMLByte* const)msgstr
						      ,(const unsigned int)strlen(msgstr),(const XMLCh* const)sysid);
    XMLString::release(&sysid);

    try{
	parser->parse(*xmlbuf);
	DOMDocument *doc = parser->getDocument();
	if(!doc)
	  return;
	DOMNode *n = (DOMNode*)doc->getDocumentElement();
	if(!n)
	  return;
	printElement(n,0);
    }
    catch(...){
      cout << "Error parsing xml buffer" <<endl;
    }
    
    delete xmlbuf;
    delete parser;
    delete errHandler;
}

void printElement(DOMNode* node,int tabcount){ //recursively print elements
  if(!node)
    return;

  if (node->getNodeType() == DOMNode::TEXT_NODE){			
	DOMText* textNode = (DOMText*)node;
	XMLCh* string = (XMLCh*)textNode->getData();
	char* data = XMLString::transcode(string);
	cout << data;
	XMLString::release(&data);
	return;
  }
  else if(node->getNodeType() != DOMNode::ELEMENT_NODE){
    return;
  }
  
  for(int i=0;i<tabcount;i++){
    cout<<"\t";
  }
  
  char *elename = XMLString::transcode(node->getNodeName());
  cout << "<"<<elename<<" ";
  
  if(node->hasAttributes()) {
    // get all the attributes of the node
    DOMNamedNodeMap *pAttributes = node->getAttributes();
    int nSize = pAttributes->getLength();
    for(int i=0;i<nSize;++i) {
      DOMAttr *pAttributeNode = (DOMAttr*) pAttributes->item(i);
      // get attribute name
      char *_name = XMLString::transcode(pAttributeNode->getName());
      char *_value = XMLString::transcode(pAttributeNode->getValue());	
      cout <<_name<<"="<<_value<<" ";
      XMLString::release(&_name);
      XMLString::release(&_value);
    }
  }

  cout<<">";
  DOMNode *child=0;
  tabcount++;
  for (child = node->getFirstChild(); child != NULL; child=child->getNextSibling()){
    printElement(child,tabcount);
  }  
  cout<<"</"<<elename<<">"<<endl;
  XMLString::release(&elename);  
}
