#include <iostream>
#include <fstream>
#include <list>
#include <cstdlib>
#include <cerrno>
#include <cstdlib>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "ContentTable.h"
#include "QWMessageParser.h"
#include "QWMessageSerializer.h"
#include "RTException.h"
#include "RetCodes.h"
#include "Message.h"
#include "FileMessageQueue.h"

using namespace std;

int main(int argc,char* argv[]){
    try{
	XMLPlatformUtils::Initialize();
    }
    catch(const XMLException& e){
	char* msg = XMLString::transcode(e.getMessage());
	cout << "Error during initialization : " << msg << endl;
	XMLString::release(&msg);
	return NULL;
    }

    if(argc<2){
      cout <<"Usage: $printqfile <file>"<<endl;
      return -1;
    }
    
    char* buf;
    int count  =0;
    const char* filename = argv[1];
     try{
	FileMessageQueue::printFile(filename);
     }
    catch(RTException e){
	cout << e.msg << endl;
	return -1;
    }

    return 0;
}
