#include <list>
#include <cstdlib>
#include <cerrno>
#include <cstdlib>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "ContentTable.h"
#include "QWMessageParser.h"
#include "QWMessageSerializer.h"
#include "RTException.h"
#include "RetCodes.h"
#include "Message.h"

using namespace std;

void getcontent(Message* msg);

int main(int argc,char* argv[]){
    try{
	XMLPlatformUtils::Initialize();
    }
    catch(const XMLException& e){
	char* msg = XMLString::transcode(e.getMessage());
	cout << "Error during initialization : " << msg << endl;
	XMLString::release(&msg);
	return NULL;
    }

    char* buf;
    int count  =0;
     try{
 	while(1){
	    if(count == 5000) break;
    	    count++;
	    
	    cout << "step 1"<<endl;
	    Message* msg = new Message(TN_TMT_EVENT_SIG);
	    msg->setDest("/home/solanki");
	    //DATA
	    	    cout << "step 2"<<endl;
	    short A[] = {2,4,6,456,34,23,545,123,64};
	    int B[] = {2,4,6,456,34,23,545,123,64};
	    long long C[] = {2,4,6,456,34,23,545,123,64};
	    double D[] = {2,4,6,456,34,23,545,123,64};
	    float E[] = {2,4,6,456,34,23,545,123,64};
	    const char* STR[] = {"Hello","How","are","you","?"};
	    
	    const char y = 'K';
	    short a = 1;
	    int b = 2;
	    long long c = 3;
	    double d = 3.14;
	    float e = 454.232;
	    const char* str = "Hello World!";
	    	    cout << "step 3"<<endl;
	    msg->append(y);
	    msg->append(a);
	    msg->append(b);
	    msg->append(c);
	    msg->append(d);
	    msg->append(e);
	    msg->append(str);
	    
	    msg->append(A,9);
	    msg->append(B,9);
	    msg->append(C,9);
	    msg->append(D,9);
	    msg->append(E,9);
	    msg->append((char**)STR,5);

	    Message *rmsg = new Message(msg->getContentTable(),TN_TMT_EVENT_SIG);
	    getcontent(rmsg);
	    delete rmsg;

	    cout << "step 4"<<endl;
 	    QWMessageParser *p = new QWMessageParser();
 	    QWMessageSerializer *s = new QWMessageSerializer();
	    cout << "step 5"<<endl;
	    ContentTable *ctable = msg->getContentTable();
	    cout << "step 6"<<endl;
	    //s->setDomainName((char*)"RT3");
	    //s->setMessageName((char*)"EVENTID");
	    //s->setType((char*)"/test/tmts");
	    //s->setSourceName((char*)"/test/trimag");
	    cout << "step 7"<<endl;
	    //buf =  (char*)s->serialize(ctable);	
	    buf =  (char*)s->serialize(*msg);	
	    cout << "step 8"<<endl;
	    cout << "step 9"<<endl;
	    delete msg;
	    cout << "step 10"<<endl;
	    if(!buf){
 		cout << "buf is null -exit"<<endl;
 		delete p;
 		delete s;
		return -1;
	    }
	    cout << buf << endl;

	    ctable = p->parse(buf);
	    free(buf);

	    if(!ctable){
		cout << "ctable is null -exit"<<endl;
		delete p;
		delete s;
		return -1;//continue; //	return -1;
	    }

	    delete ctable;
	    delete p;
	    delete s;
	}
     }
    catch(RTException e){
	cout << e.msg << endl;
	return -1;
    }

    return 0;
}

void getcontent(Message* msg){

	    short* A;int A_size;
	    int* B; int B_size;
	    long long* C;int C_size;
	    double* D; int D_size;
	    float* E; int E_size;
	    char** STR; int STR_size;
	    
	    char y;
	    short a;
	    int b;
	    long long c;
	    double d;
	    float e;
	    char* str;
	    
	    PSTEP();
	    if(msg->next(y)==TN_FAILURE){
	      cout << "Failure in y"<<endl;
	      exit(0);
	    }
 	    if(msg->next(a)==TN_FAILURE){
	      cout << "Failure in a"<<endl;
	      exit(0);
	    }
 	    if(msg->next(b)==TN_FAILURE){
	      cout << "Failure in b"<<endl;
	      exit(0);
	    }
	    if(msg->next(c)==TN_FAILURE){
	      cout << "Failure in c"<<endl;
	      exit(0);
	    }
 	    if(msg->next(d)==TN_FAILURE){
	      cout << "Failure in d"<<endl;
	      exit(0);
	    }
	    if(msg->next(e)==TN_FAILURE){
	      cout << "Failure in e"<<endl;
	      exit(0);
	    }
 	    if(msg->next(&str)==TN_FAILURE){
	      cout << "Failure in str"<<endl;
	      exit(0);
	    }
	    
 	    if(msg->next(&A,A_size)==TN_FAILURE){
	      cout << "Failure in A"<<endl;
	      exit(0);
	    }
	    if(msg->next(&B,B_size)==TN_FAILURE){
	      cout << "Failure in B"<<endl;
	      exit(0);
	    }
  	    if(msg->next(&C,C_size)==TN_FAILURE){
	      cout << "Failure in C"<<endl;
	      exit(0);
	    }
	      //  exit(0);
	    //cout << "size of long long array is "<<C_size << endl;
	    if(msg->next(&D,D_size)==TN_FAILURE){
	      cout << "Failure in D"<<endl;
	      exit(0);
	    }
	    if(msg->next(&E,E_size)==TN_FAILURE){
	      cout << "Failure in E"<<endl;
	      exit(0);
	    }
	    if(msg->next(&STR,STR_size)==TN_FAILURE){
	      cout << "Failure in STR"<<endl;
	      exit(0);
	    }

	    //Print all values
	    cout <<"char y = "<<y<<endl;
	    cout <<"short a = "<<a<<endl;
	    cout <<"int b = "<<b<<endl;
	    cout <<"long long c = "<<c<<endl;
	    cout <<"double d = "<<d<<endl;
	    cout <<"float e = "<<e<<endl;
 	    cout <<"string str = "<<str<<endl;

 	    cout<<"\nshort array:";
	    for(int i=0;i<A_size;i++){
	      cout << A[i] <<",";
	    }
	    cout<<"\nint array:";
	    for(int i=0;i<B_size;i++){
	      cout << B[i] <<",";
	    }
	    cout<<"\nlong long array:";
	    for(int i=0;i<C_size;i++){
	      cout << C[i] <<",";
	    }
	    cout<<"\ndouble array:";
	    for(int i=0;i<D_size;i++){
	      cout << D[i] <<",";
	    }
	    cout<<"\nfloat array:";
	    for(int i=0;i<E_size;i++){
	      cout << E[i] <<",";
	    }
	    cout<<"\nstring array:";
	    for(int i=0;i<STR_size;i++){
	      cout << STR[i] <<" ";
	    }
}
