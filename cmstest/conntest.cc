#include "Connection.h"
#include "Message.h"
#include "RetCodes.h"
#include "Compat.h"
#include <cstdlib>
#include <map>

using namespace std;
const int maxmsg = 100000;

typedef std::map<long,int> MsgMap;

void handleEvent(Message &m,void *arg);
void handleEvent_Bulk(Message &m,void *arg);

struct object{
    int val;
};

MsgMap history;
const unsigned int MESSAGE_SIZE = 7000;

int main(int argc,char* argv[]){
    
//   object *o1 = new object();
//   o1->val = 10;
  
//   object& o2 = *o1;
//   o2.val = 20;

//   cout <<"o1->val = "<<o1->val<<endl;

  char* mode;
  char* file  = (char*)"./tms.cfg";
  char* name;
  char* chan;
  if(argc<8){
    cout <<"Usage: $conntest -mode (send | receive | heartbeat | alarmevent | bulk_send | bulk_receive) -config <filename> -chan <channel name> -name <program name>"<<endl;
    return -1;
  }
  
  
  for(int n=1;n<8;n++){
    if(strcmp(argv[n],"-mode")==0){
      mode = argv[n+1];
    }
    else if(strcmp(argv[n],"-config")==0){
      file = argv[n+1];
    }
    else if(strcmp(argv[n],"-name")==0){
      name = argv[n+1];
    }
    else if(strcmp(argv[n],"-chan")==0){
      chan = argv[n+1];
    }
  }

  Connection conn(file,name);
  // Connection *conn = Connection::getConnection();
  if(!conn){
    cout <<"No valid Connection object returned"<<endl;
    return -1;
  }
  //  conn.subscribe("/abc/abc");
  
  if(strcmp(mode,"send")==0){
    //  cout <<"Trying to send "<<maxmsg<<" messages"<<endl;
    //Send 100 Messages//
    long oldtime=0;
    short count = 0;
    long id=0;
    // char[] lc = "  ";
    for(long i=0;i<maxmsg;i++){
      sleep(1);
      struct timeval time;
      gettimeofday(&time,NULL);
      id = time.tv_sec;
      Message* msg = new Message(TN_TMT_EVENT_SIG);
      msg->setDest(chan);
      for(int x = 0;x<1;x++){  
       	  msg->append(id);
	  msg->append("  ");
      }
      conn.send(*msg);
      cout <<id<<" sent."<<endl;
      //  cout <<"Message "<<i+1<<" has been sent successfuly"<<endl;
      delete msg;
      //    cout <<"Done"<<endl;
    }
  }//TN_TMT_HEARTBEAT
  else if(strcmp(mode,"bulk_send")==0){
      Message* msg = new Message(TN_TMT_EVENT_SIG);
      msg->setDest(chan);
      struct timeval time;
      gettimeofday(&time,NULL);
      long id = time.tv_sec;
      for(int x = 0;x<MESSAGE_SIZE;x++){  
       	  msg->append(x);
      }
      msg->append(id);
      conn.send(*msg);
      cout <<id<<" sent."<<endl;
      delete msg;
  }
  else if(strcmp(mode,"heartbeat")==0){
    //  cout <<"Trying to send "<<maxmsg<<" messages"<<endl;
    //Send 100 Messages//
    long oldtime=0;
    short count = 0;
    long id=0;
    // char[] lc = "  ";
    for(long i=0;i<maxmsg;i++){
      sleep(1);
      struct timeval time;
      gettimeofday(&time,NULL);
      id = time.tv_sec;
      Message* msg = new Message(TN_TMT_HEARTBEAT);
      msg->setDest(chan);
      for(int x = 0;x<1;x++){
	  
	  msg->append(id);
	  //msg->append("  ");
      }
      conn.send(*msg);
      cout <<id<<"heartbeat sent."<<endl;
      //  cout <<"Message "<<i+1<<" has been sent successfuly"<<endl;
      delete msg;
      //    cout <<"Done"<<endl;
    }
  }
  else if(strcmp(mode,"alarmevent")==0){
    //  cout <<"Trying to send "<<maxmsg<<" messages"<<endl;
    //Send 100 Messages//
    long oldtime=0;
    short count = 0;
    long id=0;
    // char[] lc = "  ";
    for(long i=0;i<maxmsg;i++){
      sleep(1);
      struct timeval time;
      gettimeofday(&time,NULL);
      id = time.tv_sec;
      Message* msg = new Message(TN_TMT_ALARM);
      msg->setDest(chan);
      int level = 1;
      float mag = 1.0;
      char type[123];
      strcpy(type,"TEST");
      for(int x = 0;x<1;x++){
	  msg->append(id);
 	  msg->append(type);
 	  msg->append(level);
 	  msg->append(mag);
      }
      conn.send(*msg);
      cout <<id<<"heartbeat sent."<<endl;
      //  cout <<"Message "<<i+1<<" has been sent successfuly"<<endl;
      delete msg;
      //    cout <<"Done"<<endl;
    }
  }
  else if(strcmp(mode,"receive")==0){
    cout <<"Receiving.."<<endl;
    conn.subscribe(chan);
    cout <<"Subscribed to "<<chan<<endl;
    // Register handler for TN_TMT_EVENT_SIG messages
    if(conn.registerHandler(TN_TMT_EVENT_SIG,handleEvent,NULL) != TN_SUCCESS){
      cout<<"Error registering QW message handler\n";
      exit(-1);
    }   
    cout <<"Registered callback"<<endl;
    int n=0;
    while(n<200){
      cout<<"*** Invoking listen() ****"<<endl;
      conn.listen(60);
      n++;
      // sleep(5);
    }    
  }
  else if(strcmp(mode,"bulk_receive")==0){
    cout <<"Receiving.."<<endl;
    conn.subscribe(chan);
    cout <<"Subscribed to "<<chan<<endl;
    // Register handler for TN_TMT_EVENT_SIG messages
    if(conn.registerHandler(TN_TMT_EVENT_SIG,handleEvent_Bulk,NULL) != TN_SUCCESS){
      cout<<"Error registering QW message handler\n";
      exit(-1);
    }   
    cout <<"Registered callback"<<endl;
    int n=0;
    while(n<200){
      cout<<"*** Invoking listen() ****"<<endl;
      conn.listen(60);
      n++;
      // sleep(5);
    }    
  }
  else if(strcmp(mode,"crazy")==0){
    cout <<"Subscribing 100 channels"<<endl;
    char chanstr[128];
    for(int i=0;i<100;i++){
      sprintf(chanstr,"%s_%d",chan,i);
      conn.subscribe(chanstr);
    }
    // Register handler for TN_TMT_EVENT_SIG messages
    if(conn.registerHandler(TN_TMT_EVENT_SIG,handleEvent,NULL) != TN_SUCCESS){
      cout<<"Error registering QW message handler\n";
      exit(-1);
    }   
    cout <<"Registered callback"<<endl;
    for(int z=0;z<2;z++){
	  conn.listen(20);
    }  
  }
  else{
    cout <<"Invalid mode"<<endl;
    return -1;
  }
  cout <<"Disconnecting...";
  //  Connection::removeConnection();
  cout <<"Bye"<<endl;
  return 0;
}


void handleEvent(Message &m,void *arg){
  long id = 0;
  // Unpack the event identifier
  if (m.next(id) != TN_SUCCESS) {
      cout<<"(handleEventMsg): Unable to unpack message identifier";
      return;
  }

  if(history[id] == 825){
      cout <<Compat::Form("==> Duplicate Message %d",id)<<endl;
  }
  else{
      history[id] = 825;
      cout <<Compat::Form("Received Message %d",id)<<endl;
  }
}

void handleEvent_Bulk(Message &m,void *arg){
  long id = 0;
  // Unpack the event identifier
  for(int i=0;i<MESSAGE_SIZE+1;i++){
    if (m.next(id) != TN_SUCCESS) {
      cout<<"(handleEventMsg): Unable to unpack message identifier";
      return;
    }
    cout <<"Received id : "<<id<<endl;
  }
}

