// This code does not compile, as of 2011/12/01. It appears to have not been
// updated since changes were made to cms library.
// It is not clear to me that there is any point in updating this code, since
// other programs in this suite appear to test XML parsing.
// PNL

#include <list>
#include <cstdlib>
#include <cerrno>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "ContentTable.h"
#include "QWMessageParser.h"
#include "QWMessageSerializer.h"
#include "RTException.h"
#include "RetCodes.h"
#include "Message.h"

using namespace std;

void getcontent(Message* msg);

int main(int argc,char* argv[]){
    try{
	XMLPlatformUtils::Initialize();
    }
    catch(const XMLException& e){
	char* msg = XMLString::transcode(e.getMessage());
	cout << "Error during initialization : " << msg << endl;
	XMLString::release(&msg);
	return NULL;
    }

    char* buf;
    int count  =0;
     try{
 	while(1){
	    if(count == 5000) break;
    	    count++;

	    Message* msg = new Message(TN_TMT_EVENT_SIG);
	    msg->setDest("/home/solanki");
	    //DATA
	    const char y = 'K';
	    short a = 1;
	    
	    msg->append(y);
	    msg->append(a);

	    //  ContentTable * src = msg->getContentTable();
	    //  ContentTable ct = *src;
	    //  delete src;
	    //  cout << ct;
	    
	    Message *mcopy = new Message(TN_TMT_EVENT_SIG);
	    Message xcopy(TN_TMT_EVENT_SIG);
	    Message ycopy = xcopy;
	      *mcopy = *msg;
	    //  cout <<"========== Copied Message =========="<<endl;
	    cout <<*mcopy;
	    delete msg;
	    // delete mcopy;
	}
     }
    catch(RTException e){
	cout << e.msg << endl;
	return -1;
    }

    return 0;
}

