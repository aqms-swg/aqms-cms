This directory (cmstest) contains a suite of test programs for CMS. They were written by the cms author (Kalpesh Solanki at Caltech) as he was deveopling the CMS library. In some cases he made changes to the library after he had written the cmstest programs, and did not bother to update the tests. The programs parser and simpletest do not compile. Since it appears that the tests
they perform are also carried out by other programs, I have made no effort to get these two programs running.  
  
As was Kalpesh's style, there is negligible documentation in these programs other than the command-line syntax printed when programs are run with no arguments. Note that the programs may dump core on exit when run with no arguments: simply delete the core file unless you have the urge to do more.  
  
The most important program is "conntest", which does end-to-end testing of simple CMS messages. Its syntax is:  

```
Usage: $conntest -mode (send | receive | heartbeat | alarmevent | bulk_send | bulk_receive) -config <filename> -chan <channel name> -name <program name>
```  

In one window, run conntest in "receive" mode with a cms config file configured for an appropriate CMS server and some channel and program name. In another window, run conntest in "send" mode with an appropriate cms config file, the SAME channel name and a DIFFERENT program name. In send mode, a message is published every second as long as the sender is running. The "receive" conntest should print a line for each message it receives. The cms config files for these two instances of conntest do not have to be  identical. For instance, you may have a chain of CMS servers and went to send messages through that chain. Thus the cms config files must be set up to use whatever CMS servers you want to test.  
  
Pete Lombard, UCB (retired)   
2011/12/01  
