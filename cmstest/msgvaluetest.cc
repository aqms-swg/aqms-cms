#include <list>
#include <cstdlib>
#include <cerrno>
#include <cstdlib>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "ContentTable.h"
#include "QWMessageParser.h"
#include "QWMessageSerializer.h"
#include "RTException.h"
#include "RetCodes.h"
#include "Message.h"

using namespace std;

void getcontent(Message* msg);

int main(int argc,char* argv[]){
    try{
	XMLPlatformUtils::Initialize();
    }
    catch(const XMLException& e){
	char* msg = XMLString::transcode(e.getMessage());
	cout << "Error during initialization : " << msg << endl;
	XMLString::release(&msg);
	return NULL;
    }

    char* buf;
     try{

	    Message* msg = new Message(TN_TMT_EVENT_SIG);
	    msg->setDest("/home/solanki");
	    msg->append((short)5);
	    msg->append((int)335303290);
	    msg->append((long)2112313131);
	    msg->append((float)1.5);
	    msg->append(30000001.142796);

 	    QWMessageSerializer *s = new QWMessageSerializer();
	    buf =  (char*)s->serialize(*msg);	
	    delete msg;
	    if(!buf){
 		cout << "buf is null -exit"<<endl;
 		delete s;
		return -1;
	    }
	    cout << buf << endl;

	    //Parse
	    QWMessageParser *p = new QWMessageParser();
	    ContentTable *ctable =  p->parse(buf);
	    msg = new Message(ctable,TN_TMT_EVENT_SIG);
	    short a;
	    int b;
	    long c;
	    float d;
	    double e;
	    
	    msg->next(a);
	    msg->next(b);
	    msg->next(c);
	    msg->next(d);
	    msg->next(e);

	    cout <<"a = "<<a<<endl;
	    cout <<"b = "<<b<<endl;
	    cout <<"c = "<<c<<endl;
	    cout <<"d = "<<d<<endl;
	    printf("e = %f\n",e);

	    free(buf);
	    delete s;
     }
     catch(RTException e){
       cout << e.msg << endl;
       return -1;
     }

    return 0;
}

void getcontent(Message* msg){

	    short* A;int A_size;
	    int* B; int B_size;
	    long long* C;int C_size;
	    double* D; int D_size;
	    float* E; int E_size;
	    char** STR; int STR_size;
	    
	    char y;
	    short a;
	    int b;
	    long long c;
	    double d;
	    float e;
	    char* str;
	    
	    PSTEP();
	    if(msg->next(y)==TN_FAILURE){
	      cout << "Failure in y"<<endl;
	      exit(0);
	    }
 	    if(msg->next(a)==TN_FAILURE){
	      cout << "Failure in a"<<endl;
	      exit(0);
	    }
 	    if(msg->next(b)==TN_FAILURE){
	      cout << "Failure in b"<<endl;
	      exit(0);
	    }
	    if(msg->next(c)==TN_FAILURE){
	      cout << "Failure in c"<<endl;
	      exit(0);
	    }
 	    if(msg->next(d)==TN_FAILURE){
	      cout << "Failure in d"<<endl;
	      exit(0);
	    }
	    if(msg->next(e)==TN_FAILURE){
	      cout << "Failure in e"<<endl;
	      exit(0);
	    }
 	    if(msg->next(&str)==TN_FAILURE){
	      cout << "Failure in str"<<endl;
	      exit(0);
	    }
	    
 	    if(msg->next(&A,A_size)==TN_FAILURE){
	      cout << "Failure in A"<<endl;
	      exit(0);
	    }
	    if(msg->next(&B,B_size)==TN_FAILURE){
	      cout << "Failure in B"<<endl;
	      exit(0);
	    }
  	    if(msg->next(&C,C_size)==TN_FAILURE){
	      cout << "Failure in C"<<endl;
	      exit(0);
	    }
	      //  exit(0);
	    //cout << "size of long long array is "<<C_size << endl;
	    if(msg->next(&D,D_size)==TN_FAILURE){
	      cout << "Failure in D"<<endl;
	      exit(0);
	    }
	    if(msg->next(&E,E_size)==TN_FAILURE){
	      cout << "Failure in E"<<endl;
	      exit(0);
	    }
	    if(msg->next(&STR,STR_size)==TN_FAILURE){
	      cout << "Failure in STR"<<endl;
	      exit(0);
	    }

	    //Print all values
	    cout <<"char y = "<<y<<endl;
	    cout <<"short a = "<<a<<endl;
	    cout <<"int b = "<<b<<endl;
	    cout <<"long long c = "<<c<<endl;
	    cout <<"double d = "<<d<<endl;
	    cout <<"float e = "<<e<<endl;
 	    cout <<"string str = "<<str<<endl;

 	    cout<<"\nshort array:";
	    for(int i=0;i<A_size;i++){
	      cout << A[i] <<",";
	    }
	    cout<<"\nint array:";
	    for(int i=0;i<B_size;i++){
	      cout << B[i] <<",";
	    }
	    cout<<"\nlong long array:";
	    for(int i=0;i<C_size;i++){
	      cout << C[i] <<",";
	    }
	    cout<<"\ndouble array:";
	    for(int i=0;i<D_size;i++){
	      cout << D[i] <<",";
	    }
	    cout<<"\nfloat array:";
	    for(int i=0;i<E_size;i++){
	      cout << E[i] <<",";
	    }
	    cout<<"\nstring array:";
	    for(int i=0;i<STR_size;i++){
	      cout << STR[i] <<" ";
	    }
}
