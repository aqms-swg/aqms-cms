#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <sys/time.h>

#define BLOCKSIZE 250*1024

int main(void){

    char* src = (char*)std::malloc(BLOCKSIZE);
    char* dest = (char*)std::malloc(BLOCKSIZE);

    struct timeval before,after;
    gettimeofday(&before,NULL);
    for(int j=0;j<500;j++){
	for(int i = 0;i<10;i++){
	    std::memcpy(dest,src,BLOCKSIZE);
	}
    }
    gettimeofday(&after,NULL);
    double dur = after.tv_sec - before.tv_sec + 
	1.0e-6 * (after.tv_usec - before.tv_usec);
    
    std::printf("memcpy took %f seconds.\n",dur);


}
