#include <list>
#include <cstdlib>
#include <cerrno>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "ContentTable.h"
#include "QWMessageParser.h"
#include "QWMessageSerializer.h"
#include "RTException.h"
#include "RetCodes.h"
#include "Message.h"
#include "FileMessageQueue.h"

using namespace std;

void fillMessage(Message* msg,short);

int main(int argc,char* argv[]){
    try{
	XMLPlatformUtils::Initialize();
    }
    catch(const XMLException& e){
	char* msg = XMLString::transcode(e.getMessage());
	cout << "Error during initialization : " << msg << endl;
	XMLString::release(&msg);
	return NULL;
    }

//     fstream file;
//     file.open("Testfile",fstream::out | fstream::binary);
//     if(file.fail()==true){
// 	cout <<"Can't open file"<<endl;
// 	return 0;
//     }


    char* buf;
    int count  =0;
    //    const char* filename = "Sample.q";
    try{
      FileMessageQueue* fmq;
      fmq = new FileMessageQueue(string("Sample.q"),1000);
      QWMessageSerializer s;
      while(1){
      	if(count == 10000) break;
      	count++;
	
	Message* msg;
	msg = new Message(TN_TMT_EVENT_SIG);
	fillMessage(msg,count);
	fmq->put(*msg);	    
	delete msg;
	msg = fmq->get();
	if(msg){
	  const char* buf = s.serialize(*msg);
	  cout << buf <<endl;
	  fmq->remove();
	  delete msg->getContentTable();
	  delete msg;
	  free((void*)buf);
	}
	else{
	  cout << "No Message returned"<<endl;
	}
      }
      delete fmq;
    }
    catch(RTException e){
      cout << e.msg << endl;
      return -1;
    }
    
    return 0;
}

void fillMessage(Message* msg,short msgid){
	    msg->setDest("/home/solanki");
	    msg->setDomainName("RT1");
	    msg->setSourceName("/home/kalpesh");

	    //DATA	    
	    const char y = 'K';
	    short a = msgid;
	    int b = 2;
	    long long c = 3;
	    double d = 3.14;
	    float e = 454.232;
	    const char* str = "Hello World!";

	    short A[] = {2,4,6,456,34,23,545,123,64};
	    int B[] = {2,4,6,456,34,23,545,123,64};
	    long long C[] = {2,4,6,456,34,23,545,123,64};
	    double D[] = {2,4,6,456,34,23,545,123,64};
	    float E[] = {2,4,6,456,34,23,545,123,64};
	    const char* STR[] = {"Hello","How","are","you","?"};

	    msg->append(y);
	    msg->append(a);
	    msg->append(b);
	    msg->append(c);
	    msg->append(d);
	    msg->append(e);
	    msg->append(str);

	    msg->append(A,9);
	    msg->append(B,9);
	    msg->append(C,9);
	    msg->append(D,9);
	    msg->append(E,9);
	    msg->append((char**)STR,5);

}

//UNSERVED CRC = db6627c9
//SERVED CRC   = a78a0c0e
