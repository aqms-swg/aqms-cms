// This code does not compile, as of 2011/12/01. It appears to have not been
// updated since changes were made to cms library.
// It is not clear to me that there is any point in updating this code, since
// other programs in this suite appear to test XML parsing.
// PNL

#include "ContentTable.h"
#include "QWMessageParser.h"
#include "QWMessageSerializer.h"
#include "RTException.h"
#include <list>
#include <iostream>
#include <cerrno>
#include <cstdlib>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

using namespace std;
using namespace MessageData;

int main(int argc,char* argv[]){
    try{
	XMLPlatformUtils::Initialize();
    }
    catch(const XMLException& e){
	char* msg = XMLString::transcode(e.getMessage());
	cout << "Error during initialization : " << msg << endl;
	XMLString::release(&msg);
	return NULL;
    }
    if(argc < 2){
	cout <<"Usage:$parse <xml file>"<<endl;
	return 0;
    }
    //Read XML file in a buffer
    char* buf = (char*)malloc(1024*10);
    char* tmp = buf;
    int xfd;
    if((xfd = open(argv[1],O_RDONLY))<0){
	cout << "Error opening file "<<argv[1]<<endl;
	return -1;
    }

    int n;
    while((n = read(xfd,buf,1024))>0){
       buf += n;
    }
    if(n<0){
	free(buf);
	cout<< "Error reading file "<<argv[1]<<endl;
    }
    buf = tmp;

    int count  =0;
    try{
	while(1){
	    if(count == 10000) break;
	    count++;
	    QWMessageParser *p = new QWMessageParser();
	    QWMessageSerializer *s = new QWMessageSerializer();
	    ContentTable* ctable = p->parse(buf);
	    free(buf);
	    if(!ctable){
		cout << "ctable is null -exit"<<endl;
		delete p;
		delete s;
		return -1;//continue; //	return -1;
	    }
	    
	    //cout << "Domain =" << p->getDomainName() <<endl;
	    //cout << "Name ="<< p->getMessageName() <<endl;
	    //cout << "Type =" << p->getType() << endl;
	    //cout << "Source ="<< p->getSourceName() << endl;
	    
	    //printContentTable(ctable);
	    
	    s->setDomainName(p->getDomainName());
	    s->setMessageName(p->getMessageName());
	    s->setType(p->getType());
	    s->setSourceName(p->getSourceName());
	    buf =  (char*)s->serialize(ctable);
	    cout << buf << endl;
	    
	    cleanupContentTable(ctable);
	    delete p;
	    delete s;
	}
    }
    catch(RTException e){
	cout << e.msg << endl;
	return -1;
    }
        

    return 0;
}
