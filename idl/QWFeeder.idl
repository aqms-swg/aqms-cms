//QWFeeder.idl:  CORBA interface definition for the QuakeWatch CORBA
//               feeder.
//
//  1/22/2004 -- [ET]  Initial version.
//  1/15/2005 -- [ET]  Added "sendSourced..." methods.
//
//

module com
{
  module isti
  {
    module quakewatch
    {
      module server
      {
        module qw_feeder
        {
          interface QWFeeder
          {

            /**
             * Sends a data event message.  If the event data does not
             * begin with a "DataMessage" XML element then the data will
             * be surrounded with one.  The "sendSourced..." methods are
             * preferred because the feeder-source host name and message
             * number are used for improved message tracking.
             * @param messageStr the data event message string.
             * @param feederSourceHostStr the data-source host string for
             * the message.
             * @param feederSrcHostMsgIdNum the message-ID number from the
             * data source (positive value incremented after each message).
             * @return true after the message has been successfully stored
             * and processed; false if an error occurred.
             */
            boolean sendSourcedMsg(in string messageStr,
                                              in string feederSourceHostStr,
                                        in long long feederSrcHostMsgIdNum);

            /**
             * Sends a data event message.  If the event data does not
             * begin with a "DataMessage" XML element then the data will
             * be surrounded with one.  The "sendSourced..." methods are
             * preferred because the feeder-source host name and message
             * number are used for improved message tracking.
             * @param domainStr the domain name to use.
             * @param typeStr the type name to use.
             * @param messageStr the data event message string.
             * @param feederSourceHostStr the data-source host string for
             * the message.
             * @param feederSrcHostMsgIdNum the message-ID number from the
             * data source (positive value incremented after each message).
             * @return true after the message has been successfully stored
             * and processed; false if an error occurred.
             */
            boolean sendSourcedDomainTypeMsg(in string domainStr,
                                   in string typeStr, in string messageStr,
                                              in string feederSourceHostStr,
                                        in long long feederSrcHostMsgIdNum);

            /**
             * Sends a data event message.  If the event data does not
             * begin with a "DataMessage" XML element then the data will
             * be surrounded with one.  The "sendSourced..." methods are
             * preferred because the feeder-source host name and message
             * number are used for improved message tracking.
             * @param domainStr the domain name to use.
             * @param typeStr the type name to use.
             * @param nameStr the event name to use.
             * @param messageStr the data event message string.
             * @param feederSourceHostStr the data-source host string for
             * the message.
             * @param feederSrcHostMsgIdNum the message-ID number from the
             * data source (positive value incremented after each message).
             * @return true after the message has been successfully stored
             * and processed; false if an error occurred.
             */
            boolean sendSourcedDomainTypeNameMsg(in string domainStr,
                in string typeStr, in string nameStr, in string messageStr,
                                              in string feederSourceHostStr,
                                        in long long feederSrcHostMsgIdNum);


            /**
             * Sends a data event message.  If the event data does not
             * begin with a "DataMessage" XML element then the data will
             * be surrounded with one.  The "sendSourced..." methods are
             * preferred because the feeder-source host name and message
             * number are used for improved message tracking.
             * @param messageStr the data event message string.
             * @return true after the message has been successfully stored
             * and processed; false if an error occurred.
             */
            boolean sendMessage(in string messageStr);

            /**
             * Sends a data event message.  If the event data does not
             * begin with a "DataMessage" XML element then the data will
             * be surrounded with one.  The "sendSourced..." methods are
             * preferred because the feeder-source host name and message
             * number are used for improved message tracking.
             * @param domainStr the domain name to use.
             * @param typeStr the type name to use.
             * @param messageStr the data event message string.
             * @return true after the message has been successfully stored
             * and processed; false if an error occurred.
             */
            boolean sendDomainTypeMessage(in string domainStr,
                                   in string typeStr, in string messageStr);

            /**
             * Sends a data event message.  If the event data does not
             * begin with a "DataMessage" XML element then the data will
             * be surrounded with one.  The "sendSourced..." methods are
             * preferred because the feeder-source host name and message
             * number are used for improved message tracking.
             * @param domainStr the domain name to use.
             * @param typeStr the type name to use.
             * @param nameStr the event name to use.
             * @param messageStr the data event message string.
             * @return true after the message has been successfully stored
             * and processed; false if an error occurred.
             */
            boolean sendDomainTypeNameMessage(in string domainStr,
                in string typeStr, in string nameStr, in string messageStr);

          };
        };
      };
    };
  };
};
